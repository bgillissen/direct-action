/*
@filename: initPlayerLocal.sqf
Author:
	Ben
Description:
	run on player by game engine on join
	call init functions depending on context in multiplayer
*/

if !( isMultiplayer ) exitWith {};

if ( CTXT_HEADLESS ) exitWith { [] spawn core_fnc_initHeadless; };
if ( CTXT_PLAYER ) exitWith { [] spawn core_fnc_initPlayer; };

nil