
if ( CTXT_HEADLESS ) exitWith { call core_fnc_initHeadless; };
if ( CTXT_PLAYER ) exitWith { 
	call core_fnc_initPlayer;
    ["PLAYER", "respawn", [player]] call core_fnc_featEvent;
	["SERVER", "respawn", [player]] call core_fnc_featEvent; 
};