/*
@filename: core\resetServer.sqf
Author:
	Ben
Description:
	run on server
*/

#include "_debug.hpp"

if ( SERVER_INIT ) exitWith {
    #ifdef DEBUG
    debug(LL_WARN, "resetServer: server is in INIT mode, abording");
    #endif
};

if ( (count _this) isEqualTo 0 ) exitWith {
    #ifdef DEBUG
    debug(LL_ERR, "resetServer: no mission parameters given, abording");
    #endif
};

private _hasChanged = false;
{
	private _curVal = [(configName _x)] call core_fnc_getParam;
	if !( _curVal isEqualTo (_this select _forEachIndex) ) then {
        #ifdef DEBUG
		private _debug = format["resetServer: parameter %1 has changed, was %2 is %3", (configName _x), _curVal, (_this select _forEachIndex)];
		debug(LL_DEBUG, _debug);
		#endif 
    	_hasChanged = true;
         
	};
} forEach ("true" configClasses (missionConfigFile >> "params") );

if !( _haschanged ) exitWith {
    #ifdef DEBUG
    debug(LL_WARN, "resetServer: no mission parameter has changed, abording");
    #endif
};

missionNamespace setVariable ["SERVER_INIT", true, true];

serverWaitFor = [];

"clientResetFinish" addPublicVariableEventHandler {
	serverWaitFor = serverWaitFor - [_this select 1];   
};

private _headless = (entities "HeadlessClient_F");
{ 
	serverWaitFor pushback (name _x);
} forEach _headless;

private _players = (allPlayers - _headless);
{ 
	serverWaitFor pushback (getPlayerUID _x);
} forEach _players;


private _leaveEH = addMissionEventHandler ["HandleDisconnect", {
    serverWaitFor = serverWaitFor - [_this select 2];
}];

#ifdef DEBUG
private _debug = format["resetServer: resetting %1 client(s)", (count serverWaitFor)];
debug(LL_DEBUG, _debug);
#endif

remoteExec ["core_fnc_resetClient", 0, false];

waitUntil {
	sleep 2;
    #ifdef DEBUG
	private _debug = format["resetServer: server is waiting for %1 client(s)", (count serverWaitFor)];
	debug(LL_DEBUG, _debug);
	#endif
	( (count serverWaitFor) == 0 )
};

removeMissionEventHandler ["HandleDisconnect", _leaveEH];

#ifdef DEBUG
debug(LL_DEBUG, "resetServer: resetting server");
#endif

["SERVER", "destroy"] call core_fnc_featEvent;

#ifdef DEBUG
debug(LL_DEBUG, "resetServer: updating mission parameters");
#endif
{
	[(configName _x), (_this select _forEachIndex)] call core_fnc_setParam;
} forEach ("true" configClasses (missionConfigFile >> "params") );

#ifdef DEBUG
debug(LL_DEBUG, "resetServer: re-init server");
#endif

call core_fnc_initServer;

#ifdef DEBUG
debug(LL_DEBUG, "resetServer: re-init clients");
#endif

remoteExec ["core_fnc_initClient", 0, false];