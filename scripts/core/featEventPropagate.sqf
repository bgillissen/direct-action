/*
@filename: core\featEventPropagate.sqf
Author:
	Ben
Description:
	all context.
	propagate an event to other players / server / headless clients.
*/

#include "_debug.hpp"

params ["_ctxt", "_when", ["_arg", []], ["_self", true]];

if !( [_ctxt, _when] call core_fnc_featEventUsed ) exitWith {
    #ifdef DEBUG
    private _debug = format["featEventPropagate: %1 %2 is not used, abording", _ctxt, _when];
	debug(LL_INFO, _debug);
	#endif
};

private "_target";
switch ( _ctxt ) do {
    case "SERVER" : { _target = 2; };
	case "PLAYER" : { _target = (allPlayers - entities "HeadlessClient_F");
    				  if !( _self ) then { _target = _target - [player]; }; 
					};
	case "HEADLESS" : { _target = (entities "HeadlessClient_F"); };
};

if ( isNil "_target" ) exitWith {
    #ifdef DEBUG
    private _debug = format["featEventPropagate: %1 %2, target isNil !", _ctxt, _when];
	debug(LL_ERR, _debug);
	#endif
};

private _do = true;

if ( _target isEqualType [] ) then {
    {
        if ( isNil "_x" ) then { _target deleteAt _forEachIndex; };
        if ( (_when isEqualTo "leave") && (_x isEqualTo (_arg select 0)) ) then {
            _target deleteAt _forEachIndex;
        };
    } forEach _target;
	_do = ( (count _target) > 0 ); 
};

#ifdef DEBUG
private _msg = format["featEventPropagate: %1 %2, targets: %3", _ctxt, _when, _target];
debug(LL_DEBUG, _msg);
#endif

if !( _do ) exitWith {};

[_ctxt, _when, _arg] remoteExec ["core_fnc_featEventRemote", _target, false];