/*
@filename: core\initPlayer.sqf
Author:
	Ben
Description:
	this script is executed by main init on player side only,
	it init the player side features
*/

#include "_debug.hpp"

missionNamespace setVariable ["PLAYER_INGAME",false, false];
missionNamespace setVariable ["PLAYER_INIT", true, false];
missionNamespace setVariable ["KB_stack", [], false];

["PLAYER", "preInit"] call core_fnc_featEvent;

waitUntil {
	((missionNamespace getVariable "SERVER_INIT") isEqualTo false)
};

waitUntil {!isNull player};
waitUntil {player isEqualTo player};

if ( isNil "FEH_serverJoin" ) then {
    if ( ["SERVER", "join"] call core_fnc_featEventUsed ) then {
		["SERVER", "join", [player]] call core_fnc_featEvent;
		FEH_serverJoin = true;
	#ifdef DEBUG
	} else {
	    debug(LL_DEBUG, "featEvent: skipping SERVER join, not in use");
	#endif
	};
};

["PLAYER", "init"] call core_fnc_featEvent;

if ( isNil "FEH_closeVA" ) then {
    if ( ["PLAYER", "closeVA"] call core_fnc_featEventUsed ) then {
		FEH_closeVA = [missionNamespace, "arsenalClosed", {["PLAYER", "closeVA"] call core_fnc_featEvent;}] call BIS_fnc_addScriptedEventHandler
	#ifdef DEBUG
	} else {
	    debug(LL_DEBUG, "featEvent: skipping PLAYER closeVA, not in use");
	#endif
	};
};

if ( isNil "FEH_getIn" ) then {
    if ( ["PLAYER", "getIn"] call core_fnc_featEventUsed ) then {
		FEH_getIn = player addEventHandler ["GetInMan", {["PLAYER", "getIn", _this] call core_fnc_featEvent;}];
	#ifdef DEBUG
	} else {
	    debug(LL_DEBUG, "featEvent: skipping PLAYER getIn, not in use");
	#endif
	};
};

if ( isNil "FEH_switchSeat" ) then {
    if ( ["PLAYER", "switchSeat"] call core_fnc_featEventUsed ) then {	        
		FEH_switchSeat = player addEventHandler ["SeatSwitchedMan", {["PLAYER", "switchSeat", _this] call core_fnc_featEvent;}];
	#ifdef DEBUG
	} else {
	    debug(LL_DEBUG, "featEvent: skipping PLAYER switchSeat, not in use");
	#endif
	};
};

if ( isNil "FEH_getOut" ) then {
    if ( ["PLAYER", "getOut"] call core_fnc_featEventUsed ) then {
    	FEH_getOut = player addEventHandler ["GetOutman", {["PLAYER", "getOut", _this] call core_fnc_featEvent;}];
	#ifdef DEBUG
	} else {
	    debug(LL_DEBUG, "featEvent: skipping PLAYER getOut, not in use");
	#endif
	};
};

if ( isNil "FEH_take" ) then {
    if ( ["PLAYER", "take"] call core_fnc_featEventUsed ) then {
		FEH_take = player addEventHandler ["Take", {["PLAYER", "take", _this] call core_fnc_featEvent;}];
	#ifdef DEBUG
	} else {
	    debug(LL_DEBUG, "featEvent: skipping PLAYER take, not in use");
	#endif
	};
};

if ( isNil "FEH_shoot" ) then {
    if ( ["PLAYER", "shoot"] call core_fnc_featEventUsed ) then {
		FEH_shoot = player addEventHandler ["FiredMan", {["PLAYER", "shoot", _this] call core_fnc_featEvent;}];
	#ifdef DEBUG
	} else {
	    debug(LL_DEBUG, "featEvent: skipping PLAYER shoot, not in use");
	#endif
	};
};

if ( isNil "FEH_assemble" ) then {
    if ( ["PLAYER", "assemble"] call core_fnc_featEventUsed ) then {
		FEH_assemble = player addEventHandler ["WeaponAssembled", {["PLAYER", "assemble", _this] call core_fnc_featEvent;}];
	#ifdef DEBUG
	} else {
    	debug(LL_DEBUG, "featEvent: skipping PLAYER assemble, not in use");
	#endif
	};
};

if ( isNil "FEH_inventory" ) then {
    if ( ["PLAYER", "inventory"] call core_fnc_featEventUsed ) then {
		FEH_inventory = player addEventHandler ["InventoryOpened", {["PLAYER", "inventory", _this] call core_fnc_featEvent;}];
	#ifdef DEBUG
	} else {
    	debug(LL_DEBUG, "featEvent: skipping PLAYER inventory, not in use");
	#endif
	};
};

if ( isNil "FEH_killed" ) then {
    if ( ["PLAYER", "killed"] call core_fnc_featEventUsed ) then {
	    PLAYER_UID = getPlayerUID player;
		FEH_killed = player addEventHandler ["Killed", {
				["PLAYER", "killed", _this] call core_fnc_featEvent;
				["SERVER", "killed", [_this, PLAYER_UID]] call core_fnc_featEvent; }];
	#ifdef DEBUG
	} else {
	    debug(LL_DEBUG, "featEvent: skipping PLAYER killed, not in use");
	#endif            
	};
};


if ( isNil "FEH_remoteControl" ) then {
    FEH_rcSERVER = ["PLAYER", "remoteControl"] call core_fnc_featEventUsed;
	FEH_rcPLAYER = ["SERVER", "remoteControl"] call core_fnc_featEventUsed;
	if ( FEH_rcSERVER || FEH_rcPLAYER ) then {
	    {
	        [_x, "curatorObjectRemoteControlled", {
				if ( FEH_rcPLAYER ) then { ["PLAYER", "remoteControl", _this] call core_fnc_featEvent; };
	            if ( FEH_rcSERVER ) then { ["SERVER", "remoteControl", _this] call core_fnc_featEvent; };
	        }] call BIS_fnc_addScriptedEventHandler;
		} forEach (entities "ModuleCurator_F");
	    FEH_remoteControl = true;
	#ifdef DEBUG
	} else {
    	debug(LL_DEBUG, "featEvent: skipping PLAYER remoteControl, not in use");
	#endif
	};
};

if ( isNil "FEH_zeusInterface" ) then {
	if ( ["PLAYER", "zeusInterface"] call core_fnc_featEventUsed) then {
	    inZeus = false;
		FEH_zeusInterface = addMissionEventHandler ["EachFrame",{
			if ( inZeus && (isNull curatorCamera) ) then {
	            inZeus = false;
	            ["PLAYER", "zeusInterface", [false]] spawn core_fnc_featEvent;
	            
	        };
	        if ( !inZeus && !(isNull curatorCamera) ) then {
	            inZeus = true;
	            ["PLAYER", "zeusInterface", [true]] spawn core_fnc_featEvent;
	            if !( MOD_cba3 ) then {
	            	waitUntil { !isnull (findDisplay 312) };
					(findDisplay 312) displayAddEventHandler ["KeyDown", "[true, _this] call keybind_fnc_event;"];
	        		(findDisplay 312) displayAddEventHandler ["KeyUp", "[false, _this] call keybind_fnc_event;"];
				};
	        };
		}];
	#ifdef DEBUG
	} else {
    	debug(LL_DEBUG, "featEvent: skipping PLAYER zeusInterface, not in use");
	#endif
	};
};

["PLAYER", "postInit"] call core_fnc_featEvent;

if ( isNil "FEH_keys" && !MOD_cba3 ) then {
	{
		waitUntil { !isNull (findDisplay _x) };
		(findDisplay _x) displayAddEventHandler ["KeyDown", "[true, _this] call keybind_fnc_event;"];
        (findDisplay _x) displayAddEventHandler ["KeyUp", "[false, _this] call keybind_fnc_event;"];
	} forEach [46, 12];
    FEH_keys = true; 
};

if ( isNil "FEH_playerJoin" ) then {
    if ( ["PLAYER", "join"] call core_fnc_featEventUsed )  then {
		["PLAYER", "join", [player], false] call core_fnc_featEventPropagate;
		FEH_playerJoin = true;
	#ifdef DEBUG
	} else {
    	debug(LL_DEBUG, "featEvent: skipping PLAYER join, not in use");
	#endif
	};
};

if ( isNil "FEH_headlessJoin" ) then {
    if ( ["HEADLESS", "join"] call core_fnc_featEventUsed )  then {
		["HEADLESS", "join", [player], false] call core_fnc_featEventPropagate;
		FEH_headlessJoin = true;
	#ifdef DEBUG
	} else {
    	debug(LL_DEBUG, "featEvent: skipping HEADLESS join, not in use");
	#endif
	};
};

missionNamespace setVariable ["PLAYER_INIT", false, false];