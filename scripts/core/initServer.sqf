/*
@filename: core\initServer.sqf
Author:
	Ben
Description:
	run on server
*/

#include "_debug.hpp"

missionNamespace setVariable ["SERVER_INIT", true, true];

if ( isNil "PARAMETERS" ) then { PARAMETERS = []; };
publicVariable "PARAMETERS";

["SERVER", "preInit"] call core_fnc_featEvent;

["SERVER", "init"] call core_fnc_featEvent;

//features leave eventHandler
FEH_leaveSERVER = ["SERVER", "leave"] call core_fnc_featEventUsed;
FEH_leavePLAYER = ["PLAYER", "leave"] call core_fnc_featEventUsed;
FEH_leaveHEADLESS = ["HEADLESS", "leave"] call core_fnc_featEventUsed;
private _used = ( FEH_leaveSERVER || FEH_leavePLAYER || FEH_leaveHEADLESS );
if ( isNil "FEH_leave" && _used ) then {
	FEH_leave = addMissionEventHandler ["HandleDisconnect", {
        if ( FEH_leaveSERVER ) then { ["SERVER", "leave", _this] call core_fnc_featEvent; };
		if ( FEH_leavePLAYER ) then { ["PLAYER", "leave", _this] call core_fnc_featEvent; };
		if ( FEH_leaveHEADLESS ) then { ["HEADLESS", "leave", _this] call core_fnc_featEvent; };
	}];
#ifdef DEBUG
} else {
    debug(LL_DEBUG, "featEvent: skipping SERVER leave, not in use");
#endif
};

["SERVER", "postInit"] call core_fnc_featEvent;

missionNamespace setVariable ["SERVER_INIT", false, true];