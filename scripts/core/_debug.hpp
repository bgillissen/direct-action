#include "debugLevels.hpp"

//comment the following line to disable the core functions debug
//#define DEBUG()

//core functions debug level
#define DEBUG_LVL LL_DEBUG
//debug context
#define DEBUG_CTXT "core"

#include "__debug.hpp"
