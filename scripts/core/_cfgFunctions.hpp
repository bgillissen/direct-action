class core {
	tag = "core";
	class functions {
		class context {
			file="core\context.sqf";
			preInit = 1;
		};
		class debug { file="core\debug.sqf"; };
		class featEvent { file="core\featEvent.sqf"; };
		class featEventPropagate { file="core\featEventPropagate.sqf"; };
		class featEventRemote { file="core\featEventRemote.sqf"; };
		class featEventUsed { file="core\featEventUsed.sqf"; };
		class getParam { file="core\getParam.sqf"; };
		class setParam { file="core\setParam.sqf"; };
		class getSetting { file="core\getSetting.sqf"; };
		class initClient { file="core\initClient.sqf"; };
		class initHeadless { file="core\initHeadless.sqf"; };
		class initPlayer { file="core\initPlayer.sqf"; };
		class initServer { file="core\initServer.sqf"; };
		class resetClient { file="core\resetClient.sqf"; };
		class resetHeadless { file="core\resetHeadless.sqf"; };
		class resetPlayer { file="core\resetPlayer.sqf"; };
		class resetServer { file="core\resetServer.sqf"; };
	};
};
