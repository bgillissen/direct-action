/*
@filename: core\FeatEventRemote.sqf
Author:
	Ben
Description:
	this run on all context
	used as front door for feature events triggered remotly
*/

#include "_debug.hpp"

params ["_ctxt", "_when", "_param"];


if ( _ctxt isEqualTo "SERVER" ) exitWith {
	if !( CTXT_SERVER ) exitWith {
		#ifdef DEBUG
		private _msg = format["featEventRemote: wrong context received, expected SERVER got %1, abording", _ctxt];
		debug(LL_WARN, _msg);
		#endif        
    };
    waitUntil { 
    	sleep 1;
        !SERVER_INIT 
	};
	private _allowed = ["respawn", "join", "killed", "remoteControl", "group"]; 
	if !( _when in _allowed ) exitWith {
		#ifdef DEBUG
		private _msg = format["featEventRemote: '%1' is not allowed to remotely triggered on server context, abording", _when];
		debug(LL_WARN, _msg);
		#endif	
	};
    ["SERVER", _when, _param] call core_fnc_featEvent;
};

if ( _ctxt isEqualTo "PLAYER" ) exitWith {
	if !( CTXT_PLAYER ) exitWith {
		#ifdef DEBUG
		private _msg = format["featEventRemote: wrong context received, expected PLAYER got %1, abording", _ctxt];
		debug(LL_WARN, _msg);
		#endif        
    };
    waitUntil {
        sleep 1; 
    	!PLAYER_INIT 
	};
	private _allowed = ["leave", "join", 'group']; 
	if !( _when in _allowed ) exitWith {
        #ifdef DEBUG
		private _msg = format["featEventRemote: '%1' is not allowed to remotely triggered on player context, abording", _when];
		debug(LL_WARN, _msg);
		#endif	
	};
    ["PLAYER", _when, _param] call core_fnc_featEvent;
};

if ( _ctxt isEqualTo "HEADLESS" ) exitWith {
	if !( CTXT_HEADLESS ) exitWith {
		#ifdef DEBUG
		private _msg = format["featEventRemote: wrong context received, expected HEADLESS got %1, abording", _ctxt];
		debug(LL_WARN, _msg);
		#endif        
    };
    waitUntil {
        sleep 1; 
    	!HEADLESS_INIT 
	};
	private _allowed = ["leave", "join"]; 
	if ( _when in _allowed ) exitWith {
        #ifdef DEBUG
		private _msg = format["featEventRemote: '%1' is not allowed to remotely triggered on headless context, abording", _when];
		debug(LL_WARN, _msg);
		#endif
	};
    ["HEADLESS", _when, _param] call core_fnc_featEvent;
};