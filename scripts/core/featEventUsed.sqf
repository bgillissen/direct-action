/*
@filename: core\FeatEventUsed.sqf
Author:
	Ben
Description:
	this run on all context
	check when ever a event is used by a feature
*/

#include "_debug.hpp"

params ["_ctxt", "_when"];

( ({ getNumber(_x >> _ctxt >> _when >> "enable") == 1 } count ("true" configClasses (missionConfigFile >> "features"))) > 0 );