/*
@filename: core\context.sqf
Author:
	Ben
Description:
	run on all context,
	first function called, define the context variable
*/

CTXT_SERVER = isServer;
CTXT_HEADLESS = (!isDedicated && !hasInterface);
CTXT_PLAYER = hasInterface;