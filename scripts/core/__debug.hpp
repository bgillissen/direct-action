
//comment this line to completly disable debug
#define DEBUG_OVERALL

//default debug level
#ifndef DEBUG_LVL
#define DEBUG_LVL LL_INFO
#endif

//debug context
#ifndef DEBUG_CTXT
#define DEBUG_CTXT "?"
#endif


#ifndef DEBUG_OVERALL
#undef DEBUG
#endif

#ifdef DEBUG
#define debug(lvl, msg) [DEBUG_LVL, lvl, DEBUG_CTXT, msg] call core_fnc_debug;
#else
#define debug(lvl, msg)
#endif
