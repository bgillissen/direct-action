/*
@filename: core\setParam.sqf
Author:
	Ben
Description:
	run on server
	add the given parameters to the stack of overwrited parameters 
*/
params ["_param", "_value"];

private _found = false;

{
	_x params ["_name"];
	if ( _name isEqualTo _param ) exitWith {
        _found = true; 
		PARAMETERS set [_forEachIndex, _this]; 
	};
} forEach PARAMETERS;

if !( _found ) then { PARAMETERS pushback _this; };