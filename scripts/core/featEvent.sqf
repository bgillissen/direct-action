/*
@filename: core\featEvent.sqf
Author:
	Ben
Description:
	This script is executed on every context.
	propagate events to features, call or spawn required script.
*/

#include "_debug.hpp"

params ["_ctxt", "_when", ["_arg", []]];

if ( isNil "FEAT_THREADS" ) then { FEAT_THREADS = []; };

#ifdef DEBUG
private _debug = format["featEvent: %1, %2 --- %3", _ctxt, _when, _arg];
debug(LL_DEBUG, _debug);
#endif

private _remote = call {
	if ( (_ctxt isEqualTo "SERVER") && (!CTXT_SERVER) ) exitWith { true };
	if ( (_ctxt isEqualTo "PLAYER") && (!CTXT_PLAYER) ) exitWith { true };
	if ( (_ctxt isEqualTo "HEADLESS") && (!CTXT_HEADLESS) ) exitWith { true };
	false
};

if ( _remote ) exitWith { _this call core_fnc_featEventPropagate; };

//not defined yet, we initialize it
if ( isNil "FEAT_THREADS" ) then { FEAT_THREADS = []; };

private _poolName = format["FEAT_%1_%2", _ctxt, _when];

private _pool = missionNamespace getVariable _poolName;

if ( isNil "_pool" ) then {
	_pool = [];
	private _buff = [];
	{
		if ( getNumber(_x >> _ctxt >> _when >> "enable") == 1 ) then {
			_buff pushback [(configName _x), 
			                getNumber(_x >> _ctxt >> _when >> 'thread'), 
			                getNumber(_x >> _ctxt >> _when >> 'order')];
		};
	} forEach ("true" configClasses (missionConfigFile >> "features"));

	if ( count _buff > 0 ) then {
		_buff = [_buff, [], {_x select 2}, "ASCEND"] call BIS_fnc_sortBy;
		{
			_x params ["_feat", "_thread"];
			private _fnc = format["%1_fnc_%2%3", _feat, _ctxt, _when];
			if ( isNil _fnc ) then {
				#ifdef DEBUG
				private _debug = format["featEvent: function %1 is not declared", _fnc];
				debug(LL_WARN, _debug);
				#endif				
			} else {
				private _code = _thread call {
					if ( _this isEqualTo 1 ) exitWith { compile format["_this spawn %1", _fnc] };
					compile format["_this call %1", _fnc]
				};
				_pool pushback [_feat, _forEachIndex, _thread, _code];
			};
		} forEach _buff;
		_buff = nil;
	};
	missionNamespace setVariable [_poolName, _pool, false];
};

private '_out';
private _StopOn = call {
    if ( (_ctxt isEqualTo "PLAYER") && (_when isEqualTo "inventory") ) exitWith { true };
    nil
};
private _canStop = !(isNil "_stopOn");
private _stop = false;
{
	_x params ["_feat", "_id", "_thread", "_code"];
	#ifdef DEBUG
	private _debug = format["featEvent: %1, %2 => %3", _ctxt, _when, _feat];
	debug(LL_DEBUG, _debug);
	#endif
	try {
		if ( _when isEqualTo "destroy" ) then {
			{
				_x params ["_tFeat", "_tCTXT", "_tWhen", "_tThread"];
				if ( (_feat isEqualTo _tFeat) && (_ctxt isEqualTo _tCTXT) ) then {
                    #ifdef DEBUG
					private _debug = format["featEvent: %1, destroying %2 %3 thread", _ctxt, _feat, _tWhen];
					debug(LL_DEBUG, _debug);
					#endif 
					[_tWhen, _tThread] call _code;
					FEAT_THREADS deleteAt _forEachIndex; 
				};
			} forEach FEAT_THREADS;
			[] call _code;
		} else {
			if ( _thread isEqualTo 1 ) then { 
				FEAT_THREADS pushback [_feat, _ctxt, _when, (_arg call _code)];
			} else {
				if (_canStop ) then {
					private _ret = (_arg call _code);
                	if !( isNil '_ret' ) then {
                        #ifdef DEBUG
						private _debug = format["featEvent: %1, %2 => %3 = %4", _ctxt, _when, _feat, _ret];
						debug(LL_DEBUG, _debug);
						#endif 
                    	if ( _ret isEqualTo _stopOn ) then {
                        	_stop = _ret; 
                    		_out = _ret; 
						}; 
					};
				} else {
                    (_arg call _code);
                };
			};
		};
	} catch {
		#ifdef DEBUG
		private _debug = format["featEvent: exception caugh for %1, %2 => %3, message: %4", _ctxt, _when, _feat, _exception];
		debug(LL_ERR, _debug);
		#endif		
	};
    if ( _stop ) exitWith {
        #ifdef DEBUG
		private _debug = format["featEvent: propagation for %1, %2 has been stopped by %3", _ctxt, _when, _feat];
		debug(LL_DEBUG, _debug);
		#endif		
    };
} forEach _pool;

if !( isNil '_out' ) exitWith {
    #ifdef DEBUG
	private _debug = format["featEvent: return value for %1, %2 : %3", _ctxt, _when, _out];
	debug(LL_DEBUG, _debug);
	#endif 
	_out 
};

nil