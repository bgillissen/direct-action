//Uncomment the define for the debug method you want to use and comment the other one.
//note that DEBUG_CONSOLE require the debug_console extension by KillZoneKid
//#define DEBUG_CONSOLE
#define DEBUG_RPT

#define conWhite(_msg)
#define conRed(_msg)
#define conGreen(_msg)
#define conBlue(_msg)
#define conYellow(_msg)
#define conPurple(_msg)
#define conCyan(_msg)

#ifdef DEBUG_CONSOLE
#define conWhite(_msg) "debug_console" callExtension (_msg + "#1110")
#define conRed(_msg) "debug_console" callExtension (_msg + "#1000")
#define conGreen(_msg) "debug_console" callExtension (_msg + "#0100")
#define conBlue(_msg) "debug_console" callExtension (_msg + "#0010")
#define conYellow(_msg) "debug_console" callExtension (_msg + "#1100")
#define conPurple(_msg) "debug_console" callExtension (_msg + "#1010")
#define conCyan(_msg) "debug_console" callExtension (_msg + "#0110")
#endif

#ifdef DEBUG_RPT
#define conWhite(_msg) diag_log(_msg)
#define conRed(_msg) diag_log(_msg)
#define conGreen(_msg) diag_log(_msg)
#define conBlue(_msg) diag_log(_msg)
#define conYellow(_msg) diag_log(_msg)
#define conPurple(_msg) diag_log(_msg)
#define conCyan(_msg) diag_log(_msg)
#endif
