/*
@filename: core\getParam.sqf
Author:
	Ben
Description:
	run on all context
	retrieve a mission parameter value either from missionConfigFile if it has not been overwrited,
	or from a stack of overwrited parameters
*/

params ["_conf"];

if ( isNil "PARAMETERS" ) then {
    //do not know why, but the first call sometimes return 0
	private _nul = [_conf] call BIS_fnc_getParamValue; 
	PARAMETERS = []; 
};

private "_value";
{
	_x params ["_name", "_val"];
	if ( (_name isEqualTo _conf) && !isNil "_val" ) exitWith { _value = _val; };
    true
} count PARAMETERS;

if ( isNil "_value" ) exitWith {
	([_conf] call BIS_fnc_getParamValue)
};

_value