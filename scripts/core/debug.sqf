
#include "debugLevels.hpp"
#include "debugMacros.hpp"

params["_minlvl", "_lvl", "_ctxt", "_msg"];

if ( isNil "_lvl" ) exitWith {
    _msg = format["No level given for message: %1", _msg];
    [LL_ERR, LL_ERR, "debug", _msg] call core_fnc_debug;
};

if ( _lvl > _minlvl ) exitWith {};

private _txtLvl = "?";
switch ( _lvl ) do {
    case LL_ERR : { _txtLvl = "ERROR"; };
    case LL_WARN : { _txtLvl = "WARNING"; };
    case LL_INFO : { _txtLvl = "INFO"; };
    case LL_DEBUG : { _txtLvl = "DEBUG"; };
};

_msg = format["directAction | %1 | %2 | %3", _txtLvl, _ctxt, _msg];

if ( _lvl isEqualTo LL_ERR ) exitWith { conRed(_msg); };
if ( _lvl isEqualTo LL_WARN ) exitWith { conYellow(_msg); };
if ( _lvl isEqualTo LL_INFO ) exitWith { conBlue(_msg); };
if ( _lvl isEqualTo LL_DEBUG ) exitWith { conWhite(_msg); };