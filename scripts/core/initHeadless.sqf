/*
@filename: core\initHeadless.sqf
Author:
	Ben
Description:
	this script is executed once by main init on headless side only,
	it init the headless side features
*/

missionNamespace setVariable ["HEADLESS_INIT", true, false];

["HEADLESS", "preInit"] call core_fnc_featEvent;

waitUntil {
	sleep 1;
	((missionNamespace getVariable "SERVER_INIT") isEqualTo false)
};

waitUntil {!isNull player};
waitUntil {player == player};

["HEADLESS", "init"] call core_fnc_featEvent;

["HEADLESS", "postInit"] call core_fnc_featEvent;

if ( isNil "FEH_playerJoin" ) then {
    if ( ["PLAYER", "join"] call core_fnc_featEventUsed )  then {
		["PLAYER", "join", [player], false] call core_fnc_featEventPropagate;
		FEH_playerJoin = true;
	#ifdef DEBUG
	} else {
    	debug(LL_DEBUG, "featEvent: skipping PLAYER join, not in use");
	#endif
	};
};

if ( isNil "FEH_headlessJoin" ) then {
    if ( ["HEADLESS", "join"] call core_fnc_featEventUsed )  then {
		["HEADLESS", "join", [player], false] call core_fnc_featEventPropagate;
		FEH_headlessJoin = true;
	#ifdef DEBUG
	} else {
    	debug(LL_DEBUG, "featEvent: skipping HEADLESS join, not in use");
	#endif
	};
};

missionNamespace setVariable ["HEADLESS_INIT", false, false];