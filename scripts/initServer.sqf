/*
@filename: initServer .sqf
Author:
	Ben
Description:
	run on server by game engine on mission start
	call server init functions
*/

if !( isMultiplayer ) exitWith {};

call core_fnc_initServer;

nil