class uavFix {
	tag = "uavFix";
	class functions {
		class playerKilled { file="feats\uavFix\playerKilled.sqf"; };
		class playerRespawn { file="feats\uavFix\playerRespawn.sqf"; };
		class serverLeave { file="feats\uavFix\serverLeave.sqf"; };
	};
};
