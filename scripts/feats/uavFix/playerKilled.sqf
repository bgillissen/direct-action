/*
@filename: feats\uavFix\playerKilled.sqf
Author:
	Ben
Description:
	run on player,
	store to which UAV player was connected to
*/

#include "_debug.hpp"

uavFIX = getConnectedUAV player;

if ( isNull uavFix ) exitWith { nil };

private _term = switch ( PLAYER_SIDE ) do {
    case west : { "B_UavTerminal" };
    case east : { "O_UavTerminal" };
    case independent : { "I_UavTerminal" };
};

player unassignItem _term; 
player removeItem _term;

nil