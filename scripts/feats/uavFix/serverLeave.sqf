/*
@filename: feats\uavFix\serverLeave.sqf
Author:
	Ben
Description:
	run on server,
	disconnect player from uav
*/

#include "_debug.hpp"

params["_unit", "_id", "_uid", "_name"];

{
    private _veh = _x;
    if ( isUAVConnected _veh ) then { 
		private _ctrl = (UAVControl _veh) select 0;
    	if ( _ctrl isEqualTo objNull ) then { deleteVehicle _veh };
	};
} forEach allUnitsUAV;