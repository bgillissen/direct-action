class uavFix : feat_base  {
	class server : featServer_base {
		class leave : featEvent_enable {};
	};
	class player : featPlayer_base {
		class killed : featEvent_enable {};
		class respawn : featEvent_enable {};
	};
};
