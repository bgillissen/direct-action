/*
@filename: feats\uavFix\playerRespawn.sqf
Author:
	Ben
Description:
	run on player,
	reconnect player to the uav he was connected to before he died.
*/

if ( isNil "uavFix" ) exitWith { nil };

if ( isNull uavFix ) exitWith { nil };

player connectTerminalToUAV uavFix; 

nil