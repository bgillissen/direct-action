/*
@filename: feats\gearSave\load.sqf
Author:
	Ben
Description:
	run on player,
	restore previously saved gear
*/

#include "_debug.hpp"

params [['_msg', false], ['_varName', 'gearSave']];

if !( [_varName] call gearSave_fnc_isSaved ) exitWith {
    #ifdef DEBUG
    private _debug = format["could not load loadout '%1', was not saved!", _varName];
    debug(LL_ERR, _debug);
    #endif
};

[player, [missionNamespace, _varName]] call BIS_fnc_loadInventory;

[] spawn {
    sleep 3;
	[player, ((group player) getVariable['insignia', ''])] call global_fnc_setUnitInsignia;
};

if ( _msg ) then { systemChat (["gearSave", "loadMsg"] call core_fnc_getSetting); };