class gearSave {
	saveAction = "<t color='#FAAF3A'>Quick save gear</t>";
	loadAction = "<t color='#FAAF3A'>Quick load gear</t>";
	dftAction = "<t color='#FAAF3A'>Quick load default gear</t>";
	saveMsg = "Your current gear has been saved, it will be restored on next respawn.";
	loadMsg = "Saved gear has been restored";
	refusedMsg = "Gear not saved due to restricted gear being used";
};
