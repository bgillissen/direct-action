/*
@filename: feats\gearSave\save.sqf
Author:
	Ben
Description:
	run on player,
	save current player loadout
*/

params [['_msg', false], ['_varName', 'gearSave']];

if !( call va_fnc_checkGears ) exitWith {
	if ( _msg ) then { systemChat (["gearSave", "refusedMsg"] call core_fnc_getSetting); };	
};

[player, [missionNamespace, _varName]] call BIS_fnc_saveInventory;

if ( _msg ) then { systemChat (["gearSave", "saveMsg"] call core_fnc_getSetting); };