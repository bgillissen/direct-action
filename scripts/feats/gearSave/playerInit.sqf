/*
@filename: feats\gearSave\playerInit.sqf
Author:
	Ben
Description:
	run on player,
	add saveGear action to base things with a arsenal action
*/

#include "_debug.hpp"

private _save = ["gearSave", "saveAction"] call core_fnc_getSetting;
private _load = ["gearSave", "loadAction"] call core_fnc_getSetting;
private _dft = ["gearSave", "dftAction"] call core_fnc_getSetting;

{
	{
		_x params ["_thing", "_actions"];
		{
			if ( (configName _x) isEqualTo "saveGear" ) then {
                #ifdef DEBUG
                private _debug = format["adding save action to %1", _thing];
                debug(LL_DEBUG, _debug);
                #endif
				_thing addAction [_save, {[true] call gearSave_fnc_save}, [], 0, false, true, "", "true", 4];
			} else {
            	if ( (configName _x) isEqualTo "loadGear" ) then {
                	#ifdef DEBUG
                	private _debug = format["adding load action to %1", _thing];
                	debug(LL_DEBUG, _debug);
                	#endif
					_thing addAction [_load, {[true] call gearSave_fnc_load}, [], 0, false, true, "", "[] call gearSave_fnc_isSaved", 4];
					_thing addAction [_dft, {call playerSpawn_fnc_loadout;}, [], 0, false, true, "", "true", 4];
				};
			};
		} forEach _actions;
	} forEach _x;
} forEach [BA_npc, BA_obj, BA_veh];

nil