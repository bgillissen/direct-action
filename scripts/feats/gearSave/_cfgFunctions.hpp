class gearSave {
	tag = "gearSave";
	class functions {
		class isSaved { file="feats\gearSave\isSaved.sqf"; };
		class load { file="feats\gearSave\load.sqf"; };
		class save { file="feats\gearSave\save.sqf"; };
		class playerInit { file="feats\gearSave\playerInit.sqf"; };
		class playerRespawn { file="feats\gearSave\playerRespawn.sqf"; };
	};
};
