class billboards {
	tag = "billboards";
	class functions {
		class dateUpdate { file="feats\billboards\dateUpdate.sqf"; };
		class playerInit { file="feats\billboards\playerInit.sqf"; };
		class playerRespawn { file="feats\billboards\playerRespawn.sqf"; };
		class serverInit { file="feats\billboards\serverInit.sqf"; };
		class setTexture { file="feats\billboards\setTexture.sqf"; };
	};
};
