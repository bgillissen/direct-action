/*
@filename: feats\billboards\playerInit.sqf
Author:
	Ben
Description:
	run on server,
 	set texture to base objects with a Fixed texture according base config
*/

#include "_debug.hpp"

{
	_x params ["_thing", "_actions"];
	{
		if ( (configName _x) isEqualTo "billboard" ) then {
			if ( getText(_x >> "mode") isEqualTo "fixed" ) then {
                #ifdef DEBUG
    			private _debug = format["setting fixed texture %1 to %2", getText(_x >> "texture"), _thing];
    			debug(LL_DEBUG, _debug);
    			#endif
				[_thing, "fixed", getText(_x >> "texture"), true] call billboards_fnc_setTexture;
			};
		};
	} forEach _actions;
} forEach BA_obj;

nil