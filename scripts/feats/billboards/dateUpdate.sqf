
#include "_debug.hpp"

serverDate params [["_curYear", 0], ["_curMonth", 0], ["_curDay", 0]];

{
	_x params ["_thing", "_actions"];
	{
		if ( (configName _x) isEqualTo "dateBillboard" ) then {
			private _textures = getArray(_x >> "textures");
			{
				private _texture = _x;
                {
					if ( isClass(_x >> "billboards" >> "byDate" >> _texture) ) then {
        				private _start = getArray(_x >> "billboards" >> "byDate" >> _texture >> "start");
						private _end = getArray(_x >> "billboards" >> "byDate" >> _texture >> "end");
						if ( (_curDay >= (_start select 0)) && 
				 			(_curDay <= (_end select 0)) && 
				 			(_curMonth >= (_start select 1)) && 
				 			(_curMonth <= (_end select 1)) ) then {
                            private _path = format[getText(_x >> "billboards" >> "byDate" >> "path"), _texture];
                            #ifdef DEBUG
							private _debug = format["setting byDate texture %1 to %2", _texture, _thing];
							debug(LL_DEBUG, _debug);
							#endif
							[_thing, "byDate", _path] call billboards_fnc_setTexture;
						};	
    				};                        
				} forEach ("true" configClasses (configFile >> "cfgDirectAction")); 
			} forEach _textures;
		};
	} forEach _actions;
} forEach BA_obj;