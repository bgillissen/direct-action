/*
@filename: feats\dynBillboards\playerRespawn.sqf
Author:
	Ben
Description:
	run on player,
	changes the texture displayed on the billboards with a mode set to random.
*/

#include "_debug.hpp"

if ( (count DB_list) == 0 ) exitWith { 
	#ifdef DEBUG
	debug(LL_DEBUG, "no billboard found in random pool, abording");
	#endif
    nil 
};

{
	_x params ["_thing", "_actions"];
	{
		if ( (configName _x) isEqualTo "billboard" ) then {
			if ( getText(_x >> "mode") isEqualTo "random" ) then {
				if ( (count DB_queue) == 0 ) then { DB_queue = DB_list; };
				private _texture = selectRandom DB_queue;
				DB_queue = DB_queue - [_texture];
                #ifdef DEBUG
				private _debug = format["setting random texture %1 to %2, %3 left in pool", _texture, _thing, count(DB_queue)];
    			debug(LL_DEBUG, _debug);
    			#endif
				[_thing, "randoms", _texture] call billboards_fnc_setTexture;
			};
		};
	} forEach _actions;
} forEach BA_obj;

nil