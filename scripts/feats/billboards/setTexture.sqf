/*
@filename: feats\billboards\setTexture.sqf
Author:
	Ben
Description:
	called on player side.
	It changes the texture displayed on the billboards.
*/

params ["_obj", "_type", "_texture", ["_global", false]];

if ( _type isEqualTo "byDate" ) exitWith {
	_obj setObjectTexture [0, _texture];
};

private _path = "";
{
	private _cfg = _x >> "billboards" >> _type;
	private _data = getArray(_cfg >> "entries");
	if ( _texture in getArray(_cfg >> "entries") ) then {
		_path = getText(_cfg >> "path");
	};
} forEach ("true" configClasses (configFile >> "cfgDirectAction"));

if !( _path isEqualTo "" ) then {
    if ( _global ) then {
		_obj setObjectTextureGlobal [0, format[_path, _texture]];
	} else {
        _obj setObjectTexture [0, format[_path, _texture]];
    };
};