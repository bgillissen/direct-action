/*
@filename: feats\billboards\playerInit.sqf
Author:
	Ben
Description:
	run on player
 	set texture to base objects according base config
*/

#include "_debug.hpp"

if ( isNil "DB_list" ) then {
	DB_list = [];
	{
		DB_list append getArray(_x >> "billboards" >> "randoms" >> "entries");
	} forEach ("true" configClasses (configFile >> "cfgDirectAction"));
    #ifdef DEBUG
    private _debug = format["found %1 texture in random pool", count(DB_list)];
    debug(LL_DEBUG, _debug);
    #endif
    nil
};

if ( isNil "DB_queue" ) then { DB_queue = DB_list; };

private _needDate = false;

{
	_x params ["_thing", "_actions"];
	{
		if ( (configName _x) isEqualTo "dateBillboard" ) then { _needDate = true; };
	} forEach _actions;
} forEach BA_obj;

if !( _needDate ) exitWith { nil };

if ( isNil "bbByDatePVEH" ) then {
	bbByDatePVEH = ["serverDate", {call billboards_fnc_dateUpdate;}] call pveh_fnc_add; 
};

call common_fnc_getDate;

nil