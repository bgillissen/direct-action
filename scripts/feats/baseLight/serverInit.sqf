
/*
@filename: feats\baseLight\serverInit.sqf
Author:
	Ben
Description:
	run on server
	build an array with all light objects in base safezone
*/

#include "_debug.hpp"

if ( (["baseLight"] call core_fnc_getParam) isEqualTo 0 ) exitWith {
	#ifdef DEBUG
   	debug(LL_INFO, "disabled by mission parameter");
    #endif
    nil  
};

baseLight = true;
baseLightNext = 0;
publicVariable "baseLight";
publicVariable "baseLightNext";

private _lightTypes = (["baseLight", "lights"] call core_fnc_getSetting);
private _zones = 0;
baseLights = [];
{
	_x params ["_area", "_actions"];
    if ( "baseLight" in _actions ) then {
        ([_area] call common_fnc_getAreaPosRad) params ["_pos", "_rad"]; 
		private _lights = nearestObjects [_pos, _lightTypes, _rad];
        { 
        	if !( _x inArea _area ) then { _lights deleteAt _forEachIndex; };
        } forEach _lights;
        baseLights append _lights;
        _zones = _zones + 1;       
    };
} forEach BA_zones;

publicVariable "baseLights";

{ _x allowDamage false; } forEach baseLights;

#ifdef DEBUG
private _debug = format["found %1 light(s) into %2 zone(s)", (count baseLights), _zones];
debug(LL_INFO, _debug);
#endif

nil