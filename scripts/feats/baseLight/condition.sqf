/*
@filename: feats\baseLight\condition.sqf
Author:
	Ben
Description:
	run on player,
	check if the action is available to player
*/

params ["_state"];

if ( baseLight isEqualTo _state ) exitWith { false };

if ( serverTime < baseLightNext ) exitWith { false };

private _rank = (["baseLight", "rank"] call core_fnc_getSetting);
private _prank = (player getVariable ["MD_rank", 0]);

( _prank >= _rank )