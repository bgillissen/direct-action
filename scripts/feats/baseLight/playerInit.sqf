/*
@filename: feats\baseLight\playerInit.sqf
Author:
	Ben
Description:
	run on player
	add action the things with baselight action to allow players to switch base lights on or off
*/

#include "_debug.hpp"

if ( (["baseLight"] call core_fnc_getParam) isEqualTo 0 ) exitWith {
	#ifdef DEBUG
   	debug(LL_INFO, "disabled by mission parameter");
    #endif
    nil  
};

private _on = (["baseLight", "on"] call core_fnc_getSetting);
private _off = (["baseLight", "off"] call core_fnc_getSetting);

{
	{
		_x params ["_thing", "_actions"];
		{
			if ( (configName _x) isEqualTo "baseLight" ) then {
                #ifdef DEBUG
                private _debug = format["adding actions to %1", _thing];
                debug(LL_DEBUG, _debug);
                #endif
				_thing addAction [_on, {call baseLight_fnc_switchLight}, true, 0, false, true, "", "[true] call baseLight_fnc_condition", 4];
				_thing addAction [_off, {call baseLight_fnc_switchLight}, false, 0, false, true, "", "[false] call baseLight_fnc_condition", 4];
			};
		} forEach _actions;
	} forEach _x;
} forEach [BA_npc, BA_obj, BA_veh];


if ( baseLight ) exitWith { nil };

[objNull, objNull, "", baseLight] call baseLight_fnc_switchLightPlayer;

nil