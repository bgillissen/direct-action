class baseLight {
	rank = 3;
	cooldown = 10;
	on = "Switch on base lights";
	off = "Switch off base lights";
	message = "%1 has switched %2 base lights, %3s cooldown.";
	lights[] = {"Lamps_base_F", "PowerLines_base_F", "PowerLines_Small_base_F"};
};
