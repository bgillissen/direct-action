/*
@filename: feats\baseLight\switchLight.sqf
Author:
	Ben
Description:
	run on player or server,
	if on player, ask server to propagate the switch action to all players 
	if on server, propagate the switch action to everyone, change value of baseLight PV and broadcast it
*/

if ( !isServer ) exitWith {
	_this remoteExec ["baseLight_fnc_switchLight", 2];
};

_this remoteExec ["baseLight_fnc_switchLightPlayer", 0, false];

params ["_thing", "_caller", "_id", "_arg"];

private _msg = (["baseLight", "message"] call core_fnc_getSetting);
private _cooldown = (["baseLight", "cooldown"] call core_fnc_getSetting);
[1, format[_msg, (name _caller), (["off","on"] select _arg), _cooldown], ["HQ", PLAYER_SIDE]] call global_fnc_chat;

baseLight = _arg;
baseLightNext = serverTime + _cooldown; 
publicVariable "baseLight";
publicVariable "baseLightNext";