/*
@filename: feats\_cfgRemoteFnc.hpp
Author:
	Ben
Description:
	on all context,
	included by description.ext,
	include features remote functions entries
*/

#include "define.hpp"

#include "assets\_cfgRemoteFnc.hpp"

#ifdef use_artiSupport
#include "artiSupport\_cfgRemoteFnc.hpp"
#endif

#ifdef use_baseDefence
#include "baseDefence\_cfgRemoteFnc.hpp"
#endif

#ifdef use_baseLight
#include "baseLight\_cfgRemoteFnc.hpp"
#endif

#ifdef use_baseProtection
#include "baseProtection\_cfgRemoteFnc.hpp"
#endif

#ifdef use_cleanUp
#include "cleanUp\_cfgRemoteFnc.hpp"
#endif

#include "curator\_cfgRemoteFnc.hpp"

#ifdef use_environment
#include "environment\_cfgRemoteFnc.hpp"
#endif

#ifdef use_flares
#include "flares\_cfgRemoteFnc.hpp"
#endif

#include "groups\_cfgRemoteFnc.hpp"
#include "hostage\_cfgRemoteFnc.hpp"

#ifdef use_ia
#include "ia\_cfgRemoteFnc.hpp"
#endif


#include "memberData\_cfgRemoteFnc.hpp"
#include "nutsKick\_cfgRemoteFnc.hpp"

#ifdef use_revive
#include "revive\_cfgRemoteFnc.hpp"
#endif

#include "tasks\_cfgRemoteFnc.hpp"

#ifdef use_uavRecon
#include "uavRecon\_cfgRemoteFnc.hpp"
#endif

#ifdef use_vehicleLoadout
#include "vehicleLoadout\_cfgRemoteFnc.hpp"
#endif

#ifdef use_vehicleRepair
#include "vehicleRepair\_cfgRemoteFnc.hpp"
#endif

#ifdef use_vehicleRespawn
#include "vehicleRespawn\_cfgRemoteFnc.hpp"
#endif

#ifdef use_vehicleRestrictions
#include "vehicleRestrictions\_cfgRemoteFnc.hpp"
#endif

#ifdef use_vehicleSelection
#include "vehicleSelection\_cfgRemoteFnc.hpp"
#endif

#ifdef use_zeusCompo
#include "zeusCompo\_cfgRemoteFnc.hpp"
#endif
