#include "..\core\debugLevels.hpp"

//comment the following line to disable all features debug
#define DEBUG_FEATS

//default feature debug level
#ifndef DEBUG_LVL
#define DEBUG_LVL LL_INFO
#endif

#ifndef DEBUG_FEATS
#undef DEBUG
#endif

#include "..\core\__debug.hpp"
