/*
@filename: feats\artiSupport\serverDestroy.sqf
Author:
	Ben
Description:
	run on server,
*/

#include "_debug.hpp"

if ( (["artiSupport"] call core_fnc_getParam) isEqualTo 0 ) exitWith {
	#ifdef DEBUG
   	debug(LL_INFO, "disabled by mission parameter");
    #endif
    nil  
};

{
	_x params ["_missionId", "_thread"];
    terminate _thread;    
} forEach artiSupport_missionThreads;
	
{
	_x params ["_plot", "_prep", "_reload"];
    terminate _plop;
    terminate _prep;
    terminate _reload;    
} forEach artiSupport_tubeThreads;

{
    _x params ["_batIdx", "_name", "_veh", "_group", "_unit"];
    deleteVehicle _unit;
    deleteVehicle _veh;
    deleteGroup _group;
} forEach artiSupport_tubes;