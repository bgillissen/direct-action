#include "..\_debug.hpp"

params ["_misId", "_player"];

private _missionIdx = -1;
{
	if ( (_x select 0) isEqualTo _misId ) exitWith { _missionIdx = _forEachIndex; };
} forEach artiSupport_missions;

if ( _missionIdx < 0 ) exitWith {
    #ifdef DEBUG
    private _debug = format["asap exec failed, could not find mission with id '%1'", _misId];
    debug(LL_ERR, _debug);
    #endif
};

(artiSupport_missions select _missionIdx) params ["_misId", "_name", "_owner", "_lastEditBy", "_grid", "_cor", "_ord", "_mode", "_requestedBy", "_grantedBy", "_executedBy"];

private _tubeData = [];
{
	_x params ["_tubeId", "_shell", "_amount", "_spread"];
    if ( _amount > 0 ) then {
        (artiSupport_tubes select _tubeId) params ["_batId", "_name", "_veh", "_grp", "_gunner"];
        if ( [_batId, _player] call as_fnc_canGiveOrder ) then {
			_tubeData pushback [_misId, _tubeId, _amount, _shell, _veh];
        };
	};
} forEach _ord;

#ifdef DEBUG
private _debug = format["asap: %1 tube(s) in tubeData'", count _tubeData];
debug(LL_DEBUG, _debug);
#endif

private _asapFnc = {
    params ['_misId', '_tubeID', '_amount', '_shell', '_veh'];
    for "_firedRound" from 1 to _amount do {
        #ifdef DEBUG
		private _debug = format["asap: starting wave %1 for tubeId %2", _firedRound, _tubeId];
		debug(LL_DEBUG, _debug);
		private _debug = format["asap: ploting tubeId %1", _tubeId];
		debug(LL_DEBUG, _debug);
		#endif
        private _plotThread = [_misId, _tubeId, (_firedRound > 1)] spawn as_fnc_plotThread;
		waitUntil { isNull _plotThread };
        private _ok = ( [_misId, _tubeId, 'plot'] call as_fnc_isDone );
        if ( _ok ) then {
        	#ifdef DEBUG
			private _debug = format["asap: preping tubeId %1", _tubeId];
			debug(LL_DEBUG, _debug);
			#endif
			private _prepThread = [_misId, _tubeId, (_firedRound > 1)] spawn as_fnc_prepThread;
            waitUntil { isNull _prepThread };
            _ok = ( [_misId, _tubeId, 'prep'] call as_fnc_isDone );    
        };
		if ( _ok ) then {
			(_veh getVariable "as_aim") params ["_misId", "_aimPos", "_weapon", "_mode", "_muzzle", "_eta"];
            #ifdef DEBUG
			private _debug = format["asap: fireing tubeId %1", _tubeId];
			debug(LL_DEBUG, _debug);
			#endif
        	_veh fire [_muzzle, _mode, _shell];
            
            _veh setPos (_veh getVariable "as_pos");
			_veh setDir (_veh getVariable "as_dir");
       
        	private _loadState = (_veh getVariable "as_load");
        	_loadState set [3, false];
        	_veh setVariable ["as_load", _loadState, true];
            if ( _firedRound < _amount ) then {
				#ifdef DEBUG
				private _debug = format["asap: reloading tubeId %1", _tubeId];
				debug(LL_DEBUG, _debug);
				#endif
        		_loadThread = [_misId, _tubeId] spawn as_fnc_reloadThread;
				waitUntil { isNull _loadThread };               	   
        	};   
        };
    };
};

private _threads = [];
{
    private _thread = _x spawn _asapFnc;
    artiSupport_missionThreads pushback [_misId, _thread];
    _threads pushback _thread;
} forEach _tubeData;

waitUntil {
    (({ !(isNull _x) } count _threads) isEqualTo 0 )
};

artiSupport_missionThreads = (artiSupport_missionThreads - [[_misId, scriptNull]]); 