
#include "..\_debug.hpp"

params ["_misId", "_player"];

private _missionIdx = -1;
{
	if ( (_x select 0) isEqualTo _misId ) exitWith { _missionIdx = _forEachIndex; };
} forEach artiSupport_missions;

if ( _missionIdx < 0 ) exitWith {
    #ifdef DEBUG
    private _debug = format["exec failed, could not find mission with id '%1'", _misId];
    debug(LL_ERR, _debug);
    #endif
    publicVariable "artiSupport_missions";
    if ( CTXT_PLAYER ) then { publicVariableServer "artiSupport_missions"; };
};

(artiSupport_missions select _missionIdx) params ["_misId", "_name", "_owner", "_lastEditBy", "_grid", "_cor", "_ord", "_mode", "_requestedBy", "_grantedBy", "_executedBy"];
private _isGranted = !( isNull _grantedBy );
private _isRequested = !( isNull _requestedBy );

private _ok = (isNull _executedBy);
if ( _ok ) then {
	{
    	_x params ["_tMisId", "_thread"];
    	if ( _tMisId isEqualTo _misId ) exitWith { _ok = false; };
	} forEach artiSupport_missionThreads;
};

if !( _ok ) exitWith {
	#ifdef DEBUG
    private _debug = format["exec failed, mission id '%1' is already being executed, abording", _misId];
    debug(LL_ERR, _debug);
    #endif
    publicVariable "artiSupport_missions";
    if ( CTXT_PLAYER ) then { call asCtrl_fnc_hasChanged; };    
};

if !( [_player] call as_fnc_canExecute ) exitWith {
    #ifdef DEBUG
    private _debug = format["exec failed, player '%1' can not execute, abording", _player];
    debug(LL_ERR, _debug);
    #endif
    publicVariable "artiSupport_missions";
    if ( CTXT_PLAYER ) then { call asCtrl_fnc_hasChanged; };
};

private _needGrant = ([_player] call as_fnc_needGrant);
if ( _needGrant ) then { _ok = ( _isRequested && _isGranted ); };
if !( _ok ) exitWith {
    #ifdef DEBUG
    private _debug = format["exec failed, mission id '%1' is not granted, abording", _misId];
    debug(LL_ERR, _debug);
    #endif
	publicVariable "artiSupport_missions";
    if ( CTXT_PLAYER ) then { call asCtrl_fnc_hasChanged; };
};

{
    _x params ["_tubeId", "_shell", "_amount", "_spread"];
    (artiSupport_tubes select _tubeId) params ["_batId", "_name", "_veh", "_grp", "_gunner"];
    (_veh getVariable 'as_exec') params ['_exeMisId', '_isExecuting'];
    if ( _isExecuting ) exitWith {
        #ifdef DEBUG
    	private _debug = format["exec failed, tube with id '%1' is busy, abording", _tubeId];
    	debug(LL_ERR, _debug);
    	#endif 
    	_ok = false; 
    };
} forEach _ord;

if !( _ok ) exitWith {
	publicVariable "artiSupport_missions";
    if ( CTXT_PLAYER ) then { call asCtrl_fnc_hasChanged; };
};

(artiSupport_missions select _missionIdx) set [10, _player];
publicVariable "artiSupport_missions";

{
    _x params ["_tubeId", "_shell", "_amount", "_spread"];
    (artiSupport_tubes select _tubeId) params ["_batId", "_name", "_veh", "_grp", "_gunner"];
    private _exec = (_veh getVariable 'as_exec');
    _exec set [0, _misId];
    _exec set [1, true];
    _veh setVariable ['as_exec', _exec, true];
} forEach _ord;

if ( CTXT_PLAYER ) then { call asCtrl_fnc_hasChanged; };

private _modeName = switch (_mode select 0) do {
    case 0 : { "Asap" };
    case 1 : { "Sequencial" };
    case 2 : { "Wave" };
};

#ifdef DEBUG
private _debug = format["%1 is executing mode %2 for misId %3", _player, _modeName, _misId];
debug(LL_ERR, _debug);
#endif 

private _thread = _this call compile format["_this spawn as_fnc_exec%1", _modeName];

artiSupport_missionThreads pushback [_misId, _thread];

waitUntil {
    sleep 2;
    isNull _thread
};

#ifdef DEBUG
private _debug = format["misId %1 execution is done", _misId];
debug(LL_ERR, _debug);
#endif 

artiSupport_missionThreads = (artiSupport_missionThreads - [[_misId, scriptNull]]);

{
    _x params ["_tubeId", "_shell", "_amount", "_spread"];
    (artiSupport_tubes select _tubeId) params ["_batId", "_name", "_veh", "_grp", "_gunner"];
    private _exec = (_veh getVariable 'as_exec');
    _exec set [1, false];
    _veh setVariable ['as_exec', _exec, true];
} forEach _ord;

private _missionIdx = -1;
{
	if ( (_x select 0) isEqualTo _misId ) exitWith { _missionIdx = _forEachIndex; };
} forEach artiSupport_missions;

if ( _missionIdx < 0 ) exitWith {
    #ifdef DEBUG
    private _debug = format["exec error, could not find mission with id '%1' to reset its execution state", _misId];
    debug(LL_ERR, _debug);
    #endif
};

if ( _needGrant ) then { 
	(artiSupport_missions select _missionIdx) set [8, objNull];
	(artiSupport_missions select _missionIdx) set [9, objNull];
};
(artiSupport_missions select _missionIdx) set [10, objNull];

publicVariable "artiSupport_missions";
if ( CTXT_PLAYER ) then { publicVariableServer "artiSupport_missions"; };