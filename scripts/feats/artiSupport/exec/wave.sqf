
#include "..\_debug.hpp"

params ["_misId", "_player"];

private _missionIdx = -1;
{
	if ( (_x select 0) isEqualTo _misId ) exitWith { _missionIdx = _forEachIndex; };
} forEach artiSupport_missions;

if ( _missionIdx < 0 ) exitWith {
    #ifdef DEBUG
    private _debug = format["wave exec failed, could not find mission with id '%1'", _misId];
    debug(LL_ERR, _debug);
    #endif
};

(artiSupport_missions select _missionIdx) params ["_misId", "_name", "_owner", "_lastEditBy", "_grid", "_cor", "_ord", "_mode", "_requestedBy", "_grantedBy", "_executedBy"];

private _tubeData = [];
private _loading = [];
{
	_x params ["_tubeId", "_shell", "_amount", "_spread"];
    if ( _amount > 0 ) then {
        (artiSupport_tubes select _tubeId) params ["_batId", "_name", "_veh", "_grp", "_gunner"];
        if ( [_batId, _player] call as_fnc_canGiveOrder ) then {
			_tubeData pushback [_tubeId, _amount, _shell, _veh];
            _loading pushback [_tubeId, _forEachIndex, scriptNull];
        };
	};
} forEach _ord;

private _waveDelay = _mode select 1;
private _waveStart = 0;
private _firedRound = 0;

#ifdef DEBUG
private _debug = format["wave: %1 tube(s) in tubeData'", count _tubeData];
debug(LL_DEBUG, _debug);
#endif
 
while { true } do {
    
    if ( ({( _firedRound < (_x select 1) )} count _tubeData) isEqualTo 0 ) exitWith {};
    
	#ifdef DEBUG
	private _debug = format["wave: starting wave %1", (_firedRound + 1)];
	debug(LL_DEBUG, _debug);
	#endif       
    
    private _ploting = [];
    while { (count _loading) > 0 } do { 
    	{
			_x params ["_tubeId", "_dataIdx", "_thread"];
        	if ( isNull _thread ) then {
                _loading deleteAt _forEachIndex;
				#ifdef DEBUG
				private _debug = format["wave: ploting tubeId %1", _tubeId];
				debug(LL_DEBUG, _debug);
				#endif
            	private _plotThread = [_misId, _tubeId, (_firedRound > 0)] spawn as_fnc_plotThread;
                _ploting pushback [_tubeId, _dataIdx, _plotThread];
        	};
    	} forEach _loading;
        sleep 0.5;
	};
    
    private _preping = [];
    while { (count _ploting) > 0 } do { 
    	{
			_x params ["_tubeId", "_dataIdx", "_thread"];
        	if ( isNull _thread ) then {
                _ploting deleteAt _forEachIndex;
                if ( [_misId, _tubeId, 'plot'] call as_fnc_isDone ) then {
					#ifdef DEBUG
					private _debug = format["wave: preping tubeId %1", _tubeId];
					debug(LL_DEBUG, _debug);
					#endif
                	private _prepThread = [_misId, _tubeId, (_firedRound > 0)] spawn as_fnc_prepThread;
                    _preping pushback [_tubeId, _dataIdx, _prepThread];
                };                
        	};
    	} forEach _ploting;
        sleep 0.5;
	};
    	
    private _fireing = [];
    while { (count _preping) > 0 } do { 
    	{
			_x params ["_tubeId", "_dataIdx", "_thread"];
        	if ( isNull _thread ) then {
                _preping deleteAt _forEachIndex;
                if ( [_misId, _tubeId, 'prep'] call as_fnc_isDone ) then {
                    #ifdef DEBUG
					private _debug = format["wave: tubeId %1 is ready", _tubeId];
					debug(LL_DEBUG, _debug);
					#endif
                	_fireing pushback _dataIdx;
                };                
        	};
    	} forEach _preping;
        sleep 0.5;
	};
    
    #ifdef DEBUG
	private _debug = format["wave: waiting %1s", (_waveStart + _waveDelay - serverTime)];
	debug(LL_DEBUG, _debug);
	#endif 
    waitUntil { (serverTime >= (_waveStart + _waveDelay)) }; 
    _firedRound = _firedRound + 1;
    
    private _hasFired = [];
    {
    	(_tubeData select _x) params ["_tubeId", "_amount", "_shell", "_veh"];
		_hasFired pushback [_tubeId, _x, _amount];
        (_veh getVariable "as_aim") params ["_misId", "_aimPos", "_weapon", "_mode", "_muzzle", "_eta"];
        _veh fire [_muzzle, _mode, _shell];
          
        _veh setPos (_veh getVariable "as_pos");
		_veh setDir (_veh getVariable "as_dir");
     
        private _loadState = (_veh getVariable "as_load");
        _loadState set [3, false];
        _veh setVariable ["as_load", _loadState, true];
        
    } forEach _fireing;
    
    publicVariable "artiSupport_tubeShells";
    
    _waveStart = serverTime;

    {
        _x params ["_tubeId", "_dataIdx", "_amount"];
        if ( _firedRound < _amount ) then {
			#ifdef DEBUG
			private _debug = format["wave: reloading tubeId %1", _tubeId];
			debug(LL_DEBUG, _debug);
			#endif
        	_thread = [_misId, _tubeId] spawn as_fnc_reloadThread;
            _loading pushback [_tubeId, _dataIdx, _thread];   
        };
    } forEach _hasFired;
};