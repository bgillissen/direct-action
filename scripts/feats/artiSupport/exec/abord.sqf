
#include "..\_debug.hpp"

params ["_misId", "_player"];

private _missionIdx = -1;
{
	if ( (_x select 0) isEqualTo _misId ) exitWith { _missionIdx = _forEachIndex; };
} forEach artiSupport_missions;

if ( _missionIdx < 0 ) exitWith {
    #ifdef DEBUG
    private _debug = format["abord failed, could not find mission with id '%1'", _misId];
    debug(LL_ERR, _debug);
    #endif
};

(artiSupport_missions select _missionIdx) params ["_misId", "_name", "_owner", "_lastEditBy", "_grid", "_cor", "_ord", "_mode", "_requestedBy", "_grantedBy", "_executedBy"];

if !( _player isEqualTo _executedBy ) exitWith {
    #ifdef DEBUG
    debug(LL_ERR, "abord failed, player is not the one executing this mission", 0);
    #endif
};

{
    _x params ["_tMisId", "_thread"];
    if ( _tMisId isEqualTo _misId ) then {
        #ifdef DEBUG
    	private _debug = format["mission with id '%1' execution has been aborded", _misId];
    	debug(LL_ERR, _debug);
    	#endif
        terminate _thread;
        artiSupport_missionThreads deleteAt _forEachIndex;
    };
} forEach artiSupport_missionThreads;