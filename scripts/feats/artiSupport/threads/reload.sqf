
#include "..\_debug.hpp"

params["_misId", "_tubeId", ["_prepCall", false]];

//systemChat format["loadThread: start misId: %1, tubeId: %2", _misId, _tubeId];

private _tubeThreads = (artiSupport_tubeThreads select _tubeId);
private "_ok";
if !( _prepCall ) then {
    _ok = ( ({ !isNull _x } count _tubeThreads) isEqualTo 0 );
} else {
    _ok = ( (isNull (_tubeThreads select 0)) && (isNull(_tubeThreads select 2)) );
};

if !( _ok ) exitwith {
	#ifdef DEBUG
    private _debug = format["load failed, tube with id '%1' is busy", _tubeId];
    debug(LL_ERR, _debug);
    #endif
};

(artiSupport_tubeThreads select _tubeId) set [2, _thisScript];

private _missionIdx = -1;
{
	if ( (_x select 0) isEqualTo _misId ) exitWith { _missionIdx = _forEachIndex; };
} forEach artiSupport_missions;

if ( _missionIdx < 0 ) exitWith {
    #ifdef DEBUG
    private _debug = format["prep failed, could not find mission with id '%1'", _misId];
    debug(LL_ERR, _debug);
    #endif
};

(artiSupport_missions select _missionIdx) params ["_misId", "_name", "_owner", "_lastEditBy", "_grid", "_cor", "_ord", "_mode", "_requestedBy", "_grantedBy", "_executedBy"];
(artiSupport_tubes select _tubeId) params ["_batId", "_name", "_veh", "_grp", "_gunner"];

_veh setDir (_veh getVariable "as_dir");
_veh setPos (_veh getVariable "as_pos");

(_veh getVariable 'as_plot') params ['_plotMisId', '_plotStart', '_plotDuration', '_plotIsDone', ['_plotGrid', []], ['_plotCor', []], ['_plotShell', '']];

private _loadState = (_veh getVariable 'as_load');

private _isPloted = [_misId, _tubeId, "plot"] call as_fnc_isDone;
if !( _isPloted ) exitWith {
    #ifdef DEBUG
    debug(LL_ERR, "load failed, can not load before plot");
    #endif
	_loadState set [3, false];
    _veh setVariable ['as_load', _loadState, true];
    if ( CTXT_PLAYER ) then { call asCtrl_fnc_hasChanged; };    
};

if !( (vehicle _gunner) isEqualTo _veh ) exitWith {
    #ifdef DEBUG
    debug(LL_ERR, "load failed, gunner is not in vehicle");
    #endif 
	_loadState set [3, false];
    _veh setVariable ['as_load', _loadState, true];
    if ( CTXT_PLAYER ) then { call asCtrl_fnc_hasChanged; };
};

private _duration = [_batId, 'load'] call as_fnc_getDuration;

_loadState set [0, _plotshell];
_loadState set [1, serverTime];
_loadState set [2, _duration];
_loadState set [3, false];

_veh setVariable ['as_load', _loadState, true];
if ( CTXT_PLAYER ) then { 
	[nil, nil, _veh] call asCtrl_fnc_exeTubeUpdate;
    call asCtrl_fnc_hasChanged; 
};

(_veh getVariable 'as_aim') params ["_aimMisId", "_aimPos", "_aimWeapon", "_aimMode", "_aimMuzzle", "_aimEta"];


private _turretPath = ((assignedVehicleRole _gunner) select 1);

_gunner setSkill ['reloadSpeed', 1];
_veh setWeaponReloadingTime [_gunner, _aimMuzzle, 0]; 
_veh loadMagazine [_turretPath, _aimWeapon, _plotShell];

private _end = (_loadState select 1) + (_loadState select 2);

waitUntil {
    ( (serverTime > _end) || !((vehicle _gunner) isEqualTo _veh) )
};
private _ok = ( (vehicle _gunner) isEqualTo _veh );

if ( _ok ) then {
    _weaponState = (weaponState [_veh, _turretPath]);
	_ok = ( (_weaponState select 3) isEqualTo _plotShell );    
};

//systemChat format["loadThread: finished with: %1", _ok];
_loadState set [3, _ok];
_veh setVariable ['as_load', _loadState, true];
if ( CTXT_PLAYER ) then { 
	[nil, nil, _veh] call asCtrl_fnc_exeTubeUpdate; 
	call asCtrl_fnc_hasChanged;
};