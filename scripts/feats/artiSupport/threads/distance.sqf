
params ["_target", "_caller", "_id", "_batId"];

private _limit = ["artiSupport", "maxDistance"] call core_fnc_getSetting;

while { true } do {
    if ( (_caller distance _target) > _limit ) exitWith {
    	[_target, _caller, -1, _batId] spawn as_fnc_actReleaseCommand;    
    };
    sleep 5;
};