
#include "..\_debug.hpp"

params["_misId", "_tubeId", ["_fast", false]];

if ( ({ !isNull _x } count (artiSupport_tubeThreads select _tubeId)) > 0 ) exitWith {
    #ifdef DEBUG
    private _debug = format["plot failed, tube with id '%1' is busy", _tubeId];
    debug(LL_ERR, _debug);
    #endif
};

(artiSupport_tubeThreads select _tubeId) set [0, _thisScript];

private _missionIdx = -1;
{
	if ( (_x select 0) isEqualTo _misId ) exitWith { _missionIdx = _forEachIndex; };
} forEach artiSupport_missions;

if ( _missionIdx < 0 ) exitWith {
    #ifdef DEBUG
    private _debug = format["plot failed, could not find mission with id '%1'", _misId];
    debug(LL_ERR, _debug);
    #endif
};

(artiSupport_missions select _missionIdx) params ["_misId", "_name", "_owner", "_lastEditBy", "_grid", "_cor", "_ord", "_mode", "_requestedBy", "_grantedBy", "_executedBy"];

private _ordIdx = -1;
{
	_x params ["_ordTubeId", "_shell", "_amount", "_spread"];
    if ( _tubeId isEqualTo _ordTubeId ) exitWith { _ordIdx = _forEachIndex; };
} forEach _ord;

if ( _ordIdx < 0 ) exitWith {
    #ifdef DEBUG
    private _debug = format["plot failed, could not find tube with id '%1' in mission ordonance", _tubeId];
    debug(LL_ERR, _debug);
    #endif
};

(_ord select _ordIdx) params ["_tubeId", "_shell", "_amount", "_spread"];

(artiSupport_tubes select _tubeId) params ["_batId", "_name", "_veh", "_grp", "_gunner"];

_veh setDir (_veh getVariable "as_dir");
_veh setPos (_veh getVariable "as_pos");

private _plotState = (_veh getVariable 'as_plot');

if !( (vehicle _gunner) isEqualTo _veh ) exitWith {
    #ifdef DEBUG
    debug(LL_ERR, "plot failed, gunner is not in vehicle");
    #endif 
	_plotState set [0, _misId];
	_plotState set [3, false];
    _veh setVariable ['as_plot', _plotState, true];
    if ( CTXT_PLAYER ) then { call asCtrl_fnc_hasChanged; };
};

private _isDone = [_misId, _tubeId, "plot"] call as_fnc_isDone;
private _duration = [_batId, 'plot'] call as_fnc_getDuration;
if ( _isDone && _fast ) then { _duration = 5; };

_plotState set [0, _misId];
_plotState set [1, serverTime];
_plotState set [2, _duration];
_plotState set [3, false];
_plotState set [4, _grid];
_plotState set [5, _cor];
_plotState set [6, _shell];

_veh setVariable ['as_plot', _plotState, true];
if ( CTXT_PLAYER ) then { 
	[nil, nil, _veh] call asCtrl_fnc_exeTubeUpdate;
    call asCtrl_fnc_hasChanged; 
};

_grid params ['_gridX', '_gridY', '_gridZ'];
_cor params ['_corX', '_corY'];

([(parseNumber _gridX), (parseNumber _gridY), _corX, _corY] call arti_fnc_gridToCoord) params ['_aimX', '_aimY'];

if ( _spread > 0 ) then {
	_aimX = _aimX - _spread + (2 * random _spread);
	_aimY = _aimY - _spread + (2 * random _spread);    
};

([(_veh call arti_fnc_getTubePosition), [_aimX, _aimY, (parseNumber _gridZ)]] call arti_fnc_getDistance) params ["_dist", "_trgtHeight", "_altDiff"];

private _ok = true;
private _weapon = [_gunner] call arti_fnc_getWeapon;
if ( _weapon isEqualTo "" ) then {
    #ifdef DEBUG
    private _debug = format["plot failed, empty weapon for tube '%1'", _tubeId];
    debug(LL_ERR, _debug);
    #endif
    _ok = false;
};

private "_muzzle";
if ( _ok ) then {
	private _muzzles = [_gunner] call arti_fnc_getMuzzles;
	if ( (count _muzzles) isEqualTo 0 ) then {
	    #ifdef DEBUG
	    private _debug = format["plot failed, no muzzles returned for tube '%1'", _tubeId];
	    debug(LL_ERR, _debug);
	    #endif
        _ok = false;
	} else {
		_muzzle = (_muzzles select 0);
    };
};

private "_modes";
if ( _ok ) then {
	(assignedVehicleRole _gunner) params[["_role", ""], ["_turretPath", []]];
	_modes = [_veh, _turretPath, _shell, _dist, _altDiff] call arti_fnc_getModes;
	if ( ({_x select 4} count _modes) isEqualto 0 ) exitWith {
	    #ifdef DEBUG
	    private _debug = format["plot failed, no modes returned for tube '%1' (%2m)", _tubeId, _dist];
	    debug(LL_ERR, _debug);
	    #endif
        _ok = false;		
	};
};

if ( _ok ) then {
    #define G 9.81
    private "_fireMode";
    {
    	if ( _x select 4 ) exitWith { _fireMode = _x; };
    } forEach _modes;
	_fireMode params ["_mode", "_charge", "_minRange", "_maxRange", "_inRange"];
	private _speed = getNumber(configfile >> "CfgMagazines" >> _shell >> "initSpeed") * _charge;
	if ( (_speed^4 - G * (G * _dist^2 + 2 * _trgtHeight * _speed^2)) < 0 ) exitWith {
		#ifdef DEBUG
	    private _debug = format["plot failed, target is not in range for tube '%1' (%2m)", _tubeId, _dist];
	    debug(LL_ERR, _debug);
	    #endif
        _ok = false;
	};	
	private _angle = atan( (_speed^2 + sqrt(_speed^4 - G * (G * _dist^2 + 2 * _trgtHeight * _speed^2)))/(G * _dist));
	private _aimZ = _dist * (tan _angle);
	private _eta = _dist / (_speed * cos(_angle));
	_veh setVariable['as_aim', [_misId, [_aimX, _aimY, _aimZ], _weapon, _mode, _muzzle, _eta], true];
};

private _end = (_plotState select 1) + (_plotState select 2);
waitUntil {
	( (serverTime > _end) || !((vehicle _gunner) isEqualTo _veh) )
};
    
_plotState set [3, ( ((vehicle _gunner) isEqualTo _veh) && _ok )];
_veh setVariable ['as_plot', _plotState, true];
if ( CTXT_PLAYER ) then { 
	[nil, nil, _veh] call asCtrl_fnc_exeTubeUpdate; 
	call asCtrl_fnc_hasChanged;
};