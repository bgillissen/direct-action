
params ["_player", "_veh", "_unit"];

_unit leaveVehicle _veh;
[_unit] orderGetIn false;
unassignVehicle _unit;

waitUntil {
	sleep 2; 
	( ((vehicle _player) isEqualTo _veh) || (isNull _player) || !(alive _player) || (_player getVariable ['agony', false]) ) 
};

waitUntil {
	sleep 2;
	( ((vehicle _player) isEqualTo _player) || (isNull _player) || !(alive _player) || (_player getVariable ['agony', false]) )
};

_unit assignAsGunner _veh;
[_unit] orderGetIn true;

sleep 10;

if !( (vehicle _unit) isEqualTo _veh ) then {
	_unit action ['getInGunner', _veh];    
};