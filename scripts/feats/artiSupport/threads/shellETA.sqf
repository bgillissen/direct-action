
#include "..\ui\control\define.hpp"

#define EMPTY_ARRAY []

disableSerialization;

private _dsp = findDisplay AS_CTRL_IDD;
private _scr = _dsp displayCtrl EXE_IDC;
private _list = (_scr controlsGroupCtrl EXE_TLST);
private _maxWidth = TOT_WIDTH - MENU_WIDTH - (3 * SPACE);

private _stack = [];
private _syncStack = {
    _stack = EMPTY_ARRAY;
	{ _stack pushback (_x + []); } forEach artiSupport_tubeShells;
};

while { true } do {
    if ( EXE_IDC isEqualTo artiSupport_uiMenu ) then {
	    waitUntil { !artiSupport_lockETAThread };
        private _updated = !( _stack isEqualTo artiSupport_tubeShells );
        if ( ((count _stack) > 0) || _updated ) then {
			private _loopTime = serverTime;
			private _lastY = 0;
		    private _idc = EXE_TBASE;
			while { !isNull (_list controlsGroupCtrl _idc) } do {
                if ( artiSupport_lockETAThread ) exitWith {};
		        private _entry = (_list controlsGroupCtrl _idc);
		        private _tubeId = parseNumber ctrlText (_entry controlsGroupCtrl EXE_EID);
		        private _shellHeight = 0;
		        if ( _updated ) then { 
                	call _syncStack;
		        	_shellHeight = ([_tubeId] call asCtrl_fnc_exeTubeShell); 
				} else {
                    private _slst = (_entry controlsGroupCtrl EXE_SLST);
                    private _shown = cbChecked (_entry controlsGroupCtrl EXE_ETOGL);
		        	if ( _shown ) then {
		            	private _shellIdc = EXE_SBASE;
		                private _shellCount = 0; 
		            	while { !isNull (_slst controlsGroupCtrl _shellIdc) } do {
		            		if ( artiSupport_lockETAThread ) exitWith {};
		                	private _shellEntry = _slst controlsGroupCtrl _shellIdc;
		                    private _shellPos = ctrlPosition _shellEntry;
		                    _shellPos set [1, (_shellCount * ((LINE_HEIGHT * SHELL_HEIGHT_FACTOR) + SPACE))];
		                	private _shotAt = parseNumber ctrlText (_shellEntry controlsGroupCtrl EXE_SAT);
		                	private _eta = parseNumber ctrlText (_shellEntry controlsGroupCtrl EXE_SETA);
		                	private _timeLeft = _eta + _shotAt - serverTime;
		                	if ( _timeLeft > SHELL_SHOWN_AFTER_IMPACT ) then {
                                _shellPos set [3, ((LINE_HEIGHT * SHELL_HEIGHT_FACTOR) + SPACE)];
		                        _shellEntry ctrlShow true;
		                		if ( _timeLeft < 0 ) then { _timeLeft = 0; };
		                		(_shellEntry controlsGroupCtrl EXE_STL) ctrlSetText str ([_timeLeft, 1] call common_fnc_roundTo);
		                        private _prgB = (_shellEntry controlsGroupCtrl EXE_SPGB);
								private _prct = (_eta - _timeLeft) / _eta;
		        				if ( _prct >= 1 ) then { _prgB ctrlSetBackgroundColor [1, 0.4, 0.4, 0.7]; };	                        
		                        _shellCount = _shellCount + 1;               		
							} else {
                                _shellPos set [3, 0];                    	
		                        _shellEntry ctrlShow false;
		                	};
                            _shellEntry ctrlSetPosition _shellPos;
							_shellEntry ctrlCommit 0;
		                    _shellIdc = _shellIdc + 1;
						};
		                if ( _shellCount >= 3 ) then {
		                	_shellHeight = (3 * ((LINE_HEIGHT * SHELL_HEIGHT_FACTOR) + SPACE));
						} else {
		                    _shellHeight = (_shellCount * ((LINE_HEIGHT * SHELL_HEIGHT_FACTOR) + SPACE));
		                };
                    };
                    private _pos = ctrlPosition _slst;
					_pos set [3, _shellHeight];
					_slst ctrlSetPosition _pos;
					_slst ctrlCommit 0;
                    _slst ctrlShow ( (_shellHeight > 0) && _shown );
				};
		        private _entryHeight = LINE_HEIGHT + (SPACE * 1.25) + _shellHeight;
	            private _line = (_entry controlsGroupCtrl EXE_LINE);
	            private _pos = ctrlPosition _line;
				_pos set [1, (LINE_HEIGHT + SPACE + _shellHeight)];
				_line ctrlSetPosition _pos; 
				_line ctrlCommit 0;
		        private _pos = ctrlPosition _entry;
		        _pos set [1, _lastY];
		        _pos set [3, _entryHeight];
				_entry ctrlSetPosition _pos;
				_entry ctrlCommit 0;
				_lastY = _lastY + _entryHeight;    
				_idc = _idc + 1;
		    };
		    sleep (1 / 25);
		} else {
        	sleep 0.5;
		};
	} else {
        sleep 0.5;
	};
};