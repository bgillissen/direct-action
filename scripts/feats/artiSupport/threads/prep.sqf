
#include "..\_debug.hpp"

params["_misId", "_tubeId", ["_fast", false]];

if ( ({ !isNull _x } count (artiSupport_tubeThreads select _tubeId)) > 0 ) exitWith {
    #ifdef DEBUG
    private _debug = format["prep failed, tube with id '%1' is busy", _tubeId];
    debug(LL_ERR, _debug);
    #endif
};

(artiSupport_tubeThreads select _tubeId) set [1, _thisScript];

private _missionIdx = -1;
{
	if ( (_x select 0) isEqualTo _misId ) exitWith { _missionIdx = _forEachIndex; };
} forEach artiSupport_missions;

if ( _missionIdx < 0 ) exitWith {
    #ifdef DEBUG
    private _debug = format["prep failed, could not find mission with id '%1'", _misId];
    debug(LL_ERR, _debug);
    #endif
};

(artiSupport_missions select _missionIdx) params ["_misId", "_name", "_owner", "_lastEditBy", "_grid", "_cor", "_ord", "_mode", "_requestedBy", "_grantedBy", "_executedBy"];
(artiSupport_tubes select _tubeId) params ["_batId", "_name", "_veh", "_grp", "_gunner"];

_veh setDir (_veh getVariable "as_dir");
_veh setPos (_veh getVariable "as_pos");

(_veh getVariable 'as_plot') params ['_plotMisId', '_plotStart', '_plotDuration', '_plotIsDone', ['_plotGrid', []], ['_plotCor', []], ['_plotShell', '']];

private _prepState = (_veh getVariable 'as_prep');

private _isPloted = [_misId, _tubeId, "plot"] call as_fnc_isDone;
if !( _isPloted ) exitWith {
    #ifdef DEBUG
    debug(LL_ERR, "prep failed, can not prep before plot");
    #endif
    _prepState set [0, _misId];
	_prepState set [3, false];
    _veh setVariable ['as_prep', _prepState, true];
    if ( CTXT_PLAYER ) then { call asCtrl_fnc_hasChanged; };    
};

(_veh getVariable 'as_aim') params ["_aimMisId", "_aimPos", "_aimWeapon", "_aimMode", "_aimMuzzle", "_aimEta"];

if !( _aimMisId isEqualTo _misId ) exitWith {
    #ifdef DEBUG
    debug(LL_ERR, "prep failed, aim data does not match mission id");
    #endif 
	_prepState set [0, _misId];
	_prepState set [3, false];
    _veh setVariable ['as_prep', _prepState, true];
    if ( CTXT_PLAYER ) then { call asCtrl_fnc_hasChanged; };   
};

if !( (vehicle _gunner) isEqualTo _veh ) exitWith {
    #ifdef DEBUG
    debug(LL_ERR, "prep failed, gunner is not in vehicle");
    #endif 
	_prepState set [0, _misId];
	_prepState set [3, false];
    _veh setVariable ['as_prep', _prepState, true];
    if ( CTXT_PLAYER ) then { call asCtrl_fnc_hasChanged; };
};

private _isDone = [_misId, _tubeId, "prep"] call as_fnc_isDone;
private _duration = [_batId, 'prep'] call as_fnc_getDuration;
if ( _isDone && _fast ) then { _duration = 5; };

_prepState set [0, _misId];
_prepState set [1, serverTime];
_prepState set [2, _duration];
_prepState set [3, false];
_prepState set [4, _plotGrid];
_prepState set [5, _plotCor];

_veh setVariable ['as_prep', _prepState, true];
if ( CTXT_PLAYER ) then { 
	[nil, nil, _veh] call asCtrl_fnc_exeTubeUpdate;
    call asCtrl_fnc_hasChanged; 
};

_gunner doWatch _aimPos;

private _end = (_prepState select 1) + (_prepState select 2);
waitUntil {
    ( (serverTime > _end) || !((vehicle _gunner) isEqualTo _veh) )
};

private _ok = ( (vehicle _gunner) isEqualTo _veh );
if ( _ok ) then {
	if !( [_misId, _tubeId, 'load'] call as_fnc_isDone ) then {
		private _thread = [_misId, _tubeId, true] spawn as_fnc_reloadThread;
		waitUntil { isNull _thread };
		_ok = [_misId, _tubeId, 'load'] call as_fnc_isDone;
	};    
};

_prepState set [3, _ok];
_veh setVariable ['as_prep', _prepState, true];
if ( CTXT_PLAYER ) then { 
	[nil, nil, _veh] call asCtrl_fnc_exeTubeUpdate; 
	call asCtrl_fnc_hasChanged;
};    
