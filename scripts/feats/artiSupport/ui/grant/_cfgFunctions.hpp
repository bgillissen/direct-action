class asGrant {
	tag = "asGrant";
	class functions {
		class destroy { file="feats\artiSupport\ui\grant\destroy.sqf"; };
		class update { file="feats\artiSupport\ui\grant\update.sqf"; };
		class evtDeny { file="feats\artiSupport\ui\grant\events\deny.sqf"; };
		class evtGrant { file="feats\artiSupport\ui\grant\events\grant.sqf"; };
		class evtSelect { file="feats\artiSupport\ui\grant\events\select.sqf"; };
		class evtTab { file="feats\artiSupport\ui\grant\events\tab.sqf"; };
	};
};
