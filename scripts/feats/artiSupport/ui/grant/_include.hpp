
#include "define.hpp"

class asGrant_button : daTxtButton {
	y = (0.5 - (TOT_HEIGHT / 2)) + TOT_HEIGHT - LINE_HEIGHT - YOFFSET;
	h = LINE_HEIGHT;
	w = BUTTON_WIDTH;
};

class asGrant_misEntry : daControlGroup {
	idc = -1;
	h = LINE_HEIGHT;
	w = MENU_WIDTH;
	class Controls {
		class id : daText {
			idc = MIS_ID;
			h = 0;
			w = 0;
		};
		class button : daTxtButton {
			idc = MIS_NAME;
			style = ST_LEFT;
			h = LINE_HEIGHT;
			w = MENU_WIDTH;
			text = "Fire Mission Name";
			onMouseButtonClick = "_this call asGrant_fnc_evtSelect";
			colorDisabled[] = COL_TXT;
			colorBackgroundDisabled[] = COL_USR_BCG;
		};
	};
};

class asGrant_tabButton : daTxtButton {
	h = LINE_HEIGHT;
	w = BUTTON_WIDTH;
	onMouseButtonClick = "_this call asgrant_fnc_evtTab";
	colorDisabled[] = COL_TXT;
	colorBackgroundDisabled[] = COL_USR_BCG;
};

class asGrant_ordEntry : daControlGroup {
	x = SPACE;
	w = TOT_WIDTH - MENU_WIDTH - (2 * SPACE) - SCROLLBAR;
	h = LINE_HEIGHT;
	class Controls {
		class name : daText {
			idc = ORD_NAME;
			w = (BUTTON_WIDTH * 3);
			h = LINE_HEIGHT;
		};
		class shell : daText {
			idc = ORD_SHELL;
			x = (BUTTON_WIDTH * 3);
			w = (BUTTON_WIDTH * 2);
			h = LINE_HEIGHT;
		};
		class amount : daText {
			idc = ORD_AMOUNT;
			x =  TOT_WIDTH - MENU_WIDTH - (2 * SPACE) - SCROLLBAR - BUTTON_WIDTH;
			w = (BUTTON_WIDTH * 0.5);
			h = LINE_HEIGHT;
		};
		class spread : daText {
			idc = ORD_SPREAD;
			x =  TOT_WIDTH - MENU_WIDTH - (2 * SPACE) - SCROLLBAR - (BUTTON_WIDTH * 0.5);
			w = (BUTTON_WIDTH * 0.5);
			h = LINE_HEIGHT;
		};
	};
};
