
#include "..\define.hpp"

disableSerialization;

params [["_ctrl", controlNull]];



private _missionIdx = -1;
{  
    if ( artiSupport_uiMission isEqualto (_x select 0) ) then { _missionIdx = _forEachIndex; };
} forEach artiSupport_missions;

if ( _missionIdx < 0 ) exitWith {
    [(findDisplay AS_GRANT_IDD)] call asGrant_fnc_update;
};

private _dsp = findDisplay AS_GRANT_IDD;
 
(_dsp displayCtrl DENY_IDC) ctrlEnable false;
(_dsp displayCtrl GRANT_IDC) ctrlEnable false;

private _requestedBy = (artiSupport_missions select _missionIdx) select 8;
private _misName = (artiSupport_missions select _missionIdx) select 1;

(artiSupport_missions select _missionIdx) set [8, objNull];

["asRefuse", [(player getVariable['MD_name', (name player)]), _misName]] remoteExec ['global_fnc_notificationPlayer', _requestedBy, false];

publicVariable 'artiSupport_missions';

[(findDisplay AS_GRANT_IDD)] call asGrant_fnc_update;