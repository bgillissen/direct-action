
#include "..\define.hpp"

disableSerialization;

params [["_ctrl", controlNull]];


private _dsp = findDisplay AS_GRANT_IDD;
private _tabGrp = ctrlParentControlsGroup _ctrl;

private _pos = (getMarkerPos 'asGrant_label');

if ( _pos isEqualTo [0,0,0] ) exitWith {
	(_tabGrp controlsGroupCtrl TAB_ORD) ctrlEnable false;
	(_tabGrp controlsGroupCtrl TAB_MAP) ctrlEnable false;
    (_dsp displayCtrl MAP_IDC) ctrlshow false; 
    (_dsp displayCtrl ORD_IDC) ctrlshow false;
};

if ( (ctrlIDC _ctrl) isEqualTo TAB_MAP ) then {
    artiSupport_uiTab = 0;
	(_tabGrp controlsGroupCtrl TAB_ORD) ctrlEnable true;
	(_tabGrp controlsGroupCtrl TAB_MAP) ctrlEnable false;
    (_dsp displayCtrl MAP_IDC) ctrlshow true;
    (_dsp displayCtrl ORD_IDC) ctrlshow false;
   	(_dsp displayCtrl MAP_IDC) ctrlMapAnimAdd [3, 0.1, _pos];
   	ctrlMapAnimCommit _mapGrp;
} else {
    artiSupport_uiTab = 1;
    (_tabGrp controlsGroupCtrl TAB_ORD) ctrlEnable false;
	(_tabGrp controlsGroupCtrl TAB_MAP) ctrlEnable true;
    (_dsp displayCtrl MAP_IDC) ctrlshow false; 
    (_dsp displayCtrl ORD_IDC) ctrlshow true;
};