
#include "define.hpp"

class artiSupportGrant {
	idd = AS_GRANT_IDD;
	name = "artiSupportGrant";
	scriptName = "artiSupportGrant";
	movingEnable = false;
	enableSimulation = true;
	onLoad = "_this spawn asGrant_fnc_update;";
	onUnload = "_this spawn asGrant_fnc_destroy";
	class controlsBackground {
		class title : daTitle {
			text = "Pending Artillery Stryke Request";
			x = 0.5 - (TOT_WIDTH / 2);
			y = 0.5 - (TOT_HEIGHT / 2) - YOFFSET;
			h = LINE_HEIGHT;
			w = TOT_WIDTH;
		};
		class listBackground : daContainer {
			x = (0.5 - (TOT_WIDTH / 2));
			y = (0.5 - (TOT_HEIGHT / 2)) + LINE_HEIGHT + SPACE - YOFFSET;
			h = TOT_HEIGHT - (2 * LINE_HEIGHT) - (2 * SPACE);
			w = MENU_WIDTH;
		};
		class listTitle : daText {
			style = ST_CENTER;
			x = (0.5 - (TOT_WIDTH / 2));
			y = (0.5 - (TOT_HEIGHT / 2)) + LINE_HEIGHT + SPACE - YOFFSET;
			w = MENU_WIDTH;
			h = LINE_HEIGHT;
			text = "Fire Missions";
		};
		class infoBackground : daContainer {
			x = (0.5 - (TOT_WIDTH / 2)) + MENU_WIDTH + SPACE;
			y = (0.5 - (TOT_HEIGHT / 2)) + (2 * (LINE_HEIGHT + SPACE)) - YOFFSET;
			h = TOT_HEIGHT - (3 * LINE_HEIGHT) - (3 * SPACE);
			w = TOT_WIDTH - MENU_WIDTH - SPACE;
		};
		class close : asGrant_button {
			action = "closeDialog 0;";
			text = "Close";
			x = 0.5 - (TOT_WIDTH / 2);
		};
		class refuse : asGrant_button {
			idc = DENY_IDC;
			action = "call asGrant_fnc_evtDeny";
			text = "Deny";
			x = 0.5 - (TOT_WIDTH / 2) + BUTTON_WIDTH + SPACE;
		};
		class accept : asGrant_button {
			idc = GRANT_IDC;
			action = "call asGrant_fnc_evtGrant";
			text = "Grant";
			x = 0.5 - (TOT_WIDTH / 2) + (2 * (BUTTON_WIDTH + SPACE));
		};
	};
	class Controls {
		class missionList : daControlGroup {
			idc = MIS_IDC;
			x = (0.5 - (TOT_WIDTH / 2));
			y = (0.5 - (TOT_HEIGHT / 2)) + (2 * (LINE_HEIGHT + SPACE)) - YOFFSET;
			w = MENU_WIDTH;
			h = TOT_HEIGHT - (4 * LINE_HEIGHT) - (5 * SPACE);
		};
		class tabButtons : daControlGroup {
			idc = TAB_IDC;
			x = (0.5 - (TOT_WIDTH / 2)) + MENU_WIDTH + SPACE;
			y = (0.5 - (TOT_HEIGHT / 2)) + LINE_HEIGHT + SPACE - YOFFSET;
			w = TOT_WIDTH - MENU_WIDTH - SPACE;
			h = LINE_HEIGHT;
			class Controls {
				class mapButton : asGrant_tabButton {
					idc = TAB_MAP;
					text = "Location";
					onMouseButtonClick = "_this call asgrant_fnc_evtTab";
				};
				class ordButton : asGrant_tabButton {
					idc = TAB_ORD;
					x = BUTTON_WIDTH + SPACE;
					text = "Ordonance";
					onMouseButtonClick = "_this call asgrant_fnc_evtTab";
				};
			};
		};
		class mapPanel : daMap {
			idc = MAP_IDC;
			x = (0.5 - (TOT_WIDTH / 2)) + MENU_WIDTH + SPACE;
			y = (0.5 - (TOT_HEIGHT / 2)) + (2 * (LINE_HEIGHT + SPACE)) - YOFFSET;
			h = TOT_HEIGHT - (3 * LINE_HEIGHT) - (3 * SPACE);
			w = TOT_WIDTH - MENU_WIDTH - SPACE;
		};
		class ordPanel : daControlGroup {
			idc = ORD_IDC;
			x = (0.5 - (TOT_WIDTH / 2)) + MENU_WIDTH + SPACE;
			y = (0.5 - (TOT_HEIGHT / 2)) + (2 * (LINE_HEIGHT + SPACE)) - YOFFSET;
			h = TOT_HEIGHT - (3 * LINE_HEIGHT) - (3 * SPACE);
			w = TOT_WIDTH - MENU_WIDTH - SPACE;
		};
	};
};
