
#include "define.hpp"

disableSerialization;

params [["_dsp", displayNull]];

if ( isNull _dsp ) then {
    waitUntil { !isNull (findDisplay AS_GRANT_IDD) };
    _dsp = (findDisplay AS_GRANT_IDD);
};

private _tabGrp = _dsp displayCtrl TAB_IDC;
private _lstGrp = _dsp displayCtrl MIS_IDC;

private _idc = MIS_BASE;
while { !isNull (_lstGrp controlsGroupCtrl _idc) } do {
    ctrlDelete (_lstGrp controlsGroupCtrl _idc);
    _idc = _idc + 1;
};

private _idc = MIS_BASE;
private _stack = [];
{
    _x params ["_misId", "_name", "_owner", "_lastEditBy", "_grid", "_cor", "_ord", "_mode", "_requestedBy", "_grantedBy", "_executedBy"];
    if ( !(isNull _requestedBy) && (isNull _grantedBy) ) then {
        if ( artiSupport_uiMission isEqualTo "" ) then { artiSupport_uiMission = _misId; };
		private _entry = _dsp ctrlCreate ['asGrant_misEntry', _idc, _lstGrp];
        (_entry controlsGroupCtrl MIS_ID) ctrlSetText str _misId;
        (_entry controlsGroupCtrl MIS_NAME) ctrlSetText _name;
        _stack pushback [_misId, (_entry controlsGroupCtrl MIS_NAME)];
		private _pos = ctrlPosition _entry;
    	_pos set [1, ((_idc - MIS_BASE) * (LINE_HEIGHT + SPACE))];
    	_entry ctrlSetPosition _pos;
    	_entry ctrlCommit 0;
		_idc = _idc + 1;
    };
} forEach artiSupport_missions;

private _found = false;
private _misIds = [];
{
    _x params ["_misId", "_button"];
    if ( artiSupport_uiMission isEqualTo _misId ) then {
        _found = true;
        _button ctrlEnable false;
    } else {
        _button ctrlEnable true;
    };
    _misIds pushback _misId;
} forEach _stack;

if ( !_found && ((count _stack) > 0) ) then {
	(_stack select 0) params ["_misId", "_button"];
	artiSupport_uiMission = _misId;
	_button ctrlEnable false;
};
_stack = nil;

private _missionIdx = -1;
{  
    if ( (artiSupport_uiMission isEqualto (_x select 0)) && ((_x select 0) in _misIds) ) then { _missionIdx = _forEachIndex; };
} forEach artiSupport_missions;

//cleanup map
private _mapGrp = _dsp displayCtrl MAP_IDC;
deleteMarkerLocal 'asGrant_circle';
deleteMarkerLocal 'asGrant_label';

//cleanup ord
private _ordGrp = _dsp displayCtrl ORD_IDC;
private _idc = ORD_BASE;
while { !isNull (_ordGrp controlsGroupCtrl _idc) } do {
    ctrlDelete (_ordGrp controlsGroupCtrl _idc);
    _idc = _idc + 1;
};

if ( _missionIdx < 0 ) exitWith {
    (_dsp displayCtrl GRANT_IDC) ctrlEnable false;
    (_dsp displayCtrl DENY_IDC) ctrlEnable false;
    _mapGrp ctrlShow false;
    (_tabGrp controlsGroupCtrl TAB_MAP) ctrlEnable false;
    _ordGrp ctrlShow false;
    (_tabGrp controlsGroupCtrl TAB_ORD) ctrlEnable false;
};

_mapGrp ctrlShow (artiSupport_uiTab isEqualTo 0);
(_tabGrp controlsGroupCtrl TAB_MAP) ctrlEnable !(artiSupport_uiTab isEqualTo 0);
_ordGrp ctrlShow !(artiSupport_uiTab isEqualTo 0);
(_tabGrp controlsGroupCtrl TAB_ORD) ctrlEnable (artiSupport_uiTab isEqualTo 0);

(artiSupport_missions select _missionIdx) params ["_misId", "_name", "_owner", "_lastEditBy", "_grid", "_cor", "_ord", "_mode", "_requestedBy", "_grantedBy", "_executedBy"];

private _spreads = [];
{
    _spreads pushback [(configName _x), getNumber(_x)];
} forEach configProperties [(missionConfigFile >> "settings" >> "artiSupport" >> "spreads"), "true", false];

private _idc = ORD_BASE;
private _bigestSpread = 0;
{
    _x params ["_tubeId", "_shell", "_amount", "_spread"];
    private _entry = _dsp ctrlCreate ['asGrant_ordEntry', _idc, _ordGrp];
    (_entry controlsGroupCtrl ORD_NAME) ctrlSetText ((artiSupport_tubes select _tubeId) select 1);   
    (_entry controlsGroupCtrl ORD_SHELL) ctrlSetText getText(configFile >> "cfgMagazines" >> _shell >> "displayName");
    (_entry controlsGroupCtrl ORD_AMOUNT) ctrlSetText str _amount;
    (_entry controlsGroupCtrl ORD_SPREAD) ctrlSetText ((_spreads select 0) select 0);
    {
        if ( _spread isEqualTo (_x select 1) ) exitWith {
    		(_entry controlsGroupCtrl ORD_SPREAD) ctrlSetText (_x select 0);            
        };
    } forEach _spreads;
    if ( _spread > _bigestSpread ) then { _bigestSpread = _spread; };
    private _pos = ctrlPosition _entry;
    _pos set [1, (_forEachIndex * LINE_HEIGHT)];
    _entry ctrlSetPosition _pos;
    _entry ctrlCommit 0;
    _idc = _idc + 1;
} forEach _ord;


_grid params ['_gridX', '_gridY', '_gridZ'];
_cor params ['_corX', '_corY'];

([(parseNumber _gridX), (parseNumber _gridY), _corX, _corY] call arti_fnc_gridToCoord) params ['_aimX', '_aimY'];

private _label = createMarkerLocal ['asGrant_label', [_aimX, _aimY, 0]];
_label setMarkerColorLocal 'ColorRed';
_label setMarkerTypeLocal "hd_dot";
_label setMarkerTextLocal format["%1 - %2", _name, (_requestedBy getVariable['MD_name', (name player)])];
if ( _bigestSpread > 0 ) then {
    private _circle = createMarkerLocal ['asGrant_circle', [_aimX, _aimY, 0]];
	_circle setMarkerShapeLocal 'ELLIPSE';
	_circle setMarkerBrushLocal 'Grid';
    _circle setMarkerColorLocal 'ColorRed';
    _circle setMarkerAlphaLocal 0.4;
	_circle setMarkerSizeLocal [_bigestSpread, _bigestSpread];  
};
_mapGrp ctrlMapAnimAdd [3, 0.1, [_aimX, _aimY, 0]];
ctrlMapAnimCommit _mapGrp;

(_dsp displayCtrl GRANT_IDC) ctrlEnable true;
(_dsp displayCtrl DENY_IDC) ctrlEnable true;
