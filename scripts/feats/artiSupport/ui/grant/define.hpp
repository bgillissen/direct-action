#define YOFFSET 0
#define SPACE 0.01
#define TOT_WIDTH 1.2
#define TOT_HEIGHT 1.2
#define LINE_HEIGHT (1 / 25)
#define MENU_WIDTH (6.25 / 20)
#define BUTTON_WIDTH (6.25 / 40)

#define AS_GRANT_IDD 50000

#define DENY_IDC 50001
#define GRANT_IDC 50002

#define MIS_IDC 51000
#define MIS_BASE 51100
#define MIS_ID 51001
#define MIS_NAME 51002

#define TAB_IDC 52000
#define TAB_MAP 52001
#define TAB_ORD 52002

#define MAP_IDC 53000

#define ORD_IDC 54000
#define ORD_BASE 54100
#define ORD_NAME 54001
#define ORD_SHELL 54002
#define ORD_AMOUNT 54003
#define ORD_SPREAD 54004
