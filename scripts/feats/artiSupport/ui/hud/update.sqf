
disableSerialization;

params [["_rscTitle", displayNull]];;

if ( isNull _rscTitle ) then {
    "artiSupporHUD" cutRsc ["artiSupportHUD", "PLAIN", 0.01, false];
    waitUntil {
        _rscTitle = uiNamespace getVariable ['artiSupportHUD', displayNull];
        !(isNull _rscTitle)
    };
};

private _count = 0;
{
    _x params  ["_misId", "_name", "_owner", "_lastEditBy", "_grid", "_cor", "_ord", "_mode", "_requestedBy", "_grantedBy", "_executedBy"];
    if ( !(isNull _requestedBy) && (isNull _grantedBy) ) then { _count = _count + 1; };
} forEach artiSupport_missions;

private _ctrl = _rscTitle displayCtrl 99999;
private _text = "";
if ( _count > 0 ) then {
    _text = ['artiSupport', 'messages', 'hud'] call core_fnc_getSetting;
    _text = format[_text, _count];
};

_ctrl ctrlSetStructuredText parseText _text;
_ctrl ctrlCommit 0;