
#define WIDTH ((6.25 / 40) * 4)
#define HEIGHT (1 / 20)
#define YOFFSET 0.08

class artiSupportHUD {
	idd = -1;
    fadeout = 0;
    fadein = 0;
    duration = 1e+1000;
	name= "artiSupportHUD";
	onLoad = "uiNamespace setVariable ['artiSupportHUD', (_this select 0)];";
	class controlsBackground {
		class container : daStructuredText {
			idc = 99999;
			x = 0.5 - (WIDTH / 2);
			y = SafeZoneY + YOFFSET;
			w = WIDTH;
			h = HEIGHT;
			shadow = 1;
		};
	};
};
