
#include "..\define.hpp"

disableSerialization;

params ["_scr"];

_scr ctrlShow false;

waitUntil { !(isNull (findDisplay AS_CTRL_IDD)) };

[_scr] call asCtrl_fnc_ordUpdate;

if !( ORD_IDC isEqualTo artiSupport_uiMenu ) exitWith {};

private _dsp = findDisplay AS_CTRL_IDD;
(_dsp displayCtrl AS_LOAD) ctrlShow false;
_scr ctrlShow true;