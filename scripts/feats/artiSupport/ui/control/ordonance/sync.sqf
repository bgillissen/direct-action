#include "..\define.hpp"

params [['_checkChanges', false]];

if ( artiSupport_uiNoEvents ) exitWith {};

disableSerialization;

private _dsp = findDisplay AS_CTRL_IDD;
private _scr = _dsp displayCtrl ORD_IDC;
private _list = _scr controlsGroupCtrl ORD_LIST;

private _ord = [];
private _idc = ORD_BASE;
 
while { !( isNull (_list controlsGroupCtrl _idc) ) } do {
	private _entry = _list controlsGroupCtrl _idc;
    private _do = true;
    if ( artiSupport_commanding ) then { _do = ( cbChecked (_entry controlsGroupCtrl ORD_SELECT) ); };
	if ( _do ) then {
		private _tubeId = parseNumber ctrlText (_entry controlsGroupCtrl ORD_ID);
		private _amount = parseNumber ctrlText (_entry controlsGroupCtrl ORD_AMOUNT);
		if ( _amount < 0 ) then { _amount = 0; };
		private _shell = (_entry controlsGroupCtrl ORD_SHELL) lbData (lbCurSel (_entry controlsGroupCtrl ORD_SHELL));
		private _spread = (_entry controlsGroupCtrl ORD_SPREAD) lbValue (lbCurSel (_entry controlsGroupCtrl ORD_SPREAD));
    	_ord pushback [_tubeId, _shell, _amount, _spread];
	};
	_idc = _idc + 1;
};

artiSupport_uiBuffer set [3, _ord];

if !( _checkChanges ) exitWith {};

call asCtrl_fnc_hasChanged;