
#include "..\..\define.hpp"

disableSerialization;

params ["_ctrl", "_state"];

if ( artiSupport_uiNoEvents ) exitWith {};
if !( artiSupport_commanding ) exitWith {};

_state = [false, true] select _state;
 
private _entry = ctrlParentControlsGroup _ctrl;
private _curTubeId = parseNumber ctrlText (_entry controlsGroupCtrl ORD_ID);

artiSupport_uiBuffer params ['_name', '_grid', '_cor', '_ord', '_mode'];

private _found = false;
{
    _x params ["_tubeId", "_shell", "_amount", "_spread"];
    if ( _tubeId isEqualTo _curTubeId ) then {
        _found = true;
        if !( _state ) exitWith { _ord deleteAt _forEachIndex; };
    };
} forEach _ord;

if !( _state ) exitWith { call asCtrl_fnc_hasChanged; };
if ( _state && _found ) exitWith {};

private _amount = parseNumber ctrlText (_entry controlsGroupCtrl ORD_AMOUNT);
if ( _amount < 0 ) then { _amount = 0; };
private _shellCtrl = (_entry controlsGroupCtrl ORD_SHELL);
private _shell = _shellCtrl lbData (lbCurSel _shellCtrl); 
private _spreadCtrl = (_entry controlsGroupCtrl ORD_SPREAD);
private _spread = _spreadCtrl lbValue (lbCurSel _spreadCtrl);

_ord pushback [_curTubeId, _shell, _amount, _spread];

artiSupport_uiBuffer set [3, _ord];

call asCtrl_fnc_hasChanged;