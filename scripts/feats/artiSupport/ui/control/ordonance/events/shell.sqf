
#include "..\..\define.hpp"

disableSerialization;

params ["_ctrl", "_idx"];

if ( artiSupport_uiNoEvents ) exitWith {};

private _entry = ctrlParentControlsGroup _ctrl;
private _ok = true;

if ( artiSupport_commanding ) then {
	_ok = ( cbChecked (_entry controlsGroupCtrl ORD_SELECT) );    
};

if !( _ok ) exitWith {};

private _curTubeId = parseNumber ctrlText (_entry controlsGroupCtrl ORD_ID);

artiSupport_uiBuffer params ['_name', '_grid', '_cor', '_ord', '_mode', '_hasChanged'];

{
    _x params ["_tubeId", "_shell", "_amount", "_spread"];
    if ( _tubeId isEqualTo _curTubeId ) exitWith {
    	(_ord select _forEachIndex) set [1, (_ctrl lbData _idx)];    
    };
} forEach _ord;

call asCtrl_fnc_hasChanged;