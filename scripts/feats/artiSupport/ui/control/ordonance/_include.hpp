
class asCtrl_ordEntry : daControlGroup {
	h = LINE_HEIGHT + SPACE;
	w = TOT_WIDTH - MENU_WIDTH - (3 * SPACE);
	class Controls {
		class id : daText {
			idc = ORD_ID;
			text = "-1";
			w = 0;
			h = 0;
		};
		class name : daText {
			idc = ORD_NAME;
			h = LINE_HEIGHT;
			w = (TOT_WIDTH - MENU_WIDTH - (3 * SPACE)) - (BUTTON_WIDTH * 3.5) - (3 * SPACE));
			text = "Tube name";
		};
		class shell : daCombo {
			idc = ORD_SHELL;
			x = (TOT_WIDTH - MENU_WIDTH - (3 * SPACE)) - (BUTTON_WIDTH * 3.5) - (2 * SPACE));
			h = LINE_HEIGHT;
			w = (BUTTON_WIDTH * 2);
			onLBSelChanged = "_this call asCtrl_fnc_ordEvtShell";
		};
		class amount : daEdit {
			idc = ORD_AMOUNT;
			x = (TOT_WIDTH - MENU_WIDTH - (3 * SPACE)) - (BUTTON_WIDTH * 1.5) - SPACE;
			h = LINE_HEIGHT;
			w = (BUTTON_WIDTH * 0.75);
			text = "1";
			onKillFocus = "_this call asCtrl_fnc_ordEvtAmount";
		};
		class spread : daCombo {
			idc = ORD_SPREAD;
			x = (TOT_WIDTH - MENU_WIDTH - (3 * SPACE)) - (BUTTON_WIDTH * 0.75);
			h = LINE_HEIGHT;
			w = (BUTTON_WIDTH * 0.75);
			onLBSelChanged = "_this call asCtrl_fnc_ordEvtSpread";
		};
	};
};

class asCtrl_ordCmdEntry : daControlGroup {
	h = LINE_HEIGHT + SPACE;
	w = TOT_WIDTH - MENU_WIDTH - (3 * SPACE);
	class Controls {
		class id : daText {
			idc = ORD_ID;
			text = "-1";
			w = 0;
			h = 0;
		};
		class select : daCheckBox {
			idc = ORD_SELECT;
			h = LINE_HEIGHT;
			w = LINE_HEIGHT;
			onCheckedChanged = "_this call asCtrl_fnc_ordEvtSelect;";
		};
		class name : daText {
			idc = ORD_NAME;
			x = LINE_HEIGHT;
			h = LINE_HEIGHT;
			w = (TOT_WIDTH - MENU_WIDTH - (3 * SPACE)) - LINE_HEIGHT - (BUTTON_WIDTH * 3.5) - (3 * SPACE));
			text = "Tube name";
		};
		class shell : daCombo {
			idc = ORD_SHELL;
			x = (TOT_WIDTH - MENU_WIDTH - (3 * SPACE)) - (BUTTON_WIDTH * 3.5) - (2 * SPACE));
			h = LINE_HEIGHT;
			w = (BUTTON_WIDTH * 2);
			onLBSelChanged = "_this call asCtrl_fnc_ordEvtShell";
		};
		class amount : daEdit {
			idc = ORD_AMOUNT;
			x = (TOT_WIDTH - MENU_WIDTH - (3 * SPACE)) - (BUTTON_WIDTH * 1.5) - SPACE;
			h = LINE_HEIGHT;
			w = (BUTTON_WIDTH * 0.75);
			text = "1";
			onKillFocus = "_this call asCtrl_fnc_ordEvtAmount";
		};
		class spread : daCombo {
			idc = ORD_SPREAD;
			x = (TOT_WIDTH - MENU_WIDTH - (3 * SPACE)) - (BUTTON_WIDTH * 0.75);
			h = LINE_HEIGHT;
			w = (BUTTON_WIDTH * 0.75);
			onLBSelChanged = "_this call asCtrl_fnc_ordEvtSpread";
		};
	};
};
