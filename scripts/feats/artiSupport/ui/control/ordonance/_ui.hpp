class nameTitle : daText {
	x = SPACE;
	h = LINE_HEIGHT;
	w = (BUTTON_WIDTH * 2);
	text = "Tube name";
};

class shellTitle : daText {
	x = (TOT_WIDTH - MENU_WIDTH - (3 * SPACE)) - (BUTTON_WIDTH * 3.5) - (2 * SPACE));
	h = LINE_HEIGHT;
	w = (BUTTON_WIDTH * 2);
	text = "Shell";
};

class amountTitle : daText {
	x = (TOT_WIDTH - MENU_WIDTH - (3 * SPACE)) - (BUTTON_WIDTH * 1.5) - SPACE;
	h = LINE_HEIGHT;
	w = (BUTTON_WIDTH * 0.75);
	text = "Amount";
};
class spreadTitle : daText {
	x = (TOT_WIDTH - MENU_WIDTH - (3 * SPACE)) - (BUTTON_WIDTH * 0.75);
	h = LINE_HEIGHT;
	w = (BUTTON_WIDTH * 0.75);
	text = "Spread";
};

class ordBcg : daContainer {
	x = SPACE;
	y = LINE_HEIGHT + SPACE;
	h = TOT_HEIGHT - (5 * LINE_HEIGHT) - (8 * SPACE);
	w = TOT_WIDTH - MENU_WIDTH - (3 * SPACE);
};

class ordList : daControlGroup {
	idc = ORD_LIST;
	x = SPACE;
	y = LINE_HEIGHT + SPACE;
	h = TOT_HEIGHT - (5 * LINE_HEIGHT) - (8 * SPACE);
	w = TOT_WIDTH - MENU_WIDTH - (3 * SPACE);
};
