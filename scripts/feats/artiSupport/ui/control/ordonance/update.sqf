
#include "..\define.hpp"

disableSerialization;

params [["_scr", controlNull]];

if ( isNull _scr ) then {
    waitUntil { !(isNull (findDisplay AS_CTRL_IDD)) };  
};

private _dsp = findDisplay AS_CTRL_IDD;
_scr = _dsp displayCtrl ORD_IDC;

if ( artiSupport_uiQ ) exitWith {};

if ( artiSupport_uiNoEvents ) then {
	artiSupport_uiQ = true;
	waitUntil { !artiSupport_uiNoEvents };
	artiSupport_uiQ = false;
};
    
artiSupport_uiNoEvents = true;

artiSupport_uiBuffer params ['_name', '_grid', '_cor', '_ord', '_mode'];

private _tubeIds = [];
{
	_x params ["_tubeId", "_shell", "_amount", "_spread"];
    _tubeIds pushbackUnique _tubeId;
} forEach _ord;

private _list = (_scr controlsGroupCtrl ORD_LIST);

private _idc = ORD_BASE;
while { !isNull (_list controlsGroupCtrl _idc) } do {
	ctrlDelete (_list controlsGroupCtrl _idc);
    _idc = _idc + 1;
};

private _spreads = [];
{
    _spreads pushback [(configName _x), getNumber(_x)];
} forEach configProperties [(missionConfigFile >> "settings" >> "artiSupport" >> "spreads"), "true", false];

private _c = 0;
{
    _x params ["_batId", "_name", "_veh", "_group", "_unit"];
    private _tubeId = _forEachIndex;
    private _bufferKey = _tubeIds find _tubeId;
    private ["_ok", "_class"];
    if ( artiSupport_commanding ) then {
        _class = "asCtrl_ordCmdEntry";
		_ok = ( _batId isEqualTo artiSupport_battery ); 
    } else {
        _class = "asCtrl_ordEntry";
        _ok = ( _bufferKey >= 0 );
    };
    if ( _ok ) then {
        private ['_shell', '_amount', '_spread'];
        if ( _bufferKey >= 0 ) then {
            _shell = (_ord select _bufferKey) select 1;
            _amount = (_ord select _bufferKey) select 2;
            _spread = (_ord select _bufferKey) select 3;
        } else {
            _shell = "";
            _amount = 1;
            _spread = 0;
        };
        private _ctrl = _dsp ctrlCreate [_class, (ORD_BASE + _c), _list]; 		
        (_ctrl controlsGroupCtrl ORD_ID) ctrlSetText (str _tubeId);
        (_ctrl controlsGroupCtrl ORD_NAME) ctrlSetText _name;
        (_ctrl controlsGroupCtrl ORD_AMOUNT) ctrlSetText (str _amount);
        if ( artiSupport_commanding ) then {
            (_ctrl controlsGroupCtrl ORD_SELECT) cbSetChecked (_bufferKey >= 0);
        };
        private _shellIdx = -1;
        private _shellCtrl = (_ctrl controlsGroupCtrl ORD_SHELL);
        {
            _shellCtrl lbAdd getText(configFile >> 'cfgMagazines' >> _x >> 'displayName');
            _shellCtrl lbSetData [_forEachIndex, _x];
            if ( _x isEqualTo _shell ) then { _shellIdx = _forEachIndex; };
        } forEach ([_veh] call arti_fnc_getMags);
        if ( _shellIdx < 0 ) then { _shellIdx = 0; };
        _shellCtrl lbSetCurSel _shellIdx;
        private _spreadIdx = -1;
        private _spreadCtrl = (_ctrl controlsGroupCtrl ORD_SPREAD);
        {
            _x params ["_dsp", "_val"];
            _spreadCtrl lbAdd _dsp;
            _spreadCtrl lbSetValue [_forEachIndex, _val];
            if ( _val isEqualTo _spread ) then { _spreadIdx = _forEachIndex; };
        } forEach _spreads;
        if ( _spreadIdx < 0 ) then { _spreadIdx = 0; };
        _spreadCtrl lbSetCurSel _spreadIdx;
        private _pos = ctrlPosition _ctrl;
        _pos set [1, (_c * (LINE_HEIGHT + SPACE))];
        _ctrl ctrlSetPosition _pos;
        _ctrl ctrlCommit 0;        
        _c = _c + 1;
    };
} forEach artiSupport_tubes;

artiSupport_uiNoEvents = false;
