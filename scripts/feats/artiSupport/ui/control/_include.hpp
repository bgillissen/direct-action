
#include "define.hpp"

class asCtrl_ScreenButton : daTxtButton {
	h = LINE_HEIGHT;
	w = BUTTON_WIDTH;
	y = (0.5 - (TOT_HEIGHT / 2)) + TOT_HEIGHT - LINE_HEIGHT - YOFFSET;
};

class asCtrl_Screen : daControlGroup {
	x = (0.5 - (TOT_WIDTH / 2)) + MENU_WIDTH + SPACE;
	y = (0.5 - (TOT_HEIGHT / 2)) + (3 * LINE_HEIGHT) + (5 * SPACE) - YOFFSET;
	h = TOT_HEIGHT - (3 * LINE_HEIGHT) - (5 * SPACE);
	w = TOT_WIDTH - MENU_WIDTH - SPACE;
};

#include "assets\_include.hpp"
#include "execute\_include.hpp"
#include "menu\_include.hpp"
#include "ordonance\_include.hpp"
