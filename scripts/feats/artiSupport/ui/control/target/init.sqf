
#include "..\define.hpp"

disableSerialization;

params ["_scr"];

_scr ctrlShow false;

private _xWay = _scr controlsGroupCtrl TRGT_XWAY;
_xWay lbAdd "East";
_xWay lbAdd "West";

private _yWay = _scr controlsGroupCtrl TRGT_YWAY;
_yWay lbAdd "North";
_yWay lbAdd "South";

[_scr] call asCtrl_fnc_trgtUpdate;

if !( TRGT_IDC isEqualTo artiSupport_uiMenu ) exitWith {};

waitUntil { !(isNull (findDisplay AS_CTRL_IDD)) };
private _dsp = findDisplay AS_CTRL_IDD; 
(_dsp displayCtrl AS_LOAD) ctrlShow false;
_scr ctrlShow true;
