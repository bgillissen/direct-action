
#include "..\define.hpp"

disableSerialization;

params [["_scr", controlNull]];

if ( isNull _scr ) then {
    waitUntil { !(isNull (findDisplay AS_CTRL_IDD)) };
};

private _dsp = findDisplay AS_CTRL_IDD;
_scr = _dsp displayCtrl TRGT_IDC;    

if ( artiSupport_uiQ ) exitWith {};

if ( artiSupport_uiNoEvents ) then {
	artiSupport_uiQ = true;
	waitUntil { !artiSupport_uiNoEvents };
	artiSupport_uiQ = false;
};

artiSupport_uiNoEvents = true;

artiSupport_uiBuffer params ['_name', '_grid', '_cor', '_ord', '_mode'];

_grid params ['_gridX', '_gridY', '_gridZ'];
(_scr controlsGroupCtrl TRGT_GRIDX) ctrlSetText _gridX;
(_scr controlsGroupCtrl TRGT_GRIDY) ctrlSetText _gridY;
(_scr controlsGroupCtrl TRGT_GRIDZ) ctrlSetText _gridZ;

_cor params ['_corX', '_corY'];
(_scr controlsGroupCtrl TRGT_CORX) ctrlSetText str abs _corX;
(_scr controlsGroupCtrl TRGT_XWAY) lbSetCurSel ([0, 1] select (_corX < 0));
(_scr controlsGroupCtrl TRGT_CORY) ctrlSetText str abs _corY;
(_scr controlsGroupCtrl TRGT_YWAY) lbSetCurSel ([0, 1] select (_corY < 0));

artiSupport_uiNoEvents = false;