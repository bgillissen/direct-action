
#define COR_LINE_OFFSET 1

class trgtGridTitle : daText {
	style = ST_CENTER;
	x = ((TOT_WIDTH - MENU_WIDTH - SPACE) / 2) - BUTTON_WIDTH;
	y = SPACE;
	h = LINE_HEIGHT;
	w = (BUTTON_WIDTH * 2);
	text = "Target's location";
};
class trgtGridContainer : daContainer {
	x = ((TOT_WIDTH - MENU_WIDTH - SPACE) / 2) - (((3 * BUTTON_WIDTH) + (4 * SPACE)) / 2);
	y = LINE_HEIGHT + (2 * SPACE);
	h = (2 * LINE_HEIGHT) + (3 * SPACE);
	w = (3 * BUTTON_WIDTH) + (4 * SPACE);
};

class trgtGridXTitle : daText {
	style = ST_CENTER;
	x = ((TOT_WIDTH - MENU_WIDTH - SPACE) / 2) - (((3 * BUTTON_WIDTH) + (4 * SPACE)) / 2) + SPACE;
	y = LINE_HEIGHT + (3 * SPACE);
	h = LINE_HEIGHT;
	w = BUTTON_WIDTH;
	text = "Latitude";
};

class trgtGridX : daEdit {
	idc = TRGT_GRIDX;
	x = ((TOT_WIDTH - MENU_WIDTH - SPACE) / 2) - (((3 * BUTTON_WIDTH) + (4 * SPACE)) / 2) + SPACE;
	y = (2 * LINE_HEIGHT) + (4 * SPACE);
	h = LINE_HEIGHT;
	w = BUTTON_WIDTH;
	tooltip = "5 digits max";
	onKillFocus = "[true] call asCtrl_fnc_trgtSync";
};

class trgtGridYTitle : daText {
	style = ST_CENTER;
	x = ((TOT_WIDTH - MENU_WIDTH - SPACE) / 2) - (((3 * BUTTON_WIDTH) + (4 * SPACE)) / 2) + (2 * SPACE) + BUTTON_WIDTH;
	y = LINE_HEIGHT + (3 * SPACE);
	h = LINE_HEIGHT;
	w = BUTTON_WIDTH;
	text = "Longitude";
};

class trgtGridY : daEdit {
	idc = TRGT_GRIDY;
	x = ((TOT_WIDTH - MENU_WIDTH - SPACE) / 2) - (((3 * BUTTON_WIDTH) + (4 * SPACE)) / 2) + (2 * SPACE) + BUTTON_WIDTH;
	y = (2 * LINE_HEIGHT) + (4 * SPACE);
	h = LINE_HEIGHT;
	w = BUTTON_WIDTH;
	tooltip = "5 digits max";
	onKillFocus = "[true] call asCtrl_fnc_trgtSync";
};

class trgtGridZTitle : daText {
	style = ST_CENTER;
	x = ((TOT_WIDTH - MENU_WIDTH - SPACE) / 2) - (((3 * BUTTON_WIDTH) + (4 * SPACE)) / 2) + (3 * SPACE) + (2 * BUTTON_WIDTH);
	y = LINE_HEIGHT + (3 * SPACE);
	h = LINE_HEIGHT;
	w = BUTTON_WIDTH;
	text = "Altitude";
};

class trgtGridZ : daEdit {
	idc = TRGT_GRIDZ;
	x = ((TOT_WIDTH - MENU_WIDTH - SPACE) / 2) - (((3 * BUTTON_WIDTH) + (4 * SPACE)) / 2) + (3 * SPACE) + (2 * BUTTON_WIDTH);
	y = (2 * LINE_HEIGHT) + (4 * SPACE);
	h = LINE_HEIGHT;
	w = BUTTON_WIDTH;
	onKillFocus = "[true] call asCtrl_fnc_trgtSync";
};


class trgtCorTitle : daText {
	style = ST_CENTER;
	x = ((TOT_WIDTH - MENU_WIDTH - SPACE) / 2) - BUTTON_WIDTH;
	y = ((2 + COR_LINE_OFFSET) * LINE_HEIGHT) + (10 * SPACE);
	h = LINE_HEIGHT;
	w = (BUTTON_WIDTH * 2);
	text = "Corrections (in meters)";
};
class trgtCorContainer : daContainer {
	x = ((TOT_WIDTH - MENU_WIDTH - SPACE) / 2) - (((3.4 * BUTTON_WIDTH) + (6 * SPACE)) / 2);
	y = ((3 + COR_LINE_OFFSET) * LINE_HEIGHT) + (11 * SPACE);
	h = (2 * LINE_HEIGHT) + (2 * SPACE);
	w = (3.4 * BUTTON_WIDTH) + (6 * SPACE);
};

class trgtCorXTitle : daText {
	style = ST_CENTER;
	text = "Latitude";
	x = ((TOT_WIDTH - MENU_WIDTH - SPACE) / 2) - (((3.4 * BUTTON_WIDTH) + (6 * SPACE)) / 2) + SPACE + ((((1.7 * BUTTON_WIDTH) + SPACE)) / 2) - (BUTTON_WIDTH / 2);
	y = ((3 + COR_LINE_OFFSET) * LINE_HEIGHT) + (11 * SPACE);
	h = LINE_HEIGHT;
	w = BUTTON_WIDTH;
};

class trgtCorXWay : daCombo {
	idc = TRGT_XWAY;
	x = ((TOT_WIDTH - MENU_WIDTH - SPACE) / 2) - (((3.4 * BUTTON_WIDTH) + (6 * SPACE)) / 2) + SPACE;
	y = ((4 + COR_LINE_OFFSET) * LINE_HEIGHT) + (12 * SPACE);
	h = LINE_HEIGHT;
	w = (BUTTON_WIDTH * 0.7);
	onLBSelChanged = "[true] call asCtrl_fnc_trgtSync";
};

class trgtCorX : daEdit {
	idc = TRGT_CORX;
	x = ((TOT_WIDTH - MENU_WIDTH - SPACE) / 2) - (((3.4 * BUTTON_WIDTH) + (6 * SPACE)) / 2) + (BUTTON_WIDTH * 0.7) + (2 * SPACE);
	y = ((4 + COR_LINE_OFFSET) * LINE_HEIGHT) + (12 * SPACE);
	h = LINE_HEIGHT;
	w = BUTTON_WIDTH;
	tooltip = "max 100m";
	onKillFocus = "[true] call asCtrl_fnc_trgtSync";
};
class trgtCorYTitle : daText {
	style = ST_CENTER;
	text = "Longitude";
	x = ((TOT_WIDTH - MENU_WIDTH - SPACE) / 2) - (((3.4 * BUTTON_WIDTH) + (6 * SPACE)) / 2) + (1.7 * BUTTON_WIDTH) + (4 * SPACE) + ((((1.7 * BUTTON_WIDTH) + SPACE)) / 2) - (BUTTON_WIDTH / 2);
	y = ((3 + COR_LINE_OFFSET) * LINE_HEIGHT) + (11 * SPACE);
	h = LINE_HEIGHT;
	w = BUTTON_WIDTH;
};
class trgtCorYWay : daCombo {
	idc = TRGT_YWAY;
	x = ((TOT_WIDTH - MENU_WIDTH - SPACE) / 2) - (((3.4 * BUTTON_WIDTH) + (6 * SPACE)) / 2) + (1.7 * BUTTON_WIDTH) + (4 * SPACE);
	y = ((4 + COR_LINE_OFFSET) * LINE_HEIGHT) + (12 * SPACE);
	h = LINE_HEIGHT;
	w = (BUTTON_WIDTH * 0.7);
	onLBSelChanged = "[true] call asCtrl_fnc_trgtSync";
};

class trgtCorY : daEdit {
	idc = TRGT_CORY;
	x = ((TOT_WIDTH - MENU_WIDTH - SPACE) / 2) - (((3.4 * BUTTON_WIDTH) + (6 * SPACE)) / 2) + (2.4 * BUTTON_WIDTH) + (5 * SPACE);
	y = ((4 + COR_LINE_OFFSET) * LINE_HEIGHT) + (12 * SPACE);
	h = LINE_HEIGHT;
	w = BUTTON_WIDTH;
	tooltip = "max 100m";
	onKillFocus = "[true] call asCtrl_fnc_trgtSync";
};
