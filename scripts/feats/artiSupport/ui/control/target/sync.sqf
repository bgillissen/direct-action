
#include "..\define.hpp"

params [['_checkChanges', false]];

if ( artiSupport_uiNoEvents ) exitWith {};

disableSerialization;

private _dsp = findDisplay AS_CTRL_IDD;
private _scr = _dsp displayCtrl TRGT_IDC;

private _gridX = ctrlText (_scr controlsGroupCtrl TRGT_GRIDX);
private _gridY = ctrlText (_scr controlsGroupCtrl TRGT_GRIDY);
private _gridZ = ctrlText (_scr controlsGroupCtrl TRGT_GRIDZ);

private _corX = parseNumber ctrlText (_scr controlsGroupCtrl TRGT_CORX);
private _xWay = ((lbCurSel (_scr controlsGroupCtrl TRGT_XWAY)) > 0);
if ( _corX > 100 ) then { _corX = 100; };
if ( _corX < 0 ) then { _xWay = !_xWay; };
if ( _xWay ) then { _corX = _corX * -1; };
_xWay = ([0,1] select _xWay);

private _corY = parseNumber ctrlText (_scr controlsGroupCtrl TRGT_CORY);
private _yWay = ((lbCurSel (_scr controlsGroupCtrl TRGT_YWAY)) > 0);
if ( _corY > 100 ) then { _corY = 100; };
if ( _corY < 0 ) then { _yWay = !_yWay; };
if ( _yWay ) then { _corY = _corY * -1; };
_yWay = ([0,1] select _yWay);

artiSupport_uiBuffer set [1, [_gridX, _gridY, _gridZ]];
artiSupport_uiBuffer set [2, [_corX, _corY]];

artiSupport_uiNoEvents = true;

(_scr controlsGroupCtrl TRGT_CORX) ctrlSetText (str abs _corX);
(_scr controlsGroupCtrl TRGT_XWAY) lbSetCurSel _xWay;
(_scr controlsGroupCtrl TRGT_CORY) ctrlSetText (str abs _corY);
(_scr controlsGroupCtrl TRGT_YWAY) lbSetCurSel _yWay;

artiSupport_uiNoEvents = false;

if !( _checkChanges ) exitWith {};

call asCtrl_fnc_hasChanged;