
#include "define.hpp"

params [["_missionIdx", -1]];

if ( _missionIdx < 0 ) then {
	{
		if ( (_x select 0) isEqualTo artiSupport_uiMission ) exitWith { _missionIdx = _forEachIndex; };
	} forEach artiSupport_missions;
};

if ( _missionIdx < 0 ) exitWith { false };

(artiSupport_missions select _missionIdx) params ["_misId", "_name", "_owner", "_lastEditBy", "_grid", "_cor", "_ord", "_mode", "_requestedBy", "_grantedBy", "_executedBy"];

 private _ordCp = [];
{ _ordCp pushback (_x + []); } forEach _ord;

artiSupport_uiBuffer = [_name, (_grid + []), (_cor + []), _ordCp, (_mode + [])];

[(findDisplay AS_CTRL_IDD)] call asCtrl_fnc_update;

true