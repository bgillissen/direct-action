
#include "..\define.hpp"

disableSerialization;

params ["_ctrl"];

waitUntil { !(isNull (findDisplay AS_CTRL_IDD)) };

private _dsp = findDisplay AS_CTRL_IDD;

private _entries = [];
private _height = 0; 
if ( artiSupport_commanding ) then {
    _entries = [[MENU_TRGT, "Target"], [MENU_ORD, 'Ordonance'], [MENU_EXE, 'Execution']];
    _height = ((TOT_HEIGHT - (4 * LINE_HEIGHT) - (6 * SPACE)) / 3) - ((2 * SPACE) / 3);
} else {
    _entries = [[MENU_TRGT, "Target"], [MENU_ASS, 'Assets'], [MENU_ORD, 'Ordonance'], [MENU_EXE, 'Execution']];
    _height = ((TOT_HEIGHT - (4 * LINE_HEIGHT) - (6 * SPACE)) / 4) - ((3 * SPACE) / 4);
};

private _ok = false;
private _buttons = [];
{
    _x params ["_idc", "_text"];
    private _target = ((_idc - MENU_IDC) * 1000) + AS_CTRL_IDD;
    if ( artiSupport_uiMenu isEqualTo _target ) then { _ok = true; };
	private _menuButton = _dsp ctrlCreate ["asCtrl_MenuButton", _idc, _ctrl];
    _menuButton ctrlSetText _text;
    private _pos = ctrlPosition _menuButton;
    _pos set [1, ((_height * _forEachIndex) + (SPACE * _forEachIndex))];
    _pos set [3, _height];
    _menuButton ctrlSetPosition _pos;
    _menuButton ctrlCommit 0;
    _buttons pushback [_menuButton, _target];
} forEach _entries;

if !( _ok ) then { artiSupport_uiMenu = TRGT_IDC; };

{
    _x params ["_button", "_target"];
    _button ctrlEnable !( _target isEqualTo artiSupport_uiMenu );
} forEach _buttons;