
#include "..\define.hpp"

disableSerialization;

params ["_ctrl"];

private _target = (((ctrlIDC _ctrl) - MENU_IDC) * 1000) + AS_CTRL_IDD;
private _dsp = findDisplay AS_CTRL_IDD;
private _menuGrp = ctrlParentControlsGroup _ctrl;

private _entries = [];
if ( artiSupport_commanding ) then {
    _entries = [MENU_TRGT, MENU_ORD, MENU_EXE];
} else {
    _entries = [MENU_TRGT, MENU_ASS, MENU_ORD, MENU_EXE];
};

artiSupport_uiMenu = _target;

{
    private _curTarget = ((_x - MENU_IDC) * 1000) + AS_CTRL_IDD;
    private _active = ( _curTarget isEqualTo _target );
	(_menuGrp controlsGroupCtrl _x) ctrlEnable !_active;
	(_dsp displayCtrl _curTarget) ctrlShow _active;
} forEach _entries;