
class menuTrgt : asMenuButton {
	idc = MENU_TRGT;
	text = "Target";
};

class menuAss : asMenuButton {
idc = MENU_ASS;
	text = "Assets";
	y = SPACE + (((TOT_HEIGHT - (4 * LINE_HEIGHT) - (6 * SPACE)) / 4) - ((3* SPACE) / 4));

};

class menuOrd : asMenuButton {
	idc = MENU_ORD;
	text = "Ordonance";
	y = (2 * SPACE) + (2 * (((TOT_HEIGHT - (4 * LINE_HEIGHT) - (6 * SPACE)) / 4) - ((3 * SPACE) / 4)));
};

class menuExe : asMenuButton {
	idc = MENU_EXE;
	text = "Execution";
	y = (3 * SPACE) + (3 * (((TOT_HEIGHT - (4 * LINE_HEIGHT) - (6 * SPACE)) / 4) - ((3 * SPACE) / 4)));
};
