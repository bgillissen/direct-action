
class misBackgr : daContainer {
	x = 0;
	y = 0;
	h = (2 * LINE_HEIGHT) + (3 * SPACE);
	w = TOT_WIDTH;
};

class misSelect : daCombo {
	idc = MIS_SEL;
	onLBSelChanged = "_this call asCtrl_fnc_misEvtSelect";
	x = SPACE;
	y = SPACE;
	h = LINE_HEIGHT;
	w = TOT_WIDTH - (2 * BUTTON_WIDTH) - (4 * SPACE);
};

class misClone : daTxtButton {
	idc = MIS_CLONE;
	text = "Clone";
	action = "_this call asCtrl_fnc_misEvtClone";
	x = TOT_WIDTH - (2 * BUTTON_WIDTH) - (2 * SPACE);
	y = SPACE;
	h = LINE_HEIGHT;
	w = BUTTON_WIDTH;
};

class misRemove : daTxtButton {
	idc = MIS_REMOVE;
	text = "Remove";
	action = "_this call asCtrl_fnc_misEvtRemove";
	x = TOT_WIDTH - BUTTON_WIDTH - SPACE;
	y = SPACE;
	h = LINE_HEIGHT;
	w = BUTTON_WIDTH;
};

class misName : daEdit {
	idc = MIS_NAME;
	x = SPACE;
	y = LINE_HEIGHT + (2 * SPACE);
	h = LINE_HEIGHT;
	w = TOT_WIDTH - (2 * BUTTON_WIDTH) - (4 * SPACE);
	text="Fire Mission";
	onKillFocus = "[true] call asCtrl_fnc_misSync";
};

class misApply : daTxtButton {
	idc = MIS_APPLY;
	text = "";
	action = "_this call asCtrl_fnc_apply";
	x = TOT_WIDTH - (2 * BUTTON_WIDTH) - (2 * SPACE);
	y = LINE_HEIGHT + (2 * SPACE);
	h = LINE_HEIGHT;
	w = BUTTON_WIDTH;
};

class misReset : daTxtButton {
	idc = MIS_RESET;
	text = "Reset";
	action = "_this call asCtrl_fnc_reset";
	x = TOT_WIDTH - BUTTON_WIDTH - SPACE;
	y = LINE_HEIGHT + (2 * SPACE);
	h = LINE_HEIGHT;
	w = BUTTON_WIDTH;
};
