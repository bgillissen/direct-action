
#include "..\define.hpp"

disableSerialization;

params [["_scr", controlNull]];

if ( isNull _scr ) then {
    waitUntil { !(isNull (findDisplay AS_CTRL_IDD)) };
    private _dsp = findDisplay AS_CTRL_IDD;
	_scr = _dsp displayCtrl MIS_IDC;  
};

if ( artiSupport_uiQ ) exitWith {};

if ( artiSupport_uiNoEvents ) then {
	artiSupport_uiQ = true;
	waitUntil { !artiSupport_uiNoEvents };
	artiSupport_uiQ = false;
};

artiSupport_uiNoEvents = true;

private _combo = _scr controlsGroupCtrl MIS_SEL;
private _isNew = true;

lbClear _combo; 

_combo lbAdd "New";
_combo lbSetData [0, ""];

{
	_x params ["_misId", "_name", "_owner", "_lastEditBy", "_grid", "_cor", "_ord", "_mode", "_requestedBy", "_grantedBy", "_executedBy"];
    if ( _owner isEqualTo _lastEditBy ) then {
    	_combo lbAdd format["%1 (%2)", _name, _owner];
	} else {
        _combo lbAdd format["%1 (%2, %3)", _name, _owner, _lastEditBy];
    };
	_combo lbSetData [(_forEachIndex + 1), _misId];
    if ( _misId isEqualTo artiSupport_uiMission ) then {
        _isNew = false;
        _combo lbSetCurSel (_forEachIndex + 1);
    };
} forEach artiSupport_missions;

if ( _isNew ) then {
    artiSupport_uiMission = ""; 
	_combo lbSetCurSel 0;
};

(_scr controlsGroupCtrl MIS_NAME) ctrlSetText (artiSupport_uiBuffer select 0);

artiSupport_uiNoEvents = false;