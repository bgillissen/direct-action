
#include "..\define.hpp"

params [['_checkChanges', false]];

if ( artiSupport_uiNoEvents ) exitWith {};

disableSerialization;

private _dsp = findDisplay AS_CTRL_IDD;
private _scr = _dsp displayCtrl MIS_IDC;

private _name = ctrlText (_scr controlsGroupCtrl MIS_NAME);

artiSupport_uiBuffer set [0, _name];

if !( _checkChanges ) exitWith {};

call asCtrl_fnc_hasChanged;