
#include "..\..\define.hpp"

disableSerialization;

private _dsp = findDisplay AS_CTRL_IDD;
private _scr = _dsp displayCtrl MIS_IDC;

private _missionIdx = -1;
{
	if ( (_x select 0) isEqualTo artiSupport_uiMission ) exitWith { _missionIdx = _forEachIndex; };
} forEach artiSupport_missions;

if ( _missionIdx < 0 ) exitWith {};

artiSupport_missions deleteAt _missionIdx;

publicVariable "artiSupport_missions";

[_dsp] call asCtrl_fnc_update;