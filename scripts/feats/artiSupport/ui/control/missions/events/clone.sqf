
#include "..\..\define.hpp"

disableSerialization;

private _dsp = findDisplay AS_CTRL_IDD;
private _scr = _dsp displayCtrl MIS_IDC;

private _missionIdx = -1;
{
	if ( (_x select 0) isEqualTo artiSupport_uiMission ) exitWith { _missionIdx = _forEachIndex; };
} forEach artiSupport_missions;

if ( _missionIdx < 0 ) exitWith {};

private _misData = ((artiSupport_missions select _missionIdx) + []);

artiSupport_uiMission = str time;
 
_misData set [0, artiSupport_uiMission];
_misData set [1, ([(_misData select 1) + " - copy", artiSupport_missions, {_this select 1}] call common_fnc_getUniqueName)];
_misData set [2, (name player)];
_misData set [3, (name player)];

artiSupport_missions pushback _misData;

publicVariable "artiSupport_missions";

[_dsp] call asCtrl_fnc_update;