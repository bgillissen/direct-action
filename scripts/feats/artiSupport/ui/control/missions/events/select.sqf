
#include "..\..\define.hpp"

disableSerialization;

params ["_ctrl", "_idx"];

if ( artiSupport_uiNoEvents ) exitWith {};

private _misId = (_ctrl lbData _idx);
private _scr = ctrlParentControlsGroup _ctrl;

private _missionIdx = -1;
{
	if ( (_x select 0) isEqualTo _misId ) exitWith { _missionIdx = _forEachIndex; };
} forEach artiSupport_missions;

if ( _missionIdx >= 0 ) then {
	artiSupport_uiMission = _misId;
    [_missionIdx] call asCtrl_fnc_reset;
} else {
    artiSupport_uiMission = "";
	private _name = ["Fire Mission", artiSupport_missions, {_this select 1}] call common_fnc_getUniqueName;
	artiSupport_uiBuffer = [_name, ["","",""], [0,0], [], [0,0]];
	[(findDisplay AS_CTRL_IDD)] call asCtrl_fnc_update;
};