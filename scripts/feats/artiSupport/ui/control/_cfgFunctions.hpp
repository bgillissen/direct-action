class asCtrl {
	tag = "asCtrl";
	class functions {
		class init { file="feats\artiSupport\ui\control\init.sqf"; };
		class hasChanged { file="feats\artiSupport\ui\control\hasChanged.sqf"; };
		class update { file="feats\artiSupport\ui\control\update.sqf"; };
		class destroy { file="feats\artiSupport\ui\control\destroy.sqf"; };
		class apply { file="feats\artiSupport\ui\control\apply.sqf"; };
		class reset { file="feats\artiSupport\ui\control\reset.sqf"; };

		class assInit { file="feats\artiSupport\ui\control\assets\init.sqf"; };
		class assUpdate { file="feats\artiSupport\ui\control\assets\update.sqf"; };
		class assEvtMouseOver { file="feats\artiSupport\ui\control\assets\events\mouseOver.sqf"; };
		class assEvtMouseOut { file="feats\artiSupport\ui\control\assets\events\mouseOut.sqf"; };
		class assEvtbatChange { file="feats\artiSupport\ui\control\assets\events\batChange.sqf"; };
		class assEvtbatSelect { file="feats\artiSupport\ui\control\assets\events\batSelect.sqf"; };
		class assEvtTubChange { file="feats\artiSupport\ui\control\assets\events\tubChange.sqf"; };

		class exeInit { file="feats\artiSupport\ui\control\execute\init.sqf"; };
		class exeDestroy { file="feats\artiSupport\ui\control\execute\destroy.sqf"; };
		class exeTubeShell { file="feats\artiSupport\ui\control\execute\tubeShell.sqf"; };
		class exeTubeUpdate { file="feats\artiSupport\ui\control\execute\tubeUpdate.sqf"; };
		class exeUpdate { file="feats\artiSupport\ui\control\execute\update.sqf"; };
		class exeSync { file="feats\artiSupport\ui\control\execute\sync.sqf"; };

		class exeEvtExec { file="feats\artiSupport\ui\control\execute\events\exec.sqf"; };
		class exeEvtFire { file="feats\artiSupport\ui\control\execute\events\fire.sqf"; };
		class exeEvtPlot { file="feats\artiSupport\ui\control\execute\events\plot.sqf"; };
		class exeEvtPrep { file="feats\artiSupport\ui\control\execute\events\prep.sqf"; };
		class exeEvtToggle { file="feats\artiSupport\ui\control\execute\events\toggle.sqf"; };
		class exeEvtRequest { file="feats\artiSupport\ui\control\execute\events\request.sqf"; };
		class exeEvtAbord { file="feats\artiSupport\ui\control\execute\events\abord.sqf"; };

		class misInit { file="feats\artiSupport\ui\control\missions\init.sqf"; };
		class misSync { file="feats\artiSupport\ui\control\missions\sync.sqf"; };
		class misUpdate { file="feats\artiSupport\ui\control\missions\update.sqf"; };
		class misEvtClone { file="feats\artiSupport\ui\control\missions\events\clone.sqf"; };
		class misEvtRemove { file="feats\artiSupport\ui\control\missions\events\remove.sqf"; };
		class misEvtSelect { file="feats\artiSupport\ui\control\missions\events\select.sqf"; };

		class ordInit { file="feats\artiSupport\ui\control\ordonance\init.sqf"; };
		class ordSync { file="feats\artiSupport\ui\control\ordonance\sync.sqf"; };
		class ordUpdate { file="feats\artiSupport\ui\control\ordonance\update.sqf"; };
		class ordEvtAmount { file="feats\artiSupport\ui\control\ordonance\events\amount.sqf"; };
		class ordEvtSelect { file="feats\artiSupport\ui\control\ordonance\events\select.sqf"; };
		class ordEvtShell { file="feats\artiSupport\ui\control\ordonance\events\shell.sqf"; };
		class ordEvtSpread { file="feats\artiSupport\ui\control\ordonance\events\spread.sqf"; };

		class menuInit { file="feats\artiSupport\ui\control\menu\init.sqf"; };
		class menuSwitch { file="feats\artiSupport\ui\control\menu\switch.sqf"; };

		class trgtInit { file="feats\artiSupport\ui\control\target\init.sqf"; };
		class trgtSync { file="feats\artiSupport\ui\control\target\sync.sqf"; };
		class trgtUpdate { file="feats\artiSupport\ui\control\target\update.sqf"; };
	};
};
