
#include "define.hpp"

disableSerialization;

if ( artiSupport_uiBuffer isEqualTo [] ) then {
    private _name = ["Fire Mission", artiSupport_missions, {_this select 1}] call common_fnc_getUniqueName;
    artiSupport_uiBuffer = [_name, ["","",""], [0,0], [], [0, 0]];
};

{
	_x params ["_misId", "_name", "_owner", "_lastEditBy", "_grid", "_cor", "_ord", "_mode", "_requestedBy", "_grantedBy", "_executedBy"];
    if ( _misId isEqualTo artiSupport_uiMission ) then {
        private _ordCp = [];
        { _ordCp pushback (_x + []); } forEach _ord;
        artiSupport_uiBuffer = [_name, (_grid + []), (_cor + []), _ordCp, (_mode + [])];
    };
} forEach artiSupport_missions;

waitUntil { !(isNull (findDisplay AS_CTRL_IDD)) };

private _dsp = findDisplay AS_CTRL_IDD;
private _threads = [];

_threads pushback ([(_dsp displayCtrl MIS_IDC)] spawn asCtrl_fnc_misInit);
_threads pushback ([(_dsp displayCtrl MENU_IDC)] spawn asCtrl_fnc_menuInit);
_threads pushback ([(_dsp displayCtrl TRGT_IDC)] spawn asCtrl_fnc_trgtInit);
_threads pushback ([(_dsp displayCtrl ASS_IDC)] spawn asCtrl_fnc_assInit);	
_threads pushback ([(_dsp displayCtrl ORD_IDC)] spawn asCtrl_fnc_ordInit);
_threads pushback ([(_dsp displayCtrl EXE_IDC)] spawn asCtrl_fnc_exeInit);

waitUntil {
    ( ({ !isNull _x } count _threads) isEqualTo 0 )
};

call asCtrl_fnc_hasChanged;

if !( artiSupport_commanding ) then {
	artiSupport_cmdPVEH = ['artiSupport_commanders', { [(findDisplay AS_CTRL_IDD)] spawn asCtrl_fnc_update; }] call pveh_fnc_add;
};

if !( MOD_tfar ) exitWith {};

_dsp displayAddEventHandler ["KeyDown", "[_this, 'keydown'] call TFAR_fnc_processCuratorKey"];
_dsp displayAddEventHandler ["KeyUp", "[_this, 'keyup'] call TFAR_fnc_processCuratorKey"];
_dsp displayAddEventHandler ["keyUp", "_this call TFAR_fnc_onSwTangentReleasedHack"];
_dsp displayAddEventHandler ["keyDown", "_this call TFAR_fnc_onSwTangentPressedHack"];
_dsp displayAddEventHandler ["keyUp", "_this call TFAR_fnc_onLRTangentReleasedHack"];
_dsp displayAddEventHandler ["keyUp", "_this call TFAR_fnc_onDDTangentReleasedHack"];