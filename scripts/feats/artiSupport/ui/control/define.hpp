
#define YOFFSET 0
#define SPACE 0.01
#define TOT_WIDTH 1.2
#define TOT_HEIGHT 1.2
#define LINE_HEIGHT (1 / 25)
#define MENU_WIDTH (6.25 / 40)
#define BUTTON_WIDTH (6.25 / 40)
#define SHELL_HEIGHT_FACTOR 0.85
#define SHELL_SHOWN_AFTER_IMPACT -1.5

#define AS_CTRL_IDD 60000
#define AS_LOAD 60001

#define MIS_IDC 61000
#define MIS_SEL 61001
#define MIS_CLONE 61002
#define MIS_REMOVE 61003
#define MIS_NAME 61004
#define MIS_APPLY 61005
#define MIS_RESET 61006

#define MENU_IDC 62000
#define MENU_TRGT 62003
#define MENU_ASS 62004
#define MENU_ORD 62005
#define MENU_EXE 62006

#define TRGT_IDC 63000
#define TRGT_APPLY 63001
#define TRGT_RESET 63002
#define TRGT_GRIDX 63003
#define TRGT_GRIDY 63004
#define TRGT_GRIDZ 63005
#define TRGT_CORX 63006
#define TRGT_XWAY 63016
#define TRGT_CORY 63007
#define TRGT_YWAY 63017

#define ASS_IDC 64000
#define ASS_APPLY 64001
#define ASS_RESET 64002
#define ASS_BATL 64003
#define ASS_BATS 64100
#define ASS_TUBT 64004
#define ASS_TUBL 64005
#define ASS_TUBS 64500
#define ASS_BCG 64006
#define ASS_CKB 64007
#define ASS_BUT 64008
#define ASS_ID  64009

#define ORD_IDC 65000
#define ORD_APPLY 65001
#define ORD_RESET 65002
#define ORD_LIST 65003
#define ORD_BASE 65100
#define ORD_ID 65004
#define ORD_SELECT 65005
#define ORD_NAME 65006
#define ORD_SHELL 65007
#define ORD_AMOUNT 65008
#define ORD_SPREAD 6509

#define EXE_IDC 66000
#define EXE_REQ 66001
#define EXE_PLOT 66002
#define EXE_PREP 66003
#define EXE_EXEC 66004
#define EXE_ABORD 66005
#define EXE_MODE 66006
#define EXE_DELAY 66007
#define EXE_TLST 66008
#define EXE_TBASE 66100
#define EXE_SBASE 66400
#define EXE_EID 66009
#define EXE_ESTAT 66010
#define EXE_ENAME 66011
#define EXE_EPLOT 660122
#define EXE_EPREP 66013
#define EXE_EFIRE 66014
#define EXE_ETOGL 66015
#define EXE_EPGB 66016
#define EXE_SLST 66017
#define EXE_SMAG 66018
#define EXE_SPGB 66019
#define EXE_SETA 66020
#define EXE_SAT 66021
#define EXE_STL 66022
#define EXE_LINE 66023
