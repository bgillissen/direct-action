
#include "define.hpp"

class artiSupportControl {
	idd = AS_CTRL_IDD;
	name = "artiSupportControl";
	scriptName = "artiSupportControl";
	movingEnable = false;
	enableSimulation = true;
	onLoad = "_this spawn asCtrl_fnc_init;";
	onUnload = "_this spawn asCtrl_fnc_destroy";
	class controlsBackground {
		class title : daTitle {
			text = "Artillery Control Panel (WIP)";
			x = 0.5 - (TOT_WIDTH / 2);
			y = 0.5 - (TOT_HEIGHT / 2) - YOFFSET;
			h = LINE_HEIGHT;
			w = TOT_WIDTH;
		};
		class background : daContainer {
			x = (0.5 - (TOT_WIDTH / 2)) + MENU_WIDTH + SPACE;
			y = (0.5 - (TOT_HEIGHT / 2)) + (3 * LINE_HEIGHT) + (5 * SPACE) - YOFFSET;
			h = TOT_HEIGHT - (4 * LINE_HEIGHT) - (6 * SPACE);
			w = TOT_WIDTH - MENU_WIDTH - SPACE;
		};
		class asLoading : daText {
			idc = AS_LOAD;
			text = "Loading...";
			x = (0.5 - (TOT_WIDTH / 2)) + MENU_WIDTH + (2 * SPACE);
			y = (0.5 - (TOT_HEIGHT / 2)) + (3 * LINE_HEIGHT) + (6 * SPACE) - YOFFSET;
			h = LINE_HEIGHT;
			w = BUTTON_WIDTH;
		};
		class close : asCtrl_ScreenButton {
			action = "closeDialog 0;";
			text = "Close";
			x = 0.5 - (TOT_WIDTH / 2);
		};
	};
	class Controls {
		class missions : daControlGroup {
			idc = MIS_IDC;
			onDestroy = "_this spawn asCtrl_fnc_misDestroy";
			x = 0.5 - ( TOT_WIDTH / 2 );
			y = (0.5 - (TOT_HEIGHT / 2)) + LINE_HEIGHT + SPACE - YOFFSET;
			h = (2 * LINE_HEIGHT) + (3 * SPACE);
			w = TOT_WIDTH;
			class Controls {
				#include "missions\_ui.hpp"
			};
		};
		class menu : daControlGroup {
			idc = MENU_IDC;
			x = 0.5 - ( TOT_WIDTH / 2 );
			y = (0.5 - (TOT_HEIGHT / 2)) + (3 * LINE_HEIGHT) + (5 * SPACE) - YOFFSET;
			h = TOT_HEIGHT - (4 * LINE_HEIGHT) - (6 * SPACE);
			w = MENU_WIDTH;
			class Controls {};
		};
		class target : asCtrl_Screen {
			idc = TRGT_IDC;
			onDestroy = "_this spawn asCtrl_fnc_trgtDestroy";
			class Controls {
				#include "target\_ui.hpp"
			};
		};
		class assets : asCtrl_Screen {
			idc = ASS_IDC;
			class Controls {
				#include "assets\_ui.hpp"
			};
		};
		class ordonance : asCtrl_Screen {
			idc = ORD_IDC;
			onDestroy = "_this spawn asCtrl_fnc_ordDestroy";
			class Controls {
				#include "ordonance\_ui.hpp"
			};
		};
		class execute : asCtrl_Screen {
			idc = EXE_IDC;
			onDestroy = "_this spawn asCtrl_fnc_exeDestroy";
			class Controls {
				#include "execute\_ui.hpp"
			};
		};
	};
};
