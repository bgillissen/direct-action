
class assTitleBat : daText {
	style = ST_CENTER;
	text = "Batteries";
	x = ((TOT_WIDTH - MENU_WIDTH - SPACE) / 4) - (BUTTON_WIDTH / 2);
	y = SPACE;
	h = LINE_HEIGHT;
	w = BUTTON_WIDTH;
};

class assTitleTub : daText {
	style = ST_CENTER;
	idc = ASS_TUBT;
	text = "Tubes";
	x = ((TOT_WIDTH - MENU_WIDTH - SPACE) * 0.75) - BUTTON_WIDTH;
	y = SPACE;
	h = LINE_HEIGHT;
	w = 2 * BUTTON_WIDTH;
};

class assBatBcg : daContainer {
	x = SPACE;
	y = LINE_HEIGHT + (2 * SPACE);
	h = TOT_HEIGHT - (5 * LINE_HEIGHT) - (9 * SPACE);
	w = ((TOT_WIDTH - MENU_WIDTH - (4 * SPACE)) / 2) - SPACE;
};

class assBatList : daControlGroup {
	idc = ASS_BATL;
	y = LINE_HEIGHT + (2 * SPACE);
	h = TOT_HEIGHT - (5 * LINE_HEIGHT) - (9 * SPACE);
	w = (TOT_WIDTH - MENU_WIDTH - (4 * SPACE)) / 2;
};

class assTubBcg : daContainer {
	x = ((TOT_WIDTH - MENU_WIDTH - (3 * SPACE)) / 2) + (2 * SPACE);
	y = LINE_HEIGHT + (2 * SPACE);
	h = TOT_HEIGHT - (5 * LINE_HEIGHT) - (9 * SPACE);
	w = ((TOT_WIDTH - MENU_WIDTH - (4 * SPACE)) / 2) - SPACE;
};

class assTubList : daControlGroup {
	idc = ASS_TUBL;
	x = ((TOT_WIDTH - MENU_WIDTH - (3 * SPACE)) / 2) + SPACE;
	y = LINE_HEIGHT + (2 * SPACE);
	h = TOT_HEIGHT - (5 * LINE_HEIGHT) - (9 * SPACE);
	w = (TOT_WIDTH - MENU_WIDTH - (4 * SPACE)) / 2;
};
