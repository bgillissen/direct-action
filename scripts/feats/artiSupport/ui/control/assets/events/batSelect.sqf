
#include "..\..\define.hpp"

params ["_ctrl"];

private _grp = ctrlParentControlsGroup _ctrl; 
private _batId = parseNumber (ctrlText (_grp controlsGroupCtrl ASS_ID));
   
private _same = false;
if !( isNil "artiSupport_uiBattery" ) then { 
	_same = ( artiSupport_uiBattery isEqualTo _batId );    
};

if ( _same ) exitWith { };

artiSupport_uiBattery = _batId;

call asCtrl_fnc_assUpdate;