
#include "..\..\define.hpp"

disableSerialization;

params ["_ctrl", "_state"];

private _dsp = findDisplay AS_CTRL_IDD;
private _tub = ctrlParentControlsGroup _ctrl; 
private _tubeId = parseNumber (ctrlText (_tub controlsGroupCtrl ASS_ID));

private _lockedBatteries = [];
{
    _x params ["_batId", "_uid"];
    _lockedBatteries pushback _batId; 
} forEach artiSupport_commanders;

(artiSupport_tubes select _tubeId) params ["_batId", "_name", "_veh", "_grp", "_unit"];

artiSupport_uiBuffer params ['_name', '_grid', '_cor', '_ord', '_mode'];

private _tubOrdKey = -1;
{
	_x params ["_ordTubeId", "_shell", "_amount", "_spread"];
    if ( _ordTubeId isEqualTo _tubeId ) exitWith { _tubOrdKey = _forEachIndex; };    
} forEach _ord;

artiSupport_uiNoEvents = true;

if ( _state > 0 ) then {	
    if ( ((vehicle _unit) isEqualTo _veh) && !(_batId in _lockedBatteries) ) then {
        if ( _tubOrdKey < 0 ) then {
    		private _shell = ([_veh] call arti_fnc_getMags) select 0;
			_ord pushback [_tubeId, _shell, 1, 0];
		};
	} else {
        _ctrl cbSetChecked false;
        if ( _tubOrdKey >= 0 ) then { _ord deleteAt _tubOrdKey; };
    };
} else {
    if ( _tubOrdKey >= 0 ) then { _ord deleteAt _tubOrdKey; };
};

private _batInUse = false;
{
    _x params ["_ordTubeId", "_shell", "_amount", "_spread"];
    (artiSupport_tubes select _ordTubeId) params ["_ordBatId", "_name", "_veh", "_grp", "_unit"];
    if ( _ordBatId isEqualTo _batId ) exitWith { _batInUse = true; };
} forEach _ord;

private _tubL = ctrlParentControlsGroup _tub;
private _scr = ctrlParentControlsGroup _tubL;
private _batL = (_scr controlsGroupCtrl ASS_BATL);
private _bat = (_batL controlsGroupCtrl (ASS_BATS + _batId));
(_bat controlsGroupCtrl ASS_CKB) cbSetChecked _batInUse;

artiSupport_uiNoEvents = false;

[(_dsp displayCtrl ORD_IDC)] call asCtrl_fnc_ordUpdate;
[(_dsp displayCtrl EXE_IDC)] call asCtrl_fnc_exeUpdate;

call asCtrl_fnc_hasChanged;