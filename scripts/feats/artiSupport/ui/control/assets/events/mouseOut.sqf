
#include "..\..\define.hpp"

params ["_ctrl"];

private _grp = ctrlParentControlsGroup _ctrl; 
private _bcg = _grp controlsGroupCtrl ASS_BCG;
private _ckb = _grp controlsGroupCtrl ASS_CKB;
private _batId = parseNumber (ctrlText (_grp controlsGroupCtrl ASS_ID));

private _color = [0, 0, 0, 0];

if !( isNil "artiSupport_uiBattery" ) then {
	if ( artiSupport_uiBattery isEqualTo _batId ) then {
    	_color = [(profilenamespace getvariable ['GUI_BCG_RGB_R',0.69]),
				  (profilenamespace getvariable ['GUI_BCG_RGB_G',0.75]),
				  (profilenamespace getvariable ['GUI_BCG_RGB_B',0.5]), 0.4];
	};
};

_bcg ctrlSetBackgroundColor _color;