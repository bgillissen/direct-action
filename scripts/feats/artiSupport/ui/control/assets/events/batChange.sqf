
#include "..\..\define.hpp"

disableSerialization;

params ["_ctrl", "_state"];

if ( artiSupport_uiNoEvents ) exitWith {};

private _dsp = findDisplay AS_CTRL_IDD;
private _bat = ctrlParentControlsGroup _ctrl;
private _batL = ctrlParentControlsGroup _bat; 
private _scr = ctrlParentControlsGroup _batL;
private _tubL = _scr controlsGroupCtrl ASS_TUBL;
private _bcg = _bat controlsGroupCtrl ASS_BCG;
private _batId = parseNumber (ctrlText (_bat controlsGroupCtrl ASS_ID));
private _color = [0.6, 0.6, 0.6];

if !( isNil "artiSupport_uiBattery" ) then {
	if ( artiSupport_uiBattery isEqualTo _batId ) then {
    	_color = [(profilenamespace getvariable ['GUI_BCG_RGB_R',0.69]),
				  (profilenamespace getvariable ['GUI_BCG_RGB_G',0.75]),
				  (profilenamespace getvariable ['GUI_BCG_RGB_B',0.5])];
	};
};

if ( _state > 0 ) then {
	_color pushback 1;
} else {
    _color pushback 0.4;
};

_bcg ctrlSetBackgroundColor _color;

artiSupport_uiBuffer params ['_name', '_grid', '_cor', '_ord', '_mode'];

artiSupport_uiNoEvents = true;

private _c = 0;
{
    _x params ["_tubeBatId", "_name", "_veh", "_grp", "_unit"];
    private _checked = ( (_state > 0) && ((vehicle _unit) isEqualTo _veh) );
    if ( _tubeBatId isEqualTo _batId ) then {
        if ( artiSupport_uiBattery isEqualTo _tubeBatId ) then {
        	private _idc = ASS_TUBS + _c;
            private _tub = (_tubL controlsGroupCtrl _idc);
            (_tub controlsGroupCtrl ASS_CKB) cbSetChecked _checked;
            _c = _c + 1;    
        };
    	private _tubeId = _forEachIndex;
    	if ( _checked ) then {
            private _found = false;
            {
                _x params ["_ordTubeId", "_shell", "_amount", "_spread"];
            	if ( _ordTubeId isEqualTo _tubeId ) exitWith { _found = true };
        	} forEach _ord;
            if !( _found ) then {
            	private _shell = ([_veh] call arti_fnc_getMags) select 0;
				_ord pushback [_tubeId, _shell, 1, 0];
			};
    	} else {
        	{
                _x params ["_ordTubeId", "_shell", "_amount", "_spread"];
            	if ( _ordTubeId isEqualTo _tubeId ) exitWith { _ord deleteAt _forEachIndex; };
        	} forEach _ord;
    	};
	};
} forEach artiSupport_tubes;

artiSupport_uiNoEvents = false;

[(_dsp displayCtrl ORD_IDC)] call asCtrl_fnc_ordUpdate;
[(_dsp displayCtrl EXE_IDC)] call asCtrl_fnc_exeUpdate;

call asCtrl_fnc_hasChanged;