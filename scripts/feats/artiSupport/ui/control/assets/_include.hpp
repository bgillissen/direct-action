
class assBatEntry : daControlGroup {
	h = LINE_HEIGHT + SPACE;
	w = ((TOT_WIDTH - MENU_WIDTH - (4 * SPACE)) / 2);
	class Controls {
		class bcg : daContainer {
			idc = ASS_BCG;
			x = SPACE;
			h = LINE_HEIGHT;
			w = ((TOT_WIDTH - MENU_WIDTH - (4 * SPACE)) / 2) - SPACE;
			colorBackground[] = COL_NONE;
		};
		class select : daCheckBox {
			idc = ASS_CKB;
			x = SPACE;
			h = LINE_HEIGHT;
			w = LINE_HEIGHT;
			onMouseEnter = "_this call asCtrl_fnc_assEvtMouseOver;";
			onMouseExit = "_this call asCtrl_fnc_assEvtMouseOut;";
			onCheckedChanged = "_this call asCtrl_fnc_assEvtBatChange;";
		};
		class name : daTxtButton {
			idc = ASS_BUT;
			style = ST_LEFT;
			colorDisabled[] = COL_TXT;
			colorFocused[] = COL_NONE;
			colorBackground[] = COL_NONE;
			colorBackgroundDisabled[] = COL_NONE;
			colorBackgroundActive[] = COL_NONE;
			colorShadow[] = COL_NONE;
			x = SPACE + LINE_HEIGHT;
			h = LINE_HEIGHT;
			w = ((TOT_WIDTH - MENU_WIDTH - (4 * SPACE)) / 2) - SPACE - LINE_HEIGHT - 0.0001;
			text = "Battery name";
			onMouseButtonClick = "_this call asCtrl_fnc_assEvtBatSelect;";
			onMouseEnter = "_this call asCtrl_fnc_assEvtMouseOver;";
			onMouseExit = "_this call asCtrl_fnc_assEvtMouseOut;";
		};
		class id : daText {
			idc = ASS_ID;
			w = 0;
			h = 0;
			text = "";
		};
	};
};

class assTubEntry : daControlGroup {
	h = LINE_HEIGHT + SPACE;
	w = ((TOT_WIDTH - MENU_WIDTH - (4 * SPACE)) / 2);
	class Controls {
		class bcg : daContainer {
			idc = ASS_BCG;
			x = SPACE;
			h = LINE_HEIGHT;
			w = ((TOT_WIDTH - MENU_WIDTH - (4 * SPACE)) / 2) - SPACE;
			colorBackground[] = COL_NONE;
		};
		class select : daCheckBox {
			idc = ASS_CKB;
			x = SPACE;
			h = LINE_HEIGHT;
			w = LINE_HEIGHT;
			onCheckedChanged = "_this call asCtrl_fnc_assEvtTubChange;";
		};
		class name : daTxtButton {
			idc = ASS_BUT;
			style = ST_LEFT;
			colorDisabled[] = COL_TXT;
			colorFocused[] = COL_NONE;
			colorBackground[] = COL_NONE;
			colorBackgroundDisabled[] = COL_NONE;
			colorBackgroundActive[] = COL_NONE;
			colorShadow[] = COL_NONE;
			x = SPACE + LINE_HEIGHT;
			h = LINE_HEIGHT;
			w = ((TOT_WIDTH - MENU_WIDTH - (4 * SPACE)) / 2) - SPACE - LINE_HEIGHT - 0.0001;
			text = "Tube name";
		};
		class id : daText {
			idc = ASS_ID;
			w = 0;
			h = 0;
			text = "";
		};
	};
};
