
#include "..\define.hpp"

disableSerialization;

params ["_scr"];

_scr ctrlShow false;

if ( artiSupport_commanding ) exitWith {};

waitUntil { !(isNull (findDisplay AS_CTRL_IDD)) };

private _dsp = findDisplay AS_CTRL_IDD;
private _batL = _scr controlsGroupCtrl ASS_BATL;
{
    _x params ["_name", "_plot", "_prep", "_load", "_acc"];
    private _idc = ASS_BATS + _forEachIndex;
    private _bat = _dsp ctrlCreate ["assBatEntry", _idc, _batL];
    private _pos = ctrlPosition _bat;
	_pos set [1, (_forEachIndex * (LINE_HEIGHT + SPACE))];
    _bat ctrlSetPosition _pos;
    (_bat controlsGroupCtrl ASS_ID) ctrlSetText (str _forEachIndex);
    (_bat controlsGroupCtrl ASS_BUT) ctrlSetText _name;
    _bat ctrlCommit 0;
} forEach artiSupport_batteries;

[_scr] call asCtrl_fnc_assUpdate;

if !( ASS_IDC isEqualTo artiSupport_uiMenu ) exitWith {};

(_dsp displayCtrl AS_LOAD) ctrlShow false;
_scr ctrlShow true;