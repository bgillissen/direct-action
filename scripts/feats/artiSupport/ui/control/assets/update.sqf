
#include "..\define.hpp"

if ( artiSupport_commanding ) exitWith {};

disableSerialization;

params [["_scr", controlNull]];

if ( isNull _scr ) then {
    waitUntil { !(isNull (findDisplay AS_CTRL_IDD)) };  
};

private _dsp = findDisplay AS_CTRL_IDD;
_scr = _dsp displayCtrl ASS_IDC;

if ( artiSupport_uiQ ) exitWith {};

if ( artiSupport_uiNoEvents ) then {
	artiSupport_uiQ = true;
	waitUntil { !artiSupport_uiNoEvents };
	artiSupport_uiQ = false;
};

artiSupport_uiNoEvents = true;

private _batL = _scr controlsGroupCtrl ASS_BATL;

private _lockedBatteries = [];
{
    _x params ["_batId", "_uid"];
    _lockedBatteries pushback _batId; 
} forEach artiSupport_commanders;

private _tubeIds = [];
{ 
    _x params ["_tubeId", "_shell", "_amount", "_spread"];
    _tubeIds pushbackUnique _tubeId;
} forEach (artiSupport_uiBuffer select 3);

private _batIds = [];
{
    _x params ["_batId", "_name", "_veh", "_grp", "_unit"];
    if ( _forEachIndex in _tubeIds ) then {
        if ( isNil "artiSupport_uiBattery" ) then { artiSupport_uiBattery = _batId; }; 
    	_batIds pushbackUnique _batId; 
	};
} forEach  artiSupport_tubes;

if !( isNil "artiSupport_uiBattery" ) then {
	if !( artiSupport_uiBattery in _batIds ) then { artiSupport_uiBattery = (_batIds select 0); };
};

{
    _x params ["_name", "_plot", "_prep", "_load", "_acc"];
    private _idc = ASS_BATS + _forEachIndex;
    private _bat = _batL controlsGroupCtrl _idc;
    private _ckb = (_bat controlsGroupCtrl ASS_CKB);
    private _color = [0,0,0];
    private _alpha = 0;
	_ckb cbSetChecked ( (_forEachIndex in _batIds) && !(_forEachIndex in _lockedBatteries) );
    if ( isNil "artiSupport_uiBattery" ) then { artiSupport_uiBattery = _forEachIndex; };
    if ( artiSupport_uiBattery isEqualTo _forEachIndex ) then {
        (_scr controlsGroupCtrl ASS_TUBT) ctrlSetText format["%1's tubes", _name];		
        _color = [(profilenamespace getvariable ['GUI_BCG_RGB_R',0.69]),
		  		  (profilenamespace getvariable ['GUI_BCG_RGB_G',0.75]),
				  (profilenamespace getvariable ['GUI_BCG_RGB_B',0.5])];
		if ( cbChecked _ckb ) then {
			_alpha = 1;  
		} else {
   			_alpha = 0.4;
		};
	};	
    (_bat controlsGroupCtrl ASS_BCG) ctrlSetBackgroundColor (_color + [_alpha]);
} forEach artiSupport_batteries;

private _tubL = _scr controlsGroupCtrl ASS_TUBL;

private _idc = ASS_TUBS;
while { !isNull (_tubL controlsGroupCtrl _idc) } do {
	ctrlDelete (_tubL controlsGroupCtrl _idc);
    _idc = _idc + 1;    
};

private _c = 0;
{
    _x params ["_batId", "_name", "_veh", "_grp", "_unit"];
    if ( _batId isEqualTo artiSupport_uiBattery ) then {
		private _idc = ASS_TUBS + _c;
	    private _tub = _dsp ctrlCreate ["assTubEntry", _idc, _tubL];
        private _ckb = (_tub controlsGroupCtrl ASS_CKB);
        (_tub controlsGroupCtrl ASS_ID) ctrlSetText (str _forEachIndex);
        (_tub controlsGroupCtrl ASS_BUT) ctrlSetText _name;
        private _pos = ctrlPosition _tub;
        _pos set [1, (_c * (LINE_HEIGHT + SPACE))];
        _tub ctrlSetPosition _pos;
        _tub ctrlCommit 0;
        if ( ((vehicle _unit) isEqualTo _veh) && !(_batId in _lockedBatteries) ) then {
        	_ckb ctrlEnable true; 
        	_ckb cbSetChecked (_forEachIndex in _tubeIds);
		} else {
            _ckb cbSetChecked false;
            _ckb ctrlEnable false;
        };
        _c = _c + 1;
	};
} forEach artiSupport_tubes;

artiSupport_uiNoEvents = false;