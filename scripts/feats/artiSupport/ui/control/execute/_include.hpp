
class asCtrl_shellEntry : daControlGroup {
	h = (LINE_HEIGHT * SHELL_HEIGHT_FACTOR) + SPACE;
	w = TOT_WIDTH - MENU_WIDTH - (3 * SPACE)- SCROLLBAR;
	class VScrollbar : daVScrollbar { width = 0; };
	class HScrollbar : daHScrollbar { height = 0; };
	class Controls {
		class shotAt : daText {
			idc = EXE_SAT;
			h = 0;
			w = 0;
		};
		class eta : daText {
			idc = EXE_SETA;
			h = 0;
			w = 0;
		};
		class ammo : daText {
			idc = EXE_SMAG;
			SizeEx = ENTRY_FONTSIZE * SHELL_HEIGHT_FACTOR;
			colorText[] = {1, 1, 1, 0.6};
			h = LINE_HEIGHT * SHELL_HEIGHT_FACTOR;
			w = (2 * BUTTON_WIDTH);
		};
		class time : daText {
			style = ST_RIGHT;
			idc = EXE_STL;
			SizeEx = ENTRY_FONTSIZE * SHELL_HEIGHT_FACTOR;
			colorText[] = {1, 1, 1, 1};
			x = TOT_WIDTH - MENU_WIDTH - (3 * SPACE)- (SCROLLBAR * 2) - BUTTON_WIDTH;
			w = BUTTON_WIDTH;
			h = LINE_HEIGHT * SHELL_HEIGHT_FACTOR;
		};
		class prgb : daContainer {
			idc = EXE_SPGB;
			y = LINE_HEIGHT * SHELL_HEIGHT_FACTOR;
			h = SPACE;
			w = 0;
			colorBackground[] = COL_USR_BCG;
		};
	};
};

class asCtrl_exeEntry : daControlGroup {
	h = LINE_HEIGHT + (SPACE * 1.25);
	w = TOT_WIDTH - MENU_WIDTH - (3 * SPACE);
	class VScrollbar : daVScrollbar { width = 0; };
	class HScrollbar : daHScrollbar { height = 0; };
	class Controls {
		class id : daText {
			idc = EXE_EID;
			text = "-1";
			w = 0;
			h = 0;
		};
		class state : daText {
			idc = EXE_ESTAT;
			h = LINE_HEIGHT;
			w = BUTTON_WIDTH * 0.75;
		};
		class name : daText {
			idc = EXE_ENAME;
			x = BUTTON_WIDTH * 0.75;
			h = LINE_HEIGHT;
			w = (TOT_WIDTH - MENU_WIDTH - (3 * SPACE)) - SCROLLBAR - (BUTTON_WIDTH * 2.75) - LINE_HEIGHT;
		};
		class plot : daTxtButton {
			idc = EXE_EPLOT;
			x = (TOT_WIDTH - MENU_WIDTH - (3 * SPACE)) - SCROLLBAR - (BUTTON_WIDTH * 1.5) - LINE_HEIGHT;
			h = LINE_HEIGHT;
			w = (BUTTON_WIDTH * 0.5);
			text = "Plot";
			onMouseButtonClick = "_this call asCtrl_fnc_exeEvtPlot";
		};
		class prep : daTxtButton {
			idc = EXE_EPREP;
			x = (TOT_WIDTH - MENU_WIDTH - (3 * SPACE)) - SCROLLBAR - BUTTON_WIDTH - LINE_HEIGHT;
			h = LINE_HEIGHT;
			w = (BUTTON_WIDTH * 0.5);
			text = "Prep";
			onMouseButtonClick = "_this call asCtrl_fnc_exeEvtPrep";
		};
		class fire : daTxtButton {
			idc = EXE_EFIRE;
			x = (TOT_WIDTH - MENU_WIDTH - (3 * SPACE)) - SCROLLBAR - (BUTTON_WIDTH * 0.5) - LINE_HEIGHT;
			h = LINE_HEIGHT;
			w = (BUTTON_WIDTH * 0.5);
			text = "Fire";
			onMouseButtonClick = "_this call asCtrl_fnc_exeEvtFire";
		};
		class toggle : daCheckBox {
			idc = EXE_ETOGL;
			x = (TOT_WIDTH - MENU_WIDTH - (3 * SPACE)) - SCROLLBAR -  LINE_HEIGHT;
			w = LINE_HEIGHT;
			h = LINE_HEIGHT;
			tooltip = "Toggle tube's shells display";
			onCheckedChanged = "_this call asCtrl_fnc_exeEvtToggle;";
			//todo custom textures for open / close
		};
		class prgb : daContainer {
			idc = EXE_EPGB;
			y = LINE_HEIGHT;
			h = SPACE;
			w = 0;
			colorBackground[] = COL_USR_BCG;
		};
		class shells : daControlGroup {
			idc = EXE_SLST;
			y = LINE_HEIGHT + SPACE;
			h = 0;
			w = (TOT_WIDTH - MENU_WIDTH - (3 * SPACE)) - SCROLLBAR;
		};
		class separator : daContainer {
			idc = EXE_LINE;
			h = (SPACE * 0.25);
			w = (TOT_WIDTH - MENU_WIDTH - (3 * SPACE)) - SCROLLBAR;
			colorBackground[] = {1, 1, 1, 1};
		};
	};
};
