#include "..\define.hpp"
#include "..\..\..\..\..\common\ui\_sizes.hpp"

if ( artiSupport_uiQ ) exitWith { 0 };

#define EMPTY_ARRAY []

disableSerialization;

params ["_tubeId"];

private _dsp = findDisplay AS_CTRL_IDD;
private _scr = _dsp displayCtrl EXE_IDC;
private _list = (_scr controlsGroupCtrl EXE_TLST);
private _entry = controlNull;
private _slst = controlNull;

private _idc = EXE_TBASE;
while { !isNull (_list controlsGroupCtrl _idc) } do {
    _entry = (_list controlsGroupCtrl _idc);
    if ( (str _tubeId) isEqualTo (ctrlText (_entry controlsGroupCtrl EXE_EID)) ) exitWith {
    	_slst = (_entry controlsGroupCtrl EXE_SLST);
	};
    _idc = _idc + 1;   
};

if ( isNull _slst ) exitWith { 0 };

private _shellIdc = EXE_SBASE;
while { !isNull (_slst controlsGroupCtrl _shellIdc) } do {
	ctrlDelete (_slst controlsGroupCtrl _shellIdc);
    _shellIdc = _shellIdc + 1;
};     

private _count = 0;
private _maxWidth = TOT_WIDTH - MENU_WIDTH - (3 * SPACE) - (SCROLLBAR * 2); 
{
    _x params ['_sTubeId'];    
    if ( _tubeId isEqualTo _sTubeId ) then { 
    	_x params ['_tubeId', '_shell', '_shotAt', '_eta'];
        private _timeLeft = _eta + _shotAt - serverTime;
        if ( _timeLeft > SHELL_SHOWN_AFTER_IMPACT ) then {
	    	private _shellEntry = _dsp ctrlCreate ["asCtrl_shellEntry", (EXE_SBASE + _count), _slst];
            private _shellPos = ctrlPosition _shellEntry;
			_shellPos set [1, (_count * (LINE_HEIGHT + SPACE))];
	    	(_shellEntry controlsGroupCtrl EXE_SMAG) ctrlSetText getText(configFile >> "cfgMagazines" >> _shell >> "displayName");
	        (_shellEntry controlsGroupCtrl EXE_SETA) ctrlSetText str _eta;
	        (_shellEntry controlsGroupCtrl EXE_SAT) ctrlSetText str _shotAT;
	        if ( _timeLeft < 0 ) then { _timeLeft = 0; };
	        (_shellEntry controlsGroupCtrl EXE_STL) ctrlSetText str ([_timeLeft, 1] call common_fnc_roundTo);
	        private _prgB = (_shellEntry controlsGroupCtrl EXE_SPGB);
			private _prct = (_eta - _timeLeft) / _eta;
        	if ( _prct >= 1 ) then {
            	_prgB ctrlSetBackgroundColor [1, 0.4, 0.4, 0.7];
        		_prct = 1; 
			};
        	private _pos = ctrlPosition _prgB;
			_pos set [2, (_maxWidth * _prct)];
        	_prgB ctrlSetPosition _pos;
        	_prgB ctrlCommit 0;
        	if ( _prct < 1 ) then {
        		_pos set [2, _maxWidth];
        		_prgB ctrlSetPosition _pos;
        		_prgB ctrlCommit _timeLeft;
			};
            _shellEntry ctrlSetPosition _shellPos;
            _shellEntry ctrlCommit 0;
	        _count = _count + 1;
		};        
    };
} forEach artiSupport_tubeShells;

private _shellHeight = 0;
private _shown = (cbChecked (_entry controlsGroupCtrl EXE_ETOGL));
private _pos = ctrlPosition _slst;
if ( _shown ) then {
    if ( _count >= 3 ) then {
		_shellHeight = (3 * ((LINE_HEIGHT * SHELL_HEIGHT_FACTOR) + SPACE));
    } else {
		_shellHeight = (_count * ((LINE_HEIGHT * SHELL_HEIGHT_FACTOR)+ SPACE));
	};
};
_pos set [3, _shellHeight];
_slst ctrlSetPosition _pos;
_slst ctrlCommit 0;
_slst ctrlShow ( (_shellHeight > 0) && _shown );
    
private _line = (_entry controlsGroupCtrl EXE_LINE);
private _pos = ctrlPosition _line;
_pos set [1, (LINE_HEIGHT + SPACE + _shellHeight)];
_line ctrlSetPosition _pos;
_line ctrlCommit 0;

 _shellHeight