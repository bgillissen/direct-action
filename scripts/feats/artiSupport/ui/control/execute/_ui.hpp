
class exeRequest : daTxtButton {
	idc = EXE_REQ;
	h = LINE_HEIGHT;
	w = BUTTON_WIDTH;
	y = TOT_HEIGHT - (4 * LINE_HEIGHT) - (5 * SPACE);
	text = "Request";
	action = "call asCtrl_fnc_exeEvtRequest";
};

class exeDelay : daEdit {
	idc = EXE_DELAY;
	x = (BUTTON_WIDTH * 1.2) + (2 * SPACE);
	y = SPACE;
	h = LINE_HEIGHT;
	w = BUTTON_WIDTH * 0.5;
	tooltip = "Delay in seconds";
	onKillFocus = "[true] call asCtrl_fnc_exeSync";
};

class exeExecute : daTxtButton {
	idc = EXE_EXEC;
	h = LINE_HEIGHT;
	w = BUTTON_WIDTH;
	y = TOT_HEIGHT - (4 * LINE_HEIGHT) - (5 * SPACE);
	x = BUTTON_WIDTH + SPACE;
	//x = (3 * BUTTON_WIDTH) + (3 * SPACE);
	text = "Execute";
	action = "call asCtrl_fnc_exeEvtExec";
};

class exeAbord : daTxtButton {
	idc = EXE_ABORD;
	h = LINE_HEIGHT;
	w = BUTTON_WIDTH;
	y = TOT_HEIGHT - (4 * LINE_HEIGHT) - (5 * SPACE);
	x = TOT_WIDTH - MENU_WIDTH - SPACE - BUTTON_WIDTH;
	text = "Abord";
	action = "call asCtrl_fnc_exeEvtAbord";
};

class mode : daCombo {
	idc = EXE_MODE;
	x = SPACE;
	y = SPACE;
	h = LINE_HEIGHT;
	w = BUTTON_WIDTH * 1.2;
	tooltip = "Execution mode";
	onLBSelChanged = "[true] call asCtrl_fnc_exeSync;";
};

class exeBcg : daContainer {
	x = SPACE;
	y = LINE_HEIGHT + (2 * SPACE);
	h = TOT_HEIGHT - (5 * LINE_HEIGHT) - (9 * SPACE);
	w = TOT_WIDTH - MENU_WIDTH - (3 * SPACE);
};

class exeList : daControlGroup {
	idc = EXE_TLST;
	x = SPACE;
	y = LINE_HEIGHT + (2 * SPACE);
	h = TOT_HEIGHT - (5 * LINE_HEIGHT) - (9 * SPACE);
	w = TOT_WIDTH - MENU_WIDTH - (3 * SPACE);
};
