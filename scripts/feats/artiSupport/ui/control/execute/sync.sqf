
#include "..\define.hpp"

params [['_checkChanges', false]];

if ( artiSupport_uiNoEvents ) exitWith {};

disableSerialization;

private _dsp = findDisplay AS_CTRL_IDD;
private _scr = _dsp displayCtrl EXE_IDC;

private _mode = lbcurSel (_scr controlsGroupCtrl EXE_MODE);
private _delay = parseNumber ctrlText (_scr controlsGroupCtrl EXE_DELAY);

artiSupport_uiBuffer set [4, [_mode, _delay]];

if !( _checkChanges ) exitWith {};

call asCtrl_fnc_hasChanged;