
#include "..\define.hpp"

disableSerialization;

params ["_scr"];

_scr ctrlShow false;

waitUntil { !(isNull (findDisplay AS_CTRL_IDD)) };

private _modeCtrl = (_scr controlsGroupCtrl EXE_MODE);
{
    _modeCtrl lbAdd _x;
} forEach (['artiSupport', 'modes'] call core_fnc_getSetting);

[_scr] call asCtrl_fnc_exeUpdate;

artiSupport_lockETAThread = false;
artiSupport_etaThread = ([] spawn as_fnc_shellEtaThread);

if !( EXE_IDC isEqualTo artiSupport_uiMenu ) exitWith {};

private _dsp = findDisplay AS_CTRL_IDD;
(_dsp displayCtrl AS_LOAD) ctrlShow false;
_scr ctrlShow true;