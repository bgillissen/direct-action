
#include "..\define.hpp"

disableSerialization;

params [["_scr", controlNull]];

if ( isNull _scr ) then {
    waitUntil { !(isNull (findDisplay AS_CTRL_IDD)) };  
};

private _dsp = findDisplay AS_CTRL_IDD;
_scr = _dsp displayCtrl EXE_IDC;

if ( artiSupport_uiQ ) exitWith {};

if ( artiSupport_uiNoEvents ) then {
	artiSupport_uiQ = true;
	waitUntil { !artiSupport_uiNoEvents };
	artiSupport_uiQ = false;
};

artiSupport_uiNoEvents = true;

artiSupport_uiBuffer params ['_name', '_grid', '_cor', '_ord', '_mode'];

(_scr controlsGroupCtrl EXE_MODE) lbSetCurSel (_mode select 0);
(_scr controlsGroupCtrl EXE_DELAY) ctrlSetText str (_mode select 1);

private _tubeIds = [];
{
	_x params ["_tubeId", "_shell", "_amount", "_spread"];
    _tubeIds pushbackUnique _tubeId;
} forEach _ord;

private _list = (_scr controlsGroupCtrl EXE_TLST);

artiComputer_lockETAThread = true;

private _idc = EXE_TBASE;
while { !isNull (_list controlsGroupCtrl _idc) } do {
	ctrlDelete (_list controlsGroupCtrl _idc);
    _idc = _idc + 1;
};

private _c = 0;
private _lastY = 0;
private _canExec = true;
{
    _x params ["_batId", "_name", "_veh", "_grp", "_gunner"];
    private _tubeId = _forEachIndex;
    private _bufferKey = _tubeIds find _tubeId;
    if ( _bufferKey >= 0 ) then {
        private _entry = _dsp ctrlCreate ["asCtrl_exeEntry", (EXE_TBASE + _c), _list]; 		
        (_entry controlsGroupCtrl EXE_EID) ctrlSetText (str _tubeId);
        (_entry controlsGroupCtrl EXE_ENAME) ctrlSetText _name;
        (_entry controlsGroupCtrl EXE_ETOGL) cbSetChecked (_tubeId in artiSupport_uiShowTubeShells);
        {
        	private _vName = format["as_%1", _x];
            private _pvName = format["as_%1PVEH", _x];
            private _pveh = (_veh getVariable [_pvName, -1]);
			if ( _pveh < 0 ) then {
				_pveh = ([_vName, {_this call asCtrl_fnc_exeTubeUpdate;call asCtrl_fnc_hasChanged;}, _veh] call pveh_fnc_add);
            	_veh setVariable [_pvName, _pveh];
            };
        } forEach ["plot", "prep", "load", "exec"];
        private _shellHeight = ([nil, nil, _veh] call asCtrl_fnc_exeTubeUpdate);
        private _entryHeight = LINE_HEIGHT + (SPACE * 1.25) + _shellHeight; 
        private _pos = ctrlPosition _entry;
        _pos set [1, _lastY];
        _pos set [3, _entryHeight];
		_entry ctrlSetPosition _pos;
		_entry ctrlCommit 0;
		_lastY = _lastY + _entryHeight;
        _c = _c + 1;        
    };
} forEach artiSupport_tubes;

artiSupport_uiNoEvents = false;
artiComputer_lockETAThread = false;