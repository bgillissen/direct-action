
terminate artiSupport_etaThread;

#include "..\define.hpp"

disableSerialization;

private _dsp = findDisplay AS_CTRL_IDD;
private _scr = _dsp displayCtrl EXE_IDC;
private _list = (_scr controlsGroupCtrl EXE_TLST);

//["artiSupport_tubeShells", artiSupport_shellPVEH] call pveh_fnc_del;

private _idc = EXE_TBASE;
while { !isNull (_list controlsGroupCtrl _idc) } do {
    private _entry = (_list controlsGroupCtrl _idc);
	private _tubeId = parseNumber ctrlText (_entry controlsGroupCtrl EXE_EID);
    (artySupport_tubes select _tubeId) params ["_batId", "_name", "_veh", "_unit", "_group"];
    {
    	private _vName = format["as_%1", _x];
        private _pvName = format["as_%1PVEH", _x];
        private _pveh = (_veh getVariable [_pvName, -1]);
		if ( _pveh >= 0 ) then {
            [_vName, _pveh, _veh] call pveh_fnc_del;
			_veh setVariable [_pvName, -1];
		};
	} forEach ["plot", "prep", "load", "exec"];
    _idc = _idc + 1;
};