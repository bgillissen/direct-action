
#include "..\..\define.hpp"

disableSerialization;

params ["_ctrl", "_state"];

private _tub = ctrlParentControlsGroup _ctrl;
private _tubeId = parseNumber ctrlText (_tub controlsGroupCtrl EXE_EID);

if ( _state > 0 ) then {
    (_tub controlsGroupCtrl EXE_SLST) ctrlShow true;
	artiSupport_uiShowTubeShells pushbackUnique _tubeId;
} else {
    (_tub controlsGroupCtrl EXE_SLST) ctrlShow false;
    artiSupport_uiShowTubeShells = artiSupport_uiShowTubeShells - [_tubeId];
};