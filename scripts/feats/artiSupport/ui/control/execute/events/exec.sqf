
#include "..\..\define.hpp"

disableSerialization;

private _dsp = findDisplay AS_CTRL_IDD;
private _misSCR = _dsp displayCtrl MIS_IDC;
private _exeSCR = _dsp displayCtrl EXE_IDC;

(_misSCR controlsGroupCtrl MIS_RESET) ctrlEnable false;
(_misSCR controlsGroupCtrl MIS_REMOVE) ctrlEnable false;
(_misSCR controlsGroupCtrl MIS_APPLY) ctrlEnable false;
(_exeSCR controlsGroupCtrl EXE_EXEC) ctrlEnable false;

[artiSupport_uiMission, player] remoteExec ['as_fnc_execAbstract', 2, false];
