
#include "..\..\define.hpp"

if !( [player] call as_fnc_needGrant ) exitWith {};

disableSerialization;

private _dsp = findDisplay AS_CTRL_IDD;
private _scr = _dsp displayCtrl EXE_IDC;
(_scr controlsGroupCtrl EXE_REQ) ctrlEnable false;

private _missionIdx = -1;
{
	if ( (_x select 0) isEqualTo artiSupport_uiMission ) exitWith { _missionIdx = _forEachIndex; };
} forEach artiSupport_missions;

(artiSupport_missions select _missionIdx) params ["_misId", "_name", "_owner", "_lastEditBy", "_grid", "_cor", "_ord", "_mode", "_requestedBy", "_grantedBy", "_executedBy"];

if !( isNull _requestedBy ) exitWith {};

(artiSupport_missions select _missionIdx) set [8, player];

{
    if ( [_x] call as_fnc_canGrant ) then {
		['asRequest', [(player getVariable ['MD_name', (name player)]), _name]] remoteExec ['global_fnc_notificationPlayer', _x, false];
	};
} forEach allPlayers;

publicVariable "artiSupport_missions";

call asCtrl_fnc_hasChanged;