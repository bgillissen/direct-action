
#include "..\..\define.hpp"

disableSerialization;

params [["_ctrl", controlNull]];

_ctrl ctrlEnable false;
private _tub = ctrlParentControlsGroup _ctrl;
(_tub controlsGroupCtrl EXE_EPLOT) ctrlEnable false;
(_tub controlsGroupCtrl EXE_EPREP) ctrlEnable false;
(_tub controlsGroupCtrl EXE_ESTAT) ctrlSetText "Fire";

private _dsp = findDisplay AS_CTRL_IDD;
private _misSCR = _dsp displayCtrl MIS_IDC;
private _exeSCR = _dsp displayCtrl EXE_IDC;

(_misSCR controlsGroupCtrl MIS_RESET) ctrlEnable false;
(_misSCR controlsGroupCtrl MIS_REMOVE) ctrlEnable false;
(_misSCR controlsGroupCtrl MIS_APPLY) ctrlEnable false;
(_exeSCR controlsGroupCtrl EXE_EXEC) ctrlEnable false;

private _tubeId = parseNumber ctrlText (_tub controlsGroupCtrl EXE_EID);
[artiSupport_uiMission, _tubeId, player] remoteExec ["as_fnc_manualFire", 2, false];