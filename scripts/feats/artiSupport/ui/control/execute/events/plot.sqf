
#include "..\..\define.hpp"

disableSerialization;

params [["_ctrl", controlNull]];

_ctrl ctrlEnable false;

private _tub = ctrlParentControlsGroup _ctrl;
private _tubeId = parseNumber ctrlText (_tub controlsGroupCtrl EXE_EID);

["plot", artiSupport_uiMission, _tubeId] remoteExec ["as_fnc_doThread", 2, false];
