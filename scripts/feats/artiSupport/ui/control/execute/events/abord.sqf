#include "..\..\define.hpp"

disableSerialization;

private _dsp = findDisplay AS_CTRL_IDD;
private _scr = _dsp displayCtrl EXE_IDC;

(_scr controlsGroupCtrl EXE_ABORD) ctrlEnable false;

[artiSupport_uiMission, player] remoteExec ['as_fnc_execAbord', 2, false];