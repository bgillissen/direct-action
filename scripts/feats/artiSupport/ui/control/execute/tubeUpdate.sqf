
#include "..\..\..\..\..\common\ui\_sizes.hpp"
#include "..\define.hpp"

if ( artiSupport_uiQ ) exitWith { 0 };

disableSerialization;

params [["_var", ''], ["_val", []], "_veh"];

private _tubeId = _veh getVariable ["as_tubId", -1];

if ( _tubeId < 0 ) exitWith { 0 };

private _batId = _veh getVariable ["as_batId", -1];

if ( _batId < 0 ) exitWith { 0 };

artiSupport_uiBuffer params ['_name', '_grid', '_cor', '_ord', '_mode'];

private _dsp = findDisplay AS_CTRL_IDD;

if ( isNull _dsp ) exitWith {};

private _scr = _dsp displayCtrl EXE_IDC;
private _list = (_scr controlsGroupCtrl EXE_TLST);
private _entry = controlNull;
private _idc = EXE_TBASE;
while { !isNull (_list controlsGroupCtrl _idc) } do {
	private _temp = (_list controlsGroupCtrl _idc);
    if ( (ctrlText (_temp controlsGroupCtrl EXE_EID)) isEqualTo (str _tubeId) ) exitWith { _entry = _temp; };
    _idc = _idc + 1;
};

if ( isNull _entry ) exitWith { 0 };

private _locked = artiSupport_lockETAThread;
if !( _locked ) then { artiSupport_lockETAThread = true; };

(artiSupport_tubes select _tubeId) params ["_batId", "_name", "_veh", "_group", "_unit"];

private _stateCtrl = (_entry controlsGroupCtrl EXE_ESTAT);
private _prgBar = (_entry controlsGroupCtrl EXE_EPGB);
private _line = (_entry controlsGroupCtrl EXE_LINE);
private _isBusy = false;
private _state = "";
private _timeLeft = -1;
private _length = -1;
{
    private _stateData = (_veh getVariable format["as_%1", _x]);
	_stateData params ['_done', '_start', '_duration', '_isDone'];
	if ( !_isDone && (serverTime < (_start + _duration)) ) exitWith {
        _isBusy = true; 
    	_state = _x;
        _timeLeft = (_start + _duration - serverTime); 
        _length = _duration;
	};        
} forEach ["plot", "prep", "load"]; 

private _pos = ctrlPosition _prgBar;
if !( _isBusy ) then {
     _pos set [2, 0];
     _prgBar ctrlSetPosition _pos;
     _stateCtrl ctrlSetText "Idle"; 
     _prgBar ctrlCommit 0;
} else {
	switch (_state) do {
       case "plot" : { _stateCtrl ctrlSetText "Ploting"; };  
       case "prep" : { _stateCtrl ctrlSetText "Aiming"; };
       case "load" : { _stateCtrl ctrlSetText "Reloading"; };
    };
    private _maxWidth = (TOT_WIDTH - MENU_WIDTH - (3 * SPACE) - SCROLLBAR);
    private _curWidth = (_maxWidth * ((_length - _timeLeft) / _length));
    _pos set [2, _curWidth];
    _prgBar ctrlSetPosition _pos;
    _prgBar ctrlCommit 0;
    _pos set [2, _maxWidth];
    _prgBar ctrlSetPosition _pos;
    _prgBar ctrlCommit _timeLeft;
};

private _shellHeight = ([_tubeId] call asCtrl_fnc_exeTubeShell);

_pos = ctrlPosition _line;
_pos set [1, (LINE_HEIGHT + SPACE + _shellHeight)];
_line ctrlSetPosition _pos;
_line ctrlCommit 0;

if !( _locked ) then { artiSupport_lockETAThread = false; };

_shellHeight
