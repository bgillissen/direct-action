
#include "define.hpp"

disableSerialization;

private _dsp = findDisplay AS_CTRL_IDD;

if ( isNull _dsp ) exitWith {};

private _misSCR = _dsp displayCtrl MIS_IDC;
private _exeSCR = _dsp displayCtrl EXE_IDC;
private _missionIdx = -1;
{
	if ( (_x select 0) isEqualTo artiSupport_uiMission ) exitWith { _missionIdx = _forEachIndex; };
} forEach artiSupport_missions;

artiSupport_uiBuffer params ["_bName", "_bGrid", "_bCor", "_bOrd", "_bMode"];

if ( _missionIdx < 0 ) exitWith {
    private _isValid = true;
	if ( _bGrid isEqualTo ["", "", ""] ) then { _isValid = false; };
    if ( _isValid && ((count _bOrd) isEqualTo 0) ) then { _isValid = false; };
    (_misSCR controlsGroupCtrl MIS_RESET) ctrlShow false;
    (_misSCR controlsGroupCtrl MIS_REMOVE) ctrlShow false; 
	(_misSCR controlsGroupCtrl MIS_CLONE) ctrlShow false;
    (_misSCR controlsGroupCtrl MIS_APPLY) ctrlSetText "Save";
    (_misSCR controlsGroupCtrl MIS_APPLY) ctrlEnable _isValid;
    (_exeSCR controlsGroupCtrl EXE_REQ) ctrlEnable false;
    (_exeSCR controlsGroupCtrl EXE_EXEC) ctrlEnable false;
    (_exeSCR controlsGroupCtrl EXE_ABORD) ctrlEnable false;
    (_exeSCR controlsGroupCtrl EXE_DELAY) ctrlEnable ( (_bMode select 0) > 0 );
    private _list = (_exeSCR controlsGroupCtrl EXE_TLST);
	private _idc = EXE_TBASE;
    {
     	_x params ["_oTubeId", "_oMag", "_oAmount", "_oSpread"];
        private _entry = (_list controlsGroupCtrl _idc);
        (_entry controlsGroupCtrl EXE_EPLOT) ctrlEnable false;
		(_entry controlsGroupCtrl EXE_EPREP) ctrlEnable false;
		(_entry controlsGroupCtrl EXE_EFIRE) ctrlEnable false;
		_idc = _idc + 1;
    } forEach _bOrd;
};
private _hasChanged = false;
private _matched = 0; 

(artiSupport_missions select _missionIdx) params ["_misId", "_name", "_owner", "_lastEditBy", "_grid", "_cor", "_ord", "_mode", "_requestedBy", "_grantedBy", "_executedBy"];
if !( _name isEqualTo _bName ) then { _hasChanged = true; };
if ( !_hasChanged && !( _grid isEqualTo _bGrid) ) then { _hasChanged = true; };
if ( !_hasChanged && !( _cor isEqualTo _bCor) ) then { _hasChanged = true; };
if ( !_hasChanged && !( _mode isEqualTo _bMode) ) then { _hasChanged = true; };
if ( !_hasChanged && !((count _ord) isEqualTo (count _bOrd)) ) then { _hasChanged = true; };
if ( !_hasChanged ) then {
	{
	    _x params ["_oTubeId", "_oMag", "_oAmount", "_oSpread"];
	    {
	        _x params ["_iTubeId", "_iMag", "_iAmount", "_iSpread"];
	        if ( _oTubeId isEqualTo _iTubeId ) exitWith {
	        	if !( _oMag isEqualto _iMag ) exitWith { _hasChanged = true; };    
	            if !( _oAmount isEqualto _iAmount ) exitWith { _hasChanged = true; };
	            if !( _oSpread isEqualto _iSpread ) exitWith { _hasChanged = true; };
                _matched = _matched + 1;
	        };
	    } forEach _bOrd;
	    if ( _hasChanged ) exitWith {};
	} forEach _ord;
};
if !( _hasChanged ) then { _hasChanged = !( _matched isEqualTo (count _ord) ); };

(_misSCR controlsGroupCtrl MIS_RESET) ctrlShow true;
(_misSCR controlsGroupCtrl MIS_CLONE) ctrlShow true;
(_misSCR controlsGroupCtrl MIS_REMOVE) ctrlShow true;
(_misSCR controlsGroupCtrl MIS_APPLY) ctrlSetText "Apply";

private _needGrant = [player] call as_fnc_needGrant;
private _canExecute = ([player] call as_fnc_canExecute);
private _isExecuting = !( isNull _executedBy );
private _isGranted = !( isNull _grantedBy );
private _isRequested = !( isNull _requestedBy );
private _canAct = ( !_hasChanged && !_isExecuting );
private _canExec = ( !_hasChanged && !_isExecuting &&  _canExecute );
 if ( _needGrant && _canExec ) then { _canExec = _isGranted; };
private _canUpdate = ( _hasChanged && !_isExecuting );
private _canRemove = !_isExecuting;

private _list = (_exeSCR controlsGroupCtrl EXE_TLST);
private _idc = EXE_TBASE;
   
{
    _x params ["_oTubeId", "_oMag", "_oAmount", "_oSpread"];
    private _entry = (_list controlsGroupCtrl _idc);
    private _isBusy = false;
    private _isPloted = false;
    private _isPreped = false;
    private _isLoaded = false;
    (artiSupport_tubes select _oTubeId) params ["_tBatId", "_tName", "_tVeh", "_tGrp", "_tGunner"];        
    (_tVeh getVariable 'as_exec') params ["_eMisId", "_eIsExecuting"];
    private _canGiveOrder = [_tBatId, player] call as_fnc_canGiveOrder;
    if ( _eIsExecuting ) then {
        _canExec = false;
        _canUpdate = false; 
        _canRemove = false;
    } else {
    	{
			(_tveh getVariable format["as_%1", _x]) params ['_toDo', '_start', '_duration', '_isDone'];
			if ( !_isDone && (serverTime < (_start + _duration)) ) exitWith { _isBusy = true; };        
		} forEach ["plot", "prep", "load"];
        if !( _isBusy ) then {
            _isPloted = [artiSupport_uiMission, _oTubeId, 'plot'] call as_fnc_isDone;
            _isPreped = [artiSupport_uiMission, _oTubeId, 'prep'] call as_fnc_isDone;
            _isLoaded = [artiSupport_uiMission, _oTubeId, 'load'] call as_fnc_isDone;
        } else {
            _canExec = false;
            _canUpdate = false;
			_canRemove = false;  
        };    
    };
    if ( _canAct && _canGiveOrder ) then {
    	(_entry controlsGroupCtrl EXE_EPLOT) ctrlEnable ( !_isBusy ); 
		(_entry controlsGroupCtrl EXE_EPREP) ctrlEnable ( !_isBusy && _isPloted );
        if ( _needGrant ) then {
            (_entry controlsGroupCtrl EXE_EFIRE) ctrlEnable ( !_isBusy && _isPloted && _isPreped && _isLoaded  && _canExecute && _isGranted);
		} else {
            (_entry controlsGroupCtrl EXE_EFIRE) ctrlEnable ( !_isBusy && _isPloted && _isPreped && _isLoaded  && _canExecute );
        };
	} else {
        (_entry controlsGroupCtrl EXE_EPLOT) ctrlEnable false;
		(_entry controlsGroupCtrl EXE_EPREP) ctrlEnable false;
		(_entry controlsGroupCtrl EXE_EFIRE) ctrlEnable false;
	};
    _idc = _idc + 1;
} forEach _ord;

(_misSCR controlsGroupCtrl MIS_RESET) ctrlEnable _canUpdate;    
(_misSCR controlsGroupCtrl MIS_APPLY) ctrlEnable _canUpdate;
(_misSCR controlsGroupCtrl MIS_REMOVE) ctrlEnable _canRemove;

(_exeSCR controlsGroupCtrl EXE_DELAY) ctrlEnable ( (_bMode select 0) > 0 );
(_exeSCR controlsGroupCtrl EXE_REQ) ctrlEnable ( _needGrant && !_isRequested && ([player] call as_fnc_canRequest) );
(_exeSCR controlsGroupCtrl EXE_EXEC) ctrlEnable _canExec;
(_exeSCR controlsGroupCtrl EXE_ABORD) ctrlEnable ( _isExecuting && (player isEqualTo _executedBy) );