
#include "define.hpp"

disableSerialization;

params [["_dsp", displayNull]];

if ( isNull _dsp ) exitWith {};

if ( artiSupport_uiQ ) exitWith {};

if ( artiSupport_uiNoEvents ) then {
	artiSupport_uiQ = true;
	waitUntil { !artiSupport_uiNoEvents };
	artiSupport_uiQ = false;
};

[(_dsp displayCtrl MIS_IDC)] call asCtrl_fnc_misUpdate;
[(_dsp displayCtrl TRGT_IDC)] call asCtrl_fnc_trgtUpdate;
[(_dsp displayCtrl ASS_IDC)] call asCtrl_fnc_assUpdate;	
[(_dsp displayCtrl ORD_IDC)] call asCtrl_fnc_ordUpdate;
[(_dsp displayCtrl EXE_IDC)] call asCtrl_fnc_exeUpdate;

call asCtrl_fnc_hasChanged;