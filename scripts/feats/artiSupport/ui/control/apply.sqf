
#include "define.hpp"

private _code = [];
{ 
	_code pushback format["[false] call asCtrl_fnc_%1Sync", _x]; 
} forEach ['mis', 'trgt', 'ord', 'exe'];

0 = call compile (_code joinString ";");

private _name = artiSupport_uiBuffer select 0;
private _grid = (artiSupport_uiBuffer select 1) + [];
private _cor = (artiSupport_uiBuffer select 2) + [];
private _mode = (artiSupport_uiBuffer select 4) + [];
private _ord = [];
{ 
	_ord pushback (_x + []); 
} forEach (artiSupport_uiBuffer select 3);

private _missionIdx = -1;
if !( artiSupport_uiMission isEqualTo "" ) then {
	{
		if ( (_x select 0) isEqualTo artiSupport_uiMission ) exitWith { _missionIdx = _forEachIndex; };
	} forEach artiSupport_missions;
};

if ( _missionIdx < 0 ) then {
 	artiSupport_uiMission = str time; 
    artiSupport_missions pushback [artiSupport_uiMission, _name, (name player), (name player), _grid, _cor, _ord, _mode, objNull, objNull, objNull];
} else {
    (artiSupport_missions select _missionIdx) params ["_misId", "_name", "_owner", "_lastEditBy", "_misGrid", "_misCor", "_misOrd", "_misMode", "_requestedBy", "_grantedBy", "_executedBy"];
    if ( isNull _executedBy ) then {
	    (artiSupport_missions select _missionIdx) set [1, _name];
	    (artiSupport_missions select _missionIdx) set [3, (name player)];
	    (artiSupport_missions select _missionIdx) set [4, _grid];
	    (artiSupport_missions select _missionIdx) set [5, _cor];
		(artiSupport_missions select _missionIdx) set [6, _ord];
	    (artiSupport_missions select _missionIdx) set [7, _mode];
        //reset requestedBy, grantedBy, if grid has changed, ordonance has changed (tubes, shell amount)  
        private _hasChanged = (_misGrid isEqualTo _grid);
        if !( _hasChanged ) then {
            private _newTubeIds = [];
            private _newShells = 0;
	    	{
                _x params ["_tubeId", "_mag", "_amount", "_spread"];
                _newTubeIds pushback _tubeId;
                _newShells = _newShells + _amount;
            } forEach _ord;
            private _oldTubeIds = [];
            private _oldShells = 0;
	    	{
                _x params ["_tubeId", "_mag", "_amount", "_spread"];
                _oldTubeIds pushback _tubeId;
                _oldShells = _oldShells + _amount;
            } forEach _misOrd;
            _hasChanged = (_newShells > _oldShells);
        	if !( _hasChanged ) then {
				{
                    if ( _x in _oldTubeIds ) exitWith { _hasChanged = true; };
                } forEach _newTubeIds;
                if !( _hasChanged ) then {
                    {
                    	if ( _x in _newTubeIds ) exitWith { _hasChanged = true; };
                	} forEach _oldTubeIds;
                };
            };
		};
		if ( _hasChanged ) then {
        	(artiSupport_missions select _missionIdx) set [8, objNull];
        	(artiSupport_missions select _missionIdx) set [9, objNull];
		}; 
    };  
};

publicVariable "artiSupport_missions";

[(findDisplay AS_CTRL_IDD)] call asCtrl_fnc_update;