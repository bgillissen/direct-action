class asRequest {
	title = "New artillery stryke request";
	description = "%1 has requested the permission to execute the artillery support mission '%2'.";
	iconPicture = "feats\artiSupport\media\shell.paa";
	color[] = {0.75, 0.49, 0.09, 1};
	priority = 9;
	duration = 5;
};

class asGrant {
	title = "Artillery fire mission accepted";
	description = "Artillery fire mission '%2' execution has been accepted by %1";
	iconPicture = "feats\artiSupport\media\shell.paa";
	color[] = {0.3, 0.8, 0.06, 1};
	priority = 9;
	duration = 5;
};

class asRefuse {
	title = "Artillery fire mission refused";
	description = "Artillery fire mission '%2' execution has been denied by %1";
	iconPicture = "feats\artiSupport\media\shell.paa";
	color[] = {0.8, 0.13, 0.14, 1};
	priority = 9;
	duration = 5;
};
