/*
@filename: feats\artiSupport\playerRespawn.sqf
Author:
	Ben
Description:
	run on player,
*/

#include "_debug.hpp"

if ( (["artiSupport"] call core_fnc_getParam) isEqualTo 0 ) exitWith {
	#ifdef DEBUG
   	debug(LL_INFO, "disabled by mission parameter");
    #endif
    nil  
};

if ( (count artiSupport_tubes) isEqualTo 0 ) exitWith { 
	#ifdef DEBUG
   	debug(LL_INFO, "no tubes available, abording");
    #endif
    nil  

};

AS_ctrlAction = -1;

if ( [player] call as_fnc_canControl ) then {
	AS_ctrlAction = (player addAction ["Artillery Control Panel", {createDialog "artiSupportControl"}, [], 0, false, true, "", "true", 4]);
};

AS_grantAction = -1;

if ( [player] call as_fnc_canGrant ) then {
	AS_grantAction = (player addAction ["Pending Stryke Requests", {createDialog "artiSupportGrant"}, [], 0, false, true, "", "['grant'] call as_fnc_actCondition", 4]);
};

nil