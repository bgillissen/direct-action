class artiSupport {
	tag = "artiSupport";
	class functions {
		class playerInit { file="feats\artiSupport\playerInit.sqf"; };
		class playerKilled { file="feats\artiSupport\playerKilled.sqf"; };
		class playerRespawn { file="feats\artiSupport\playerRespawn.sqf"; };
		class playerSwitchSeat { file="feats\artiSupport\playerSwitchSeat.sqf"; };
		class serverInit { file="feats\artiSupport\serverInit.sqf"; };
		class serverLeave { file="feats\artiSupport\serverLeave.sqf"; };
		class serverDestroy { file="feats\artiSupport\serverDestroy.sqf"; };
	};
};

class as {
	tag = "as";
	class functions {
		class actCondition { file="feats\artiSupport\actions\condition.sqf"; };
		class actGetIn { file="feats\artiSupport\actions\getIn.sqf"; };
		class actReleaseCommand { file="feats\artiSupport\actions\releaseCommand.sqf"; };
		class actTakeCommand { file="feats\artiSupport\actions\takeCommand.sqf"; };

		class canCommand { file="feats\artiSupport\common\canCommand.sqf"; };
		class canControl { file="feats\artiSupport\common\canControl.sqf"; };
		class canExecute { file="feats\artiSupport\common\canExecute.sqf"; };
		class canGiveOrder { file="feats\artiSupport\common\canGiveOrder.sqf"; };
		class canGrant { file="feats\artiSupport\common\canGrant.sqf"; };
		class canRequest { file="feats\artiSupport\common\canRequest.sqf"; };
		class getDuration { file="feats\artiSupport\common\getDuration.sqf"; };
		class gunnerShot { file="feats\artiSupport\common\gunnerShot.sqf"; };
		class isDone { file="feats\artiSupport\common\isDone.sqf"; };
		class manualFire { file="feats\artiSupport\common\manualFire.sqf"; };
		class needGrant { file="feats\artiSupport\common\needGrant.sqf"; };

		class doThread { file="feats\artiSupport\threads\do.sqf"; };
		class distanceThread { file="feats\artiSupport\threads\distance.sqf"; };
		class gunnerThread { file="feats\artiSupport\threads\gunner.sqf"; };
		class plotThread { file="feats\artiSupport\threads\plot.sqf"; };
		class prepThread { file="feats\artiSupport\threads\prep.sqf"; };
		class reloadThread { file="feats\artiSupport\threads\reload.sqf"; };
		class shellEtaThread { file="feats\artiSupport\threads\shellETA.sqf"; };

		class execAbord { file="feats\artiSupport\exec\abord.sqf"; };
		class execAbstract { file="feats\artiSupport\exec\abstract.sqf"; };
		class execAsap { file="feats\artiSupport\exec\asap.sqf"; };
		class execSequencial { file="feats\artiSupport\exec\sequencial.sqf"; };
		class execWave { file="feats\artiSupport\exec\wave.sqf"; };
	};
};

#include "ui\_cfgFunctions.hpp"
