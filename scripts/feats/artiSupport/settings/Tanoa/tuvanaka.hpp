class tuvanaka {
	class base {
		batId = 0;
		name = "Base Battery";
		pool = "artiTank";
		tubes[] = {"AS_base_1", "AS_base_2", "AS_base_3", "AS_base_4"};
		plot = 0.6;
		prepare = 0.8;
		reload = 1;
		accuracy = 1;
	};
	class fob {
		batId = 1;
		name = "FOB Battery";
		pool = "artiCannon";
		tubes[] = {"AS_fob_1", "AS_fob_2", "AS_fob_3", "AS_fob_4"};
		plot = 1;
		prepare = 1.2;
		reload = 1;
		accuracy = 1;
	};
};
