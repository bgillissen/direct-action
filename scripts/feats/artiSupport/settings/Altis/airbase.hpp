class airbase {
	class base {
		batId = 0;
		name = "Base Battery";
		pool = "artiTank";
		tubes[] = {"AS_base_1", "AS_base_2", "AS_base_3", "AS_base_4"};
		plot = 0.8;
		prepare = 1;
		reload = 1;
		accuracy = 1;
	};
};
