/*
@filename: feats\artiSupport\playerInit.sqf
Author:
	Ben
Description:
	run on player,
*/

#include "_debug.hpp"
#include "ui\control\define.hpp"
#include "ui\grant\define.hpp"

if ( (["artiSupport"] call core_fnc_getParam) isEqualTo 0 ) exitWith {
	#ifdef DEBUG
   	debug(LL_INFO, "disabled by mission parameter");
    #endif
    nil  
};

if ( (count artiSupport_tubes) isEqualTo 0 ) exitWith {
	#ifdef DEBUG
   	debug(LL_INFO, "no tubes available, abording");
    #endif
    nil  
};

artiSupport_commanding = false;
artiSupport_distanceThread = scriptNull;
artiSupport_uiAction = -1;
artiSupport_uiTab = 0;
artiSupport_uiMission = "";
artiSupport_uiMenu = TRGT_IDC;
artiSupport_uiBattery = nil;
artiSupport_uiBuffer = [];
artiSupport_uiShowTubeShells = [];
artiSupport_uiNoEvents = false;
artiSupport_uiQ = false;
artiSupport_uiTubeLock = [];
artiSupport_inTube = objNull;
artiSupport_lockETAThread = false;

//register artiSupport_missions PVEH
private _misPVEH = {};
if ( [player] call as_fnc_canGrant ) then { 
	if ( [player] call as_fnc_canControl ) then {
        _misPVEH = {
      	  	[] spawn asHud_fnc_update; 
        	if !( isNull (findDisplay AS_CTRL_IDD) ) then { [(findDisplay AS_CTRL_IDD)] spawn asCtrl_fnc_update; };
			if !( isNull (findDisplay AS_GRANT_IDD) ) then { [(findDisplay AS_GRANT_IDD)] spawn asGrant_fnc_update; };
    	};
    } else {
	    _misPVEH = {
    	    [] spawn asHud_fnc_update; 
			if !( isNull (findDisplay AS_GRANT_IDD) ) then { [(findDisplay AS_GRANT_IDD)] spawn asGrant_fnc_update; };
    	};    
    };
} else {
    if ( [player] call as_fnc_canControl ) then {
	    _misPVEH = {
	        if ( isNull (findDisplay AS_CTRL_IDD) ) exitWith {};
	        [(findDisplay AS_CTRL_IDD)] spawn asCtrl_fnc_update;
	    };
    }; 
}; 
artiSupport_misPVEH = ['artiSupport_missions', _misPVEH] call pveh_fnc_add;


if !( [player] call as_fnc_canCommand ) exitWith {};

private _take = "Take Battery Commandment";
private _release = "Release Battery Commandment";
{
	{
		_x params ["_thing", "_actions"];
		{
			if ( (configName _x) isEqualTo "artiSupport" ) then {
                private _batId = getNumber(_x >> "batId");
				#ifdef DEBUG
				private _debug = format["action has been added to %1 for batId %2", _thing, _batId];
				debug(LL_DEBUG, _debug);
				#endif
                private _cond = format["['takeCommand', %1] call as_fnc_actCondition", _batId];
				_thing addAction [_take, {_this call as_fnc_actTakeCommand}, _batId, 0, false, true, "", _cond, 4];
                private _cond = format["['releaseCommand', %1] call as_fnc_actCondition", _batId];
                _thing addAction [_release, {_this call as_fnc_actReleaseCommand}, _batId, 0, false, true, "", _cond, 4];
			};
		} forEach _actions;
	} forEach _x;
} forEach [BA_npc, BA_obj, BA_veh];

nil