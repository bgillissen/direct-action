
params ["_target", "_caller", "_id", "_tubeId"];

(artiSupport_tubes select _tubeId) params ["_tubeBatId", "_name", "_veh", "_grp", "_unit"];

artiSupport_inTube = _veh;

[player, _veh, _unit] remoteExec ["as_fnc_gunnerThread", 2, false];

waitUntil {
	sleep 1; 
	!( (vehicle _unit) isEqualTo _veh ) 
};

player action ['getInGunner', _veh];

waitUntil {
	sleep 1; 
	( ((vehicle player) isEqualTo _veh)  || (isNull player) || !(alive player) || (player getVariable ['agony', false]) )
};

waitUntil {
	sleep 1; 
	( ((vehicle player) isEqualTo player) || (isNull player) || !(alive player) || (player getVariable ['agony', false]) )
};

artiSupport_inTube = objNull;