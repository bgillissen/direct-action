
params ["_action", ["_arg", '']];

if ( _action isEqualTo "takeCommand" ) exitWith {
    private _bool = true;
	{
	    _x params ["_batId", '_uid'];
	    if ( _batId isEqualTo _arg ) exitWith { _bool = false; };
	} forEach artiSupport_commanders;
    _bool  
};

if ( _action isEqualTo "releaseCommand" ) exitWith {
    private _bool = false;
	{
	    _x params ["_batId", '_uid'];
	    if ( _uid isEqualTo (getPlayerUid player) && (_batId isEqualTo _arg) ) exitWith { _bool = true; };
	} forEach artiSupport_commanders;
    _bool
};

if ( _action isEqualTo "getIn" ) exitWith {
	  if !( (vehicle player) isEqualTo player ) exitWith { false };
      if !( isNull artiSupport_inTube ) exitWith { false };
      (artiSupport_tubes select _arg) params ["_tubeBatId", "_name", "_veh", "_grp", "_unit"];
      ( (vehicle _unit) isEqualTo _veh )
};

if ( _action isEqualTo "grant" ) exitWith {
    private _bool = false;
    {
        if ( !isNull(_x select 8) && isNull(_x select 9) ) exitWith { _bool = true; };
    } forEach artiSupport_missions;
    _bool
};