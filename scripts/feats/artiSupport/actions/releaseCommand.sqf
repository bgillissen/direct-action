
params ["_target", "_caller", "_id", "_batId"];

private _uid = (getPlayerUid player);
{
    _x params ["_batId", '_puid'];
    if ( _puid isEqualTo _uid ) exitWith {
		terminate artiSupport_distanceThread;
        artiSupport_commanding = false;
		private _msg = ['artiSupport', 'messages', 'release'] call core_fnc_getSetting;
		_msg = format[_msg, (name _caller), ((artiSupport_batteries select _batId) select 0)];
		[1, _msg, ["HQ", PLAYER_SIDE]] call global_fnc_chat;
        {
    		_x params ["_tubeBatId", "_name", "_veh", "_grp", "_unit"];
    		if ( _tubeBatId isEqualTo _batId ) then {
        		_veh removeAction (_veh getVariable ['as_actGetIn', -1]);
                _veh setVariable ['as_actGetIn', -1, false];
			};
        } forEach artiSupport_tubes;
        player removeAction artiSupport_uiAction;
        artiSupport_uiAction = -1;
        artiSupport_commanders deleteAt _forEachIndex;
		publicVariable "artiSupport_commanders";
    };
} forEach artiSupport_commanders;