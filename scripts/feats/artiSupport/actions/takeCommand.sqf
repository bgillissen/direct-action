
params ["_target", "_caller", "_id", "_batId"];

private _ok = true;
{
    _x params ["_cmdBatId", "_uid"];
    if ( _cmdBatId isEqualTo _batId ) exitWith { _ok = false; };
} forEach artiSupport_commanders;

if !( _ok ) exitWith {};

artiSupport_commanding = true;
artiSupport_battery = _batId;

artiSupport_commanders pushBack [_batId, (getplayerUID player)];
publicVariable "artiSupport_commanders";

private _msg = ['artiSupport', 'messages', 'take'] call core_fnc_getSetting;
_msg = format[_msg, (name _caller), ((artiSupport_batteries select _batId) select 0)];
[1, _msg, ["HQ", PLAYER_SIDE]] call global_fnc_chat;

artiSupport_uiAction = (player addAction ["Artillery Control Panel", {createDialog "artiSupportControl"}, [], 0, false, true, "", "true", 4]);

{
    _x params ["_tubeBatId", "_name", "_veh", "_grp", "_unit"];
    if ( _tubeBatId isEqualTo _batId ) then {
        private _cond = format["['getIn', %1] call as_fnc_actCondition", _forEachIndex];
    	private _action = (_veh addAction  ["Replace Gunner", {_this spawn as_fnc_actGetIn}, _forEachIndex, 0, false, true, "", _cond, 4]);
        _veh setVariable ['as_actGetIn', _action, false];            
    };
} forEach artiSupport_tubes;

artiSupport_distanceThread = (_this spawn as_fnc_distanceThread);