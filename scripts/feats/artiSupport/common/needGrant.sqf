
params [["_player", objNull]];

if ( (["artiSupport_grant"] call core_fnc_getParam) isEqualTo 0 ) exitWith { false };

private _HQpresent = false;
{
    if ( (_x getVariable "role") isEqualTo "hq" ) exitWith { _HQpresent = true; };
} forEach allPlayers;

if !( _HQpresent ) exitWith { false };

if ( (_player getVariable "role") isEqualTo "hq" ) exitWith { false };

true