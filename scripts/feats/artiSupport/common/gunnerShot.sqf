
params ["_unit", "_weapon", "_muzzle", "_mode", "_ammo", "_mag", "_shell", "_veh"];

if ( isNull _veh ) exitWith {};

private _tubeId = (_veh getVariable ["as_tubId", -1]);

if ( _tubeId < 0 ) exitWith {};

private "_owner";
(_veh getVariable "as_exec") params ["_misId", "_isExecuting"];

(_veh getVariable "as_aim") params ["_misId", "_aimPos", "_aimWeapon", "_aimMode", "_aimMuzzle", "_eta"];

private _missionIdx = -1;
{
	if ( (_x select 0) isEqualTo _misId ) exitWith { _missionIdx = _forEachIndex; };
} forEach artiSupport_missions;

if ( !_isExecuting || (_missionIdx < 0) ) then { 
    _owner = (_veh getVariable ["manualFireBy", objNull]); 
} else {
    _owner = (artiSupport_missions select _missionIdx) select 10;
};

_shell setShotParents [(vehicle _owner), _owner];

{
    _x params ['_tubeId', '_mag', '_shotAt', '_eta'];
    if ( serverTime > (_shotAt + _eta + 1.5) ) then {
        artiSupport_tubeShells deleteAt _forEachIndex;
    };
} forEach artiSupport_tubeShells;

artiSupport_tubeShells pushback [_tubeId, _mag, serverTime, _eta];

publicVariable "artiSupport_tubeShells";

_veh setVehicleAmmo 1;

_veh setDir (_veh getVariable "as_dir");
_veh setPos (_veh getVariable "as_pos");