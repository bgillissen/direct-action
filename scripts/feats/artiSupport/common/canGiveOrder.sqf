
params ["_batId", "_player"];

private _isCommanded = false;
private _isCommander = false;
{
	    _x params ["_pBatId", '_puid'];
	    if ( _pBatId isEqualTo _batId ) exitWith { 
			_isCommanded = true;
            _isCommander = ((getPlayerUID _player) isEqualTo _puid);
		};
} forEach artiSupport_commanders;

if ( _isCommanded ) exitWith { _isCommander };

( ([_player] call as_fnc_canControl) || ([_player] call as_fnc_canCommand) ) 