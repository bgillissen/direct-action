
params ["_batId", "_action"];

private _factor = switch(_action) do {
    case 'plot' : {(artiSupport_batteries select _batId) select 1};  
    case 'prep' : {(artiSupport_batteries select _batId) select 2};
    case 'load' : {(artiSupport_batteries select _batId) select 3};
};

private _min = ['artiSupport', 'timings', _action, 'min'] call core_fnc_getSetting;
private _max = ['artiSupport', 'timings', _action, 'max'] call core_fnc_getSetting;
private _mid = (_max - (random (_max - _min)));

(random [_min, (_mid * _factor), (_max * _factor)])