
#include "..\_debug.hpp"

params["_misId", "_tubeId", "_player"];

if ( ({ !isNull _x } count (artiSupport_tubeThreads select _tubeId)) > 0 ) exitWith {
    #ifdef DEBUG
    private _debug = format["fire failed, tube with id '%1' is busy", _tubeId];
    debug(LL_ERR, _debug);
    #endif
};

private _missionIdx = -1;
{
	if ( (_x select 0) isEqualTo _misId ) exitWith { _missionIdx = _forEachIndex; };
} forEach artiSupport_missions;

if ( _missionIdx < 0 ) exitWith {
    #ifdef DEBUG
    private _debug = format["fire failed, could not find mission with id '%1'", _misId];
    debug(LL_ERR, _debug);
    #endif
};

(artiSupport_missions select _missionIdx) params ["_misId", "_name", "_owner", "_lastEditBy", "_grid", "_cor", "_ord", "_mode", "_requestedBy", "_grantedBy", "_executedBy"];

if !( isNull _executedBy ) exitWith {
	#ifdef DEBUG
    private _debug = format["fire failed, mission id with id '%1' iis being executed", _tubeId];
    debug(LL_ERR, _debug);
    #endif    
};

private _ordIdx = -1;
{
	_x params ["_ordTubeId", "_shell", "_amount", "_spread"];
    if ( _tubeId isEqualTo _ordTubeId ) exitWith { _ordIdx = _forEachIndex; };
} forEach _ord;

if ( _ordIdx < 0 ) exitWith {
    #ifdef DEBUG
    private _debug = format["fire failed, could not find tube with id '%1' in mission ordonance", _tubeId];
    debug(LL_ERR, _debug);
    #endif
};

(_ord select _ordIdx) params ["_tubeId", "_shell", "_amount", "_spread"];

(artiSupport_tubes select _tubeId) params ["_batId", "_name", "_veh", "_grp", "_gunner"];

if !( (vehicle _gunner) isEqualto _veh ) exitWith {
    #ifdef DEBUG
    debug(LL_ERR, "fire failed, gunner is not in vehicle", 0);
    #endif    
};

private _ok = true;
{
    if !( [_misId, _tubeId, "prep"] call as_fnc_isDone ) exitwith {
        #ifdef DEBUG
    	private _debug = format["fire failed, was not %1ed", _x];
    	debug(LL_ERR, _debug);
    	#endif 
    	_ok = false; 
    };
} forEach ['plot', 'prep', 'load'];

if !( _ok ) exitWith {};

(_veh getVariable "as_aim") params ["_misId", "_pos", "_weapon", "_mode", "_muzzle", "_eta"];

_veh setVariable ["manualFireBy", _player];

_veh fire [_muzzle, _mode, _shell];

_veh setPos (_veh getVariable "as_pos");
_veh setDir (_veh getVariable "as_dir");

[_misId, _tubeId] spawn as_fnc_reloadThread;