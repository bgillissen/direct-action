
params ["_misId", "_tubeId", "_action"];

private _missionIdx = -1;
{
	if ( (_x select 0) isEqualTo _misId ) exitWith { _missionIdx = _forEachIndex; };
} forEach artiSupport_missions;

if ( _missionIdx < 0 ) exitWith { false };

(artiSupport_missions select _missionIdx) params ["_misId", "_name", "_owner", "_lastEditBy", "_grid", "_cor", "_ord", "_mode", "_requestedBy", "_grantedBy", "_executedBy"];

(artiSupport_tubes select _tubeId) params ["_batId", "_name", "_veh", "_group", "_unit"];

private _misShell = "";
{
	_x params ["_ordTubeId", "_ordShell"];
	if ( _ordTubeId isEqualTo _tubeId ) exitWith { _misShell = _ordShell; };
} forEach _ord;

(_veh getVariable format["as_%1", _action]) params ['_done', '_start', '_duration', '_isDone', ['_pGrid', []], ['_pCor', []], ['_plotShell', '']];

if ( _done isEqualTo "" ) exitWith { false };

private "_ok";

switch ( _action ) do {
	case "load" : { _ok = (_done isEqualTo _misShell); };
    case "plot" : { _ok = (_done isEqualTo _misId);
    				if ( _ok ) then { _ok = (_pGrid isEqualTo _grid); };
                    if ( _ok ) then { _ok = (_pCor isEqualTo _cor); };
            		if ( _ok ) then { _ok = (_plotShell isEqualTo _misShell); };
                  };
    case "prep" : { _ok = ( _done isEqualTo _misId );
    				if ( _ok ) then { _ok = (_pGrid isEqualTo _grid); };
                    if ( _ok ) then { _ok = (_pCor isEqualTo _cor); };
    			  };
};

_ok