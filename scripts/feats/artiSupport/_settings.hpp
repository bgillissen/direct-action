
class artiSupport {
	crew = "C_man_pilot_F";
	skill = 6;
	maxdistance = 100;
	modes[] = {"ASAP", "Sequencial", "Wave"};
	class messages {
		take = "%1 has taken command over the %2";
		release = "%1 has released command over the %2";
		hud = "<t color='#FFFFFF' size='1.2' align='center'>%1 Pending Artillery Request</t>";
	};
	class timings {
		class plot {
			min = 15;
			max = 25;
		};
		class prep {
			min = 10;
			max = 20;
		};
		class load {
			min = 5;
			max = 15;
		};
	};
	class spreads {
		none = 0;
		small = 10;
		medium = 20;
		large = 35;
	};
	class restrictions {
		control[] = {"sl", "tl", "jtac", "hq"}; 	//who can open the artiSupport control panel
		request[] = {"sl", "tl", "jtac", "mortar"};	//who can request a stryke
		command[] = {"mortar"};						//who can take command of a battery (once commanding they can open the control panel)
		execute[] = {"sl", "mortar", "jtac", "hq"};	//who can execute a stryke
		acceptance[] = {"hq"};						//who can accept a stryke request
	};
	#include "settings\Altis\_settings.hpp"
	#include "settings\Malden\_settings.hpp"
	#include "settings\Nam2\_settings.hpp"
	#include "settings\Tanoa\_settings.hpp"
	#include "settings\virtualReality\_settings.hpp"
};
