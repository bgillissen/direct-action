/*
@filename: feats\artiSupport\_params.hpp
Author:
	Ben
Description:
		included by feats\_params.hpp
*/

class artiSupport {
	title = "Artillery Support";
	values[] = {0, 1};
	texts[] = {"Disabled", "Enabled"};
	default = 1;
};
class artiSupport_grant {
	title = "Artillery Support strykes got to be granted by HQ (if present)";
	values[] = {0, 1};
	texts[] = {"No", "Yes"};
	default = 1;
};
