/*
@filename: feats\artiSupport\serverInit.sqf
Author:
	Ben
Description:
	run on server,
*/

#include "_debug.hpp"

if ( (["artiSupport"] call core_fnc_getParam) isEqualTo 0 ) exitWith {
	#ifdef DEBUG
   	debug(LL_INFO, "disabled by mission parameter");
    #endif
    nil  
};

artiSupport_batteries = [];
artiSupport_commanders = [];
artiSupport_tubes = [];
artiSupport_tubeShells = [];
artiSupport_tubeThreads = [];
artiSupport_missions = [];
artiSupport_missionThreads = [];

private _baseConf = (missionConfigFile >> "settings" >> "artiSupport" >> toUpper(worldName) >> BASE_NAME);

private _skill = ["artiSupport", "skill"] call core_fnc_getSetting;
private _crew = ["artiSupport", "crew"] call core_fnc_getSetting;

private _vname = format["RL_crew_%1", PLAYER_SIDE];
private _loadout =  missionNamespace getVariable _vname;

if ( isNil "_loadout" ) exitWith {
	#ifdef DEBUG
    private _debug = format["roleLoadout variable '%1' is not defined, abording", _vname];
   	debug(LL_ERR, _debug);
    #endif
    nil	    
};

_loadout params["_u", "_v", "_b", "_pw", "_sw", "_hw", "_h", "_f", "_c", "_t", "_m", "_bino", "_n", "_w", "_cp"];

private _tubeId = 0;
{
    private _batConf = _x;
    private _batId = getNumber(_batConf >> "batId");
    private _pool = getText(_batConf >> "pool");
	private _count = 0;
	{    
	    private _marker = _x;
	    private _pos = getMarkerPos _marker;
	    if !( _pos isEqualTo [0,0,0] ) then {
            private _dir = (markerDir _marker);
	        private _poolData = missionNamespace getVariable[format["BV_%1", _pool], []];
	        if !( count _poolData isEqualTo 0 ) then {
		        private _veh = createVehicle [(selectRandom _poolData), [0,0,2000], [], 0, "NONE"];
		        if !( isNull _veh ) then {
		        	_veh allowDamage false;
		        	_veh setDir _dir;
					_veh setPos _pos;
		            _veh setFuel 0;
					_veh lock 3;
                    _veh setVariable ["as_pos", _pos, false];
                    _veh setVariable ["as_dir", _dir, false];
		            clearWeaponCargoGlobal _veh;
					clearMagazineCargoGlobal _veh;
					clearItemCargoGlobal _veh;
					clearBackpackCargoGlobal _veh;
					_veh addEventHandler ["Fired", {(_this select 0) setVehicleAmmo 1;}];
		            _veh setVariable ["NOCLEANUP", true, true];
                    _veh setVariable ["NOVR", true, true];
                    _veh setVariable ["NOBP", true, true];
                    _veh setVariable ["as_batId", _batId, true];
        			_veh setVariable ["as_tubId", _tubeId, true];
            		_veh setVariable ["as_exec", ["", false], true];
        			_veh setVariable ["as_plot", ["", 0, 0, false, [0,0,0], [0,0], ""], true];
            		_veh setVariable ["as_prep", ["", 0, 0, false, [0,0,0], [0,0]], true];
                    _veh setVariable ["as_aim", ["", [0,0,0], "", "", 0], true];
					private _grp = createGroup PLAYER_SIDE;
		            if !( isNull _grp ) then {
                        _grp setGroupIdGlobal [([(format["Artillery %1 - %2",(configName _batConf), _count]), allGroups, {groupId _this}] call common_fnc_getUniqueName)];
		                _grp setVariable ["NOLB", true];
                        _grp allowFleeing 0;
		            	_grp setCombatMode "BLUE";
                        _grp setBehaviour "CARELESS";
                        _grp setSpeedMode "LIMITED"; 
		        		private _unit = _grp createUnit ["C_man_pilot_F", _pos, [], 0, "NONE"];
		                if !( isNull _unit ) then {
							_unit allowDamage false;
                            _unit setSkill 1;
		                    [_unit] joinSilent _grp;
		                	_unit setVariable ["NOAI", true, true];
                            { _unit disableAI _x; } count ["TARGET", "AUTOTARGET", "FSM", "SUPPRESSION", "COVER", "AIMINGERROR"];
		                	[_unit, _u, _v, ["", []], ["", []], ["", []], ["", []], _h, _f, _c, _t, _m, "", _n, _w, _cp] call common_fnc_setLoadout;
							_unit assignAsGunner _veh;
							_unit moveInGunner _veh; 
                            _unit addEventHandler ["firedMan", {_this call as_fnc_gunnerShot;}];   
		                    private _name = format["%1 (%2)", name _unit, getText(configFile >> "cfgVehicles" >> (typeOf _veh) >> "displayName")];                                            
		        			artiSupport_tubes pushback [_batId, _name, _veh, _grp, _unit];
		        			artiSupport_tubeThreads pushback [scriptNull, scriptNull, scriptNull];
		        			_count = _count + 1;
                            _tubeId = _tubeId + 1;
                            [_unit, _veh] spawn {
                                params ["_unit", "_veh"];
                                sleep 2;
                            	private _turretPath = ((assignedVehicleRole _unit) select 1);
                            	private _loadedMag = ((weaponState [_veh, _turretPath]) select 3);
                    			_veh setVariable ['as_load', [_loadedMag, 0, 0, true], true];
                                //[[_veh], true] call curator_fnc_addEditable;
							};
						#ifdef DEBUG
		                	private _debug = format["tube created @ %1 for battery %2", _marker, _batId];
		   					debug(LL_DEBUG, _debug);
		    			} else { 
		   					debug(LL_DEBUG, "unit creation failed");
		    			#endif 
						};
					#ifdef DEBUG
		    		} else { 
		   				debug(LL_DEBUG, "group creation failed");
		    		#endif 
					};
				#ifdef DEBUG
		    	} else {
 		   			debug(LL_DEBUG, "tube / anchor creation failed");
		    	#endif 
		        };
			#ifdef DEBUG
		    } else { 
		   		debug(LL_DEBUG, "vehicle pool is empty");
		    #endif 
			};
		#ifdef DEBUG
	    } else { 
	    	private _debug = format["marker %1 does not exists", _marker];
	   		debug(LL_DEBUG, _debug);
	    #endif 
		};
    } forEach getArray(_batConf >> "tubes");
    if ( _count > 0 ) then {
    	private _name = getText(_batConf >> "name"); 
    	private _plot = getNumber(_batConf >> "plot");
    	private _prep = getNumber(_batConf >> "prepare");
    	private _load = getNumber(_batConf >> "reload");
    	private _acc = getNumber(_batConf >> "accuracy");
    	artiSupport_batteries pushback [_name, _plot, _prep, _load, _acc];
	};
} forEach (configProperties [_baseConf, "isClass _x", true]);

publicVariable "artiSupport_batteries";
publicVariable "artiSupport_commanders";
publicVariable "artiSupport_tubes";
publicVariable "artiSupport_tubeShells";
publicVariable "artiSupport_missions";

nil

/*
missionNamespace >> artiSupport_batteries (public)
 	[[name, plotFactor, prepFactor, loadFactor, accuracyFactor]]

missionNamespace >> artiSupport_commanders (public)
 	[[batId, playerUID]]

missionNamespace >> artiSupport_tubes (public)
 	[[batId, name, veh, group, unit]]

veh >> as_plot  (public)
	[misId, plotStart, plotDuration, isPloted, plotGrid, plotShell]

veh >> as_prep  (public)
	[misId, prepStart, prepDuration, isPrepared]

veh >> as_load  (public)
	[shell, reloadStart, reloadDuration, isLoaded]]

veh >> as_exec  (public)
	[missionId, isExecuting]

veh >> as_aim  (public)
	[misId, aimPos, weapon, mode, muzzle, eta]

missionNamespace >> artiSupport_tubeThreads (server)
 	[[plotThread, prepThread, reloadThread]]

missionNamespace >> artiSupport_tubeShells (public)
	[[tubeId, shell, shotAt, eta]]

missionNamespace >> artiSupport_missions (public)
 	[[missionId, name, owner, lastEditBy, [gridX, gridY, gridZ], [corX, corY], [[tubIdx, shell, amount, spread]], [mode, delay], requestedBy, grantedBy, executedBy]]

missionNamespace >> artiSupport_missionThreads (server)
 	[[missionId, exeThread]]

missionNamespace >> artiSupport_uiMenu (player)
	screenIDC 

missionNamespace >> artiSupport_uiMission (player)
	missionId 

missionNamespace >> artiSupport_uiBattery (player)
	batId 

missionNamespace >> artiSupport_etaThread (player)
	etaThread

missionNamespace >> artiSupport_uiBuffer (player)
	[name, [gridX, gridY, gridZ], [corX, corY], [[tubeId, shell, amount, spread]], [mode, delay]]
*/