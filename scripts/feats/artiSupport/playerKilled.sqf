/*
@filename: feats\artiSupport\playerKilled.sqf
Author:
	Ben
Description:
	run on player,
*/

#include "_debug.hpp"

if ( (["artiSupport"] call core_fnc_getParam) isEqualTo 0 ) exitWith {
	#ifdef DEBUG
   	debug(LL_INFO, "disabled by mission parameter");
    #endif
    nil  
};

private _uid = (getPlayerUid player);
{
	_x params ["_batId", '_puid'];
	if ( _uid isEqualTo  _puid ) exitWith {
		terminate artiSupport_distanceThread;
	    artiSupport_commanders deleteAt _forEachIndex;
		publicVariable "artiSupport_commanders";
        {
    		_x params [_tubeBatId, _name, _veh, _grp, _unit];
    		if ( _tubeBatId isEqualTo _batId ) then {
                _veh removeAction (_veh getVariable ['as_actTakeOver', -1]);
                _veh setVariable ['as_actGetIn', -1, false];
                if !( (vehicle _unit) isEqualTo _veh ) then {
                	_unit assignAsGunner _veh;
					[_unit] orderGetIn true;
				};
			};
        } forEach artiSupport_tubes;                    	    
	};
} forEach artiSupport_commanders;

nil