/*
@filename: feats\artiSupport\playerDestroy.sqf
Author:
	Ben
Description:
	run on server,
*/

#include "_debug.hpp"

if ( (["artiSupport"] call core_fnc_getParam) isEqualTo 0 ) exitWith {
	#ifdef DEBUG
   	debug(LL_INFO, "disabled by mission parameter");
    #endif
    nil  
};

if ( (count artiSupport_tubes) isEqualTo 0 ) exitWith { 
	#ifdef DEBUG
   	debug(LL_INFO, "no tubes available, abording");
    #endif
    nil  
};

['artiSupport_missions', artiSupport_misPVEH] call pveh_fnc_del;
artiSupport_misPVEH = nil;

player removeAction AS_ctrlAction;
AS_ctrlAction = -1;

player removeAction AS_grantAction;
AS_grantAction = -1;

nil