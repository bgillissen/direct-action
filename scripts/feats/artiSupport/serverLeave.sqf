/*
@filename: feats\artiSupport\serverLeave.sqf
Author:
	Ben
Description:
	run on server
*/

#include "_debug.hpp"

params ["_unit", "_id", "_uid", "_name"];

if ( (["artiSupport"] call core_fnc_getParam) isEqualTo 0 ) exitWith {
	#ifdef DEBUG
   	debug(LL_INFO, "disabled by mission parameter");
    #endif
    nil  
};

{
	_x params ["_batId", '_puid'];
	if ( _uid isEqualTo  _puid ) exitWith {
	    artiSupport_commanders deleteAt _forEachIndex;
		publicVariable "artiSupport_commanders";
		{
    		_x params [_tubeBatId, _name, _veh, _grp, _unit];
    		if ( (_tubeBatId isEqualTo _batId) && !((vehicle _unit) isEqualTo _veh) ) then {
				_unit assignAsGunner _veh;
				[_unit] orderGetIn true;
			};
        } forEach artiSupport_tubes;
	};
} forEach artiSupport_commanders;

nil