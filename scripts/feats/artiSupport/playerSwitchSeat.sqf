/*
@filename: feats\artiSupport\playerSwitchSeat.sqf
Author:
	Ben
Description:
	run on player,
	move player back to gunner seat!
*/

params ["_unit", "_notneeded", "_veh"];

if ( isNull artiSupport_inTube ) exitWith { nil };

if !( _veh isEqualTo artiSupport_inTube ) exitWith { nil };

if !( player isEqualTo (gunner artiSupport_inTube) ) then {
    systemChat "Only the gunner seat is allowed on battery tubes !";
    player action ["moveToGunner", _veh]; 
};

nil