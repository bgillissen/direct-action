class artiSupport : feat_base  {
	class player : featPlayer_base {
		class init : featEvent_enable {};
		class killed : featEvent_enable {};
		class respawn : featEvent_enable {};
		class switchSeat : featEvent_enable {};
		class destroy : featEvent_enable {};
	};
	class server : featServer_base {
		class init : featEvent_enable {};
		class leave : featEvent_enable {};
	};
};
