/*
@filename: feats\cleanUp\clean.sqf
Author:
	Ben
Description:
	run on server
	cleanup mission garbage
*/

#include "_debug.hpp"

private _dist = (["cleanUp", "distance"] call core_fnc_getSetting);
private _forcedZones = [];
{
	_x params ["_area", "_actions"];
    if ( "cleanForced" in _actions ) then { _forcedZones pushback _area; };
} forEach BA_zones;


private _mineExclusions = [];
private _rtMarker = ["ia", "ao", "radioTower", "circle"] call core_fnc_getSetting;
 if !( (getMarkerPos _rtMarker) isEqualTo [0,0,0] ) then { _mineExclusions pushback _rtMarker; };
if !( isNil "SIDE_mineMarker" ) then {
	if !( SIDE_mineMarker isEqualTo "" ) then { _mineExclusions pushback SIDE_mineMarker; };
};
private _obj  = 0;
private _unit = 0;
private _grp  = 0;

_obj = _obj + ({ [_x, _forcedZones, [], _dist] call cleanUp_fnc_delObject; } count (allMissionObjects "CraterLong") );
_obj = _obj + ({ [_x, _forcedZones, [], _dist] call cleanUp_fnc_delObject; } count (allMissionObjects "WeaponHolder") );
_obj = _obj + ({ [_x, _forcedZones, [], _dist] call cleanUp_fnc_delObject; } count (allMissionObjects "WeaponHolderSimulated") );
_obj = _obj + ({ [_x, _forcedZones, [], _dist] call cleanUp_fnc_delObject; } count (allMissionObjects "StaticWeapon") );
_obj = _obj + ({ [_x, _forcedZones, _mineExclusions, _dist] call cleanUp_fnc_delObject; } count allMines);
_unit = _unit + ({ [_x, _forcedZones, [], _dist] call cleanUp_fnc_delObject; } count allDead);

private _noEnemyZones = [];
{
	_x params ["_area", "_actions"];
    if ( "noEnemy" in _actions ) then { _noEnemyZones pushback _area; };
} forEach BA_zones;

{
	if !( _x getVariable['NOCLEANUP', false] ) then {
		if ( {alive _x} count (units _x) == 0 ) then {
			_grp = _grp + 1;
			_x remoteExec["cleanup_fnc_delGroup", (groupOwner _x), false];
		};
        if ( (side _x) in ENEMIES ) then {
			{
				private _u = _x;
				if ( ({( _u inArea _x )} count _noEnemyZones) > 0 ) then {
					_unit = _unit + 1;
	           		deleteVehicle _u;  
				};
			} forEach (units _x);
        };
	};
} forEach allGroups;

#ifdef DEBUG
private _debug = format["%1 unit(s), %2 object(s), %3 group(s) removed", _unit, _obj, _grp];
debug(LL_DEBUG, _debug);
#endif