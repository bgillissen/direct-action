/*
@filename: feats\cleanUp\serverInit.sqf
Author:
	Ben
Description:
	run on server
	cleanup garbage thread
*/

#include "_debug.hpp"

cleanUp = true;
publicVariable "cleanUp";

private _delay = (["cleanUp", "loopDelay"] call core_fnc_getSetting);

#ifdef DEBUG
private _debug = format["thread has started with %1s delay between cleanups", _delay];
debug(LL_DEBUG, _debug);
#endif

while { true } do {
	sleep _delay;
    if ( cleanUp ) then { call cleanUp_fnc_clean; };
};

nil