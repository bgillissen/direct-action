/*
@filename: feats\cleanup\serverDestroy.sqf
Author:
	Ben
Description:
	run on server,
	kill the cleanup serverInit thread
 */

if ( count(_this) == 0 ) exitWith {};

params ["_when", "_thread"];

if ( scriptDone _thread ) exitWith {};

terminate _thread;

waitUntil{ scriptDone _thread };

nil