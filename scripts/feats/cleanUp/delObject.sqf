/*
@filename: feats\cleanUp\delObject.sqf
Author:
	Ben
Description:
	run on server
	check the given object against forced zones, exclude zone and players distance
	if in one of the given zones and no player within given distance remove it
	exludeZone : if object is in the zone, it will not be deleted
	forceZone : if object is in the zone, it will deleted, even if player are within _playerDist distance
	the object in none of the zones above will only be deleted if no player are within _playerDist distance
*/

params[["_obj", objNull], ["_ForceZones", []], ["_ExcludeZones", []], ["_playerDist", 0]];

if ( _obj getVariable['NOCLEANUP', false] ) exitWith { false };

private _skip = false;
if ( _obj isKindOf "StaticWeapon" ) then { _skip = ( count (crew _obj) > 0 ); };

if ( _skip ) exitWith { false };

private _pos = getPos _obj;

private _excluded = false;
{
	if ( _pos inArea _x ) exitWith { _excluded = true; };    
} forEach _ExcludeZones;

if ( _excluded ) exitWith { false };

private _del = false;
{
    if ( _pos inArea _x ) exitWith {
		_del = true;
        deleteVehicle _obj;
	};
} forEach _ForceZones;
 
if ( _del ) exitWith { true };
 
if ( _playerDist isEqualTo 0 ) exitWith { false };
 
if ( ({(_pos distance (getPos _x)) <= _playerDist} count allPlayers) == 0 ) then {
    _del = true; 
	deleteVehicle _obj; 
};

_del