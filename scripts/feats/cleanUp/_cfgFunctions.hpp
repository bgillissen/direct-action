class cleanUp {
	tag = "cleanUp";
	class functions {
		class clean { file="feats\cleanUp\clean.sqf"; };
		class delObject { file="feats\cleanUp\delObject.sqf"; };
		class delGroup { file="feats\cleanUp\delGroup.sqf"; };
		class playerInit { file="feats\cleanUp\playerInit.sqf"; };
		class serverDestroy { file="feats\cleanUp\serverDestroy.sqf"; };
		class serverInit { file="feats\cleanUp\serverInit.sqf"; };
	};
};
