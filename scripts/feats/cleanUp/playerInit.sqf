/*
@filename: feats\cleanup\playerInit.sqf
Author:
	Ben
Description:
	run on player
 	add cleanup actions to base objects
*/

#include "_debug.hpp"

private _now = (["cleanUp", "now"] call core_fnc_getSetting);
private _pause = (["cleanUp", "pause"] call core_fnc_getSetting);
private _resume = (["cleanUp", "resume"] call core_fnc_getSetting);
{
	{
		_x params ["_thing", "_actions"];
		{
			if ( (configName _x) isEqualTo "cleanup" ) then {
                #ifdef DEBUG
				private _debug = format["adding actions to %1", _thing];
				debug(LL_DEBUG, _debug);
				#endif
				_thing addAction [_now, {remoteExec['cleanUp_fnc_clean', 2, false];systemChat "Cleanup script has been executed.";}, true, 0, false, true, "", "((call BIS_fnc_admin) > 0) || isAssigned", 4];
                _thing addAction [_pause, {cleanUp = false; publicVariable "cleanUp";}, true, 0, false, true, "", "( (((call BIS_fnc_admin) > 0) || isAssigned) && cleanUp )", 4];
                _thing addAction [_resume, {cleanUp = true; publicVariable "cleanUp";}, true, 0, false, true, "", "( (((call BIS_fnc_admin) > 0) || isAssigned) && !cleanUp )", 4];
			};
		} forEach _actions;
	} forEach _x;
} forEach [BA_npc, BA_obj, BA_veh];

nil