/*
@filename: feats\assets\_params.hpp
Author:
	Ben
Description:
		included by feats\_params.hpp
		include individual mods parameters
*/


#include "vanilla\_params.hpp"
#include "kart\_params.hpp"
#include "heli\_params.hpp"
#include "marksmen\_params.hpp"
#include "apex\_params.hpp"
#include "jet\_params.hpp"
#include "orange\_params.hpp"
#include "tanks\_params.hpp"
