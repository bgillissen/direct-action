class spawn_vanilla {
	title = "Add vanilla assets to enemy spawn pool";
	values[] = {0,1};
	texts[] = {"no", "yes"};
	default = 1;
};

class vanilla_modEnemy {
	title = "Vanilla units are spawned when mods (enemy side) are present";
	values[] = {0,1};
	texts[] = {"no", "yes"};
	default = 0;
};

class gear_vanilla {
	title = "Which vanilla gear is available in arsenal";
	values[] = {0,1,2,3};
	texts[] = {"none", "player side only", "player side + allies", "all"};
	default = 1;
};

class vanilla_modgear {
	title = "Vanilla gear are available when mods (player side) are present";
	values[] = {0,1};
	texts[] = {"no", "yes"};
	default = 0;
};

class vanilla_modrl {
	title = "Player spawn with vanilla loadout when mods (player side) are present";
	values[] = {0,1};
	texts[] = {"no", "yes"};
	default = 0;
};
class vanilla_modbalo {
	title = "base NPC spawn with vanilla loadout when mods (player side) are present";
	values[] = {0,1};
	texts[] = {"no", "yes"};
	default = 0;
};

class vanilla_modbv {
	title = "Vanilla vehicle are available when mods (player side) are present";
	values[] = {0,1};
	texts[] = {"no", "yes"};
	default = 0;
};
class vanilla_modreward {
	title = "Vanilla rewards are available when mods (player side) are present";
	values[] = {0,1};
	texts[] = {"no", "yes"};
	default = 0;
};

class reward_vanilla {
	title = "Add vanilla vehicles to reward pool";
	values[] = {0,1,2,3};
	texts[] = {"no", "player side only", "player side + allies", "all"};
	default = 2;
};

class bv_vanilla {
	title = "Add vanilla vehicle to base vehicle pool";
	values[] = {0,1,2,3};
	texts[] = {"no", "player side only", "player side + allies", "all"};
	default = 2;
};

class rl_vanilla {
	title = "Players spawn with vanilla loadout";
	values[] = {0,1};
	texts[] = {"no", "yes"};
	default = 1;
};
