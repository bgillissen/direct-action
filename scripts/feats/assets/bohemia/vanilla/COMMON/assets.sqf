/*
@filename: feats\assets\bohemia\vanilla\COMMON\assets.sqf
Author:
	Ben
Description:
	run on server,
	return the vanilla assets common to all side
*/

private _out = [];

private _isWood = (["jungle", "wood"] call assets_fnc_mapKeywords);

//------------------------------------------------------------ Arsenal Vanilla (common)

A_VC = 0;

private _backpacks = (call vc_fnc_getBackpacks);
private _items = (call vc_fnc_getItems);
private _weapons = (call vc_fnc_getWeapons);
private _ammo = (call vc_fnc_getMagazines);

_out set [A_VC, [_backpacks, _items, _weapons, _ammo]];

//------------------------------------------------------------ Allowed Vehicles Vanilla (common)

AV_VC = A_VC + 1;

private _heli = [];
private _plane = [];
private _tank = [];

_out set [AV_VC, [_heli, _plane, _tank]];

//------------------------------------------------------------ supplyDrop Vanilla (common)

SD_VC = AV_VC + 1;

private _backpacks = [];
private _items = [["Laserdesignator", 3],
                  ["Laserbatteries", 5],
                  ["FirstAidKit", 20],
                  ["Medikit", 1],
                  ["ToolKit", 1]];
private _weapons = [];
private _ammo = [["SatchelCharge_Remote_Mag", 2],
                 ["DemoCharge_Remote_Mag", 5],
                 ["HandGrenade", 10]];
private _crates = ["B_supplyCrate_F"];
private _vehicles = [];

_out set [SD_VC, [ _backpacks, _items, _weapons, _ammo, _crates, _vehicles]];

//------------------------------------------------------------ Rewards Vanilla (common)

R_VC = SD_VC + 1;

private _rewards = ["C_Offroad_02_unarmed_F", "C_Offroad_01_F", "C_Offroad_01_repair_F", "C_Hatchback_01_sport_F", "C_Hatchback_01_F"];

_out set [R_VC, _rewards];

//------------------------------------------------------------ Vehicles Vanilla  (common)

BV_VC = R_VC + 1;

private _car = [];
private _carArmed = [];
private _apc = [];
private _tank = [];
private _aaTank = [];
private _planeCAS = [];
private _planeAA = [];
private _planeTransport = [];
private _uav = [];
private _heliSmall = [];
private _heliSmallArmed = [];
private _heliMedium = [];
private _heliMedEvac = [];
private _heliBig = [];
private _heliAttack = [];
private _boatSmall = ["B_Boat_Transport_01_F"];
private _boatAttack = ["B_Boat_Armed_01_minigun_F"];
private _boatBig = [];
private _sub = ["B_SDV_01_F"];
private _landMedic = [];
private _repair = [];
private _fuel = [];
private _ammo = [];
private _truck = [];
private _quad = ["B_Quadbike_01_F"];
private _artiTank = [];
private _artiCannon = [];
private _artiTube = [];

_out set [BV_VC, [_car, _carArmed, _apc, _tank, _aaTank, _planeCAS, _planeAA, _planeTransport, _uav,
                _heliSmall, _heliSmallArmed, _heliMedium, _heliMedEvac, _heliBig, _heliAttack,
                _boatSmall, _boatAttack, _boatBig, _sub, _landMedic, _repair, _fuel, _ammo, _truck, _quad,
                _artiTank, _artiCannon, _artiTube]];

//------------------------------------------------------------ Vehicles Cargo Vanilla (common)

VC_VC = BV_VC + 1;

private _nv = "NVGoggles";
if ( _isWood ) then {  _nv = "NVGoggles_INDEP"; };
 
private _car = [[],
                [["FirstAidKit", 20], ["Laserdesignator", 1], ["MineDetector", 1], [_nv, 2]],
                [],
                [["Laserbatteries", 3],["HandGrenade", 10]]];
private _carArmed = [[],
                     [["FirstAidKit", 20], ["Laserdesignator", 1], ["MineDetector", 1], [_nv, 2]],
                     [],
                     [["Laserbatteries", 3],["HandGrenade", 10]]];
private _apc = [[],
                [["FirstAidKit", 20], ["Laserdesignator", 1], ["MineDetector", 1], [_nv, 2]],
                [],
                [["Laserbatteries", 3],["HandGrenade", 10]]];
private _tank = [[],
                 [["FirstAidKit", 20], ["ToolKit", 1], ["Laserdesignator", 1], ["MineDetector", 1], [_nv, 2]],
                 [],
                 [["Laserbatteries", 3],["HandGrenade", 10]]];
private _aaTank = [[],
                   [["FirstAidKit", 20], ["Laserdesignator", 1], ["MineDetector", 1], [_nv, 2]],
                   [],
                   [["Laserbatteries", 3],["HandGrenade", 10]]];
private _planeCAS = [[],[],[],[]];
private _planeAA = [[],[],[],[]];
private _planeTransport = [[["B_Parachute", 10]],
                           [["FirstAidKit", 20]],
                           [],
                           []];
private _uav = [[],[],[],[]];
private _heliSmall = [[],
                      [["FirstAidKit", 10], ["Laserdesignator", 1], ["MineDetector", 1], [_nv, 2]],
                      [],
                      [["Laserbatteries", 3],["HandGrenade", 10]]];
private _heliSmallArmed = [[],
                           [["FirstAidKit", 5]],
                           [],
                           [["HandGrenade", 10]]];
private _heliMedium = [[],
                       [["FirstAidKit", 10], ["Laserdesignator", 1], ["MineDetector", 1], [_nv, 2]],
                       [],
                       [["Laserbatteries", 3],["HandGrenade", 10]]];
private _heliMedEvac = [[],
                        [["FirstAidKit", 30], ["Medikit", 2]],
                         [],
                         []];
private _heliBig = [[],
                    [["FirstAidKit", 20], ["Laserdesignator", 1], ["MineDetector", 1], [_nv, 2]],
                    [],
                    [["Laserbatteries", 3]]];
private _heliAttack = [[],[],[],[]];
private _boatSmall = [[],
                      [["FirstAidKit", 10]],
                      [],
                      []];
private _boatAttack = [[],
                       [["FirstAidKit", 10]],
                       [],
                       [["HandGrenade", 10]]];
private _boatBig = [[],
                    [["FirstAidKit", 20], ["Laserdesignator", 1], ["MineDetector", 1], [_nv, 2],["HandGrenade", 10]],
                    [],
                    [["Laserbatteries", 3],["HandGrenade", 10]]];
private _sub = [[],[],[],[]];
private _landMedic = [[],
                      [["FirstAidKit", 30], ["Medikit", 2]],
                      [],
                      []];
private _repair = [[],[],[],[]];
private _fuel = [[],[],[],[]];
private _ammo = [[],[],[],[]];
private _truck = [[],[],[],[]];
private _quad = [[],[],[],[]];
private _artiTank = [[],[],[],[]];
private _artiCannon = [[],[],[],[]];
private _artiTube = [[],[],[],[]];

_out set [VC_VC, [_car, _carArmed, _apc, _tank, _aaTank, _planeCAS, _planeAA, _planeTransport, _uav,
                 _heliSmall, _heliSmallArmed, _heliMedium, _heliMedEvac, _heliBig, _heliAttack,
                 _boatSmall, _boatAttack, _boatBig, _sub, _landMedic, _repair, _fuel, _ammo, _truck, _quad,
                 _artiTank, _artiCannon, _artiTube]];

//------------------------------------------------------------ Role Loadout Vanilla Common
/*
 [uniform, [inUniform]], 
 [vest, inVest]], 
 [backpack, [inBackpack]], 
 [primWeapon, [muzzle, bipod, acc, scope, ammo2, ammo]] 
 [secWeapon, [muzzle, bipod, acc, scope, ammo]], 
 [handWeapon, [muzzle, bipod, acc, scope, ammo]], 
 helmet, face, comm, terminal, map, bino, nv, watch, compass
*/
RL_VC = VC_VC + 1;

private _hq = [[nil, [["FirstAidKit", 2]]],
               [nil, [["NVGoggles_OPFOR", 1]]],
               [nil, []],
               [nil, []],
               [nil, []],
               [nil, []],
               nil, nil, "ItemRadio", "ItemGPS", "ItemMap", "Rangefinder", nil, "ItemWatch", "ItemCompass"];
private _sl = [[nil, [["FirstAidKit", 2]]],
               [nil, [["Laserbatteries", 2], ["UGL_FlareWhite_F", 2], ["1Rnd_Smoke_Grenade_shell", 4]]],
               [nil, [["FirstAidKit", 4], ["Chemlight_blue", 2], ["SmokeShellBlue", 2], ["SmokeShell", 5]]],
               [nil, []],
               [nil, []],
               [nil, []],
               nil, nil, "ItemRadio", "ItemGPS", "ItemMap", "Laserdesignator", nil, "ItemWatch", "ItemCompass"];
private _tl = [[nil, [["FirstAidKit", 2]]],
               [nil, [["Laserbatteries", 2], ["UGL_FlareWhite_F", 2], ["1Rnd_Smoke_Grenade_shell", 4]]],
               [nil, [["FirstAidKit", 4], ["Chemlight_blue", 2], ["SmokeShellBlue", 2], ["SmokeShell", 5]]],
               [nil, []],
               [nil, []],
               [nil, []],
               nil, nil, "ItemRadio", "ItemGPS", "ItemMap", "Laserdesignator", nil, "ItemWatch", "ItemCompass"];
private _medic = [[nil, [["FirstAidKit", 2]]],
                  [nil, [["HandGrenade", 2]]],
                  ["B_Kitbag_mcamo", [["Medikit", 1], ["FirstAidKit", 15], ["SmokeShell", 15]]],
                  [nil, []],
                  [nil, []],
                  [nil, []],
                  nil, nil, "ItemRadio", "ItemGPS", "ItemMap", "Binocular", nil, "ItemWatch", "ItemCompass"];
private _lmg = [[nil, [["FirstAidKit", 2]]],
                [nil, [["HandGrenade", 2], ["SmokeShell", 2]]],
                [nil, []],
                [nil, []],
                [nil, []],
                [nil, []],
                nil, nil, "ItemRadio", "ItemGPS", "ItemMap", "Binocular", nil, "ItemWatch", "ItemCompass"];
private _hmg = [[nil, [["FirstAidKit", 2]]],
                [nil, [["HandGrenade", 2], ["SmokeShell", 2]]],
                [nil, []],
                [nil, []],
                [nil, []],
                [nil, []],
                nil, nil, "ItemRadio", "ItemGPS", "ItemMap", "Binocular", nil, "ItemWatch", "ItemCompass"];
private _assHMG = [[nil, [["FirstAidKit", 2]]],
                   [nil, [["HandGrenade", 2], ["SmokeShell", 3]]],
                   [nil, []],
                   [nil, []],
                   [nil, []],
                   [nil, []],
                   nil, nil, "ItemRadio", "ItemGPS", "ItemMap", "Rangefinder", nil, "ItemWatch", "ItemCompass"];
private _aa = [[nil, [["FirstAidKit", 2]]],
               [nil, [["HandGrenade", 2], ["SmokeShell", 3]]],
               [nil, []],
               [nil, []],
               [nil, []],
               [nil, []],
               nil, nil, "ItemRadio", "ItemGPS", "ItemMap", "Rangefinder", nil, "ItemWatch", "ItemCompass"];
private _assAA = [[nil, [["FirstAidKit", 2]]],
                  [nil, [["HandGrenade", 2], ["SmokeShell", 3]]],
                  [nil, []],
                  [nil, []],
                  [nil, []],
                  [nil, []],
                  nil, nil, "ItemRadio", "ItemGPS", "ItemMap", "Rangefinder", nil, "ItemWatch", "ItemCompass"];
private _at = [[nil, [["FirstAidKit", 2]]],
               [nil, [["HandGrenade", 2], ["SmokeShell", 3]]],
               [nil, []],
               [nil, []],
               [nil, []],
               [nil, []],
               nil, nil, "ItemRadio", "ItemGPS", "ItemMap", "Rangefinder", nil, "ItemWatch", "ItemCompass"];
private _assAT = [[nil, [["FirstAidKit", 2]]],
                  [nil, [["HandGrenade", 2], ["SmokeShell", 3]]],
                  [nil, []],
                  [nil, []],
                  [nil, []],
                  [nil, []],
                  nil, nil, "ItemRadio", "ItemGPS", "ItemMap", "Rangefinder", nil, "ItemWatch", "ItemCompass"];
private _sniper = [[nil, [["FirstAidKit", 2]]],
                   [nil, [["Laserbatteries", 2], ["HandGrenade", 2], ["SmokeShell", 3]]],
                   [nil, []],
                   [nil, []],
                   [nil, []],
                   [nil, []],
                   nil, nil, "ItemRadio", "ItemGPS", "ItemMap", "Laserdesignator", nil, "ItemWatch", "ItemCompass"];
private _marksman = [[nil, [["FirstAidKit", 2]]],
                     [nil, [["Laserbatteries", 2], ["HandGrenade", 1], ["SmokeShell", 2]]],
                     [nil, []],
                     [nil, []],
                     [nil, []],
                     [nil, []],
                     nil, nil, "ItemRadio", "ItemGPS", "ItemMap", "Laserdesignator", nil, "ItemWatch", "ItemCompass"];
private _repair = [[nil, [["FirstAidKit", 2]]],
                   [nil, [["HandGrenade", 2], ["SmokeShell", 3]]],
                   [nil, [["ToolKit", 1]]],
                   [nil, []],
                   [nil, []],
                   [nil, []],
                   nil, nil, "ItemRadio", "ItemGPS", "ItemMap", "Binocular", nil, "ItemWatch", "ItemCompass"];
private _demo = [[nil, [["FirstAidKit", 2]]],
                 [nil, [["HandGrenade", 2], ["SmokeShell", 3]]],
                 [nil, [["MineDetector", 1], ["ToolKit", 1]]],
                 [nil, []],
                 [nil, []],
                 [nil, []],
                 nil, nil, "ItemRadio", "ItemGPS", "ItemMap", "Binocular", nil, "ItemWatch", "ItemCompass"];
private _engineer = [[nil, [["FirstAidKit", 2]]],
                     [nil, [["HandGrenade", 2], ["SmokeShell", 3]]],
                     [nil, [["MineDetector", 1], ["ToolKit", 1]]],
                     [nil, []],
                     [nil, []],
                     [nil, []],
                     nil, nil, "ItemRadio", "ItemGPS", "ItemMap", "Rangefinder", nil, "ItemWatch", "ItemCompass"];
private _grenadier = [[nil, [["FirstAidKit", 2]]],
                      [nil, [["UGL_FlareWhite_F", 1], ["SmokeShell", 3]]],
                      [nil, []],
                      [nil, []],
                      [nil, []],
                      [nil, []],
                      nil, nil, "ItemRadio", "ItemGPS", "ItemMap", "Rangefinder", nil, "ItemWatch", "ItemCompass"];
private _rifleman = [[nil, [["FirstAidKit", 2]]],
                     [nil, [["HandGrenade", 2], ["SmokeShell", 3]]],
                     [nil, []],
                     [nil, []],
                     [nil, []],
                     [nil, []],
                     nil, nil, "ItemRadio", "ItemGPS", "ItemMap", "Binocular", nil, "ItemWatch", "ItemCompass"];
private _jtac = [[nil, [["FirstAidKit", 2]]],
                 [nil, [["HandGrenade", 2], ["SmokeShell", 3]]],
                 [nil, [["Laserbatteries", 2], ["SmokeShellBlue", 4], ["SmokeShellGreen", 4], ["SmokeShellOrange", 4], ["SmokeShellPurple", 4], ["SmokeShellRed", 4], ["SmokeShell", 4], ["SmokeShellYellow", 4], ["Chemlight_blue", 2], ["Chemlight_green", 2], ["Chemlight_red", 2], ["Chemlight_yellow", 2]]],
                 [nil, []],
                 [nil, []],
                 [nil, []],
                 nil, nil, "ItemRadio", "ItemGPS", "ItemMap", "Laserdesignator", nil, "ItemWatch", "ItemCompass"];
private _hPilot = [[nil, [["FirstAidKit", 2]]],
                   ["V_PlateCarrier1_blk", [["SmokeShell", 2], ["DemoCharge_Remote_Mag", 1], ["Chemlight_blue", 2], ["Chemlight_red", 2], ["SmokeShellBlue", 2], ["SmokeShellOrange", 2], ["SmokeShellRed", 2]]],
                   [nil, []],
                   [nil, []],
                   [nil, []],
                   [nil, []],
                   nil, nil, "ItemRadio", "ItemGPS", "ItemMap", "Binocular", nil, "ItemWatch", "ItemCompass"];
private _jPilot = [[nil, [["FirstAidKit", 2]]],
                   [nil, [["SmokeShell", 5], ["Chemlight_blue", 2], ["Chemlight_red", 2], ["SmokeShellBlue", 2], ["SmokeShellOrange", 2], ["SmokeShellRed", 2]]],
                   [nil, []],
                   [nil, []],
                   [nil, []],
                   [nil, []],
                   "H_PilotHelmetFighter_B", nil, "ItemRadio", "ItemGPS", "ItemMap", "Binocular", nil, "ItemWatch", "ItemCompass"];
private _crew = [[nil, [["FirstAidKit", 2]]],
                 [nil, [["HandGrenade", 2], ["SmokeShell", 3]]],
                 [nil, [["ToolKit", 1]]],
                 [nil, []],
                 [nil, []],
                 [nil, []],
                 nil, nil, "ItemRadio", "ItemGPS", "ItemMap", "Rangefinder", nil, "ItemWatch", "ItemCompass"];
private _mortar = [[nil, [["FirstAidKit", 2]]],
                   [nil, [["HandGrenade", 2], ["SmokeShell", 3]]],
                   [nil, []],
                   [nil, []],
                   [nil, []],
                   [nil, []],
                   nil, nil, "ItemRadio", "ItemGPS", "ItemMap", "Rangefinder", nil, "ItemWatch", "ItemCompass"];
private _uavOp = [[nil, [["FirstAidKit", 2]]],
                   [nil, [["HandGrenade", 2], ["SmokeShell", 3]]],
                   [nil, []],
                   [nil, []],
                   [nil, []],
                   [nil, []],
                   nil, nil, "ItemRadio", nil, "ItemMap", "Binocular", nil, "ItemWatch", "ItemCompass"];
private _spotter = [[nil, [["FirstAidKit", 2]]],
                    [nil, [["HandGrenade", 1], ["SmokeShell", 2], ["Laserbatteries", 2]]],
                    [nil, []],
                    [nil, []],
                    [nil, []],
                    [nil, []],
                    nil, nil, "ItemRadio", "ItemGPS", "ItemMap", "Laserdesignator", nil, "ItemWatch", "ItemCompass"];

_out set [RL_VC, [_hq, _sl, _tl, _medic, _lmg, _hmg, _assHMG, _aa, _assAA, _at, _assAT, _sniper, _marksman,
                 _repair, _demo, _engineer, _grenadier, _rifleman, _jtac, _hPilot, _jPilot, _crew,
                 _mortar, _uavOp, _spotter]];

//------------------------------------------------------------ Base Atmosphere Role Vanilla Common

BALO_VC = RL_VC + 1;

private _medic = [];
private _gear = [];
private _support = [];
private _default = [];

_out set [BALO_VC, [_medic, _gear, _support, _default]];

//------------------------------------------------------------ Spawn Vanilla Common

S_VC = BALO_VC + 1;

private _rt = ["Land_TTowerBig_2_F"];
private _crates = ["Land_CargoBox_V1_F"];
private _pGroups = [];
private _sGroups = [];
private _pilot = [];
private _crew = [];
private _officer = [];
private _garrison = [];
private _aa = [];
private _arti = [];
private _static = [];
private _cas = [];
private _tank = [];
private _apc = [];
private _car = [];
private _carArmed = [];
private _aPatrol = [];
private _civ = [//"C_man_p_beggar_1_F",
                "C_man_1",
                "C_man_polo_1_F",
                "C_man_polo_2_F",
                "C_man_polo_3_F",
                "C_man_polo_4_F",
                "C_man_polo_5_F",
                "C_man_polo_6_F",
                "C_man_shorts_1_F",
                "C_man_shorts_2_F",
                "C_man_shorts_3_F",
                "C_man_1_1_F",
                "C_man_1_2_F",
                "C_man_1_3_F",
                "C_man_p_fugitive_F",
                "C_man_p_shorts_1_F",
                "C_man_w_worker_F",
                "C_man_hunter_1_F"];

_out set [S_VC, [_rt, _crates, _pGroups, _sGroups, _pilot, _crew, _officer, _garrison, _civ,
                _aa, _arti, _static, _cas, _tank, _apc, _car, _carArmed, _aPatrol]];


//------------------------------------------------------------ FINITO, return

_out
