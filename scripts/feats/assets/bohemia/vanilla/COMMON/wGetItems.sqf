
[	"muzzle_snds_L",
    "muzzle_snds_acp",
    "muzzle_snds_M",
    "muzzle_snds_H",
    "muzzle_snds_H_MG",
    "muzzle_snds_B",

	"NVGoggles",
	"NVGoggles_OPFOR",
	"NVGoggles_INDEP",
	
    "acc_flashlight",
    "acc_pointer_IR",

    "optic_Yorris",
    "optic_MRD",
    "optic_Aco_smg",
    "optic_ACO_grn_smg",
    "optic_Holosight_smg",
    "optic_Aco",
    "optic_ACO_grn",
    "optic_Holosight",
    "optic_Hamr",
    "optic_Arco",

    "optic_MRCO",
    "optic_DMS",
    "optic_SOS",
    "optic_LRPS",
    "optic_NVS",
    "optic_Nightstalker",
    "optic_tws",
    "optic_tws_mg"
]
