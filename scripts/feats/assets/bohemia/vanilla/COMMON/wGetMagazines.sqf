[	"16Rnd_9x21_Mag", //P07
    "16Rnd_9x21_red_Mag",
    "16Rnd_9x21_green_Mag",
    "16Rnd_9x21_yellow_Mag",
    "30Rnd_9x21_Mag",
    "30Rnd_9x21_Red_Mag",
    "30Rnd_9x21_Yellow_Mag",
    "30Rnd_9x21_Green_Mag",
    
	"30Rnd_556x45_Stanag",	//Mk20
    "30Rnd_556x45_Stanag_Tracer_Red",
    "30Rnd_556x45_Stanag_Tracer_Green",
    "30Rnd_556x45_Stanag_Tracer_Yellow",
    "30Rnd_556x45_Stanag_red",
    "30Rnd_556x45_Stanag_green",
    
    "30Rnd_556x45_Stanag",	//SDAR, TRG21, TRG20
    "30Rnd_556x45_Stanag_Tracer_Red",
    "30Rnd_556x45_Stanag_Tracer_Green",
    "30Rnd_556x45_Stanag_Tracer_Yellow",
    "30Rnd_556x45_Stanag_green",
    "30Rnd_556x45_Stanag_red",
    
    "200Rnd_65x39_cased_Box", //MK200
    "200Rnd_65x39_cased_Box_Tracer",
    
    "Titan_AP",
    "Titan_AT",
    "Titan_AA",
	"RPG32_F",
    "RPG32_HE_F"
]