/*
@filename: feats\assets\bohemia\vanilla\COMMON\assets.sqf
Author:
	Ben
Description:
	run on server,
	return the vanilla assets common to all side
*/

private _out = [];

private _isWood = (["jungle", "wood"] call assets_fnc_mapKeywords);

//------------------------------------------------------------ Arsenal Vanilla (common)

A_VCW = 0;

private _backpacks = [];
private _items = (call vc_fnc_wGetItems);
private _weapons = (call vc_fnc_wGetWeapons);
private _ammo = (call vc_fnc_wGetMagazines);

_out set [A_VCW, [_backpacks, _items, _weapons, _ammo]];

//------------------------------------------------------------ Restricted Gear Vanilla BLUFOR

RG_VCW = A_VCW + 1;

private _launcher = [];
private _mg = ["LMG_MK200_F"];
private _sRifle = [];
private _mRifle = [];
private _sScope = ["optic_LRPS"];
private _mScope = ["optic_DMS"];
private _oScope = ["optic_Nightstalker", "optic_tws", "optic_tws_mg", "optic_NVS"];
private _mbrItem = [];
private _backpack = [];

_out set [RG_VCW, [_launcher, _mg, _sRifle, _mRifle, _sScope, _mScope, _oScope, _mbrItem, _backpack]];

_out
