class vc {
	tag = "vc";
	class functions {
		class assets { file="feats\assets\bohemia\vanilla\COMMON\assets.sqf"; };
		class getBackpacks  { file="feats\assets\bohemia\vanilla\COMMON\getBackpacks.sqf"; };
		class getItems { file="feats\assets\bohemia\vanilla\COMMON\getItems.sqf"; };
		class getMagazines { file="feats\assets\bohemia\vanilla\COMMON\getMagazines.sqf"; };
		class getWeapons { file="feats\assets\bohemia\vanilla\COMMON\getWeapons.sqf"; };
		class weapAssets { file="feats\assets\bohemia\vanilla\COMMON\weapAssets.sqf"; };
		class wGetMagazines { file="feats\assets\bohemia\vanilla\COMMON\wGetMagazines.sqf"; };
		class wGetItems { file="feats\assets\bohemia\vanilla\COMMON\wGetItems.sqf"; };
		class wGetWeapons { file="feats\assets\bohemia\vanilla\COMMON\wGetWeapons.sqf"; };
	};
};
