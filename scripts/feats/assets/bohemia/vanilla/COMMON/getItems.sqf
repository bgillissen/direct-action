
[   "G_Aviator",
    "G_Balaclava_blk",
    "G_Balaclava_combat",
    "G_Balaclava_lowprofile",
    "G_Balaclava_oli",
    "G_Bandanna_aviator",
    "G_Bandanna_beast",
    "G_Bandanna_blk",
    "G_Bandanna_khk",
    "G_Bandanna_oli",
    "G_Bandanna_shades",
    "G_Bandanna_sport",
    "G_Bandanna_tan",
    "G_Combat",
    "G_Diving",
    "G_Lady_Red",
    "G_Lady_Blue",
    "G_Lady_Mirror",
    "G_Lady_Dark",
    "G_Lowprofile",
    "G_Shades_Black",
    "G_Shades_Blue",
    "G_Shades_Green",
    "G_Shades_Red",
    "G_Spectacles",
    "G_Spectacles_Tinted",
    "G_Sport_Blackred",
    "G_Sport_Red",
    "G_Sport_Blackyellow",
    "G_Sport_BlackWhite",
    "G_Sport_Checkered",
    "G_Sport_Greenblack",
    "G_Squares",
    "G_Squares_Tinted",
    "G_Tactical_Black",
    "G_Tactical_Clear",
    "V_TacVestIR_blk",

    "V_PlateCarrier1_blk",
    "V_PlateCarrier1_rgr",
    "V_PlateCarrier2_rgr",
    "V_PlateCarrier3_rgr",
    "V_PlateCarrierGL_rgr",
    "V_PlateCarrierSpec_rgr",
    "V_PlateCarrierL_CTRG",
    "V_PlateCarrierH_CTRG",
    "V_PlateCarrier_Kerry",
    "V_Chestrig_khk",
    "V_Chestrig_rgr",
    "V_Chestrig_oli",
    "V_Chestrig_blk",
    "V_Rangemaster_belt",
    "V_TacVest_camo",
    "V_TacVest_khk",
    "V_TacVest_brn",
    "V_TacVest_oli",
    "V_TacVest_blk",

    "H_Bandanna_mcamo",
    "H_BandMask_blk",
    "H_BandMask_demon",
    "H_BandMask_khk",
    "H_BandMask_reaper",
    "H_Booniehat_mcamo",
    "H_MilCap_mcamo",
    "H_Beret_grn_SF",
    "H_Beret_brn_SF",
    "H_Beret_02",
    "H_Cap_brn_SPECOPS",
    "H_Cap_tan_specops_US",
    "H_Cap_khaki_specops_UK",
    "H_Watchcap_blk",
    "H_Hat_camo",		//for Sammy
    
    "ItemWatch",
    "ItemCompass",
    "ItemGPS",
    "ItemRadio",
    "ItemMap",
    "MineDetector",

    "FirstAidKit",
    "Medikit",
    "ToolKit",

    "H_Shemag_khk",
    "H_Shemag_olive",
    "H_Shemag_olive_hs",
    "H_Shemag_tan"
]
