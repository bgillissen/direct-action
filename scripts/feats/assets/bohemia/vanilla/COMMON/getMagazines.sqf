[	"1Rnd_HE_Grenade_shell",
    "UGL_FlareWhite_F",
    "UGL_FlareGreen_F",
    "UGL_FlareRed_F",
    "UGL_FlareYellow_F",
    "UGL_FlareCIR_F",
    "1Rnd_Smoke_Grenade_shell",
    "1Rnd_SmokeRed_Grenade_shell",
    "1Rnd_SmokeGreen_Grenade_shell",
    "1Rnd_SmokeYellow_Grenade_shell",
    "1Rnd_SmokePurple_Grenade_shell",
    "1Rnd_SmokeBlue_Grenade_shell",
    "1Rnd_SmokeOrange_Grenade_shell",
    "3Rnd_HE_Grenade_shell",
    "3Rnd_UGL_FlareWhite_F",
    "3Rnd_UGL_FlareGreen_F",
    "3Rnd_UGL_FlareRed_F",
    "3Rnd_UGL_FlareYellow_F",
    "3Rnd_UGL_FlareCIR_F",
    "3Rnd_Smoke_Grenade_shell",
    "3Rnd_SmokeRed_Grenade_shell",
    "3Rnd_SmokeGreen_Grenade_shell",
    "3Rnd_SmokeYellow_Grenade_shell",
    "3Rnd_SmokePurple_Grenade_shell",
    "3Rnd_SmokeBlue_Grenade_shell",
    "3Rnd_SmokeOrange_Grenade_shell",
    
    "HandGrenade",
    "MiniGrenade",
    
    "SmokeShell",
    "SmokeShellYellow",
    "SmokeShellGreen",
    "SmokeShellRed",
    "SmokeShellPurple",
    "SmokeShellOrange",
    "SmokeShellBlue",
    
    "Chemlight_green",
    "Chemlight_red",
    "Chemlight_yellow",
    "Chemlight_blue",   
    
    "DemoCharge_Remote_Mag",
    "IEDUrbanSmall_Remote_Mag",
    "IEDLandSmall_Remote_Mag",
    "SatchelCharge_Remote_Mag",
    "ATMine_Range_Mag",
    "ClaymoreDirectionalMine_Remote_Mag",
    "APERSMine_Range_Mag",
    "APERSBoundingMine_Range_Mag",
    "SLAMDirectionalMine_Wire_Mag",
    "APERSTripMine_Wire_Mag",
    
    "Laserbatteries",
    
    "20Rnd_556x45_UW_mag"/*,
    
	"16Rnd_9x21_Mag", //P07
    "16Rnd_9x21_red_Mag",
    "16Rnd_9x21_green_Mag",
    "16Rnd_9x21_yellow_Mag",
    "30Rnd_9x21_Mag",
    "30Rnd_9x21_Red_Mag",
    "30Rnd_9x21_Yellow_Mag",
    "30Rnd_9x21_Green_Mag",
    
	"30Rnd_556x45_Stanag",	//Mk20
    "30Rnd_556x45_Stanag_Tracer_Red",
    "30Rnd_556x45_Stanag_Tracer_Green",
    "30Rnd_556x45_Stanag_Tracer_Yellow",
    "30Rnd_556x45_Stanag_red",
    "30Rnd_556x45_Stanag_green",
    
    "20Rnd_556x45_UW_mag", //SDAR, TRG21, TRG20
    "30Rnd_556x45_Stanag",
    "30Rnd_556x45_Stanag_Tracer_Red",
    "30Rnd_556x45_Stanag_Tracer_Green",
    "30Rnd_556x45_Stanag_Tracer_Yellow",
    "30Rnd_556x45_Stanag_green",
    "30Rnd_556x45_Stanag_red",
    
    "200Rnd_65x39_cased_Box", //MK200
    "200Rnd_65x39_cased_Box_Tracer",
    
    "Titan_AP",
    "Titan_AT",
    "Titan_AA",
	"RPG32_F",
    "RPG32_HE_F"
    */
]