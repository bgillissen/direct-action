/*
@filename: feats\assets\bohemia\vanilla\init.sqf
Author:
	Ben
Description:
	run on server.
	implent vanilla assets
*/

private _data = [["VC", (call vc_fnc_assets), [0, 1, 2], ["unsung"]],
				 ["VCW", (call vc_fnc_weapAssets), [0, 1, 2], ["rhsAFRF", "rhsUSAF", "rhsGREF", "unsung"]],
                 ["VO", (call vo_fnc_assets), [0], ["rhsAFRF", "unsung"]],
                 ["VB", (call vb_fnc_assets), [1], ["rhsUSAF", "unsung"]],
                 ["VBM", (call vb_fnc_modern), [1], ["unsung"]],
                 ["VI", (call vi_fnc_assets), [2], ["rhsGREF", "unsung"]]];

["vanilla", _data] call assets_fnc_implent;