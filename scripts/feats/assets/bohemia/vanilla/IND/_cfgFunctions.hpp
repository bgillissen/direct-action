class vi {
	tag = "vi";
	class functions {
		class assets { file="feats\assets\bohemia\vanilla\IND\assets.sqf"; };
		class getBackpacks { file="feats\assets\bohemia\vanilla\IND\getBackpacks.sqf"; };
		class getItems { file="feats\assets\bohemia\vanilla\IND\getItems.sqf"; };
		class getMagazines { file="feats\assets\bohemia\vanilla\IND\getMagazines.sqf"; };
		class getWeapons { file="feats\assets\bohemia\vanilla\IND\getWeapons.sqf"; };
		class backpack { file="feats\assets\bohemia\vanilla\IND\backpack.sqf"; };
		class helmet { file="feats\assets\bohemia\vanilla\IND\helmet.sqf"; };
		class primWeap { file="feats\assets\bohemia\vanilla\IND\primWeap.sqf"; };
		class uniform { file="feats\assets\bohemia\vanilla\IND\uniform.sqf"; };
		class vest { file="feats\assets\bohemia\vanilla\IND\vest.sqf"; };
	};
};
