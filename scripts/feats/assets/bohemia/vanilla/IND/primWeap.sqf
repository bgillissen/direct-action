
params['_role'];

if ( _role in ["marksman", "spotter", "hmg"] ) exitWith { "" }; //handled by marksmen DLC
if ( _role isEqualTo "lmg" ) exitWith { "LMG_Mk200_F" };
if ( _role in ["grenadier", "tl", "sl"] ) exitWith { "arifle_Mk20_GL_F" };
if ( _role in ["hPilot", "jPilot", "crew", "uavOp", "mortar"] ) exitWith { "arifle_Mk20C_F" };
if ( _role isEqualTo "sniper" ) exitWith { "srifle_GM6_F" };

"arifle_Mk20_F"