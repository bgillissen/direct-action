[  	"I_UavTerminal",

	"U_I_CombatUniform",
	"U_I_CombatUniform_tshirt",
	"U_I_CombatUniform_shortsleeve",
	"U_I_pilotCoveralls",
    "U_I_HeliPilotCoveralls",
    "U_I_GhillieSuit",
    "U_I_FullGhillie_lsh",
    "U_I_FullGhillie_sard",
    "U_I_FullGhillie_ard",
    "U_I_OfficerUniform",
    "U_I_Wetsuit",
	"U_I_G_Story_Protagonist_F",
    
    "V_PlateCarrierIA2_dgtl",
    "V_PlateCarrierIAGL_dgtl",
    "V_RebreatherIA",
    
    "H_Booniehat_dgtl",
    "H_HelmetIA",
    "H_HelmetIA_net",
    "H_HelmetIA_camo",
    "H_PilotHelmetFighter_I",
    "H_PilotHelmetHeli_I",
    "H_CrewHelmetHeli_I",
    "H_MilCap_dgtl"
]