/*
@filename: feats\assets\bohemia\vanilla\IND\assets.sqf
Author:
	Ben
Description:
	call by feats\assets\bohemia\vanilla\init.sqf
	return the Vanilla IND assets
*/

private _out = [];

//------------------------------------------------------------ Arsenal Vanilla IND

A_VI = 0;

private _backpacks = (call vi_fnc_getBackpacks);
private _items = (call vi_fnc_getItems);
private _weapons = (call vi_fnc_getWeapons);
private _ammo = (call vi_fnc_getMagazines);

_out set [A_VI, [_backpacks, _items, _weapons, _ammo]];

//------------------------------------------------------------ Restricted Gear Vanilla IND

RG_VI = A_VI + 1;

private _launcher = [];
private _mg = [];
private _sRifle = [];
private _mRifle = [];
private _sScope = [];
private _mScope = [];
private _oScope = [];
private _mbrItem = [];
private _backpack = [];

_out set [RG_VI, [_launcher, _mg, _sRifle, _mRifle, _sScope, _mScope, _oScope, _mbrItem, _backpack]];

//------------------------------------------------------------ Allowed Vehicles Vanilla IND

AV_VI = RG_VI + 1;

private _heli = [];
private _plane = [];
private _tank = [];

_out set [AV_VI, [_heli, _plane, _tank]];

//------------------------------------------------------------ supplyDrop Vanilla IND

SD_VI = AV_VI + 1;

private _backpacks = [];
private _items = [];
private _weapons = [];
private _ammo = [["30Rnd_556x45_Stanag_Tracer_Yellow",30],
				 ["200Rnd_65x39_cased_Box_Tracer",5], 
				 ["1Rnd_HE_Grenade_shell",20],
                 ["5Rnd_127x108_Mag", 15],
                 ["RPG32_F", 4],
                 ["Titan_AA", 3],
                 ["Titan_AT", 3]];
private _crates = ["I_supplyCrate_F"];
private _vehicles = ["I_Heli_light_03_unarmed_F", "I_Heli_light_03_F"];

_out set [SD_VI, [_backpacks, _items, _weapons, _ammo, _crates, _vehicles]];

//------------------------------------------------------------ Rewards Vanilla IND

R_VI = SD_VI + 1;

private _rewards = ["I_Heli_light_03_F"];

_out set [R_VI, _rewards];

//------------------------------------------------------------ Spawn Vanilla IND

S_VI = R_VI + 1;

private _rt = [];
private _crates = [];
private _pGroups = [["Indep", "IND_F", "Infantry"]];
private _sGroups = [["Indep", "IND_F", "Infantry", "HAF_SniperTeam"]];
private _pilot = ["I_Pilot_F"];
private _crew = ["I_Crew_F"];
private _officer = ["I_Officer_F"];
private _garrison = ["I_soldier_AR_F", "I_Soldier_GL_F", "I_Soldier_lite_F"];
private _aa = [];
private _arti = [];
private _static = ["I_static_AA_F", "I_static_AT_F", "I_HMG_01_F", "I_HMG_01_high_F", "I_HMG_01_A_F", "I_GMG_01_F", "I_GMG_01_high_F", "I_GMG_01_A_F", "I_Mortar_01_F", "I_G_Mortar_01_F"];
private _cas = ["I_Plane_Fighter_03_CAS_F", "I_Plane_Fighter_03_AA_F"];
private _tank = ["I_MBT_03_cannon_F"];
private _apc = ["I_APC_tracked_03_cannon_F", "I_APC_Wheeled_03_cannon_F"];
private _car = ["I_G_Offroad_01_F", "I_MRAP_03_F"];
private _carArmed = [/*"I_G_Offroad_01_armed_F",*/ "I_MRAP_03_hmg_F", "I_MRAP_03_gmg_F"];
private _aPatrol = ["I_Heli_light_03_F"];
private _civ = [];

_out set [S_VI, [_rt, _crates, _pGroups, _sGroups, _pilot, _crew, _officer, _garrison, _civ, 
                _aa, _arti, _static, _cas, _tank, _apc, _car, _carArmed, _aPatrol]];

//------------------------------------------------------------ Vehicles Vanilla IND

BV_VI = S_VI + 1;

private _car = ["I_G_Offroad_01_F", "I_MRAP_03_F"];
private _carArmed = [/*"I_G_Offroad_01_armed_F",*/ "I_MRAP_03_hmg_F", "I_MRAP_03_gmg_F"];
private _apc = ["I_APC_tracked_03_cannon_F", "I_APC_Wheeled_03_cannon_F"];
private _tank = ["I_MBT_03_cannon_F"];
private _aaTank = ["O_APC_Tracked_02_AA_F"];//i know, but we need baseDefence...
private _planeCAS = ["I_Plane_Fighter_03_CAS_F"];
private _planeAA = ["I_Plane_Fighter_03_AA_F"];
private _planeTransport = [];
private _uav = ["I_UAV_02_F", "I_UAV_02_CAS_F"];
private _heliSmall = [];
private _heliSmallArmed = ["I_Heli_light_03_F"];
private _heliMedium = ["I_Heli_light_03_unarmed_F"];
private _heliMedEvac = [];
private _heliBig = [];
private _heliAttack = [];
private _boatSmall = ["I_Boat_Transport_01_F"];
private _boatAttack = ["I_Boat_Armed_01_minigun_F"];
private _boatBig = [];
private _sub = ["I_SDV_01_F"];
private _landMedic = ["I_Truck_02_medical_F"];
private _repair = ["I_G_Offroad_01_repair_F"];
private _fuel = ["I_Truck_02_fuel_F"];
private _ammo = ["I_Truck_02_ammo_F"];
private _truck = ["I_Truck_02_covered_F", "I_Truck_02_transport_F"];
private _quad = ["I_Quadbike_01_F", "I_G_Quadbike_01_F"];
private _artiTank = [];
private _artiCannon = [];
private _artiTube = [];

_out set [BV_VI, [_car, _carArmed, _apc, _tank, _aaTank, _planeCAS, _planeAA, _planeTransport, _uav, 
                _heliSmall, _heliSmallArmed, _heliMedium, _heliMedEvac, _heliBig, _heliAttack, 
                _boatSmall, _boatAttack, _boatBig, _sub, _landMedic, _repair, _fuel, _ammo, _truck, _quad,
                _artiTank, _artiCannon, _artiTube]];

//------------------------------------------------------------ Vehicles Cargo Vanilla IND

VC_VI = BV_VI + 1;

private _car = [[],[],[],[]];
private _carArmed = [[],[],[],[]];
private _apc = [[],[],[],[]];
private _tank = [[],[],[],[]];
private _aaTank = [[],[],[],[]];
private _planeCAS = [[],[],[],[]];
private _planeAA = [[],[],[],[]];
private _planeTransport = [[],[],[],[]];
private _uav = [[],[],[],[]];
private _heliSmall = [[],[],[],[]];
private _heliSmallArmed = [[],[],[],[]];
private _heliMedium = [[],[],[],[]];
private _heliMedEvac = [[],[],[],[]];
private _heliBig = [[],[],[],[]];
private _heliAttack = [[],[],[],[]];
private _boatSmall = [[],[],[],[]];
private _boatAttack = [[],[],[],[]];
private _boatBig = [[],[],[],[]];
private _sub = [[],[],[],[]];
private _landMedic = [[],[],[],[]];
private _repair = [[],[],[],[]];
private _fuel = [[],[],[],[]];
private _ammo = [[],[],[],[]];
private _truck = [[],[],[],[]];
private _quad = [[],[],[],[]];
private _artiTank = [[],[],[],[]];
private _artiCannon = [[],[],[],[]];
private _artiTube = [[],[],[],[]];

_out set [VC_VI, [_car, _carArmed, _apc, _tank, _aaTank, _planeCAS, _planeAA, _planeTransport, _uav, 
                 _heliSmall, _heliSmallArmed, _heliMedium, _heliMedEvac, _heliBig, _heliAttack, 
                 _boatSmall, _boatAttack, _boatBig, _sub, _landMedic, _repair, _fuel, _ammo, _truck, _quad,
                 _artiTank, _artiCannon, _artiTube]];

//------------------------------------------------------------ Role loadouts RMS
/*
 [uniform, [inUniform]], 
 [vest, inVest]], 
 [backpack, [inBackpack]], 
 [primWeapon, [muzzle, bipod, acc, scope, ammo2, ammo]] 
 [secWeapon, [muzzle, bipod, acc, scope, ammo]], 
 [handWeapon, [muzzle, bipod, acc, scope, ammo]], 
 helmet, face, comm, terminal, map, bino, nv, watch, compass
*/

RL_VI = VC_VI + 1;

private _hq = [[(["hq"] call vi_fnc_uniform), [["NVGoggles_INDEP",1]]],
               [(["hq"] call vi_fnc_vest), []],
               [nil, []],
               [nil, []],
               [nil, []],
               [nil, []],
               (["hq"] call vi_fnc_helmet), nil, nil, nil, nil, nil, nil, nil, nil];
private _sl = [[(["sl"] call vi_fnc_uniform), [["muzzle_snds_M",1]]],
               [(["sl"] call vi_fnc_vest), [["30Rnd_556x45_Stanag_Tracer_Yellow",8], ["1Rnd_HE_Grenade_shell",5]]],
               [nil, [["30Rnd_556x45_Stanag_Tracer_Yellow",4], ["1Rnd_HE_Grenade_shell",10]]],
               [(["sl"] call vi_fnc_primWeap), ["", "", "acc_pointer_IR", "optic_hamr", "1Rnd_HE_Grenade_shell", "30Rnd_556x45_Stanag_Tracer_Yellow"]],
               [nil, []],
               [nil, []],
               (["sl"] call vi_fnc_helmet), nil, nil, nil, nil, nil, "NVGoggles_INDEP", nil, nil];
private _tl = [[(["tl"] call vi_fnc_uniform), [["muzzle_snds_M",1]]],
               [(["tl"] call vi_fnc_vest), [["30Rnd_556x45_Stanag_Tracer_Yellow",8], ["1Rnd_HE_Grenade_shell",5]]],
               [nil, [["30Rnd_556x45_Stanag_Tracer_Yellow",4], ["1Rnd_HE_Grenade_shell",10]]],
               [(["tl"] call vi_fnc_primWeap), ["", "", "acc_pointer_IR", "optic_hamr", "1Rnd_HE_Grenade_shell", "30Rnd_556x45_Stanag_Tracer_Yellow"]],
               [nil, []],
               [nil, []],
               (["tl"] call vi_fnc_helmet), nil, nil, nil, nil, nil, "NVGoggles_INDEP", nil, nil];
private _medic = [[(["medic"] call vi_fnc_uniform), [["muzzle_snds_M",1]]],
               	  [(["medic"] call vi_fnc_vest), [["30Rnd_556x45_Stanag_Tracer_Yellow",10]]],
               	  [(["medic"] call vi_fnc_backpack), []],
               	  [(["medic"] call vi_fnc_primWeap), ["", "", "acc_pointer_IR", "optic_hamr", "", "30Rnd_556x45_Stanag_Tracer_Yellow"]],
                  [nil, []],
                  [nil, []],
                  (["medic"] call vi_fnc_helmet), nil, nil, nil, nil, nil, "NVGoggles_INDEP", nil, nil];
private _lmg = [[(["lmg"] call vi_fnc_uniform), [["muzzle_snds_h",1]]],
               [(["lmg"] call vi_fnc_vest), [["200Rnd_65x39_cased_Box_Tracer",1]]],
               [(["lmg"] call vi_fnc_backpack), [["200Rnd_65x39_cased_Box_Tracer",3]]],
               [(["lmg"] call vi_fnc_primWeap), ["", "bipod_03_F_blk", "acc_pointer_IR", "optic_hamr", "", "200Rnd_65x39_cased_Box_Tracer"]],
               [nil, []],
               [nil, []],
               (["lmg"] call vi_fnc_helmet), nil, nil, nil, nil, nil, "NVGoggles_INDEP", nil, nil];
private _hmg = [[(["hmg"] call vi_fnc_uniform), []],
               [(["hmg"] call vi_fnc_vest), []],
               [(["hmg"] call vi_fnc_backpack), []],
               [nil, [nil, nil, "acc_pointer_IR", "optic_hamr", "", nil]],
               [nil, []],
               [nil, []],
               (["hmg"] call vi_fnc_helmet), nil, nil, nil, nil, nil, "NVGoggles_INDEP", nil, nil];
private _assHMG = [[(["assHMG"] call vi_fnc_uniform), [["muzzle_snds_M",1],["NVGoggles_INDEP",1]]],
               [(["assHMG"] call vi_fnc_vest), [["30Rnd_556x45_Stanag_Tracer_Yellow",10]]],
               [(["assHMG"] call vi_fnc_backpack), []],
               [(["assHMG"] call vi_fnc_primWeap), ["", "", "acc_pointer_IR", "optic_hamr", "", "30Rnd_556x45_Stanag_Tracer_Yellow"]],
               [nil, []],
               [nil, []],
               (["assHMG"] call vi_fnc_helmet), nil, nil, nil, nil, nil, "NVGoggles_INDEP", nil, nil];
private _aa = [[(["aa"] call vi_fnc_uniform), [["muzzle_snds_M",1]]],
               [(["aa"] call vi_fnc_vest), [["30Rnd_556x45_Stanag_Tracer_Yellow",10]]],
               [(["aa"] call vi_fnc_backpack), [["Titan_AA",3]]],
               [(["aa"] call vi_fnc_primWeap), ["", "", "acc_pointer_IR", "optic_hamr", "", "30Rnd_556x45_Stanag_Tracer_Yellow"]],
               ["launch_I_Titan_F", ["", "", "", "", "Titan_AA"]],
               [nil, []],
               (["aa"] call vi_fnc_helmet), nil, nil, nil, nil, nil, "NVGoggles_INDEP", nil, nil];
private _assAA = [[(["assAA"] call vi_fnc_uniform), [["muzzle_snds_M",1]]],
                  [(["assAA"] call vi_fnc_vest), [["30Rnd_556x45_Stanag_Tracer_Yellow",10]]],
                  [(["assAA"] call vi_fnc_backpack), [["Titan_AA",3]]],
                  [(["assAA"] call vi_fnc_primWeap), ["", "", "acc_pointer_IR", "optic_hamr", "", "30Rnd_556x45_Stanag_Tracer_Yellow"]],
                  [nil, []],
                  [nil, []],
                  (["assAA"] call vi_fnc_helmet), nil, nil, nil, nil, nil, "NVGoggles_INDEP", nil, nil];
private _at = [[(["at"] call vi_fnc_uniform), [["muzzle_snds_M",1]]],
               [(["at"] call vi_fnc_vest), [["30Rnd_556x45_Stanag_Tracer_Yellow",10]]],
               [(["at"] call vi_fnc_backpack), [["Titan_AT",3]]],
               [(["at"] call vi_fnc_primWeap), ["", "", "acc_pointer_IR", "optic_hamr", "", "30Rnd_556x45_Stanag_Tracer_Yellow"]],
               ["launch_I_Titan_short_F", ["", "", "", "", "Titan_AT"]],
               [nil, []],
               (["at"] call vi_fnc_helmet), nil, nil, nil, nil, nil, "NVGoggles_INDEP", nil, nil];
private _assAT = [[(["assAT"] call vi_fnc_uniform), [["muzzle_snds_M",1]]],
                  [(["assAT"] call vi_fnc_vest), [["30Rnd_556x45_Stanag_Tracer_Yellow",10]]],
                  [(["assAT"] call vi_fnc_backpack), [["Titan_AT",3]]],
                  [(["assAT"] call vi_fnc_primWeap), ["", "", "acc_pointer_IR", "optic_hamr", "", "30Rnd_556x45_Stanag_Tracer_Yellow"]],
                  [nil, []],
                  [nil, []],
                  (["assAT"] call vi_fnc_helmet), nil, nil, nil, nil, nil, "NVGoggles_INDEP", nil, nil];
private _sniper = [[(["sniper"] call vi_fnc_uniform), [["NVGoggles_INDEP",1]]],
                   [(["sniper"] call vi_fnc_vest), [["5Rnd_127x108_Mag",5]]],
                   [(["sniper"] call vi_fnc_backpack), [["5Rnd_127x108_Mag",10]]],
                   [(["sniper"] call vi_fnc_primWeap), ["", "", "", "optic_LRPS", "", "5Rnd_127x108_Mag"]],
               	   [nil, []],
                   [nil, []],
                   (["sniper"] call vi_fnc_helmet), nil, nil, nil, nil, nil, nil, nil, nil];
private _marksman = [[(["marksman"] call vi_fnc_uniform), []],
               		 [(["marksman"] call vi_fnc_vest), []],
               		 [(["marksman"] call vi_fnc_backpack), []],
               		 [(["marksman"] call vi_fnc_primWeap), [nil, nil, "acc_pointer_IR", "optic_SOS", nil, nil]],
               		 [nil, []],
               		 [nil, []],
               		 (["marksman"] call vi_fnc_helmet), nil, nil, nil, nil, nil, "NVGoggles_INDEP", nil, nil];
private _repair = [[(["repair"] call vi_fnc_uniform), [["muzzle_snds_M",1]]],
               	   [(["repair"] call vi_fnc_vest), [["30Rnd_556x45_Stanag_Tracer_Yellow",10]]],
               	   [(["repair"] call vi_fnc_backpack), []],
               	   [(["repair"] call vi_fnc_primWeap), ["", "", "acc_pointer_IR", "optic_hamr", "", "30Rnd_556x45_Stanag_Tracer_Yellow"]],
               	   [nil, []],
               	   [nil, []],
               	   (["repair"] call vi_fnc_helmet), nil, nil, nil, nil, nil, "NVGoggles_INDEP", nil, nil];
private _demo = [[(["demo"] call vi_fnc_uniform), [["muzzle_snds_M",1]]],
               	 [(["demo"] call vi_fnc_vest), [["30Rnd_556x45_Stanag_Tracer_Yellow",10]]],
                 [(["demo"] call vi_fnc_backpack), []],
                 [(["demo"] call vi_fnc_primWeap), ["", "", "acc_pointer_IR", "optic_hamr", "", "30Rnd_556x45_Stanag_Tracer_Yellow"]],
                 [nil, []],
                 [nil, []],
                 (["demo"] call vi_fnc_helmet), nil, nil, nil, nil, nil, "NVGoggles_INDEP", nil, nil];
private _engineer = [[(["engineer"] call vi_fnc_uniform), [["muzzle_snds_M",1]]],
               		 [(["engineer"] call vi_fnc_vest), [["30Rnd_556x45_Stanag_Tracer_Yellow",10]]],
               		 [(["engineer"] call vi_fnc_backpack), []],
               		 [(["engineer"] call vi_fnc_primWeap), ["", "", "acc_pointer_IR", "optic_hamr", "", "30Rnd_556x45_Stanag_Tracer_Yellow"]],
               		 [nil, []],
               		 [nil, []],
               		 (["engineer"] call vi_fnc_helmet), nil, nil, nil, nil, nil, "NVGoggles_INDEP", nil, nil];
private _grenadier = [[(["grenadier"] call vi_fnc_uniform), [["muzzle_snds_M",1]]],
               		  [(["grenadier"] call vi_fnc_vest), [["30Rnd_556x45_Stanag_Tracer_Yellow",8], ["1Rnd_HE_Grenade_shell",5]]],
               		  [nil, []],
               		  [(["grenadier"] call vi_fnc_primWeap), ["", "", "acc_pointer_IR", "optic_hamr", "1Rnd_HE_Grenade_shell", "30Rnd_556x45_Stanag_Tracer_Yellow"]],
               		  [nil, []],
               		  [nil, []],
               		  (["grenadier"] call vi_fnc_helmet), nil, nil, nil, nil, nil, "NVGoggles_INDEP", nil, nil];
private _rifleman = [[(["rifleman"] call vi_fnc_uniform), [["muzzle_snds_M",1]]],
               		 [(["rifleman"] call vi_fnc_vest), [["30Rnd_556x45_Stanag_Tracer_Yellow",10]]],
               		 [(["rifleman"] call vi_fnc_backpack), [["RPG32_F",2]]],
               		 [(["rifleman"] call vi_fnc_primWeap), ["", "", "acc_pointer_IR", "optic_hamr", "", "30Rnd_556x45_Stanag_Tracer_Yellow"]],
               		 ["launch_RPG32_F", ["RPG32_F"]],
               		 [nil, []],
               		 (["rifleman"] call vi_fnc_helmet), nil, nil, nil, nil, nil, "NVGoggles_INDEP", nil, nil];
private _jtac = [[(["jtac"] call vi_fnc_uniform), [["muzzle_snds_M",1]]],
               	 [(["jtac"] call vi_fnc_vest), [["30Rnd_556x45_Stanag_Tracer_Yellow",10]]],
               	 [nil, []],
               	 [(["jtac"] call vi_fnc_primWeap), ["", "", "acc_pointer_IR", "optic_hamr", "", "30Rnd_556x45_Stanag_Tracer_Yellow"]],
               	 [nil, []],
               	 [nil, []],
               	 (["jtac"] call vi_fnc_helmet), nil, nil, nil, nil, nil, "NVGoggles_INDEP", nil, nil];
private _hPilot = [[(["hPilot"] call vi_fnc_uniform), [["muzzle_snds_M",1]]],
               	   [(["hPilot"] call vi_fnc_vest), [["30Rnd_556x45_Stanag_Tracer_Yellow",5]]],
               	   [nil, []],
               	   [(["hPilot"] call vi_fnc_primWeap), ["", "", "acc_pointer_IR", "optic_hamr", "", "30Rnd_556x45_Stanag_Tracer_Yellow"]],
               	   [nil, []],
               	   [nil, []],
               	   (["hPilot"] call vi_fnc_helmet), nil, nil, nil, nil, nil, "NVGoggles_INDEP", nil, nil];
private _jPilot = [[(["jPilot"] call vi_fnc_uniform), [["muzzle_snds_M",1]]],
               	   [(["jPilot"] call vi_fnc_vest), [["30Rnd_556x45_Stanag_Tracer_Yellow",5]]],
               	   [nil, []],
               	   [(["jPilot"] call vi_fnc_primWeap), ["", "", "acc_pointer_IR", "optic_hamr", "", "30Rnd_556x45_Stanag_Tracer_Yellow"]],
               	   [nil, []],
               	   [nil, []],
               	   (["jPilot"] call vi_fnc_helmet), nil, nil, nil, nil, nil, "NVGoggles_INDEP", nil, nil];
private _crew = [[(["crew"] call vi_fnc_uniform), [["muzzle_snds_M",1]]],
               	 [(["crew"] call vi_fnc_vest), [["30Rnd_556x45_Stanag_Tracer_Yellow",10]]],
               	 [nil, []],
               	 [(["crew"] call vi_fnc_primWeap), ["", "", "acc_pointer_IR", "optic_hamr", "", "30Rnd_556x45_Stanag_Tracer_Yellow"]],
               	 [nil, []],
               	 [nil, []],
               	 (["crew"] call vi_fnc_helmet), nil, nil, nil, nil, nil, "NVGoggles_INDEP", nil, nil];
private _mortar = [[(["mortar"] call vi_fnc_uniform), [["muzzle_snds_M",1]]],
               	   [(["mortar"] call vi_fnc_vest), [["30Rnd_556x45_Stanag_Tracer_Yellow",10]]],
               	   [nil, []],
                   [(["mortar"] call vi_fnc_primWeap), ["", "", "acc_pointer_IR", "optic_hamr", "", "30Rnd_556x45_Stanag_Tracer_Yellow"]],
                   [nil, []],
                   [nil, []],
                   (["mortar"] call vi_fnc_helmet), nil, nil, nil, nil, nil, "NVGoggles_INDEP", nil, nil];
private _uavOp = [[(["uavOp"] call vi_fnc_uniform), [["muzzle_snds_M",1]]],
               	  [(["uavOp"] call vi_fnc_vest), [["30Rnd_556x45_Stanag_Tracer_Yellow",10]]],
               	  [nil, []],
               	  [(["uavOp"] call vi_fnc_primWeap), ["", "", "acc_pointer_IR", "optic_hamr", "", "30Rnd_556x45_Stanag_Tracer_Yellow"]],
               	  [nil, []],
               	  [nil, []],
               	  (["uavOp"] call vi_fnc_helmet), nil, nil, "I_UavTerminal", nil, nil, "NVGoggles_INDEP", nil, nil];
private _spotter = [[(["spotter"] call vi_fnc_uniform), []],
               		[(["spotter"] call vi_fnc_vest), []],
               		[(["spotter"] call vi_fnc_backpack), []],
               		[(["spotter"] call vi_fnc_primWeap), [nil, nil, "acc_pointer_IR", "optic_SOS", nil, nil]],
               		[nil, []],
               		[nil, []],
               		(["spotter"] call vi_fnc_helmet), nil, nil, nil, nil, nil, "NVGoggles_INDEP", nil, nil];

_out set [RL_VI, [_hq, _sl, _tl, _medic, _lmg, _hmg, _assHMG, _aa, _assAA, _at, _assAT, _sniper, _marksman,
                   _repair, _demo, _engineer, _grenadier, _rifleman, _jtac, _hPilot, _jPilot, _crew, 
                   _mortar, _uavOp, _spotter]];

//------------------------------------------------------------ Base Atmosphere Role Loadout rmSplinter

BALO_VI = RL_VI + 1;

private _medic = [[(["medic"] call vi_fnc_uniform), []],
                  [(["medic"] call vi_fnc_vest), []],
                  ["", []],
                  ["", []],
                  ["", []],
                  ["", []],
                  "", "", "", "", "", "", "", "", ""];
private _gear = [[(["hq"] call vi_fnc_uniform), []],
                 ["", []],
                 ["", []],
                 ["", []],
                 ["", []],
                 ["", []],
                 "", "", "", "", "", "", "", "", ""];
private _support = [[(["hq"] call vi_fnc_uniform), []],
                    ["", []],
                    ["", []],
                    ["", []],
                    ["", []],
                    ["", []],
                    "", "", "", "", "", "", "", "", ""];
private _default = [[(["hq"] call vi_fnc_uniform), []],
                    ["", []],
                    ["", []],
                    ["", []],
                    ["", []],
                    ["", []],
                    "", "", "", "", "", "", "", "", ""];

_out set [BALO_VI, [_medic, _gear, _support, _default]];

//------------------------------------------------------------ FINITO, return
          
_out