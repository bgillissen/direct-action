
params ["_role"];

if ( _role isEqualTo "hPilot" ) exitWith { "H_PilotHelmetHeli_I" };
if ( _role isEqualTo "jPilot" ) exitWith { "H_PilotHelmetFighter_I" };
if ( _role isEqualTo "hq" ) exitWith { "H_MilCap_dgtl" };
if ( _role in ["sniper", "spotter", "marksman"] ) exitWith { "H_Booniehat_dgtl" };

"H_HelmetIA"