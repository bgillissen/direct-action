params ["_role"];

if ( _role in ["jPilot", "hPilot"] ) exitWith { "U_I_pilotCoveralls" };
if ( _role isEqualTo "hq" ) exitWith { "U_I_CombatUniform_tshirt" };

"U_I_CombatUniform"