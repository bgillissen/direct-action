class vb {
	tag = "vb";
	class functions {
		class assets { file="feats\assets\bohemia\vanilla\BLUFOR\assets.sqf"; };
		class modern { file="feats\assets\bohemia\vanilla\BLUFOR\modern.sqf"; };
		class getBackpacks  { file="feats\assets\bohemia\vanilla\BLUFOR\getBackpacks.sqf"; };
		class getItems { file="feats\assets\bohemia\vanilla\BLUFOR\getItems.sqf"; };
		class getMagazines { file="feats\assets\bohemia\vanilla\BLUFOR\getMagazines.sqf"; };
		class getWeapons { file="feats\assets\bohemia\vanilla\BLUFOR\getWeapons.sqf"; };
		class primWeap { file="feats\assets\bohemia\vanilla\BLUFOR\primWeap.sqf"; };
		class pwMag { file="feats\assets\bohemia\vanilla\BLUFOR\pwMag.sqf"; };
	};
};
