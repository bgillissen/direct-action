/*
@filename: feats\assets\bohemia\vanilla\BLUFOR\modern.sqf
Author:
	Ben
Description:
	call by feats\assets\bohemia\vanilla\init.sqf
	return the Vanilla BLUFOR assets only unavailable when unsung is loaded
*/

private _out = [];

//------------------------------------------------------------ Arsenal Vanilla BLUFOR

A_VBM = 0;

private _backpacks = ["B_Parachute"];
private _items = ["B_UavTerminal", "V_RebreatherB", "U_B_Wetsuit"];
private _weapons = [];
private _ammo = [];

_out set [A_VBM, [_backpacks, _items, _weapons, _ammo]];

_out