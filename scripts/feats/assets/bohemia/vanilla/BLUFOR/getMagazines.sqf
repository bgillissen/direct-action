
[	"B_IR_Grenade",
    
    "11Rnd_45ACP_Mag", //Pistol Heavy 01

    "30Rnd_45ACP_Mag_SMG_01",	//SMG01
    "30Rnd_45ACP_Mag_SMG_01_tracer_green",
    "30Rnd_45ACP_Mag_SMG_01_Tracer_Red",
    "30Rnd_45ACP_Mag_SMG_01_Tracer_Yellow",

    "30Rnd_65x39_caseless_mag", //MX sand
    "30Rnd_65x39_caseless_mag_Tracer",
    "100Rnd_65x39_caseless_mag",
    "100Rnd_65x39_caseless_mag_Tracer",

    "30Rnd_65x39_caseless_black_mag", //MX black
    "30Rnd_65x39_caseless_black_mag_Tracer",
    "100Rnd_65x39_caseless_black_mag",
    "100Rnd_65x39_caseless_black_mag_Tracer",

    "20Rnd_762x51_Mag", //EBR
    "7Rnd_408_Mag", //M320 LRR

    "NLAW_F"
]