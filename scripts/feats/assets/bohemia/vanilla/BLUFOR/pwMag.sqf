params ['_role'];

private _isWood = (["jungle", "wood", "dry"] call assets_fnc_mapKeywords);

if ( _role in ["hq", "jPilot"] ) exitWith { nil };
if ( _role in ["sniper"] ) exitWith { "7Rnd_408_Mag" };
if ( _role in ["marksman", "spotter"] ) exitWith { "20Rnd_762x51_Mag" };
if ( _role in ["hmg"] ) exitWith { "200Rnd_65x39_cased_Box_Tracer" };
if ( _role in ["lmg"] ) exitWith { 
	if ( _isWood ) exitWith { "100Rnd_65x39_caseless_black_mag_Tracer" };
	"100Rnd_65x39_caseless_mag_Tracer"
};

if ( _isWood ) exitWith { "30Rnd_65x39_caseless_black_mag" };
"30Rnd_65x39_caseless_mag"