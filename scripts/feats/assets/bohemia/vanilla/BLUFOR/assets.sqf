/*
@filename: feats\assets\bohemia\vanilla\BLUFOR\assets.sqf
Author:
	Ben
Description:
	call by feats\assets\bohemia\vanilla\init.sqf
	return the Vanilla BLUFOR assets
*/

private _out = [];

private _isDesert = (["desert", "dry"] call assets_fnc_mapKeywords);
private _isWood = (["jungle", "wood"] call assets_fnc_mapKeywords);


//------------------------------------------------------------ Arsenal Vanilla BLUFOR

A_VB = 0;

private _backpacks = (call vb_fnc_getBackpacks);
private _items = (call vb_fnc_getItems);
private _weapons = (call vb_fnc_getWeapons);
private _ammo = (call vb_fnc_getMagazines);

_out set [A_VB, [_backpacks, _items, _weapons, _ammo]];

//------------------------------------------------------------ Restricted Gear Vanilla BLUFOR

RG_VB = A_VB + 1;

private _launcher = ["launch_NLAW_F", "launch_B_Titan_F", "launch_B_Titan_short_F"];
private _mg = ["arifle_MX_SW_F", "arifle_MX_SW_Black_F", ""];
private _sRifle = ["srifle_LRR_F"];
private _mRifle = ["srifle_EBR_F"];
private _sScope = [];
private _mScope = [];
private _oScope = [];
private _mbrItem = [];
private _backpack = [];

_out set [RG_VB, [_launcher, _mg, _sRifle, _mRifle, _sScope, _mScope, _oScope, _mbrItem, _backpack]];

//------------------------------------------------------------ Allowed Vehicles Vanilla BLUFOR

AV_VB = RG_VB + 1;

private _heli = ["B_Heli_Light_01_F"];
private _plane = [];
private _tank = [];

_out set [AV_VB, [_heli, _plane, _tank]];

//------------------------------------------------------------ supplyDrop Vanilla BLUFOR

SD_VB = AV_VB + 1;

private _backpacks = [["B_AssaultPack_mcamo", 1]];
private _items = [];
private _weapons = [[(["rifleman"] call vb_fnc_primWeap), 1]];
private _ammo = [[(["rifleman"] call vb_fnc_pwMag), 30], 
				 [(["hmg"] call vb_fnc_pwMag), 10],
				 [(["lmg"] call vb_fnc_pwMag), 15],
				 ["Titan_AA", 5],
				 ["Titan_AT", 5],
				 ["RPG32_F", 5]];
private _crates = ["B_supplyCrate_F"];
private _vehicles = ["B_Heli_Transport_01_F", "B_Heli_Transport_01_camo_F"];

_out set [SD_VB, [_backpacks, _items, _weapons, _ammo, _crates, _vehicles]];

//------------------------------------------------------------ Rewards Vanilla BLUFOR

R_VB = SD_VB + 1;

private _rewards = ["B_Heli_Attack_01_F"];

_out set [R_VB, _rewards];

//------------------------------------------------------------ Spawn Vanilla BLUFOR

S_VB = R_VB + 1;

private _rt = [];
private _crates = [];
private _pGroups = [["West", "BLU_F", "Infantry"]];
private _sGroups = [["West", "BLU_F", "Infantry", "BUS_SniperTeam"]];
private _pilot = ["B_pilot_F"];
private _crew = ["B_crew_F"];
private _officer = ["B_officer_F"];
private _garrison = ["B_soldier_AR_F", "B_Soldier_GL_F", "B_Soldier_lite_F"];
private _aa = ["B_APC_Tracked_01_AA_F"];
private _arti = ["B_MBT_01_arty_F"];
private _static = ["B_HMG_01_F", "B_HMG_01_high_F", "B_HMG_01_A_F", "B_GMG_01_F", "B_GMG_01_high_F", "B_GMG_01_A_F", "B_Mortar_01_F", "B_G_Mortar_01_F", "B_static_AA_F", "B_static_AT_F"];
private _cas = ["B_Plane_CAS_01_F"];
private _tank = ["B_MBT_01_cannon_F", "B_MBT_01_mlrs_F", "B_MBT_01_TUSK_F"];
private _apc = ["B_APC_Tracked_01_rcws_F", "B_APC_Wheeled_01_cannon_F"];
private _car = ["B_MRAP_01_F", "B_G_Offroad_01_F"];
private _carArmed = ["B_MRAP_01_gmg_F", "B_MRAP_01_hmg_F"/*, "B_G_Offroad_01_armed_F"*/];
private _aPatrol = ["B_Heli_Attack_01_F"];
private _civ = [];

_out set [S_VB, [_rt, _crates, _pGroups, _sGroups, _pilot, _crew, _officer, _garrison, _civ,
                 _aa, _arti, _static, _cas, _tank, _apc, _car, _carArmed, _aPatrol]];

//------------------------------------------------------------ Vehicles Vanilla BLUFOR

BV_VB = S_VB + 1;

private _car = ["B_MRAP_01_F", "B_G_Offroad_01_F"];
private _carArmed = ["B_MRAP_01_gmg_F", "B_MRAP_01_hmg_F"/*, "B_G_Offroad_01_armed_F"*/];
private _apc = ["B_APC_Tracked_01_rcws_F", "B_APC_Wheeled_01_cannon_F"];
private _tank = ["B_MBT_01_cannon_F", "B_MBT_01_arty_F", "B_MBT_01_mlrs_F", "B_MBT_01_TUSK_F"];
private _aaTank = ["B_APC_Tracked_01_AA_F"];
private _planeCAS = ["B_Plane_CAS_01_F"];
private _planeAA = [];
private _planeTransport = [];
private _uav = ["B_UAV_02_F", "B_UAV_02_CAS_F"];
private _heliSmall = ["B_Heli_Light_01_F"];
private _heliSmallArmed = ["B_Heli_Light_01_armed_F"];
private _heliMedium = ["B_Heli_Transport_01_F", "B_Heli_Transport_01_camo_F"];
private _heliMedEvac = [];
private _heliBig = [];
private _heliAttack = ["B_Heli_Attack_01_F"];
private _boatSmall = ["B_Boat_Transport_01_F"];
private _boatAttack = ["B_Boat_Armed_01_minigun_F"];
private _boatBig = [];
private _sub = ["B_SDV_01_F"];
private _landMedic = ["B_Truck_01_medical_F"];
private _repair = [/*"B_APC_Tracked_01_CRV_F",*/ "B_G_Offroad_01_repair_F", "B_Truck_01_Repair_F"];
private _fuel = ["B_Truck_01_fuel_F"];
private _ammo = ["B_Truck_01_ammo_F"];
private _truck = ["B_Truck_01_transport_F", "B_Truck_01_covered_F", "B_Truck_01_mover_F"];
private _quad = ["B_Quadbike_01_F", "B_G_Quadbike_01_F"];
private _artiTank = ["B_MBT_01_arty_F"];
private _artiCannon = [];
private _artiTube = ["B_Mortar_01_F"];

_out set [BV_VB, [_car, _carArmed, _apc, _tank, _aaTank, _planeCAS, _planeAA, _planeTransport, _uav, 
                 _heliSmall, _heliSmallArmed, _heliMedium, _heliMedEvac, _heliBig, _heliAttack, 
                 _boatSmall, _boatAttack, _boatBig, _sub, _landMedic, _repair, _fuel, _ammo, _truck, _quad,
                 _artiTank, _artiCannon, _artiTube]];

//------------------------------------------------------------ Vehicles Cargo Vanilla BLUFOR

VC_VB = BV_VB + 1;

private _car = [[],
   				[],
   				[[(["rifleman"] call vb_fnc_primWeap), 1]],
   				[[(["rifleman"] call vb_fnc_pwMag), 20], 
				 [(["hmg"] call vb_fnc_pwMag), 5],
				 [(["lmg"] call vb_fnc_pwMag), 10],
				 ["Titan_AA", 2],
				 ["Titan_AT", 2],
				 ["RPG32_F", 3]]];
private _carArmed = [[],
   				[],
   				[[(["rifleman"] call vb_fnc_primWeap), 1]],
   				[[(["rifleman"] call vb_fnc_pwMag), 20], 
				 [(["hmg"] call vb_fnc_pwMag), 5],
				 [(["lmg"] call vb_fnc_pwMag), 10],
				 ["Titan_AA", 2],
				 ["Titan_AT", 2],
				 ["RPG32_F", 3]]];
private _apc = [[],
   				[],
   				[[(["rifleman"] call vb_fnc_primWeap), 1]],
   				[[(["rifleman"] call vb_fnc_pwMag), 20], 
				 [(["hmg"] call vb_fnc_pwMag), 5],
				 [(["lmg"] call vb_fnc_pwMag), 10],
				 ["Titan_AA", 2],
				 ["Titan_AT", 2],
				 ["RPG32_F", 3]]];
private _tank = [[],
   				 [],
   				 [[(["rifleman"] call vb_fnc_primWeap), 1]],
   				 [[(["rifleman"] call vb_fnc_pwMag), 20], 
				  [(["hmg"] call vb_fnc_pwMag), 5],
				  [(["lmg"] call vb_fnc_pwMag), 10],
				  ["Titan_AA", 2],
				  ["Titan_AT", 2],
				  ["RPG32_F", 3]]];
private _aaTank = [[],
   				   [],
   				   [[(["rifleman"] call vb_fnc_primWeap), 1]],
   				   [[(["rifleman"] call vb_fnc_pwMag), 20], 
				    [(["hmg"] call vb_fnc_pwMag), 5],
				    [(["lmg"] call vb_fnc_pwMag), 10],
				    ["Titan_AA", 2],
				    ["Titan_AT", 2],
				    ["RPG32_F", 3]]];
private _planeCAS = [[],[],[],[]];
private _planeAA = [[],[],[],[]];
private _planeTransport = [[],[],[],[]];
private _uav = [[],[],[],[]];
private _heliSmall = [[],
   					  [],
   					  [[(["rifleman"] call vb_fnc_primWeap), 1]],
   					  [[(["rifleman"] call vb_fnc_pwMag), 20], 
				 	   [(["hmg"] call vb_fnc_pwMag), 5],
				 	   [(["lmg"] call vb_fnc_pwMag), 10]]];
private _heliSmallArmed = [[],
   					  	   [],
   					  	   [[(["rifleman"] call vb_fnc_primWeap), 1]],
   					  	   [[(["rifleman"] call vb_fnc_pwMag), 20], 
				 	   		[(["hmg"] call vb_fnc_pwMag), 5],
				 	   		[(["lmg"] call vb_fnc_pwMag), 10]]];
private _heliMedium = [[],
   					   [],
   					   [[(["rifleman"] call vb_fnc_primWeap), 1]],
   					   [[(["rifleman"] call vb_fnc_pwMag), 20], 
				 		[(["hmg"] call vb_fnc_pwMag), 5],
				 		[(["lmg"] call vb_fnc_pwMag), 10],
				 		["Titan_AA", 2],
				 		["Titan_AT", 2],
				 		["RPG32_F", 2]]];
private _heliMedEvac = [[],[],[],[]];
private _heliBig = [[],
					[],
   					[[(["rifleman"] call vb_fnc_primWeap), 1]],
   					[[(["rifleman"] call vb_fnc_pwMag), 20], 
				 	 [(["hmg"] call vb_fnc_pwMag), 5],
				 	 [(["lmg"] call vb_fnc_pwMag), 10],
				 	 ["Titan_AA", 5],
				 	 ["Titan_AT", 5],
				 	 ["RPG32_F", 5]]];
private _heliAttack = [[],[],[],[]];
private _boatSmall = [[],
   					  [],
   					  [[(["rifleman"] call vb_fnc_primWeap), 1]],
   					  [[(["rifleman"] call vb_fnc_pwMag), 20], 
				 	   [(["hmg"] call vb_fnc_pwMag), 5],
				 	   [(["lmg"] call vb_fnc_pwMag), 10]]];
private _boatAttack = [[],
   					   [],
   					   [[(["rifleman"] call vb_fnc_primWeap), 1]],
   					   [[(["rifleman"] call vb_fnc_pwMag), 20], 
				 	    [(["hmg"] call vb_fnc_pwMag), 5],
				 	    [(["lmg"] call vb_fnc_pwMag), 10]]];
private _boatBig = [[],
   					[],
   					[[(["rifleman"] call vb_fnc_primWeap), 1]],
   					[[(["rifleman"] call vb_fnc_pwMag), 20], 
				 	 [(["hmg"] call vb_fnc_pwMag), 5],
				 	 [(["lmg"] call vb_fnc_pwMag), 10],
				 	 ["Titan_AA", 5],
				 	 ["Titan_AT", 5],
				 	 ["RPG32_F", 5]]];
private _sub = [[],[],[],[]];
private _landMedic = [[],[],[],[]];
private _repair = [[],[],[],[]];
private _fuel = [[],[],[],[]];
private _ammo = [[],[],[],[]];
private _truck = [[],[],[],[]];
private _quad = [[],[],[],[]];
private _artiTank = [[],[],[],[]];
private _artiCannon = [[],[],[],[]];
private _artiTube = [[],[],[],[]];

_out set [VC_VB, [_car, _carArmed, _apc, _tank, _aaTank, _planeCAS, _planeAA, _planeTransport, _uav, 
                  _heliSmall, _heliSmallArmed, _heliMedium, _heliMedEvac, _heliBig, _heliAttack, 
                  _boatSmall, _boatAttack, _boatBig, _sub, _landMedic, _repair, _fuel, _ammo, _truck, _quad,
                  _artiTank, _artiCannon, _artiTube]];

//------------------------------------------------------------ Role Loadout Vanilla BLUFOR
/*
 [uniform, [inUniform]], 
 [vest, inVest]], 
 [backpack, [inBackpack]], 
 [primWeapon, [muzzle, bipod, acc, scope, ammo2, ammo]] 
 [secWeapon, [muzzle, bipod, acc, scope, ammo]], 
 [handWeapon, [muzzle, bipod, acc, scope, ammo]], 
 helmet, face, comm, terminal, map, bino, nv, watch, compass
*/
RL_VB = VC_VB + 1;

private _nv = "NVGoggles";
private _vGL = "V_PlateCarrierGL_mtp";
if ( _isWood ) then { 
	_nv = "NVGoggles_INDEP"; 
	_vGL = "V_PlateCarrierGL_rgr";
};

private _hq = [["U_B_CombatUniform_mcam_tshirt", [[_nv, 1]]],
               [nil, []],
               [nil, []],
               [(["hq"] call vb_fnc_primWeap), [nil, nil, nil, nil, nil, (['hq'] call vb_fnc_pwMag)]],
               [nil, []],
               [nil, []],
               nil, nil, nil, nil, nil, nil, nil, nil, nil];
private _sl = [["U_B_CombatUniform_mcam", [[_nv, 1]]],
			   [_vGL, [["11Rnd_45ACP_Mag", 2], [(["sl"] call vb_fnc_pwMag), 7], ["HandGrenade", 2]]],
               ["B_AssaultPack_mcamo", [[(["sl"] call vb_fnc_pwMag), 5], ["3Rnd_HE_Grenade_shell", 6], ["muzzle_snds_h", 1]]],
               [(["sl"] call vb_fnc_primWeap), ["", "", "acc_pointer_IR", "optic_hamr", "3Rnd_HE_Grenade_shell", (["sl"] call vb_fnc_pwMag)]],
               [nil, []],
               ["hgun_Pistol_heavy_01_F", ["muzzle_sdns_acp", "", "acc_flashlight_pistol", "optic_MRD", "11Rnd_45ACP_Mag"]],
               "H_HelmetB", nil, nil, nil, nil, nil, "", nil, nil];
private _tl = [["U_B_CombatUniform_mcam", [[_nv, 1]]],
			   [_vGL, [["11Rnd_45ACP_Mag", 2], [(["tl"] call vb_fnc_pwMag), 7], ["HandGrenade", 2]]],
               ["B_AssaultPack_mcamo", [[(["tl"] call vb_fnc_pwMag),5], ["3Rnd_HE_Grenade_shell", 6], ["muzzle_snds_h", 1]]],
               [(["tl"] call vb_fnc_primWeap), ["", "", "acc_pointer_IR", "optic_hamr", "3Rnd_HE_Grenade_shell", (["tl"] call vb_fnc_pwMag)]],
               [nil, []],
               ["hgun_Pistol_heavy_01_F", ["muzzle_snds_acp", "", "acc_flashlight_pistol", "optic_MRD", "11Rnd_45ACP_Mag"]],
               "H_HelmetB", nil, nil, nil, nil, nil, "", nil, nil];
private _medic = [["U_B_CombatUniform_mcam", [[_nv, 1]]],
			      ["V_PlateCarrier2_rgr", [["11Rnd_45ACP_Mag", 2], [(["medic"] call vb_fnc_pwMag), 9], ["HandGrenade", 2]]],
                  ["B_Kitbag_mcamo", [["muzzle_snds_h", 1]]],
                  [(["medic"] call vb_fnc_primWeap), ["", "bipod_01_F_snd", "acc_pointer_IR", "optic_hamr", "", (["medic"] call vb_fnc_pwMag)]],
                  [nil, []],
                  ["hgun_Pistol_heavy_01_F", ["muzzle_snds_acp", "", "acc_flashlight_pistol", "optic_MRD", "11Rnd_45ACP_Mag"]],
                  "H_HelmetB", nil, nil, nil, nil, nil, "", nil, nil];
private _lmg = [["U_B_CombatUniform_mcam", [[_nv, 1]]],
			    ["V_PlateCarrier2_rgr", [["11Rnd_45ACP_Mag", 2], [(["lmg"] call vb_fnc_pwMag), 3]]],
                ["B_AssaultPack_mcamo", [["muzzle_snds_h", 1],[(["lmg"] call vb_fnc_pwMag), 4]]],
                [(["lmg"] call vb_fnc_primWeap), ["", "bipod_01_F_snd", "acc_pointer_IR", "optic_hamr", "", (["lmg"] call vb_fnc_pwMag)]],
                [nil, []],
                ["hgun_Pistol_heavy_01_F", ["muzzle_snds_acp", "", "acc_flashlight_pistol", "optic_MRD", "11Rnd_45ACP_Mag"]],
                "H_HelmetB", nil, nil, nil, nil, nil, "", nil, nil];
private _hmg = [["U_B_CombatUniform_mcam", [[_nv, 1]]],
			    ["V_PlateCarrier2_rgr", [["11Rnd_45ACP_Mag", 2], [(["hmg"] call vb_fnc_pwMag), 1]]],
                ["B_AssaultPack_mcamo", [["muzzle_snds_h", 1],[(["hmg"] call vb_fnc_pwMag), 4]]],
                [(["hmg"] call vb_fnc_primWeap), ["", "bipod_01_F_snd", "acc_pointer_IR", "optic_hamr", "", (["hmg"] call vb_fnc_pwMag)]],
                [nil, []],
                ["hgun_Pistol_heavy_01_F", ["muzzle_snds_acp", "", "acc_flashlight_pistol", "optic_MRD", "11Rnd_45ACP_Mag"]],
                "H_HelmetB", nil, nil, nil, nil, nil, "", nil, nil];
private _assHMG = [["U_B_CombatUniform_mcam", [[_nv, 1]]],
			       ["V_PlateCarrier2_rgr", [["11Rnd_45ACP_Mag", 2], [(["assHMG"] call vb_fnc_pwMag), 9], ["HandGrenade", 2]]],
                   ["B_AssaultPack_mcamo", [["muzzle_snds_h", 1], [(["hmg"] call vb_fnc_pwMag), 4]]],
                   [(["assHMG"] call vb_fnc_primWeap), ["", "bipod_01_F_snd", "acc_pointer_IR", "optic_hamr", "", (["assHMG"] call vb_fnc_pwMag)]],
                   [nil, []],
                   ["hgun_Pistol_heavy_01_F", ["muzzle_snds_acp", "", "acc_flashlight_pistol", "optic_MRD", "11Rnd_45ACP_Mag"]],
                  "H_HelmetB", nil, nil, nil, nil, nil, "", nil, nil];
private _aa = [["U_B_CombatUniform_mcam", [[_nv, 1]]],
			   ["V_PlateCarrier2_rgr", [["11Rnd_45ACP_Mag", 2], [(["aa"] call vb_fnc_pwMag), 9], ["HandGrenade", 2]]],
               ["B_Carryall_mcamo", [["muzzle_snds_h", 1], ["Titan_AA", 2]]],
               [(["aa"] call vb_fnc_primWeap), ["", "bipod_01_F_snd", "acc_pointer_IR", "optic_hamr", "", (["aa"] call vb_fnc_pwMag)]],
               ["launch_B_Titan_F", ["", "", "", "", "Titan_AA"]],
               ["hgun_Pistol_heavy_01_F", ["muzzle_snds_acp", "", "acc_flashlight_pistol", "optic_MRD", "11Rnd_45ACP_Mag"]],
               "H_HelmetB", nil, nil, nil, nil, nil, "", nil, nil];
private _assAA = [["U_B_CombatUniform_mcam", [[_nv, 1]]],
			      ["V_PlateCarrier2_rgr", [["11Rnd_45ACP_Mag", 2], [(["assAA"] call vb_fnc_pwMag), 9], ["HandGrenade", 2]]],
                  ["B_Carryall_mcamo", [["muzzle_snds_h", 1], ["Titan_AA", 2]]],
                  [(["assAA"] call vb_fnc_primWeap), ["", "bipod_01_F_snd", "acc_pointer_IR", "optic_hamr", "", (["assAA"] call vb_fnc_pwMag)]],
                  [nil, []],
                  ["hgun_Pistol_heavy_01_F", ["muzzle_snds_acp", "", "acc_flashlight_pistol", "optic_MRD", "11Rnd_45ACP_Mag"]],
                  "H_HelmetB", nil, nil, nil, nil, nil, "", nil, nil];
private _at = [["U_B_CombatUniform_mcam", [[_nv, 1]]],
			   ["V_PlateCarrier2_rgr", [["11Rnd_45ACP_Mag", 2], [(["at"] call vb_fnc_pwMag), 9], ["HandGrenade", 2]]],
               ["B_Carryall_mcamo", [["muzzle_snds_h", 1], ["Titan_AT", 2]]],
               [(["at"] call vb_fnc_primWeap), ["", "bipod_01_F_snd", "acc_pointer_IR", "optic_hamr", "", (["at"] call vb_fnc_pwMag)]],
               ["launch_B_Titan_short_F", ["", "", "", "", "Titan_AT"]],
               ["hgun_Pistol_heavy_01_F", ["muzzle_snds_acp", "", "acc_flashlight_pistol", "optic_MRD", "11Rnd_45ACP_Mag"]],
               "H_HelmetB", nil, nil, nil, nil, nil, "", nil, nil];
private _assAT = [["U_B_CombatUniform_mcam", [[_nv, 1]]],
			      ["V_PlateCarrier2_rgr", [["11Rnd_45ACP_Mag", 2], [(["assAT"] call vb_fnc_pwMag), 9], ["HandGrenade", 2]]],
                  ["B_Carryall_mcamo", [["muzzle_snds_h", 1], ["Titan_AA", 2]]],
                  [(["assAT"] call vb_fnc_primWeap), ["", "bipod_01_F_snd", "acc_pointer_IR", "optic_hamr", "", (["assAT"] call vb_fnc_pwMag)]],
                  [nil, []],
                  ["hgun_Pistol_heavy_01_F", ["muzzle_snds_acp", "", "acc_flashlight_pistol", "optic_MRD", "11Rnd_45ACP_Mag"]],
                  "H_HelmetB", nil, nil, nil, nil, nil, "", nil, nil];
private _sniper = [["U_B_CombatUniform_mcam", [[_nv, 1]]],
                   ["V_PlateCarrier2_rgr", [["11Rnd_45ACP_Mag", 2], [(["sniper"] call vb_fnc_pwMag), 4], ["HandGrenade", 2]]],
                   ["B_AssaultPack_mcamo", [[[(["sniper"] call vb_fnc_pwMag), 4], 8]]],
                   [(["sniper"] call vb_fnc_primWeap), ["", "", "", "optic_LRPS", "", (["sniper"] call vb_fnc_pwMag)]],
                   [nil, []],
                   ["hgun_Pistol_heavy_01_F", ["muzzle_snds_acp", "", "acc_flashlight_pistol", "optic_MRD", "11Rnd_45ACP_Mag"]],
                   "H_Booniehat_mcamo", nil, nil, nil, nil, nil, "", nil, nil];
private _marksman = [["U_B_CombatUniform_mcam", [[_nv, 1]]],
                   	 ["V_PlateCarrier2_rgr", [["11Rnd_45ACP_Mag", 2], [(["marksman"] call vb_fnc_pwMag), 6], ["HandGrenade", 2]]],
                   	 ["B_AssaultPack_mcamo", [["muzzle_snds_B", 1], [(["marksman"] call vb_fnc_pwMag), 6]]],
                   	 [(["marksman"] call vb_fnc_primWeap), ["", "bipod_01_F_snd", "acc_pointer_IR", "optic_AMS_snd", "", (["marksman"] call vb_fnc_pwMag)]],
                   	 [nil, []],
                   	 ["hgun_Pistol_heavy_01_F", ["muzzle_snds_acp", "", "acc_flashlight_pistol", "optic_MRD", "11Rnd_45ACP_Mag"]],
                   	 "H_Booniehat_mcamo", nil, nil, nil, nil, nil, "", nil, nil];
private _repair = [["U_B_CombatUniform_mcam", [[_nv, 1]]],
			       ["V_PlateCarrier2_rgr", [["11Rnd_45ACP_Mag", 2], [(["repair"] call vb_fnc_pwMag), 9], ["HandGrenade", 2]]],
                   ["B_Kitbag_mcamo", [["muzzle_snds_h", 1]]],
                   [(["repair"] call vb_fnc_primWeap), ["", "bipod_01_F_snd", "acc_pointer_IR", "optic_hamr", "", (["repair"] call vb_fnc_pwMag)]],
                   [nil, []],
                   ["hgun_Pistol_heavy_01_F", ["muzzle_snds_acp", "", "acc_flashlight_pistol", "optic_MRD", "11Rnd_45ACP_Mag"]],
                   "H_HelmetB", nil, nil, nil, nil, nil, "", nil, nil];
private _demo = [["U_B_CombatUniform_mcam", [[_nv, 1]]],
			     ["V_PlateCarrier2_rgr", [["11Rnd_45ACP_Mag", 2], [(["demo"] call vb_fnc_pwMag), 9], ["HandGrenade", 2]]],
                 ["B_Kitbag_mcamo", [["muzzle_snds_h", 1]]],
                 [(["demo"] call vb_fnc_primWeap), ["", "bipod_01_F_snd", "acc_pointer_IR", "optic_hamr", "", (["demo"] call vb_fnc_pwMag)]],
                 [nil, []],
                 ["hgun_Pistol_heavy_01_F", ["muzzle_snds_acp", "", "acc_flashlight_pistol", "optic_MRD", "11Rnd_45ACP_Mag"]],
                 "H_HelmetB", nil, nil, nil, nil, nil, "", nil, nil];
private _engineer = [["U_B_CombatUniform_mcam", [[_nv, 1]]],
			      	 ["V_PlateCarrier2_rgr", [["11Rnd_45ACP_Mag", 2], [(["engineer"] call vb_fnc_pwMag), 9], ["HandGrenade", 2]]],
                  	 ["B_Kitbag_mcamo", [["muzzle_snds_h", 1]]],
                  	 [(["engineer"] call vb_fnc_primWeap), ["", "bipod_01_F_snd", "acc_pointer_IR", "optic_hamr", "", (["engineer"] call vb_fnc_pwMag)]],
                  	 [nil, []],
                  	 ["hgun_Pistol_heavy_01_F", ["muzzle_snds_acp", "", "acc_flashlight_pistol", "optic_MRD", "11Rnd_45ACP_Mag"]],
                  	 "H_HelmetB", nil, nil, nil, nil, nil, "", nil, nil];
private _grenadier = [["U_B_CombatUniform_mcam", [[_nv, 1]]],
			   		  [_vGL, [["11Rnd_45ACP_Mag", 2], [(["grenadier"] call vb_fnc_pwMag), 7], ["HandGrenade", 2]]],
               		  ["B_AssaultPack_mcamo", [[(["grenadier"] call vb_fnc_pwMag), 5], ["3Rnd_HE_Grenade_shell", 6], ["muzzle_snds_h", 1]]],
               		  [(["grenadier"] call vb_fnc_primWeap), ["", "", "acc_pointer_IR", "optic_hamr", "3Rnd_HE_Grenade_shell", (["grenadier"] call vb_fnc_pwMag)]],
               		  [nil, []],
               		  ["hgun_Pistol_heavy_01_F", ["muzzle_snds_acp", "", "acc_flashlight_pistol", "optic_MRD", "11Rnd_45ACP_Mag"]],
               		  "H_HelmetB", nil, nil, nil, nil, nil, "", nil, nil];
private _rifleman = [["U_B_CombatUniform_mcam", [[_nv, 1]]],
			      	 ["V_PlateCarrier2_rgr", [["11Rnd_45ACP_Mag", 2], [(["rifleman"] call vb_fnc_pwMag), 9], ["HandGrenade", 2]]],
                  	 ["B_AssaultPack_mcamo", [["muzzle_snds_h", 1], ["RPG32_F", 2]]],
                  	 [(["rifleman"] call vb_fnc_primWeap), ["", "bipod_01_F_snd", "acc_pointer_IR", "optic_hamr", "", (["rifleman"] call vb_fnc_pwMag)]],
                  	 ["launch_RPG32_F", ["", "", "", "", "RPG32_F"]],
                  	 ["hgun_Pistol_heavy_01_F", ["muzzle_snds_acp", "", "acc_flashlight_pistol", "optic_MRD", "11Rnd_45ACP_Mag"]],
                  	 "H_HelmetB", nil, nil, nil, nil, nil, "", nil, nil];
private _jtac = [["U_B_CombatUniform_mcam", [[_nv, 1]]],
			     ["V_PlateCarrier2_rgr", [["11Rnd_45ACP_Mag", 2], [(["jtac"] call vb_fnc_pwMag), 9], ["HandGrenade", 2]]],
                 ["B_AssaultPack_mcamo", [["muzzle_snds_h", 1]]],
                 [(["jtac"] call vb_fnc_primWeap), ["", "bipod_01_F_snd", "acc_pointer_IR", "optic_hamr", "", (["jtac"] call vb_fnc_pwMag)]],
                 [nil, []],
                 ["hgun_Pistol_heavy_01_F", ["muzzle_snds_acp", "", "acc_flashlight_pistol", "optic_MRD", "11Rnd_45ACP_Mag"]],
                 "H_HelmetB", nil, nil, nil, nil, nil, "", nil, nil];
private _hPilot = [["U_B_HeliPilotCoveralls", [[_nv, 1]]],
			       ["V_TacVest_oli", [["11Rnd_45ACP_Mag", 2], [(["hPilot"] call vb_fnc_pwMag), 2]]],
                   ["", []],
                   [(["hPilot"] call vb_fnc_primWeap), ["", "bipod_01_F_snd", "acc_pointer_IR", "optic_hamr", "", (["hPilot"] call vb_fnc_pwMag)]],
                   [nil, []],
                   ["hgun_Pistol_heavy_01_F", ["muzzle_snds_acp", "", "acc_flashlight_pistol", "optic_MRD", "11Rnd_45ACP_Mag"]],
                   "H_PilotHelmetHeli_B", nil, nil, nil, nil, nil, "", nil, nil];
private _jPilot = [["U_B_PilotCoveralls", []],
                   ["V_Rangemaster_belt", [["11Rnd_45ACP_Mag", 3]]],
                   ["B_Parachute", []],
                   [(["jPilot"] call vb_fnc_primWeap), [nil, nil, nil, nil, nil, (["jPilot"] call vb_fnc_pwMag)]],
                   [nil, []],
                   ["hgun_Pistol_heavy_01_F", ["muzzle_snds_acp", "", "acc_flashlight_pistol", "optic_MRD", "11Rnd_45ACP_Mag"]],
                   "H_PilotHelmetFighter_B", nil, nil, nil, nil, nil, "", nil, nil];
private _crew = [["U_B_CombatUniform_mcam", [[_nv, 1]]],
			     ["V_PlateCarrier2_rgr", [["11Rnd_45ACP_Mag", 2], [(["crew"] call vb_fnc_pwMag), 9], ["HandGrenade", 2]]],
                 ["B_AssaultPack_mcamo", [["muzzle_snds_h", 1]]],
                 [(["crew"] call vb_fnc_primWeap), ["", "bipod_01_F_snd", "acc_pointer_IR", "optic_hamr", "", (["crew"] call vb_fnc_pwMag)]],
                 [nil, []],
                 ["hgun_Pistol_heavy_01_F", ["muzzle_snds_acp", "", "acc_flashlight_pistol", "optic_MRD", "11Rnd_45ACP_Mag"]],
                 "H_HelmetCrew_B", nil, nil, nil, nil, nil, "", nil, nil];
private _mortar = [["U_B_CombatUniform_mcam", [[_nv, 1]]],
			       ["V_PlateCarrier2_rgr", [["11Rnd_45ACP_Mag", 2], [(["mortar"] call vb_fnc_pwMag), 9], ["HandGrenade", 2]]],
                   ["B_AssaultPack_mcamo", [["muzzle_snds_h", 1]]],
                   [(["mortar"] call vb_fnc_primWeap), ["", "bipod_01_F_snd", "acc_pointer_IR", "optic_hamr", "", (["mortar"] call vb_fnc_pwMag)]],
                   [nil, []],
                   ["hgun_Pistol_heavy_01_F", ["muzzle_snds_acp", "", "acc_flashlight_pistol", "optic_MRD", "11Rnd_45ACP_Mag"]],
                   "H_HelmetB", nil, nil, nil, nil, nil, "", nil, nil];
private _uavOp = [["U_B_CombatUniform_mcam", [[_nv, 1]]],
			      ["V_PlateCarrier2_rgr", [["11Rnd_45ACP_Mag", 2], [(["uavOp"] call vb_fnc_pwMag), 9], ["HandGrenade", 2]]],
                  ["B_AssaultPack_mcamo", [["muzzle_snds_h", 1]]],
                  [(["uavOp"] call vb_fnc_primWeap), ["", "bipod_01_F_snd", "acc_pointer_IR", "optic_hamr", "", (["uavOp"] call vb_fnc_pwMag)]],
                  [nil, []],
                  ["hgun_Pistol_heavy_01_F", ["muzzle_snds_acp", "", "acc_flashlight_pistol", "optic_MRD", "11Rnd_45ACP_Mag"]],
                  "H_HelmetB", nil, nil, nil, nil, nil, "", nil, nil];
private _spotter = [["U_B_CombatUniform_mcam", [[_nv, 1]]],
                   	["V_PlateCarrier2_rgr", [["11Rnd_45ACP_Mag", 2], [(["spotter"] call vb_fnc_pwMag), 6], ["HandGrenade", 2]]],
                   	["B_AssaultPack_mcamo", [["muzzle_snds_B", 1], [(["spotter"] call vb_fnc_pwMag), 6]]],
                   	[(["spotter"] call vb_fnc_primWeap), ["", "bipod_01_F_snd", "acc_pointer_IR", "optic_AMS_snd", "", (["spotter"] call vb_fnc_pwMag)]],
                   	[nil, []],
                   	["hgun_Pistol_heavy_01_F", ["muzzle_snds_acp", "", "acc_flashlight_pistol", "optic_MRD", "11Rnd_45ACP_Mag"]],
                   	"H_Booniehat_mcamo", nil, nil, nil, nil, nil, "", nil, nil];

_out set [RL_VB, [_hq, _sl, _tl, _medic, _lmg, _hmg, _assHMG, _aa, _assAA, _at, _assAT, _sniper, _marksman,
                 _repair, _demo, _engineer, _grenadier, _rifleman, _jtac, _hPilot, _jPilot, _crew,
                 _mortar, _uavOp, _spotter]];

//------------------------------------------------------------ Base Atmosphere Role Vanilla BLUFOR

BALO_VB = RL_VB + 1;

private _medic = [["U_B_CombatUniform_mcam", []],
			      ["V_PlateCarrier2_rgr", []],
                  ["", []],
                  ["", []],
                  ["", []],
                  ["", []],
                  "", "", "", "", "", "", "", "", ""];                
private _gear = [["U_B_CombatUniform_mcam_tshirt", []],
				 ["", []],
                 ["", []],
                 ["", []],
                 ["", []],
                 ["", []],
                 "", "", "", "", "", "", "", "", ""];
private _support = [["U_B_CombatUniform_mcam_tshirt", []],
				    ["", []],
                    ["", []],
                    ["", []],
                    ["", []],
                    ["", []],
                    "", "", "", "", "", "", "", "", ""];
private _default = [["U_B_CombatUniform_mcam_tshirt", []],
				    ["", []],
                    ["", []],
                    ["", []],
                    ["", []],
                    ["", []],
                    "", "", "", "", "", "", "", "", ""];

_out set [BALO_VB, [_medic, _gear, _support, _default]];

//------------------------------------------------------------ FINITO, return
          
_out