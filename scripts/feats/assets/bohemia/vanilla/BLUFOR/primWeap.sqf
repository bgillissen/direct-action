params ['_role'];

private _isWood = (["jungle", "wood", "dry"] call assets_fnc_mapKeywords);

if ( _role in ["hq", "jPilot"] ) exitWith { nil };
if ( _role in ["sniper"] ) exitWith { "srifle_LRR_F" };
if ( _role in ["marksman", "spotter"] ) exitWith { "srifle_EBR_F" };
if ( _role in ["hmg"] ) exitWith { "LMG_mk200_F" };
if ( _role in ["lmg"] ) exitWith { 
	if ( _isWood ) exitWith { "arifle_MX_SW_Black_F" };
	"arifle_MX_SW_F" 
};
if ( _role in ["sl", "tl", "grenadier"] ) exitWith { 
	if ( _isWood ) exitWith { "arifle_MX_GL_Black_F" };
	"arifle_MX_GL_F"
};
if ( _role in ["jtac"] ) exitWith { 
	if ( _isWood ) exitWith { "arifle_MXM_Black_F" };
	"arifle_MXM_F" 
};
if ( _role in ["hPilot", "crew"] ) exitWith { 
	if ( _isWood ) exitWith { "arifle_MXC_Black_F" };
	"arifle_MXC_F"
};

if ( _isWood ) exitWith { "arifle_MX_Black_F" };

"arifle_MX_F"