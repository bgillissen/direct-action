
[	"O_IR_Grenade",
	
    "6Rnd_45ACP_Cylinder", //Pistol Heavy 02
    
	"30Rnd_9x21_Mag_SMG_02", //SMG02
    "30Rnd_9x21_Mag_SMG_02_Tracer_Red",
    "30Rnd_9x21_Mag_SMG_02_Tracer_Yellow",
    "30Rnd_9x21_Mag_SMG_02_Tracer_Green",
    "30Rnd_9x21_Mag",
    "30Rnd_9x21_Red_Mag",
    "30Rnd_9x21_Yellow_Mag",
    "30Rnd_9x21_Green_Mag",
    
	"30Rnd_65x39_caseless_green", //Katiba
    "30Rnd_65x39_caseless_green_mag_Tracer",
    
    "10Rnd_762x54_Mag", //DMR
    
    "5Rnd_127x108_Mag",	//Lynx
    "5Rnd_127x108_APDS_Mag",
    
	"150Rnd_762x54_Box", //Zafir
    "150Rnd_762x54_Box_Tracer"
]