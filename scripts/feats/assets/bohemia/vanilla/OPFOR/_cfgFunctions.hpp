class vo {
	tag = "vo";
	class functions {
		class assets { file="feats\assets\bohemia\vanilla\OPFOR\assets.sqf"; };
		class getItems { file="feats\assets\bohemia\vanilla\OPFOR\getItems.sqf"; };
		class getMagazines { file="feats\assets\bohemia\vanilla\OPFOR\getMagazines.sqf"; };
		class getWeapons { file="feats\assets\bohemia\vanilla\OPFOR\getWeapons.sqf"; };
	};
};
