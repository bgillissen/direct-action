/*
@filename: feats\assets\vanilla\OPFOR\assets.sqf
Author:
	Ben
Description:
	call by feats\assets\bohemia\vanilla\init.sqf
	return the Vanilla OPFOR assets
*/

private _out = [];

//------------------------------------------------------------ Arsenal Vanilla OPFOR

A_VO = 0;

private _backpacks = ["O_Static_Designator_02_weapon_F", "O_GMG_01_A_weapon_F", "O_HMG_01_A_weapon_F"];
private _items = (call vo_fnc_getItems);
private _weapons = (call vo_fnc_getWeapons);
private _ammo = (call vo_fnc_getMagazines);

_out set [A_VO, [_backpacks, _items, _weapons, _ammo]];

//------------------------------------------------------------ Restricted Gear Vanilla OPFOR

RG_VO = A_VO + 1;

private _launcher = ["launch_O_Titan_F",
					 "launch_O_Titan_short_F"];
private _mg = ["LMG_Mk200_F",
			   "LMG_Zafir_F"];
private _sRifle = ["srifle_GM6_F"];
private _mRifle = ["srifle_DMR_01_F"];
private _sScope = [];
private _mScope = [];
private _oScope = [];
private _mbrItem = [];
private _backpack = [];

_out set [RG_VO, [_launcher, _mg, _sRifle, _mRifle, _sScope, _mScope, _oScope, _mbrItem, _backpack]];

//------------------------------------------------------------ Allowed Vehicles Vanilla OPFOR

AV_VO = RG_VO + 1;

private _heli = [];
private _plane = [];
private _tank = [];

_out set [AV_VO, [_heli, _plane, _tank]];

//------------------------------------------------------------ supplyDrop Vanilla OPFOR

SD_VO = AV_VO + 1;

private _backpacks = [];
private _items = [];
private _weapons = [];
private _ammo = [];
private _crates = ["O_supplyCrate_F"];
private _vehicles = ["O_Heli_Light_02_F", "O_Heli_Light_02_v2_F", "O_Heli_Light_02_unarmed_F"];

_out set [SD_VO, [_backpacks, _items, _weapons, _ammo, _crates, _vehicles]];

//------------------------------------------------------------ Rewards Vanilla OPFOR

R_VO = SD_VO + 1;

private _rewards = ["O_Heli_Attack_02_F"];

_out set [R_VO, _rewards];

//------------------------------------------------------------ Spawn Vanilla OPFOR

S_VO = R_VO + 1;

private _rt = [];
private _crates = [];
private _pGroups = [["East", "OPF_F", "Infantry"]];
private _sGroups = [["East", "OPF_F", "Infantry", "OI_SniperTeam"]];
private _pilot = ["O_pilot_F"];
private _crew = ["O_crew_F"];
private _officer = ["O_officer_F"];
private _garrison = ["O_soldier_AR_F", "O_Soldier_GL_F", "O_Soldier_lite_F"];
private _aa = ["O_APC_Tracked_02_AA_F"];
private _arti = ["O_MBT_02_arty_F"];
private _static = ["O_static_AA_F", "O_static_AT_F", "O_HMG_01_F", "O_HMG_01_high_F", "O_HMG_01_A_F", "O_GMG_01_F", "O_GMG_01_high_F", "O_GMG_01_A_F", "O_Mortar_01_F", "O_G_Mortar_01_F"];
private _cas = ["O_Plane_CAS_02_F"];
private _tank = ["O_MBT_02_cannon_F"];
private _apc = ["O_APC_Tracked_02_cannon_F"];
private _car = ["O_MRAP_02_F", "O_G_Offroad_01_F"];
private _carArmed = ["O_MRAP_02_hmg_F", "O_MRAP_02_gmg_F"/*, "O_G_Offroad_01_armed_F"*/];
private _aPatrol = ["O_Heli_Light_02_F", "O_Heli_Light_02_v2_F", "O_Heli_Attack_02_F", "O_Heli_Attack_02_black_F"];
private _civ = [];

_out set [S_VO, [_rt, _crates, _pGroups, _sGroups, _pilot, _crew, _officer, _garrison, _civ,
                _aa, _arti, _static, _cas, _tank, _apc, _car, _carArmed, _aPatrol]];

//------------------------------------------------------------ Vehicles Vanilla OPFOR

BV_VO = S_VO + 1;

private _car = ["O_MRAP_02_F", "O_G_Offroad_01_F"];
private _carArmed = ["O_MRAP_02_hmg_F", "O_MRAP_02_gmg_F"/*, "O_G_Offroad_01_armed_F"*/];
private _apc = ["O_APC_Tracked_02_cannon_F"];
private _tank = ["O_MBT_02_cannon_F","O_MBT_02_arty_F"];
private _aaTank = ["O_APC_Tracked_02_AA_F"];
private _planeCAS = ["O_Plane_CAS_02_F"];
private _planeAA = [];
private _planeTransport = [];
private _uav = ["O_UAV_02_F", "O_UAV_02_CAS_F"];
private _heliSmall = [];
private _heliSmallArmed = [];
private _heliMedium = ["O_Heli_Light_02_F", "O_Heli_Light_02_v2_F", "O_Heli_Light_02_unarmed_F"];
private _heliMedEvac = [];
private _heliBig = [];
private _heliAttack = ["O_Heli_Attack_02_F"];
private _boatSmall = ["O_Boat_Transport_01_F", "O_G_Boat_Transport_01_F"];
private _boatAttack = ["O_Boat_Armed_01_hmg_F"];
private _boatBig = [];
private _sub = ["O_SDV_01_F"];
private _landMedic = ["O_Truck_02_medical_F", "O_Truck_03_medical_F"];
private _repair = ["O_G_Offroad_01_repair_F", "O_Truck_03_repair_F"];
private _fuel = ["O_Truck_02_fuel_F", "O_Truck_03_fuel_F"];
private _ammo = ["O_Truck_02_Ammo_F", "O_Truck_03_ammo_F"];
private _truck = ["O_Truck_02_covered_F", "O_Truck_02_transport_F", "O_Truck_03_transport_F", "O_Truck_03_covered_F"];
private _quad = ["O_Quadbike_01_F", "O_G_Quadbike_01_F"];
private _artiTank = [];
private _artiCannon = [];
private _artiTube = [];

_out set [BV_VO, [_car, _carArmed, _apc, _tank, _aaTank, _planeCAS, _planeAA, _planeTransport, _uav, 
                _heliSmall, _heliSmallArmed, _heliMedium, _heliMedEvac, _heliBig, _heliAttack, 
                _boatSmall, _boatAttack, _boatBig, _sub, _landMedic, _repair, _fuel, _ammo, _truck, _quad,
                _artiTank, _artiCannon, _artiTube]];

//------------------------------------------------------------ Vehicles Cargo Vanilla OPFOR

VC_VO = BV_VO + 1;

private _car = [[],[],[],[]];
private _carArmed = [[],[],[],[]];
private _apc = [[],[],[],[]];
private _tank = [[],[],[],[]];
private _aaTank = [[],[],[],[]];
private _planeCAS = [[],[],[],[]];
private _planeAA = [[],[],[],[]];
private _planeTransport = [[],[],[],[]];
private _uav = [[],[],[],[]];
private _heliSmall = [[],[],[],[]];
private _heliSmallArmed = [[],[],[],[]];
private _heliMedium = [[],[],[],[]];
private _heliMedEvac = [[],[],[],[]];
private _heliBig = [[],[],[],[]];
private _heliAttack = [[],[],[],[]];
private _boatSmall = [[],[],[],[]];
private _boatAttack = [[],[],[],[]];
private _boatBig = [[],[],[],[]];
private _sub = [[],[],[],[]];
private _landMedic = [[],[],[],[]];
private _repair = [[],[],[],[]];
private _fuel = [[],[],[],[]];
private _ammo = [[],[],[],[]];
private _truck = [[],[],[],[]];
private _quad = [[],[],[],[]];
private _artiTank = [[],[],[],[]];
private _artiCannon = [[],[],[],[]];
private _artiTube = [[],[],[],[]];

_out set [VC_VO, [_car, _carArmed, _apc, _tank, _aaTank, _planeCAS, _planeAA, _planeTransport, _uav, 
                 _heliSmall, _heliSmallArmed, _heliMedium, _heliMedEvac, _heliBig, _heliAttack, 
                 _boatSmall, _boatAttack, _boatBig, _sub, _landMedic, _repair, _fuel, _ammo, _truck, _quad,
                 _artiTank, _artiCannon, _artiTube]];

//------------------------------------------------------------ Role Loadout Vanilla OPFOR
/*
 [uniform, [inUniform]], 
 [vest, inVest]], 
 [backpack, [inBackpack]], 
 [primWeapon, [muzzle, bipod, acc, scope, ammo2, ammo]] 
 [secWeapon, [muzzle, bipod, acc, scope, ammo]], 
 [handWeapon, [muzzle, bipod, acc, scope, ammo]], 
 helmet, face, comm, terminal, map, bino, nv, watch, compass
*/

RL_VO = VC_VO + 1;

private _hq = [];
private _sl = [];
private _tl = [];
private _medic = [];
private _lmg = [];
private _hmg = [];
private _assHMG = [];
private _aa = [];
private _assAA = [];
private _at = [];
private _assAT = [];
private _sniper = [];
private _marksman = [];
private _repair = [];
private _demo = [];
private _engineer = [];
private _grenadier = [];
private _rifleman = [];
private _jtac = [];
private _hpilot = [];
private _jPilot = [];
private _crew = [];
private _mortar = [];
private _uavOp = [[nil, []],
                  [nil, []],
                  [nil, []],
                  [nil, []],
                  [nil, []],
                  [nil, []],
                  nil, nil, nil, "O_UavTerminal", nil, nil, "", nil, nil];
private _spotter = [];

_out set [RL_VO, [_hq, _sl, _tl, _medic, _lmg, _hmg, _assHMG, _aa, _assAA, _at, _assAT, _sniper, _marksman,
                 _repair, _demo, _engineer, _grenadier, _rifleman, _jtac, _hPilot, _jPilot, _crew,
                 _mortar, _uavOp, _spotter]];

//------------------------------------------------------------ Base Atmosphere Role Vanilla BLUFOR

BALO_VO = RL_VO + 1;

private _medic = [];
private _gear = [];
private _support = [];
private _default = [];

_out set [BALO_VO, [_medic, _gear, _support, _default]];

//------------------------------------------------------------ FINITO, return
          
_out