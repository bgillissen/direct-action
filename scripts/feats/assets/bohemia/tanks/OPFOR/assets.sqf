/*
@filename: feats\assets\bohemia\tanks\OPFOR\assets.sqf
Author:
	Ben
Description:
	run on server,
	return the tanks DLC assets for OPFOR
*/

private _isDesert = (["desert", "dry"] call assets_fnc_mapKeywords);
private _isWood = (["jungle", "wood"] call assets_fnc_mapKeywords);

private _out = [];

//------------------------------------------------------------ Arsenal tanks DLC  OPFOR

A_TO = 0;

private _backpacks = [];
private _items = [];
private _weapons = [];
if ( _isWood ) then {
	_weapons = ["launch_O_Vorona_green_F"];
} else {
	_weapons = ["launch_O_Vorona_brown_F"];
};

private _ammo = ["Vorona_HEAT", "Vorona_HE"];

_out set [A_TO, [_backpacks, _items, _weapons, _ammo]];

//------------------------------------------------------------ Restricted Gear Vanilla OPFOR

RG_TO = A_TO + 1;

private _launcher = ["launch_O_Vorona_green_F", "launch_O_Vorona_brown_F"];
private _mg = [];
private _sRifle = [];
private _mRifle = [];
private _sScope = [];
private _mScope = [];
private _oScope = [];
private _mbrItem = [];
private _backpack = [];

_out set [RG_TO, [_launcher, _mg, _sRifle, _mRifle, _sScope, _mScope, _oScope, _mbrItem, _backpack]];

//------------------------------------------------------------ supplyDrop tanks DLC OPFOR

SD_TO = RG_TO + 1;

private _backpacks = [];
private _items = [];
private _weapons = [];
private _ammo = [];
private _crates = [];
private _vehicles = [];

_out set [SD_TO, [ _backpacks, _items, _weapons, _ammo, _crates, _vehicles]];

//------------------------------------------------------------ Rewards tanks DLC OPFOR

R_TO = SD_TO + 1;

private _rewards = [];

_out set [R_TO, _rewards];

//------------------------------------------------------------ Spawn tanks DLC OPFOR

S_TO = R_TO + 1;

private _rt = [];
private _crates = [];
private _pGroups = [];
private _sGroups = [];
private _pilot = [];
private _crew = [];
private _officer = [];
private _garrison = [];
private _aa = [];
private _arti = [];
private _static = [];
private _cas = [];
private _tank = [];
if ( _isWood ) then {
	_tank = ["O_T_MBT_04_cannon_F", "O_T_MBT_04_command_F"]; 
} else {
	_tank = ["O_MBT_04_cannon_F", "O_MBT_04_command_F"];
};
private _apc = [];
private _car = [];
private _carArmed = [];
private _aPatrol = [];
private _civ = [];

_out set [S_TO, [_rt, _crates, _pGroups, _sGroups, _pilot, _crew, _officer, _garrison, _civ,
                _aa, _arti, _static, _cas, _tank, _apc, _car, _carArmed, _aPatrol]];

//------------------------------------------------------------ Vehicles Cargo tanks DLC OPFOR

VC_TO = S_TO + 1;

private _car = [[],[],[],[]];
private _carArmed = [[],[],[],[]];
private _apc = [[],[],[],[]];
private _tank = [[],[],[],[]];
private _aaTank = [[],[],[],[]];
private _planeCAS = [[],[],[],[]];
private _planeAA = [[],[],[],[]];
private _planeTransport = [[],[],[],[]];
private _uav = [[],[],[],[]];
private _heliSmall = [[],[],[],[]];
private _heliSmallArmed = [[],[],[],[]];
private _heliMedium = [[],[],[],[]];
private _heliMedEvac = [[],[],[],[]];
private _heliBig = [[],[],[],[]];
private _heliAttack = [[],[],[],[]];
private _boatSmall = [[],[],[],[]];
private _boatAttack = [[],[],[],[]];
private _boatBig = [[],[],[],[]];
private _sub = [[],[],[],[]];
private _landMedic = [[],[],[],[]];
private _repair = [[],[],[],[]];
private _fuel = [[],[],[],[]];
private _ammo = [[],[],[],[]];
private _truck = [[],[],[],[]];
private _quad = [[],[],[],[]];
private _artiTank = [[],[],[],[]];
private _artiCannon = [[],[],[],[]];
private _artiTube = [[],[],[],[]];

_out set [VC_TO, [_car, _carArmed, _apc, _tank, _aaTank, _planeCAS, _planeAA, _planeTransport, _uav, 
                 _heliSmall, _heliSmallArmed, _heliMedium, _heliMedEvac, _heliBig, _heliAttack, 
                 _boatSmall, _boatAttack, _boatBig, _sub, _landMedic, _repair, _fuel, _ammo, _truck, _quad,
                 _artiTank, _artiCannon, _artiTube]];

//------------------------------------------------------------ Vehicles Vanilla OPFOR

BV_TO = VC_TO + 1;

private _car = [];
private _carArmed = [];
private _apc = [];
private _tank = [];
if ( _isWood ) then {
	_tanks = ["O_T_MBT_04_cannon_F", "O_T_MBT_04_command_F"]; 
} else {
	_tanks = ["O_MBT_04_cannon_F", "O_MBT_04_command_F"];
};
private _aaTank = [];
private _planeCAS = [];
private _planeAA = [];
private _planeTransport = [];
private _uav = [];
private _heliSmall = [];
private _heliSmallArmed = [];
private _heliMedium = [];
private _heliMedEvac = [];
private _heliBig = [];
private _heliAttack = [];
private _boatSmall = [];
private _boatAttack = [];
private _boatBig = [];
private _sub = [];
private _landMedic = [];
private _repair = [];
private _fuel = [];
private _ammo = [];
private _truck = [];
private _quad = [];
private _artiTank = [];
private _artiCannon = [];
private _artiTube = [];

_out set [BV_TO, [_car, _carArmed, _apc, _tank, _aaTank, _planeCAS, _planeAA, _planeTransport, _uav, 
                 _heliSmall, _heliSmallArmed, _heliMedium, _heliMedEvac, _heliBig, _heliAttack, 
                 _boatSmall, _boatAttack, _boatBig, _sub, _landMedic, _repair, _fuel, _ammo, _truck, _quad,
                 _artiTank, _artiCannon, _artiTube]];

//------------------------------------------------------------ FINITO, return

_out