/*
@filename: feats\assets\bohemia\tanks\init.sqf
Author:
	Ben
Description:
	run on server.
	implent tanks DLC assets
*/

private _data = [["TC", (call tc_fnc_assets), [0, 1, 2], ["rhsAFRF", "rhsUSAF", "rhsGREF", "unsung"]],
				 ["TO", (call to_fnc_assets), [0], ["rhsAFRF", "unsung"]],
				 ["TB", (call tb_fnc_assets), [1], ["rhsUSAF", "unsung"]],
                 ["TI", (call ti_fnc_assets), [2], ["rhsGREF", "unsung"]]];

["tanks", _data] call assets_fnc_implent;