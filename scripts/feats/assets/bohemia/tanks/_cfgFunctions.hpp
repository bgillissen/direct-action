class tanks {
	tag = "tanks";
	class functions {
		class init { file="feats\assets\bohemia\tanks\init.sqf"; };
		class postInit { file="core\dummy.sqf"; };
		class preInit { file="core\dummy.sqf"; };
	};
};

#include "COMMON\_cfgFunctions.hpp"
#include "BLUFOR\_cfgFunctions.hpp"
#include "IND\_cfgFunctions.hpp"
#include "OPFOR\_cfgFunctions.hpp"
