class spawn_tanks {
	title = "Add tanks DLC assets to enemy spawn pool";
	values[] = {0,1};
	texts[] = {"no", "yes"};
	default = 1;
};

class tanks_modEnemy {
	title = "Tanks DLC units are spawned when mods (enemy side) are present";
	values[] = {0,1};
	texts[] = {"no", "yes"};
	default = 0;
};

class gear_tanks {
	title = "Which tanks DLC gear is available in arsenal";
	values[] = {0,1,2,3};
	texts[] = {"none", "player side only", "player side + allies", "all"};
	default = 1;
};

class tanks_modgear {
	title = "Tanks DLC gear is available when mods (player side) are present";
	values[] = {0,1};
	texts[] = {"no", "yes"};
	default = 0;
};

class tanks_modbv {
	title = "Tanks DLC vehicles are available when mods (player side) are present";
	values[] = {0,1};
	texts[] = {"no", "yes"};
	default = 0;
};

class tanks_modreward {
	title = "Tanks DLC rewards are available when mods (player side) are present";
	values[] = {0,1};
	texts[] = {"no", "yes"};
	default = 0;
};

class reward_tanks {
	title = "Add tanks DLC vehicles to reward pool";
	values[] = {0,1,2,3};
	texts[] = {"no", "player side only", "player side + allies", "all"};
	default = 2;
};

class bv_tanks {
	title = "Add tanks DLC vehicle to base vehicle pool";
	values[] = {0,1,2,3};
	texts[] = {"no", "player side only", "player side + allies", "all"};
	default = 2;
};
