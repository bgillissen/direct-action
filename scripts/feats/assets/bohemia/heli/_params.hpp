class spawn_heli {
	title = "Add Helicopter DLC assets to enemy spawn pool";
	values[] = {0,1};
	texts[] = {"no", "yes"};
	default = 1;
};

class heli_modEnemy {
	title = "Helicopter DLC assets are spawned when RHS (enemy side) is present";
	values[] = {0,1};
	texts[] = {"no", "yes"};
	default = 0;
};

class heli_modbv {
	title = "Helicopter DLC vehicles are available when RHS (player side) is present";
	values[] = {0,1};
	texts[] = {"no", "yes"};
	default = 0;
};

class reward_heli {
	title = "Add helicopter DLC vehicles to reward pool";
	values[] = {0,1,2,3};
	texts[] = {"no", "player side only", "player side + allies", "all"};
	default = 2;
};

class bv_heli {
	title = "Add helicopter DLC vehicles to base vehicle pool";
	values[] = {0,1,2,3};
	texts[] = {"no", "player side only", "player side + allies", "all"};
	default = 2;
};
