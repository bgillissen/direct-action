/*
@filename: feats\assets\bohemia\heli\init.sqf
Author:
	Ben
Description:
	run on server.
	implent helicopter DLC assets
*/

private _data = [["HO", (call ho_fnc_assets), [0], ["rhsAFRF", "unsung"]],
				 ["HB", (call hb_fnc_assets), [1], ["rhsUSAF", "unsung"]],
				 ["HI", (call hi_fnc_assets), [2], ["rhsAFRF", "rhsUSAF", "unsung"]]];

["heli", _data] call assets_fnc_implent;