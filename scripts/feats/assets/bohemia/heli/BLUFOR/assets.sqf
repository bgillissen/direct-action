/*
@filename: feats\assets\bohemia\heli\BLUFOR\assets.sqf
Author:
	Ben
Description:
	run on server,
	return the helicopter DLC BLUFOR assets
*/

private _out = [];

//------------------------------------------------------------ Rewards Helicopter BLUFOR

R_HB = 0;

private _rewards = [];

_out set [R_HB, _rewards];

//------------------------------------------------------------ supplyDrop Helicopter DLC BLUFOR

SD_HB = R_HB + 1;

private _backpacks = [];
private _items = [];
private _weapons = [];
private _ammo = [];
private _crates = [];
private _vehicles = ["B_Heli_Transport_03_F", "B_Heli_Transport_03_unarmed_F"];

_out set [SD_HB, [_backpacks, _items, _weapons, _ammo, _crates, _vehicles]];

//------------------------------------------------------------ Vehicles Vanilla BLUFOR

BV_HB = SD_HB + 1;

private _car = [];
private _carArmed = [];
private _apc = [];
private _tank = [];
private _aaTank = [];
private _planeCAS = [];
private _planeAA = [];
private _planeTransport = [];
private _uav = [];
private _heliSmall = [];
private _heliSmallArmed = [];
private _heliMedium = [];
private _heliMedEvac = [];
private _heliBig = ["B_Heli_Transport_03_F", "B_Heli_Transport_03_unarmed_F"];
private _heliAttack = [];
private _boatSmall = [];
private _boatAttack = [];
private _boatBig = [];
private _sub = [];
private _landMedic = [];
private _repair = [];
private _fuel = [];
private _ammo = [];
private _truck = [];
private _quad = [];
private _artiTank = [];
private _artiCannon = [];
private _artiTube = [];

_out set [BV_HB, [_car, _carArmed, _apc, _tank, _aaTank, _planeCAS, _planeAA, _planeTransport, _uav, 
                 _heliSmall, _heliSmallArmed, _heliMedium, _heliMedEvac, _heliBig, _heliAttack, 
                 _boatSmall, _boatAttack, _boatBig, _sub, _landMedic, _repair, _fuel, _ammo, _truck, _quad,
                 _artiTank, _artiCannon, _artiTube]];

_out