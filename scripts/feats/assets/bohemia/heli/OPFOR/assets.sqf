/*
@filename: feats\assets\bohemia\heli\OPFOR\assets.sqf
Author:
	Ben
Description:
	run on server,
	return the helicopter DLC OPFOR assets
*/

private _out = [];

//------------------------------------------------------------ Rewards Helicopter DLC OPFOR

R_HO = 0;

private _rewards = ["O_Heli_Transport_04_repair_F", "O_Heli_Transport_04_fuel_F", "O_Heli_Transport_04_ammo_F"];

_out set [R_HO, _rewards];

//------------------------------------------------------------ supplyDrop Helicopter DLC OPFOR

SD_HO = R_HO + 1;

private _backpacks = [];
private _items = [];
private _weapons = [];
private _ammo = [];
private _crates = [];
private _vehicles = ["O_Heli_Transport_04_covered_F", "O_Heli_Transport_04_bench_F", "O_Heli_Transport_04_F"];

_out set [SD_HO, [_backpacks, _items, _weapons, _ammo, _crates, _vehicles]];

//------------------------------------------------------------ Vehicles Helicopter DLC OPFOR

BV_HO = SD_HO + 1;

private _car = [];
private _carArmed = [];
private _apc = [];
private _tank = [];
private _aaTank = [];
private _planeCAS = [];
private _planeAA = [];
private _planeTransport = [];
private _uav = [];
private _heliSmall = [];
private _heliSmallArmed = [];
private _heliMedium = [];
private _heliMedEvac = ["O_Heli_Transport_04_medevac_F"];
private _heliBig = ["O_Heli_Transport_04_covered_F", "O_Heli_Transport_04_bench_F", "O_Heli_Transport_04_F"];
private _heliAttack = [];
private _boatSmall = [];
private _boatAttack = [];
private _boatBig = [];
private _sub = [];
private _landMedic = [];
private _repair = [];
private _fuel = [];
private _ammo = [];
private _truck = [];
private _quad = [];
private _artiTank = [];
private _artiCannon = [];
private _artiTube = [];

_out set [BV_HO, [_car, _carArmed, _apc, _tank, _aaTank, _planeCAS, _planeAA, _planeTransport, _uav, 
                 _heliSmall, _heliSmallArmed, _heliMedium, _heliMedEvac, _heliBig, _heliAttack, 
                 _boatSmall, _boatAttack, _boatBig, _sub, _landMedic, _repair, _fuel, _ammo, _truck, _quad,
                 _artiTank, _artiCannon, _artiTube]];
_out