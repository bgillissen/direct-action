class kart {
	tag = "kart";
	class functions {
		class assets { file="feats\assets\bohemia\kart\assets.sqf"; };
		class init { file="feats\assets\bohemia\kart\init.sqf"; };
		class postInit { file="core\dummy.sqf"; };
		class preInit { file="core\dummy.sqf"; };

	};
};
