/*
@filename: feats\assets\bohemia\kart\init.sqf
Author:
	Ben
Description:
	run on server.
	implent kart DLC assets
*/

private _data = [["KC", (call kart_fnc_assets), [0, 1, 2]]];

["kart", _data] call assets_fnc_implent;