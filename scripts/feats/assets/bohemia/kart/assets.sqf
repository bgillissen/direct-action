/*
@filename: feats\assets\bohemia\kart\assets.sqf
Author:
	Ben
Description:
	run on server,
	return the kart DLC assets
*/

private _out = [];

//------------------------------------------------------------ Arsenale marksmen DLC  (common)

A_KC = 0;

private _backpacks = [];
private _items = [];
private _weapons = ["hgun_Pistol_Signal_F"];
private _ammo = ["6Rnd_GreenSignal_F", "6Rnd_RedSignal_F"];

_out set [A_KC, [_backpacks, _items, _weapons, _ammo]];

//------------------------------------------------------------ FINITO, return

_out