
class gear_orange {
	title = "Which orange DLC gear is available in arsenal";
	values[] = {0,1,2,3};
	texts[] = {"none", "player side only", "player side + allies", "all"};
	default = 1;
};

class orange_modgear {
	title = "Orange DLC gear is available when mods (player side) are present";
	values[] = {0,1};
	texts[] = {"no", "yes"};
	default = 0;
};

class orange_modbv {
	title = "Orange DLC vehicles are available when mods (player side) are present";
	values[] = {0,1};
	texts[] = {"no", "yes"};
	default = 0;
};

class orange_modreward {
	title = "Orange DLC rewards are available when mods (player side) are present";
	values[] = {0,1};
	texts[] = {"no", "yes"};
	default = 0;
};

class reward_orange {
	title = "Add orange DLC vehicles to reward pool";
	values[] = {0,1,2,3};
	texts[] = {"no", "player side only", "player side + allies", "all"};
	default = 2;
};

class bv_orange {
	title = "Add orange DLC vehicle to base vehicle pool";
	values[] = {0,1,2,3};
	texts[] = {"no", "player side only", "player side + allies", "all"};
	default = 2;
};
