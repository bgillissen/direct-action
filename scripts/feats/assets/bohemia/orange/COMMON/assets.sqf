/*
@filename: feats\assets\bohemia\orange\COMMON\assets.sqf
Author:
	Ben
Description:
	run on server,
	return the orange DLC assets common to all side
*/

private _out = [];

//------------------------------------------------------------ Arsenale orange DLC  (common)

A_OC = 0;

private _backpacks = ["C_UAV_06_backpack_F", "C_IDAP_UAV_06_antimine_backpack_F", "C_UAV_06_medical_backpack_F" ];
private _items = ["H_HeadBandage_bloody_F", "H_HeadBandage_stained_F", "H_HeadBandage_clean_F"];
private _weapons = [];
private _ammo = ["APERSMineDispenser_Mag"];

_out set [A_OC, [_backpacks, _items, _weapons, _ammo]];

//------------------------------------------------------------ Restricted Gear Vanilla BLUFOR

RG_OC = A_OC + 1;

private _launcher = [];
private _mg = [];
private _sRifle = [];
private _mRifle = [];
private _sScope = [];
private _mScope = [];
private _oScope = [];
private _mbrItem = [];
private _backpack = [];

_out set [RG_OC, [_launcher, _mg, _sRifle, _mRifle, _sScope, _mScope, _oScope, _mbrItem, _backpack]];

//------------------------------------------------------------ supplyDrop orange DLC (common)

SD_OC = RG_OC + 1;

private _backpacks = [];
private _items = [];
private _weapons = [];
private _ammo = [];
private _crates = [];
private _vehicles = [];

_out set [SD_OC, [ _backpacks, _items, _weapons, _ammo, _crates, _vehicles]];

//------------------------------------------------------------ Vehicles Cargo orange DLC (common)

VC_OC = SD_OC + 1;

private _car = [[],[],[],[]];
private _carArmed = [[],[],[],[]];
private _apc = [[],[],[],[]];
private _tank = [[],[],[],[]];
private _aaTank = [[],[],[],[]];
private _planeCAS = [[],[],[],[]];
private _planeAA = [[],[],[],[]];
private _planeTransport = [[],[],[],[]];
private _uav = [[],[],[],[]];
private _heliSmall = [[],[],[],[]];
private _heliSmallArmed = [[],[],[],[]];
private _heliMedium = [[],[],[],[]];
private _heliMedEvac = [[],[],[],[]];
private _heliBig = [[],[],[],[]];
private _heliAttack = [[],[],[],[]];
private _boatSmall = [[],[],[],[]];
private _boatAttack = [[],[],[],[]];
private _boatBig = [[],[],[],[]];
private _sub = [[],[],[],[]];
private _landMedic = [[],[],[],[]];
private _repair = [[],[],[],[]];
private _fuel = [[],[],[],[]];
private _ammo = [[],[],[],[]];
private _truck = [[],[],[],[]];
private _quad = [[],[],[],[]];
private _artiTank = [[],[],[],[]];
private _artiCannon = [[],[],[],[]];
private _artiTube = [[],[],[],[]];

_out set [VC_OC, [_car, _carArmed, _apc, _tank, _aaTank, _planeCAS, _planeAA, _planeTransport, _uav, 
                 _heliSmall, _heliSmallArmed, _heliMedium, _heliMedEvac, _heliBig, _heliAttack, 
                 _boatSmall, _boatAttack, _boatBig, _sub, _landMedic, _repair, _fuel, _ammo, _truck, _quad,
                 _artiTank, _artiCannon, _artiTube]];

//------------------------------------------------------------ Vehicles Vanilla (common)

BV_OC = VC_OC + 1;

//"C_IDAP_Van_02_vehicle_F", "C_IDAP_Van_02_transport_F", "C_Van_02_vehicle_F", "C_Van_02_transport_F"
private _car = [];
private _carArmed = [];
private _apc = [];
private _tank = [];
private _aaTank = [];
private _planeCAS = [];
private _planeAA = [];
private _planeTransport = [];
private _uav = [];
private _heliSmall = [];
private _heliSmallArmed = [];
private _heliMedium = [];
private _heliMedEvac = [];
private _heliBig = [];
private _heliAttack = [];
private _boatSmall = [];
private _boatAttack = [];
private _boatBig = [];
private _sub = [];
private _landMedic = ["C_IDAP_Van_02_medevac_F", "C_Van_02_medevac_F"];
private _repair = ["C_Van_02_service_F"];
private _fuel = [];
private _ammo = [];
private _truck = [];
private _quad = [];
private _artiTank = [];
private _artiCannon = [];
private _artiTube = [];

_out set [BV_OC, [_car, _carArmed, _apc, _tank, _aaTank, _planeCAS, _planeAA, _planeTransport, _uav, 
                 _heliSmall, _heliSmallArmed, _heliMedium, _heliMedEvac, _heliBig, _heliAttack, 
                 _boatSmall, _boatAttack, _boatBig, _sub, _landMedic, _repair, _fuel, _ammo, _truck, _quad,
                 _artiTank, _artiCannon, _artiTube]];

//------------------------------------------------------------ FINITO, return

_out