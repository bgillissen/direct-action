/*
@filename: feats\assets\bohemia\orange\init.sqf
Author:
	Ben
Description:
	run on server.
	implent orange DLC assets
*/

private _data = [["OC", (call oc_fnc_assets), [0, 1, 2], ["unsung"]],
				 ["OO", (call oo_fnc_assets), [0], ["rhsAFRF", "unsung"]],
				 ["OB", (call ob_fnc_assets), [1], ["rhsUSAF", "unsung"]],
                 ["OI", (call oi_fnc_assets), [2], ["rhsGREF", "unsung"]]];

["orange", _data] call assets_fnc_implent;