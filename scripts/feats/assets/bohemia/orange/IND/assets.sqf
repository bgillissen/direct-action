/*
@filename: feats\assets\bohemia\orange\IND\assets.sqf
Author:
	Ben
Description:
	run on server,
	return the orange DLC assets for IND
*/

private _out = [];

//------------------------------------------------------------ Arsenale orange DLC  IND

A_OI = 0;

private _backpacks = ["I_UAV_06_backpack_F", "I_UAV_06_medical_backpack_F"];
private _items = (call mc_fnc_getItems);
private _weapons = (call mc_fnc_getWeapons);
private _ammo = (call mc_fnc_getMagazines);

_out set [A_OI, [_backpacks, _items, _weapons, _ammo]];

//------------------------------------------------------------ Restricted Gear Vanilla IND

RG_OI = A_OI + 1;

private _launcher = [];
private _mg = [];
private _sRifle = [];
private _mRifle = [];
private _sScope = [];
private _mScope = [];
private _oScope = [];
private _mbrItem = [];
private _backpack = [];

_out set [RG_OI, [_launcher, _mg, _sRifle, _mRifle, _sScope, _mScope, _oScope, _mbrItem, _backpack]];
//------------------------------------------------------------ supplyDrop orange DLC IND

SD_OI = RG_OI + 1;

private _backpacks = [];
private _items = [];
private _weapons = [];
private _ammo = [];
private _crates = [];
private _vehicles = [];

_out set [SD_OI, [ _backpacks, _items, _weapons, _ammo, _crates, _vehicles]];

//------------------------------------------------------------ Vehicles Cargo orange DLC IND

VC_OI = SD_OI + 1;

/*C_IDAP_Van_02_medevac_F
C_IDAP_Van_02_vehicle_F
C_IDAP_Van_02_transport_F
C_Van_02_medevac_F
C_Van_02_vehicle_F
C_Van_02_service_F
C_Van_02_transport_F*/

private _car = [[],[],[],[]];
private _carArmed = [[],[],[],[]];
private _apc = [[],[],[],[]];
private _tank = [[],[],[],[]];
private _aaTank = [[],[],[],[]];
private _planeCAS = [[],[],[],[]];
private _planeAA = [[],[],[],[]];
private _planeTransport = [[],[],[],[]];
private _uav = [[],[],[],[]];
private _heliSmall = [[],[],[],[]];
private _heliSmallArmed = [[],[],[],[]];
private _heliMedium = [[],[],[],[]];
private _heliMedEvac = [[],[],[],[]];
private _heliBig = [[],[],[],[]];
private _heliAttack = [[],[],[],[]];
private _boatSmall = [[],[],[],[]];
private _boatAttack = [[],[],[],[]];
private _boatBig = [[],[],[],[]];
private _sub = [[],[],[],[]];
private _landMedic = [[],[],[],[]];
private _repair = [[],[],[],[]];
private _fuel = [[],[],[],[]];
private _ammo = [[],[],[],[]];
private _truck = [[],[],[],[]];
private _quad = [[],[],[],[]];
private _artiTank = [[],[],[],[]];
private _artiCannon = [[],[],[],[]];
private _artiTube = [[],[],[],[]];

_out set [VC_OI, [_car, _carArmed, _apc, _tank, _aaTank, _planeCAS, _planeAA, _planeTransport, _uav, 
                 _heliSmall, _heliSmallArmed, _heliMedium, _heliMedEvac, _heliBig, _heliAttack, 
                 _boatSmall, _boatAttack, _boatBig, _sub, _landMedic, _repair, _fuel, _ammo, _truck, _quad,
                 _artiTank, _artiCannon, _artiTube]];
//------------------------------------------------------------ Vehicles Vanilla IND

BV_OI = VC_OI + 1;

private _car = ["I_G_Van_02_vehicle_F", "I_G_Van_02_transport_F", "I_C_Van_02_vehicle_F", "I_C_Van_02_transport_F"];
private _carArmed = [];
private _apc = [];
private _tank = [];
private _aaTank = [];
private _planeCAS = [];
private _planeAA = [];
private _planeTransport = [];
private _uav = [];
private _heliSmall = [];
private _heliSmallArmed = [];
private _heliMedium = [];
private _heliMedEvac = [];
private _heliBig = [];
private _heliAttack = [];
private _boatSmall = [];
private _boatAttack = [];
private _boatBig = [];
private _sub = [];
private _landMedic = [];
private _repair = [];
private _fuel = [];
private _ammo = [];
private _truck = [];
private _quad = [];
private _artiTank = [];
private _artiCannon = [];
private _artiTube = [];

_out set [BV_OI, [_car, _carArmed, _apc, _tank, _aaTank, _planeCAS, _planeAA, _planeTransport, _uav, 
                 _heliSmall, _heliSmallArmed, _heliMedium, _heliMedEvac, _heliBig, _heliAttack, 
                 _boatSmall, _boatAttack, _boatBig, _sub, _landMedic, _repair, _fuel, _ammo, _truck, _quad,
                 _artiTank, _artiCannon, _artiTube]];
//------------------------------------------------------------ FINITO, return

_out