class orange {
	tag = "orange";
	class functions {
		class init { file="feats\assets\bohemia\orange\init.sqf"; };
		class postInit { file="core\dummy.sqf"; };
		class preInit { file="core\dummy.sqf"; };
	};
};

#include "BLUFOR\_cfgFunctions.hpp"
#include "COMMON\_cfgFunctions.hpp"
#include "IND\_cfgFunctions.hpp"
#include "OPFOR\_cfgFunctions.hpp"
