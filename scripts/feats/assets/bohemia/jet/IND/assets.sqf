/*
@filename: feats\assets\bohemia\jet\IND\assets.sqf
Author:
	Ben
Description:
	run on server,
	return the jet DLC IND assets
*/

private _out = [];

//------------------------------------------------------------ Rewards Jet DLC IND

R_JI = 0;

private _rewards = [];

_out set [R_JI, _rewards];

//------------------------------------------------------------ supplyDrop Jet DLC IND

SD_JI = R_JI + 1;

private _backpacks = [];
private _items = [];
private _weapons = [];
private _ammo = [];
private _crates = [];
private _vehicles = ["I_Heli_light_03_dynamicLoadout_F"];

_out set [SD_JI, [_backpacks, _items, _weapons, _ammo, _crates, _vehicles]];

//------------------------------------------------------------ Spawn Jet DLC IND

S_JI = SD_JI + 1;

private _rt = [];
private _crates = [];
private _pGroups = [];
private _sGroups = [];
private _pilot = [];
private _crew = [];
private _officer = [];
private _garrison = [];
private _aa = [];
private _arti = [];
private _static = [];
private _cas = ["I_Plane_Fighter_04_F", "I_Plane_Fighter_03_dynamicLoadout_F"];
private _tank = [];
private _apc = [];
private _car = [];
private _carArmed = [];
private _aPatrol = ["I_Heli_light_03_dynamicLoadout_F"];
private _civ = [];

_out set [S_JI, [_rt, _crates, _pGroups, _sGroups, _pilot, _crew, _officer, _garrison, _civ,
                 _aa, _arti, _static, _cas, _tank, _apc, _car, _carArmed, _aPatrol]];

//------------------------------------------------------------ Vehicles Jet DLC OPFOR

BV_JI = S_JI + 1;

private _car = [];
private _carArmed = [];
private _apc = [];
private _tank = [];
private _aaTank = [];
private _planeCAS = [];
private _planeAA = ["I_Plane_Fighter_04_F", "I_Plane_Fighter_03_dynamicLoadout_F"];
private _planeTransport = [];
private _uav = ["I_UAV_02_dynamicLoadout_F"];
private _heliSmall = [];
private _heliSmallArmed = ["I_Heli_light_03_dynamicLoadout_F"];
private _heliMedium = [];
private _heliMedEvac = [];
private _heliBig = [];
private _heliAttack = [];
private _boatSmall = [];
private _boatAttack = [];
private _boatBig = [];
private _sub = [];
private _landMedic = [];
private _repair = [];
private _fuel = [];
private _ammo = [];
private _truck = [];
private _quad = [];
private _artiTank = [];
private _artiCannon = [];
private _artiTube = [];

_out set [BV_JI, [_car, _carArmed, _apc, _tank, _aaTank, _planeCAS, _planeAA, _planeTransport, _uav, 
                 _heliSmall, _heliSmallArmed, _heliMedium, _heliMedEvac, _heliBig, _heliAttack, 
                 _boatSmall, _boatAttack, _boatBig, _sub, _landMedic, _repair, _fuel, _ammo, _truck, _quad,
                 _artiTank, _artiCannon, _artiTube]];
_out