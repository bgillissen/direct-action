class spawn_jet {
	title = "Add Jet DLC assets to enemy spawn pool";
	values[] = {0,1};
	texts[] = {"no", "yes"};
	default = 1;
};

class jet_modEnemy {
	title = "Jet DLC assets are spawned when RHS (enemy side) is present";
	values[] = {0,1};
	texts[] = {"no", "yes"};
	default = 0;
};

class jet_modbv {
	title = "Jet DLC vehicles are available when RHS (player side) is present";
	values[] = {0,1};
	texts[] = {"no", "yes"};
	default = 0;
};

class reward_jet {
	title = "Add Jet DLC vehicles to reward pool";
	values[] = {0,1,2,3};
	texts[] = {"no", "player side only", "player side + allies", "all"};
	default = 2;
};

class bv_jet {
	title = "Add Jet DLC vehicles to base vehicle pool";
	values[] = {0,1,2,3};
	texts[] = {"no", "player side only", "player side + allies", "all"};
	default = 2;
};
