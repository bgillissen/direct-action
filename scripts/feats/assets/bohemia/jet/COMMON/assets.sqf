/*
@filename: feats\assets\bohemia\jet\COMMON\assets.sqf
Author:
	Ben
Description:
	run on server,
	return the jet DLC assets common to all side
*/

private _out = [];

//------------------------------------------------------------ Arsenal jet DLC  (common)

A_JC = 0;

private _backpacks = [];
private _items = ["acc_flashlight_pistol"];
private _weapons = [];
private _ammo = [];

_out set [A_JC, [_backpacks, _items, _weapons, _ammo]];

//------------------------------------------------------------ FINITO, return

_out