/*
@filename: feats\assets\bohemia\jet\OPFOR\assets.sqf
Author:
	Ben
Description:
	run on server,
	return the jet DLC OPFOR assets
*/

private _out = [];

//------------------------------------------------------------ Rewards Jet OPFOR

R_JO = 0;

private _rewards = ["O_Heli_Attack_02_dynamicLoadout_F"];

_out set [R_JO, _rewards];

//------------------------------------------------------------ supplyDrop Jet DLC OPFOR

SD_JO = R_JO + 1;

private _backpacks = [];
private _items = [];
private _weapons = [];
private _ammo = [];
private _crates = [];
private _vehicles = ["O_T_VTOL_02_infantry_dynamicLoadout_F", 
                     "O_T_VTOL_02_vehicle_dynamicLoadout_F", 
                     "O_Heli_Light_02_dynamicLoadout_F"];

_out set [SD_JO, [_backpacks, _items, _weapons, _ammo, _crates, _vehicles]];

//------------------------------------------------------------ Spawn Jet DLC OPFOR

S_JO = SD_JO + 1;

private _rt = [];
private _crates = [];
private _pGroups = [];
private _sGroups = [];
private _pilot = [];
private _crew = [];
private _officer = [];
private _garrison = [];
private _aa = [];
private _arti = [];
private _static = [];
private _cas = ["O_Plane_CAS_02_dynamicLoadout_F", "O_Plane_Fighter_02_F", "O_Plane_Fighter_02_Stealth_F"];
private _tank = [];
private _apc = [];
private _car = [];
private _carArmed = [];
private _aPatrol = ["O_Heli_Attack_02_dynamicLoadout_F", "O_Heli_Light_02_dynamicLoadout_F"];
private _civ = [];

_out set [S_JO, [_rt, _crates, _pGroups, _sGroups, _pilot, _crew, _officer, _garrison, _civ,
                 _aa, _arti, _static, _cas, _tank, _apc, _car, _carArmed, _aPatrol]];

//------------------------------------------------------------ Vehicles Jet DLC OPFOR

BV_JO = S_JO + 1;

private _car = [];
private _carArmed = [];
private _apc = [];
private _tank = [];
private _aaTank = [];
private _planeCAS = ["O_Plane_CAS_02_dynamicLoadout_F"];
private _planeAA = ["O_Plane_Fighter_02_F", "O_Plane_Fighter_02_Stealth_F"];
private _planeTransport = ["O_T_VTOL_02_infantry_dynamicLoadout_F", "O_T_VTOL_02_vehicle_dynamicLoadout_F"];
private _uav = ["O_UAV_02_dynamicLoadout_F"];
private _heliSmall = [];
private _heliSmallArmed = [];
private _heliMedium = ["O_Heli_Light_02_dynamicLoadout_F"];
private _heliMedEvac = [];
private _heliBig = [];
private _heliAttack = ["O_Heli_Attack_02_dynamicLoadout_F"];
private _boatSmall = [];
private _boatAttack = [];
private _boatBig = [];
private _sub = [];
private _landMedic = [];
private _repair = [];
private _fuel = [];
private _ammo = [];
private _truck = [];
private _quad = [];
private _artiTank = [];
private _artiCannon = [];
private _artiTube = [];

_out set [BV_JO, [_car, _carArmed, _apc, _tank, _aaTank, _planeCAS, _planeAA, _planeTransport, _uav, 
                 _heliSmall, _heliSmallArmed, _heliMedium, _heliMedEvac, _heliBig, _heliAttack, 
                 _boatSmall, _boatAttack, _boatBig, _sub, _landMedic, _repair, _fuel, _ammo, _truck, _quad,
                 _artiTank, _artiCannon, _artiTube]];
_out