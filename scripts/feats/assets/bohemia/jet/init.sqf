/*
@filename: feats\assets\bohemia\jet\init.sqf
Author:
	Ben
Description:
	run on server.
	implent jet DLC assets
*/

private _data = [["JC", (call jc_fnc_assets), [0, 1, 2], ["rhsAFRF", "rhsUSAF", "rhsGREF", "unsung"]],
				 ["JO", (call jo_fnc_assets), [0], ["rhsAFRF", "unsung"]],
				 ["JB", (call jb_fnc_assets), [1], ["rhsUSAF", "unsung"]],
                 ["JI", (call ji_fnc_assets), [2], ["rhsGREF", "unsung"]]];

["jet", _data] call assets_fnc_implent;