/*
@filename: feats\assets\bohemia\jet\BLUFOR\assets.sqf
Author:
	Ben
Description:
	run on server,
	return the jet DLC BLUFOR assets
*/

private _out = [];

//------------------------------------------------------------ Rewards Jet DLC BLUFOR

R_JB = 0;

private _rewards = ["B_Heli_Attack_01_dynamicLoadout_F"];

_out set [R_JB, _rewards];

//------------------------------------------------------------ supplyDrop Jet DLC BLUFOR

SD_JB = R_JB + 1;

private _backpacks = [];
private _items = [];
private _weapons = [];
private _ammo = [];
private _crates = [];
private _vehicles = [];

_out set [SD_JB, [_backpacks, _items, _weapons, _ammo, _crates, _vehicles]];

//------------------------------------------------------------ Spawn Jet DLC BLUFOR

S_JB = SD_JB + 1;

private _rt = [];
private _crates = [];
private _pGroups = [];
private _sGroups = [];
private _pilot = [];
private _crew = [];
private _officer = [];
private _garrison = [];
private _aa = [];
private _arti = [];
private _static = [];
private _cas = ["B_Plane_CAS_01_dynamicLoadout_F", "B_Plane_Fighter_01_F", "B_Plane_Fighter_01_Stealth_F"];
private _tank = [];
private _apc = [];
private _car = [];
private _carArmed = [];
private _aPatrol = ["B_Heli_Light_01_dynamicLoadout_F"];
private _civ = [];

_out set [S_JB, [_rt, _crates, _pGroups, _sGroups, _pilot, _crew, _officer, _garrison, _civ,
                 _aa, _arti, _static, _cas, _tank, _apc, _car, _carArmed, _aPatrol]];

//------------------------------------------------------------ Vehicles Jet DLC BLUFOR

BV_JB = S_JB + 1;

private _car = [];
private _carArmed = [];
private _apc = [];
private _tank = [];
private _aaTank = [];
private _planeCAS = ["B_Plane_CAS_01_dynamicLoadout_F"];
private _planeAA = ["B_Plane_Fighter_01_F", "B_Plane_Fighter_01_Stealth_F"];
private _planeTransport = [];
private _uav = ["B_UAV_05_F", "B_T_UAV_03_dynamicLoadout_F", "B_UAV_02_dynamicLoadout_F"];
private _heliSmall = [];
private _heliSmallArmed = ["B_Heli_Light_01_dynamicLoadout_F"];
private _heliMedium = [];
private _heliMedEvac = [];
private _heliBig = [];
private _heliAttack = ["B_Heli_Attack_01_dynamicLoadout_F"];
private _boatSmall = [];
private _boatAttack = [];
private _boatBig = [];
private _sub = [];
private _landMedic = [];
private _repair = [];
private _fuel = [];
private _ammo = [];
private _truck = [];
private _quad = [];
private _artiTank = [];
private _artiCannon = [];
private _artiTube = [];

_out set [BV_JB, [_car, _carArmed, _apc, _tank, _aaTank, _planeCAS, _planeAA, _planeTransport, _uav, 
                 _heliSmall, _heliSmallArmed, _heliMedium, _heliMedEvac, _heliBig, _heliAttack, 
                 _boatSmall, _boatAttack, _boatBig, _sub, _landMedic, _repair, _fuel, _ammo, _truck, _quad,
                 _artiTank, _artiCannon, _artiTube]];
_out