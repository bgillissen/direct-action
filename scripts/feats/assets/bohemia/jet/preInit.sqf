/*
@filename: feats\assets\bohemia\jet\preInit.sqf
Author:
	Ben
Description:
	run on server
	implent jet DLC blacklist (backpack, item, weapon, ammo, veh, unit, group, object)
*/

private _veh = ["B_Plane_CAS_01_F", 
				"B_UAV_02_F", "B_UAV_02_CAS_F", "B_T_UAV_03_F", 
                "B_Heli_Light_01_armed_F", "B_Heli_Attack_01_F",
                "O_Plane_CAS_02_F", "O_T_VTOL_02_infantry_F", "O_T_VTOL_02_vehicle_F",
                "O_UAV_02_F", "O_UAV_02_CAS_F",
                "O_Heli_Attack_02_F", "O_Heli_Light_02_F",
                "I_Plane_Fighter_03_CAS_F", "I_Plane_Fighter_03_AA_F",
                "I_Heli_light_03_F",
                "I_UAV_02_F"];

[[], [], [], [], [], _veh, [], [], []] call common_fnc_addToBlacklists;