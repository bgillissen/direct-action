class jet {
	tag = "jet";
	class functions {
		class init { file="feats\assets\bohemia\jet\init.sqf"; };
		class postInit { file="core\dummy.sqf"; };
		class preInit { file="feats\assets\bohemia\jet\preInit.sqf"; };
	};
};

#include "BLUFOR\_cfgFunctions.hpp"
#include "COMMON\_cfgFunctions.hpp"
#include "IND\_cfgFunctions.hpp"
#include "OPFOR\_cfgFunctions.hpp"
