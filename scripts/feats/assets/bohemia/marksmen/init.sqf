/*
@filename: feats\assets\bohemia\marksmen\init.sqf
Author:
	Ben
Description:
	run on server.
	implent marskmen DLC assets
*/

private _data = [["MC", (call mc_fnc_assets), [0, 1, 2], ["rhsAFRF", "rhsUSAF", "rhsGREF", "unsung"]],
                 ["MO", (call mo_fnc_assets), [0], ["rhsAFRF", "unsung"]],
                 ["MB", (call mb_fnc_assets), [1], ["rhsUSAF", "unsung"]],
                 ["MI", (call mi_fnc_assets), [2], ["rhsGREF", "unsung"]]];

["marksmen", _data] call assets_fnc_implent;