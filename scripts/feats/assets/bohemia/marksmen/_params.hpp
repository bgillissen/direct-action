
class gear_marksmen {
	title = "Which marksmen DLC gear is available in arsenal";
	values[] = {0,1,2,3};
	texts[] = {"none", "player side only", "player side + allies", "all"};
	default = 1;
};

class marksmen_modgear {
	title = "Marksmen DLC gear is available when mods (player side) are present";
	values[] = {0,1};
	texts[] = {"no", "yes"};
	default = 0;
};

class rl_marksmen {
	title = "Players spawn with Marksmen DLC loadout";
	values[] = {0,1};
	texts[] = {"no", "yes"};
	default = 1;
};

class marksmen_modrl {
	title = "Player spawn with Marksmen DLC loadout when mods (player side) are present";
	values[] = {0,1};
	texts[] = {"no", "yes"};
	default = 0;
};