
[	"MMG_01_hex_F",
	"MMG_01_tan_F",

	"MMG_02_black_F",
	"MMG_02_camo_F",
	"MMG_02_sand_F",

	"srifle_DMR_02_F",
	"srifle_DMR_02_camo_F",
	"srifle_DMR_02_sniper_F",
	
	"srifle_DMR_03_F",
	"srifle_DMR_03_multicam_F",
	"srifle_DMR_03_khaki_F",
	"srifle_DMR_03_tan_F",
	"srifle_DMR_03_woodland_F",
	
	"srifle_DMR_04_F",
	"srifle_DMR_04_Tan_F",
	
	"srifle_DMR_05_blk_F",
	"srifle_DMR_05_hex_F",
	"srifle_DMR_05_tan_F",
	
	"srifle_DMR_06_camo_F",
	"srifle_DMR_06_olive_F"
] call assets_fnc_addBaseWeapons;