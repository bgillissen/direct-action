class mc {
	tag = "mc";
	class functions {
		class assets { file="feats\assets\bohemia\marksmen\COMMON\assets.sqf"; };
		class getItems { file="feats\assets\bohemia\marksmen\COMMON\getItems.sqf"; };
		class getMagazines { file="feats\assets\bohemia\marksmen\COMMON\getMagazines.sqf"; };
		class getWeapons { file="feats\assets\bohemia\marksmen\COMMON\getWeapons.sqf"; };
	};
};
