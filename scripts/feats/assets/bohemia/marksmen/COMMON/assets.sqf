/*
@filename: feats\assets\bohemia\marksmen\COMMON\assets.sqf
Author:
	Ben
Description:
	run on server,
	return the marksmen DLC assets common to all side
*/

private _out = [];

//------------------------------------------------------------ Arsenale marksmen DLC  (common)

A_MC = 0;

private _backpacks = [];
private _items = (call mc_fnc_getItems);
private _weapons = (call mc_fnc_getWeapons);
private _ammo = (_weapons call assets_fnc_getWeaponMagazines);

_out set [A_MC, [_backpacks, _items, _weapons, _ammo]];

//------------------------------------------------------------ Restricted Gear Vanilla BLUFOR

RG_MC = A_MC + 1;

private _launcher = [];
private _mg = ["MMG_01_hex_F",
           	   "MMG_01_tan_F",
           	   "MMG_02_black_F",
           	   "MMG_02_camo_F",
           	   "MMG_02_sand_F"];
private _sRifle = [];
private _mRifle = ["srifle_DMR_02_F",
                   "srifle_DMR_02_camo_F",
                   "srifle_DMR_02_sniper_F",
               	
                   "srifle_DMR_03_F",
                   "srifle_DMR_03_multicam_F",
                   "srifle_DMR_03_khaki_F",
                   "srifle_DMR_03_tan_F",
                   "srifle_DMR_03_woodland_F",
               	
               		"srifle_DMR_04_F",
               		"srifle_DMR_04_Tan_F",
               	
               		"srifle_DMR_05_blk_F",
               		"srifle_DMR_05_hex_F",
               		"srifle_DMR_05_tan_F",
               	
               		"srifle_DMR_06_camo_F",
               		"srifle_DMR_06_olive_F"];
private _sScope = [];
private _mScope = ["optic_AMS", 
                   "optic_AMS_khk",
                   "optic_AMS_snd",
                   "optic_KHS_blk",
                   "optic_KHS_hex",
                   "optic_KHS_old",
                   "optic_KHS_tan"];
private _oScope = [];
private _mbrItem = [];
private _backpack = [];

_out set [RG_MC, [_launcher, _mg, _sRifle, _mRifle, _sScope, _mScope, _oScope, _mbrItem, _backpack]];
//------------------------------------------------------------ supplyDrop marksmen DLC (common)

SD_MC = RG_MC + 1;

private _backpacks = [];
private _items = [];
private _weapons = [];
private _ammo = [];
private _crates = [];
private _vehicles = [];

_out set [SD_MC, [ _backpacks, _items, _weapons, _ammo, _crates, _vehicles]];

//------------------------------------------------------------ Vehicles Cargo marksmen DLC (common)

VC_MC = SD_MC + 1;

private _car = [[],[],[],[]];
private _carArmed = [[],[],[],[]];
private _apc = [[],[],[],[]];
private _tank = [[],[],[],[]];
private _aaTank = [[],[],[],[]];
private _planeCAS = [[],[],[],[]];
private _planeAA = [[],[],[],[]];
private _planeTransport = [[],[],[],[]];
private _uav = [[],[],[],[]];
private _heliSmall = [[],[],[],[]];
private _heliSmallArmed = [[],[],[],[]];
private _heliMedium = [[],[],[],[]];
private _heliMedEvac = [[],[],[],[]];
private _heliBig = [[],[],[],[]];
private _heliAttack = [[],[],[],[]];
private _boatSmall = [[],[],[],[]];
private _boatAttack = [[],[],[],[]];
private _boatBig = [[],[],[],[]];
private _sub = [[],[],[],[]];
private _landMedic = [[],[],[],[]];
private _repair = [[],[],[],[]];
private _fuel = [[],[],[],[]];
private _ammo = [[],[],[],[]];
private _truck = [[],[],[],[]];
private _quad = [[],[],[],[]];
private _artiTank = [[],[],[],[]];
private _artiCannon = [[],[],[],[]];
private _artiTube = [[],[],[],[]];

_out set [VC_MC, [_car, _carArmed, _apc, _tank, _aaTank, _planeCAS, _planeAA, _planeTransport, _uav, 
                 _heliSmall, _heliSmallArmed, _heliMedium, _heliMedEvac, _heliBig, _heliAttack, 
                 _boatSmall, _boatAttack, _boatBig, _sub, _landMedic, _repair, _fuel, _ammo, _truck, _quad,
                 _artiTank, _artiCannon, _artiTube]];

//------------------------------------------------------------ FINITO, return

_out