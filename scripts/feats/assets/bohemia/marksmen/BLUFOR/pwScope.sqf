params ['_role'];

private _isWood = (["jungle", "wood"] call assets_fnc_mapKeywords);

if !( _role in ["spotter", "marksman"] ) exitWith { nil };

if ( _isWood ) exitWith { "optic_AMS_khk" };

"optic_AMS_snd"