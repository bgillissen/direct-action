
params ['_role'];

private _pwMag = ([_role] call mb_fnc_pwMag);

if ( isNil "_pwMag" ) exitWith { [] };

private _pwMagAmount = nil;

switch _role do {
	case "lmg" : { _pwMagAmount = 1; };
	case "hmg" : { _pwMagAmount = 1; };
	case "marksman" : {_pwMagAmount = 5; };
	case "spotter" : { _pwMagAmount = 5; };
	case "hq";
	case "jPilot";
	case "hPilot";
	case "sl";
	case "tl";
	case "medic";
	case "assHMG";
	case "aa";
	case "assAA";
	case "at";
	case "assAT";
	case "sniper";

	case "repair";
	case "demo";
	case "engineer";
	case "grenadier";
	case "rifleman";
	case "jtac";
	case "crew";
	case "mortar";
	case "uavOp";
};

private _out = [];

if !( isNil "_pwMagAmount" ) then { 
	_out append [[_pwMag, _pwMagAmount]];
	private _aMag = ([_role] call ab_fnc_pwMag);
	if !( isNil "_aMag" ) then { _out append [[_aMag, 0]]; };
	private _vMag = ([_role] call vb_fnc_pwMag);
	if !( isNil "_vMag" ) then { _out append [[_vMag, 0]]; };
};

_out