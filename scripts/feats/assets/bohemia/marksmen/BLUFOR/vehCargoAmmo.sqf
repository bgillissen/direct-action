params ['_veh'];

private _amounts = [0, 0, 0, 0];

switch ( _veh ) do {
	case "car"; 
	case "carArmed";
	case "apc";
	case "tank";
	case "heliSmall";
	case "heliSmallArmed";
	case "heliMedium";
	case "heliBig";	
	case "boatSmall";
	case "boatAttack";
	case "boatBig";
	case "aaTank": { _amounts = [2, 5, 2, 2]; };
	case "quad": { _amounts = [1, 2, 1, 1]; };
	case "supplyDrop": { _amounts = [5, 10, 5, 5]; };
	case "planeCAS"; 
	case "planeAA";
	case "planeTransport";
	case "heliMedEvac";
	case "heliAttack";
	case "sub";
	case "landMedic";
	case "repair";
	case "fuel";
	case "ammo";
	case "truck";
	case "artiTank";
	case "artiCannon";
	case "artiTube";
	case "uav";
};

private _cargo = [];

{
	if ( (_amounts select _forEachIndex) > 0 ) then {
		private _nMag = ([_x] call mb_fnc_pwMag);
		if !( isNil "_nMag" ) then { 
			_cargo append [[_nMag, (_amounts select _forEachIndex)]]; 
			private _vMag = ([_x] call vb_fnc_pwMag);
			if !( isNil "_vMag" ) then {  _cargo append [[_vMag, 0]]; };
			private _aMag = ([_x] call ab_fnc_pwMag);
			if !( isNil "_aMag" ) then {  _cargo append [[_aMag, 0]]; };
		};
	};
} forEach ["hmg", "lmg", "marksman", "spotter"];

diag_log format ["------------ MARKSMEN -- Replacing Veh Ammo cargo of veh: %1, newCargo: %3", _veh, _amounts, _cargo];

_cargo