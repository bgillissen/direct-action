/*
@filename: feats\assets\bohemia\marksmen\BLUFOR\assets.sqf
Author:
	Ben
Description:
	call by feats\assets\bohemia\marksmen\init.sqf
	return the marksmen DLC BLUFOR assets
*/

private _out = [];

private _isWood = (["jungle", "wood"] call assets_fnc_mapKeywords);

//------------------------------------------------------------ Arsenal marksmen DLC BLUFOR

A_MB = 0;

private _backpacks = [];
private _items = (call mb_fnc_getItems);
private _weapons = [];
private _ammo = [];

_out set [A_MB, [_backpacks, _items, _weapons, _ammo]];

//------------------------------------------------------------ Restricted Gear marksmen DLC BLUFOR

RG_MB = A_MB + 1;

private _launcher = [];
private _mg = [];
private _sRifle = [];
private _mRifle = [];
private _sScope = [];
private _mScope = [];
private _oScope = [];
private _mbrItem = [];
private _backpack = [];

_out set [RG_MB, [_launcher, _mg, _sRifle, _mRifle, _sScope, _mScope, _oScope, _mbrItem, _backpack]];

//------------------------------------------------------------ supplyDrop marksmen DLC BLUFOR

SD_MB = RG_MB + 1;

private _backpacks = [];
private _items = [];
private _weapons = [];
private _ammo = (["supplyDrop"] call mb_fnc_vehCargoAmmo);
private _crates = [];
private _vehicles = [];

_out set [SD_MB, [_backpacks, _items, _weapons, _ammo, _crates, _vehicles]];

//------------------------------------------------------------ Vehicles Cargo marksmen DLC BLUFOR

VC_MB = SD_MB + 1;

private _cargos = [];
{
    _cargos append [ [[], [], [], ([_x] call mb_fnc_vehCargoAmmo)] ];
} foreach ["car", "carArmed", "apc", "tank", "aaTank", "planeCAS", "planeAA", "planeTransport", "uav", 
           "heliSmall", "heliSmallArmed", "heliMedium", "heliMedEvac", "heliBig", "heliAttack",
           "boatSmall", "boatAttack", "boatBig", "sub", "landMedic", "repair", "fuel","ammo", "truck", "quad",
           "artiTank", "artiCannon", "artiTube"];

_out set [VC_MB, _cargos];

//------------------------------------------------------------ Role Loadout marksmen DLC BLUFOR
/*
 [uniform, [inUniform]], 
 [vest, inVest]], 
 [backpack, [inBackpack]], 
 [primWeapon, [muzzle, bipod, acc, scope, ammo2, ammo]] 
 [secWeapon, [muzzle, bipod, acc, scope, ammo]], 
 [handWeapon, [muzzle, bipod, acc, scope, ammo]], 
 helmet, face, comm, terminal, map, bino, nv, watch, compass
*/
RL_MB = VC_MB + 1;

private _hq = [];
private _sl = [];
private _tl = [];
private _medic = [];
private _lmg = [[nil, []],
			    [nil, (["lmg"] call mb_fnc_vestContent)],
                [nil, (["lmg"] call mb_fnc_backpackContent)],
                [(["lmg"] call mb_fnc_primWeap), [nil, (["lmg"] call mb_fnc_pwBipod), nil, nil, nil, (["lmg"] call mb_fnc_pwMag)]],
                [nil, []],
                [nil, []],
                nil, nil, nil, nil, nil, nil, nil, nil, nil];
private _hmg = [[nil, []],
			    [nil, (["hmg"] call mb_fnc_vestContent)],
                [nil, (["hmg"] call mb_fnc_backpackContent)],
                [(["hmg"] call mb_fnc_primWeap), [nil, (["hmg"] call mb_fnc_pwBipod), nil, nil, nil, (["hmg"] call mb_fnc_pwMag)]],
                [nil, []],
                [nil, []],
                nil, nil, nil, nil, nil, nil, nil, nil, nil];
private _assHMG = [];
private _aa = [];
private _assAA = [];
private _at = [];
private _assAT = [];
private _sniper = [];
private _marksman = [[nil, []],
			         [nil, (["marksman"] call mb_fnc_vestContent)],
                     [nil, (["marksman"] call mb_fnc_backpackContent)],
                     [(["marksman"] call mb_fnc_primWeap), [nil, nil, nil, (["marksman"] call mb_fnc_pwScope), nil, (["marksman"] call mb_fnc_pwMag)]],
                     [nil, []],
                     [nil, []],
                     nil, nil, nil, nil, nil, nil, nil, nil, nil];
private _repair = [];
private _demo = [];
private _engineer = [];
private _grenadier = [];
private _rifleman = [];
private _jtac = [];
private _hPilot = [];
private _jPilot = [];
private _crew = [];
private _mortar = [];
private _uavOp = [];
private _spotter = [[nil, []],
			        [nil, (["spotter"] call mb_fnc_vestContent)],
                    [nil, (["spotter"] call mb_fnc_backpackContent)],
                    [(["spotter"] call mb_fnc_primWeap), [nil, nil, nil, (["spotter"] call mb_fnc_pwScope), nil,(["spotter"] call mb_fnc_pwMag)]],
                    [nil, []],
                    [nil, []],
                    nil, nil, nil, nil, nil, nil, nil, nil, nil];

_out set [RL_MB, [_hq, _sl, _tl, _medic, _lmg, _hmg, _assHMG, _aa, _assAA, _at, _assAT, _sniper, _marksman,
                 _repair, _demo, _engineer, _grenadier, _rifleman, _jtac, _hPilot, _jPilot, _crew,
                 _mortar, _uavOp, _spotter]];

//------------------------------------------------------------ FINITO, return
          
_out