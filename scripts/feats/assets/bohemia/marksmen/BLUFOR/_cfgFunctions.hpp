class mb {
	tag = "mb";
	class functions {
		class assets { file="feats\assets\bohemia\marksmen\BLUFOR\assets.sqf"; };
		class getItems { file="feats\assets\bohemia\marksmen\BLUFOR\getItems.sqf"; };
		class primWeap { file="feats\assets\bohemia\marksmen\BLUFOR\primWeap.sqf"; };
		class pwBipod { file="feats\assets\bohemia\marksmen\BLUFOR\pwBipod.sqf"; };
		class pwMag { file="feats\assets\bohemia\marksmen\BLUFOR\pwMag.sqf"; };
		class pwScope { file="feats\assets\bohemia\marksmen\BLUFOR\pwScope.sqf"; };
		class vestContent { file="feats\assets\bohemia\marksmen\BLUFOR\vestContent.sqf"; };
		class backpackContent { file="feats\assets\bohemia\marksmen\BLUFOR\backpackContent.sqf"; };
		class vehCargoAmmo { file="feats\assets\bohemia\marksmen\BLUFOR\vehCargoAmmo.sqf"; };
	};
};
