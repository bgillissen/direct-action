
params ['_role'];

private _pwMag = ([_role] call mb_fnc_pwMag);

if ( isNil "_pwMag" ) exitWith { [] };

private _pwMagAmount = nil;
private _nMuzzle = nil;
private _oMuzzle = nil;

switch _role do {
	case "lmg" : { _pwMagAmount = 4; _nMuzzle = "muzzle_snds_h"; _oMuzzle = "muzzle_snds_B"; };
	case "hmg" : { _pwMagAmount = 2; _nMuzzle = "muzzle_snds_338_black"; _oMuzzle = "muzzle_snds_h"; };
	case "marksman" : {_pwMagAmount = 6; _nMuzzle = "muzzle_snds_338_black"; _oMuzzle = "muzzle_snds_B"; };
	case "spotter" : { _pwMagAmount = 6; _nMuzzle = "muzzle_snds_338_black"; _oMuzzle = "muzzle_snds_B"; };
	case "hq";
	case "jPilot";
	case "hPilot";
	case "sl";
	case "tl";
	case "medic";
	case "assHMG";
	case "aa";
	case "assAA";
	case "at";
	case "assAT";
	case "sniper";
	case "repair";
	case "demo";
	case "engineer";
	case "grenadier";
	case "rifleman";
	case "jtac";
	case "crew";
	case "mortar";
	case "uavOp";
};

private _out = [];

if !( isNil "_pwMagAmount" ) then {
	_out append [[_pwMag, _pwMagAmount]];
	private _aMag = ([_role] call ab_fnc_pwMag);
	if !( isNil "_aMag" ) then { _out append [[_aMag, 0]]; };
	private _vMag = ([_role] call vb_fnc_pwMag);
	if !( isNil "_vMag" ) then { _out append [[_vMag, 0]]; };
};

if !( isNil "_nMuzzle" ) then { 
	_out append [[_nMuzzle, 1]];
	if !( isNil "_oMuzzle" ) then { _out append [[_oMuzzle, 0]]; };
};

diag_log format ["------------ MARKSMEN -- Replacing Backpack Content of role: %1, content: %2", _role, _out];

_out