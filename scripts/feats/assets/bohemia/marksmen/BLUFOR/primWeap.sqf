
params ['_role'];

if !( _role in ["spotter", "marksman", "hmg", "lmg"] ) exitWith { nil };

private _isWood = (["jungle", "wood"] call assets_fnc_mapKeywords);

if ( _role in ["spotter", "marksman"] ) exitWith { 
	if ( _isWood ) exitWith { "srifle_DMR_02_camo_F" };
	"srifle_DMR_02_sniper_F"
};

if ( _role in ["lmg"] ) exitWith { "LMG_mk200_F" };

if ( _role in ["hmg"] ) exitWith { 
	if ( _isWood ) exitWith { "MMG_02_black_F" };
	"MMG_02_sand_F"
};

nil