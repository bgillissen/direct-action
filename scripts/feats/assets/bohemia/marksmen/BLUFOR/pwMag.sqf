
params ['_role'];

if ( _role in ["spotter", "marksman"] ) exitWith { "10Rnd_338_Mag" };

if ( _role in ["lmg"] ) exitWith { "200Rnd_65x39_cased_Box_Tracer" };

if ( _role in ["hmg"] ) exitWith { "130Rnd_338_Mag" };

nil