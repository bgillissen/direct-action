params ['_role'];

if !( _role in ["hmg", "lmg"] ) exitWith { nil };

private _isWood = (["jungle", "wood"] call assets_fnc_mapKeywords);

if !( _iswood ) exitWith { nil };

"bipod_01_F_blk"