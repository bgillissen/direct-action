class mi {
	tag = "mi";
	class functions {
		class assets { file="feats\assets\bohemia\marksmen\IND\assets.sqf"; };
		class getItems { file="feats\assets\bohemia\marksmen\IND\getItems.sqf"; };
		class getWeapons { file="feats\assets\bohemia\marksmen\IND\getWeapons.sqf"; };
	};
};
