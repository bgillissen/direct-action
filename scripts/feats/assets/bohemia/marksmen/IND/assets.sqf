/*
@filename: feats\assets\bohemia\marksmen\IND\assets.sqf
Author:
	Ben
Description:
	call by feats\assets\bohemia\marksmen\init.sqf
	return the marksmen DLC IND assets
*/

private _out = [];

//------------------------------------------------------------ Arsenal marksmen DLC IND

A_MI = 0;

private _backpacks = [];
private _items = (call mi_fnc_getItems);
private _weapons = (call mi_fnc_getWeapons);
private _ammo = [];

_out set [A_MI, [_backpacks, _items, _weapons, _ammo]];

//------------------------------------------------------------ Restricted Gear marksmen DLC IND

RG_MI = A_MI + 1;

private _launcher = [];
private _mg = [];
private _sRifle = [];
private _mRifle = [];
private _sScope = [];
private _mScope = [];
private _oScope = [];
private _mbrItem = [];
private _backpack = [];

_out set [RG_MI, [_launcher, _mg, _sRifle, _mRifle, _sScope, _mScope, _oScope, _mbrItem, _backpack]];

//------------------------------------------------------------ supplyDrop marksmen DLC IND

SD_MI = RG_MI + 1;

private _backpacks = [];
private _items = [];
private _weapons = [];
private _ammo = [["10Rnd_93x64_DMR_05_Mag",15],
				 ["150Rnd_93x64_Mag",10]];
private _crates = [];
private _vehicles = [];

_out set [SD_MI, [_backpacks, _items, _weapons, _ammo, _crates, _vehicles]];

//------------------------------------------------------------ Vehicles Cargo marksmen DLC IND

VC_MI = SD_MI + 1;

private _car = [[],[],[],[]];
private _carArmed = [[],[],[],[]];
private _apc = [[],[],[],[]];
private _tank = [[],[],[],[]];
private _aaTank = [[],[],[],[]];
private _planeCAS = [[],[],[],[]];
private _planeAA = [[],[],[],[]];
private _planeTransport = [[],[],[],[]];
private _uav = [[],[],[],[]];
private _heliSmall = [[],[],[],[]];
private _heliSmallArmed = [[],[],[],[]];
private _heliMedium = [[],[],[],[]];
private _heliMedEvac = [[],[],[],[]];
private _heliBig = [[],[],[],[]];
private _heliAttack = [[],[],[],[]];
private _boatSmall = [[],[],[],[]];
private _boatAttack = [[],[],[],[]];
private _boatBig = [[],[],[],[]];
private _sub = [[],[],[],[]];
private _landMedic = [[],[],[],[]];
private _repair = [[],[],[],[]];
private _fuel = [[],[],[],[]];
private _ammo = [[],[],[],[]];
private _truck = [[],[],[],[]];
private _quad = [[],[],[],[]];
private _artiTank = [[],[],[],[]];
private _artiCannon = [[],[],[],[]];
private _artiTube = [[],[],[],[]];

_out set [VC_MI, [_car, _carArmed, _apc, _tank, _aaTank, _planeCAS, _planeAA, _planeTransport, _uav, 
                  _heliSmall, _heliSmallArmed, _heliMedium, _heliMedEvac, _heliBig, _heliAttack, 
                  _boatSmall, _boatAttack, _boatBig, _sub, _landMedic, _repair, _fuel, _ammo, _truck, _quad,
                  _artiTank, _artiCannon, _artiTube]];

//------------------------------------------------------------ Role Loadout marksmen DLC IND
/*
 [uniform, [inUniform]], 
 [vest, inVest]], 
 [backpack, [inBackpack]], 
 [primWeapon, [muzzle, bipod, acc, scope, ammo2, ammo]] 
 [secWeapon, [muzzle, bipod, acc, scope, ammo]], 
 [handWeapon, [muzzle, bipod, acc, scope, ammo]], 
 helmet, face, comm, terminal, map, bino, nv, watch, compass
*/
RL_MI = VC_MI + 1;

private _hq = [];
private _sl = [];
private _tl = [];
private _medic = [];
private _lmg = [];
private _hmg = [[nil, [["muzzle_snds_93mmg",1]]],
                [nil, [["150Rnd_93x64_Mag",1]]],
                [nil, [["150Rnd_93x64_Mag",2]]],
                ["MMG_01_hex_F", ["", "bipod_03_F_blk", nil, nil, "", "150Rnd_93x64_Mag"]],
                [nil, []],
                [nil, []],
                nil, nil, nil, nil, nil, nil, nil, nil, nil];
private _assHMG = [[nil, []],
                   [nil, []],
				   [nil, [["150Rnd_93x64_Mag",2]]],
                   [nil, []],
                   [nil, []],
                   [nil, []],
                   nil, nil, nil, nil, nil, nil, nil, nil, nil];
private _aa = [];
private _assAA = [];
private _at = [];
private _assAT = [];
private _sniper = [];
private _marksman = [[nil, [["muzzle_snds_93mmg",1]]],
                	 [nil, [["10Rnd_93x64_DMR_05_Mag",6]]],
                	 [nil, [["10Rnd_93x64_DMR_05_Mag",8]]],
                	 ["srifle_DMR_05_hex_F", ["", "bipod_03_F_blk", nil, nil, "", "10Rnd_93x64_DMR_05_Mag"]],
                	 [nil, []],
                	 [nil, []],
                	 nil, nil, nil, nil, nil, nil, nil, nil, nil];
private _repair = [];
private _demo = [];
private _engineer = [];
private _grenadier = [];
private _rifleman = [];
private _jtac = [];
private _hPilot = [];
private _jPilot = [];
private _crew = [];
private _mortar = [];
private _uavOp = [];
private _spotter = [[nil, [["muzzle_snds_93mmg",1]]],
                	[nil, [["10Rnd_93x64_DMR_05_Mag",6]]],
                	[nil, [["10Rnd_93x64_DMR_05_Mag",8]]],
                	["srifle_DMR_05_hex_F", ["", "bipod_03_F_blk", nil, nil, "", "10Rnd_93x64_DMR_05_Mag"]],
                	[nil, []],
                	[nil, []],
                	nil, nil, nil, nil, nil, nil, nil, nil, nil];

_out set [RL_MI, [_hq, _sl, _tl, _medic, _lmg, _hmg, _assHMG, _aa, _assAA, _at, _assAT, _sniper, _marksman,
                 _repair, _demo, _engineer, _grenadier, _rifleman, _jtac, _hPilot, _jPilot, _crew,
                 _mortar, _uavOp, _spotter]];

//------------------------------------------------------------ FINITO, return
          
_out