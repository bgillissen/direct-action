class mo {
	tag = "mo";
	class functions {
		class assets { file="feats\assets\bohemia\marksmen\OPFOR\assets.sqf"; };
		class getItems { file="feats\assets\bohemia\marksmen\OPFOR\getItems.sqf"; };
		class getWeapons { file="feats\assets\bohemia\marksmen\OPFOR\getWeapons.sqf"; };
	};
};
