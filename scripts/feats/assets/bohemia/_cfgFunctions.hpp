class bohemia {
	tag = "bohemia";
	class functions {
		class isArmed { file="feats\assets\bohemia\isArmed.sqf"; };
	};
};

#include "vanilla\_cfgFunctions.hpp"
#include "kart\_cfgFunctions.hpp"
#include "marksmen\_cfgFunctions.hpp"
#include "heli\_cfgFunctions.hpp"
#include "apex\_cfgFunctions.hpp"
#include "jet\_cfgFunctions.hpp"
#include "orange\_cfgFunctions.hpp"
#include "tanks\_cfgFunctions.hpp"