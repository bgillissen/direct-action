/*
@filename: feats\assets\bohemia\apex\COMMON\assets.sqf
Author:
	Ben
Description:
	run on server,
	return the apex DLC assets common to all side
*/

private _out = [];

private _isWood = (["jungle", "wood"] call assets_fnc_mapKeywords);

//------------------------------------------------------------ Arsenal Vanilla (common)

A_AC = 0;

private _backpacks = (call ac_fnc_getBackpacks);
private _items = (call ac_fnc_getItems);
private _weapons = (call ac_fnc_getWeapons);
private _ammo = (call ac_fnc_getMagazines);

_out set [A_AC, [_backpacks, _items, _weapons, _ammo]];

//------------------------------------------------------------ Restricted Gear Apex BLUFOR

RG_AC = A_AC + 1;

private _launcher = [];
private _mg = [];
private _sRifle = [];
private _mRifle = ["srifle_DMR_07_blk_F"];
private _sScope = ["optic_LRPS_tna_F", "optic_SOS_khk_F"];
private _mScope = [];
private _oScope = [];
private _mbrItem = [];
private _backpack = ["B_Bergen_tna_F", "B_Bergen_dgtl_F", "B_Bergen_mcamo_F"];

_out set [RG_AC, [_launcher, _mg, _sRifle, _mRifle, _sScope, _mScope, _oScope, _mbrItem, _backpack]];

//------------------------------------------------------------ Allowed Vehicles Vanilla (common)

AV_AC = RG_AC + 1;

private _heli = [];
private _plane = [];
private _tank = [];

_out set [AV_AC, [_heli, _plane, _tank]];

//------------------------------------------------------------ supplyDrop Vanilla (common)

SD_AC = AV_AC + 1;

private _backpacks = [];
private _items = [];
private _weapons = [];
private _ammo = [];
private _crates = [];
private _vehicles = [];

_out set [SD_AC, [ _backpacks, _items, _weapons, _ammo, _crates, _vehicles]];

//------------------------------------------------------------ Rewards Vanilla (common)

R_AC = SD_AC + 1;

private _rewards = [];

_out set [R_AC, _rewards];

//------------------------------------------------------------ Vehicles Vanilla  (common)

BV_AC = R_AC + 1;

private _car = [];
private _carArmed = [];
private _apc = [];
private _tank = [];
private _aaTank = [];
private _planeCAS = [];
private _planeAA = [];
private _planeTransport = [];
private _uav = [];
private _heliSmall = [];
private _heliSmallArmed = [];
private _heliMedium = [];
private _heliMedEvac = [];
private _heliBig = [];
private _heliAttack = [];
private _boatSmall = ["I_C_Boat_Transport_02_F"];
private _boatAttack = [];
private _boatBig = [];
private _sub = [];
private _landMedic = [];
private _repair = [];
private _fuel = [];
private _ammo = [];
private _truck = [];
private _quad = [];
private _artiTank = [];
private _artiCannon = [];
private _artiTube = [];

_out set [BV_AC, [_car, _carArmed, _apc, _tank, _aaTank, _planeCAS, _planeAA, _planeTransport, _uav, 
                _heliSmall, _heliSmallArmed, _heliMedium, _heliMedEvac, _heliBig, _heliAttack, 
                _boatSmall, _boatAttack, _boatBig, _sub, _landMedic, _repair, _fuel, _ammo, _truck, _quad,
                _artiTank, _artiCannon, _artiTube]];

//------------------------------------------------------------ Vehicles Cargo Vanilla (common)

VC_AC = BV_AC + 1;

private _items = [];
if ( _isWood ) then {
	_items = [["Laserdesignator", 0], ["Laserdesignator_01_khk_F", 1]];
};

private _car = [[],_items,[],[]];
private _carArmed = [[],_items,[],[]];
private _apc = [[],_items,[],[]];
private _tank = [[],_items,[],[]];
private _aaTank = [[],_items,[],[]];
private _planeCAS = [[],[],[],[]];
private _planeAA = [[],[],[],[]];
private _planeTransport = [[],_items,[],[]];
private _uav = [[],[],[],[]];
private _heliSmall = [[],_items,[],[]];
private _heliSmallArmed = [[],_items,[],[]];
private _heliMedium = [[],_items,[],[]];
private _heliMedEvac = [[],[],[],[]];
private _heliBig = [[],_items,[],[]];
private _heliAttack = [[],[],[],[]];
private _boatSmall = [[],_items,[],[]];
private _boatAttack = [[],_items,[],[]];
private _boatBig = [[],_items,[],[]];
private _sub = [[],[],[],[]];
private _landMedic = [[],[],[],[]];
private _repair = [[],[],[],[]];
private _fuel = [[],[],[],[]];
private _ammo = [[],[],[],[]];
private _truck = [[],[],[],[]];
private _quad = [[],[],[],[]];
private _artiTank = [[],[],[],[]];
private _artiCannon = [[],[],[],[]];
private _artiTube = [[],[],[],[]];

_out set [VC_AC, [_car, _carArmed, _apc, _tank, _aaTank, _planeCAS, _planeAA, _planeTransport, _uav, 
                 _heliSmall, _heliSmallArmed, _heliMedium, _heliMedEvac, _heliBig, _heliAttack, 
                 _boatSmall, _boatAttack, _boatBig, _sub, _landMedic, _repair, _fuel, _ammo, _truck, _quad,
                 _artiTank, _artiCannon, _artiTube]];

//------------------------------------------------------------ Role Loadout Vanilla Common
/*
 [uniform, [inUniform]], 
 [vest, inVest]], 
 [backpack, [inBackpack]], 
 [primWeapon, [primWeaponItems]] 
 [secWeapon, [secWeapItems]], 
 [handWeapon, handWeapItems]], 
 helmet, face, comm, terminal, map, bino, nv, watch, compass
*/
RL_AC = VC_AC + 1;

private _hq = [];
private _sl = [[nil, []],
               [nil, []],
               [nil, []],
               [nil, []],
               [nil, []],
               [nil, []],
               nil, nil, nil, nil, nil, (call ac_fnc_bino), nil, nil, nil];
private _tl = [[nil, []],
               [nil, []],
               [nil, []],
               [nil, []],
               [nil, []],
               [nil, []],
               nil, nil, nil, nil, nil, (call ac_fnc_bino), nil, nil, nil];
private _medic = [];
private _lmg = [];
private _hmg = [];
private _assHMG = [];
private _aa = [];
private _assAA = [];
private _at = [];
private _assAT = [];
private _sniper = [];
private _marksman = [[nil, []],
                     [nil, []],
                     [nil, []],
                     [nil, []],
                     [nil, []],
                     [nil, []],
                     nil, nil, nil, nil, nil, (call ac_fnc_bino), nil, nil, nil];
private _repair = [];
private _demo = [];
private _engineer = [];
private _grenadier = [];
private _rifleman = [];
private _jtac = [[nil, []],
                 [nil, []],
                 [nil, []],
                 [nil, []],
                 [nil, []],
                 [nil, []],
                 nil, nil, nil, nil, nil, (call ac_fnc_bino), nil, nil, nil];
private _hPilot = [];
private _jPilot = [];
private _crew = [];
private _mortar = [];
private _uavOp = [];
private _spotter = [[nil, []],
                    [nil, []],
                    [nil, []],
                    [nil, []],
                    [nil, []],
                    [nil, []],
                    nil, nil, nil, nil, nil, (call ac_fnc_bino), nil, nil, nil];

_out set [RL_AC, [_hq, _sl, _tl, _medic, _lmg, _hmg, _assHMG, _aa, _assAA, _at, _assAT, _sniper, _marksman,
                 _repair, _demo, _engineer, _grenadier, _rifleman, _jtac, _hPilot, _jPilot, _crew, 
                 _mortar, _uavOp, _spotter]];

//------------------------------------------------------------ Base Atmosphere Role Vanilla Common

BALO_AC = RL_AC + 1;

private _medic = [];
private _gear = [];
private _support = [];
private _default = [];

_out set [BALO_AC, [_medic, _gear, _support, _default]];

//------------------------------------------------------------ Spawn Vanilla Common

S_AC = BALO_AC + 1;

private _rt = [];
private _crates = [];
private _pGroups = [];
private _sGroups = [];
private _pilot = [];
private _crew = [];
private _officer = [];
private _garrison = [];
private _aa = [];
private _arti = [];
private _static = [];
private _cas = [];
private _tank = [];
private _apc = [];
private _car = [];
private _carArmed = [];
private _aPatrol = [];
private _civ = [];

_out set [S_AC, [_rt, _crates, _pGroups, _sGroups, _pilot, _crew, _officer, _garrison, _civ,
                _aa, _arti, _static, _cas, _tank, _apc, _car, _carArmed, _aPatrol]];


//------------------------------------------------------------ FINITO, return

_out