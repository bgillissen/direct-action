class ac {
	tag = "ac";
	class functions {
		class assets { file="feats\assets\bohemia\apex\COMMON\assets.sqf"; };
		class getBackpacks  { file="feats\assets\bohemia\apex\COMMON\getBackpacks.sqf"; };
		class getItems { file="feats\assets\bohemia\apex\COMMON\getItems.sqf"; };
		class getMagazines { file="feats\assets\bohemia\apex\COMMON\getMagazines.sqf"; };
		class getWeapons { file="feats\assets\bohemia\apex\COMMON\getWeapons.sqf"; };
		class bino { file="feats\assets\bohemia\apex\COMMON\bino.sqf"; };
	};
};
