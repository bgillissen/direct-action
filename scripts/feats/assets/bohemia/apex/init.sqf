/*
@filename: feats\assets\bohemia\apex\init.sqf
Author:
	Ben
Description:
	run on server.
	implent vanilla assets
*/

private _data = [["AC", (call ac_fnc_assets), [0, 1, 2], ["rhsAFRF", "rhsUSAF", "rhsGREF", "unsung"]],
				 ["AB", (call ab_fnc_assets), [1], ["rhsUSAF", "unsung"]],
                 ["AI", (call ai_fnc_assets), [2], ["rhsGREF", "unsung"]],
                 ["AO", (call apxo_fnc_assets), [0], ["rhsAFRF", "unsung"]]];                

["apex", _data] call assets_fnc_implent;