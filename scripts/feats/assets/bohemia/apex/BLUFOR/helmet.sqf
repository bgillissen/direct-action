params ['_role'];

private _isWood = (["jungle", "wood"] call assets_fnc_mapKeywords);

if !( _isWood ) exitWith { nil };

if ( _role in ["hq", "jPilot", "hPilot", "crew"] ) exitWith { nil };

if ( _role in ["spotter", "marksman", "sniper"] ) exitWith { "H_Booniehat_tna_F" };

"H_HelmetB_tna_F"