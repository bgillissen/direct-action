params ['_veh'];

private _isWood = (["jungle", "wood"] call assets_fnc_mapKeywords);

if !( _isWood ) exitWith { [] };

private _amounts = [0, 0, 0];

switch ( _veh ) do {
	case "car"; 
	case "carArmed";
	case "apc";
	case "tank";
	case "heliSmall";
	case "heliSmallArmed";
	case "heliMedium";
	case "heliBig";	
	case "boatSmall";
	case "boatAttack";
	case "boatBig";
	case "aaTank": { _amounts = [20, 5, 10]; };
	case "quad": { _amounts = [5, 1, 2]; };
	case "supplyDrop": { _amounts = [30, 10, 15]; };
	case "planeCAS"; 
	case "planeAA";
	case "planeTransport";
	case "heliMedEvac";
	case "heliAttack";
	case "sub";
	case "landMedic";
	case "repair";
	case "fuel";
	case "ammo";
	case "truck";
	case "artiTank";
	case "artiCannon";
	case "artiTube";
	case "uav";
};

private _cargo = [];

{
	if ( (_amounts select _forEachIndex) > 0 ) then {
		private _nMag = ([_x] call ab_fnc_pwMag);
		if !( isNil "_nMag" ) then { 
			_cargo append [[_nMag, (_amounts select _forEachIndex)]]; 
			private _oMag = ([_x] call vb_fnc_pwMag);
			if !( isNil "_oMag" ) then {  _cargo append [[_oMag, 0]]; };
		};		
	};
} forEach ["rifleman", "hmg", "lmg"];

diag_log format ["------------ APEX -- Replacing Veh Ammo cargo of veh: %1, newCargo: %3", _veh, _amounts, _cargo];

_cargo