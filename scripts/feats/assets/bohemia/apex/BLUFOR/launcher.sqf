params ['_role'];

private _isWood = (["jungle", "wood"] call assets_fnc_mapKeywords);

if !( _isWood ) exitWith { nil };

if ( _role isEqualTo "aa" ) exitWith { "launch_B_Titan_tna_F" };

if ( _role isEqualTo "at" ) exitWith { "launch_B_Titan_short_tna_F" };

if ( _role isEqualTo "rifleman" ) exitWith { "launch_RPG32_ghex_F" };

nil