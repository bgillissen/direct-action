/*
@filename: feats\assets\bohemia\apex\BLUFOR\assets.sqf
Author:
	Ben
Description:
	call by feats\assets\bohemia\apex\init.sqf
	return the Apex BLUFOR assets
*/

private _out = [];

//------------------------------------------------------------ Arsenal Apex BLUFOR

A_AB = 0;

private _backpacks = [];
private _items = (call ab_fnc_getItems);
private _weapons = (call ab_fnc_getWeapons);
private _ammo = (_weapons call ab_fnc_getMagazines);
//private _ammo = (_weapons call assets_fnc_getWeaponMagazines);

_out set [A_AB, [_backpacks, _items, _weapons, _ammo]];

//------------------------------------------------------------ Restricted Gear Apex BLUFOR

RG_AB = A_AB + 1;

private _launcher = [];
private _mg = ["arifle_MX_SW_khk_F",
			   "arifle_SPAR_02_blk_F",
			   "arifle_SPAR_02_khk_F",
			   "arifle_SPAR_02_snd_F"];
private _sRifle = ["srifle_LRR_tna_F"];
private _mRifle = ["arifle_SPAR_03_blk_F",
				   "arifle_SPAR_03_khk_F",
				   "arifle_SPAR_03_snd_F"];
private _sScope = [];
private _mScope = [];
private _oScope = [];
private _mbrItem = [];
private _backpack = [];

_out set [RG_AB, [_launcher, _mg, _sRifle, _mRifle, _sScope, _mScope, _oScope, _mbrItem, _backpack]];

//------------------------------------------------------------ Allowed Vehicles Apex BLUFOR

AV_AB = RG_AB + 1;

private _heli = ["B_Heli_Light_01_F"];
private _plane = [];
private _tank = [];

_out set [AV_AB, [_heli, _plane, _tank]];

//------------------------------------------------------------ supplyDrop Apex BLUFOR

SD_AB = AV_AB + 1;

private _backpacks = [];
private _items = [];
private _weapons = (["supplyDrop"] call ab_fnc_vehCargoWeapons);
private _ammo = (["supplyDrop"] call ab_fnc_vehCargoAmmo);
private _crates = [];
private _vehicles = ["B_CTRG_Heli_Transport_01_sand_F", 
					 "B_CTRG_Heli_Transport_01_tropic_F", 
                     "B_T_VTOL_01_infantry_F", 
                     "B_T_VTOL_01_vehicle_F", 
                     "B_T_VTOL_01_armed_F"];

_out set [SD_AB, [_backpacks, _items, _weapons, _ammo, _crates, _vehicles]];

//------------------------------------------------------------ Rewards Apex BLUFOR

R_AB = SD_AB + 1;

private _rewards = ["B_T_VTOL_01_armed_F"];

_out set [R_AB, _rewards];

//------------------------------------------------------------ Spawn Apex BLUFOR

S_AB = R_AB + 1;

private _rt = [];
private _crates = [];
private _pGroups = [["West", "BLU_CTRG_F", "Infantry"],
					["West", "BLU_T_F", "Infantry"]];
private _sGroups = [["West", "BLU_T_F", "Infantry", "B_T_SniperTeam"]];
private _pilot = [];
private _crew = [];
private _officer = [];
private _garrison = [];
private _aa = [];
private _arti = [];
private _static = [];
private _cas = [];
private _tank = [];
private _apc = [];
private _car = [];
private _carArmed = [];
private _aPatrol = [];
private _civ = [];

_out set [S_AB, [_rt, _crates, _pGroups, _sGroups, _pilot, _crew, _officer, _garrison, _civ, 
                 _aa, _arti, _static, _cas, _tank, _apc, _car, _carArmed, _aPatrol]];

//------------------------------------------------------------ Vehicles Apex BLUFOR

BV_AB = S_AB + 1;

private _car = ["B_T_MRAP_01_F", "B_LSV_01_unarmed_F", "B_T_LSV_01_unarmed_F", "B_CTRG_LSV_01_light_F"];
private _carArmed = ["B_T_MRAP_01_gmg_F", "B_T_MRAP_01_hmg_F", "B_LSV_01_armed_F", "B_T_LSV_01_armed_F"];
private _apc = ["B_T_APC_Tracked_01_rcws_F", "B_T_APC_Wheeled_01_cannon_F"];
private _tank = ["B_T_MBT_01_cannon_F", "B_T_MBT_01_arty_F", "B_T_MBT_01_mlrs_F", "B_T_MBT_01_TUSK_F"];
private _aaTank = ["B_T_APC_Tracked_01_AA_F"];
private _planeCAS = [];
private _planeAA = [];
private _planeTransport = ["B_T_VTOL_01_infantry_F", "B_T_VTOL_01_vehicle_F"];
private _uav = ["B_T_UAV_03_F"];
private _heliSmall = ["B_Heli_Light_01_F"];
private _heliSmallArmed = ["B_Heli_Light_01_armed_F"];
private _heliMedium = ["B_CTRG_Heli_Transport_01_sand_F", "B_CTRG_Heli_Transport_01_tropic_F"];
private _heliMedEvac = [];
private _heliBig = [];
private _heliAttack = [];
private _boatSmall = [];
private _boatAttack = [];
private _boatBig = [];
private _sub = [];
private _landMedic = ["B_T_Truck_01_medical_F"];
private _repair = [/*"B_T_APC_Tracked_01_CRV_F"*/];
private _fuel = ["B_T_Truck_01_fuel_F"];
private _ammo = ["B_T_Truck_01_ammo_F"];
private _truck = ["B_T_Truck_01_transport_F", "B_T_Truck_01_covered_F", "B_T_Truck_01_mover_F"];
private _quad = ["B_T_Quadbike_01_F"];
private _artiTank = [];
private _artiCannon = [];
private _artiTube = [];

_out set [BV_AB, [_car, _carArmed, _apc, _tank, _aaTank, _planeCAS, _planeAA, _planeTransport, _uav, 
                 _heliSmall, _heliSmallArmed, _heliMedium, _heliMedEvac, _heliBig, _heliAttack, 
                 _boatSmall, _boatAttack, _boatBig, _sub, _landMedic, _repair, _fuel, _ammo, _truck, _quad,
                 _artiTank, _artiCannon, _artiTube]];

//------------------------------------------------------------ Vehicles Cargo Apex BLUFOR

VC_AB = BV_AB + 1;

private _cargos = [];
{
    _cargos append [ [[], [], ([_x] call ab_fnc_vehCargoWeapons), ([_x] call ab_fnc_vehCargoAmmo)] ];
} foreach ["car", "carArmed", "apc", "tank", "aaTank", "planeCAS", "planeAA", "planeTransport", "uav", 
           "heliSmall", "heliSmallArmed", "heliMedium", "heliMedEvac", "heliBig", "heliAttack",
           "boatSmall", "boatAttack", "boatBig", "sub", "landMedic", "repair", "fuel","ammo", "truck", "quad",
           "artiTank", "artiCannon", "artiTube"];

_out set [VC_AB, _cargos];

//------------------------------------------------------------ Role Loadout Apex BLUFOR
/*
 [uniform, [inUniform]], 
 [vest, inVest]], 
 [backpack, [inBackpack]], 
 [primWeapon, [muzzle, bipod, acc, scope, ammo2, ammo]] 
 [secWeapon, [muzzle, bipod, acc, scope, ammo]], 
 [handWeapon, [muzzle, bipod, acc, scope, ammo]], 
 helmet, face, comm, terminal, map, bino, nv, watch, compass
*/
RL_AB = VC_AB + 1;

private _loadouts = [];
{
    _loadouts append [[[([_x] call ab_fnc_uniform), []],
                       [([_x] call ab_fnc_vest), ([_x] call ab_fnc_vestContent)],
                       [([_x] call ab_fnc_backpack), ([_x] call ab_fnc_backpackContent)],
                       [([_x] call ab_fnc_primWeap), [nil, ([_x] call ab_fnc_pwBipod), nil, ([_x] call ab_fnc_pwScope), nil, ([_x] call ab_fnc_pwMag)]],
                       [nil, []],
                       [nil, []],
                       ([_x] call ab_fnc_helmet), nil, nil, nil, nil, nil, nil, nil, nil]];
} foreach ["hq", "sl", "tl", "medic", "lmg", "hmg", "assHMG", "aa", "assAA", "at", "assAT", "sniper", "marksman",
           "repair", "demo", "engineer", "grenadier", "rifleman", "jtac", "hPilot", "jPilot", "crew",
           "mortar", "uavOp", "spotter"];
_out set [RL_AB, _loadouts];


//------------------------------------------------------------ Base Atmosphere Role Apex BLUFOR

BALO_AB = RL_AB + 1;

private _medic = [[(["medic"] call ab_fnc_uniform), []],
               	  [(["medic"] call ab_fnc_vest), []],
                  ["", []],
                  ["", []],
                  ["", []],
                  ["", []],
                  "", "", "", "", "", "", "", "", ""];
private _gear = [[(["hq"] call ab_fnc_uniform), []],
               	 ["", []],
                 ["", []],
                 ["", []],
                 ["", []],
                 ["", []],
                 "", "", "", "", "", "", "", "", ""];
private _support = [[(["hq"] call ab_fnc_uniform), []],
               	    ["", []],
                    ["", []],
                    ["", []],
                    ["", []],
                    ["", []],
                    "", "", "", "", "", "", "", "", ""];
private _default = [[(["hq"] call ab_fnc_uniform), []],
               	    ["", []],
                    ["", []],
                    ["", []],
                    ["", []],
                    ["", []],
                    "", "", "", "", "", "", "", "", ""];

_out set [BALO_AB, [_medic, _gear, _support, _default]];

//------------------------------------------------------------ FINITO, return
          
_out