
params ['_role'];

private _isWood = (["jungle", "wood"] call assets_fnc_mapKeywords);

if !( _isWood ) exitWith { nil };

if ( _role in ["hq", "jPilot"] ) exitWith { nil };

if ( _role in ["medic", "engineer", "demo", "repair"] ) exitWith { "B_Kitbag_rgr" };

if ( _role in ["at", "aa", "assAT", "assAA"] ) exitWith { "B_Carryall_oli" };

"B_AssaultPack_tna_F"