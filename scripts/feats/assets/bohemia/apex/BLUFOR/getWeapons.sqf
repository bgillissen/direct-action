
[	"SMG_05_F",	//protector
	
    "arifle_MX_GL_khk_F",	//MX
	"arifle_MX_khk_F",
	"arifle_MX_SW_khk_F",
	"arifle_MXC_khk_F",
	"arifle_MXM_khk_F",
    
    "launch_B_Titan_tna_F", //launchers
    "launch_B_Titan_short_tna_F",
    
    "arifle_SPAR_01_blk_F",	//spar-16s
	"arifle_SPAR_01_khk_F",
	"arifle_SPAR_01_snd_F",
	"arifle_SPAR_01_GL_blk_F",
	"arifle_SPAR_01_GL_khk_F",
    "arifle_SPAR_01_GL_snd_F",
    
    "arifle_SPAR_02_blk_F",	//lmg
	"arifle_SPAR_02_khk_F",
	"arifle_SPAR_02_snd_F",
    
	"arifle_SPAR_03_blk_F",//maksman
	"arifle_SPAR_03_khk_F",
	"arifle_SPAR_03_snd_F",
    
    "srifle_LRR_tna_F"	//sniper 
] call assets_fnc_addBaseWeapons;