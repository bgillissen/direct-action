
[
    "30Rnd_9x21_Green_Mag", //pdw2000, p07, rook-40

    "30Rnd_9x21_Mag_SMG_02",
    "30Rnd_9x21_Mag_SMG_02_Tracer_Red",
    "30Rnd_9x21_Mag_SMG_02_Tracer_Yellow",
    "30Rnd_9x21_Mag_SMG_02_Tracer_Green",

    "30Rnd_65x39_caseless_khaki_mag", //MX khaki
    "30Rnd_65x39_caseless_khaki_mag_Tracer",
	"100Rnd_65x39_caseless_khaki_mag",
    "100Rnd_65x39_caseless_khaki_mag_Tracer",

	"150Rnd_556x45_Drum_Mag_F", //SPAR-16
    "150Rnd_556x45_Drum_Mag_Tracer_F",
    "150Rnd_556x45_Drum_Sand_Mag_F",
    "150Rnd_556x45_Drum_Sand_Mag_Tracer_F",
    "150Rnd_556x45_Drum_Green_Mag_F",
    "150Rnd_556x45_Drum_Green_Mag_Tracer_F"
]