params ['_role'];

private _isWood = (["jungle", "wood"] call assets_fnc_mapKeywords);

if !( _isWood ) exitWith { nil };

if ( _role in ["hq", "jPilot", "spotter", "marksman", "sniper"] ) exitWith { nil };

"optic_Hamr_khk_F"