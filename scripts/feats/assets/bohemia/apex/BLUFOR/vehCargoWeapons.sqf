params ['_veh'];

private _isWood = (["jungle", "wood"] call assets_fnc_mapKeywords);

if !( _isWood ) exitWith { [] };

private _amount = 0;

switch ( _veh ) do {
	case "car"; 
	case "carArmed";
	case "apc";
	case "tank";
	case "heliSmall";
	case "heliSmallArmed";
	case "heliMedium";
	case "heliBig";	
	case "boatSmall";
	case "boatAttack";
	case "boatBig";
	case "supplyDrop";
	case "aaTank": { _amount = 1; };
	case "planeCAS"; 
	case "planeAA";
	case "planeTransport";
	case "heliMedEvac";
	case "heliAttack";
	case "sub";
	case "landMedic";
	case "repair";
	case "fuel";
	case "ammo";
	case "truck";
	case "quad";
	case "artiTank";
	case "artiCannon";
	case "artiTube";
	case "uav";
};

private _cargo = [];

if ( _amount > 0 ) then {
	private _nWeap = (["rifleman"] call ab_fnc_primWeap);
	if !( isNil "_nWeap" ) then { 
		_cargo append [[_nWeap, _amount]]; 
		private _oWeap = (["rifleman"] call vb_fnc_primWeap);
		if !( isNil "_oWeap" ) then { _cargo append [[_oWeap, 0]]; };
	};
};

_cargo