
params ['_role'];

private _isWood = (["jungle", "wood"] call assets_fnc_mapKeywords);

if !( _isWood ) exitWith { nil };

if ( _role in ["hq", "hPilot", "jPilot"] ) exitWith { nil };

if ( _role in ["sl", "tl", "grenadier"] ) exitWith { "V_PlateCarrierGL_tna_F" };

"V_PlateCarrier2_tna_F"