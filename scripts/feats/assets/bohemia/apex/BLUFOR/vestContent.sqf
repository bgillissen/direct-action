
params ['_role'];

private _isWood = (["jungle", "wood"] call assets_fnc_mapKeywords);

if !( _isWood ) exitWith { [] };

private _pwMag = ([_role] call ab_fnc_pwMag);

if ( isNil "_pwMag" ) exitWith { [] };

private _pwMagAmount = nil;

switch _role do {
	case "hq" : {  };
	case "jPilot" : {  };
	case "hPilot" : {  };
	case "sl" : { _pwMagAmount = 7; };
	case "tl" : { _pwMagAmount = 7; };
	case "medic" : { _pwMagAmount = 9; };
	case "lmg" : { _pwMagAmount = 3; };
	case "hmg" : {  };
	case "assHMG" : { _pwMagAmount = 9; };
	case "aa" : { _pwMagAmount = 9; };
	case "assAA" : { _pwMagAmount = 9; };
	case "at" : { _pwMagAmount = 9; };
	case "assAT" : {  _pwMagAmount = 9; };
	case "sniper" : {  };
	case "marksman" : {  };
	case "repair" : { _pwMagAmount = 9; };
	case "demo" : { _pwMagAmount = 9;} ;
	case "engineer" : { _pwMagAmount = 9; };
	case "grenadier" : { _pwMagAmount = 7; };
	case "rifleman" : { _pwMagAmount = 9; };
	case "jtac" : {  };
	case "crew" : { _pwMagAmount = 9; };
	case "mortar" : { _pwMagAmount = 9; };
	case "uavOp" : { _pwMagAmount = 9; };
	case "spotter" : {  };
};

if ( isNil "_pwMagAmount" ) exitWith { [] };

[[([_role] call vb_fnc_pwMag), 0], [_pwMag, _pwMagAmount]]