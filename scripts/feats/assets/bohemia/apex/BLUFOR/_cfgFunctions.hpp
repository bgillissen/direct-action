class ab {
	tag = "ab";
	class functions {
		class assets { file="feats\assets\bohemia\apex\BLUFOR\assets.sqf"; };
		class getItems { file="feats\assets\bohemia\apex\BLUFOR\getItems.sqf"; };
		class getMagazines { file="feats\assets\bohemia\apex\BLUFOR\getMagazines.sqf"; };
		class getWeapons { file="feats\assets\bohemia\apex\BLUFOR\getWeapons.sqf"; };
		class uniform { file="feats\assets\bohemia\apex\BLUFOR\uniform.sqf"; };
		class vest { file="feats\assets\bohemia\apex\BLUFOR\vest.sqf"; };
		class vestContent { file="feats\assets\bohemia\apex\BLUFOR\vestContent.sqf"; };
		class backpack { file="feats\assets\bohemia\apex\BLUFOR\backpack.sqf"; };
		class backpackContent { file="feats\assets\bohemia\apex\BLUFOR\backpackContent.sqf"; };
		class primWeap { file="feats\assets\bohemia\apex\BLUFOR\primWeap.sqf"; };
		class pwScope { file="feats\assets\bohemia\apex\BLUFOR\pwScope.sqf"; };
		class pwBipod { file="feats\assets\bohemia\apex\BLUFOR\pwBipod.sqf"; };
		class pwMag { file="feats\assets\bohemia\apex\BLUFOR\pwMag.sqf"; };
		class launcher { file="feats\assets\bohemia\apex\BLUFOR\launcher.sqf"; };
		class helmet { file="feats\assets\bohemia\apex\BLUFOR\helmet.sqf"; };
		class vehCargoWeapons { file="feats\assets\bohemia\apex\BLUFOR\vehCargoWeapons.sqf"; };
		class vehCargoAmmo { file="feats\assets\bohemia\apex\BLUFOR\vehCargoAmmo.sqf"; };
	};
};
