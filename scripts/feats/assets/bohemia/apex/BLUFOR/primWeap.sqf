params ['_role'];

private _isWood = (["jungle", "wood"] call assets_fnc_mapKeywords);

if !( _isWood ) exitWith { nil };

if ( _role in ["hq", "jPilot", "spotter", "marksman", "sniper", "hmg"] ) exitWith { nil };

if ( _role in ["sl", "tl", "grenadier"] ) exitWith { "arifle_MX_GL_khk_F" };

if ( _role in ["lmg"] ) exitWith { "arifle_MX_SW_khk_F" };

if ( _role in ["hPilot", "crew"] ) exitWith { "arifle_MXC_khk_F" };

"arifle_MX_khk_F"