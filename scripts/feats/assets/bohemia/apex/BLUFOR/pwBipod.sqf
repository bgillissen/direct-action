params ['_role'];

private _isWood = (["jungle", "wood"] call assets_fnc_mapKeywords);

if !( _isWood ) exitWith { nil };

if ( _role in ["hq", "jPilot", "sniper", "sl", "tl", "grenadier"] ) exitWith { nil };

"bipod_01_F_khk"