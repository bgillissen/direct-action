params ['_role'];

private _isWood = (["jungle", "wood"] call assets_fnc_mapKeywords);

if !( _isWood ) exitWith { nil };

if ( _role in ["hq", "jPilot", "spotter", "marksman", "sniper", "hmg"] ) exitWith { nil };

if ( _role in ["lmg"] ) exitWith { "100Rnd_65x39_caseless_khaki_mag_Tracer" };

"30Rnd_65x39_caseless_khaki_mag"