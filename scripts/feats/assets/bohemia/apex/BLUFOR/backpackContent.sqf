
params ['_role'];

private _isWood = (["jungle", "wood"] call assets_fnc_mapKeywords);

if !( _isWood ) exitWith { [] };

private _pwMag = ([_role] call ab_fnc_pwMag);

if ( isNil "_pwMag" ) exitWith { [] };

private _pwMagAmount = nil;

switch _role do {
	case "hq" : {  };
	case "jPilot" : {  };
	case "hPilot" : {  };
	case "sl" : { _pwMagAmount = 5; };
	case "tl" : { _pwMagAmount = 5; };
	case "medic" : {  };
	case "lmg" : { _pwMagAmount = 4; };
	case "hmg" : {  };
	case "assHMG" : {  };
	case "aa" : {  };
	case "assAA" : {  };
	case "at" : {  };
	case "assAT" : {  _pwMagAmount = 5; };
	case "sniper" : {  };
	case "marksman" : {  };
	case "repair" : { _pwMagAmount = 5; };
	case "demo" : { _pwMagAmount = 5;} ;
	case "engineer" : { _pwMagAmount = 5; };
	case "grenadier" : { _pwMagAmount = 5; };
	case "rifleman" : { };
	case "jtac" : {  };
	case "crew" : {  };
	case "mortar" : {  };
	case "uavOp" : {  };
	case "spotter" : {  };
};

if ( isNil "_pwMagAmount" ) exitWith { [] };

if ( _role in ['lmg'] ) then {
	diag_log format["------------------------- LMG BACKPACK : %1", [[([_role] call vb_fnc_pwMag), 0], [_pwMag, _pwMagAmount]]];
};

[[([_role] call vb_fnc_pwMag), 0], [_pwMag, _pwMagAmount]]