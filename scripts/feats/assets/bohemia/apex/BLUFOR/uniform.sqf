params ['_role'];

private _isWood = (["jungle", "wood"] call assets_fnc_mapKeywords);

if !( _isWood ) exitWith { nil };

if ( _role in ["hPilot", "jPilot"] ) exitWith { nil };

if ( _role in ["hq"] ) exitWith { "U_B_T_Soldier_AR_F" };

"U_B_T_Soldier_F"