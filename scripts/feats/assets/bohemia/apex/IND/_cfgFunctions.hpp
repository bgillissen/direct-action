class ai {
	tag = "ai";
	class functions {
		class assets { file="feats\assets\bohemia\apex\IND\assets.sqf"; };
		class getBackpacks  { file="feats\assets\bohemia\apex\IND\getBackpacks.sqf"; };
		class getItems { file="feats\assets\bohemia\apex\IND\getItems.sqf"; };
		class getMagazines { file="feats\assets\bohemia\apex\IND\getMagazines.sqf"; };
		class getWeapons { file="feats\assets\bohemia\apex\IND\getWeapons.sqf"; };
	};
};
