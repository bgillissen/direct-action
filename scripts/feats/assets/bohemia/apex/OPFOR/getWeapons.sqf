
[	"arifle_ARX_blk_F", //Type 115
	"arifle_ARX_ghex_F",
	"arifle_ARX_hex_F",
	"arifle_ARX_Viper_F",
	"arifle_ARX_Viper_hex_F",

	"launch_O_Titan_ghex_F",		//launchers
	"launch_O_Titan_short_ghex_F",
	
	"arifle_CTAR_blk_F", //CAR-95 
    "arifle_CTARS_blk_F", //CAR-95-1 
	"arifle_CTARS_hex_F",
	"arifle_CTARS_ghex_F",

	"srifle_DMR_07_ghex_F" //CMR-76
] call assets_fnc_addBaseWeapons;