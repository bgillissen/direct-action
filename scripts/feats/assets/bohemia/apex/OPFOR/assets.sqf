/*
@filename: feats\assets\bohemia\apex\OPFOR\assets.sqf
Author:
	Ben
Description:
	call by feats\assets\bohemia\apex\init.sqf
	return the Apex OPFOR assets
*/

private _out = [];

//------------------------------------------------------------ Arsenal Apex OPFOR

A_AO = 0;

private _backpacks = (call apxo_fnc_getBackpacks);
private _items = (call apxo_fnc_getItems);
private _weapons = (call apxo_fnc_getWeapons);
private _ammo = (call apxo_fnc_getMagazines);

_out set [A_AO, [_backpacks, _items, _weapons, _ammo]];

//------------------------------------------------------------ Restricted Gear Apex OPFOR

RG_AO = A_AO + 1;

private _launcher = [];
private _mg = [];
private _sRifle = [];
private _mRifle = [];
private _sScope = [];
private _mScope = [];
private _oScope = [];
private _mbrItem = [];
private _backpack = [];

_out set [RG_AO, [_launcher, _mg, _sRifle, _mRifle, _sScope, _mScope, _oScope, _mbrItem, _backpack]];

//------------------------------------------------------------ Allowed Vehicles Apex OPFOR

AV_AO = RG_AO + 1;

private _heli = [];
private _plane = [];
private _tank = [];

_out set [AV_AO, [_heli, _plane, _tank]];

//------------------------------------------------------------ supplyDrop Apex OPFOR

SD_AO = AV_AO + 1;

private _backpacks = [];
private _items = [];
private _weapons = [];
private _ammo = [];
private _crates = [];
private _vehicles = ["O_T_VTOL_02_infantry_F", 
					 "O_T_VTOL_02_vehicle_F"];

_out set [SD_AO, [_backpacks, _items, _weapons, _ammo, _crates, _vehicles]];

//------------------------------------------------------------ Rewards Apex OPFOR

R_AO = SD_AO + 1;

private _rewards = [];

_out set [R_AO, _rewards];

//------------------------------------------------------------ Spawn Apex OPFOR

S_AO = R_AO + 1;

private _rt = [];
private _crates = [];
private _pGroups = [["East", "OPF_T_F", "Infantry"]];
private _sGroups = [["East", "OPF_T_F", "Infantry", "O_T_SniperTeam"]];
private _pilot = ["O_T_Pilot_F"];
private _crew = ["O_T_Crew_F"];
private _officer = ["O_T_Officer_F"];
private _garrison = ["O_T_soldier_AR_F", "O_T_Soldier_GL_F", "O_T_Soldier_F"];
private _aa = ["O_T_APC_Tracked_02_AA_ghex_F"];
private _arti = ["O_T_MBT_02_arty_ghex_F"];
private _static = [];
private _cas = [];
private _tank = ["O_T_MBT_02_cannon_ghex_F"];
private _apc = ["O_T_APC_Tracked_02_cannon_ghex_F", "O_T_APC_Wheeled_02_rcws_ghex_F"];
private _car = ["O_T_LSV_02_unarmed_F", "O_LSV_02_unarmed_F", "O_T_MRAP_02_ghex_F"];
private _carArmed = ["O_T_LSV_02_armed_F", "O_LSV_02_armed_F", "O_T_MRAP_02_hmg_ghex_F", "O_T_MRAP_02_gmg_ghex_F"];
private _aPatrol = [];
private _civ = [];

_out set [S_AO, [_rt, _crates, _pGroups, _sGroups, _pilot, _crew, _officer, _garrison, _civ,
                _aa, _arti, _static, _cas, _tank, _apc, _car, _carArmed, _aPatrol]];

//------------------------------------------------------------ Vehicles Apex OPFOR

BV_AO = S_AO + 1;

private _car = [];
private _carArmed = [];
private _apc = ["O_T_APC_Tracked_02_cannon_ghex_F", "O_T_APC_Wheeled_02_rcws_ghex_F"];
private _tank = ["O_T_MBT_02_cannon_ghex_F", "O_T_MBT_02_arty_ghex_F"];
private _aaTank = ["O_T_APC_Tracked_02_AA_ghex_F"];
private _planeCAS = [];
private _planeAA = [];
private _planeTransport = ["O_T_VTOL_02_infantry_F", "O_T_VTOL_02_vehicle_F"];
private _uav = ["O_T_UAV_04_CAS_F"];
private _heliSmall = [];
private _heliSmallArmed = [];
private _heliMedium = [];
private _heliMedEvac = [];
private _heliBig = [];
private _heliAttack = [];
private _boatSmall = ["O_T_Boat_Transport_01_F", "O_G_Boat_Transport_02_F"];
private _boatAttack = ["O_T_Boat_Armed_01_hmg_F"];
private _boatBig = [];
private _sub = [];
private _landMedic = ["O_T_Truck_03_medical_ghex_F"];
private _repair = ["O_T_Truck_03_repair_ghex_F"];
private _fuel = ["O_T_Truck_03_fuel_ghex_F"];
private _ammo = ["O_T_Truck_03_ammo_ghex_F"];
private _truck = ["O_T_Truck_03_transport_ghex_F", "O_T_Truck_03_covered_ghex_F"];
private _quad = ["O_T_Quadbike_01_ghex_F"];
private _artiTank = [];
private _artiCannon = [];
private _artiTube = [];

_out set [BV_AO, [_car, _carArmed, _apc, _tank, _aaTank, _planeCAS, _planeAA, _planeTransport, _uav, 
                _heliSmall, _heliSmallArmed, _heliMedium, _heliMedEvac, _heliBig, _heliAttack, 
                _boatSmall, _boatAttack, _boatBig, _sub, _landMedic, _repair, _fuel, _ammo, _truck, _quad,
                  _artiTank, _artiCannon, _artiTube]];

//------------------------------------------------------------ Vehicles Cargo Apex OPFOR

VC_AO = BV_AO + 1;

private _car = [[],[],[],[]];
private _carArmed = [[],[],[],[]];
private _apc = [[],[],[],[]];
private _tank = [[],[],[],[]];
private _aaTank = [[],[],[],[]];
private _planeCAS = [[],[],[],[]];
private _planeAA = [[],[],[],[]];
private _planeTransport = [[],[],[],[]];
private _uav = [[],[],[],[]];
private _heliSmall = [[],[],[],[]];
private _heliSmallArmed = [[],[],[],[]];
private _heliMedium = [[],[],[],[]];
private _heliMedEvac = [[],[],[],[]];
private _heliBig = [[],[],[],[]];
private _heliAttack = [[],[],[],[]];
private _boatSmall = [[],[],[],[]];
private _boatAttack = [[],[],[],[]];
private _boatBig = [[],[],[],[]];
private _sub = [[],[],[],[]];
private _landMedic = [[],[],[],[]];
private _repair = [[],[],[],[]];
private _fuel = [[],[],[],[]];
private _ammo = [[],[],[],[]];
private _truck = [[],[],[],[]];
private _quad = [[],[],[],[]];
private _artiTank = [[],[],[],[]];
private _artiCannon = [[],[],[],[]];
private _artiTube = [[],[],[],[]];

_out set [VC_AO, [_car, _carArmed, _apc, _tank, _aaTank, _planeCAS, _planeAA, _planeTransport, _uav, 
                 _heliSmall, _heliSmallArmed, _heliMedium, _heliMedEvac, _heliBig, _heliAttack, 
                 _boatSmall, _boatAttack, _boatBig, _sub, _landMedic, _repair, _fuel, _ammo, _truck, _quad,
                  _artiTank, _artiCannon, _artiTube]];

//------------------------------------------------------------ Role Loadout Apex OPFOR

RL_AO = VC_AO + 1;

private _hq = [];
private _sl = [];
private _tl = [];
private _medic = [];
private _lmg = [];
private _hmg = [];
private _assHMG = [];
private _aa = [];
private _assAA = [];
private _at = [];
private _assAT = [];
private _sniper = [];
private _marksman = [];
private _repair = [];
private _demo = [];
private _engineer = [];
private _grenadier = [];
private _rifleman = [];
private _jtac = [];
private _hpilot = [];
private _jPilot = [];
private _crew = [];
private _mortar = [];
private _uavOp = [];
private _spotter = [];

_out set [RL_AO, [_hq, _sl, _tl, _medic, _lmg, _hmg, _assHMG, _aa, _assAA, _at, _assAT, _sniper, _marksman,
                 _repair, _demo, _engineer, _grenadier, _rifleman, _jtac, _hPilot, _jPilot, _crew,
                 _mortar, _uavOp, _spotter]];

//------------------------------------------------------------ Base Atmosphere Role Apex OPFOR

BALO_AO = RL_AO + 1;

private _medic = [];
private _gear = [];
private _support = [];
private _default = [];

_out set [BALO_AO, [_medic, _gear, _support, _default]];

//------------------------------------------------------------ FINITO, return
          
_out