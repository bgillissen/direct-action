class apxo {
	tag = "apxo";
	class functions {
		class assets { file="feats\assets\bohemia\apex\OPFOR\assets.sqf"; };
		class getBackpacks  { file="feats\assets\bohemia\apex\OPFOR\getBackpacks.sqf"; };
		class getItems { file="feats\assets\bohemia\apex\OPFOR\getItems.sqf"; };
		class getMagazines { file="feats\assets\bohemia\apex\OPFOR\getMagazines.sqf"; };
		class getWeapons { file="feats\assets\bohemia\apex\OPFOR\getWeapons.sqf"; };
	};
};
