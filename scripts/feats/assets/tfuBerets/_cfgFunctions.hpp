class tfuBerets {
	tag = "tfuBerets";
	class functions {
		class assets { file="feats\assets\tfuBerets\assets.sqf"; };
		class init { file="feats\assets\tfuBerets\init.sqf"; };
		class postInit { file="core\dummy.sqf"; };
		class preInit { file="feats\assets\tfuBerets\preInit.sqf"; };
	};
};
