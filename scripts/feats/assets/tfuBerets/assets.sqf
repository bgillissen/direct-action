/*
@filename: feats\assets\tfuBerets\assets.sqf
Author:
	Ben
Description:
	run on server
	return TFU assets
*/

private _out = [];

//------------------------------------------------------------ Arsenal TFU Berets

A_TFU = 0;

private _backpacks = [];
private _items = ["H_Beret_TFU_Tan_01",
                  "H_Beret_TFU_Black_01",
                  "H_Beret_TFU_Green_01",
                  "H_Beret_TFU_Olive_01",
                  "H_Beret_TFU_Red_01"];
private _weapons = [];
private _ammo = [];

_out set [A_TFU, [_backpacks, _items, _weapons, _ammo]];

//------------------------------------------------------------ Restricted Gear TFU Berets

RG_TFU = A_TFU + 1;

private _launcher = [];
private _mg = [];
private _sRifle = [];
private _mRifle = [];
private _sScope = [];
private _mScope = [];
private _oScope = [];
private _mbrItems = _items;
private _backpack = [];

_out set [RG_TFU, [_launcher, _mg, _sRifle, _mRifle, _sScope, _mScope, _oScope,  _mbrItems, _backpack]];

//------------------------------------------------------------ Role Loadout TFU Berets

RL_TFU = RG_TFU + 1;

private _hq = [[nil, []],
			   [nil, []],
               [nil, []],
               [nil, []],
               [nil, []],
               [nil, []],
               "H_Beret_TFU_Black_01", nil, nil, nil, nil, nil, nil, nil, nil];
private _sl = [];
private _tl = [];
private _medic = [];
private _lmg = [];
private _hmg = [];
private _assHMG = [];
private _aa = [];
private _assAA = [];
private _at = [];
private _assAT = [];
private _sniper = [];
private _marksman = [];
private _repair = [];
private _demo = [];
private _engineer = [];
private _grenadier = [];
private _rifleman = [];
private _jtac = [];
private _hPilot = [];
private _jPilot = [];
private _crew = [];
private _mortar = [];
private _uavOp = [];
private _spotter = [];

_out set [RL_TFU, [_hq, _sl, _tl, _medic, _lmg, _hmg, _assHMG, _aa, _assAA, _at, _assAT, _sniper, _marksman,
                   _repair, _demo, _engineer, _grenadier, _rifleman, _jtac, _hPilot, _jPilot, _crew, 
                   _mortar, _uavOp, _spotter]];

//------------------------------------------------------------ Base Atmosphere Role Loadout TFU Berets

BALO_TFU = RL_TFU + 1;

private _medic = [[nil, []],
                  [nil, []],
                  [nil, []],
                  [nil, []],
                  [nil, []],
                  [nil, []],
                  "H_Beret_TFU_Black_01", nil, nil, nil, nil, nil, nil, nil, nil];
private _gear = [[nil, []],
                 [nil, []],
                 [nil, []],
                 [nil, []],
                 [nil, []],
                 [nil, []],
                 "H_Beret_TFU_Black_01", nil, nil, nil, nil, nil, nil, nil, nil];
private _support = [[nil, []],
                    [nil, []],
                    [nil, []],
                    [nil, []],
                    [nil, []],
                    [nil, []],
                    "H_Beret_TFU_Black_01", nil, nil, nil, nil, nil, nil, nil, nil];
private _default = [[nil, []],
                    [nil, []],
                    [nil, []],
                    [nil, []],
                    [nil, []],
                    [nil, []],
                    "H_Beret_TFU_Black_01", nil, nil, nil, nil, nil, nil, nil, nil];

_out set [BALO_TFU, [_medic, _gear, _support, _default]];
//------------------------------------------------------------ FINITO, return

_out