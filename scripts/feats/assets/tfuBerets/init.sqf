/*
@filename: feats\assets\tfuBerets\init.sqf
Author:
	Ben
Description:
	run on server
	implent TFU assets
*/

private _c = call tfuBerets_fnc_assets;

(_c select A_TFU) params ["_backpacks", "_items", "_weapons", "_ammo"];

[_backpacks, _items, _weapons, _ammo] call assets_fnc_implentArsenal;

(_c select RG_TFU) call assets_fnc_implentRestrictedGear;


{
	["player", _forEachIndex, (_c select RL_TFU select _forEachIndex), PLAYER_SIDE] call assets_fnc_implentRoleLoadout;
} forEach ((PV select RL_k) select 1);

{
	["npc", _forEachIndex, (_c select BALO_TFU select _forEachIndex), PLAYER_SIDE] call assets_fnc_implentRoleLoadout;
} forEach ((PV select BALO_k) select 1);