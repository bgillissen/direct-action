/*
@filename: feats\assets\advSlingLoading\preInit.sqf
Author:
	Ben
Description:
	run on server,
	configure Advanced Sling Loading
*/

//Advanced Sling Loading										 
missionNamespace setVariable ["SA_ASL_HEAVY_LIFTING_ENABLED", false, true];
missionNamespace setVariable ["ASL_SUPPORTED_VEHICLES_OVERRIDE", ["Helicopter"], true]; 
missionNamespace setVariable ["ASL_SLING_RULES_OVERRIDE",[	["Helicopter", "CAN_SLING", "All"],
															["Helicopter", "CANT_SLING", "Helicopter"],
															["Helicopter", "CANT_SLING", "Air"],
															["Helicopter", "CANT_SLING", "Tank"],
															["Helicopter", "CANT_SLING", "IFV"],
															["Helicopter", "CANT_SLING", "Truck"],
                                                            ["UAV_06_base_F", "CANT_SLING", "StaticATWeapon"],
                                                            ["UAV_06_base_F", "CANT_SLING", "StaticWeapon"]
														 ], true];