/*
@filename: feats\assets\unsung\preInit.sqf
Author:
	Ben
Description:
	run on server
	implent unsung blacklist (backpack, item, weapon, ammo, veh, unit, group, object)
*/

private _backpacks = ["ACE_TacticalLadder_Pack"];
private _items = ["MineDetector", "ItemGPS", "B_UavTerminal", "O_UavTerminal", 
				  "ACE_DAGR", 
				  "ACE_microDAGR", 
				  "ACE_ATragMX",
				  "ACE_IR_Strobe_Item",
				  "ACE_ATragMX",
                  "ACE_Kestrel4500",
                  "ACE_Cellphone",
                  "ACE_Vector",
                  "ACE_VectorDay",
                  "ACE_Yardage450",
				  "ACE_NVG_Gen1",
                  "ACE_NVG_Gen2",
                  "ACE_NVG_Gen3",
                  "ACE_NVG_Gen4",
                  "ACE_NVG_Wide",
                  "ACE_optic_Arco_2D",
                  "ACE_optic_Arco_PIP",
                  "ACE_optic_Hamr_2D",
                  "ACE_optic_Hamr_PIP",
                  "ACE_optic_LRPS_2D",
                  "ACE_optic_LRPS_PIP",
                  "ACE_optic_SOS_2D",
                  "ACE_optic_SOS_PIP",
                  "ACE_optic_MRCO_2D",
                  "ACE_optic_MRCO_PIP"];
private _ammo = ["ACE_HuntIR_M203"];                  

[_backpacks, _items, [], _ammo, [], [], [], [], []] call common_fnc_addToBlacklists;