/*
@filename: feats\assets\unsung\BLUFOR\assets.sqf
Author:
	Ben
Description:
	call by feats\assets\unsung\init.sqf
	return the Unsung BLUFOR assets
*/

private _out = [];

//------------------------------------------------------------ Arsenal Unsung BLUFOR

A_UB = 0;

private _backpacks = (call ub_fnc_getBackpacks);
private _items = (call ub_fnc_getItems);
private _weapons = (call ub_fnc_getWeapons);
private _ammo = (call ub_fnc_getMagazines);

_out set [A_UB, [_backpacks, _items, _weapons, _ammo]];

//------------------------------------------------------------ Restricted Gear Unsung BLUFOR

RG_UB = A_UB + 1;

private _launcher = [];
private _mg = ["uns_M60", "uns_M60shorty", "uns_M60support"];
private _sRifle = [];
private _mRifle = [];
private _sScope = [];
private _mScope = [];
private _oScope = [];
private _mbrItem = [];
private _backpack = [];

_out set [RG_UB, [_launcher, _mg, _sRifle, _mRifle, _sScope, _mScope, _oScope, _mbrItem, _backpack]];

//------------------------------------------------------------ Allowed Vehicles Unsung BLUFOR

AV_UB = RG_UB + 1;

private _heli = [];
private _plane = [];
private _tank = [];

_out set [AV_UB, [_heli, _plane, _tank]];

//------------------------------------------------------------ supplyDrop Unsung BLUFOR

SD_UB = AV_UB + 1;

private _backpacks = [];
private _items = [];
private _weapons = [];
private _ammo = [["uns_20Rnd_556x45_Stanag", 40],
				 ["uns_m60mag", 20],
				 ["uns_m60mag_200", 10],
				 ["uns_m2carbinemag_NT", 20],
				 ["uns_model70mag", 15],
				 ["uns_M28A2_mag", 8],
				 ["uns_m18white", 20],
				 ["uns_mk2gren", 10],
				 ["uns_M118_mag_remote", 5],
				 ["uns_mine_TM_mag", 5]
				 ];
private _crates = [];
private _vehicles = ["uns_UH1D_m60"];

_out set [SD_UB, [_backpacks, _items, _weapons, _ammo, _crates, _vehicles]];

//------------------------------------------------------------ Rewards Unsung BLUFOR

R_UB = SD_UB + 1;

private _rewards = [];

_out set [R_UB, _rewards];

//------------------------------------------------------------ Spawn Unsung BLUFOR

S_UB = R_UB + 1;

private _rt = [];
private _crates = [];
private _pGroups = [];
private _sGroups = [];
private _pilot = [];
private _crew = [];
private _officer = [];
private _garrison = [];
private _aa = [];
private _arti = [];
private _static = ["uns_M2_low", "uns_M2_high", "uns_M1919_low", "uns_M60_low", "uns_US_SearchLight", "uns_M40_106mm_US", "uns_US_MK18_low", "uns_M30_107mm_mortar",
				   "uns_M1_81mm_mortar", "uns_M1_81mm_mortar_arty", "uns_M2_60mm_mortar", "uns_M102_artillery", "uns_m114_artillery"];
private _cas = [];
private _tank = [];
private _apc = [];
private _car = [];
private _carArmed = [];
private _aPatrol = [];
private _civ = [];

_out set [S_UB, [_rt, _crates, _pGroups, _sGroups, _pilot, _crew, _officer, _garrison, _civ,
                _aa, _arti, _static, _cas, _tank, _apc, _car, _carArmed, _aPatrol]];

//------------------------------------------------------------ Vehicles Unsung BLUFOR

BV_UB = S_UB + 1;

private _car = ["uns_willys"];
private _carArmed = ["uns_willysmg50", "uns_willysmg", "uns_willysm40"];
private _apc = ["uns_M113_M2", "uns_M113_M60", "uns_M113_XM182", "uns_M113_M134", "uns_M113_30cal"];
private _tank = ["uns_M132"];
private _aaTank = ["uns_m163"];
private _planeCAS = [];
private _planeAA = [];
private _planeTransport = [];
private _uav = [];
private _heliSmall = [];
private _heliSmallArmed = [];
private _heliMedium = ["UNS_UH1B_TOW", 
					   "uns_UH1C_M21_M158", "uns_UH1C_M21_M158_M134", "uns_UH1C_M21_M200", "uns_UH1C_M21_M200_1AC", 
					   "UNS_UH1C_M3_ARA", "UNS_UH1C_M3_ARA_AP", "UNS_UH1C_M3_ARA_AT",
					   "uns_UH1C_M6_M158", "uns_UH1C_M6_M200", "uns_UH1C_M6_M200_1AC", "uns_UH1C_M6_M200_M134",
					   "uns_UH1D_m60", "uns_UH1D_m60_light",
					   "uns_UH1F_M21_M158_Hornet", "uns_UH1F_M6_M158_Hornet", "uns_UH1H_m60", "uns_UH1H_m60_light"];
private _heliMedEvac = ["uns_uh1D_med_light"];
private _heliBig = [];
private _heliAttack = [];
private _boatSmall = ["UNS_floatraft_3", "UNS_Zodiac_W"];
private _boatAttack = ["uns_pbr_m10", "uns_pbr_mk18"];
private _boatBig = ["uns_pbr"];
private _sub = [];
private _landMedic = ["uns_M577_amb"];
private _repair = ["uns_M113_ENG"];
private _fuel = [];
private _ammo = [];
private _truck = [];
private _quad = [];
private _artiTank = [];
private _artiCannon = [];
private _artiTube = [];

_out set [BV_UB, [_car, _carArmed, _apc, _tank, _aaTank, _planeCAS, _planeAA, _planeTransport, _uav, 
                _heliSmall, _heliSmallArmed, _heliMedium, _heliMedEvac, _heliBig, _heliAttack, 
                _boatSmall, _boatAttack, _boatBig, _sub, _landMedic, _repair, _fuel, _ammo, _truck, _quad,
                 _artiTank, _artiCannon, _artiTube]];

//------------------------------------------------------------ Vehicles Cargo Unsung BLUFOR

VC_UB = BV_UB + 1;

private _car = [[],[],[],[]];
private _carArmed = [[],[],[],[]];
private _apc = [[],[],[],[]];
private _tank = [[],[],[],[]];
private _aaTank = [[],[],[],[]];
private _planeCAS = [[],[],[],[]];
private _planeAA = [[],[],[],[]];
private _planeTransport = [[],[],[],[]];
private _uav = [[],[],[],[]];
private _heliSmall = [[],[],[],[]];
private _heliSmallArmed = [[],[],[],[]];
private _heliMedium = [[],[],[],[]];
private _heliMedEvac = [[],[],[],[]];
private _heliBig = [[],[],[],[]];
private _heliAttack = [[],[],[],[]];
private _boatSmall = [[],[],[],[]];
private _boatAttack = [[],[],[],[]];
private _boatBig = [[],[],[],[]];
private _sub = [[],[],[],[]];
private _landMedic = [[],[],[],[]];
private _repair = [[],[],[],[]];
private _fuel = [[],[],[],[]];
private _ammo = [[],[],[],[]];
private _truck = [[],[],[],[]];
private _quad = [[],[],[],[]];
private _artiTank = [[],[],[],[]];
private _artiCannon = [[],[],[],[]];
private _artiTube = [[],[],[],[]];

_out set [VC_UB, [_car, _carArmed, _apc, _tank, _aaTank, _planeCAS, _planeAA, _planeTransport, _uav, 
                 _heliSmall, _heliSmallArmed, _heliMedium, _heliMedEvac, _heliBig, _heliAttack, 
                 _boatSmall, _boatAttack, _boatBig, _sub, _landMedic, _repair, _fuel, _ammo, _truck, _quad,
                 _artiTank, _artiCannon, _artiTube]];

//------------------------------------------------------------ Role Loadout Unsung BLUFOR
/*
 [uniform, [inUniform]], 
 [vest, inVest]], 
 [backpack, [inBackpack]], 
 [primWeapon, [muzzle, bipod, acc, scope, ammo2, ammo]] 
 [secWeapon, [muzzle, bipod, acc, scope, ammo]], 
 [handWeapon, [muzzle, bipod, acc, scope, ammo]], 
 helmet, face, comm, terminal, map, bino, nv, watch, compass
*/

RL_UB = VC_UB + 1;

private _hq = [["UNS_USMC_Flak_F", []],
               ["UNS_M1956_A7", []],
               [nil, []],
               [nil, []],
               [nil, []],
               [nil, []],
               "UNS_M1_3A", nil, nil, nil, nil, nil, nil, nil, nil];
private _sl = [["UNS_USMC_Flak_F", []],
               ["UNS_M1956_A7", [["uns_20Rnd_556x45_Stanag", 8],["uns_m1911mag",3]]],
               ["UNS_Alice_2", [["Uns_1Rnd_HE_M381", 18],["uns_m18white",4],["uns_mk2gren", 3]]],
               ["uns_m16a1_m203", ["", "", "", "", "Uns_1Rnd_HE_M381", "uns_20Rnd_556x45_Stanag"]],
               [nil, []],
               ["uns_m1911",["uns_s_M1911", "", "", "", "uns_m1911mag"]],
               "UNS_M1_3A", nil, nil, nil, nil, nil, nil, nil, nil];
private _tl = [["UNS_USMC_Flak_F", []],
               ["UNS_M1956_A7", [["uns_20Rnd_556x45_Stanag", 8],["uns_m1911mag",3]]],
               ["UNS_Alice_2", [["Uns_1Rnd_HE_M381", 18],["uns_m18white",4],["uns_mk2gren", 3]]],
               ["uns_m16a1_m203", ["", "", "", "", "Uns_1Rnd_HE_M381", "uns_20Rnd_556x45_Stanag"]],
               [nil, []],
               ["uns_m1911", ["uns_s_M1911", "", "", "", "uns_m1911mag"]],
               "UNS_M1_3A", nil, nil, nil, nil, nil, nil, nil, nil];
private _medic = [["UNS_USMC_Flak_F", []],
                  ["UNS_M1956_A7", [["uns_20Rnd_556x45_Stanag", 8],["uns_m1911mag",3]]],
                  ["UNS_Alice_2", [["uns_m18white",15],["uns_mk2gren", 3]]],
                  ["uns_M16A1", ["", "", "", "", "", "uns_20Rnd_556x45_Stanag"]],
                  [nil, []],
                  ["uns_m1911", ["uns_s_M1911", "", "", "", "uns_m1911mag"]],
                  "UNS_M1_3A", nil, nil, nil, nil, nil, nil, nil, nil];
private _lmg = [["UNS_USMC_Flak_F", []],
                ["UNS_M1956_A11", [["uns_m60mag", 2]]],
                ["UNS_Alice_2", [["uns_m18white",4],["uns_m60mag", 4],["uns_mk2gren", 3]]],
                ["uns_M60shorty", ["", "", "", "", "", "uns_m60mag"]],
                [nil, []],
                ["uns_m1911", ["uns_s_M1911", "", "", "", "uns_m1911mag"]],
                "UNS_M1_3A", nil, nil, nil, nil, nil, nil, nil, nil];
private _hmg = [["UNS_USMC_Flak_F", []],
                ["UNS_M1956_A11", [["uns_m60mag_200", 1]]],
                ["UNS_Alice_2", [["uns_m18white",4],["uns_m60mag_200", 2],["uns_mk2gren", 3]]],
                ["uns_M60", ["", "", "", "", "", "uns_m60mag_200"]],
                [nil, []],
                ["uns_m1911", ["uns_s_M1911", "", "", "", "uns_m1911mag"]],
                "UNS_M1_3A", nil, nil, nil, nil, nil, nil, nil, nil];
private _assHMG = [["UNS_USMC_Flak_F", []],
                   ["UNS_M1956_A5", [["uns_20Rnd_556x45_Stanag", 8],["uns_m1911mag",3]]],
                   ["UNS_Alice_2", [["uns_m18white",4],["uns_m60mag_200", 3],["uns_mk2gren", 3]]],
                   ["uns_M16A1", ["", "", "", "", "", "uns_20Rnd_556x45_Stanag"]],
                   [nil, []],
                   ["uns_m1911", ["uns_s_M1911", "", "", "", "uns_m1911mag"]],
                   "UNS_M1_3A", nil, nil, nil, nil, nil, nil, nil, nil];
private _aa = [["UNS_USMC_Flak_F", []],
               ["UNS_M1956_A7", [["uns_20Rnd_556x45_Stanag", 8],["uns_m1911mag",3]]],
               ["UNS_Alice_2", [["uns_m18white",4],["uns_mk2gren", 3]]],
               ["uns_M16A1", ["", "", "", "", "", "uns_20Rnd_556x45_Stanag"]],
               [nil, []],
               ["uns_m1911", ["uns_s_M1911", "", "", "", "uns_m1911mag"]],
               "UNS_M1_3A", nil, nil, nil, nil, nil, nil, nil, nil];
private _assAA = [["UNS_USMC_Flak_F", []],
                  ["UNS_M1956_A7", [["uns_20Rnd_556x45_Stanag", 8],["uns_m1911mag",3]]],
                  ["UNS_Alice_2", [["uns_m18white",4],["uns_mk2gren", 3]]],
                  ["uns_M16A1", ["", "", "", "", "", "uns_20Rnd_556x45_Stanag"]],
                  [nil, []],
                  ["uns_m1911", ["uns_s_M1911", "", "", "", "uns_m1911mag"]],
                  "UNS_M1_3A", nil, nil, nil, nil, nil, nil, nil, nil];
private _at = [["UNS_USMC_Flak_F", []],
               ["UNS_M1956_A7", [["uns_20Rnd_556x45_Stanag", 8],["uns_m1911mag",3]]],
               ["UNS_Alice_2", [["uns_m18white",4],["uns_M28A2_mag",5],["uns_mk2gren", 3]]],
               ["uns_M16A1", ["", "", "", "", "", "uns_20Rnd_556x45_Stanag"]],
               ["uns_m20_bazooka", ["", "", "", "", "uns_M28A2_mag"]],
               ["uns_m1911", ["uns_s_M1911", "", "", "", "uns_m1911mag"]],
               "UNS_M1_3A", nil, nil, nil, nil, nil, nil, nil, nil];
private _assAT = [["UNS_USMC_Flak_F", []],
                  ["UNS_M1956_A7", [["uns_20Rnd_556x45_Stanag", 8],["uns_m1911mag",3]]],
                  ["UNS_Alice_2", [["uns_m18white",4],["uns_M28A2_mag",5],["uns_mk2gren", 3]]],
                  ["uns_M16A1", ["", "", "", "", "", "uns_20Rnd_556x45_Stanag"]],
                  [nil, []],
                  ["uns_m1911", ["uns_s_M1911", "", "", "", "uns_m1911mag"]],
                  "UNS_M1_3A", nil, nil, nil, nil, nil, nil, nil, nil];
private _sniper = [["UNS_USMC_Flak_F", []],
                   ["UNS_M1956_A7", [["uns_model70mag", 8]]],
                   [nil, []],
                   ["uns_model70_iron", ["uns_s_M14", "", "", "uns_o_Unertl8x_m70", "", "uns_model70mag"]],
                   [nil, []],
                   ["uns_m1911", ["uns_s_M1911", "", "", "", "uns_m1911mag"]],
                   "UNS_M1_3A", nil, nil, nil, nil, nil, nil, nil, nil];
private _marksman = [["UNS_USMC_Flak_F", []],
	                 ["UNS_M1956_A7", [["uns_m2carbinemag_NT", 4]]],
                  	 [nil, []],
                  	 ["uns_m2carbine", ["", "", "", "uns_o_M84", "", "uns_m2carbinemag_NT"]],
                  	 [nil, []],
                  	 ["uns_m1911", ["uns_s_M1911", "", "", "", "uns_m1911mag"]],
                  	 "UNS_M1_3A", nil, nil, nil, nil, nil, nil, nil, nil];
private _repair = [["UNS_USMC_Flak_F", []],
                   ["UNS_M1956_A7", [["uns_20Rnd_556x45_Stanag", 8],["uns_m1911mag",3]]],
                   ["UNS_Alice_2", [["uns_m18white",4],["uns_mk2gren", 3]]],
                   [nil, []],
                   [nil, []],
                   ["uns_m1911", ["uns_s_M1911", "", "", "", "uns_m1911mag"]],
                   "UNS_M1_3A", nil, nil, nil, nil, nil, nil, nil, nil];
private _demo = [["UNS_USMC_Flak_F", []],
                 ["UNS_M1956_A7", [["uns_20Rnd_556x45_Stanag", 8],["uns_m1911mag",3]]],
                 ["UNS_Alice_2", [["uns_m18white",4],["uns_mk2gren", 3],["uns_M118_mag_remote", 4],["uns_mine_TM_mag",2]]],
                 ["uns_M16A1", ["", "", "", "", "", "uns_20Rnd_556x45_Stanag"]],
                 [nil, []],
                 ["uns_m1911", ["uns_s_M1911", "", "", "", "uns_m1911mag"]],
                 "UNS_M1_3A", nil, nil, nil, nil, nil, nil, nil, nil];
private _engineer = [["UNS_USMC_Flak_F", []],
                  	 ["UNS_M1956_A7", [["uns_20Rnd_556x45_Stanag", 8],["uns_m1911mag",3]]],
                  	 ["UNS_Alice_2", [["uns_m18white",4],["uns_mk2gren", 3],["uns_M118_mag_remote", 4],["uns_mine_TM_mag",2]]],
                  	 ["uns_M16A1", ["", "", "", "", "", "uns_20Rnd_556x45_Stanag"]],
                  	 [nil, []],
                  	 ["uns_m1911", ["uns_s_M1911", "", "", "", "uns_m1911mag"]],
                  	 "UNS_M1_3A", nil, nil, nil, nil, nil, nil, nil, nil];
private _grenadier = [["UNS_USMC_Flak_F", []],
                  	  ["UNS_M1956_A7", [["uns_20Rnd_556x45_Stanag", 8],["uns_m1911mag",3]]],
                  	  ["UNS_Alice_2", [["Uns_1Rnd_HE_M381", 18],["uns_m18white",4],["uns_mk2gren", 3]]],
                  	  ["uns_m16a1_m203", ["", "", "", "", "Uns_1Rnd_HE_M381", "uns_20Rnd_556x45_Stanag"]],
                  	  [nil, []],
                  	  ["uns_m1911", ["uns_s_M1911", "", "", "", "uns_m1911mag"]],
                  	  "UNS_M1_3A", nil, nil, nil, nil, nil, nil, nil, nil];
private _rifleman = [["UNS_USMC_Flak_F", []],
                  	 ["UNS_M1956_A7", [["uns_20Rnd_556x45_Stanag", 8],["uns_m1911mag",3]]],
                  	 ["UNS_Alice_2", [["uns_m18white",4],["uns_mk2gren", 3]]],
                  	 ["uns_M16A1", ["", "", "", "", "", "uns_20Rnd_556x45_Stanag"]],
                  	 ["uns_m72", ["", "", "", "", "uns_m72rocket"]],
                  	 ["uns_m1911", ["uns_s_M1911", "", "", "", "uns_m1911mag"]],
                  	 "UNS_M1_3A", nil, nil, nil, nil, nil, nil, nil, nil];
private _jtac = [["UNS_USMC_Flak_F", []],
                 ["UNS_M1956_A7", [["uns_20Rnd_556x45_Stanag", 8],["uns_m1911mag",3]]],
                 ["UNS_Alice_2", [["uns_m18white",4],["uns_mk2gren", 3]]],
                 ["uns_M16A1", ["", "", "", "", "", "uns_20Rnd_556x45_Stanag"]],
                 [nil, []],
                 ["uns_m1911", ["uns_s_M1911", "", "", "", "uns_m1911mag"]],
                 "UNS_M1_3A", nil, nil, nil, nil, nil, nil, nil, nil];
private _hpilot = [["UNS_Pilot_BDU", [["uns_m1911mag",3]]],
                   [nil, []],
                   [nil, []],
                   [nil, []],
                   [nil, []],
                   ["uns_m1911", ["uns_s_M1911", "", "", "", "uns_m1911mag"]],
                   "UNS_HP_Helmet", nil, nil, nil, nil, nil, nil, nil, nil];
private _jPilot = [["UNS_JPilot_BDU", [["uns_m1911mag",3]]],
                   [nil, []],
                   [nil, []],
                   [nil, []],
                   [nil, []],
                   ["uns_m1911", ["uns_s_M1911", "", "", "", "uns_m1911mag"]],
                   "UNS_JP_Helmet", nil, nil, nil, nil, nil, nil, nil, nil];
private _crew = [["UNS_USMC_Flak_F", []],
                 ["UNS_M1956_A7", [["uns_20Rnd_556x45_Stanag", 8],["uns_m1911mag",3],["uns_mk2gren", 3]]],
                 [nil, []],
                 ["uns_M16A1", ["", "", "", "", "", "uns_20Rnd_556x45_Stanag"]],
                 [nil, []],
                 ["uns_m1911", ["uns_s_M1911", "", "", "", "uns_m1911mag"]],
                 "UNS_M1_3A", nil, nil, nil, nil, nil, nil, nil, nil];
private _mortar = [["UNS_USMC_Flak_F", []],
                   ["UNS_M1956_A7", [["uns_20Rnd_556x45_Stanag", 8],["uns_m1911mag",3]]],
                   ["UNS_Alice_2", [["uns_m18white",4],["uns_mk2gren", 3]]],
                   ["uns_M16A1", ["", "", "", "", "", "uns_20Rnd_556x45_Stanag"]],
                   [nil, []],
                   ["uns_m1911", ["uns_s_M1911", "", "", "", "uns_m1911mag"]],
                   "UNS_M1_3A", nil, nil, nil, nil, nil, nil, nil, nil];
private _uavOp = [["UNS_USMC_Flak_F", []],
                  ["UNS_M1956_A7", [["uns_20Rnd_556x45_Stanag", 8],["uns_m1911mag",3]]],
                  ["UNS_Alice_2", [["uns_m18white",4],["uns_mk2gren", 3]]],
                  ["uns_M16A1", ["", "", "", "", "", "uns_20Rnd_556x45_Stanag"]],
                  [nil, []],
                  ["uns_m1911", ["uns_s_M1911", "", "", "", "uns_m1911mag"]],
                  "UNS_M1_3A", nil, nil, nil, nil, nil, nil, nil, nil];
private _spotter = [["UNS_USMC_Flak_F", []],
                  	["UNS_M1956_A7", [["uns_20Rnd_556x45_Stanag", 8],["uns_m1911mag",3]]],
                  	["UNS_Alice_2", [["uns_m18white",4],["uns_mk2gren", 3]]],
                  	["uns_M16A1", ["", "", "", "", "", "uns_20Rnd_556x45_Stanag"]],
                  	[nil, []],
                  	["uns_m1911", ["uns_s_M1911", "", "", "", "uns_m1911mag"]],
                  	"UNS_M1_3A", nil, nil, nil, nil, nil, nil, nil, nil];

_out set [RL_UB, [_hq, _sl, _tl, _medic, _lmg, _hmg, _assHMG, _aa, _assAA, _at, _assAT, _sniper, _marksman,
                 _repair, _demo, _engineer, _grenadier, _rifleman, _jtac, _hPilot, _jPilot, _crew,
                 _mortar, _uavOp, _spotter]];

//------------------------------------------------------------ Base Atmosphere Role Unsung BLUFOR

BALO_UB = RL_UB + 1;

private _medic = [["UNS_USMC_Flak_F", []],
                  ["UNS_M1956_A7", []],
                  ["", []],
                  ["", []],
                  ["", []],
                  ["", []],
                  "", "", "", "", "", "", "", "", ""];
private _gear = [["UNS_USMC_Flak_F", []],
                 ["UNS_M1956_A7", []],
                 ["", []],
                 ["", []],
                 ["", []],
                 ["", []],
                 "", "", "", "", "", "", "", "", ""];
private _support = [["UNS_USMC_Flak_F", []],
                    ["UNS_M1956_A7", []],
					["", []],
                  	["", []],
                  	["", []],
					["", []],
					"", "", "", "", "", "", "", "", ""];
private _default = [["UNS_USMC_Flak_F", []],
                  	["UNS_M1956_A7", []],
                  	["", []],
                  	["", []],
                  	["", []],
                  	["", []],
                  	"", "", "", "", "", "", "", "", ""];

_out set [BALO_UB, [_medic, _gear, _support, _default]];

//------------------------------------------------------------ FINITO, return
          
_out