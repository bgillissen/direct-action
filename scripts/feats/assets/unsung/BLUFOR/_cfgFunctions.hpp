class ub {
	tag = "ub";
	class functions {
		class assets { file="feats\assets\unsung\BLUFOR\assets.sqf"; };
		class getBackpacks { file="feats\assets\unsung\BLUFOR\getBackpacks.sqf"; };
		class getItems { file="feats\assets\unsung\BLUFOR\getItems.sqf"; };
		class getMagazines { file="feats\assets\unsung\BLUFOR\getMagazines.sqf"; };
		class getWeapons { file="feats\assets\unsung\BLUFOR\getWeapons.sqf"; };
	};
};
