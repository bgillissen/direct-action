/*
@filename: feats\assets\unsung\COMMON\assets.sqf
Author:
	Ben
Description:
	call by feats\assets\unsung\init.sqf
	return the Unsung COMMON assets
*/

private _out = [];

//------------------------------------------------------------ Arsenal Unsung COMMON

A_UC = 0;

private _backpacks = [];
private _items = (call uc_fnc_getItems);
private _weapons = [];
private _ammo = [];

_out set [A_UC, [_backpacks, _items, _weapons, _ammo]];

//------------------------------------------------------------ Restricted Gear Unsung COMMON

RG_UC = A_UC + 1;

private _launcher = [];
private _mg = [];
private _sRifle = [];
private _mRifle = [];
private _sScope = [];
private _mScope = [];
private _oScope = [];
private _mbrItem = [];
private _backpack = [];

_out set [RG_UC, [_launcher, _mg, _sRifle, _mRifle, _sScope, _mScope, _oScope, _mbrItem, _backpack]];

//------------------------------------------------------------ Allowed Vehicles Unsung COMMON

AV_UC = RG_UC + 1;

private _heli = [];
private _plane = [];
private _tank = [];

_out set [AV_UC, [_heli, _plane, _tank]];

//------------------------------------------------------------ supplyDrop Unsung COMMON

SD_UC = AV_UC + 1;

private _backpacks = [];
private _items = [["FirstAidKit", 20],
                  ["Medikit", 1],
                  ["ToolKit", 1]];
private _weapons = [];
private _ammo = [];
private _crates = ["B_supplyCrate_F"];
private _vehicles = [];

_out set [SD_UC, [_backpacks, _items, _weapons, _ammo, _crates, _vehicles]];

//------------------------------------------------------------ Rewards Unsung COMMON

R_UC = SD_UC + 1;

private _rewards = [];

_out set [R_UC, _rewards];

//------------------------------------------------------------ Spawn Unsung COMMON

S_UC = R_UC + 1;

private _rt = [];
private _crates = [];
private _pGroups = [];
private _sGroups = [];
private _pilot = [];
private _crew = [];
private _officer = [];
private _garrison = [];
private _aa = [];
private _arti = [];
private _static = [];
private _cas = [];
private _tank = [];
private _apc = [];
private _car = [];
private _carArmed = [];
private _aPatrol = [];
private _civ = ["uns_civilian1",
				"uns_civilian2",
				"uns_civilian3",
				"uns_civilian4"];

_out set [S_UC, [_rt, _crates, _pGroups, _sGroups, _pilot, _crew, _officer, _garrison, _civ,
                _aa, _arti, _static, _cas, _tank, _apc, _car, _carArmed, _aPatrol]];

//------------------------------------------------------------ Vehicles Unsung COMMON

BV_UC = S_UC + 1;

private _car = [];
private _carArmed = [];
private _apc = [];
private _tank = [];
private _aaTank = [];
private _planeCAS = [];
private _planeAA = [];
private _planeTransport = [];
private _uav = [];
private _heliSmall = [];
private _heliSmallArmed = [];
private _heliMedium = [];
private _heliMedEvac = [];
private _heliBig = [];
private _heliAttack = [];
private _boatSmall = [];
private _boatAttack = [];
private _boatBig = [];
private _sub = [];
private _landMedic = [];
private _repair = [];
private _fuel = [];
private _ammo = [];
private _truck = [];
private _quad = [];
private _artiTank = [];
private _artiCannon = [];
private _artiTube = [];

_out set [BV_UC, [_car, _carArmed, _apc, _tank, _aaTank, _planeCAS, _planeAA, _planeTransport, _uav, 
                _heliSmall, _heliSmallArmed, _heliMedium, _heliMedEvac, _heliBig, _heliAttack, 
                _boatSmall, _boatAttack, _boatBig, _sub, _landMedic, _repair, _fuel, _ammo, _truck, _quad,
                 _artiTank, _artiCannon, _artiTube]];

//------------------------------------------------------------ Vehicles Cargo Unsung COMMON

VC_UC = BV_UC + 1;

private _car = [[],
                [["FirstAidKit", 20]],
                [],
                []];
private _carArmed = [[],
                     [["FirstAidKit", 20]],
                     [],
                     []];
private _apc = [[],
                [["FirstAidKit", 20]],
                [],
                []];
private _tank = [[],
                 [["FirstAidKit", 20], ["ToolKit", 1]],
                 [],
                 []];
private _aaTank = [[],
                   [["FirstAidKit", 20]],
                   [],
                   []];
private _planeCAS = [[],[],[],[]];
private _planeAA = [[],[],[],[]];
private _planeTransport = [[["B_Parachute", 10]],
                           [["FirstAidKit", 20]],
                           [],
                           []];
private _uav = [[],[],[],[]];
private _heliSmall = [[],
                      [["FirstAidKit", 10]],
                      [],
                      []];
private _heliSmallArmed = [[],
                           [["FirstAidKit", 5]],
                           [],
                           []];
private _heliMedium = [[],
                       [["FirstAidKit", 10]],
                       [],
                       []];
private _heliMedEvac = [[],
                        [["FirstAidKit", 30], ["Medikit", 2]],
                         [],
                         []];
private _heliBig = [[],
                    [["FirstAidKit", 20]],
                    [],
                    []];
private _heliAttack = [[],[],[],[]];
private _boatSmall = [[],
                      [["FirstAidKit", 10]],
                      [],
                      []];
private _boatAttack = [[],
                       [["FirstAidKit", 10]],
                       [],
                       []];
private _boatBig = [[],
                    [["FirstAidKit", 20]],
                    [],
                    [["Laserbatteries", 3]]];
private _sub = [[],[],[],[]];
private _landMedic = [[],
                      [["FirstAidKit", 30], ["Medikit", 2]],
                      [],
                      []];
private _repair = [[],[],[],[]];
private _fuel = [[],[],[],[]];
private _ammo = [[],[],[],[]];
private _truck = [[],[],[],[]];
private _quad = [[],[],[],[]];
private _artiTank = [[],[],[],[]];
private _artiCannon = [[],[],[],[]];
private _artiTube = [[],[],[],[]];

_out set [VC_UC, [_car, _carArmed, _apc, _tank, _aaTank, _planeCAS, _planeAA, _planeTransport, _uav, 
                 _heliSmall, _heliSmallArmed, _heliMedium, _heliMedEvac, _heliBig, _heliAttack, 
                 _boatSmall, _boatAttack, _boatBig, _sub, _landMedic, _repair, _fuel, _ammo, _truck, _quad,
                 _artiTank, _artiCannon, _artiTube]];

//------------------------------------------------------------ Role Loadout Unsung COMMON
/*
 [uniform, [inUniform]], 
 [vest, inVest]], 
 [backpack, [inBackpack]], 
 [primWeapon, [muzzle, bipod, acc, scope, ammo2, ammo]] 
 [secWeapon, [muzzle, bipod, acc, scope, ammo]], 
 [handWeapon, [muzzle, bipod, acc, scope, ammo]], 
 helmet, face, comm, terminal, map, bino, nv, watch, compass
*/

RL_UC = VC_UC + 1;

private _hq = [[nil, [["FirstAidKit", 2]]],
               [nil, []],
               [nil, []],
               [nil, []],
               [nil, []],
               [nil, []],
               nil, nil, "ItemRadio", nil, "ItemMap", "Binocular", nil, "ItemWatch", "ItemCompass"];
private _sl = [[nil, [["FirstAidKit", 2]]],
               [nil, []],
               [nil, [["FirstAidKit", 4]]],
               [nil, []],
               [nil, []],
               [nil, []],
               nil, nil, "ItemRadio", nil, "ItemMap", "Binocular", nil, "ItemWatch", "ItemCompass"];
private _tl = [[nil, [["FirstAidKit", 2]]],
               [nil, []],
               [nil, [["FirstAidKit", 4]]],
               [nil, []],
               [nil, []],
               [nil, []],
               nil, nil, "ItemRadio", nil, "ItemMap", "Binocular", nil, "ItemWatch", "ItemCompass"];
private _medic = [[nil, [["FirstAidKit", 2]]],
                  [nil, []],
                  [nil, [["Medikit", 1], ["FirstAidKit", 15]]],
                  [nil, []],
                  [nil, []],
                  [nil, []],
                  nil, nil, "ItemRadio", nil, "ItemMap", "Binocular", nil, "ItemWatch", "ItemCompass"];
private _lmg = [[nil, [["FirstAidKit", 2]]],
                [nil, []],
                [nil, []],
                [nil, []],
                [nil, []],
                [nil, []],
                nil, nil, "ItemRadio", nil, "ItemMap", "Binocular", nil, "ItemWatch", "ItemCompass"];
private _hmg = [[nil, [["FirstAidKit", 2]]],
                [nil, []],
                [nil, []],
                [nil, []],
                [nil, []],
                [nil, []],
                nil, nil, "ItemRadio", nil, "ItemMap", "Binocular", nil, "ItemWatch", "ItemCompass"];
private _assHMG = [[nil, [["FirstAidKit", 2]]],
                   [nil, []],
                   [nil, []],
                   [nil, []],
                   [nil, []],
                   [nil, []],
                   nil, nil, "ItemRadio", nil, "ItemMap", "Binocular", nil, "ItemWatch", "ItemCompass"];
private _aa = [[nil, [["FirstAidKit", 2]]],
               [nil, []],
               [nil, []],
               [nil, []],
               [nil, []],
               [nil, []],
               nil, nil, "ItemRadio", nil, "ItemMap", "Binocular", nil, "ItemWatch", "ItemCompass"];
private _assAA = [[nil, [["FirstAidKit", 2]]],
                  [nil, []],
                  [nil, []],
                  [nil, []],
                  [nil, []],
                  [nil, []],
                  nil, nil, "ItemRadio", nil, "ItemMap", "Binocular", nil, "ItemWatch", "ItemCompass"];
private _at = [[nil, [["FirstAidKit", 2]]],
               [nil, []],
               [nil, []],
               [nil, []],
               [nil, []],
               [nil, []],
               nil, nil, "ItemRadio", nil, "ItemMap", "Binocular", nil, "ItemWatch", "ItemCompass"];
private _assAT = [[nil, [["FirstAidKit", 2]]],
                  [nil, []],
                  [nil, []],
                  [nil, []],
                  [nil, []],
                  [nil, []],
                  nil, nil, "ItemRadio", nil, "ItemMap", "Binocular", nil, "ItemWatch", "ItemCompass"];
private _sniper = [[nil, [["FirstAidKit", 2]]],
                   [nil, []],
                   [nil, []],
                   [nil, []],
                   [nil, []],
                   [nil, []],
                   nil, nil, "ItemRadio", nil, "ItemMap", "Binocular", nil, "ItemWatch", "ItemCompass"];
private _marksman = [[nil, [["FirstAidKit", 2]]],
                     [nil, []],
                     [nil, []],
                     [nil, []],
                     [nil, []],
                     [nil, []],
                     nil, nil, "ItemRadio", nil, "ItemMap", "Binocular", nil, "ItemWatch", "ItemCompass"];
private _repair = [[nil, [["FirstAidKit", 2]]],
                   [nil, []],
                   [nil, [["ToolKit", 1]]],
                   [nil, []],
                   [nil, []],
                   [nil, []],
                   nil, nil, "ItemRadio", nil, "ItemMap", "Binocular", nil, "ItemWatch", "ItemCompass"];
private _demo = [[nil, [["FirstAidKit", 2]]],
                 [nil, []],
                 [nil, [["ToolKit", 1]]],
                 [nil, []],
                 [nil, []],
                 [nil, []],
                 nil, nil, "ItemRadio", nil, "ItemMap", "Binocular", nil, "ItemWatch", "ItemCompass"];
private _engineer = [[nil, [["FirstAidKit", 2]]],
                     [nil, []],
                     [nil, [["ToolKit", 1]]],
                     [nil, []],
                     [nil, []],
                     [nil, []],
                     nil, nil, "ItemRadio", nil, "ItemMap", "Binocular", nil, "ItemWatch", "ItemCompass"];
private _grenadier = [[nil, [["FirstAidKit", 2]]],
                      [nil, []],
                      [nil, []],
                      [nil, []],
                      [nil, []],
                      [nil, []],
                      nil, nil, "ItemRadio", nil, "ItemMap", "Binocular", nil, "ItemWatch", "ItemCompass"];
private _rifleman = [[nil, [["FirstAidKit", 2]]],
                     [nil, []],
                     [nil, []],
                     [nil, []],
                     [nil, []],
                     [nil, []],
                     nil, nil, "ItemRadio", nil, "ItemMap", "Binocular", nil, "ItemWatch", "ItemCompass"];
private _jtac = [[nil, [["FirstAidKit", 2]]],
                 [nil, []],
                 [nil, []],
                 [nil, []],
                 [nil, []],
                 [nil, []],
                 nil, nil, "ItemRadio", nil, "ItemMap", "Binocular", nil, "ItemWatch", "ItemCompass"];
private _hPilot = [[nil, [["FirstAidKit", 2]]],
                   [nil, []],
                   [nil, []],
                   [nil, []],
                   [nil, []],
                   [nil, []],
                   nil, nil, "ItemRadio", nil, "ItemMap", "Binocular", nil, "ItemWatch", "ItemCompass"];
private _jPilot = [[nil, [["FirstAidKit", 2]]],
                   [nil, []],
                   ["B_Parachute", []],
                   [nil, []],
                   [nil, []],
                   [nil, []],
                   nil, nil, "ItemRadio", nil, "ItemMap", "Binocular", nil, "ItemWatch", "ItemCompass"];
private _crew = [[nil, [["FirstAidKit", 2]]],
                 [nil, []],
                 [nil, [["ToolKit", 1]]],
                 [nil, []],
                 [nil, []],
                 [nil, []],
                 nil, nil, "ItemRadio", nil, "ItemMap", "Binocular", nil, "ItemWatch", "ItemCompass"];
private _mortar = [[nil, [["FirstAidKit", 2]]],
                   [nil, []],
                   [nil, []],
                   [nil, []],
                   [nil, []],
                   [nil, []],
                   nil, nil, "ItemRadio", nil, "ItemMap", "Binocular", nil, "ItemWatch", "ItemCompass"];
private _uavOp = [[nil, [["FirstAidKit", 2]]],
                   [nil, []],
                   [nil, []],
                   [nil, []],
                   [nil, []],
                   [nil, []],
                   nil, nil, "ItemRadio", nil, "ItemMap", "Binocular", nil, "ItemWatch", "ItemCompass"];
private _spotter = [[nil, [["FirstAidKit", 2]]],
                    [nil, []],
                    [nil, []],
                    [nil, []],
                    [nil, []],
                    [nil, []],
                    nil, nil, "ItemRadio", nil, "ItemMap", "Binocular", nil, "ItemWatch", "ItemCompass"];

_out set [RL_UC, [_hq, _sl, _tl, _medic, _lmg, _hmg, _assHMG, _aa, _assAA, _at, _assAT, _sniper, _marksman,
                 _repair, _demo, _engineer, _grenadier, _rifleman, _jtac, _hPilot, _jPilot, _crew,
                 _mortar, _uavOp, _spotter]];

//------------------------------------------------------------ Base Atmosphere Role Unsung COMMON

BALO_UC = RL_UC + 1;

private _medic = [];
private _gear = [];
private _support = [];
private _default = [];

_out set [BALO_UC, [_medic, _gear, _support, _default]];

//------------------------------------------------------------ FINITO, return
          
_out