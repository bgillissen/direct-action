class unsung {
	title = "A required mod is missing.";
	description = "Unsung is not loaded. Please make sure that you load all the mods in the required list when you join this server from the launcher. The missing mod can be found here: http://www.armanam.eu/";
	picture = "";
};
