class uo {
	tag = "uo";
	class functions {
		class assets { file="feats\assets\unsung\OPFOR\assets.sqf"; };
		class getBackpacks { file="feats\assets\unsung\OPFOR\getBackpacks.sqf"; };
		class getItems { file="feats\assets\unsung\OPFOR\getItems.sqf"; };
		class getMagazines { file="feats\assets\unsung\OPFOR\getMagazines.sqf"; };
		class getWeapons { file="feats\assets\unsung\OPFOR\getWeapons.sqf"; };
	};
};
