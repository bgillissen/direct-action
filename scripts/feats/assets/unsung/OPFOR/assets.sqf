/*
@filename: feats\assets\unsung\OPFOR\assets.sqf
Author:
	Ben
Description:
	call by feats\assets\unsung\init.sqf
	return the Unsung OPFOR assets
*/

private _out = [];

//------------------------------------------------------------ Arsenal Unsung OPFOR

A_UO = 0;

private _backpacks = [];
private _items = (call ub_fnc_getItems);
private _weapons = (call ub_fnc_getWeapons);
private _ammo = (call ub_fnc_getMagazines);

_out set [A_UO, [_backpacks, _items, _weapons, _ammo]];

//------------------------------------------------------------ Restricted Gear Unsung OPFOR

RG_UO = A_UO + 1;

private _launcher = [];
private _mg = [];
private _sRifle = [];
private _mRifle = [];
private _sScope = [];
private _mScope = [];
private _oScope = [];
private _mbrItem = [];
private _backpack = [];

_out set [RG_UO, [_launcher, _mg, _sRifle, _mRifle, _sScope, _mScope, _oScope, _mbrItem, _backpack]];

//------------------------------------------------------------ Allowed Vehicles Unsung OPFOR

AV_UO = RG_UO + 1;

private _heli = [];
private _plane = [];
private _tank = [];

_out set [AV_UO, [_heli, _plane, _tank]];

//------------------------------------------------------------ supplyDrop Unsung OPFOR

SD_UO = AV_UO + 1;

private _backpacks = [];
private _items = [];
private _weapons = [];
private _ammo = [];
private _crates = [];
private _vehicles = [];

_out set [SD_UO, [_backpacks, _items, _weapons, _ammo, _crates, _vehicles]];

//------------------------------------------------------------ Rewards Unsung OPFOR

R_UO = SD_UO + 1;

private _rewards = [];

_out set [R_UO, _rewards];

//------------------------------------------------------------ Spawn Unsung OPFOR

S_UO = R_UO + 1;

private _rt = [];
private _crates = [];
private _pGroups = [];
private _sGroups = [];
private _pilot = [];
private _crew = ["uns_men_NVA_crew_driver"];
private _officer = ["uns_men_NVA_65_off", "uns_men_NVA_68_Roff"];
private _garrison = ["uns_men_NVA_65_RF2", "uns_men_NVA_65_AS1", "uns_men_NVA_65_AS7"];
private _aa = ["uns_BTR152_ZPU", "uns_Type55_ZU"];
private _arti = [];
private _static = ["uns_dshk_high_VC", "uns_dshk_low_VC", "uns_dshk_armoured_VC", "uns_dshk_wheeled_VC", "uns_dshk_twin_VC", "uns_dshk_bunker_open_VC", "uns_dshk_bunker_closed_VC",
				   "uns_dshk_twin_bunker_open_VC", "uns_dshk_twin_bunker_closed_VC", "uns_pk_low_VC", "uns_pk_high_VC", "uns_pk_bunker_low_VC", "uns_pk_bunker_open_VC", "uns_pk_bunker_closed_VC",
				   "uns_pk_tower_VC", "uns_mg42_low_VC", "uns_M40_106mm_VC", "uns_SPG9_73mm_VC", "uns_Type36_57mm_VC", "uns_m1941_82mm_mortarNVA", "uns_m1941_82mm_mortarNVA_arty", 
                   "uns_m1941_82mm_mortarVC", "uns_D20_artillery","uns_D30_artillery", "uns_ZPU4_VC", "uns_ZPU4_NVA", "uns_ZU23_VC", "uns_ZU23_NVA", "uns_S60_VC", "uns_S60_NVA", "uns_Type74_VC", "uns_Type74_NVA"];
private _cas = [];
private _tank = [];
private _apc = ["uns_Type55_RR57", "uns_Type55_RR73", "uns_Type55_M40", "uns_Type55_mortar", "uns_BTR152_DSHK"];
private _car = [];
private _carArmed = [];
private _aPatrol = [];
private _civ = [];

_out set [S_UO, [_rt, _crates, _pGroups, _sGroups, _pilot, _crew, _officer, _garrison, _civ,
                _aa, _arti, _static, _cas, _tank, _apc, _car, _carArmed, _aPatrol]];

//------------------------------------------------------------ Vehicles Unsung OPFOR

BV_UO = S_UO + 1;

private _car = [];
private _carArmed = [];
private _apc = [];
private _tank = [];
private _aaTank = ["uns_BTR152_ZPU", "uns_Type55_ZU"];
private _planeCAS = [];
private _planeAA = [];
private _planeTransport = [];
private _uav = [];
private _heliSmall = [];
private _heliSmallArmed = [];
private _heliMedium = [];
private _heliMedEvac = [];
private _heliBig = [];
private _heliAttack = [];
private _boatSmall = [];
private _boatAttack = [];
private _boatBig = [];
private _sub = [];
private _landMedic = [];
private _repair = [];
private _fuel = [];
private _ammo = [];
private _truck = [];
private _quad = [];
private _artiTank = [];
private _artiCannon = [];
private _artiTube = [];

_out set [BV_UO, [_car, _carArmed, _apc, _tank, _aaTank, _planeCAS, _planeAA, _planeTransport, _uav, 
                _heliSmall, _heliSmallArmed, _heliMedium, _heliMedEvac, _heliBig, _heliAttack, 
                _boatSmall, _boatAttack, _boatBig, _sub, _landMedic, _repair, _fuel, _ammo, _truck, _quad,
                 _artiTank, _artiCannon, _artiTube]];

//------------------------------------------------------------ Vehicles Cargo Unsung OPFOR

VC_UO = BV_UO + 1;

private _car = [[],[],[],[]];
private _carArmed = [[],[],[],[]];
private _apc = [[],[],[],[]];
private _tank = [[],[],[],[]];
private _aaTank = [[],[],[],[]];
private _planeCAS = [[],[],[],[]];
private _planeAA = [[],[],[],[]];
private _planeTransport = [[],[],[],[]];
private _uav = [[],[],[],[]];
private _heliSmall = [[],[],[],[]];
private _heliSmallArmed = [[],[],[],[]];
private _heliMedium = [[],[],[],[]];
private _heliMedEvac = [[],[],[],[]];
private _heliBig = [[],[],[],[]];
private _heliAttack = [[],[],[],[]];
private _boatSmall = [[],[],[],[]];
private _boatAttack = [[],[],[],[]];
private _boatBig = [[],[],[],[]];
private _sub = [[],[],[],[]];
private _landMedic = [[],[],[],[]];
private _repair = [[],[],[],[]];
private _fuel = [[],[],[],[]];
private _ammo = [[],[],[],[]];
private _truck = [[],[],[],[]];
private _quad = [[],[],[],[]];
private _artiTank = [[],[],[],[]];
private _artiCannon = [[],[],[],[]];
private _artiTube = [[],[],[],[]];

_out set [VC_UO, [_car, _carArmed, _apc, _tank, _aaTank, _planeCAS, _planeAA, _planeTransport, _uav, 
                 _heliSmall, _heliSmallArmed, _heliMedium, _heliMedEvac, _heliBig, _heliAttack, 
                 _boatSmall, _boatAttack, _boatBig, _sub, _landMedic, _repair, _fuel, _ammo, _truck, _quad,
                 _artiTank, _artiCannon, _artiTube]];

//------------------------------------------------------------ Role Loadout Unsung OPFOR
/*
 [uniform, [inUniform]], 
 [vest, inVest]], 
 [backpack, [inBackpack]], 
 [primWeapon, [muzzle, bipod, acc, scope, ammo2, ammo]] 
 [secWeapon, [muzzle, bipod, acc, scope, ammo]], 
 [handWeapon, [muzzle, bipod, acc, scope, ammo]], 
 helmet, face, comm, terminal, map, bino, nv, watch, compass
*/

RL_UO = VC_UO + 1;

private _hq = [];
private _sl = [];
private _tl = [];
private _medic = [];
private _lmg = [];
private _hmg = [];
private _assHMG = [];
private _aa = [];
private _assAA = [];
private _at = [];
private _assAT = [];
private _sniper = [];
private _marksman = [];
private _repair = [];
private _demo = [];
private _engineer = [];
private _grenadier = [];
private _rifleman = [];
private _jtac = [];
private _hpilot = [];
private _jPilot = [];
private _crew = [];
private _mortar = [];
private _uavOp = [[nil, []],
                  [nil, []],
                  [nil, []],
                  [nil, []],
                  [nil, []],
                  [nil, []],
                  nil, nil, nil, nil, nil, nil, nil, nil, nil];
private _spotter = [];

_out set [RL_UO, [_hq, _sl, _tl, _medic, _lmg, _hmg, _assHMG, _aa, _assAA, _at, _assAT, _sniper, _marksman,
                 _repair, _demo, _engineer, _grenadier, _rifleman, _jtac, _hPilot, _jPilot, _crew,
                 _mortar, _uavOp, _spotter]];

//------------------------------------------------------------ Base Atmosphere Role Unsung OPFOR

BALO_UO = RL_UO + 1;

private _medic = [];
private _gear = [];
private _support = [];
private _default = [];

_out set [BALO_UO, [_medic, _gear, _support, _default]];

//------------------------------------------------------------ FINITO, return
          
_out