class spawn_unsung {
	title = "Add Unsung units to enemy units spawn pool";
	values[] = {0,1};
	texts[] = {"no", "yes"};
	default = 1;
};
class gear_unsung {
	title = "Which Unsung gear is available in arsenal";
	values[] = {0,1,2,3};
	texts[] = {"none", "player side only", "player side + allies", "all"};
	default = 1;
};
class reward_unsung {
	title = "Add Unsung vehicles to reward pool";
	values[] = {0,1,2,3};
	texts[] = {"no", "player side only", "player side + allies", "all"};
	default = 2;
};
class bv_unsung {
	title = "Add Unsung vehicle to base vehicle pool";
	values[] = {0,1,2,3};
	texts[] = {"no", "player side only", "player side + allies", "all"};
	default = 2;
};
class rl_unsung {
	title = "Players spawn with Unsung loadout";
	values[] = {0,1};
	texts[] = {"no", "yes"};
	default = 1;
};
