class unsung {
	tag = "unsung";
	class functions {
		class init { file="feats\assets\unsung\init.sqf"; };
		class postInit { file="core\dummy.sqf"; };
		class preInit { file="feats\assets\unsung\preInit.sqf"; };
	};
};

#include "BLUFOR\_cfgFunctions.hpp"
#include "COMMON\_cfgFunctions.hpp"
#include "OPFOR\_cfgFunctions.hpp"
