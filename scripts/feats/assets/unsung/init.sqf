/*
@filename: feats\assets\unsung\init.sqf
Author:
	Ben
Description:
	run on server,
	implent Unsung assets
*/
["unsung", [["UC", (call uc_fnc_assets), [0,1,2]],
			["UB", (call ub_fnc_assets), [1]],
			["UO", (call uo_fnc_assets), [0]]]] call assets_fnc_implent;