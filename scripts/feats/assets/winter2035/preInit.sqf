/*
@filename: feats\assets\winter2035\preInit.sqf
Author:
	Ben
Description:
	run on server,
	update MAP_KEYWORDS
*/

if ( (toUpper worldName) isEqualTo "ALTIS" ) then {
    MAP_KEYWORDS = MAP_KEYWORDS - ["dry"]; 
    MAP_KEYWORDS pushback "winter";
};