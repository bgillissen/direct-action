/*
@filename: feats\assets\ace\assets.sqf
Author:
	Ben
Description:
	run on server
	call by feats\assets\ace\init.sqf
	return ACE assets
*/

private _out = [];

//------------------------------------------------------------ Arsenal ACE

A_ACE = 0;

private _backpacks = ["ACE_gunbag", 
					  "ACE_gunbag_Tan",
                      "ACE_NonSteerableParachute",
                      "ACE_ReserveParachute", 
                      "ACE_TacticalLadder_Pack"];
private _items = ["acc_pointer_IR",
				  "ACE_acc_pointer_green_IR",
                  "ACE_acc_pointer_green",
                  "ACE_acc_pointer_red",
                  "ACE_Altimeter",
                  "ACE_ATragMX",
                  "ACE_atropine",
                  "ACE_Banana",
                  "ACE_bloodIV",
                  "ACE_bloodIV_250",
                  "ACE_bloodIV_500",
                  "ACE_bodyBag",
                  "ACE_bodyBagObject",
                  "ACE_CableTie",
                  "ACE_Cellphone",
                  "ACE_Clacker",
                  "ACE_DAGR",
                  "ACE_DeadManSwitch",
                  "ACE_DefusalKit",
                  "ACE_EarPlugs",
                  "ACE_elasticBandage",
                  "ACE_EntrenchingTool",
                  "ACE_epinephrine",
                  "ACE_fieldDressing",
                  "ACE_Flashlight_MX991",
				  "ACE_Flashlight_KSF1",
				  "ACE_Flashlight_XL50",
                  "ACE_HuntIR_monitor",
                  "ACE_IR_Strobe_Item",
                  "ACE_Kestrel4500",
                  "ACE_M26_Clacker",
                  "ACE_MapTools",
                  "ACE_microDAGR",
                  "ACE_morphine",
                  "ACE_MX2A",
                  "ACE_NVG_Gen1",
                  "ACE_NVG_Gen2",
                  "ACE_NVG_Gen3",
                  "ACE_NVG_Gen4",
                  "ACE_NVG_Wide",
                  "ACE_optic_Arco_2D",
                  "ACE_optic_Arco_PIP",
                  "ACE_optic_Hamr_2D",
                  "ACE_optic_Hamr_PIP",
                  "ACE_optic_LRPS_2D",
                  "ACE_optic_LRPS_PIP",
                  "ACE_optic_SOS_2D",
                  "ACE_optic_SOS_PIP",
                  "ACE_optic_MRCO_2D",
                  "ACE_optic_MRCO_PIP",
                  "ACE_packingBandage",
                  "ACE_personalAidKit",
                  "ACE_plasmaIV",
                  "ACE_plasmaIV_250",
                  "ACE_plasmaIV_500",
                  "ACE_quikclot",
                  "ACE_RangeCard",
                  "ACE_RangeTable_82mm",
                  "ACE_salineIV",
                  "ACE_salineIV_250",
                  "ACE_salineIV_500",
                  "ACE_SpareBarrel",
                  "ACE_SpottingScope",
                  /*"ACE_SpraypaintBlack",
				  "ACE_SpraypaintBlue",
				  "ACE_SpraypaintGreen",
				  "ACE_SpraypaintRed",*/ //commented to prevent abusive tags in base
                  "ACE_surgicalKit",
                  "ACE_tourniquet",
                  "ACE_Tripod",
                  "ACE_UAVBattery",
                  "ACE_Vector",
                  "ACE_VectorDay",
                  "ACE_Yardage450",
                  "ACE_wirecutter"];
private _weapons = ["ACE_VMH3", 
					"ACE_VMM3", 
                    "ACE_MX2A",
                    "ACE_Vector",
                    "ACE_VectorDay", 
                    "ACE_Yardage450"];
private _ammo = ["ACE_Chemlight_HiOrange",
				 "ACE_Chemlight_HiRed",
				 "ACE_Chemlight_HiYellow",
				 "ACE_Chemlight_HiWhite",
				 "ACE_Chemlight_Orange",
				 "ACE_Chemlight_White",
				 "ACE_Chemlight_IR",
				 "ACE_HandFlare_Green",
                 "ACE_HandFlare_Red",
                 "ACE_HandFlare_White",
                 "ACE_HandFlare_Yellow",
                 "ACE_HuntIR_M203",
                 "ACE_M14",
                 "ACE_M84",
                 "IEDUrbanSmall_Remote_Mag",
                 "IEDLandSmall_Remote_Mag",
                 "IEDUrbanBig_Remote_Mag",
                 "IEDLandBig_Remote_Mag",
                 "ACE_100Rnd_65x39_caseless_mag_Tracer_Dim",
				 "ACE_200Rnd_65x39_cased_Box_Tracer_Dim",
				 "ACE_30Rnd_65x39_caseless_mag_Tracer_Dim",
				 "ACE_30Rnd_65x39_caseless_green_mag_Tracer_Dim",
				 "ACE_30Rnd_556x45_Stanag_M995_AP_mag",
				 "ACE_30Rnd_556x45_Stanag_Mk262_mag",
				 "ACE_30Rnd_556x45_Stanag_Mk318_mag",
				 "ACE_30Rnd_556x45_Stanag_Tracer_Dim",
				 "ACE_20Rnd_762x51_Mag_Tracer",
				 "ACE_20Rnd_762x51_Mag_Tracer_Dim",
				 "ACE_20Rnd_762x51_Mag_SD",
				 "ACE_10Rnd_762x51_M118LR_Mag",
				 "ACE_10Rnd_762x51_Mk316_Mod_0_Mag",
				 "ACE_10Rnd_762x51_Mk319_Mod_0_Mag",
				 "ACE_10Rnd_762x51_M993_AP_Mag",
				 "ACE_20Rnd_762x51_M118LR_Mag",
				 "ACE_20Rnd_762x51_Mk316_Mod_0_Mag",
				 "ACE_20Rnd_762x51_Mk319_Mod_0_Mag",
				 "ACE_20Rnd_762x51_M993_AP_Mag",
				 "ACE_20Rnd_762x67_Mk248_Mod_0_Mag",
				 "ACE_20Rnd_762x67_Mk248_Mod_1_Mag",
				 "ACE_20Rnd_762x67_Berger_Hybrid_OTM_Mag",
				 "ACE_30Rnd_65x47_Scenar_mag",
				 "ACE_30Rnd_65_Creedmor_mag",
				 "ACE_10Rnd_338_300gr_HPBT_Mag",
				 "ACE_10Rnd_338_API526_Mag",
				 "ACE_5Rnd_127x99_Mag",
				 "ACE_5Rnd_127x99_API_Mag",
				 "ACE_5Rnd_127x99_AMAX_Mag",
				 "ACE_30Rnd_9x19_mag",
				 "ACE_16Rnd_9x19_mag",
				 "ACE_10Rnd_762x54_Tracer_mag"];

_out set [A_ACE, [_backpacks, _items, _weapons, _ammo]];

//------------------------------------------------------------ supplyDrop ACE

SD_ACE = A_ACE + 1;

private _backpacks = [];
private _items = [["ACE_M26_Clacker", 2], 
                  ["ACE_epinephrine", 20], 
                  ["ACE_morphine", 40],
                  ["ACE_packingBandage", 40],
                  ["ACE_fieldDressing", 40],
                  ["ACE_elasticBandage", 40],
                  ["ACE_quikclot", 40],
                  ["ACE_bloodIV_500", 20],
                  ["ACE_salineIV_500", 20],
                  ["ACE_tourniquet", 8],
                  ["ACE_bodyBag", 5],
                  ["ACE_EarPlugs", 10],
                  ["ACE_surgicalKit", 15],
                  ["ACE_personalAidKit", 15],
                  ["ACE_IR_Strobe_Item", 10]];
private _weapons = [["ACE_VMH3", 2]];
private _ammo = [];

_out set [SD_ACE, [_backpacks, _items, _weapons, _ammo, [], []]];

//------------------------------------------------------------ Vehicles Cargo ACE

VC_ACE = SD_ACE + 1;

private _car = [[],[],[],[]];
private _carArmed = [[],[],[],[]];
private _apc = [[],[],[],[]];
private _tank = [[],[],[],[]];
private _aaTank = [[],[],[],[]];
private _planeCAS = [[],[],[],[]];
private _planeAA = [[],[],[],[]];
private _planeTransport = [[],[],[],[]];
private _uav = [[],[],[],[]];
private _heliSmall = [[],[],[],[]];
private _heliSmallArmed = [[],[],[],[]];
private _heliMedium = [[],[],[],[]];
private _heliMedEvac = [[],[],[],[]];
private _heliBig = [[],[],[],[]];
private _heliAttack = [[],[],[],[]];
private _boatSmall = [[],[],[],[]];
private _boatAttack = [[],[],[],[]];
private _boatBig = [[],[],[],[]];
private _sub = [[],[],[],[]];
private _landMedic = [[],[],[],[]];
private _repair = [[],[],[],[]];
private _fuel = [[],[],[],[]];
private _ammo = [[],[],[],[]];
private _truck = [[],[],[],[]];
private _quad = [[],[],[],[]];
private _artiTank = [[],[],[],[]];
private _artiCannon = [[],[],[],[]];
private _artiTube = [[],[],[],[]];

_out set [VC_ACE, [_car, _carArmed, _apc, _tank, _aaTank, _planeCAS, _planeAA, _planeTransport, _uav, 
                   _heliSmall, _heliSmallArmed, _heliMedium, _heliMedEvac, _heliBig, _heliAttack, 
                   _boatSmall, _boatAttack, _boatBig, _sub, _landMedic, _repair, _fuel, _ammo, _truck, _quad,
                   _artiTank, _artiCannon, _artiTube]];

//------------------------------------------------------------ Role Loadout ACE

RL_ACE = VC_ACE + 1;

/*
 [uniform, [inUniform]],
 [vest, [inVest]],
 [backpack, [inBackpack]],
 [primWeapon, [primWeaponItems]]
 [secWeapon, [secWeapItems]],
 [handWeapon, [handWeapItems]],
 helmet, face, comm, terminal, map, bino, nv, watch, compass
*/

private _hq = [["", [["ACE_EarPlugs", 1], ["ACE_IR_Strobe_Item", 1], ["ACE_bodyBag", 1], ["ACE_microDAGR", 1]]],
               ["", [["ACE_Flashlight_XL50", 1]]],
               ["", []],
               ["", []],
               ["", []],
               ["", []],
               "", "", "", "", "", "ACE_Vector", "", "", ""];
private _sl = [["", [["ACE_EarPlugs", 1], ["ACE_IR_Strobe_Item", 1], ["ACE_bodyBag", 1], ["ACE_microDAGR", 1]]],
               ["", [["ACE_morphine", 4], ["ACE_fieldDressing", 5], ["ACE_elasticBandage", 5], ["ACE_quikclot", 5], ["ACE_tourniquet", 4], ["ACE_Flashlight_XL50", 1]]],
               ["", []],
               ["", []],
               ["", []],
               ["", []],
               "", "", "", "", "", "ACE_Vector", "", "", ""];
private _tl = [["", [["ACE_EarPlugs", 1], ["ACE_IR_Strobe_Item", 1], ["ACE_bodyBag", 1], ["ACE_microDAGR", 1]]],
               ["", [["ACE_morphine", 4], ["ACE_fieldDressing", 5], ["ACE_elasticBandage", 5], ["ACE_quikclot", 5], ["ACE_tourniquet", 4], ["ACE_Flashlight_XL50", 1]]],
               ["", []],
               ["", []],
               ["", []],
               ["", []],
               "", "", "", "", "", "ACE_Vector", "", "", ""];
private _medic = [["", [["ACE_EarPlugs", 1], ["ACE_IR_Strobe_Item", 1], ["ACE_bodyBag", 1], ["ACE_microDAGR", 1]]],
                  ["", [["ACE_EntrenchingTool", 1], ["ACE_Flashlight_XL50", 1]]],
                  ["", [["ACE_morphine", 15], ["ACE_salineIV_500", 5], ["ACE_epinephrine", 10], ["ACE_elasticBandage", 10], ["ACE_quikclot", 10], ["ACE_packingBandage", 10], ["ACE_tourniquet", 4], ["ACE_surgicalKit", 1], ["ACE_personalAidKit", 8]]],
                  ["", []],
                  ["", []],
                  ["", []],
                  "", "", "", "", "", "", "", "", ""];
private _lmg = [["", [["ACE_EarPlugs", 1], ["ACE_IR_Strobe_Item", 1], ["ACE_bodyBag", 1], ["ACE_microDAGR", 1]]],
                ["", [["ACE_morphine", 4], ["ACE_fieldDressing", 5], ["ACE_elasticBandage", 5], ["ACE_quikclot", 5], ["ACE_tourniquet", 4], ["ACE_Flashlight_XL50", 1]]],
                ["", []],
                ["", []],
                ["", []],
                ["", []],
                "", "", "", "", "", "", "", "", ""];
private _hmg = [["", [["ACE_EarPlugs", 1], ["ACE_IR_Strobe_Item", 1], ["ACE_bodyBag", 1], ["ACE_microDAGR", 1]]],
                ["", [["ACE_morphine", 4], ["ACE_fieldDressing", 5], ["ACE_elasticBandage", 5], ["ACE_quikclot", 5], ["ACE_tourniquet", 4], ["ACE_Flashlight_XL50", 1]]],
                ["", []],
                ["", []],
                ["", []],
                ["", []],
                "", "", "", "", "", "", "", "", ""];
private _assHMG = [["", [["ACE_EarPlugs", 1], ["ACE_IR_Strobe_Item", 1], ["ACE_bodyBag", 1], ["ACE_microDAGR", 1]]],
                   ["", [["ACE_morphine", 4], ["ACE_fieldDressing", 5], ["ACE_elasticBandage", 5], ["ACE_quikclot", 5], ["ACE_tourniquet", 4], ["ACE_Flashlight_XL50", 1]]],
                   ["", []],
                   ["", []],
                   ["", []],
                   ["", []],
                   "", "", "", "", "", "", "", "", ""];
private _aa = [["", [["ACE_EarPlugs", 1], ["ACE_IR_Strobe_Item", 1], ["ACE_bodyBag", 1], ["ACE_microDAGR", 1]]],
               ["", [["ACE_morphine", 4], ["ACE_fieldDressing", 5], ["ACE_elasticBandage", 5], ["ACE_quikclot", 5], ["ACE_tourniquet", 4], ["ACE_Flashlight_XL50", 1]]],
               ["", []],
               ["", []],
               ["", []],
               ["", []],
               "", "", "", "", "", "", "", "", ""];
private _assAA = [["", [["ACE_EarPlugs", 1], ["ACE_IR_Strobe_Item", 1], ["ACE_bodyBag", 1], ["ACE_microDAGR", 1]]],
                  ["", [["ACE_morphine", 4], ["ACE_fieldDressing", 5], ["ACE_elasticBandage", 5], ["ACE_quikclot", 5], ["ACE_tourniquet", 4], ["ACE_Flashlight_XL50", 1]]],
                  ["", []],
                  ["", []],
                  ["", []],
                  ["", []],
                  "", "", "", "", "", "", "", "", ""];
private _at = [["", [["ACE_EarPlugs", 1], ["ACE_IR_Strobe_Item", 1], ["ACE_bodyBag", 1], ["ACE_microDAGR", 1]]],
               ["", [["ACE_morphine", 4], ["ACE_fieldDressing", 5], ["ACE_elasticBandage", 5], ["ACE_quikclot", 5], ["ACE_tourniquet", 4], ["ACE_Flashlight_XL50", 1]]],
               ["", []],
               ["", []],
               ["", []],
               ["", []],
               "", "", "", "", "", "", "", "", ""];
private _assAT = [["", [["ACE_EarPlugs", 1], ["ACE_IR_Strobe_Item", 1], ["ACE_bodyBag", 1], ["ACE_microDAGR", 1]]],
                  ["", [["ACE_morphine", 4], ["ACE_fieldDressing", 5], ["ACE_elasticBandage", 5], ["ACE_quikclot", 5], ["ACE_tourniquet", 4], ["ACE_Flashlight_XL50", 1]]],
                  ["", []],
                  ["", []],
                  ["", []],
                  ["", []],
                  "", "", "", "", "", "", "", "", ""];
private _sniper = [["", [["ACE_EarPlugs", 1], ["ACE_IR_Strobe_Item", 1], ["ACE_bodyBag", 1], ["ACE_microDAGR", 1]]],
                   ["", [["ACE_morphine", 4], ["ACE_fieldDressing", 5], ["ACE_elasticBandage", 5], ["ACE_quikclot", 5], ["ACE_tourniquet", 4], ["ACE_Flashlight_XL50", 1]]],
                   ["", []],
                   ["", []],
                   ["", []],
                   ["", []],
                   "", "", "", "", "", "ACE_Vector", "", "", ""];
private _marksman = [["", [["ACE_EarPlugs", 1], ["ACE_IR_Strobe_Item", 1], ["ACE_bodyBag", 1], ["ACE_microDAGR", 1]]],
                     ["", [["ACE_morphine", 4], ["ACE_fieldDressing", 5], ["ACE_elasticBandage", 5], ["ACE_quikclot", 5], ["ACE_tourniquet", 4], ["ACE_Flashlight_XL50", 1]]],
                     ["", []],
                     ["", []],
                     ["", []],
                     ["", []],
                     "", "", "", "", "", "ACE_Vector", "", "", ""];
private _repair = [["", [["ACE_EarPlugs", 1], ["ACE_IR_Strobe_Item", 1], ["ACE_bodyBag", 1], ["ACE_microDAGR", 1]]],
                   ["", [["ACE_morphine", 4], ["ACE_fieldDressing", 5], ["ACE_elasticBandage", 5], ["ACE_quikclot", 5], ["ACE_tourniquet", 4], ["ACE_Flashlight_XL50", 1]]],
                   ["", []],
                   ["", []],
                   ["", []],
                   ["", []],
                   "", "", "", "", "", "", "", "", ""];
private _demo = [["", [["ACE_EarPlugs", 1], ["ACE_IR_Strobe_Item", 1], ["ACE_bodyBag", 1], ["ACE_microDAGR", 1]]],
                 ["", [["ACE_morphine", 4], ["ACE_fieldDressing", 5], ["ACE_elasticBandage", 5], ["ACE_quikclot", 5], ["ACE_tourniquet", 4], ["ACE_Flashlight_XL50", 1]]],
                 ["", []],
                 ["", []],
                 ["", []],
                 ["", []],
                 "", "", "", "", "", "", "", "", ""];
private _engineer = [["", [["ACE_EarPlugs", 1], ["ACE_IR_Strobe_Item", 1], ["ACE_bodyBag", 1], ["ACE_microDAGR", 1]]],
                     ["", [["ACE_morphine", 4], ["ACE_fieldDressing", 5], ["ACE_elasticBandage", 5], ["ACE_quikclot", 5], ["ACE_tourniquet", 4], ["ACE_Flashlight_XL50", 1]]],
                     ["", []],
                     ["", []],
                     ["", []],
                     ["", []],
                     "", "", "", "", "", "", "", "", ""];
private _grenadier = [["", [["ACE_EarPlugs", 1], ["ACE_IR_Strobe_Item", 1], ["ACE_bodyBag", 1], ["ACE_microDAGR", 1]]],
                      ["", [["ACE_morphine", 4], ["ACE_fieldDressing", 5], ["ACE_elasticBandage", 5], ["ACE_quikclot", 5], ["ACE_tourniquet", 4], ["ACE_Flashlight_XL50", 1]]],
                      ["", []],
                      ["", []],
                      ["", []],
                      ["", []],
                      "", "", "", "", "", "", "", "", ""];
private _rifleman = [["", [["ACE_EarPlugs", 1], ["ACE_IR_Strobe_Item", 1], ["ACE_bodyBag", 1], ["ACE_microDAGR", 1]]],
                     ["", [["ACE_morphine", 4], ["ACE_fieldDressing", 5], ["ACE_elasticBandage", 5], ["ACE_quikclot", 5], ["ACE_tourniquet", 4], ["ACE_Flashlight_XL50", 1]]],
                     ["", []],
                     ["", []],
                     ["", []],
                     ["", []],
                     "", "", "", "", "", "", "", "", ""];
private _jtac = [["", [["ACE_EarPlugs", 1], ["ACE_IR_Strobe_Item", 1], ["ACE_bodyBag", 1], ["ACE_microDAGR", 1]]],
                 ["", [["ACE_morphine", 4], ["ACE_fieldDressing", 5], ["ACE_elasticBandage", 5], ["ACE_quikclot", 5], ["ACE_tourniquet", 4], ["ACE_Flashlight_XL50", 1]]],
                 ["", []],
                 ["", []],
                 ["", []],
                 ["", []],
                 "", "", "", "", "", "ACE_Vector", "", "", ""];
private _hPilot = [["", [["ACE_EarPlugs", 1], ["ACE_IR_Strobe_Item", 1], ["ACE_bodyBag", 1], ["ACE_microDAGR", 1]]],
                   ["", [["ACE_morphine", 2], ["ACE_fieldDressing", 3], ["ACE_elasticBandage", 3], ["ACE_quikclot", 3], ["ACE_tourniquet", 2], ["ACE_Flashlight_XL50", 1]]],
                   ["", []],
                   ["", []],
                   ["", []],
                   ["", []],
                   "", "", "", "", "", "", "", "", ""];
private _jPilot = [["", [["ACE_EarPlugs", 1], ["ACE_IR_Strobe_Item", 1], ["ACE_bodyBag", 1], ["ACE_microDAGR", 1], ["ACE_Flashlight_XL50", 1]]],
                   ["", []],
                   ["", []],
                   ["", []],
                   ["", []],
                   ["", []],
                   "", "", "", "", "", "", "", "", ""];
private _crew = [["", [["ACE_EarPlugs", 1], ["ACE_IR_Strobe_Item", 1], ["ACE_bodyBag", 1], ["ACE_microDAGR", 1]]],
                 ["", [["ACE_morphine", 4], ["ACE_fieldDressing", 5], ["ACE_elasticBandage", 5], ["ACE_quikclot", 5], ["ACE_tourniquet", 4], ["ACE_Flashlight_XL50", 1]]],
                 ["", []],
                 ["", []],
                 ["", []],
                 ["", []],
                 "", "", "", "", "", "", "", "", ""];
private _mortar = [["", [["ACE_EarPlugs", 1], ["ACE_IR_Strobe_Item", 1], ["ACE_bodyBag", 1], ["ACE_microDAGR", 1]]],
                   ["", [["ACE_morphine", 4], ["ACE_fieldDressing", 5], ["ACE_elasticBandage", 5], ["ACE_quikclot", 5], ["ACE_tourniquet", 4], ["ACE_Flashlight_XL50", 1]]],
                   ["", []],
                   ["", []],
                   ["", []],
                   ["", []],
                   "", "", "", "", "", "ACE_Vector", "", "", ""];
private _uavOp = [["", [["ACE_EarPlugs", 1], ["ACE_IR_Strobe_Item", 1], ["ACE_bodyBag", 1], ["ACE_microDAGR", 1]]],
                  ["", [["ACE_morphine", 4], ["ACE_fieldDressing", 5], ["ACE_elasticBandage", 5], ["ACE_quikclot", 5], ["ACE_tourniquet", 4], ["ACE_Flashlight_XL50", 1]]],
                  ["", []],
                  ["", []],
                  ["", []],
                  ["", []],
                  "", "", "", "", "", "", "", "", ""];
private _spotter = [["", [["ACE_EarPlugs", 1], ["ACE_IR_Strobe_Item", 1], ["ACE_bodyBag", 1], ["ACE_microDAGR", 1]]],
                    ["", [["ACE_morphine", 4], ["ACE_fieldDressing", 5], ["ACE_elasticBandage", 5], ["ACE_quikclot", 5], ["ACE_tourniquet", 4], ["ACE_Flashlight_XL50", 1]]],
                    ["", []],
                    ["", []],
                    ["", []],
                    ["", []],
                    "", "", "", "", "", "ACE_Vector", "", "", ""];


_out set [RL_ACE, [_hq, _sl, _tl, _medic, _lmg, _hmg, _assHMG, _aa, _assAA, _at, _assAT, _sniper, _marksman,
                   _repair, _demo, _engineer, _grenadier, _rifleman, _jtac, _hPilot, _jPilot, _crew, 
                   _mortar, _uavOp, _spotter]];

//------------------------------------------------------------ FINITO, return
_out