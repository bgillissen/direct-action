class ace {
	tag = "ace";
	class functions {
		class assets { file="feats\assets\ace\assets.sqf"; };
		class init { file="feats\assets\ace\init.sqf"; };
		class postInit { file="core\dummy.sqf"; };
		class preInit { file="feats\assets\ace\preInit.sqf"; };
	};
};
