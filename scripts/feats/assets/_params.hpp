/*
@filename: feats\assets\_params.hpp
Author:
	Ben
Description:
		included by feats\_params.hpp
		include individual mods parameters
*/

#include "bohemia\_params.hpp"
#include "rhs\_params.hpp"
#include "uk3cb\_params.hpp"
#include "rmSplinter\_params.hpp"
#include "shacktac\_params.hpp"
#include "tfar\_params.hpp"
#include "unsung\_params.hpp"
