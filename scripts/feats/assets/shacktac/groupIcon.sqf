
params ["_unit", ["_disableVehicleIcons", true] ];

private _veh = (vehicle _unit);
if !( _veh isEqualTo _unit ) exitWith {
    if ( (_veh isEqualTo (vehicle player)) && !(_unit isEqualTo player) ) exitwith { "" };
	getText (configFile >> "CfgVehicles" >> typeOf _veh >> "icon");
};

/*
//checking if the player is in a vehicle and if vehicle role icons are disabled
//we only want to change the role icon next to their name on the HUD
if (vehicle _unit != _unit && !_disableVehicleIcons) exitWith {
    //selecting this specific _unit from vehicle crew array and determining role
    //TODO: optimize the way the player's role is determined
    private _crewInfo = ((fullCrew (vehicle _unit)) select {_x select 0 isEqualTo _unit}) select 0;
    _crewInfo params ["", "_role", "_index", "_turretPath", "_isTurret"];
	if (_role == "cargo") exitWith {
		"a3\ui_f\data\igui\cfg\commandbar\imagecargo_ca.paa"
	};
    if (_role == "driver") exitWith
    {
        if (vehicle _unit isKindOf "Air") then {
            //no suitable icons for this so we are using a resized one
            "@stui\addons\grouphud\imagepilot_ca.paa"
        } else {
            "a3\ui_f\data\igui\cfg\commandbar\imagedriver_ca.paa"
        };
    };
    //FFV
    if (_role == "turret" && _isTurret) exitWith {
        "a3\ui_f\data\igui\cfg\simpletasks\types\rifle_ca.paa"
    };
    //gunners and sometimes copilots
    if (_role == "gunner" || (_role == "turret" && !_isTurret)) exitWith {
        "a3\ui_f\data\igui\cfg\commandbar\imagegunner_ca.paa"
    };
    if (_role == "commander") exitWith {
        "a3\ui_f\data\igui\cfg\commandbar\imagecommander_ca.paa"
    };
};
*/
if (leader(_unit) == _unit) exitWith {
    "a3\Ui_f\data\GUI\Cfg\Ranks\general_gs.paa"
};

if (_unit call STHud_IsMedic) exitWith {
    "\A3\ui_f\data\map\vehicleicons\iconManMedic_ca.paa";
};

private _prim = primaryWeapon(_unit);
private _ismg = player getVariable ("ismg_" + _prim);
if (isNil {_ismg}) then {
    _ismg = _prim call STHud_IsMG;
    player setVariable ["ismg_" + _prim, _ismg];
};

if (_ismg) exitWith { "\A3\ui_f\data\map\vehicleicons\iconManMG_ca.paa"; };

private _sec = secondaryWeapon(_unit);
private _isat = player getVariable ("isat_"+_sec);
if (isNil {_isat}) then {
    _isat = _sec call STHud_IsAT;
    player setVariable ["isat_"+_sec, _isat];
};
if (_isat) exitWith { "\A3\ui_f\data\map\vehicleicons\iconManAT_ca.paa"; };

STHud_BGIcon;