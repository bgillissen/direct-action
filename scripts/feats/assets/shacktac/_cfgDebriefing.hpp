class shacktac {
	title = "A required mod is missing.";
	description = "ShackTac User Interface is not loaded. Please make sure that you load all the mods in the required list when you join this server from the launcher. The missing mod can be found here: http://steamcommunity.com/sharedfiles/filedetails/?id=498740884";
	picture = "";
};
