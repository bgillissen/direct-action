
params ["_unit", "_distance"];

private "_colour";
if ( (_unit getVariable['agony', false]) || (_unit getVariable['ACE_isUnconscious', false]) ) then {
    _colour = MT_colorUnconcious;
} else {
    _colour = _unit call STHud_Colour_Text;
};

private _alpha = linearConversion [STGI_MaxDist, STGI_FadeEdge, _distance, STGI_AlphaClamp, 0, true];

[_colour select 0, _colour select 1, _colour select 2, _alpha];