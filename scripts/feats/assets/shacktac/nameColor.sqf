
private _side = side _this;

if ( _side isEqualTo east ) exitWith { MT_colorEast };
if ( _side isEqualTo west ) exitWith { MT_colorWest };
if ( _side isEqualTo independent ) exitWith { MT_colorInd };
if ( _side isEqualTo civilian ) exitWith { MT_colorCiv };

MT_colorDft