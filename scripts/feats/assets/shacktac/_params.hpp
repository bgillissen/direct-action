class shacktac_fade  {
	title = "Fade compass icons when out of sight";
	values[] = {0,1};
	texts[] = {"no", "yes"};
	default = 0;
};

class shacktac_bearing {
	title = "Always display bearing above the compass";
	values[] = {0,1};
	texts[] = {"no", "yes"};
	default = 1;
};

class shacktac_emptyGroup {
	title = "Display shacktacUI when alone in a group";
	values[] = {0,1};
	texts[] = {"no", "yes"};
	default = 1;
};

class shacktac_direction  {
	title = "Display directions even if no compass";
	values[] = {0,1};
	texts[] = {"no", "yes"};
	default = 1;
};
