/*
@filename: feats\assets\shacktac\postInit.sqf
Author:
	Ben
Description:
	run on player
	overwrite some functions for a better implentation
*/

STUI_Occlusion = [false, true] select (["shacktac_fade"] call core_fnc_getParam);
DA_STUIbearing = [false, true] select (["shacktac_bearing"] call core_fnc_getParam);
DA_STUIemptyGroup = [false, true] select (["shacktac_emptygroup"] call core_fnc_getParam);
DA_STUIdirection = [false, true] select (["shacktac_direction"] call core_fnc_getParam);

player setVariable ["STMF_Side", (str (side player)), true];
private _name = (player getVariable["MD_name", (name player)]);
player setVariable ["STMF_Name", _name, true];

STUI_RemoveDeadViaProximity = false;
STUI_assignedTeam = { _this getVariable ['activeColor', 'WHITE'] };
STUI_ALUT = ["WHITE", "RED", "GREEN", "BLUE", "YELLOW"];
STUI_HasAssignedTeam = { (_this call STUI_assignedTeam) != "WHITE" };

STGI_GetColour_FromTeams = compile preprocessFileLineNumbers("feats\assets\shacktac\iconColor.sqf");
STGI_Update = compile preprocessFileLineNumbers("feats\assets\shacktac\iconTag.sqf");

STNT_sideColour = compile preprocessFileLineNumbers("feats\assets\shacktac\nameColor.sqf");
STNT_Update = compile preprocessFileLineNumbers("feats\assets\shacktac\nameTag.sqf");

STHud_NoSquadBarMode = true;
STHud_GetName = compile preprocessFileLineNumbers("feats\assets\shacktac\getName.sqf");
STHud_Colour_Teams = compile preprocessFileLineNumbers("feats\assets\shacktac\groupColor.sqf");
STHud_DrawHUD = compile preprocessFileLineNumbers("feats\assets\shacktac\groupHUD.sqf");
STHud_Icon = compile preprocessFileLineNumbers("feats\assets\shacktac\groupIcon.sqf");
STHud_Namelist = compile preprocessFileLineNumbers("feats\assets\shacktac\groupList.sqf");

private _value = [true];
private _name = (player getVariable["MD_name", (name player)]);
private _role = (["mapTracker", "shortRoleDescriptions", (player getVariable ["role", "none"])] call core_fnc_getSetting);
private _nameDsp = format["[%2] %1", _name, _role];
_value set [1, _nameDsp];
_value set [2, ([(_value select 1), STHud_MaxNameLen] call STUI_TruncateText)];
player setVariable ["sth_name", _value, true];
