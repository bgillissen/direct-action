
params ["_unit", "_fullName"];

private _value = _unit getVariable ["sth_name", []];

if ((count(_value) isEqualTo 0) || {!(isPlayer(_unit) isEqualTo (_value select 0))}) then {
    _value set [0, isPlayer(_unit)];
    private _name = (_unit getVariable["MD_name", (name _unit)]);
    private _role = (["mapTracker", "shortRoleDescriptions", (_unit getVariable ["role", "none"])] call core_fnc_getSetting);
    private _nameDsp = format["[%2] %1", _name, _role];
    _value set [1, _nameDsp];
    _value set [2, ([(_value select 1), STHud_MaxNameLen] call STUI_TruncateText)];
    _unit setVariable ["sth_name", _value, false];
};

if (_fullName) then {
    _value select 1;
} else {
	_value select 2;
};