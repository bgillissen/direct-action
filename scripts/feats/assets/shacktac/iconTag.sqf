
#define CANVAS_BORDER 0.02

if (!STGI_Enabled) exitWith {};
if (!call STUI_Canvas_ShownHUD) exitWith {};

private _units = [];
{
    if !( isObjectHidden _x ) then { _units pushback _x; };
} forEach (units(player) - [player]);

if (_units isEqualTo []) exitWith {};

    params ["_canvas"];

// Adjust the maximum alpha based on day/night
STGI_AlphaClamp = STGI_NightAlpha + sunOrMoon * STGI_DayAlphaDelta;
STGI_ColourOrange set [3, STGI_AlphaClamp + 0.2];

private _isUsingTeams = call STUI_UsingTeams;
private _colourFunc = STGI_GetColour_FromTeams;

{
    private _unit = _x;

    // TODO: You sometimes get a nice visualisation of where units sit
    // if you omit the vehicle. However, this isn't smoothly interpolated
    // so judders around in MP.
    private _icon_pos = ASLtoAGL(visiblePositionASL(vehicle(_unit)));

    private _height_adjust = 0.2;
    if (vehicle(_unit) == _unit) then
    {
        _height_adjust = _height_adjust + (_unit selectionPosition "pelvis" select 2);
    }
    else
    {
        _height_adjust = _height_adjust + 0.7;
    };
    _icon_pos set [2, (_icon_pos select 2) + _height_adjust];

    private _screen_pos = worldToScreen(_icon_pos);

    if (count(_screen_pos) == 2) then
    {
        private _clip_left  = ( (_screen_pos select 0) <= (SafeZoneX + CANVAS_BORDER) );
        private _clip_right = ( (_screen_pos select 0) >= (SafeZoneX + SafeZoneW - CANVAS_BORDER) );
        private _clip_top = ( (_screen_pos select 1) <= (SafeZoneY + CANVAS_BORDER) );
        private _clip_bottom = ( (_screen_pos select 1) >= (SafeZoneY + SafeZoneH - CANVAS_BORDER) );
        private _clipped = ( _clip_left || _clip_right || _clip_top || _clip_bottom );

        _distance = _icon_pos distance vehicle(player);

        _colour = [_unit, _distance] call _colourFunc;

        private "_texture";
        if ( _clipped ) then {
            _texture = "\A3\ui_f\data\igui\cfg\cursors\outarrow_ca.paa";
        } else {
            _texture = "\A3\ui_f\data\igui\cfg\cursors\select_ca.paa";
        };
        
        private _angle = 0;

        switch (true) do
        {
        case _clip_left: {_screen_pos set [0, SafeZoneX + CANVAS_BORDER]};
        case _clip_right: {_screen_pos set [0, SafeZoneX + SafeZoneW - CANVAS_BORDER]; _angle = 180;};
        };
        switch (true) do
        {
        case _clip_top: {_screen_pos set [1, SafeZoneY + CANVAS_BORDER]; _angle = 90;};
        case _clip_bottom: {_screen_pos set [1, SafeZoneY + SafeZoneH - CANVAS_BORDER]; _angle = 270;};
        };

        if ( (player distance _x) < STGI_FadeEdge ) then
        {
            private _vis = [(vehicle _x), "VIEW"] checkVisibility [eyePos player,  AGLToASL (_x modelToWorldVisual (_x selectionPosition "Spine3"))];
            private _alphaPre = _colour select 3;

            private _fadetime = 1.5;
            private _fadeDegree = _x getVariable ["STUI_Occlude_Fade",0];

            _alphaDiff = STUI_DeltaT / _fadetime;
            _fadeState = _x getVariable ["STUI_Occlude_Fade",0];

            if (_fadeState > 1) then {_fadeState = 1};
            if (_fadeState < 0) then {_fadeState = 0};

            if (_vis == 0 && _alphaPre > 0) then
            {
                _curAlpha = _fadeState;
                _newAlpha = (_curAlpha - _alphaDiff )min _alphaPre;
                _x setVariable ["STUI_Occlude_Fade",_newAlpha];
                _colour set [3,_newAlpha];
            }
            else
            {
                _curAlpha = _fadeState;
                _newAlpha = (_curAlpha + _alphaDiff) min _alphaPre;
                _x setVariable ["STUI_Occlude_Fade",_newAlpha];
                _colour set [3,_newAlpha];
            };
        };
		
        [
            _screen_pos,
            _colour,
            _texture,
            _angle
        ] call STUI_drawIcon3d;

        if (leader(player) == _unit) then
        {
            [
                _screen_pos,
                _colour,
                "\A3\ui_f\data\igui\cfg\cursors\leader_ca.paa",
                0
            ] call STUI_drawIcon3d;
        };
    };
} foreach(_units);