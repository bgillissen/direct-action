params ["_canvas"];

private _units = [];
{
    if !( isObjectHidden _x ) then { _units pushback _x; };
} forEach (units(player) - [player]);

// Don't show HUD if in group by self and not in vehicle
if ( !DA_STUIemptyGroup && (vehicle(player) == player) && {count(_units) isEqualTo 0}) exitWith {};

_canvas call STHud_Background;

// Can't use eyeDirection as it jitters when in some vehicles
(positionCameraToWorld [0,0,1] vectorDiff positionCameraToWorld [0,0,0]) params ["_lx", "_ly"];
private _lookDir = _lx atan2 _ly;
private _lookPos = getPosATLVisual(vehicle(player));
private _colourFunc = STHud_Colour_Teams;
private _playerColour = call STHud_Colour_Teams_Player;

STUI_DeltaT = time - STUI_LastTick;

{
    private _unit = _x;
    private _vehicle = vehicle(_unit);
    private _pos = getPosATLVisual(_vehicle);
    private _distance = _pos distance2d _lookPos;
    (_pos vectorDiff _lookPos) params ["_bx", "_by"];
    private _bearing = (_bx atan2 _by) - _lookDir;
    private _mapPos = STHud_Mid vectorAdd [sin(_bearing) * _distance * STHud_Scale, cos(_bearing) * _distance * STHud_Scale, 0];
    private _dir = getDirVisual(_vehicle) - _lookDir;

    private _icon = _unit call STHud_Icon;
    if !( _icon isEqualTo "" ) then { 
	    private _colour = [_unit, _distance] call _colourFunc;
	    private _size = 17;//19.2;
	    if ( _vehicle != _unit ) then {
	        if !( _veh isEqualTo (vehicle player) ) then {
	            _size = _size * 0.60;
	        /*} else {
	            _size = _size * 0.5;*/
	        };
	    };
	
	    if (STUI_Occlusion && (player distance _x < STHud_FadeEdge) ) then {
	        private _vis = [(vehicle _x), "VIEW"] checkVisibility [eyePos player,  AGLToASL (_x modelToWorldVisual (_x selectionPosition "Spine3"))];
	        private _alphaPre = _colour select 3;
	
	        private _fadetime = 2.5;
	        private _fadeDegree = _x getVariable ["STUI_Occlude_Fade_HUD",0];
	
	        _alphaDiff = STUI_DeltaT / _fadetime;
	        _fadeState = _x getVariable ["STUI_Occlude_Fade_HUD",0];
	
	        if (_fadeState > 1) then {_fadeState = 1};
	        if (_fadeState < 0) then {_fadeState = 0};
	
	        if (_vis == 0 && _alphaPre > 0) then {
	            _curAlpha = _fadeState;
	            _newAlpha = (_curAlpha - _alphaDiff )min _alphaPre;
	            _x setVariable ["STUI_Occlude_Fade_HUD",_newAlpha];
	            _colour set [3,_newAlpha];
	        } else {
	            _curAlpha = _fadeState;
	            _newAlpha = (_curAlpha + _alphaDiff) min _alphaPre;
	            _x setVariable ["STUI_Occlude_Fade_HUD",_newAlpha];
	            _colour set [3,_newAlpha];
	        };
	    };
	    _canvas drawIcon [_icon, _colour, _mapPos,_size, _size, _dir];
	};
} forEach(_units);

if (STHud_NoSquadBarMode) then {
    private _showHud = shownHUD;
	_showHud set [6, false];
	showHud (_showHud select [0, 8]);
}
else
{
    private _showHud = shownHUD;
	_showHud set [6, true];
	showHud (_showHud select [0, 8]);
};

STUI_LastTick = time;

_playerDir = getDirVisual(vehicle(player));
_playerIcon = player call STHud_Icon;
_playerSize = 17;//19.2;
_canvas drawIcon [
    _playerIcon, _playerColour, STHud_Mid,
    _playerSize, _playerSize, _playerDir-_lookDir
];

if ( ("ItemCompass" in assignedItems(player)) || DA_STUIdirection ) then
{
    [_canvas, _lookDir] call STHud_Compass;
};