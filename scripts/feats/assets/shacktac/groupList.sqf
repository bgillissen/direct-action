params ["_canvas", "_colourFunc", "_playerColour"];

private _units = [];
{
    if (count(_units) >= count(STHud_Namepos)) exitWith {};

    if ( (alive(_x) || !(isNil {_x getVariable "sth_name"}) ) && !(isObjectHidden _x) ) then {
        _units pushBack [_x, _x call STHud_Colour_Text];
    };
} forEach (units(player));

// Could expand this to check if the player is a driver, gunner, or commander, but I don't recall what exactly is needed to check to catch multiple turrets etc while excluding FFV.
if ( (vehicle player != player && STHud_ShowBearingInVehicle) || DA_STUIbearing ) then
{
    private _viewVec = positionCameratoWorld [0,0,0] vectorFromTo (positionCameraToWorld [0,0,50]);
    private _bearing = ((_viewVec select 0) atan2 (_viewVec select 1) + 360) % 360;

    _bearingBase = (str round _bearing);
    _bearingPretty = "";

    switch (count _bearingBase) do
    {
        case 1: {_bearingPretty = "00" + _bearingBase};
        case 2: {_bearingPretty = "0" + _bearingBase};
        case 3: {_bearingPretty = _bearingBase};
    };

    _canvas drawIcon
    [
        "#(argb,8,8,3)color(0,0,0,0)", [1,1,1,.9],
        STHud_Bearing,
        16, 16, 0,
        _bearingPretty, STHud_TextShadow, 0.035, STHud_Font, "Center"
    ];
};

if (count(_units) == 1) exitWith {};

private _fullName = count(_units) < 7;

{
    _x params ["_unit", "_colour"];
    private _isUnitSelected = if (leader (group player)==player) then {_unit in (groupSelectedUnits player)} else {false}; // Check if they're leader, if not, ignore.
    private _selIndicator = "";
    if (_isUnitSelected) then {_selIndicator = "> "}; // Tried this on the left and right. Being on the left seems to make it 'pop' more.
    private _icon = "#(argb,1,1,1)color(0,0,0,0)";
    if ( leader(_unit) == _unit ) then { _icon = "a3\Ui_f\data\GUI\Cfg\Ranks\general_gs.paa"; };

    _finalName = ([_unit, _fullName] call STHud_GetName);

    if (STHud_NoSquadBarMode) then {_finalName = _selIndicator + _finalName;};

    if (count (groupSelectedUnits player) > 0 && STHud_NoSquadBarMode) then
    {
        private _tempName = vehicleVarName _unit;
        _unit setVehicleVarName "";
        private _strName = str(_x);
        _unit setVehicleVarName _tempName;
        private _pl = _strName find ":";
        private _numStr = _strName select [_pl +1,2];
        if (_numStr find "," > -1 || _numStr find " " > -1) then {_numStr = _numStr select [0,1]};
        _canvas drawIcon
        [
            "#(argb,1,1,1)color(0,0,0,0)", [1,1,1,1],
            STHud_Selectpos select _forEachIndex,
            1, 1, 0,
            _numStr, STHud_TextShadow, 0.035, STHud_Font, "Left"
        ];
    };

    _canvas drawIcon
    [
        _icon, _colour,
        STHud_Namepos select _forEachIndex,
        16, 16, 0,
        _finalName, STHud_TextShadow, 0.035, STHud_Font, "Right"
    ];
} forEach(_units);

if (count (groupSelectedUnits player) > 0 && STHud_NoSquadBarMode) then
{
    _canvas drawIcon
    [
        "#(argb,1,1,1)color(0,0,0,0)", [1,1,1,1],
        STHud_GroupNamePos,
        16, 16, 0,
        group(player) getVariable ["STMF_GroupID", ""], STHud_TextShadow, 0.035, STHud_Font, "Right"
    ];
};