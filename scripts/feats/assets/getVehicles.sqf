
params [["_dlcFilter", 0], ["_factionFilters", []], ["_kindFilters", []], ["_armed", []], ["_extFilters", []]];

_armed params [["_isArmed", 0], ["_func", "assets_fnc_isArmed"]];

private _filters = [_dlcFilter, _factionFilters, _kindFilters, _isArmed, _func, _extFilters] call assets_fnc_vehicleFilters;

private _veh = ["CfgVehicles", _filters, "configName (_this select 0)"] call assets_fnc_searchThings;

/*
diag_log "VEHICLE >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>";
{ 
	diag_log format["%1 => %2", _x, ( getText(configFile >> "CfgVehicles" >> _x >> "displayName"))];
} count _veh;

diag_log "VEHICLE <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<";
*/

_veh