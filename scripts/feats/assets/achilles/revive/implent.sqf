
if ( MOD_ace ) exitWith {};

#include "..\..\..\define.hpp"

#ifdef use_revive
["Direct Action", "Instant Revive", {_this call achilles_fnc_instantRevive}] call Ares_fnc_RegisterCustomModule;
["Direct Action", "Set uncounscious", {_this call achilles_fnc_inAgony}] call Ares_fnc_RegisterCustomModule;
#endif