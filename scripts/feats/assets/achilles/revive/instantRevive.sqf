
private _thing = param [1, ObjNull, [ObjNull]];

private _things = [];

if ( isNull _thing ) then {
	_things = (["objects"] call Achilles_fnc_SelectUnits);
} else {
	_things = [_thing];
};

if ( isNil "_things" ) exitWith {
    [objNull, "Nothing selected"] call bis_fnc_showCuratorFeedbackMessage;
};

private _units = [];
{ _units append (crew _x); } forEach _things;

private _players = allPlayers - (entities "HeadlessClient_F");

if ( ({_x in _players} count _units) == 0 ) exitWith {
	[objNull, "No player selected"] call bis_fnc_showCuratorFeedbackMessage;
};

private _c = 0;
{
	if ( _x in _players ) then {
		if ( _x getVariable ["agony", false] ) then {
			_c = _c + 1; 
			_x setVariable ["agony", false, true]; 
		};
	};
} forEach _units;

[objNull, format["%1 player(s) have been revived", _c]] call bis_fnc_showCuratorFeedbackMessage;