
#include "\A3\Functions_F_MP_Mark\DynamicGroupsCommonDefines.inc"

params ["_sideIdx", "_who", "_player"];

private _sides = [west, east, independent];
private _side = _sides select _sideIdx;

private _units = [];
if ( _who isEqualTo 0 ) then { _units = [_player]; };
if ( _who isEqualTo 1 ) then { _units = allPlayers - [_player]; };
if ( _who isEqualTo 2 ) then { _units = allPlayers; };
if ( _who > 2 ) then { _units = (units SQUADS select (_who - 3)); };

private _newGrps = [];
if ( _side isEqualTo PLAYER_SIDE ) then {
    {
		private "_leader";
	    if ( (count (units _x)) isEqualTo 0 ) then {
	        _leader = objNull;
	    } else {
	        _leader = (leader _x);
	    };
        _newGrps pushback [_x, (_x getVariable [VAR_GROUP_INSIGNIA, ""]), (groupId _x), _leader];
    } forEach SQUADS;
};

{
    private _unit = _x;
    private _srcGrp = group _unit;
    if !( (side _srcGrp) isEqualTo _side ) then {
    	private _srcId = groupId _srcGrp;
        private _insignia = _srcGrp getVariable [VAR_GROUP_INSIGNIA, ""];
        private _isLeader = ( (leader _srcGrp) isEqualTo _unit );
    	private "_newGrp";
    	{
        	if ( (_x select 2) isEqualTo _srcId ) exitWith { 
            	_newGrp = (_x select 0);
                if ( isNull (_x select 3) && _isLeader ) then {
                    _x set [3, _unit];
                    _newGrps set [_forEachIndex, _x]; 
				};
			};
    	} forEach _newGrps;
    	if ( isNil "_newGrp" ) then {
			_newGrp = createGroup _side;
            _newGrps pushback [_newGrp, _insignia, _srcId, (leader _srcGrp)];
    	};
        [_unit] joinSilent _newGrp;
	};
} forEach _units;

{
    _x params ["_grp", "_insignia", "_name", "_leader"];
 	["RegisterGroup", [_grp, _leader, [_insignia, _name, false]]] call BIS_fnc_dynamicGroups;    
} forEach _newGrps;