
private _groups = [];
_groups pushback "Only me";
_groups pushback "All excepted me";
_groups pushback "All";
{ _groups pushBack (groupId _x); } forEach SQUADS;

private _result = ["Switch Side", [
	["Side", ["Blufor", "Opfor", "Independent"], 0], 
    ["Who", _groups, 0]
]] call Ares_fnc_showChooseDialog;

if ( (count _result) isEqualTo 0 ) exitWith {
    [objNull, "Aborded"] call bis_fnc_showCuratorFeedbackMessage;
};

private _targetSide = (_result select 0);
private _who = (_result select 1);

[_targetSide, _who, player] remoteExec ["achilles_fnc_switchSide", 2, false];