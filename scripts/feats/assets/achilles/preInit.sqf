/*
@filename: feats\assets\achilles\preInit.sqf
Author:
	Ben
Description:
	run on server,
	disable Achilles remoteExecution feature
*/

missionNamespace setVariable ['Ares_Allow_Zeus_To_Execute_Code', true, true];