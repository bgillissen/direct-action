
if !( CTXT_PLAYER ) exitWith {};

params ["_thing"];

if ( (isNull _thing) || !(alive _thing) ) exitWith {};

private _action = ["gearSave", "loadAction"] call core_fnc_getSetting;

private _id = _thing addAction [_action, {call gearSave_fnc_load}, [], 0, false, true, "", "gearSaved", 4];

_thing setVariable ["achillesGearSave", _id];

