
if !( CTXT_PLAYER ) exitWith {};

params ["_thing"];

private _id = _thing getVariable ["achillesGearSave", -1];
if ( _id >= 0 ) then { _thing removeAction _id; };
