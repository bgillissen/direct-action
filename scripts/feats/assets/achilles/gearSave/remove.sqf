private _thing = param [1, ObjNull, [ObjNull]];

private _things = [];

if ( isNull _thing ) then {
	_things = (["objects"] call Achilles_fnc_SelectUnits);
} else {
	_things = [_thing];
};

if ( isNil "_things" ) exitWith {
    [objNull, "Nothing selected"] call bis_fnc_showCuratorFeedbackMessage;
};

if ( (count _things) == 0 ) exitWith {
	[objNull, "Nothing selected"] call bis_fnc_showCuratorFeedbackMessage;
};

{
    private _thing = _x;
    {
        _x params ["_jip", "_obj"];
        if ( _thing isEqualTo _obj ) exitWith {
            remoteExec ["", _jip];
            achillesGearSaveJIP deleteAt _forEachIndex; 
        };
	} forEach achillesGearSaveJIP;
    [_thing] remoteExec ["achilles_fnc_gearSaveRemovePlayer", 0, false];
} foreach _things;

publicVariable "achillesGearSaveJIP";

[objNull, format["Load Saved Gear Action removed from %1 objects", count _things]] call bis_fnc_showCuratorFeedbackMessage;