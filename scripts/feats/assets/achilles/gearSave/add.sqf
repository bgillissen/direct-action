
private _thing = param [1, ObjNull, [ObjNull]];

private _things = [];

if ( isNull _thing ) then {
	_things = (["objects"] call Achilles_fnc_SelectUnits);
} else {
	_things = [_thing];
};

if ( isNil "_things" ) exitWith {
    [objNull, "Nothing selected"] call bis_fnc_showCuratorFeedbackMessage;
};

{ 
	if !( alive _x ) then { _things deleteAt _forEachIndex; };
} forEach _things;

if ( (count _things) == 0 ) exitWith {
	[objNull, "Nothing in the selection is alive"] call bis_fnc_showCuratorFeedbackMessage;
};

if ( isNil "achillesGearSaveJIP" ) then { achillesGearSaveJIP = []; };

private _done = false;
{
    _x params ["_jip", "objects"];
    {
        if ( _things in _objects ) exitWith { _done = true; };
    } foreach _things;
} forEach achillesGearSaveJIP;

if ( _done ) exitWith {
	[objNull, "Failed, action has already been added to at least one selected object"] call bis_fnc_showCuratorFeedbackMessage;
};

private _removeEH = {
	params ["_thing"];
	{
		_x params ["_jip", "_obj"];
	    if ( _obj isEqualTo _thing) exitWith {
	    	remoteExec ["", _jip];
	    	achillesGearSaveJIP deleteAt _forEachIndex;   
	    };
	} forEach achillesGearSaveJIP;
	publicVariable "achillesGearSaveJIP";
};

{
	_x addEventHandler ["Deleted", _removeEH];
	private _jip = ([_x] remoteExec ["achilles_fnc_gearSaveAddPlayer", 0, true]);
	achillesGearSaveJIP pushback [_jip, _x];      	
} foreach _things;

publicVariable "achillesGearSaveJIP";

[objNull, format["Load Saved Gear action added to %1 objects", count _things]] call bis_fnc_showCuratorFeedbackMessage;