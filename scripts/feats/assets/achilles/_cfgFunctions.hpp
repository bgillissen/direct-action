class achilles {
	tag = "achilles";
	class functions {
		class init { file="core\dummy.sqf"; };
		class preInit { file="feats\assets\achilles\preInit.sqf"; };
		class postInit { file="feats\assets\achilles\postInit.sqf"; };
		#include "arsenal\_cfgFunctions.hpp"
		#include "artiSupport\_cfgFunctions.hpp"
		#include "autoFlares\_cfgFunctions.hpp"
		#include "chopperDamage\_cfgFunctions.hpp"
		#include "gearSave\_cfgFunctions.hpp"
		#include "hostage\_cfgFunctions.hpp"
		#include "parameters\_cfgFunctions.hpp"
		#include "revive\_cfgFunctions.hpp"
		#include "spawnVehicle\_cfgFunctions.hpp"
		#include "supplyDrop\_cfgFunctions.hpp"
		#include "switchSide\_cfgFunctions.hpp"
		#include "trappedAnimal\_cfgFunctions.hpp"
		#include "uavRecon\_cfgFunctions.hpp"
		#include "vcomai\_cfgFunctions.hpp"
		#include "zeusCompo\_cfgFunctions.hpp"
	};
};
