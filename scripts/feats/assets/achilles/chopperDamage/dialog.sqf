
private _thing = param [1, ObjNull, [ObjNull]];

private _things = [];

if ( isNull _thing ) then {
	_things = (["objects"] call Achilles_fnc_SelectUnits);
} else {
	_things = [_thing];
};

if ( isNil "_things" ) exitWith {
    [objNull, "Nothing selected"] call bis_fnc_showCuratorFeedbackMessage;
};

private _vehs = [];
{ 
	if ( _x isKindOf "Helicopter" ) then { 
    	_vehs pushback _x 
	} else { 
    	if ( (vehicle _x) isKindOf "Helicopter" ) then { _vehs pushback (vehicle _x) };
	};
} forEach _things;

if ( (count _vehs) == 0 ) exitWith {
	[objNull, "No Helicopter selected"] call bis_fnc_showCuratorFeedbackMessage;
};

private _result = ["Set Rotor Damage",
[
	["Main Rotor", "SLIDER", 0],
    ["Tail Rotor", "SLIDER", 0]
]] call Ares_fnc_showChooseDialog;

if (count _result == 0) exitWith {
    [objNull, "Aborded"] call bis_fnc_showCuratorFeedbackMessage;
};

_result params ["_main", "_tail"];
{
    [_x, _main, _tail] remoteExec ["achilles_fnc_chopperDamageSet", 0, false];
} forEach _vehs;

[objNull, "Damage applyed"] call bis_fnc_showCuratorFeedbackMessage;