
private _thing = param [1, ObjNull, [ObjNull]];

private _things = [];

if ( isNull _thing ) then {
	_things = (["objects"] call Achilles_fnc_SelectUnits);
} else {
	_things = [_thing];
};

if ( isNil "_things" ) exitWith {
    [objNull, "Nothing selected"] call bis_fnc_showCuratorFeedbackMessage;
};

private _units = [];
{ _units append (units group _x); } forEach _things;

private _on = 0;
private _off = 0;

{
	private _noAI = (_x getVariable['NOAI', false]);
	if ( _noAI ) then {
		_on = _on + 1;
	} else {
		_off = _off + 1;
	};
	_x 	setVariable ['NOAI', !_noAI, true];
} forEach _units;

private _msg = format["vcomAI has been turned off on %2 units, and on on %1 units.", _on, _off];
[objNull, _msg] call bis_fnc_showCuratorFeedbackMessage;