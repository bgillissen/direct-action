
params ["_pos", "_color", "_radius", "_amount", "_rate"];

private _origin = "Land_BloodBag_F" createVehicle [(_pos select 0),(_pos select 1), 0];
[_origin, false] call curator_fnc_addEditable;

private _flareClass = format["F_40mm_%1", _color];
private _color = getArray(missionConfigfile >> "settings" >> "flares" >> "colors" >> _flareClass);

private "_count"; 
if ( _radius isEqualTo 100 ) then { _count = 2 * _amount; };
if ( _radius isEqualTo 250 ) then { _count = 4 * _amount; };
if ( _radius isEqualTo 500 ) then { _count = 6 * _amount; };
if ( _radius isEqualTo 1000 ) then { _count = 8 * _amount; };

private _delay = 15 / _rate / _count;

//TODO devide area into 4 sectors and spawn flares into those.
private _sector = 0;

while { !isNull _origin } do {
    if ( _sector > 3 ) then { _sector = 0; };
    private _rndPos = [(getPos _origin), _radius] call BIS_fnc_randomPosTrigger;
    private _alt = (120 + (random 80));
    _rndPos set [2, _alt];
    private _flare = _flareClass createVehicle _rndPos;
    _flare setVelocity [0, 0, -3];
	[_flare, _color] remoteExec ["flares_fnc_flare", 0, false];
    sleep _delay;
    _sector = _sector + 1;
};