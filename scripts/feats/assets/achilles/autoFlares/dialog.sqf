
private _pos = param [0, [0,0,0], [[]]];

private _colors = ["White", "Red", "Green", "Yellow"];
private _radiusDsp = ["100m", "250m", "500m", "1Km"];
private _radiusVal = [100, 250, 500, 1000];

private _result = ["Auto Flares Properties",
[
	["Color", _colors, 0], 
	["Radius", _radiusDsp, 2],
	["Amount", "SLIDER", 0.5],
    ["Rate", "SLIDER", 0.5]
]] call Ares_fnc_showChooseDialog;

if ( (count _result) isEqualTo 0 ) exitWith {
    [objNull, "Aborded"] call bis_fnc_showCuratorFeedbackMessage;
};

if ( ((_result select 2) isEqualTo 0) ) exitWith {
    [objNull, "Amount is zero"] call bis_fnc_showCuratorFeedbackMessage;
};

if ( ((_result select 3) isEqualTo 0) ) exitWith {
    [objNull, "Rate is zero"] call bis_fnc_showCuratorFeedbackMessage;
};

[_pos, (_colors select (_result select 0)), (_radiusVal select (_result select 1)), (_result select 2), (_result select 3)] remoteExec ["achilles_fnc_autoFlaresThread", 2, false];