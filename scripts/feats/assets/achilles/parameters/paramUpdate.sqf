
private _paramsOpt = [];

{
	private _param = _x; 
	private _entry = [];
	_entry set [0, getText(_param >> "title")];

    private _texts = getArray(_param >> "texts");
    {
        if !( (typeName _x) isEqualTo "STRING" ) then { _texts set [_forEachIndex, (str _x)]; }; 
	} forEach _texts;
    _entry set [1, _texts];
    
    private _newVal = time;
    if !( isNil "ACHILLES_MISPARAMS" ) then { _newVal = (ACHILLES_MISPARAMS select _forEachIndex); };
	private _curVal = ([configName _param] call core_fnc_getParam);
    private _dftVal = getNumber(_param >> "default");
    private _newIndex = -1;
    private _curIndex = -1;
    private _dftIndex = -1;
    {
        if ( _newVal isEqualTo _x ) then { _newIndex = _forEachIndex; };
        if ( _curVal isEqualTo _x ) then { _curIndex = _forEachIndex; };
		if ( _dftVal isEqualTo _x ) then { _dftIndex = _forEachIndex; };
	} forEach getArray(_param >> "values");
    if ( _newIndex >= 0 ) then {
		_entry set [2, _newIndex];
	} else {
		if ( _curIndex >= 0 ) then { 
			_entry set [2, _curIndex];
		} else {
            if ( _dftIndex >= 0 ) then {
				_entry set [2, _dftIndex]; 
			} else {
				_entry set [2, 0];
			};
		};
	};
    _entry set [3, true];
    _paramsOpt pushback _entry;
} forEach ("true" configClasses (missionConfigFile >> "params") );

private _result = ["Mission Parameters", _paramsOpt] call Ares_fnc_showChooseDialog;

if ( (count _result) isEqualTo 0 ) exitWith {
    [objNull, "Aborded"] call bis_fnc_showCuratorFeedbackMessage;
};


if ( isNil "ACHILLES_MISPARAMS" ) then { ACHILLES_MISPARAMS = []; };

{
    private _vals = getArray(_x >> "values");
	ACHILLES_MISPARAMS set [_forEachIndex, (_vals select (_result select _forEachIndex))];   
} forEach ("true" configClasses (missionConfigFile >> "params") );

[objNull, "Parameters stored, not yet applyed"] call bis_fnc_showCuratorFeedbackMessage;