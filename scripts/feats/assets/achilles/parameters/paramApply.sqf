

if ( isNil "ACHILLES_MISPARAMS" ) exitWith {
    [objNull, "No point resetting the server without any parameters change"] call bis_fnc_showCuratorFeedbackMessage;
};

if !( ((admin (owner player)) > 0) || CTXT_SERVER ) exitWith {
	[objNull, "You must be logged as admin to apply the new parameters"] call bis_fnc_showCuratorFeedbackMessage;
};

ACHILLES_MISPARAMS remoteExec ["core_fnc_resetServer", 2, false];