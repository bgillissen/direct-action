
private _thing = param [1, ObjNull, [ObjNull]];

private _things = [];

if ( isNull _thing ) then {
	_things = (["objects"] call Achilles_fnc_SelectUnits);
} else {
	_things = [_thing];
};

if ( isNil "_things" ) exitWith {
    [objNull, "Nothing selected"] call bis_fnc_showCuratorFeedbackMessage;
};

if ( (count _things) isEqualTo 0 ) exitWith {
    [objNull, "Nothing selected"] call bis_fnc_showCuratorFeedbackMessage;
};

private _Hcs = (entities "HeadlessClient_F");

{
    if ( isNull _x ) then { _things deleteAt _forEachIndex; };
    if !( _x isKindOf "Man" ) then { _things deleteAt _forEachIndex; };
	if ( isPlayer _x ) then { _things deleteAt _forEachIndex; };
    [_x] call BIS_fnc_drawCuratorDeaths;
} forEach _things;

if ( (count _things) isEqualTo 0 ) exitWith {
    [objNull, "Nothing valid selected"] call bis_fnc_showCuratorFeedbackMessage;
};

_things call hostage_fnc_serverAdd;

[objNull, format["%1 Unit(s) have been set as hostage", (count _things)]] call bis_fnc_showCuratorFeedbackMessage;