
private _pos = param [0, [0,0,0], [[]]];

//private _typeDsp = ["Darter", "Greyhawk"];
//private _typeVal = ["B_UAV_01_F", "B_UAV_02_F"];

private _altDsp = ["500m", "1000m", "1500m", "2000m"];
private _altVal = [500, 1000, 1500, 2000];

private _radiusDsp = ["200m", "500m", "750m", "1000m", "1500m"];
private _radiusVal = [200, 500, 750, 1000, 1500];

private _loiter = ["Disabled", "Enabled"];
private _bearings = ["N", "NE", "E", "SE", "S", "SW", "W", "NW"];

private _modeDsp = ["Normal", "NV", "Thermal"];
private _modeVal = [0, 1, 2];

private _result = ["UAV Recon",
[
	["Name", "", "UAV Recon"],
	["Altitude", _altDsp, 1],
    ["Loiter", _loiter, 0], 
	["Distance", _radiusDsp, 2],
    ["Spawn Position", _bearings, 0],
    ["Mode", _modeDsp, 0],
    ["Zoom", "SLIDER", 0.65]
]] call Ares_fnc_showChooseDialog;

if ( (count _result) isEqualTo 0 ) exitWith {
    [objNull, "Aborded"] call bis_fnc_showCuratorFeedbackMessage;
};

private _uniqueName = true;
{
    if ( (_x select 0) isEqualTo (_result select 0) ) exitWith { _uniqueName = false; };
} forEach uavReconStack;

if !( _uniqueName ) exitWith {
    [objNull, "Name already in use !"] call bis_fnc_showCuratorFeedbackMessage;    
};

[_pos,
 (_result select 0),
 "B_UAV_01_F", 
 (_altVal select (_result select 1)),
 ((_result select 2) isEqualTo 1), 
 (_radiusVal select (_result select 3)),
 (_bearings select (_result select 4)),
 (_modeVal select (_result select 5)),
 (2 - ((_result select 6) * 2))
] remoteExec ["uavRecon_fnc_serverStart", 2, false];

[objNull, format["%1 spawned", (_result select 0)]] call bis_fnc_showCuratorFeedbackMessage;