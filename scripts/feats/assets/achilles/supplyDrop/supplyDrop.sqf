
private _pos = param [0,[0,0,0],[[]]];
 
_pos set [2, 100];

private _chuteType = PLAYER_SIDE call {
	if ( _this == west ) exitWith { "B_Parachute_02_F" };
	if ( _this == east ) exitWith { "O_Parachute_02_F" };
	"I_Parachute_02_F"
};

private _chute = createVehicle [_chuteType, _pos, [], 0, 'FLY'];
private _crate = createVehicle [(selectRandom SD_crates), position _chute, [], 0, 'NONE'];

_crate attachTo [_chute, [0, 0, -1.3]];
_crate allowdamage false;
	
[_crate, SD_backpacks, SD_items, SD_weapons, SD_ammo] call common_fnc_setCargo;

waitUntil {
	sleep 0.2;
	( position _crate select 2 < 1 || isNull _chute )
};

private _posCrate = position _crate;
detach _crate;
_crate enableSimulationGlobal true;
//to make it touch the ground next to the crate, it's needed or it never collapse
_chute setPos [(_posCrate select 0) + 0.5 , (_posCrate select 1) + 0.5, (_posCrate select 2)];

private _smoke = ["supplyDrop", "smoke"] call core_fnc_getSetting;
_smoke = _smoke createVehicle [_posCrate select 0, _posCrate select 1, 2];

private _light = ["supplyDrop", "light"] call core_fnc_getSetting;
_light = _light createVehicle [(_posCrate select 0) + 0.5, (_posCrate select 1) + 0.5, 2];

[[_crate, _light, _smoke], false] call curator_fnc_addEditable;