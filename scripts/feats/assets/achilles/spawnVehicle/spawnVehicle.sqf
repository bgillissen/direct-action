
params ["_poolName", "_pos"];

private _pool = missionNamespace getVariable format["BV_%1", _poolName];

private _veh = createVehicle [(selectRandom _pool), _pos, [], 0, "CAN_COLLIDE"];

private _args = [_veh];
_args append (missionNamespace getVariable format["VC_%1", _poolName]);
_args call common_fnc_setCargo;

[_veh, true] call curator_fnc_addEditable;