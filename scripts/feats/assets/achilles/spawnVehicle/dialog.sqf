
private _pos = param [0,[0,0,0],[[]]];

private _pools = ["car", "carArmed", "apc", "tank", "aaTank", "planeCAS", "planeAA", "planeTransport", "uav", 
	              "heliSmall", "heliSmallArmed", "heliMedium", "heliMedEvac", "heliBig", "heliAttack",
				  "boatSmall", "boatAttack", "boatBig", "sub", "landMedic", "repair", "fuel","ammo", "truck", "quad"];

private _result = ["Select Vehicle Pool", [["Pool", _pools, 0]]] call Ares_fnc_showChooseDialog;

if (count _result == 0) exitWith {
    [objNull, "Aborded"] call bis_fnc_showCuratorFeedbackMessage;
};

[(_pools select (_result select 0)), _pos] remoteExec ["achilles_fnc_spawnVehicle", 2, false];