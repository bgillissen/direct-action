
if ( (["artiSupport"] call core_fnc_getParam) isEqualTo 0 ) exitWith {};

if ( [player] call as_fnc_canGrant ) then {
	["Direct Action", "Grant Artillery Stryke", {createDialog "artiSupportGrant"}] call Ares_fnc_RegisterCustomModule;    
};

if ( [player] call as_fnc_canControl ) then {
    ["Direct Action", "Artillery Control Panel", {createDialog "artiSupportControl"}] call Ares_fnc_RegisterCustomModule;
};
