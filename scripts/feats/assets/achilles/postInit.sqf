/*
@filename: feats\assets\achilles\postInit.sqf
Author:
	Ben
Description:
	run on player
	implent achilles feature's custom modules
*/

{
	private _fnc =  format["achilles_fnc_%1Implent", _x];
	if ( !isNil _fnc ) then {
		private _code = compile format["call %1", _fnc];
		0 = call _code;
	#ifdef DEBUG
	} else {
		private _debug = format["Achilles %1 postInit failed, function %1 is not defined)", _name, _fnc];
		debug(LL_ERR, _debug);
	#endif	
	};
} forEach getArray(missionConfigFile >> "settings" >> "assets" >> "achilles" >> "features");