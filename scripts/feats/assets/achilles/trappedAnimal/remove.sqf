
if !( CTXT_SERVER ) exitWith {
    _this remoteExec ["achilles_fnc_taRemove", 2, false];
};

diag_log "trappedAnimal: removing jip, trigger and EHs globaly";

remoteExec ["", (_this getVariable ["jip", ""])];

deleteVehicle (_this getVariable ["trigger", objNull]);

[_this, false] remoteExec ["achilles_fnc_localEH", 0, false];