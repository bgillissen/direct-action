
private _pos = param [0, [0,0,0], [[]]];

private _typeVal = [];
private _typeDsp = [];

if ( surfaceIsWater _pos ) then {
    _typeVal = ["CatShark_F", "Turtle_F"];
	_typeDsp = ["Shark", "Turtle"];
} else {
    _typeVal = ["Fin_random_F", "Goat_random_F", "Sheep_random_F"];
	_typeDsp = ["Dog", "Goat", "Sheep"];
};

private _radVal = [0, 5, 10, 15, 20, 30];
private _radDsp = ["None", "5m", "10m", "15m", "20m", "30m"];
private _bombVal = ["APERSTripMine_Wire_Ammo", "DemoCharge_Remote_Ammo_Scripted", "Bo_GBU12_LGB"]; 
private _bombDsp = ["Apers Mine", "C4", "GBU"];

private _result = ["Trapped Animal", [
	["Kind", _typeDsp, 0], 
    ["Detection Radius", _radDsp, 0],
    ["Detonate on death", ["False", "True"], 0],
    ["Ordonance", _bombDsp, 0]
]] call Ares_fnc_showChooseDialog;

if ( (count _result) isEqualTo 0 ) exitWith {
    [objNull, "Aborded"] call bis_fnc_showCuratorFeedbackMessage;
};

private _type = _typeVal select (_result select 0);
private _radius = _radVal select (_result select 1);
private _onDeath = [false, true] select (_result select 2);
private _bomb = _bombVal select (_result select 3);

[_pos, _type, _radius, _onDeath, _bomb] remoteExec ["achilles_fnc_taSpawn", 2, false];