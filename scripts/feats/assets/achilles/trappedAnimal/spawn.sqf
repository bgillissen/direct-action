
params ["_pos", "_animal", "_radius", "_onDeath", "_bomb"];

private _group = createGroup Civilian;
//_group setGroupIdGlobal [("Trapped Animal" call ia_fnc_getGroupName)];
_group setGroupIdGlobal [(["Trapped Animal", allGroups] call common_fnc_getUniqueName)];

private _unit = _group createUnit [_animal, _pos, [], 0, "NONE"];
{ 
	_unit setObjectTextureGlobal [_forEachIndex, _x];
} forEach (getObjectTextures _unit);

_unit setVariable ["bomb", _bomb, true];
_unit setVariable ["onDeath", _onDeath, true];

if ( _radius > 0 ) then {
	private _trigger = createTrigger ["EmptyDetector", getPos _unit];
	_trigger setTriggerArea [_radius, _radius, 0, false];
	_trigger setTriggerActivation [toUpper (str PLAYER_SIDE), "PRESENT", false];
	_trigger setTriggerStatements ["this", "(thisTrigger getVariable 'unit') call achilles_fnc_taDetonate", ""];
	_trigger attachTo [_unit, [0,0,0]];
    _trigger setVariable ["unit", _unit];
    _unit setVariable ["trigger", _trigger];
};

[_unit, true] call achilles_fnc_taLocalEH;
[_unit, true] call curator_fnc_addEditable;

private _jip = _unit remoteExec["achilles_fnc_taAddEH", 0, true];
_unit setVariable ["jip", _jip];