
params ["_unit", "_isLocal"];

if ( _isLocal ) then {
    diag_log "trappedAnimal: adding eventHandlers";
    private _eh = _unit addEventHandler ["deleted", {
   	 	diag_log "trappedAnimal: delete event";
        (_this select 0) call achilles_fnc_taRemove;
	}];
    _unit setVariable ['delEH', _eh];
    private _eh = _unit addEventHandler ["Killed", {
        diag_log "trappedAnimal: killed event";
        (_this select 0) call achilles_fnc_taDetonate;
   	}];
    _unit setVariable ['deathEH', _eh];
} else {
    diag_log "trappedAnimal: removing eventHandlers";
    private _eh = _unit getVariable ['delEH', -1];
    if ( _eh >= 0 ) then { 
    	_unit removeEventHandler["deleted", _eh];
        _unit setVariable ['delEH', -1]; 
    };
    private _eh = _unit getVariable ['deathEH', -1];
    if ( _eh >= 0 ) then { 
    	_unit removeEventHandler["Killed", _eh];
        _unit setVariable ['deathEH', -1]; 
	};
};
