
if !( CTXT_SERVER ) exitWith {
    _this remoteExec ["achilles_fnc_taDetonate", 2, false];
};

private _units = [];

if ( (typeName _this) isEqualTo "OBJECT" ) then { 
	_units pushback _this; 
} else {
	private _thing = param [1, ObjNull, [ObjNull]];
	if ( isNull _thing ) then {
		_units = (["objects"] call Achilles_fnc_SelectUnits);
	} else {
		_units pushback _thing;
	};
};

if ( (count _units) isEqualTo 0 ) exitWith {};

{
    private _doIt = true;
    if !( alive _x ) then { _doIt = _x getVariable ["onDeath", false]; };
    if ( _doIt ) then {
        private _ordonance = _x getVariable ["bomb", ""];
    	if !( _ordonance isEqualTo "" ) then {
    		private _bomb = _ordonance createVehicle (getPos _x);
        	_bomb hideObjectGlobal true;
        	_bomb setDamage 1;
    	};
	};
    _x call achilles_fnc_taRemove;
} forEach _units;