private _thing = param [1, ObjNull, [ObjNull]];

private _things = [];

if ( isNull _thing ) then {
	_things = (["objects"] call Achilles_fnc_SelectUnits);
} else {
	_things = [_thing];
};

if ( isNil "_things" ) exitWith {
    [objNull, "Nothing selected"] call bis_fnc_showCuratorFeedbackMessage;
};

{ 
	if !( alive _x ) then { _things deleteAt _forEachIndex; };
} forEach _things;

if ( (count _things) == 0 ) exitWith {
	[objNull, "Nothing in the selection is alive"] call bis_fnc_showCuratorFeedbackMessage;
};

{ [_x] remoteExec ["va_fnc_remove", 2, false]; } forEach _things;

[objNull, format["Filtered Arsenal removed from %1 objects", count _things]] call bis_fnc_showCuratorFeedbackMessage;