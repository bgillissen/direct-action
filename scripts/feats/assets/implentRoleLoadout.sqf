/*
@filename: feats\assets\implentRoleLoadout.sqf
Author:
	Ben
Description:
	run on server
	it is used to update a role loadout
*/

#include "_debug.hpp"

params ["_target", "_index", "_loadOut", "_side"];

if ( isNil "_loadout" ) exitWith {};
if ( count _loadOut == 0 ) exitWith {};

_loadOut params["_uniform", "_vest", "_backpack", "_pw", "_sw", "_hw", "_helmet", "_face", "_comm", "_term", "_map", "_bino", "_nv", "_watch", "_compass"];
_loadOut = nil;

private ["_vname", "_current"];
if ( _target isEqualTo "player" ) then {
    //_vname = format["%1_%2", ((PV select RL_k) select 0), (((PV select RL_k) select 1) select _index)];
	_vname = format["%1_%2_%3", ((PV select RL_k) select 0), (((PV select RL_k) select 1) select _index), _side];
	_current = missionNamespace getVariable _vname;
};
if ( _target isEqualTo "npc" ) then {
    //_vname = format["%1_%2", ((PV select BALO_k) select 0), (((PV select BALO_k) select 1) select _index)];
	_vname = format["%1_%2_%3", ((PV select BALO_k) select 0), (((PV select BALO_k) select 1) select _index), _side];
	_current = missionNamespace getVariable _vname;
};

if ( isNil '_current' ) exitWith {
    #ifdef DEBUG
    private _debug = format["failed to implent roleLoadout, variable '%1' is not define", _vname];
    debug(LL_ERR, _debug);
    #endif
};

private _appendTo = {
	params ["_target", "_thing", ["_qty", 1]];
    if ( _thing isEqualTo "clearMags" ) exitWith {
        diag_log "clearing Mags";
    	{
			if ( isClass(configFile >> "CfgMagazines" >> (_x select 0))) then { _x set [1, 0];diag_log format["removing %1", _x]; };
		} forEach _target;	    
    };
    if ( _thing isEqualTo "clearItems" ) exitWith {
    	{
			if ( getNumber(configFile >> "CfgWeapons" >> (_x select 0) >> "itemInfo" >> "type") in _qty ) then { _target deleteAt _forEachIndex; };
		} forEach _target;    
    };
    if ( _thing isEqualTo "clearSlots" ) exitWith {
    	{
			if ( getNumber(configFile >> "CfgWeapons" >> (_x select 0) >> "type") in _qty ) then { _target deleteAt _forEachIndex; };
		} forEach _target;    
    };
    private _idx = -1;
    {
		if ( (_x select 0) isEqualTo _thing ) exitWith { _idx = _forEachIndex; }
	} forEach _target;
    if ( _qty > 0 ) then {
        if ( _idx < 0 ) then {
            _target pushback [_thing, _qty];
        } else {
            (_target select _idx) set [1, (((_target select _idx) select 1) + _qty)];
        };
    } else {
        if ( _idx >= 0 ) then {
        	private "_newQty";
        	if ( _qty isEqualTo 0 ) then {
        		_newQty = 0;
			} else {
            	_newQty = (((_target select _idx) select 1) + _qty);
			};
            if ( _newQty <= 0 ) then {
                _target deleteAt _idx;
            } else {
            	(_target select _idx) set [1, _newQty];
			};
        };
    };  
};

//uniform
private _key = 0;
_uniform params ["_container", "_content"];
_uniform = nil;
if !( isNil "_container" ) then {
	if !( [_container, 'item'] call common_fnc_isBlacklisted ) then {
		(_current select _key) set [0, _container]; 
	};
};
{
    _x params ["_thing", ["_qty", 1]];
	if !( [_thing, 'item'] call common_fnc_isBlacklisted ) then {
		if !( [_thing, 'ammo'] call common_fnc_isBlacklisted ) then {
            if !( [_thing, 'weapon'] call common_fnc_isBlackListed ) then {
                [((_current select _key) select 1), _thing, _qty] call _appendTo;
			};
		};
	};
} forEach _content;
_container = nil;
_content = nil;

//vest
_key = 1;
_vest params ["_container", "_content"];
_vest = nil;
if !( isNil "_container" ) then {
	if !( [_container, 'item'] call common_fnc_isBlacklisted ) then {
		(_current select _key) set [0, _container]; 
	};
};
{
    _x params ["_thing", ["_qty", 1]];
	if !( [_thing, 'item'] call common_fnc_isBlacklisted ) then {
		if !( [_thing, 'ammo'] call common_fnc_isBlacklisted ) then {
            if !( [_thing, 'weapon'] call common_fnc_isBlackListed ) then {
				[((_current select _key) select 1), _thing, _qty] call _appendTo;
			};
		};
	};
} forEach _content;
_container = nil;
_content = nil;

//backpack
_key = 2;
_backpack params ["_container", "_content"];
_backpack = nil;
if !( isNil "_container" ) then {
	if !( [_container, 'backpack'] call common_fnc_isBlacklisted ) then {
		(_current select _key) set [0, _container]; 
	};
};
{
    _x params ["_thing", ["_qty", 1]];
	if !( [_thing, 'item'] call common_fnc_isBlackListed ) then {
		if !( [_thing, 'ammo'] call common_fnc_isBlackListed ) then {
            if !( [_thing, 'weapon'] call common_fnc_isBlackListed ) then {
				[((_current select _key) select 1), _thing, _qty] call _appendTo;
			};
		};
	};
} forEach _content;
_container = nil;
_content = nil;

//primWeap
_key = 3;
_pw params ["_container", "_content"];
_pw = nil;
if !( isNil "_container" ) then {
	if !( [_container, 'weapon'] call common_fnc_isBlackListed ) then {
		(_current select _key) set [0, _container];
	};
};
{
    if !( isNil "_x" ) then {
		if !( [_x, 'item'] call common_fnc_isBlackListed ) then {
        	((_current select _key) select 1) set [_forEachIndex, _x];
        };
	};
} forEach _content;
_container = nil;
_content = nil;

//secondWeap
_key = 4;
_sw params ["_container", "_content"];
_sw = nil;
if !( isNil "_container" ) then {
	if !( [_container, 'weapon'] call common_fnc_isBlackListed ) then {
		(_current select _key) set [0, _container];
	};
};
{
    if !( isNil "_x" ) then {
		if !( [_x, 'item'] call common_fnc_isBlackListed ) then {
			((_current select _key) select 1) set [_forEachIndex, _x];
		};
    };
} forEach _content;
_container = nil;
_content = nil;

//handWeap
_key = 5;
_hw params ["_container", "_content"];
_hw = nil;
if !( isNil "_container" ) then {
	if !( [_container, 'weapon'] call common_fnc_isBlackListed ) then {
		(_current select _key) set [0, _container];
	};
};
{
    if !( isNil "_x" ) then {
		if !( [_x, 'item'] call common_fnc_isBlackListed ) then {
			((_current select _key) select 1) set [_forEachIndex, _x];
        };
	};
} forEach _content;
_container = nil;
_content = nil;

_key = 6;
if !( isNil "_helmet" ) then {
	if !( [_helmet, 'item'] call common_fnc_isBlackListed ) then { _current set [_key, _helmet]; };
};
_helmet = nil;

_key = 7;
if !( isNil "_face" ) then {
	if !( [_face, 'item'] call common_fnc_isBlackListed ) then { _current set [_key, _face]; };
};
_face = nil;

_key = 8;
if !( isNil "_comm" ) then {
	if !( [_comm, 'item'] call common_fnc_isBlackListed ) then { _current set [_key, _comm]; };
};
_comm = nil;

_key = 9;
if !( isNil "_term" ) then {
	if !( [_term, 'item'] call common_fnc_isBlackListed ) then { _current set [_key, _term]; };
};
_term = nil;

_key = 10;
if !( isNil "_map" ) then {
	if !( [_map, 'item'] call common_fnc_isBlackListed ) then { _current set [_key, _map]; };
};
_map = nil;

_key = 11;
if !( isNil "_bino" ) then {
	if !( [_bino, 'item'] call common_fnc_isBlackListed ) then { _current set [_key, _bino]; };
};
_bino = nil;

_key = 12;
if !( isNil "_nv" ) then {
	if !( [_nv, 'item'] call common_fnc_isBlackListed ) then { _current set [_key, _nv]; };
};
_nv = nil;

_key = 13;
if !( isNil "_watch" ) then {
	if !( [_watch, 'item'] call common_fnc_isBlackListed ) then { _current set [_key, _watch]; };
};
_watch = nil;

_key = 14;
if !( isNil "_compass" ) then {
	if !( [_compass, 'item'] call common_fnc_isBlackListed ) then { _current set [_key, _compass]; };
};
_compass = nil;


missionNameSpace setVariable [_vname, _current, false];