/*
@filename: feats\assets\rmSplinter\init.sqf
Author:
	Ben
Description:
	run on server
	implent Rainman's splinter camo assets
*/

private _c = call rmSplinter_fnc_assets;

(_c select A_RMS) params ["_backpacks", "_items", "_weapons", "_ammo"];

[_backpacks, _items, _weapons, _ammo] call assets_fnc_implentArsenal;

private _modFilter = ["unsung"];

if ( ["rl", "rmSplinter", [PLAYER_SIDE], _modFilter, false] call assets_fnc_condition ) then {
	{
		["player", _forEachIndex, (_c select RL_RMS select _forEachIndex), PLAYER_SIDE] call assets_fnc_implentRoleLoadout;
	} forEach ((PV select RL_k) select 1);
};

if ( ["balo", "rmSplinter", [PLAYER_SIDE], _modFilter, false] call assets_fnc_condition ) then {
	{
		["npc", _forEachIndex, (_c select BALO_RMS select _forEachIndex), PLAYER_SIDE] call assets_fnc_implentRoleLoadout;
	} forEach ((PV select BALO_k) select 1);
};