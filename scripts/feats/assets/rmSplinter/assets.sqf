/*
@filename: feats\assets\rmSplinter\assets.sqf
Author:
	Ben
Description:
	run on server
	return Rainman's splinter camo assets
*/

private _out = [];

//------------------------------------------------------------ Arsenal RMS

A_RMS = 0;

private _backpacks = ["Rainman_Kitbag_Splinter",
					  "Rainman_Kitbag_Splinter_Urban",
                      "Rainman_Kitbag_Splinter_Desert",
                      "Rainman_Kitbag_Splinter_Winter",
                      "Rainman_Kitbag_Splinter_Multi"];
private _items = ["Rainman_Combat_Fatigues_Splinter",
				  "Rainman_Combat_Fatigues_Splinter_2", 
				  "Rainman_Combat_Fatigues_Splinter_Urban",
                  "Rainman_Combat_Fatigues_Splinter_Urban_2",
                  "Rainman_Combat_Fatigues_Splinter_Desert",
                  "Rainman_Combat_Fatigues_Splinter_Desert_2",
                  "Rainman_Combat_Fatigues_Splinter_Winter",
                  "Rainman_Combat_Fatigues_Splinter_Winter_2",
                  "Rainman_Combat_Fatigues_Splinter_Multi",
                  "Rainman_Combat_Fatigues_Splinter_Multi_2",
                  "Rainman_Combat_Fatigues_Sleeve_Splinter",
                  "Rainman_Combat_Fatigues_Sleeve_Splinter_Desert",
                  "Rainman_Combat_Fatigues_Sleeve_Splinter_Urban",
                  "Rainman_Combat_Fatigues_Sleeve_Splinter_Multi",
                  "Rainman_Combat_Fatigues_Tee_Splinter", 
                  "Rainman_Combat_Fatigues_Tee_Splinter_2", 
                  "Rainman_Combat_Fatigues_Tee_Splinter_Urban", 
                  "Rainman_Combat_Fatigues_Tee_Splinter_Urban_2", 
                  "Rainman_Combat_Fatigues_Tee_Splinter_Desert",
                  "Rainman_Combat_Fatigues_Tee_Splinter_Desert_2",
                  "Rainman_Combat_Fatigues_Tee_Splinter_Multi",
                  "Rainman_Combat_Fatigues_Tee_Splinter_Multi_2",
                  "Rainman_Booniehat_Splinter",
                  "Rainman_Booniehat_Splinter_Urban",
                  "Rainman_Booniehat_Splinter_Desert",
                  "Rainman_Booniehat_Splinter_Multi",
                  "Rainman_HelmetB_Light_Splinter",
                  "Rainman_HelmetB_Light_Splinter_Urban",
                  "Rainman_HelmetB_Light_Splinter_Desert",
                  "Rainman_HelmetB_Light_Splinter_Winter",
                  "Rainman_HelmetB_Light_Splinter_Multi",
                  "Rainman_HelmetIA_Splinter",
                  "Rainman_HelmetIA_Splinter_Urban",
                  "Rainman_HelmetIA_Splinter_Desert",
                  "Rainman_HelmetIA_Splinter_Winter",
                  "Rainman_HelmetIA_Splinter_Multi",
                  "Rainman_MilCap_Splinter",
                  "Rainman_MilCap_Splinter_Urban",
                  "Rainman_MilCap_Splinter_Desert",
                  "Rainman_MilCap_Splinter_Multi",
                  "Rainman_PlateCarrier2_Splinter",
                  "Rainman_PlateCarrier2_Splinter_Urban",
                  "Rainman_PlateCarrier2_Splinter_Desert",
				  "Rainman_PlateCarrier2_Splinter_Winter",
                  "Rainman_PlateCarrier2_Splinter_Multi"
                  ];
private _weapons = [];
private _ammo = [];

_out set [A_RMS, [_backpacks, _items, _weapons, _ammo]];

//------------------------------------------------------------ Role loadouts RMS
/*
 [uniform, [[inUniform, qty]]],
 [vest, [[inVest, qty]]],
 [backpack, [[inBackpack, qty]]],
 [primWeapon, [primWeaponItems]]
 [secWeapon, [secWeapItems]],
 [handWeapon, [handWeapItems]],
 helmet, face, comm, terminal, map, bino, nv, watch, compass
*/

RL_RMS = A_RMS + 1;

private _hq = [[(["hq"] call rmSplinter_fnc_uniform), []],
               [(["hq"] call rmSplinter_fnc_vest), []],
               [(["hq"] call rmSplinter_fnc_backpack), []],
               [nil, []],
               [nil, []],
               [nil, []],
               (["hq"] call rmSplinter_fnc_helmet), nil, nil, nil, nil, nil, nil, nil, nil];
private _sl = [[(["sl"] call rmSplinter_fnc_uniform), []],
               [(["sl"] call rmSplinter_fnc_vest), []],
               [(["sl"] call rmSplinter_fnc_backpack), []],
               [nil, []],
               [nil, []],
               [nil, []],
               (["sl"] call rmSplinter_fnc_helmet), nil, nil, nil, nil, nil, nil, nil, nil];
private _tl = [[(["tl"] call rmSplinter_fnc_uniform), []],
               [(["tl"] call rmSplinter_fnc_vest), []],
               [(["tl"] call rmSplinter_fnc_backpack), []],
               [nil, []],
               [nil, []],
               [nil, []],
               (["tl"] call rmSplinter_fnc_helmet), nil, nil, nil, nil, nil, nil, nil, nil];
private _medic = [[(["medic"] call rmSplinter_fnc_uniform), []],
               	  [(["medic"] call rmSplinter_fnc_vest), []],
               	  [(["medic"] call rmSplinter_fnc_backpack), []],
               	  [nil, []],
                  [nil, []],
                  [nil, []],
                  (["medic"] call rmSplinter_fnc_helmet), nil, nil, nil, nil, nil, nil, nil, nil];
private _lmg = [[(["lmg"] call rmSplinter_fnc_uniform), []],
               [(["lmg"] call rmSplinter_fnc_vest), []],
               [(["lmg"] call rmSplinter_fnc_backpack), []],
               [nil, []],
               [nil, []],
               [nil, []],
               (["lmg"] call rmSplinter_fnc_helmet), nil, nil, nil, nil, nil, nil, nil, nil];
private _hmg = [[(["hmg"] call rmSplinter_fnc_uniform), []],
               [(["hmg"] call rmSplinter_fnc_vest), []],
               [(["hmg"] call rmSplinter_fnc_backpack), []],
               [nil, []],
               [nil, []],
               [nil, []],
               (["hmg"] call rmSplinter_fnc_helmet), nil, nil, nil, nil, nil, nil, nil, nil];
private _assHMG = [[(["assHMG"] call rmSplinter_fnc_uniform), []],
               [(["assHMG"] call rmSplinter_fnc_vest), []],
               [(["assHMG"] call rmSplinter_fnc_backpack), []],
               [nil, []],
               [nil, []],
               [nil, []],
               (["assHMG"] call rmSplinter_fnc_helmet), nil, nil, nil, nil, nil, nil, nil, nil];
private _aa = [[(["aa"] call rmSplinter_fnc_uniform), []],
               [(["aa"] call rmSplinter_fnc_vest), []],
               [(["aa"] call rmSplinter_fnc_backpack), []],
               [nil, []],
               [nil, []],
               [nil, []],
               (["aa"] call rmSplinter_fnc_helmet), nil, nil, nil, nil, nil, nil, nil, nil];
private _assAA = [[(["assAA"] call rmSplinter_fnc_uniform), []],
                  [(["assAA"] call rmSplinter_fnc_vest), []],
                  [(["assAA"] call rmSplinter_fnc_backpack), []],
                  [nil, []],
                  [nil, []],
                  [nil, []],
                  (["assAA"] call rmSplinter_fnc_helmet), nil, nil, nil, nil, nil, nil, nil, nil];
private _at = [[(["at"] call rmSplinter_fnc_uniform), []],
               [(["at"] call rmSplinter_fnc_vest), []],
               [(["at"] call rmSplinter_fnc_backpack), []],
               [nil, []],
               [nil, []],
               [nil, []],
               (["aaAA"] call rmSplinter_fnc_helmet), nil, nil, nil, nil, nil, nil, nil, nil];
private _assAT = [[(["assAT"] call rmSplinter_fnc_uniform), []],
                  [(["assAT"] call rmSplinter_fnc_vest), []],
                  [(["assAT"] call rmSplinter_fnc_backpack), []],
                  [nil, []],
                  [nil, []],
                  [nil, []],
                  (["assAT"] call rmSplinter_fnc_helmet), nil, nil, nil, nil, nil, nil, nil, nil];
private _sniper = [[(["sniper"] call rmSplinter_fnc_uniform), []],
                   [(["sniper"] call rmSplinter_fnc_vest), []],
                   [(["sniper"] call rmSplinter_fnc_backpack), []],
                   [nil, []],
               	   [nil, []],
                   [nil, []],
                   (["sniper"] call rmSplinter_fnc_helmet), nil, nil, nil, nil, nil, nil, nil, nil];
private _marksman = [[(["marksman"] call rmSplinter_fnc_uniform), []],
               		 [(["marksman"] call rmSplinter_fnc_vest), []],
               		 [(["marksman"] call rmSplinter_fnc_backpack), []],
               		 [nil, []],
               		 [nil, []],
               		 [nil, []],
               		 (["marksman"] call rmSplinter_fnc_helmet), nil, nil, nil, nil, nil, nil, nil, nil];
private _repair = [[(["repair"] call rmSplinter_fnc_uniform), []],
               	   [(["repair"] call rmSplinter_fnc_vest), []],
               	   [(["repair"] call rmSplinter_fnc_backpack), []],
               	   [nil, []],
               	   [nil, []],
               	   [nil, []],
               	   (["repair"] call rmSplinter_fnc_helmet), nil, nil, nil, nil, nil, nil, nil, nil];
private _demo = [[(["demo"] call rmSplinter_fnc_uniform), []],
               	 [(["demo"] call rmSplinter_fnc_vest), []],
                 [(["demo"] call rmSplinter_fnc_backpack), []],
                 [nil, []],
                 [nil, []],
                 [nil, []],
                 (["demo"] call rmSplinter_fnc_helmet), nil, nil, nil, nil, nil, nil, nil, nil];
private _engineer = [[(["engineer"] call rmSplinter_fnc_uniform), []],
               		 [(["engineer"] call rmSplinter_fnc_vest), []],
               		 [(["engineer"] call rmSplinter_fnc_backpack), []],
               		 [nil, []],
               		 [nil, []],
               		 [nil, []],
               		 (["engineer"] call rmSplinter_fnc_helmet), nil, nil, nil, nil, nil, nil, nil, nil];
private _grenadier = [[(["grenadier"] call rmSplinter_fnc_uniform), []],
               		  [(["grenadier"] call rmSplinter_fnc_vest), []],
               		  [(["grenadier"] call rmSplinter_fnc_backpack), []],
               		  [nil, []],
               		  [nil, []],
               		  [nil, []],
               		  (["grenadier"] call rmSplinter_fnc_helmet), nil, nil, nil, nil, nil, nil, nil, nil];
private _rifleman = [[(["rifleman"] call rmSplinter_fnc_uniform), []],
               		 [(["rifleman"] call rmSplinter_fnc_vest), []],
               		 [(["rifleman"] call rmSplinter_fnc_backpack), []],
               		 [nil, []],
               		 [nil, []],
               		 [nil, []],
               		 (["rifleman"] call rmSplinter_fnc_helmet), nil, nil, nil, nil, nil, nil, nil, nil];
private _jtac = [[(["jtac"] call rmSplinter_fnc_uniform), []],
               	 [(["jtac"] call rmSplinter_fnc_vest), []],
               	 [(["jtac"] call rmSplinter_fnc_backpack), []],
               	 [nil, []],
               	 [nil, []],
               	 [nil, []],
               	 (["jtac"] call rmSplinter_fnc_helmet), nil, nil, nil, nil, nil, nil, nil, nil];
private _hPilot = [[(["hPilot"] call rmSplinter_fnc_uniform), []],
               	   [(["hPilot"] call rmSplinter_fnc_vest), []],
               	   [(["hPilot"] call rmSplinter_fnc_backpack), []],
               	   [nil, []],
               	   [nil, []],
               	   [nil, []],
               	   (["hPilot"] call rmSplinter_fnc_helmet), nil, nil, nil, nil, nil, nil, nil, nil];
private _jPilot = [[(["jPilot"] call rmSplinter_fnc_uniform), []],
               	   [(["jPilot"] call rmSplinter_fnc_vest), []],
               	   [(["jPilot"] call rmSplinter_fnc_backpack), []],
               	   [nil, []],
               	   [nil, []],
               	   [nil, []],
               	   (["jPilot"] call rmSplinter_fnc_helmet), nil, nil, nil, nil, nil, nil, nil, nil];
private _crew = [[(["crew"] call rmSplinter_fnc_uniform), []],
               	 [(["crew"] call rmSplinter_fnc_vest), []],
               	 [(["crew"] call rmSplinter_fnc_backpack), []],
               	 [nil, []],
               	 [nil, []],
               	 [nil, []],
               	 (["crew"] call rmSplinter_fnc_helmet), nil, nil, nil, nil, nil, nil, nil, nil];
private _mortar = [[(["mortar"] call rmSplinter_fnc_uniform), []],
               	   [(["mortar"] call rmSplinter_fnc_vest), []],
               	   [(["mortar"] call rmSplinter_fnc_backpack), []],
                   [nil, []],
                   [nil, []],
                   [nil, []],
                   (["mortar"] call rmSplinter_fnc_helmet), nil, nil, nil, nil, nil, nil, nil, nil];
private _uavOp = [[(["uavOp"] call rmSplinter_fnc_uniform), []],
               	  [(["uavOp"] call rmSplinter_fnc_vest), []],
               	  [(["uavOp"] call rmSplinter_fnc_backpack), []],
               	  [nil, []],
               	  [nil, []],
               	  [nil, []],
               	  (["uavOp"] call rmSplinter_fnc_helmet), nil, nil, nil, nil, nil, nil, nil, nil];
private _spotter = [[(["spotter"] call rmSplinter_fnc_uniform), []],
               		[(["spotter"] call rmSplinter_fnc_vest), []],
               		[(["spotter"] call rmSplinter_fnc_backpack), []],
               		[nil, []],
               		[nil, []],
               		[nil, []],
               		(["spotter"] call rmSplinter_fnc_helmet), nil, nil, nil, nil, nil, nil, nil, nil];

_out set [RL_RMS, [_hq, _sl, _tl, _medic, _lmg, _hmg, _assHMG, _aa, _assAA, _at, _assAT, _sniper, _marksman,
                   _repair, _demo, _engineer, _grenadier, _rifleman, _jtac, _hPilot, _jPilot, _crew, 
                   _mortar, _uavOp, _spotter]];

//------------------------------------------------------------ Base Atmosphere Role Loadout rmSplinter

BALO_RMS = RL_RMS + 1;

private _medic = [[(["medic"] call rmSplinter_fnc_uniform), []],
                  [(["medic"] call rmSplinter_fnc_vest), []],
                  [nil, []],
                  [nil, []],
                  [nil, []],
                  [nil, []],
                  nil, nil, nil, nil, nil, nil, nil, nil, nil];
private _gear = [[(["hq"] call rmSplinter_fnc_uniform), []],
                 [nil, []],
                 [nil, []],
                 [nil, []],
                 [nil, []],
                 [nil, []],
                 nil, nil, nil, nil, nil, nil, nil, nil, nil];
private _support = [[(["hq"] call rmSplinter_fnc_uniform), []],
                    [nil, []],
                    [nil, []],
                    [nil, []],
                    [nil, []],
                    [nil, []],
                    nil, nil, nil, nil, nil, nil, nil, nil, nil];
private _default = [[(["hq"] call rmSplinter_fnc_uniform), []],
                    [nil, []],
                    [nil, []],
                    [nil, []],
                    [nil, []],
                    [nil, []],
                    nil, nil, nil, nil, nil, nil, nil, nil, nil];

_out set [BALO_RMS, [_medic, _gear, _support, _default]];

//------------------------------------------------------------ FINITO, return

_out