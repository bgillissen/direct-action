class rmSplinter {
	tag = "rmSplinter";
	class functions {
		class assets { file="feats\assets\rmSplinter\assets.sqf"; };
		class init { file="feats\assets\rmSplinter\init.sqf"; };
		class postInit { file="core\dummy.sqf"; };
		class preInit { file="feats\assets\rmSplinter\preInit.sqf"; };
		class uniform { file="feats\assets\rmSplinter\uniform.sqf"; };
		class vest { file="feats\assets\rmSplinter\vest.sqf"; };
		class backpack { file="feats\assets\rmSplinter\backpack.sqf"; };
		class helmet { file="feats\assets\rmSplinter\helmet.sqf"; };
	};
};
