/*
@filename: feats\assets\rhs\USAF\vest.sqf
Author:
	Ben
Description:
	run on server
	call by feats\assets\rhs\USAF\assets.sqf
	return a RHS USAF vest depending on given role and MAP_KEYWORDS
Params:
	STRING	unit role
Environment:
	missionNamespace:
		MAP_KEYWORDS
Return:
	STRING	a vest classname
*/
params ["_role"];

if ( _role in ["hq", "hPilot", "jPilot"] ) exitWith { nil };

if ( "desert" in MAP_KEYWORDS ) exitWith {
	"Rainman_PlateCarrier2_Splinter_Desert"	
};
if ( "jungle" in MAP_KEYWORDS ) exitWith {
	"Rainman_PlateCarrier2_Splinter"
};
if ( ("dry" in MAP_KEYWORDS) || ("wood" in MAP_KEYWORDS) ) exitWith {
	"Rainman_PlateCarrier2_Splinter_Multi"
};
"Rainman_PlateCarrier2_Splinter_Winter"