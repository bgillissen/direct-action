/*
@filename: feats\assets\rmSplinter\helmet.sqf
Author:
	Ben
Description:
	run on server
	call by feats\assets\rmSplinter\assets.sqf
	return a rmSplinter helmet depending on given role and MAP_KEYWORDS
Params:
	STRING	unit role
Environment:
	missionNamespace:
		MAP_KEYWORDS
Return:
	STRING	a helmet classname
*/
params ["_role"];

if ( _role in ["hq", "hPilot", "jPilot", "crew"] ) exitWith { nil };

if ( "desert" in MAP_KEYWORDS ) exitWith {
    if ( _role in ["marksman", "spotter", "sniper"] ) exitWith { "Rainman_Booniehat_Splinter_Desert" };
	"Rainman_HelmetB_Light_Splinter_Desert"
};
if ( "jungle" in MAP_KEYWORDS ) exitWith {
	if ( _role in ["marksman", "spotter", "sniper"] ) exitWith { "Rainman_Booniehat_Splinter" };
	"Rainman_HelmetB_Light_Splinter"
};
if ( ("dry" in MAP_KEYWORDS) || ("wood" in MAP_KEYWORDS) ) exitWith {
	if ( _role in ["marksman", "spotter", "sniper"] ) exitWith { "Rainman_Booniehat_Splinter_Multi" };
	"Rainman_HelmetB_Light_Splinter_Multi"
};
if ( _role in ["marksman", "spotter", "sniper"] ) exitWith { "Rainman_Booniehat_Splinter_Winter" };
"Rainman_HelmetB_Light_Splinter_Winter"