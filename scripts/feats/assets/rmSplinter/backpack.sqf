/*
@filename: feats\assets\rmSplinter\backpack.sqf
Author:
	Ben
Description:
	run on server
	call by feats\assets\rmSplinter\assets.sqf
	return a rmSplinter backpack depending on given role and MAP_KEYWORDS
Params:
	STRING	unit role
Environment:
	missionNamespace:
		MAP_KEYWORDS
Return:
	STRING	an uniform classname
*/
params ["_role"];

if ( _role in ["jPilot", "hq", "marksman", "sniper", "rifleman", "uavOp", "grenadier", "hPilot", "mortar", "spotter"]) exitWith { nil };

if ( ("desert" in MAP_KEYWORDS) ) exitWith {
	"Rainman_Kitbag_Splinter_Desert"
};
if ( ("jungle" in MAP_KEYWORDS) ) exitWith {
	"Rainman_Kitbag_Splinter"
};
if ( ("dry" in MAP_KEYWORDS) || ("wood" in MAP_KEYWORDS) ) exitWith {
	"Rainman_Kitbag_Splinter_Multi"
};

"Rainman_Kitbag_Splinter_Winter"