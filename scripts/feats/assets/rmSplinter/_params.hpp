
class rmSplinter_modrl {
	title = "Player spawn with rmSplinter loadout when RHS (player side) is present";
	values[] = {0,1};
	texts[] = {"no", "yes"};
	default = 0;
};
class rmSplinter_modbalo {
	title = "base NPC spawn with rmSplinter loadout when RHS (player side) is present";
	values[] = {0,1};
	texts[] = {"no", "yes"};
	default = 0;
};
class rl_rmSplinter {
	title = "Players spawn with rmSplinter loadout";
	values[] = {0,1};
	texts[] = {"no", "yes"};
	default = 1;
};
class balo_rmSplinter {
	title = "base NPC spawn with rmSplinter loadout";
	values[] = {0,1};
	texts[] = {"no", "yes"};
	default = 1;
};
