/*
@filename: feats\assets\serverInit.sqf
Author:
	Ben
Description:
	run on server
	check which mods are available and init them,
	broadcast assets list to players
*/

#include "_debug.hpp"

{
	_x params ["_name", "_isPresent", "_sides"];
	if ( _isPresent ) then {
		private _fnc =  format["%1_fnc_init", _name];
		if ( !isNil _fnc ) then {
			private _code = compile format["_this call %1", _fnc];
			#ifdef DEBUG			
			private _debug = format["%1 init", _name];
			debug(LL_DEBUG, _debug);
			#endif
			[] call _code;
		#ifdef DEBUG
		} else {
			private _debug = format["%1 init failed, function %2 is not defined", _name, _fnc];
			debug(LL_ERR, _debug);
		#endif
		};
	};
} forEach MODS;

//broadcast computed assets to clients
{
	_x params ["_prefix", "_vars", "_broadcast"];
	if ( _broadcast ) then {
		{
            if ( _prefix in ["RL", "BALO"] ) then {
                private _v = _x;
            	{ publicVariable format["%1_%2_%3", _prefix, _v, _x]; } forEach [west, east, independent];    
            } else {
				publicVariable format["%1_%2", _prefix, _x];
			};
		} forEach _vars;
	};
} forEach PV;

nil