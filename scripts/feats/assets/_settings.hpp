class assets {
	class winter2035 {
		required = 1;
		cfgPatch = "winter_2035";
	};
	class cba {
		name = "cba3";
		required = 0;
		cfgPatch = "cba_main";
	};
	class ace {
		required = 1;
		cfgPatch = "ace_main";
	};
	class advSlingLoading {};
	class advTowing {};
	class directAction {
		required = 1;
		cfgPatch = "TFUDA";
	};
	class vanilla {};
	class kart {};
	class heli {};
	class apex {};
	class marksmen {};
	class jet {};
	class orange {};
	class tanks {};
	class achilles {
		clientMod = 1;
		cfgPatch = "achilles_modules_f_achilles";
		features[] = {"arsenal",
					  "artiSupport",
					  "autoFlares",
					  "chopperDamage",
					  "gearSave",
					  "parameters",
					  "revive",
					  "spawnVehicle",
					  "supplyDrop",
					  "switchSide",
					  "trappedAnimal",
					  "uavRecon",
					  "hostage",
					  "zeusCompo", 
					  "vcomai" };
	};
	class shacktac {
		required = 0;
		clientMod = 1;
		cfgPatch = "STUI_Core";
	};
	class afrf {
		required = 1;
		name = "rhsAFRF";
		cfgPatch = "rhs_main";
	};
	class gref {
		required = 1;
		name = "rhsGREF";
		cfgPatch = "rhsgref_main";
	};
	class usaf {
		required = 1;
		name = "rhsUSAF";
		cfgPatch = "rhsusf_main";
	};
	class uk3cbEquip {
		required = 1;
		cfgPatch = "UK3CB_BAF_Equipment";
	};
	class uk3cbVeh {
		required = 1;
		cfgPatch = "UK3CB_BAF_Vehicles";
	};
	class uk3cbWeap {
		required = 1;
		cfgPatch = "UK3CB_BAF_Weapons";
	};
	class rmSplinter {
		required = 1;
		cfgPatch = "Splinter_camo_addon";
	};
	class tfar {
		required = 1;
		cfgPatch = "task_force_radio";
	};
	class adr97 {
		required = 1;
		cfgPatch = "A3_Weapons_F_Mod";
	};
	class unsung {
		required = 1;
		cfgPatch = "uns_main";
	};
	class tfuBerets {
		required = 1;
		cfgPatch = "insignia_addon";
	};
};

#include "tfar\_settings.hpp"
