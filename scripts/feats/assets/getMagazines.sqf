params [["_dlcFilter", 0], ["_extFilters", []]];

private _filters = [_dlcFilter, _extFilters] call assets_fnc_magazineFilters;

private _mags = [];

["CfgMagazines", _filters, "[_this select 0, _this select 1] call assets_fnc_addMagazine", _mags] call assets_fnc_searchThings;

/*
diag_log "MAGAZINES >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>";
{ 
	diag_log format["%1 => %2", _x, ( getText(configFile >> "CfgMagazines" >> _x >> "displayName"))];
} count _mags;
diag_log "MAGAZINES <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<";
*/

_mags