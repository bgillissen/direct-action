class ar_rappel_all_cargo {
	allowedTargets = 0;
	jip = 0;
};
class ar_hint {
	allowedTargets = 1;
	jip = 0;
};
class ar_rappel_from_heli {
	allowedTargets = 2;
	jip = 0;
};
class ar_hide_object_global {
	allowedTargets = 2;
	jip = 0;
};
class ar_play_rappelling_sounds_global {
	allowedTargets = 2;
	jip = 0;
};
class ar_client_rappel_from_heli {
	allowedTargets = 0;
	jip = 0;
};
class ar_enable_rappelling_animation {
	allowedTargets = 2;
	jip = 0;
};
