/*
@filename: feats\assets\getWeaponMagazines.sqf
Author:
	Ben
Description:
	run on server
	it is used to get all the magazines compatible with a weapon
	params:  a list of weapon's classnames
*/

#include "_debug.hpp";

private _mags = [];
{
	#ifdef DEBUG
	private _debug = format["Adding magazine for weapon %1", _x];
	debug(LL_DEBUG, _debug);
	#endif
	//first the default mags
	_nul = _mags append getArray(configFile >> "cfgWeapons" >> _x >> "magazines");
	//then the magazineWells
	{
		private _magWell = (configFile >> "cfgMagazineWells" >> _x);
		for "_i" from 0 to count _magWell - 1 do {
			{ _nul = _mags pushBackUnique _x; } foreach getArray(_magWell select _i);
		};
	} foreach getArray(configFile >> "cfgWeapons" >> _x >> "magazineWell");
} foreach _this;

_mags