class adr97 {
	title = "A required mod is missing.";
	description = "ADR-97 Weapon Pack (Official Mod) is not loaded. Please make sure that you load all the mods in the required list when you join this server from the launcher. The missing mod can be found here: http://steamcommunity.com/sharedfiles/filedetails/?id=669962280";
	picture = "";
};
