class asets {
	tag = "assets";
	class functions {
		class serverPreInit { file="feats\assets\serverPreInit.sqf"; };
		class serverInit { file="feats\assets\serverInit.sqf"; };
		class playerPostInit { file="feats\assets\playerPostInit.sqf"; };

		class addBaseWeapons { file="feats\assets\addBaseWeapons.sqf"; };
		class addMagazine { file="feats\assets\addMagazine.sqf"; };
		class addWeapon { file="feats\assets\addWeapon.sqf"; };
		class baseWeapon { file="feats\assets\baseWeapon.sqf"; };
		class getBackpacks { file="feats\assets\getBackpacks.sqf"; };
		class getItems { file="feats\assets\getItems.sqf"; };
		class getMagazines { file="feats\assets\getMagazines.sqf"; };
		class getWeaponMagazines { file="feats\assets\getWeaponMagazines.sqf"; };
		class getVehicles { file="feats\assets\getVehicles.sqf"; };
		class getWeapons { file="feats\assets\getWeapons.sqf"; };
		class vehicleFilters { file="feats\assets\vehicleFilters.sqf"; };
		class weaponFilters { file="feats\assets\weaponFilters.sqf"; };
		class magazineFilters { file="feats\assets\magazineFilters.sqf"; };
		class searchThings { file="feats\assets\searchThings.sqf"; };

		class condition { file="feats\assets\condition.sqf"; };
		class implent { file="feats\assets\implent.sqf"; };
		class implentAllowedVehicle { file="feats\assets\implentAllowedVehicle.sqf"; };
		class implentArsenal { file="feats\assets\implentArsenal.sqf"; };
		class implentBaseVehicle { file="feats\assets\implentBaseVehicle.sqf"; };
		class implentCond { file="feats\assets\implentCond.sqf"; };
		class implentRestrictedGear { file="feats\assets\implentRestrictedGear.sqf"; };
		class implentReward { file="feats\assets\implentReward.sqf"; };
		class implentRoleLoadout { file="feats\assets\implentRoleLoadout.sqf"; };
		class implentSpawn { file="feats\assets\implentSpawn.sqf"; };
		class implentSupplyDrop { file="feats\assets\implentSupplyDrop.sqf"; };
		class implentVehicleCargo { file="feats\assets\implentVehicleCargo.sqf"; };

		class mapKeywords { file="feats\assets\mapKeywords.sqf"; };
	};
};

#include "ace\_cfgFunctions.hpp"
#include "achilles\_cfgFunctions.hpp"
#include "adr97\_cfgFunctions.hpp"
#include "advTowing\_cfgFunctions.hpp"
#include "advSlingLoading\_cfgFunctions.hpp"
#include "bohemia\_cfgFunctions.hpp"
#include "directAction\_cfgFunctions.hpp"
#include "rhs\_cfgFunctions.hpp"
#include "rmSplinter\_cfgFunctions.hpp"
#include "shacktac\_cfgFunctions.hpp"
#include "tfar\_cfgFunctions.hpp"
#include "tfuBerets\_cfgFunctions.hpp"
#include "uk3cb\_cfgFunctions.hpp"
#include "unsung\_cfgFunctions.hpp"
#include "winter2035\_cfgFunctions.hpp"
