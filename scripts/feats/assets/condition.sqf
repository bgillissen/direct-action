/*
@filename: fates\mods\condition.sqf
Author:
	Ben
Description:
	run on server
	it helps to find out if something got to be implented
*/

#include "_debug.hpp"

params ["_type", "_confName", "_sides", "_mods", "_isEnemy"];

private _confParam = format["%1_%2", _type, _confName];

if ( !isClass(missionConfigFile >> "Params" >> _confParam) ) exitWith {
    #ifdef DEBUG
	private _debug = format["condition, %1 isNil => true", _confParam];
    debug(LL_DEBUG, _debug);
    #endif
	true 
};

private _conf = [_confParam] call core_fnc_getParam;

if ( _conf isEqualTo 0 ) exitWith {
    #ifdef DEBUG
	private _debug = format["condition, %1 is 0 => false", _confParam];
	debug(LL_DEBUG, _debug);
    #endif
	false 
};

private _out = true;

if ( _conf isEqualTo 1 ) then {
	if ( _isEnemy ) then {
		_out = ( ({_x in ENEMIES} count _sides) > 0 ); 
	} else {
		_out = ( PLAYER_SIDE in _sides );
	};
};
if ( !_out ) exitWith {
	#ifdef DEBUG
	private _debug = format["condition, %1 is 1 (%2) => false", _confParam, _isEnemy];
    debug(LL_DEBUG, _debug);
    #endif
	false 
};

if ( _conf isEqualTo 2 ) then {
	_out = ( ({_x in ALLIES} count _sides) > 0 );
};
if !( _out ) exitWith {
	#ifdef DEBUG
	private _debug = format["condition, %1 is 2 => false", _confParam];
    debug(LL_DEBUG, _debug);
    #endif
	false 
};

if ( (count _mods) == 0 ) exitWith {
	#ifdef DEBUG
	private _debug = format["condition, %1 is %2, mods is empty => true", _confParam, _conf];
    debug(LL_DEBUG, _debug);
    #endif
	true
};

private "_modConfName";

if ( _isEnemy ) then {
    _modConfName = format["%1_mod%2", _confName, "Enemy"];
} else {
    _modConfName = format["%1_mod%2", _confName, _type];
}; 

if !( isClass(missionConfigFile >> "Params" >> _modConfName) ) exitWith {
	#ifdef DEBUG
	private _debug = format["condition, %1 isNil => true", _modConfName];
    debug(LL_DEBUG, _debug);
    #endif
	true 
};

if ( ([_modConfName] call core_fnc_getParam) isEqualTo 1 ) exitWith {
	#ifdef DEBUG
	private _debug = format["condition, %1 is 1 => true", _modConfName];
    debug(LL_DEBUG, _debug);
    #endif
	true 
};	

{
    _x params ["_mod", "_isPresent"];
	if ( _isPresent ) then {
        if ( _mod in _mods ) exitWith { _out = false };
	};
    if !( _out ) exitWith {
    	#ifdef DEBUG
		private _debug = format["condition, %1, excluded by %2", _confParam, _mod];
    	debug(LL_DEBUG, _debug);
    	#endif    
    };
} count MODS;


#ifdef DEBUG
private _debug = format["condition, %1 is %2, %3 is 0 => %4", _confParam, _conf, _modconfName, _out];
debug(LL_DEBUG, _debug);
#endif

_out