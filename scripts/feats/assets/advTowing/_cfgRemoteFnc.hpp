class sa_set_owner {
	allowedTargets = 2;
	jip = 0;
};
class sa_simulate_towing {
	allowedTargets = 0;
	jip = 0;
};
class sa_hint {
	allowedTargets = 0;
	jip = 0;
};
class sa_hide_object_global {
	allowedTargets = 2;
	jip = 0;
};
class sa_attach_tow_ropes {
	allowedTargets = 0;
	jip = 0;
};
class sa_take_tow_ropes {
	allowedTargets = 0;
	jip = 0;
};
class sa_pickup_tow_ropes {
	allowedTargets = 0;
	jip = 0;
};
class sa_drop_tow_ropes {
	allowedTargets = 0;
	jip = 0;
};
class sa_put_away_tow_ropes {
	allowedTargets = 0;
	jip = 0;
};
