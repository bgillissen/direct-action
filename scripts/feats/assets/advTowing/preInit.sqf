/*
@filename: feats\assets\advTowing\preInit.sqf
Author:
	Ben
Description:
	run on server,
	configure Advanced Towing
*/

//Advanced Towing
missionNamespace setVariable ["SA_MAX_TOWED_CARGO", 1, true];														 
missionNamespace setVariable ["SA_TOW_SUPPORTED_VEHICLES_OVERRIDE", ["Car", "Truck", "Tank", "APC,", "IFV", "Ship"], true];														 
missionNamespace setVariable ["SA_TOW_RULES_OVERRIDE", [["Car", 	"CAN_TOW", "Car"],
														["Car", 	"CAN_TOW", "Plane"],
														["Tank", 	"CAN_TOW", "Car"],
														["Tank", 	"CAN_TOW", "Tank"],
                                                        ["Tank", 	"CAN_TOW", "Truck"], 
                                                        ["Tank", 	"CAN_TOW", "APC"],
                                                        ["Tank", 	"CAN_TOW", "IFV"],
														["Truck", 	"CAN_TOW", "Truck"], 
														["Truck", 	"CAN_TOW", "Car"], 
														["Ship", 	"CAN_TOW", "Ship"],
                                                        ["IFV", 	"CAN_TOW", "Truck"], 
                                                        ["IFV", 	"CAN_TOW", "Car"],
                                                        ["IFV", 	"CAN_TOW", "IFV"],
                                                        ["IFV", 	"CAN_TOW", "APC"],
                                                        ["APC", 	"CAN_TOW", "Truck"], 
                                                        ["APC", 	"CAN_TOW", "Car"],
                                                        ["APC", 	"CAN_TOW", "APC"]
													   ], true];