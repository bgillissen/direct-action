class assets: feat_base  {
	class server : featServer_base {
		class preInit : featEvent_enable { order=12; };
		class init : featEvent_enable { order=12; };
	};
	class player : featPlayer_base {
		class postInit : featEvent_enable { thread = 1; };
	};
};
