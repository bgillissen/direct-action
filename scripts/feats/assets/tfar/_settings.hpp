class tfar {
	terrainInterception = 0;
	srvName = "Task Force Unicorn";
	seriousMode = 1;
	dftChannel = "public";
	prvChannel = "member";
	class channels {
		class public {
			private = 0;
			name = "Direct Action Server";
		};
		class member {
			private = 1;
			name = "Member Area";
			passVarName = "TFU_mbrChanPass";
		};
		class modding {
			private = 0;
			name = "Modding";
		};
		class officers {
			private = 0;
			name = "Officers";
		};
		class pvp {
			private = 0;
			name = "PvP";
		};
	};
};
