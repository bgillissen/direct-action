/*
@filename: feats\assets\tfar\IND\assets.sqf
Author:
	Ben
Description:
	run on server
	return IND TFAR assets
*/

private _out = [];

//------------------------------------------------------------ Arsenal TFAR IND

A_TFARI = 0;

private _backpacks = ["tf_anarc210",
                      "tf_anprc155",
                      "tf_anprc155_coyote"];
private _items = ["tf_anprc148jem"];
private _weapons = [];
private _ammo = [];

_out set [A_TFARI, [_backpacks, _items, _weapons, _ammo]];
//------------------------------------------------------------ supplyDrop TFAR IND

SD_TFARI = A_TFARI + 1;

private _backpacks = [];
private _items = [["tf_anprc148jem", 3]];
private _weapons = [];
private _ammo = [];
private _crates = [];
private _vehicles = [];

_out set [SD_TFARI, [_backpacks, _items, _weapons, _ammo, _crates, _vehicles]];

//------------------------------------------------------------ Vehicles Cargo TFAR IND

VC_TFARI = SD_TFARI + 1;

private _car = [[], [["tf_anprc148jem", 3]], [], []];
private _carArmed = [[], [["tf_anprc148jem", 3]], [], []];
private _apc = [[], [["tf_anprc148jem", 3]], [], []];
private _tank = [[], [["tf_anprc148jem", 3]], [], []];
private _aaTank = [[], [["tf_anprc148jem", 3]], [], []];
private _planeCAS = [[],[],[],[]];
private _planeAA = [[],[],[],[]];
private _planeTransport = [[],[],[],[]];
private _uav = [[],[],[],[]];
private _heliSmall = [[], [["tf_anprc148jem", 3]], [], []];
private _heliSmallArmed = [[], [["tf_anprc148jem", 3]], [], []];
private _heliMedium = [[], [["tf_anprc148jem", 3]], [], []];
private _heliMedEvac = [[], [["tf_anprc148jem", 3]], [], []];
private _heliBig = [[], [["tf_anprc148jem", 3]], [], []];
private _heliAttack = [[],[],[],[]];
private _boatSmall = [[],[],[],[]];
private _boatAttack = [[], [["tf_anprc148jem", 3]], [], []];
private _boatBig = [[], [["tf_anprc148jem", 3]], [], []];
private _sub = [[],[],[],[]];
private _landMedic = [[], [["tf_anprc148jem", 3]], [], []];
private _repair = [[],[],[],[]];
private _fuel = [[],[],[],[]];
private _ammo = [[],[],[],[]];
private _truck = [[], [["tf_anprc148jem", 3]], [], []];
private _quad = [[], [["tf_anprc148jem", 1]], [], []];
private _artiTank = [[],[],[],[]];
private _artiCannon = [[],[],[],[]];
private _artiTube = [[],[],[],[]];

_out set [VC_TFARI, [_car, _carArmed, _apc, _tank, _aaTank, _planeCAS, _planeAA, _planeTransport, _uav,
                 _heliSmall, _heliSmallArmed, _heliMedium, _heliMedEvac, _heliBig, _heliAttack,
                 _boatSmall, _boatAttack, _boatBig, _sub, _landMedic, _repair, _fuel, _ammo, _truck, _quad,
                 _artiTank, _artiCannon, _artiTube]];
                 
//------------------------------------------------------------ Role Loadout TFAR IND
/*
 [uniform, [inUniform]], 
 [vest, inVest]], 
 [backpack, [inBackpack]], 
 [primWeapon, [muzzle, bipod, acc, scope, ammo2, ammo]] 
 [secWeapon, [muzzle, bipod, acc, scope, ammo]], 
 [handWeapon, [muzzle, bipod, acc, scope, ammo]], 
 helmet, face, comm, terminal, map, bino, nv, watch, compass
*/

RL_TFARI = VC_TFARI + 1;

private _buff = [];
{
    _buff pushback [[nil, []], 
 					[nil, []], 
 					[([_x] call tfarI_fnc_backpack), []], 
 					[nil, []], 
 					[nil, []], 
 					[nil, []], 
 					nil, nil, "tf_anprc148jem", nil, nil, nil, nil, nil, nil]; 
} forEach ((PV select RL_k) select 1);

_out set [RL_TFARI, _buff];

//------------------------------------------------------------ FINITO, return
          
_out