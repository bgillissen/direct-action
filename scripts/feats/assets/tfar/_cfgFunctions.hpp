class tfar {
	tag = "tfar";
	class functions {
		//class blufor { file="feats\assets\tfar\blufor.sqf"; };
		//class ind { file="feats\assets\tfar\ind.sqf"; };
		class init { file="feats\assets\tfar\init.sqf"; };
		//class lrByRole { file="feats\assets\tfar\lrByRole.sqf"; };
		//class opfor { file="feats\assets\tfar\opfor.sqf"; };
		class postInit { file="feats\assets\tfar\postInit.sqf"; };
		class preInit { file="feats\assets\tfar\preInit.sqf"; };
		class setChannel { file="feats\assets\tfar\setChannel.sqf"; };
		class channelUpdate { file="feats\assets\tfar\channelUpdate.sqf"; };
		class xehPreInit {
			file="feats\assets\tfar\xehPreInit.sqf";
			preInit = 1;
		};
		//tfar Fix
		class backpacklr { file="feats\assets\tfar\fix\backpacklr.sqf"; };
		class calcTerrainInterception { file="feats\assets\tfar\fix\calcTerrainInterception.sqf"; };
		class clientInit { file="feats\assets\tfar\fix\clientInit.sqf"; };
		class currentDirection { file="feats\assets\tfar\fix\currentDirection.sqf"; };
		class defaultPositionCoordinates { file="feats\assets\tfar\fix\defaultPositionCoordinates.sqf"; };
		class eyeDepth { file="feats\assets\tfar\fix\eyeDepth.sqf"; };
		class getDefaultRadioClasses { file="feats\assets\tfar\fix\getDefaultRadioClasses.sqf"; };
		class getNearPlayers { file="feats\assets\tfar\fix\getNearPlayers.sqf"; };
		class preparePositionCoordinates {  file="feats\assets\tfar\fix\preparePositionCoordinates.sqf"; };
		class requestRadios { file="feats\assets\tfar\fix\requestRadios.sqf"; };
		class serverInit { file="feats\assets\tfar\fix\serverInit.sqf"; };
		class vehicleId { file="feats\assets\tfar\fix\vehicleId.sqf"; };
	};
};

#include "BLUFOR\_cfgFunctions.hpp"
#include "IND\_cfgFunctions.hpp"
#include "OPFOR\_cfgFunctions.hpp"
