/*
@filename: feats\assets\tfar\xehPreInit.sqf
Author:
	Ben
Description:
	run on server,
	triggered by game engine at preInit,
	set some TFAR public variables settings,
	define default ts channel,
	if serious mode is on 
*/

//so player does not spawn with longrange radio as backpacks
missionNamespace setVariable ["tf_no_auto_long_range_radio", true, false];

//so player does not have radio automaticaly in their inventory
missionNamespace setVariable ["tf_give_personal_radio_to_regular_soldier", false, false];

//To reduce the terrain interception coefficient
missionNamespace setVariable ["TF_terrain_interception_coefficient", getNumber( missionConfigFile >> "settings" >> "tfar" >> "terrainInterception"), false];

//so players does not have microDAGR in their inventory
missionNamespace setVariable ["TF_give_microdagr_to_soldier_server", false, false];
missionNamespace setVariable ["TF_give_microdagr_to_soldier", false, false];

if !( CTXT_SERVER ) exitWith {};

TFAR_channel = (["tfar", "dftChannel"] call core_fnc_getSetting);
publicVariable "TFAR_channel";

if ( (["tfar", "seriousMode"] call core_fnc_getSetting) == 0 ) exitWith {};

//To enforce TS channel if seriousMode is enabled 
false call tfar_fnc_channelUpdate;

TFAR_serverChanPVEH = ["TFAR_channel", { true call tfar_fnc_channelUpdate }] call pveh_fnc_add;

TFAR_serverConnEH = addMissionEventHandler ["PlayerConnected", {
    if ( getNumber( missionConfigFile >> "settings" >> "tfar" >> "channels" >> TFAR_channel >> "private" ) isEqualTo 0 ) exitWith {};
    params ["_id", "_uid", "_name", "_jip", "_owner"];    
    {
		if ( _uid isEqualTo (_x select 0) ) exitWith { 
			_owner publicVariableClient "tf_radio_channel_password"; 
		}; 
	} forEach memberData;
}];


