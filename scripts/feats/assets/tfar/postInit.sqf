/*
@filename: feats\assets\tfar\postInit.sqf
Author:
	Ben
Description:
	run on player,
	set some unit variable settings
*/

player setVariable ["tf_sendingDistanceMultiplicator", 10, true];
player setVariable ["tf_receivingDistanceMultiplicator", 10, true];

TF_defaultWestPersonalRadio = "tf_anprc152";
TF_defaultWestRiflemanRadio = "tf_anprc152";

TF_defaultEastPersonalRadio = "tf_fadak";
TF_defaultEastRiflemanRadio = "tf_fadak";

TF_defaultGuerPersonalRadio = "tf_anprc148jem";
TF_defaultGuerRiflemanRadio = "tf_anprc148jem";

true