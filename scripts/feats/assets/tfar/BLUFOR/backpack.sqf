
params["_role"];

if !( _role in ["hq", "sl", "tl", "jtac", "uavOp"] ) exitWith { nil };

if ( _role isEqualTo "hq" ) exitWith { "tf_rt1523g_bwmod" };

if ( ("wood" in MAP_KEYWORDS) || ("jungle" in MAP_KEYWORDS) ) exitWith { "tf_rt1523g_big_bwmod" };

"tf_rt1523g_big_rhs"
