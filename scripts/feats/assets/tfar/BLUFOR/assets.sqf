/*
@filename: feats\assets\tfar\BLUFOR\assets.sqf
Author:
	Ben
Description:
	run on server
	return BLUFOR TFAR assets
*/

private _out = [];

//------------------------------------------------------------ Arsenal TFAR BLUFOR

A_TFARB = 0;

private _backpacks = ["tf_anarc210",
                      "tf_rt1523g", 
				 	  "tf_rt1523g_big",
				 	  "tf_rt1523g_big_bwmod",
				 	  "tf_rt1523g_big_bwmod_tropen",
				 	  "tf_rt1523g_big_rhs",
				 	  "tf_rt1523g_black",
				 	  "tf_rt1523g_bwmod",
				 	  "tf_rt1523g_fabric",
				 	  "tf_rt1523g_green",
				 	  "tf_rt1523g_rhs",
				 	  "tf_rt1523g_sage"];
private _items = ["tf_anprc152"];
private _weapons = [];
private _ammo = [];

_out set [A_TFARB, [_backpacks, _items, _weapons, _ammo]];

//------------------------------------------------------------ supplyDrop TFAR BLUFOR

SD_TFARB = A_TFARB + 1;

private _backpacks = [];
private _items = [["tf_anprc152", 3]];
private _weapons = [];
private _ammo = [];
private _crates = [];
private _vehicles = [];

_out set [SD_TFARB, [_backpacks, _items, _weapons, _ammo, _crates, _vehicles]];

//------------------------------------------------------------ Vehicles Cargo TFAR BLUFOR

VC_TFARB = SD_TFARB + 1;

private _car = [[], [["tf_anprc152", 3]], [], []];
private _carArmed = [[], [["tf_anprc152", 3]], [], []];
private _apc = [[], [["tf_anprc152", 3]], [], []];
private _tank = [[], [["tf_anprc152", 3]], [], []];
private _aaTank = [[], [["tf_anprc152", 3]], [], []];
private _planeCAS = [[],[],[],[]];
private _planeAA = [[],[],[],[]];
private _planeTransport = [[],[],[],[]];
private _uav = [[],[],[],[]];
private _heliSmall = [[], [["tf_anprc152", 3]], [], []];
private _heliSmallArmed = [[], [["tf_anprc152", 3]], [], []];
private _heliMedium = [[], [["tf_anprc152", 3]], [], []];
private _heliMedEvac = [[], [["tf_anprc152", 3]], [], []];
private _heliBig = [[], [["tf_anprc152", 3]], [], []];
private _heliAttack = [[],[],[],[]];
private _boatSmall = [[],[],[],[]];
private _boatAttack = [[], [["tf_anprc152", 3]], [], []];
private _boatBig = [[], [["tf_anprc152", 3]], [], []];
private _sub = [[],[],[],[]];
private _landMedic = [[], [["tf_anprc152", 3]], [], []];
private _repair = [[],[],[],[]];
private _fuel = [[],[],[],[]];
private _ammo = [[],[],[],[]];
private _truck = [[], [["tf_anprc152", 3]], [], []];
private _quad = [[], [["tf_anprc152", 1]], [], []];
private _artiTank = [[],[],[],[]];
private _artiCannon = [[],[],[],[]];
private _artiTube = [[],[],[],[]];

_out set [VC_TFARB, [_car, _carArmed, _apc, _tank, _aaTank, _planeCAS, _planeAA, _planeTransport, _uav,
                 _heliSmall, _heliSmallArmed, _heliMedium, _heliMedEvac, _heliBig, _heliAttack,
                 _boatSmall, _boatAttack, _boatBig, _sub, _landMedic, _repair, _fuel, _ammo, _truck, _quad,
                 _artiTank, _artiCannon, _artiTube]];
                 
//------------------------------------------------------------ Role Loadout TFAR BLUFOR

/*
 [uniform, [inUniform]], 
 [vest, inVest]], 
 [backpack, [inBackpack]], 
 [primWeapon, [muzzle, bipod, acc, scope, ammo2, ammo]] 
 [secWeapon, [muzzle, bipod, acc, scope, ammo]], 
 [handWeapon, [muzzle, bipod, acc, scope, ammo]], 
 helmet, face, comm, terminal, map, bino, nv, watch, compass
*/

RL_TFARB = VC_TFARB + 1;

private _buff = [];
{
    _buff pushback [[nil, []], 
 					[nil, []], 
 					[([_x] call tfarB_fnc_backpack), []], 
 					[nil, []], 
 					[nil, []], 
 					[nil, []], 
 					nil, nil, 'tf_anprc152', nil, nil, nil, nil, nil, nil]; 
} forEach ((PV select RL_k) select 1);

_out set [RL_TFARB, _buff];

//------------------------------------------------------------ FINITO, return
          
_out