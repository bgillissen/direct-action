/*
 	Name: TFAR_fnc_calcTerrainInterception
 	
 	Author(s):
		NKey

 	Description:
		Calculates the terrain interception between the player and the passed unit.
	
	Parameters:
		OBJECT - Unit to calculate terrain interception with.
 	
 	Returns:
		NUMBER - Terrain Interception
 	
 	Example:
		_interception = soldier2 call TFAR_fnc_calcTerrainInterception;
*/
private ["_result", "_l", "_r", "_m", "_p1", "_p2", "_middle"];

_result = 0;

if ( !(isNull curatorCamera) && !stayInBody ) then {
	_p1 = getPosASL curatorCamera;
} else {
    if ( alive TFAR_currentUnit ) then {
		_p1 = eyePos TFAR_currentUnit;
    } else {
        _p1 = [0, 0, 0];
    };
};

private _p2CuratorCamPos = (_this getVariable ["tf_curatorCamPos", []]);
if !( _p2CuratorCamPos isEqualTo [] ) then { 
	_p2 = _p2CuratorCamPos;
} else {
    if ( alive _this ) then {    	
		_p2 = eyePos _this;
	} else {
        _p2 = [0, 0, 0];
	};
};

if (terrainIntersectASL[_p1, _p2]) then {
	_l = 10.0;
	_r = 250.0;
	_m = 100.0;

	_middle = [((_p1 select 0) + (_p2 select 0)) / 2.0, ((_p1 select 1) + (_p2 select 1)) / 2.0, ((_p1 select 2) + (_p2 select 2)) / 2.0];	
	_base = _middle select 2;

	while {(_r - _l) > 10} do {
		_middle set[2, _base + _m];
		if ((!terrainIntersectASL [ _p1, _middle ]) and {!terrainIntersectASL [ _p2, _middle ]}) then {
			_r = _m;
		} else {
			_l = _m;
		};
		_m = (_l + _r) / 2.0;
	};
	_result = _m;
};
_result