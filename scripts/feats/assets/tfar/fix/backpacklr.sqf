/*
 	Name: TFAR_fnc_backpackLR
 	
 	Author(s):
		NKey
 	Description:
		Returns the backpack radio (if there is one)
	
	Parameters:
		0: Object: Unit
 	
 	Returns:
		ARRAY: Manpack or empty array
 	
 	Example:
		_radio = player call TFAR_fnc_backpackLR;
*/

private _result = [];
private _boc = false;
if !( isNil "BOC_onChest" ) then { _boc = ( (player isEqualTo _this) && BOC_onChest ); };

private "_backpack";
if ( _boc ) then {
	_backpack = (backpackCargo BOC_fakeBag) select 0;
} else {
    _backpack = backpack _this;
};

if ( ([_backpack, "tf_hasLRradio", 0] call TFAR_fnc_getConfigProperty) == 1 ) then {
    if ( _boc ) then {
        _result = [firstBackpack BOC_fakeBag, "radio_settings"];
	} else {
		_result = [unitBackpack _this, "radio_settings"];
	};
};

_result