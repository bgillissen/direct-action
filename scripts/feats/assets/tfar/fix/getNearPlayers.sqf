
if !( alive TFAR_currentUnit ) exitWith { [] };

private _result = [];

private _playerVP = TFAR_currentUnit;
if ( !(isNull curatorCamera) && !stayInBody ) then { _playerVP = curatorCamera; };

private _allUnits = _playerVP nearEntities ["Man", TF_max_voice_volume];

{
	private _veh = _x;		
	{ _allUnits pushBack _x; } forEach (crew _veh);
} forEach  (_playerVP nearEntities [["LandVehicle", "Air", "Ship"], TF_max_voice_volume]);

{
    private _puppet = (_x getVariable "tf_controlled_unit");
	if !( isNil "_puppet" ) then {
        if ( ((getPosASL _playerVP) distance _puppet) <= TF_max_voice_volume ) then { _result pushBack _x; }; 
	} else {
    	private _curatorCamPos = (_x getVariable ["tf_curatorCamPos", []]);
		if !( _curatorCamPos isEqualTo [] ) then {
			if ( ((getPosASL _playerVP) distance _curatorCamPos) <= TF_max_voice_volume ) then { _result pushBack _x; };
		};     
	}; 
} forEach allPlayers;
            	
{	
	if ( alive _x ) then {	
		if ( isPlayer _x ) then {
        	private _puppet = (_x getVariable "tf_controlled_unit");
        	if ( isNil "_puppet" ) then {               
        		if !( _x getVariable ["tf_forceSpectator", false] ) then { _result pushBack _x; };    
        	};
    	};
	};
	true;
} count _allUnits;

_result