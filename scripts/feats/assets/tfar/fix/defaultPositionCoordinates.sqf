/*
 	Name: TFAR_fnc_defaultPositionCoordinates
 	
 	Author(s):
		NKey

 	Description:
		Prepares the position coordinates of the passed unit.
	
	Parameters:
		0: OBJECT - unit
		1: BOOLEAN - Is near player
 	
 	Returns:
		Nothing
 	
 	Example:
		
*/

params ["_unit", "_isNearPlayer"];

private "_current_eyepos";
private _current_rotation_horizontal = 0;

if !( _unit isEqualTo TFAR_currentUnit ) then {
    private _curatorCamPos = (_unit getVariable ["tf_curatorCamPos", []]);
	if !( _curatorCamPos isEqualTo [] ) then { 
    	_current_eyepos = _curatorCamPos; 
	} else {
        _current_eyepos = eyePos _unit;
        if ( _isNearPlayer ) then {
			_current_eyepos = (visiblePosition _unit) vectorAdd (_current_eyepos vectorDiff (getPos _unit));
		};
    };
	if ( alive TFAR_currentUnit ) then {
        private "_playerPos";
		if ( !(isNull curatorCamera) && !stayInBody ) then { 
			_playerPos = (getPosASL curatorCamera); 
		} else {
			_playerPos = eyePos TFAR_currentUnit;
		};
		_current_eyepos = _current_eyepos vectorDiff _playerPos;
	};
} else {
    _current_eyepos = [0,0,0];
	_current_rotation_horizontal = call TFAR_fnc_currentDirection;	
};    

_current_eyepos pushBack _current_rotation_horizontal;

_current_eyepos