/*
@filename: feats\assets\tfar\setChannel.sqf
Author:
	Ben
Description:
	run on player,
	update TFAR serious settings
*/

if !( CTXT_PLAYER ) exitWith {};

params ["_name", "_pass"];

tf_radio_channel_name = _name;
tf_radio_channel_password = _pass;

call TFAR_fnc_sendVersionInfo;