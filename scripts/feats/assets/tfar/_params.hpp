class gear_tfar {
	title = "Which TFAR gear is available in arsenal";
	values[] = {0,1,2,3};
	texts[] = {"none", "player side only", "player side + allies", "all"};
	default = 1;
};

class rl_tfar {
	title = "Players spawn with TFAR radios";
	values[] = {0,1};
	texts[] = {"no", "yes"};
	default = 1;
};
