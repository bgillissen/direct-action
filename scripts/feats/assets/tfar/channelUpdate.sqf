/*
@filename: feats\assets\tfar\channelUpdate.sqf
Author:
	Ben
Description:
	run on server,
	update TFAR serious settings following the active TS channel
*/

private _name = getText( missionConfigFile >> "settings" >> "tfar" >> "channels" >> TFAR_channel >> "name" );
private _varName = getText( missionConfigFile >> "settings" >> "tfar" >> "channels" >> TFAR_channel >> "passVarName" );
private _password = getText( missionConfigFile >> "settings" >> "tfar" >> "channels" >> TFAR_channel >> "password" );
private _private = [false, true] select (getNumber( missionConfigFile >> "settings" >> "tfar" >> "channels" >> TFAR_channel >> "private" ));

if !( _varName isEqualTo "" ) then {
    if ( isNil _varName ) then {
		diag_log format["TFAR: channel password variable (%1) is not set!", _varName];
        _password = "";
    } else {
		_password = missionNameSpace getVariable [_varName, ""];
	}; 
};

if ( _password isEqualTo "" ) then { _password = "123"; };

tf_radio_channel_name = _name;
tf_radio_channel_password = _password;

publicVariable "tf_radio_channel_name";

if !( _this ) exitWith {
    if !( _private ) then { publicVariable "tf_radio_channel_password"; };
};

private _targets = 0;

if ( _private ) then {
    _targets = [];
	{
		if ( [_x] call memberData_fnc_isMember ) then { _targets pushback (owner _x); };
	} forEach allPlayers;
};

[tf_radio_channel_name, tf_radio_channel_password] remoteExec ["tfar_fnc_setChannel", _targets, false];