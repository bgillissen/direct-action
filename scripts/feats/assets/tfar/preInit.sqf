/*
@filename: feats\assets\tfar\preInit.sqf
Author:
	Ben
Description:
	run on server,
	implent tfar blacklist (backpack, item, weapon, ammo, veh, unit, group, object)
*/

TF_defaultWestPersonalRadio = "tf_anprc152";
TF_defaultWestRiflemanRadio = "tf_anprc152";

TF_defaultEastPersonalRadio = "tf_fadak";
TF_defaultEastRiflemanRadio = "tf_fadak";

TF_defaultGuerPersonalRadio = "tf_anprc148jem";
TF_defaultGuerRiflemanRadio = "tf_anprc148jem";

[[], ["ItemRadio"], [], [], [], [], [], [], []] call common_fnc_addToBlacklists;