/*
@filename: mods\tfar\init.sqf
Author:
	Ben
Description:
	run on server,
	implent TFAR assets
*/

private _o = (call tfarO_fnc_assets);
private _b = (call tfarB_fnc_assets);
private _i = (call tfarI_fnc_assets);

private _data = [["TFARO", _o, [0]], ["TFARB", _b, [1]], ["TFARI", _i, [2]]];

["tfar", _data] call assets_fnc_implent;

//those are needed client side by radioFreq and va (when filtered)
TFAR_SR = [];
TFAR_LR = [];

{
	_x params ["_src", "_side"];
	if ( _side in ALLIES ) then {
		TFAR_LR append (_src select 0 select 0);
		TFAR_SR append (_src select 0 select 1);
	};
} forEach [[_o, east], [_b, west], [_i, independent]];

publicVariable "TFAR_SR";
publicVariable "TFAR_LR";