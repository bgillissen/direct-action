/*
@filename: feats\assets\tfar\OPFOR\assets.sqf
Author:
	Ben
Description:
	run on server
	return OPFOR TFAR assets
*/

private _out = [];

//------------------------------------------------------------ Arsenal TFAR OPFOR

A_TFARO = 0;

private _backpacks = ["tf_mr3000",
                      "tf_mr3000_bwmod",
                      "tf_mr3000_bwmod_tropen",
				 	  "tf_mr3000_multicam", 
				 	  "tf_mr3000_rhs",
				 	  "tf_mr6000l"];
private _items = ["tf_fadak"];
private _weapons = [];
private _ammo = [];

_out set [A_TFARO, [_backpacks, _items, _weapons, _ammo]];

//------------------------------------------------------------ supplyDrop TFAR OPFOR

SD_TFARO = A_TFARO + 1;

private _backpacks = [];
private _items = [["tf_fadak", 3]];
private _weapons = [];
private _ammo = [];
private _crates = [];
private _vehicles = [];

_out set [SD_TFARO, [_backpacks, _items, _weapons, _ammo, _crates, _vehicles]];

//------------------------------------------------------------ Vehicles Cargo TFAR OPFOR

VC_TFARO = SD_TFARO + 1;

private _car = [[], [["tf_fadak", 3]], [], []];
private _carArmed = [[], [["tf_fadak", 3]], [], []];
private _apc = [[], [["tf_fadak", 3]], [], []];
private _tank = [[], [["tf_fadak", 3]], [], []];
private _aaTank = [[], [["tf_fadak", 3]], [], []];
private _planeCAS = [[],[],[],[]];
private _planeAA = [[],[],[],[]];
private _planeTransport = [[],[],[],[]];
private _uav = [[],[],[],[]];
private _heliSmall = [[], [["tf_fadak", 3]], [], []];
private _heliSmallArmed = [[], [["tf_fadak", 3]], [], []];
private _heliMedium = [[], [["tf_fadak", 3]], [], []];
private _heliMedEvac = [[], [["tf_fadak", 3]], [], []];
private _heliBig = [[], [["tf_fadak", 3]], [], []];
private _heliAttack = [[],[],[],[]];
private _boatSmall = [[],[],[],[]];
private _boatAttack = [[], [["tf_fadak", 3]], [], []];
private _boatBig = [[], [["tf_fadak", 3]], [], []];
private _sub = [[],[],[],[]];
private _landMedic = [[], [["tf_fadak", 3]], [], []];
private _repair = [[],[],[],[]];
private _fuel = [[],[],[],[]];
private _ammo = [[],[],[],[]];
private _truck = [[], [["tf_fadak", 3]], [], []];
private _quad = [[], [["tf_fadak", 1]], [], []];
private _artiTank = [[],[],[],[]];
private _artiCannon = [[],[],[],[]];
private _artiTube = [[],[],[],[]];

_out set [VC_TFARO, [_car, _carArmed, _apc, _tank, _aaTank, _planeCAS, _planeAA, _planeTransport, _uav,
                 _heliSmall, _heliSmallArmed, _heliMedium, _heliMedEvac, _heliBig, _heliAttack,
                 _boatSmall, _boatAttack, _boatBig, _sub, _landMedic, _repair, _fuel, _ammo, _truck, _quad,
                 _artiTank, _artiCannon, _artiTube]];
                 
//------------------------------------------------------------ Role Loadout TFAR BLUFOR
/*
 [uniform, [inUniform]], 
 [vest, inVest]], 
 [backpack, [inBackpack]], 
 [primWeapon, [muzzle, bipod, acc, scope, ammo2, ammo]] 
 [secWeapon, [muzzle, bipod, acc, scope, ammo]], 
 [handWeapon, [muzzle, bipod, acc, scope, ammo]], 
 helmet, face, comm, terminal, map, bino, nv, watch, compass
*/

RL_TFARO = VC_TFARO + 1;

private _buff = [];
{
    _buff pushback [[nil, []], 
 					[nil, []], 
 					[([_x] call tfarO_fnc_backpack), []], 
 					[nil, []], 
 					[nil, []], 
 					[nil, []], 
 					nil, nil, "tf_fadak", nil, nil, nil, nil, nil, nil]; 
} forEach ((PV select RL_k) select 1);

_out set [RL_TFARO, _buff];

//------------------------------------------------------------ FINITO, return
          
_out