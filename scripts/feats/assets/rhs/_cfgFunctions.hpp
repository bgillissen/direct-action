class rhs {
	tag = "rhs";
	class functions {
		class isArmed { file="feats\assets\rhs\isArmed.sqf"; };
		class getMagazines { file="feats\assets\rhs\getMagazines.sqf"; };
	};
};

#include "AFRF\_cfgFunctions.hpp"
#include "GREF\_cfgFunctions.hpp"
#include "USAF\_cfgFunctions.hpp"
