class rhsAFRF {
	tag = "rhsAFRF";
	class functions {
		class assets { file="feats\assets\rhs\AFRF\assets.sqf"; };
		class backpack { file="feats\assets\rhs\AFRF\backpack.sqf"; };
		class handWeap { file="feats\assets\rhs\AFRF\handWeap.sqf"; };
		class helmet { file="feats\assets\rhs\AFRF\helmet.sqf"; };
		class init { file="feats\assets\rhs\AFRF\init.sqf"; };
		class preInit { file="feats\assets\rhs\AFRF\preInit.sqf"; };
		class primWeap { file="feats\assets\rhs\AFRF\primWeap.sqf"; };
		class postInit { file="core\dummy.sqf"; };
		class uniform { file="feats\assets\rhs\AFRF\uniform.sqf"; };
		class vest { file="feats\assets\rhs\AFRF\vest.sqf"; };
	};
};
