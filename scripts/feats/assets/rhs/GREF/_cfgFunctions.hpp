class rhsGREF {
	tag = "rhsGREF";
	class functions {
		class init { file="feats\assets\rhs\GREF\init.sqf"; };
		class postInit { file="core\dummy.sqf"; };
		class preInit { file="feats\assets\rhs\GREF\preInit.sqf"; };
	};
};

#include "BLUFOR\_cfgFunctions.hpp"
#include "IND\_cfgFunctions.hpp"
