/*
@filename: feats\assets\rhs\GREF\init.sqf
Author:
	Ben
Description:
	run on server,
	implent RHS GREF assets
*/

["rhs", [["GREFI", (call gi_fnc_assets), [2]]]] call assets_fnc_implent;
["rhs", [["GREFB", (call gb_fnc_assets), [1]]]] call assets_fnc_implent;