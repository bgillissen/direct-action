/*
@filename: feats\assets\rhs\USAF\helmet.sqf
Author:
	Ben
Description:
	run on server
	call by feats\assets\rhs\USAF\assets.sqf
	return a RHS USAF helmet depending on given role and MAP_KEYWORDS
Params:
	STRING	unit role
Environment:
	missionNamespace:
		MAP_KEYWORDS
Return:
	STRING	a helmet classname
*/
params ["_role"];

if ( _role isEqualTo "hq" ) exitWith { "H_Beret_02" };
if ( _role isEqualTo "hPilot" ) exitWith { "rhsusf_hgu56p_black" };
if ( _role isEqualTo "jPilot" ) exitWith { "rhsusf_hgu56p_visor_mask_Empire_black" };

if ( ("desert" in MAP_KEYWORDS) || ("dry" in MAP_KEYWORDS) ) exitWith {
	if ( _role in ["tl", "sl", "medic", "hmg", "lmg", "assHMG", "engineer", "mortar", "uavOp"] ) exitWith { "rhsusf_ach_helmet_headset_ocp" };
	if ( _role in ["marksman", "spotter", "sniper"] ) exitWith { "rhs_Booniehat_ocp" };
	if ( _role isEqualTo "crew" ) exitWith { "rhsusf_cvc_ess" };
	if ( _role isEqualTo "jtac" ) exitWith { "rhsusf_Bowman" };
	"rhsusf_ach_helmet_ocp"
};

if ( _role isEqualTo "crew" ) exitWith { "rhsusf_cvc_green_ess" };

"rhsusf_ach_bare_headset"
