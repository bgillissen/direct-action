/*
@filename: feats\assets\rhs\USAF\assets.sqf
Author:
	Ben
Description:
	run on server
	call by feats\assets\rhs\USAF\init.sqf
	return the RHS USAF assets
*/

private _isDesert = (["desert", "dry"] call assets_fnc_mapKeywords);
private _isWood = (["jungle", "wood"] call assets_fnc_mapKeywords);

private _factions = [];

if ( _isDesert )  then { _factions pushback "rhs_faction_usarmy_d"; };
if ( _isWood ) then { _factions pushback "rhs_faction_usarmy_wd"; };

private _out = [];

//------------------------------------------------------------ Arsenal RHS USAF

A_USAF = 0;

private _backpacks = ["RHS_USAF"] call assets_fnc_getBackpacks;
_backpacks pushback "B_UAV_01_backpack_F";

private _items = ["RHS_USAF"] call assets_fnc_getItems;
//_items append ([0, "", ["getText( _x >> 'author' ) isEqualTo 'Red Hammer Studios'"]] call assets_fnc_getItems);
_items append ["B_UavTerminal",
			   "rhsusf_acc_anpeq15", 
			   "rhsusf_acc_anpeq15_light", 
               "rhsusf_acc_anpeq15_top", 
               "rhsusf_acc_anpeq15_bk", 
               "rhsusf_acc_anpeq15_bk_h",
               "rhsusf_acc_anpeq15_bk_light_h", 
               "rhsusf_acc_M952V", 
               "rhsusf_acc_ACOG3_3d", 
               "rhsusf_ANPVS_14",
               "rhsusf_ANPVS_15",
               "rhsusf_ANVIS",
               "rhsusf_anvis_nvg_bc_caps",
               "rhs_optic_maaws",
               "rhs_ess_black", 
               "rhs_googles_black", 
               "rhs_googles_clear", 
               "rhs_googles_orange", 
               "rhs_googles_yellow",
               "rhsusf_oakley_goggles_blk",
               "rhsusf_oakley_goggles_clr",
               "rhsusf_oakley_goggles_ylw",
               "rhsusf_shemagh_gogg_grn",
               "rhsusf_shemagh_gogg_od",
               "rhsusf_shemagh_gogg_tan",
               "rhsusf_shemagh_gogg_white",
               "rhsusf_shemagh_grn",
               "rhsusf_shemagh_od",
               "rhsusf_shemagh_tan",
               "rhsusf_shemagh_white",
               "rhsusf_shemagh2_gogg_grn",
               "rhsusf_shemagh2_gogg_od",
               "rhsusf_shemagh2_gogg_tan",
               "rhsusf_shemagh2_gogg_white",
               "rhsusf_shemagh2_grn",
               "rhsusf_shemagh2_od",
               "rhsusf_shemagh2_tan",
               "rhsusf_shemagh2_white"];
private _weapons = ["RHS_USAF"] call assets_fnc_getWeapons;
_weapons append ["rhsusf_weap_MP7A2_aor1", "rhsusf_weap_MP7A2"];
private _ammo = _weapons call assets_fnc_getWeaponMagazines; //rhs_fnc_getMagazines;
_ammo append ["rhsusf_mine_m14_mag",
			  "rhs_mine_M19_mag"];

_out set [A_USAF, [_backpacks, _items, _weapons, _ammo]];

//------------------------------------------------------------ Restricted Gear RHS USAF

RG_USAF = A_USAF + 1;

private _launcher = ["rhs_weap_fgm148",
					 "rhs_weap_fim92",
                     "rhs_weap_smaw",
                     "rhs_weap_smaw_green", 
                     "rhs_weap_maaws",
                     "launch_RPG32_F"];
private _mg = ["rhs_weap_m240B",
			   "rhs_weap_m240B_CAP",
               "rhs_weap_m240G",
			   "rhs_weap_m249",
			   "rhs_weap_m249_pip",
               "rhs_weap_m249_pip_elcan",
               "rhs_weap_m249_pip_L",
               "rhs_weap_m249_pip_L_para",
               "rhs_weap_m249_pip_L_vfg",
               "rhs_weap_m249_pip_L_vfg1",
               "rhs_weap_m249_pip_L_vfg2",
               "rhs_weap_m249_pip_L_vfg3",
               "rhs_weap_m249_pip_S",
               "rhs_weap_m249_pip_S_para",
               "rhs_weap_m249_pip_S_vfg",
               "rhs_weap_m249_pip_S_vfg1",
               "rhs_weap_m249_pip_S_vfg2",
               "rhs_weap_m249_pip_S_vfg3",
               "rhs_weap_m249_pip_usmc"];
private _sRifle = ["rhs_weap_M107",
				   "rhs_weap_M107_d",
                   "rhs_weap_M107_w"];
private _mRifle = ["rhs_weap_sr25",
				   "rhs_weap_sr25_ec",
				   "rhs_weap_sr25_ec_d",
                   "rhs_weap_sr25_ec_wd",
                   "rhs_weap_m40a5",
                   "rhs_weap_m40a5_d",
                   "rhs_weap_m40a5_wd",
                   "rhs_weap_m14ebrri",
                   "rhs_weap_XM2010",
                   "rhs_weap_XM2010_wd",
                   "rhs_weap_XM2010_d",
                   "rhs_weap_XM2010_sa",
                   "rhs_weap_m24sws",
                   "rhs_weap_m24sws_blk",
                   "rhs_weap_m24sws_ghillie"];
private _sScope = ["rhsusf_acc_LEUPOLDMK4_2",
				   "rhsusf_acc_LEUPOLDMK4_2_d",
                   "rhsusf_acc_M8541",
                   "rhsusf_acc_M8541_low",
                   "rhsusf_acc_M8541_low_d",
                   "rhsusf_acc_M8541_low_wd",
                   "rhsusf_acc_premier",
                   "rhsusf_acc_premier_low",
                   "rhsusf_acc_premier_anpvs27"];
private _mScope = ["rhsusf_acc_LEUPOLDMK4",
				   "rhsusf_acc_ACOG_MDO"];
private _oScope = ["rhsusf_acc_anpas13gv1"];
private _mbrItem = [];
private _backpack = [];

_out set [RG_USAF, [_launcher, _mg, _sRifle, _mRifle, _sScope, _mScope, _oScope, _mbrItem, _backpack]];

//------------------------------------------------------------ Allowed Vehicles RHS USAF

AV_USAF = RG_USAF + 1;

private _heli = ["RHS_MELB_MH6M", "RHS_MELB_H6M"];
private _plane = [];
private _tank = [];

_out set [AV_USAF, [_heli, _plane, _tank]];

//------------------------------------------------------------ supplyDrop RHS USAF

SD_USAF = AV_USAF + 1;

private _backpacks = [["rhsusf_assault_eagleaiii_coy", 2]];
private _items = [];
private _weapons = [["rhs_weap_M136_hp", 5],
					["rhs_weap_m4a1_blockII", 3]];
private _ammo = [["rhs_mag_30Rnd_556x45_M855A1_Stanag_Tracer_Red", 40],
                 ["rhsusf_100Rnd_762x51_m62_tracer", 15],
				 ["rhsusf_200rnd_556x45_M855_mixed_box", 15],
				 ["rhs_fim92_mag", 3],
				 ["rhs_fgm148_magazine_AT", 3],
                 ["rhs_mag_maaws_HEAT", 3],
				 ["rhsusf_mag_10Rnd_STD_50BMG_M33", 20],
				 ["rhs_mag_M441_HE", 30],
				 ["rhs_mag_an_m8hc", 40]];
private _crates = ["rhsusf_mags_crate"];
private _vehicles = ["RHS_UH60M",
					 "RHS_CH_47F_10",
					 "rhsusf_CH53E_USMC_D",
					 "RHS_C130J"];

_out set [SD_USAF, [_backpacks, _items, _weapons, _ammo, _crates, _vehicles]];

//------------------------------------------------------------ Rewards RHS USAF

R_USAF = SD_USAF + 1;

private _rewards = (["RHS_USAF", _factions, "Heli_Attack_01_base_F", [true, "rhs_fnc_isArmed"]] call assets_fnc_getVehicles); //apache
if ( _isDesert ) then { //for Haggis 
	_rewards append ["rhsusf_m113d_usarmy_medical", "rhsusf_M1083A1P2_B_M2_d_Medical_fmtv_usarmy"]; 
} else {
	_rewards append ["rhsusf_m113_usarmy_medical"];    
};
_out set [R_USAF, _rewards];

//------------------------------------------------------------ Spawn RHS USAF

S_USAF = R_USAF + 1;

private _rt = [];
private _crates = ["rhsusf_mags_crate"];
private _pGroups = [["West", "rhs_faction_usarmy_wd", "rhs_group_nato_usarmy_wd_infantry"]];
if ( _isDesert ) then {
	_pGroups = [["West", "rhs_faction_usarmy_d", "rhs_group_nato_usarmy_d_infantry"]];
};
private _sGroups = [["West", "rhs_faction_usarmy_wd", "rhs_group_nato_usarmy_wd_infantry", "rhs_group_nato_usarmy_wd_infantry_squad_sniper"]];
if ( _isDesert ) then {
	_sGroups = [["West", "rhs_faction_usarmy_d", "rhs_group_nato_usarmy_d_infantry", "rhs_group_nato_usarmy_d_infantry_squad_sniper"]];
};
private _pilot = ["rhsusf_airforce_jetpilot"];
private _crew = ["rhsusf_usmc_marpat_wd_combatcrewman"];
if ( _isDesert ) then { _crew = ["rhsusf_usmc_marpat_d_combatcrewman"]; };
private _officer = ["rhsusf_usmc_marpat_wd_officer"];
if ( _isDesert ) then { _officer = ["rhsusf_usmc_marpat_d_officer"]; };
private _garrison = ["rhsusf_usmc_marpat_wd_grenadier", "rhsusf_usmc_marpat_wd_machinegunner"];
if ( _isDesert ) then { _garrison = ["rhsusf_usmc_marpat_d_grenadier", "rhsusf_usmc_marpat_d_machinegunner"]; };
private _aa = ["RHS_M6_wd"];
if ( _isDesert ) then { _aa = ["RHS_M6"]; };
private _arti = ["RHS_USAF", _factions, "MBT_01_arty_base_F", [true, "rhs_fnc_isArmed"]] call assets_fnc_getVehicles;
private _static = ["RHS_USAF", _factions, "StaticWeapon", [true, "rhs_fnc_isArmed"]] call assets_fnc_getVehicles;
private _cas = ["RHS_A10", "RHS_A10_AT"];
private _tank = ["RHS_USAF", _factions, "MBT_01_base_F", [true, "rhs_fnc_isArmed"]] call assets_fnc_getVehicles;
private _apc = ["RHS_USAF", _factions, ["APC_Tracked_03_base_F", "Wheeled_APC_F"], [true, "rhs_fnc_isArmed"]] call assets_fnc_getVehicles;
private _car = ["RHS_USAF", _factions, "MRAP_01_base_F", [false, "rhs_fnc_isArmed"]] call assets_fnc_getVehicles;
if ( _isDesert ) then { _car append ["rhsusf_rg33_d", "rhsusf_mrzr4_d"]; };
if ( _isWood ) then { _car append ["rhsusf_rg33_wd", "rhsusf_mrzr4_w"]; };
private _carArmed = ["RHS_USAF", _factions, "MRAP_01_base_F", [true, "rhs_fnc_isArmed"]] call assets_fnc_getVehicles;
if ( _isDesert ) then { _carArmed pushback "rhsusf_rg33_m2_d"; };
if ( _isWood ) then { _carArmed pushback "rhsusf_rg33_m2_wd"; };
private _aPatrol = ["RHS_USAF", _factions, "Heli_Attack_01_base_F", [true, "rhs_fnc_isArmed"]] call assets_fnc_getVehicles;
		_aPatrol append ["RHS_MELB_AH6M_H", "RHS_MELB_AH6M_L", "RHS_MELB_AH6M_M"];
private _civ = [];
private _truck = ["rhsusf_M1078A1P2_B_wd_fmtv_usarmy", 
				  "rhsusf_M1078A1P2_B_wd_open_fmtv_usarmy",
                  "rhsusf_M1232_usarmy_wd",
                  "rhsusf_M1232_M2_usarmy_wd",
                  "rhsusf_M1232_MK19_usarmy_wd",
                  "rhsusf_M1237_M2_usarmy_wd",
                  "rhsusf_M1237_MK19_usarmy_wd",
                  "rhsusf_M1220_usarmy_wd", 
                  "rhsusf_M1220_M153_M2_usarmy_wd",
                  "rhsusf_M1220_M2_usarmy_wd",
                  //"rhsusf_M1220_M153_MK19_usarmy_wd",
                  "rhsusf_M1230_M2_usarmy_wd",
                  "rhsusf_M1230_MK19_usarmy_wd",
                  "rhsusf_M1232_usarmy_wd",
                  "rhsusf_M1232_M2_usarmy_wd",
                  "rhsusf_M1232_MK19_usarmy_wd",
                  "rhsusf_M1078A1R_SOV_M2_D_fmtv_socom"];
if ( _isDesert ) then { 
	_truck = ["rhsusf_M1078A1P2_B_d_fmtv_usarmy",
    		  "rhsusf_M1078A1P2_B_d_open_fmtv_usarmy",
              "rhsusf_M1232_usarmy_d",
              "rhsusf_M1232_M2_usarmy_d",
              "rhsusf_M1232_MK19_usarmy_d",
              "rhsusf_M1237_M2_usarmy_d",
              "rhsusf_M1237_MK19_usarmy_d",
              "rhsusf_M1220_usarmy_d", 
			  "rhsusf_M1220_M153_M2_usarmy_d",
              "rhsusf_M1220_M2_usarmy_d",
		      //"rhsusf_M1220_M153_MK19_usarmy_d",
              "rhsusf_M1230_M2_usarmy_d",
              "rhsusf_M1230_MK19_usarmy_d",
              "rhsusf_M1232_usarmy_d",
              "rhsusf_M1232_M2_usarmy_d",
              "rhsusf_M1232_MK19_usarmy_d",
              "rhsusf_M1078A1R_SOV_M2_D_fmtv_socom"]; 
};
_carArmed = _carArmed - _truck;

_out set [S_USAF, [_rt, _crates, _pGroups, _sGroups, _pilot, _crew, _officer, _garrison, _civ,
                _aa, _arti, _static, _cas, _tank, _apc, _car, _carArmed, _aPatrol]];

//------------------------------------------------------------ Vehicles RHS USAF

BV_USAF = S_USAF + 1;

//private _car = [];
//private _carArmed = [];
//private _apc = [];
//private _tank = [];
//private _aaTank = [];
private _planeCAS = ["RHS_A10"];
//private _planeAA = ["rhsusf_f22"];
//private _planeCAS = ["B_Plane_CAS_01_dynamicLoadout_F"];
private _planeAA = ["B_Plane_Fighter_01_F", "B_Plane_Fighter_01_Stealth_F"];
private _planeTransport = ["RHS_C130J"];
private _uav = ["B_UAV_02_dynamicLoadout_F", "B_UAV_05_F"];
private _heliSmall = ["RHS_MELB_MH6M", "RHS_MELB_H6M"];
private _heliSmallArmed = [/*"RHS_MELB_AH6M_H"/*,
                           "RHS_MELB_AH6M_L",*/
                           "RHS_MELB_AH6M_M"];
private _heliMedium = ["RHS_UH60M", "RHS_UH60M_ESSS", "RHS_UH60M_ESSS2", "RHS_UH1Y_UNARMED"];
private _heliMedEvac = ["RHS_UH60M_MEV", "RHS_UH60M_MEV2"];
private _heliBig = ["rhsusf_CH53E_USMC_D"];
if ( _isDesert ) then { _heliBig pushback "RHS_CH_47F_light"; };
if ( _isWood ) then { _heliBig pushback "RHS_CH_47F_10"; };
private _heliAttack = ["RHS_USAF", _factions, "Heli_Attack_01_base_F", [true, "rhs_fnc_isArmed"]] call assets_fnc_getVehicles;
private _boatSmall = [];
private _boatAttack = [];
private _boatBig = ["rhsusf_mkvsoc"];
private _sub = [];
private _landMedic = ["rhsusf_m113_usarmy_medical", "rhsusf_M1230a1_usarmy_wd", "rhsusf_M1085A1P2_B_WD_Medical_fmtv_usarmy"];
if ( _isDesert ) then { _landMedic = ["rhsusf_m113d_usarmy_medical", "rhsusf_M1083A1P2_B_M2_d_Medical_fmtv_usarmy", "rhsusf_M1230a1_usarmy_d", "rhsusf_M1085A1P2_B_D_Medical_fmtv_usarmy"]; };
private _repair = ["rhsusf_M977A4_REPAIR_BKIT_usarmy_wd"];
if ( _isDesert ) then { _repair = ["rhsusf_M977A4_REPAIR_BKIT_usarmy_d"]; };
private _fuel = ["rhsusf_M978A4_BKIT_usarmy_wd"];
if ( _isDesert ) then { _fuel = ["rhsusf_M978A4_BKIT_usarmy_d"]; };
private _ammo = ["rhsusf_M977A4_AMMO_BKIT_usarmy_wd"];
if ( _isDesert ) then { _ammo = ["rhsusf_M977A4_AMMO_BKIT_usarmy_d"]; };

private _quad = [];
private _artiTank = ["rhsusf_m109_usarmy"];
if ( _isDesert ) then { _artiTank = ["rhsusf_m109d_usarmy"]; };
private _artiCannon = ["RHS_M119_WD"];
if ( _isDesert ) then { _artiCannon = ["RHS_M119_D"]; };
private _artiTube = ["RHS_M252_WD"];
if ( _isDesert ) then { _artiTube = ["RHS_M252_D"]; };

_out set [BV_USAF, [_car, _carArmed, _apc, _tank, _aa, _planeCAS, _planeAA, _planeTransport, _uav,
                _heliSmall, _heliSmallArmed, _heliMedium, _heliMedEvac, _heliBig, _heliAttack,
                _boatSmall, _boatAttack, _boatBig, _sub, _landMedic, _repair, _fuel, _ammo, _truck, _quad,
                 _artiTank, _artiCannon, _artiTube]];

//------------------------------------------------------------ Vehicles Cargo RHS USAF

VC_USAF = BV_USAF + 1;

private _car = [[],
				[],
                [["rhs_weap_M136_hp", 3], ["rhs_weap_m4a1_blockII", 1]],
                [["rhs_mag_M441_HE", 10], ["rhs_mag_an_m8hc", 15], ["rhs_mag_30Rnd_556x45_M855A1_Stanag_Tracer_Red", 30], ["rhsusf_100Rnd_762x51_m62_tracer", 5], ["rhsusf_200rnd_556x45_M855_mixed_box", 5]]];
private _carArmed = [[],
					 [],
                     [["rhs_weap_M136_hp", 3], ["rhs_weap_m4a1_blockII", 1]],
                     [["rhs_mag_M441_HE", 10], ["rhs_mag_an_m8hc", 15], ["rhs_mag_30Rnd_556x45_M855A1_Stanag_Tracer_Red", 30], ["rhsusf_100Rnd_762x51_m62_tracer", 5], ["rhsusf_200rnd_556x45_M855_mixed_box", 5]]];
private _apc = [[],
				[],
                [["rhs_weap_M136_hp", 3],
                ["rhs_weap_m4a1_blockII", 2]],
                [["rhs_mag_M441_HE", 10], ["rhs_mag_an_m8hc", 15, ["rhs_mag_30Rnd_556x45_M855A1_Stanag_Tracer_Red", 30], ["rhsusf_100Rnd_762x51_m62_tracer", 5], ["rhsusf_200rnd_556x45_M855_mixed_box", 5], ["rhs_mag_maaws_HEAT", 5]]]];
private _tank = [[],[],[],[]];
private _aaTank = [[],[],[],[]];
private _planeCAS = [[],[],[],[]];
private _planeAA = [[],[],[],[]];
private _planeTransport = [[],[],[],[]];
private _uav = [[],[],[],[]];
private _heliSmall = [[],
					  [],
                      [["rhs_weap_m4a1_blockII", 1]],
                      [["rhs_mag_M441_HE", 10], ["rhs_mag_an_m8hc", 15], ["rhs_mag_30Rnd_556x45_M855A1_Stanag_Tracer_Red", 30], ["rhsusf_100Rnd_762x51_m62_tracer", 5], ["rhsusf_200rnd_556x45_M855_mixed_box", 10], ["rhs_mag_maaws_HEAT", 5]]];
private _heliSmallArmed = [[],[],[],[]];
private _heliMedium = [[],
					   [],
                       [["rhs_weap_M136_hp", 3], ["rhs_weap_m4a1_blockII", 1]],
                       [["rhs_mag_M441_HE", 10], ["rhs_mag_an_m8hc", 15], ["rhs_mag_30Rnd_556x45_M855A1_Stanag_Tracer_Red", 30], ["rhsusf_100Rnd_762x51_m62_tracer", 5], ["rhsusf_200rnd_556x45_M855_mixed_box", 10], ["rhs_mag_maaws_HEAT", 5]]];
private _heliMedEvac = [[],[],[],[]];
private _heliBig = [[],
					[],
                    [["rhs_weap_M136_hp", 3], ["rhs_weap_m4a1_blockII", 1]],
                    [["rhs_mag_M441_HE", 10], ["rhs_mag_an_m8hc", 15], ["rhs_mag_30Rnd_556x45_M855A1_Stanag_Tracer_Red", 30], ["rhsusf_100Rnd_762x51_m62_tracer", 5], ["rhsusf_200rnd_556x45_M855_mixed_box", 10], ["rhs_mag_maaws_HEAT", 5]]];
private _heliAttack = [[],[],[],[]];
private _boatSmall = [[],[],[],[]];
private _boatAttack = [[],
					   [],
                       [["rhs_weap_M136_hp", 3],
                       ["rhs_weap_m4a1_blockII", 1]],
                       [["rhs_mag_M441_HE", 10], ["rhs_mag_an_m8hc", 15], ["rhs_mag_30Rnd_556x45_M855A1_Stanag_Tracer_Red", 30], ["rhsusf_100Rnd_762x51_m62_tracer", 5], ["rhsusf_200rnd_556x45_M855_mixed_box", 10], ["rhs_mag_maaws_HEAT", 5]]];
private _boatBig = [[],
					[],
                    [["rhs_weap_M136_hp", 3], ["rhs_weap_m4a1_blockII", 1]],
                    [["rhs_mag_M441_HE", 10], ["rhs_mag_an_m8hc", 15], ["rhs_mag_30Rnd_556x45_M855A1_Stanag_Tracer_Red", 30], ["rhsusf_100Rnd_762x51_m62_tracer", 5], ["rhsusf_200rnd_556x45_M855_mixed_box", 10], ["rhs_mag_maaws_HEAT", 5]]];
private _sub = [[],[],[],[]];
private _landMedic = [[],[],[],[]];
private _repair = [[],[],[],[]];
private _fuel = [[],[],[],[]];
private _ammo = [[],[],[],[]];
private _truck = [[],
				  [],
                  [["rhs_weap_M136_hp", 3], ["rhs_weap_m4a1_blockII", 1]],
                  [["rhs_mag_M441_HE", 10], ["rhs_mag_an_m8hc", 15], ["rhs_mag_30Rnd_556x45_M855A1_Stanag_Tracer_Red", 30], ["rhsusf_100Rnd_762x51_m62_tracer", 5], ["rhsusf_200rnd_556x45_M855_mixed_box", 10], ["rhs_mag_maaws_HEAT", 5]]];
private _quad = [[],[],[],[]];
private _artiTank = [[],[],[],[]];
private _artiCannon = [[],[],[],[]];
private _artiTube = [[],[],[],[]];

_out set [VC_USAF, [_car, _carArmed, _apc, _tank, _aaTank, _planeCAS, _planeAA, _planeTransport, _uav,
                 _heliSmall, _heliSmallArmed, _heliMedium, _heliMedEvac, _heliBig, _heliAttack,
                 _boatSmall, _boatAttack, _boatBig, _sub, _landMedic, _repair, _fuel, _ammo, _truck, _quad,
                 _artiTank, _artiCannon, _artiTube]];

//------------------------------------------------------------ Role Loadout RHS USAF
/*
 [uniform, [inUniform]], 
 [vest, inVest]], 
 [backpack, [inBackpack]], 
 [primWeapon, [muzzle, bipod, acc, scope, ammo2, ammo]] 
 [secWeapon, [muzzle, bipod, acc, scope, ammo]], 
 [handWeapon, [muzzle, bipod, acc, scope, ammo]], 
 helmet, face, comm, terminal, map, bino, nv, watch, compass
*/

RL_USAF = VC_USAF + 1;

private _hq = [[(["hq"] call rhsUSAF_fnc_uniform), [["rhsusf_ANPVS_15", 1]]],
               [(["hq"] call rhsUSAF_fnc_vest), [["rhsusf_weap_m1911a1", 1], ["rhsusf_mag_17Rnd_9x19_FMJ", 3]]],
               ["", []],
               ["", []],
               ["", []],
               ["", []],
               (["hq"] call rhsUSAF_fnc_helmet), nil, nil, nil, nil, nil, "", nil, nil];
               
private _sl = [[(["sl"] call rhsUSAF_fnc_uniform), [["rhsusf_ANPVS_15", 1]]],
			   [(["sl"] call rhsUSAF_fnc_vest), [["rhs_mag_30Rnd_556x45_M855A1_Stanag_Tracer_Red", 9], ["rhsusf_mag_17Rnd_9x19_FMJ", 2]]],
               [nil, [["rhs_mag_30Rnd_556x45_M855A1_Stanag_Tracer_Red", 4], ["3Rnd_HE_Grenade_shell", 3]]],
               [(["sl"] call rhsUSAF_fnc_primWeap), ["rhsusf_acc_SF3P556", "", "rhsusf_acc_anpeq15_bk", "rhsusf_acc_ACOG_RMR", "3Rnd_HE_Grenade_shell", "rhs_mag_30Rnd_556x45_M855A1_Stanag_Tracer_Red"]],
               ["", []],
               [(["sl"] call rhsUSAF_fnc_handWeap), ["rhsusf_acc_omega9k", "", "acc_flashlight_pistol", "", "rhsusf_mag_17Rnd_9x19_FMJ"]],
               (["sl"] call rhsUSAF_fnc_helmet), nil, nil, nil, nil, nil, "", nil, nil];

private _tl = [[(["tl"] call rhsUSAF_fnc_uniform), [["rhsusf_ANPVS_15", 1]]],
               [(["tl"] call rhsUSAF_fnc_vest), [["rhs_mag_30Rnd_556x45_M855A1_Stanag_Tracer_Red", 9], ["rhsusf_mag_17Rnd_9x19_FMJ", 1]]],
               [nil, [["rhs_mag_30Rnd_556x45_M855A1_Stanag_Tracer_Red", 4], ["3Rnd_HE_Grenade_shell", 3]]],
               [(["tl"] call rhsUSAF_fnc_primWeap), ["rhsusf_acc_SF3P556", "", "rhsusf_acc_anpeq15_bk", "rhsusf_acc_ACOG_RMR", "3Rnd_HE_Grenade_shell", "rhs_mag_30Rnd_556x45_M855A1_Stanag_Tracer_Red"]],
               ["", []],
               [(["tl"] call rhsUSAF_fnc_handWeap), ["rhsusf_acc_omega9k", "", "acc_flashlight_pistol", "", "rhsusf_mag_17Rnd_9x19_FMJ"]],
               (["tl"] call rhsUSAF_fnc_helmet), nil, nil, nil, nil, nil, "", nil, nil];

private _medic = [[(["medic"] call rhsUSAF_fnc_uniform), [["rhsusf_ANPVS_15", 1]]],
                  [(["medic"] call rhsUSAF_fnc_vest), [["rhs_mag_30Rnd_556x45_M855A1_Stanag_Tracer_Red", 9], ["rhsusf_mag_17Rnd_9x19_FMJ", 2]]],
                  [(["medic"] call rhsUSAF_fnc_backpack), []],
                  [(["medic"] call rhsUSAF_fnc_primWeap), ["rhsusf_acc_SF3P556", "rhsusf_acc_harris_bipod", "rhsusf_acc_anpeq15_bk", "rhsusf_acc_ACOG_RMR", "", "rhs_mag_30Rnd_556x45_M855A1_Stanag_Tracer_Red"]],
                  ["", []],
                  [(["medic"] call rhsUSAF_fnc_handWeap), ["rhsusf_acc_omega9k", "", "acc_flashlight_pistol", "", "rhsusf_mag_17Rnd_9x19_FMJ"]],
                  (["medic"] call rhsUSAF_fnc_helmet), nil, nil, nil, nil, nil, "", nil, nil];

private _lmg = [[(["lmg"] call rhsUSAF_fnc_uniform), [["rhsusf_ANPVS_15", 1]]],
                [(["lmg"] call rhsUSAF_fnc_vest), [["rhsusf_200rnd_556x45_M855_mixed_box", 3], ["rhsusf_mag_17Rnd_9x19_FMJ", 1]]],
                ["", []],
                [(["lmg"] call rhsUSAF_fnc_primWeap), ["rhsusf_acc_SF3P556", "", "rhsusf_acc_anpeq15side_bk", "rhsusf_acc_ACOG_RMR", "", "rhsusf_200rnd_556x45_M855_mixed_box"]],
                ["", []],
                [(["lmg"] call rhsUSAF_fnc_handWeap), ["rhsusf_acc_omega9k", "", "acc_flashlight_pistol", "", "rhsusf_mag_17Rnd_9x19_FMJ"]],
                (["lmg"] call rhsUSAF_fnc_helmet), nil, nil, nil, nil, nil, "", nil, nil];

private _hmg = [[(["hmg"] call rhsUSAF_fnc_uniform), [["rhsusf_ANPVS_15", 1]]],
                [(["hmg"] call rhsUSAF_fnc_vest), [["rhsusf_100Rnd_762x51_m61_ap", 3]]],
                [(["hmg"] call rhsUSAF_fnc_backpack), [["rhsusf_100Rnd_762x51_m62_tracer", 2], ["rhsusf_100Rnd_762x51_m61_ap", 2]]],
                [(["hmg"] call rhsUSAF_fnc_primWeap), ["rhsusf_acc_ARDEC_M240", "", "", "rhsusf_acc_ACOG_RMR", "", "rhsusf_100Rnd_762x51_m62_tracer"]],
                ["", []],
                [(["hmg"] call rhsUSAF_fnc_handWeap), ["rhsusf_acc_omega9k", "", "acc_flashlight_pistol", "", "rhsusf_mag_17Rnd_9x19_FMJ"]],
                (["hmg"] call rhsUSAF_fnc_helmet), nil, nil, nil, nil, nil, "", nil, nil];

private _assHMG = [[(["assHMG"] call rhsUSAF_fnc_uniform), [["rhsusf_ANPVS_15", 1]]],
                   [(["assHMG"] call rhsUSAF_fnc_vest), [["rhs_mag_30Rnd_556x45_M855A1_Stanag_Tracer_Red", 9], ["rhsusf_mag_17Rnd_9x19_FMJ", 2]]],
                   [(["assHMG"] call rhsUSAF_fnc_backpack), [["rhsusf_100Rnd_762x51_m62_tracer", 2], ["rhsusf_100Rnd_762x51_m61_ap", 1]]],
                   [(["assHMG"] call rhsUSAF_fnc_primWeap), ["rhsusf_acc_SF3P556", "rhsusf_acc_harris_bipod", "rhsusf_acc_anpeq15_bk", "rhsusf_acc_ACOG_RMR", "", "rhs_mag_30Rnd_556x45_M855A1_Stanag_Tracer_Red"]],
                   ["", []],
                   [(["assHMG"] call rhsUSAF_fnc_handWeap), ["rhsusf_acc_omega9k", "", "acc_flashlight_pistol", "", "rhsusf_mag_17Rnd_9x19_FMJ"]],
                   (["assHMG"] call rhsUSAF_fnc_helmet), nil, nil, nil, nil, nil, "", nil, nil];

private _aa = [[(["aa"] call rhsUSAF_fnc_uniform), [["rhsusf_ANPVS_15", 1]]],
               [(["aa"] call rhsUSAF_fnc_vest), [["rhs_mag_30Rnd_556x45_M855A1_Stanag_Tracer_Red", 9], ["rhsusf_mag_17Rnd_9x19_FMJ", 2]]],
               [(["aa"] call rhsUSAF_fnc_backpack), [["rhs_fim92_mag", 3]]],
               [(["aa"] call rhsUSAF_fnc_primWeap), ["rhsusf_acc_SF3P556", "rhsusf_acc_harris_bipod", "rhsusf_acc_anpeq15_bk", "rhsusf_acc_ACOG_RMR", "", "rhs_mag_30Rnd_556x45_M855A1_Stanag_Tracer_Red"]],
               ["rhs_weap_fim92", ["", "", "", "", "rhs_fim92_mag"]],
               [(["aa"] call rhsUSAF_fnc_handWeap), ["rhsusf_acc_omega9k", "", "acc_flashlight_pistol", "", "rhsusf_mag_17Rnd_9x19_FMJ"]],
               (["aa"] call rhsUSAF_fnc_helmet), nil, nil, nil, nil, nil, "", nil, nil];

private _assAA = [[(["assAA"] call rhsUSAF_fnc_uniform), [["rhsusf_ANPVS_15", 1]]],
                  [(["assAA"] call rhsUSAF_fnc_vest), [["rhs_mag_30Rnd_556x45_M855A1_Stanag_Tracer_Red", 9], ["rhsusf_mag_17Rnd_9x19_FMJ", 2]]],
                  [(["assAA"] call rhsUSAF_fnc_backpack), [["rhs_fim92_mag", 2]]],
                  [(["assAA"] call rhsUSAF_fnc_primWeap), ["rhsusf_acc_SF3P556", "rhsusf_acc_harris_bipod", "rhsusf_acc_anpeq15_bk", "rhsusf_acc_ACOG_RMR", "", "rhs_mag_30Rnd_556x45_M855A1_Stanag_Tracer_Red"]],
                  ["", []],
                  [(["assAA"] call rhsUSAF_fnc_handWeap), ["rhsusf_acc_omega9k", "", "acc_flashlight_pistol", "", "rhsusf_mag_17Rnd_9x19_FMJ"]],
                  (["assAA"] call rhsUSAF_fnc_helmet), nil, nil, nil, nil, nil, "", nil, nil];

private _at = [[(["at"] call rhsUSAF_fnc_uniform), [["rhsusf_ANPVS_15",1]]],
               [(["at"] call rhsUSAF_fnc_vest), [["rhs_mag_30Rnd_556x45_M855A1_Stanag_Tracer_Red", 9], ["rhsusf_mag_17Rnd_9x19_FMJ", 2]]],
               [(["at"] call rhsUSAF_fnc_backpack), [["rhs_mag_maaws_HEAT", 3]]],
               [(["at"] call rhsUSAF_fnc_primWeap), ["rhsusf_acc_SF3P556", "rhsusf_acc_harris_bipod", "rhsusf_acc_anpeq15_bk", "rhsusf_acc_ACOG_RMR", "", "rhs_mag_30Rnd_556x45_M855A1_Stanag_Tracer_Red"]],
               ["rhs_weap_maaws", ["", "", "", "rhs_optic_maaws", "rhs_mag_maaws_HEAT"]],
               [(["at"] call rhsUSAF_fnc_handWeap), ["rhsusf_acc_omega9k", "", "acc_flashlight_pistol", "", "rhsusf_mag_17Rnd_9x19_FMJ"]],
               (["at"] call rhsUSAF_fnc_helmet), nil, nil, nil, nil, nil, "", nil, nil];

private _assAT = [[(["assAT"] call rhsUSAF_fnc_uniform), [["rhsusf_ANPVS_15", 1]]],
                  [(["assAT"] call rhsUSAF_fnc_vest), [["rhs_mag_30Rnd_556x45_M855A1_Stanag_Tracer_Red", 9], ["rhsusf_mag_17Rnd_9x19_FMJ", 2]]],
                  [(["assAT"] call rhsUSAF_fnc_backpack), [["rhs_mag_maaws_HEAT", 2]]],
                  [(["assAT"] call rhsUSAF_fnc_primWeap), ["rhsusf_acc_SF3P556", "rhsusf_acc_harris_bipod", "rhsusf_acc_anpeq15_bk", "rhsusf_acc_ACOG_RMR", "", "rhs_mag_30Rnd_556x45_M855A1_Stanag_Tracer_Red"]],
                  ["", []],
                  [(["assAT"] call rhsUSAF_fnc_handWeap), ["rhsusf_acc_omega9k", "", "acc_flashlight_pistol", "", "rhsusf_mag_17Rnd_9x19_FMJ"]],
                  (["assAT"] call rhsUSAF_fnc_helmet), nil, nil, nil, nil, nil, "", nil, nil];

private _sniper = [[(["sniper"] call rhsUSAF_fnc_uniform), []],
                   [(["sniper"] call rhsUSAF_fnc_vest), [["rhsusf_10Rnd_762x51_m118_special_Mag", 8], ["rhsusf_10Rnd_762x51_m993_Mag", 4], ["rhsusf_mag_17Rnd_9x19_FMJ", 2], ["rhsusf_acc_ACOG_MDO", 1]]],
                   ["", []],
                   [(["sniper"] call rhsUSAF_fnc_primWeap), ["rhsusf_acc_anpeq15side", "rhsusf_acc_M8541_low_d", "rhsusf_acc_harris_swivel","rhsusf_10Rnd_762x51_m118_special_Mag"]],
                   ["", []],
                   [(["sniper"] call rhsUSAF_fnc_handWeap), ["rhsusf_mag_17Rnd_9x19_FMJ"]],
                   (["sniper"] call rhsUSAF_fnc_helmet), nil, nil, nil, nil, nil, "", nil, nil];

private _marksman = [[(["marksman"] call rhsUSAF_fnc_uniform), []],
                     [(["marksman"] call rhsUSAF_fnc_vest), [["rhsusf_20Rnd_762x51_SR25_m993_Mag", 3], ["rhsusf_20Rnd_762x51_SR25_m62_Mag", 3], ["rhsusf_mag_17Rnd_9x19_FMJ", 1]]],
                     ["", []],
                     [(["marksman"] call rhsUSAF_fnc_primWeap), ["rhsusf_acc_anpeq15side_bk", "rhsusf_acc_ACOG_MDO", "rhsusf_acc_harris_bipod", "rhsusf_20Rnd_762x51_SR25_m993_Mag"]],
                     ["", []],
                     [(["marksman"] call rhsUSAF_fnc_handWeap), ["rhsusf_mag_17Rnd_9x19_FMJ"]],
                     (["marksman"] call rhsUSAF_fnc_helmet), nil, nil, nil, nil, nil, "", nil, nil];

private _repair = [[(["repair"] call rhsUSAF_fnc_uniform), [["rhsusf_ANPVS_15", 1]]],
                   [(["repair"] call rhsUSAF_fnc_vest), [["rhs_mag_30Rnd_556x45_M855A1_Stanag_Tracer_Red", 9], ["rhsusf_mag_17Rnd_9x19_FMJ", 2]]],
                   [(["repair"] call rhsUSAF_fnc_backpack), []],
                   [(["repair"] call rhsUSAF_fnc_primWeap), ["rhsusf_acc_SF3P556", "rhsusf_acc_harris_bipod", "rhsusf_acc_anpeq15_bk", "rhsusf_acc_ACOG_RMR", "", "rhs_mag_30Rnd_556x45_M855A1_Stanag_Tracer_Red"]],
                   ["", []],
                   [(["repair"] call rhsUSAF_fnc_handWeap), ["rhsusf_acc_omega9k", "", "acc_flashlight_pistol", "", "rhsusf_mag_17Rnd_9x19_FMJ"]],
                   (["repair"] call rhsUSAF_fnc_helmet), nil, nil, nil, nil, nil, "", nil, nil];

private _demo = [[(["demo"] call rhsUSAF_fnc_uniform), [["rhsusf_ANPVS_15", 1]]],
	                 [(["demo"] call rhsUSAF_fnc_vest), [["rhs_mag_30Rnd_556x45_M855A1_Stanag_Tracer_Red", 9], ["rhsusf_mag_17Rnd_9x19_FMJ", 2]]],
	                 [(["demo"] call rhsUSAF_fnc_backpack), [["rhsusf_m112_mag", 4]]],
	                 [(["demo"] call rhsUSAF_fnc_primWeap), ["rhsusf_acc_SF3P556", "rhsusf_acc_harris_bipod", "rhsusf_acc_anpeq15_bk", "rhsusf_acc_ACOG_RMR", "", "rhs_mag_30Rnd_556x45_M855A1_Stanag_Tracer_Red"]],
	                 ["", []],
	                 [(["demo"] call rhsUSAF_fnc_handWeap), ["rhsusf_acc_omega9k", "", "acc_flashlight_pistol", "", "rhsusf_mag_17Rnd_9x19_FMJ"]],
	                 (["demo"] call rhsUSAF_fnc_helmet), nil, nil, nil, nil, nil, "", nil, nil];

private _engineer = [[(["engineer"] call rhsUSAF_fnc_uniform), [["rhsusf_ANPVS_15", 1]]],
	                [(["engineer"] call rhsUSAF_fnc_vest), [["rhs_mag_30Rnd_556x45_M855A1_Stanag_Tracer_Red", 9], ["rhsusf_mag_17Rnd_9x19_FMJ", 2]]],
	                [(["engineer"] call rhsUSAF_fnc_backpack), [["rhsusf_m112_mag", 4]]],
	                [(["engineer"] call rhsUSAF_fnc_primWeap), ["rhsusf_acc_SF3P556", "rhsusf_acc_harris_bipod", "rhsusf_acc_anpeq15_bk", "rhsusf_acc_ACOG_RMR", "", "rhs_mag_30Rnd_556x45_M855A1_Stanag_Tracer_Red"]],
	                ["", []],
	                [(["engineer"] call rhsUSAF_fnc_handWeap), ["rhsusf_acc_omega9k", "", "acc_flashlight_pistol", "", "rhsusf_mag_17Rnd_9x19_FMJ"]],
	                (["engineer"] call rhsUSAF_fnc_helmet), nil, nil, nil, nil, nil, "", nil, nil];

private _grenadier = [[(["grenadier"] call rhsUSAF_fnc_uniform), [["rhsusf_ANPVS_15", 1]]],
					  [(["grenadier"] call rhsUSAF_fnc_vest), [["rhs_mag_30Rnd_556x45_M855A1_Stanag_Tracer_Red", 8], ["rhsusf_mag_17Rnd_9x19_FMJ", 1], ["3Rnd_HE_Grenade_shell", 3]]],
					  ["", []],
					  [(["grenadier"] call rhsUSAF_fnc_primWeap), ["rhsusf_acc_SF3P556", "", "rhsusf_acc_anpeq15_bk", "rhsusf_acc_ACOG_RMR", "3Rnd_HE_Grenade_shell", "rhs_mag_30Rnd_556x45_M855A1_Stanag_Tracer_Red"]],
                      ["", []],
                      [(["grenadier"] call rhsUSAF_fnc_handWeap), ["rhsusf_acc_omega9k", "", "acc_flashlight_pistol", "", "rhsusf_mag_17Rnd_9x19_FMJ"]],
                      (["grenadier"] call rhsUSAF_fnc_helmet), nil, nil, nil, nil, nil, "", nil, nil];

private _rifleman = [[(["rifleman"] call rhsUSAF_fnc_uniform), [["rhsusf_ANPVS_15", 1]]],
                     [(["rifleman"] call rhsUSAF_fnc_vest), [["rhs_mag_30Rnd_556x45_M855A1_Stanag_Tracer_Red", 9], ["rhsusf_mag_17Rnd_9x19_FMJ", 2]]],
                     ["", []],
                     [(["rifleman"] call rhsUSAF_fnc_primWeap), ["rhsusf_acc_SF3P556", "rhsusf_acc_harris_bipod", "rhsusf_acc_anpeq15_bk", "rhsusf_acc_ACOG_RMR", "", "rhs_mag_30Rnd_556x45_M855A1_Stanag_Tracer_Red"]],
                     ["rhs_weap_M136", []],
                     [(["rifleman"] call rhsUSAF_fnc_handWeap), ["rhsusf_acc_omega9k", "", "acc_flashlight_pistol", "", "rhsusf_mag_17Rnd_9x19_FMJ"]],
                     (["rifleman"] call rhsUSAF_fnc_helmet), nil, nil, nil, nil, nil, "", nil, nil];

private _jtac = [[(["jtac"] call rhsUSAF_fnc_uniform), [["rhsusf_ANPVS_15", 1]]],
                 [(["jtac"] call rhsUSAF_fnc_vest), [["rhs_mag_30Rnd_556x45_M855A1_Stanag_Tracer_Red", 9], ["rhsusf_mag_17Rnd_9x19_FMJ", 2]]],
                 [(["jtac"] call rhsUSAF_fnc_backpack), []],
                 [(["jtac"] call rhsUSAF_fnc_primWeap), ["rhsusf_acc_SF3P556", "rhsusf_acc_harris_bipod", "rhsusf_acc_anpeq15_bk", "rhsusf_acc_ACOG_RMR", "", "rhs_mag_30Rnd_556x45_M855A1_Stanag_Tracer_Red"]],
                 ["", []],
                 [(["jtac"] call rhsUSAF_fnc_handWeap), ["rhsusf_acc_omega9k", "", "acc_flashlight_pistol", "", "rhsusf_mag_17Rnd_9x19_FMJ"]],
                 (["jtac"] call rhsUSAF_fnc_helmet), nil, nil, nil, nil, nil, "", nil, nil];

private _hPilot = [[(["hPilot"] call rhsUSAF_fnc_uniform), []],
                   ["", [["rhs_mag_30Rnd_556x45_M855A1_Stanag_Tracer_Red", 3], ["rhsusf_mag_17Rnd_9x19_FMJ", 2]]],
                   ["", []],
                   [(["hPilot"] call rhsUSAF_fnc_primWeap), ["rhsusf_acc_SF3P556", "", "rhsusf_acc_anpeq15_bk", "rhsusf_acc_eotech_xps3", "", "rhs_mag_30Rnd_556x45_M855A1_Stanag_Tracer_Red"]],
                   ["", []],
                   [(["hPilot"] call rhsUSAF_fnc_handWeap), ["rhsusf_acc_omega9k", "", "acc_flashlight_pistol", "", "rhsusf_mag_17Rnd_9x19_FMJ"]],
                   (["hPilot"] call rhsUSAF_fnc_helmet), nil, nil, nil, nil, nil, "rhsusf_ANPVS_15", nil, nil];

private _jPilot = [[(["jPilot"] call rhsUSAF_fnc_uniform), []],
                  [(["jPilot"] call rhsUSAF_fnc_vest), [["rhs_mag_30Rnd_556x45_M855A1_Stanag_Tracer_Red", 3]]],
                  [(["jPilot"] call rhsUSAF_fnc_backpack), []],
                  [(["jPilot"] call rhsUSAF_fnc_primWeap), ["rhsusf_acc_SF3P556", "", "rhsusf_acc_anpeq15_bk", "rhsusf_acc_eotech_xps3", "", "rhs_mag_30Rnd_556x45_M855A1_Stanag_Tracer_Red"]],
                  ["", []],
                  ["", []],
                  (["jPilot"] call rhsUSAF_fnc_helmet), nil, nil, nil, nil, nil, "", nil, nil];

private _crew = [[(["crew"] call rhsUSAF_fnc_uniform), [["rhsusf_ANPVS_15", 1]]],
                 [(["crew"] call rhsUSAF_fnc_vest), [["rhs_mag_30Rnd_556x45_M855A1_Stanag_Tracer_Red", 4], ["rhsusf_mag_17Rnd_9x19_FMJ", 2]]],
                 [(["crew"] call rhsUSAF_fnc_backpack), []],
                 [(["crew"] call rhsUSAF_fnc_primWeap), ["rhsusf_acc_SF3P556", "", "rhsusf_acc_anpeq15_bk", "rhsusf_acc_eotech_xps3", "", "rhs_mag_30Rnd_556x45_M855A1_Stanag_Tracer_Red"]],
                 ["", []],
                 [(["crew"] call rhsUSAF_fnc_handWeap), ["rhsusf_acc_omega9k", "", "acc_flashlight_pistol", "", "rhsusf_mag_17Rnd_9x19_FMJ"]],
                 (["crew"] call rhsUSAF_fnc_helmet), nil, nil, nil, nil, nil, "", nil, nil];

private _mortar = [[(["mortar"] call rhsUSAF_fnc_uniform), [["rhsusf_ANPVS_15", 1]]],
                   [(["mortar"] call rhsUSAF_fnc_vest), [["rhs_mag_30Rnd_556x45_M855A1_Stanag_Tracer_Red", 4], ["rhsusf_mag_17Rnd_9x19_FMJ", 2]]],
                   ["", []],
                   [(["mortar"] call rhsUSAF_fnc_primWeap), ["rhsusf_acc_SF3P556", "", "rhsusf_acc_anpeq15_bk", "rhsusf_acc_eotech_xps3", "", "rhs_mag_30Rnd_556x45_M855A1_Stanag_Tracer_Red"]],
                   ["", []],
                   [(["mortar"] call rhsUSAF_fnc_handWeap), ["rhsusf_acc_omega9k", "", "acc_flashlight_pistol", "", "rhsusf_mag_17Rnd_9x19_FMJ"]],
                   (["mortar"] call rhsUSAF_fnc_helmet), nil, nil, nil, nil, nil, "", nil, nil];

private _uavOp = [[(["uavOp"] call rhsUSAF_fnc_uniform), [["rhsusf_ANPVS_15", 1]]],
                  [(["uavOp"] call rhsUSAF_fnc_vest), [["rhs_mag_30Rnd_556x45_M855A1_Stanag_Tracer_Red", 4], ["rhsusf_mag_17Rnd_9x19_FMJ", 2]]],
				  ["", []],
                  [(["uavOp"] call rhsUSAF_fnc_primWeap), ["rhsusf_acc_anpeq15_bk", "rhsusf_acc_eotech_xps3", "", "rhs_mag_30Rnd_556x45_M855A1_Stanag_Tracer_Red"]],
                  ["", []],
                  [(["uavOp"] call rhsUSAF_fnc_handWeap), ["rhsusf_acc_omega9k", "", "acc_flashlight_pistol", "", "rhsusf_mag_17Rnd_9x19_FMJ"]],
                  (["uavOp"] call rhsUSAF_fnc_helmet), nil, nil, "B_UavTerminal", nil, nil, "", nil, nil];

private _spotter = [[(["spotter"] call rhsUSAF_fnc_uniform), []],
                    [(["spotter"] call rhsUSAF_fnc_vest), [["rhsusf_20Rnd_762x51_m118_special_Mag", 3], ["rhsusf_20Rnd_762x51_m993_Mag", 3], ["rhsusf_mag_17Rnd_9x19_FMJ", 1], ["Leupold_Mk4", 1]]],
                    ["", []],
                    [(["spotter"] call rhsUSAF_fnc_primWeap), ["", "rhsusf_acc_harris_bipod", "rhsusf_acc_anpeq15side_bk", "rhsusf_acc_ACOG_MDO", "", "rhsusf_20Rnd_762x51_m118_special_Mag"]],
                    ["", []],
                    [(["spotter"] call rhsUSAF_fnc_handWeap), ["rhsusf_acc_omega9k", "", "acc_flashlight_pistol", "", "rhsusf_mag_17Rnd_9x19_FMJ"]],
                    (["spotter"] call rhsUSAF_fnc_helmet), nil, nil, nil, nil, nil, "", nil, nil];

_out set [RL_USAF, [_hq, _sl, _tl, _medic, _lmg, _hmg, _assHMG, _aa, _assAA, _at, _assAT, _sniper, _marksman,
                 _repair, _demo, _engineer, _grenadier, _rifleman, _jtac, _hPilot, _jPilot, _crew,
                 _mortar, _uavOp, _spotter]];

//------------------------------------------------------------ Base Atmosphere Role Loadout RHS USAF

BALO_USAF = RL_USAF + 1;

private _medic = [[(["medic"] call rhsUSAF_fnc_uniform), []],
                  [(["medic"] call rhsUSAF_fnc_vest), []],
                  ["", []],
                  ["", []],
                  ["", []],
                  ["", []],
                  "", "", "", "", "", "", "", "", ""];
private _gear = [[(["hq"] call rhsUSAF_fnc_uniform), []],
                 ["", []],
                 ["", []],
                 ["", []],
                 ["", []],
                 ["", []],
                 "", "", "", "", "", "", "", "", ""];
private _support = [[(["hq"] call rhsUSAF_fnc_uniform), []],
                    ["", []],
                    ["", []],
                    ["", []],
                    ["", []],
                    ["", []],
                    "", "", "", "", "", "", "", "", ""];
private _default = [[(["hq"] call rhsUSAF_fnc_uniform), []],
                    ["", []],
                    ["", []],
                    ["", []],
                    ["", []],
                    ["", []],
                    "", "", "", "", "", "", "", "", ""];

_out set [BALO_USAF, [_medic, _gear, _support, _default]];

//------------------------------------------------------------ FINITO, return

_out
