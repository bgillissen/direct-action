class rhsUSAF {
	tag = "rhsUSAF";
	class functions {
		class assets { file="feats\assets\rhs\USAF\assets.sqf"; };
		class backpack { file="feats\assets\rhs\USAF\backpack.sqf"; };
		class handWeap { file="feats\assets\rhs\USAF\handWeap.sqf"; };
		class helmet { file="feats\assets\rhs\USAF\helmet.sqf"; };
		class init { file="feats\assets\rhs\USAF\init.sqf"; };
		class postInit { file="core\dummy.sqf"; };
		class preInit { file="feats\assets\rhs\USAF\preInit.sqf"; };
		class primWeap { file="feats\assets\rhs\USAF\primWeap.sqf"; };
		class uniform { file="feats\assets\rhs\USAF\uniform.sqf"; };
		class vest { file="feats\assets\rhs\USAF\vest.sqf"; };
	};
};
