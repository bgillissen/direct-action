/*
@filename: feats\assets\rhs\USAF\backpack.sqf
Author:
	Ben
Description:
	run on server
	call by feats\assets\rhs\USAF\assets.sqf
	return a RHS USAF backpack depending on given role and MAP_KEYWORDS
Params:
	STRING	unit role
Environment:
	missionNamespace:
		MAP_KEYWORDS
Return:
	STRING	a backpack classname
*/
params ["_role"];

if ( _role isEqualTo "jPilot" ) exitWith { "B_Parachute" };

if ( ("jungle" in MAP_KEYWORDS) || ("wood" in MAP_KEYWORDS) ) exitWith {
    if ( _role in ["at", "assAT", "medic", "aa", "assAT"] ) exitWith { "B_Carryall_cbr" }; 
	"rhsusf_falconii_coy" 
};

if ( ("desert" in MAP_KEYWORDS) || ("dry" in MAP_KEYWORDS) ) exitWith {
	if ( _role in ["at", "assAT", "medic", "aa", "assAT"] ) exitWith { "B_Carryall_cbr" };
	"rhsusf_falconii_mc"
};

"rhsusf_falconii"
