/*
@filename: feats\assets\rhs\USAF\handWeap.sqf
Credits:
  Rainman
Author:
	Ben
Description:
	run on server
	call by feats\assets\rhs\USAF\assets.sqf
	return a RHS USAF hand weapon depending on given role and MAP_KEYWORDS
Params:
	STRING	unit role
Environment:
	missionNamespace:
		MAP_KEYWORDS
Return:
	STRING	a hand weapon classname
*/
params['_role'];

if ( _role in ["hq"] ) exitWith { "" };

"rhsusf_weap_glock17g4"
