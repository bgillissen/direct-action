/*
@filename: feats\assets\rhs\USAF\primWeap.sqf
Author:
	Ben
Description:
	run on server
	call by feats\assets\rhs\USAF\assets.sqf
	return a RHS USAF primary weapon depending on given role and MAP_KEYWORDS
Params:
	STRING	unit role
Environment:
	missionNamespace:
		MAP_KEYWORDS
Return:
	STRING	a primary weapon classname
*/
params['_role'];

if ( _role isEqualTo "hmg" ) exitWith { "rhs_weap_m240B" };
if ( _role isEqualTo "lmg" ) exitWith { "rhs_weap_m249_pip_S" };
if ( _role in ["grenadier", "tl", "sl"] ) exitWith { "rhs_weap_hk416d145_m320" };
if ( _role in ["marksman", "spotter"] ) exitWith { "rhs_weap_sr25_ec" };
if ( _role in ["hPilot", "jPilot", "crew", "uavOp", "mortar"] ) exitWith { "rhs_weap_hk416d10" };

private _isDesert = (["desert", "dry"] call assets_fnc_mapKeywords);
private _isWood = (["jungle", "wood"] call assets_fnc_mapKeywords);

private _weap = "rhs_weap_hk416d145";

if ( _isDesert ) then {
	if ( _role isEqualTo "sniper" ) then {_weap = "rhs_weap_m40a5_d"; };
} else {
	if ( _isWood ) then {
		if ( _role isEqualTo "sniper" ) then {_weap = "rhs_weap_m40a5_wd"; };
	} else {
		if ( _role isEqualTo "sniper" ) then {_weap = "rhs_weap_m40a5"; };
	};
};

_weap
