/*
@filename: feats\assets\rhs\USAF\init.sqf
Author:
	Ben
Description:
	run on server,
	implent RHS USAF assets
*/
["rhs", [["USAF", (call rhsUSAF_fnc_assets), [1]]]] call assets_fnc_implent;