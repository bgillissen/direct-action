class uk3cbVeh {
	tag = "uk3cbVeh";
	class functions {
		class assets { file="feats\assets\uk3cb\vehicles\assets.sqf"; };
		class init { file="feats\assets\uk3cb\vehicles\init.sqf"; };
		class postInit { file="core\dummy.sqf"; };
		class preInit { file="core\dummy.sqf";  };
	};
};
