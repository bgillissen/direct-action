/*
@filename: feats\assets\uk3cb\vehicles\assets.sqf
Author:
	Ben
Description:
	run on server
	call by feats\assets\uk3cb\vehicles\init.sqf
	return the 3CB Vehicles assets
*/

private _isDesert = (["desert", "dry"] call assets_fnc_mapKeywords);
private _isWood = (["jungle", "wood"] call assets_fnc_mapKeywords);
private _isWinter = (["winter"] call assets_fnc_mapKeywords);

private _out = [];

//------------------------------------------------------------ Allowed Vehicles 3Cb Vehicles

AV_UK3CBVEH = 0;

private _heli = [];
private _plane = [];
private _tank = [];

_out set [AV_UK3CBVEH, [_heli, _plane, _tank]];

//------------------------------------------------------------ supplyDrop 3Cb Vehicles

SD_UK3CBVEH = AV_UK3CBVEH + 1;

private _backpacks = [];
private _items = [];
private _weapons = [];
private _ammo = [];
private _crates = [];
private _vehicles = [];

_out set [SD_UK3CBVEH, [_backpacks, _items, _weapons, _ammo, _crates, _vehicles]];

//------------------------------------------------------------ Rewards 3Cb Vehicles

R_UK3CBVEH = SD_UK3CBVEH + 1;

private _rewards = [];

_out set [R_UK3CBVEH, _rewards];


//------------------------------------------------------------ Spawn 3Cb Vehicles

S_UK3CBVEH = R_UK3CBVEH + 1;

private _rt = [];
private _crates = [];
private _pGroups = [];
private _sGroups = [];
private _pilot = [];
private _crew = [];
private _officer = [];
private _garrison = [];
private _aa = [];
private _arti = [];
private _static = [];
private _cas = [];
private _tank = [];
private _apc = [];
private _car = [];
private _carArmed = [];
private _aPatrol = [];
private _civ = [];
private _truck = [];

_out set [S_UK3CBVEH, [_rt, _crates, _pGroups, _sGroups, _pilot, _crew, _officer, _garrison, _civ,
                _aa, _arti, _static, _cas, _tank, _apc, _car, _carArmed, _aPatrol]];

//------------------------------------------------------------ Vehicles 3Cb Vehicles

BV_UK3CBVEH = S_UK3CBVEH + 1;

private _car = [];
private _carArmed = [];
private _apc = [];
private _tank = [];
private _aaTank = [];
private _planeCAS = [];
private _planeAA = [];
private _planeTransport = [];
private _uav = [];
private _heliSmall = [];
private _heliSmallArmed = [];
private _heliMedium = [];
private _heliMedEvac = [];
private _heliBig = [];
private _heliAttack = [];
private _boatSmall = [];
private _boatAttack = [];
private _boatBig = [];
private _sub = [];
private _landMedic = [];
private _repair = [];
private _fuel = [];
private _ammo = [];
private _quad = [];
private _artiTank = [];
private _artiCannon = [];
private _artiTube = [];

_out set [BV_UK3CBVEH, [_car, _carArmed, _apc, _tank, _aa, _planeCAS, _planeAA, _planeTransport, _uav,
                _heliSmall, _heliSmallArmed, _heliMedium, _heliMedEvac, _heliBig, _heliAttack,
                _boatSmall, _boatAttack, _boatBig, _sub, _landMedic, _repair, _fuel, _ammo, _truck, _quad,
                 _artiTank, _artiCannon, _artiTube]];
                 
//------------------------------------------------------------ FINITO, return

_out