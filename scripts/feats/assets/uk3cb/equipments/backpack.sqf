/*
@filename: feats\assets\3cb\equipment\backpack.sqf
Author:
	Ben
Description:
	run on server
	call by feats\assets\3cb\equipments\assets.sqf
	return a 3CB Equipments backpack depending on given role and MAP_KEYWORDS
Params:
	STRING	unit role
Environment:
	missionNamespace:
		MAP_KEYWORDS
Return:
	STRING	a backpack classname
*/
params ["_role"];

if ( ("jungle" in MAP_KEYWORDS) || ("wood" in MAP_KEYWORDS) ) exitWith {
    if ( _role in ["at", "assAT", "medic", "aa", "assAT"] ) exitWith { "" }; 
	"" 
};

if ( ("desert" in MAP_KEYWORDS) || ("dry" in MAP_KEYWORDS) ) exitWith {
	if ( _role in ["at", "assAT", "medic", "aa", "assAT"] ) exitWith { "" };
	""
};

if ( "winter" in MAP_KEYWORDS ) exitWith {
	if ( _role in ["at", "assAT", "medic", "aa", "assAT"] ) exitWith { "" };
	""
};

""