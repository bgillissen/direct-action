/*
@filename: feats\assets\3cb\equipments\uniform.sqf
Author:
	Ben
Description:
	run on server
	call by feats\assets\3cb\equipments\assets.sqf
	return a 3CB Equipments uniform depending on given role and MAP_KEYWORDS
Params:
	STRING	unit role
Environment:
	missionNamespace:
		MAP_KEYWORDS
Return:
	STRING	an uniform classname
*/
params ["_role"];

if ( _role in ["hq", "jPilot", "hPilot"] ) exitWith { "" };

if ("wood" in MAP_KEYWORDS) exitWith { "" };

if ("jungle" in MAP_KEYWORDS) exitWith { "" };

if ("winter" in MAP_KEYWORDS) exitWith { "" };

private _uniform = "";

if ( ("desert" in MAP_KEYWORDS) || ("dry" in MAP_KEYWORDS) ) then {
  _uniform = "";
  if ( _role isEqualTo "crew" ) then { _uniform = ""; };
};

_uniform