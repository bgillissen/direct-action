/*
@filename: feats\assets\3cb\equipments\helmet.sqf
Author:
	Ben
Description:
	run on server
	call by feats\assets\3cb\equipments\assets.sqf
	return a 3CB Equipments helmet depending on given role and MAP_KEYWORDS
Params:
	STRING	unit role
Environment:
	missionNamespace:
		MAP_KEYWORDS
Return:
	STRING	a helmet classname
*/
params ["_role"];

if ( _role isEqualTo "hq" ) exitWith { "" };
if ( _role isEqualTo "hPilot" ) exitWith { "" };
if ( _role isEqualTo "jPilot" ) exitWith { "" };

if ( ("desert" in MAP_KEYWORDS) || ("dry" in MAP_KEYWORDS) ) exitWith {
	if ( _role in ["tl", "sl", "medic", "hmg", "lmg", "assHMG", "engineer", "mortar", "uavOp"] ) exitWith { "" };
	if ( _role in ["marksman", "spotter", "sniper"] ) exitWith { "" };
	if ( _role isEqualTo "crew" ) exitWith { "" };
	if ( _role isEqualTo "jtac" ) exitWith { "" };
	""
};
if ( "winter" in MAP_KEYWORDS ) exitWith {
    if ( _role in ["tl", "sl", "medic", "hmg", "lmg", "assHMG", "engineer", "mortar", "uavOp"] ) exitWith { "" };
	if ( _role in ["marksman", "spotter", "sniper"] ) exitWith { "" };
	if ( _role isEqualTo "crew" ) exitWith { "" };
	if ( _role isEqualTo "jtac" ) exitWith { "" };
	""
};

if ( _role isEqualTo "crew" ) exitWith { "" };

""
