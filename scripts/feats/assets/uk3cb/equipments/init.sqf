/*
@filename: feats\assets\uk3cb\equipments\init.sqf
Author:
	Ben
Description:
	run on server,
	implent 3CB weapons assets
*/
["uk3cb", [["uk3cbEquip", (call uk3cbEquip_fnc_assets), [1]]]] call assets_fnc_implent;