/*
@filename: feats\assets\uk3cb\equipments\assets.sqf
Author:
	Ben
Description:
	run on server
	call by feats\assets\uk3cb\equipments\init.sqf
	return the 3CB Equipments assets
*/

private _isDesert = (["desert", "dry"] call assets_fnc_mapKeywords);
private _isWood = (["jungle", "wood"] call assets_fnc_mapKeywords);
private _isWinter = (["winter"] call assets_fnc_mapKeywords);

private _out = [];

//------------------------------------------------------------ Arsenal 3CB Equipments

A_UK3CBEQUIP = 0;

private _backpacks = ["UK3CB_BAF_Equipment"] call assets_fnc_getBackpacks;
_backpacks pushback "B_UAV_01_backpack_F";
private _items = ["UK3CB_BAF_Equipment"] call assets_fnc_getItems;
private _weapons = ["UK3CB_BAF_Equipment"] call assets_fnc_getWeapons;
private _ammo = ["UK3CB_BAF_Equipment"] call assets_fnc_getMagazines;

_out set [A_UK3CBEQUIP, [_backpacks, _items, _weapons, _ammo]];

//------------------------------------------------------------ Restricted Gear 3CB Equipments

RG_UK3CBEQUIP = A_UK3CBEQUIP + 1;

private _launcher = [];
private _mg = [];
private _sRifle = [];
private _mRifle = [];
private _sScope = [];
private _mScope = [];
private _oScope = [];
private _mbrItem = [];
private _backpack = [];

_out set [RG_UK3CBEQUIP, [_launcher, _mg, _sRifle, _mRifle, _sScope, _mScope, _oScope, _mbrItem, _backpack]];

//------------------------------------------------------------ supplyDrop 3CB Equipments

SD_UK3CBEQUIP = RG_UK3CBEQUIP + 1;

private _backpacks = [];
private _items = [];
private _weapons = [];
private _ammo = [];
private _crates = [];
private _vehicles = [];

_out set [SD_UK3CBEQUIP, [_backpacks, _items, _weapons, _ammo, _crates, _vehicles]];

                 
//------------------------------------------------------------ Vehicles Cargo 3CB Equipments

VC_UK3CBEQUIP = SD_UK3CBEQUIP + 1;

private _car = [[],[],[],[]];
private _carArmed = [[],[],[],[]];
private _apc = [[],[],[],[]];
private _tank = [[],[],[],[]];
private _aaTank = [[],[],[],[]];
private _planeCAS = [[],[],[],[]];
private _planeAA = [[],[],[],[]];
private _planeTransport = [[],[],[],[]];
private _uav = [[],[],[],[]];
private _heliSmall = [[],[],[],[]];
private _heliSmallArmed = [[],[],[],[]];
private _heliMedium = [[],[],[],[]];
private _heliMedEvac = [[],[],[],[]];
private _heliBig = [[],[],[],[]];
private _heliAttack = [[],[],[],[]];
private _boatSmall = [[],[],[],[]];
private _boatAttack = [[],[],[],[]];
private _boatBig = [[],[],[],[]];
private _sub = [[],[],[],[]];
private _landMedic = [[],[],[],[]];
private _repair = [[],[],[],[]];
private _fuel = [[],[],[],[]];
private _ammo = [[],[],[],[]];
private _truck = [[],[],[],[]];
private _quad = [[],[],[],[]];
private _artiTank = [[],[],[],[]];
private _artiCannon = [[],[],[],[]];
private _artiTube = [[],[],[],[]];

_out set [VC_UK3CBEQUIP, [_car, _carArmed, _apc, _tank, _aaTank, _planeCAS, _planeAA, _planeTransport, _uav,
                 _heliSmall, _heliSmallArmed, _heliMedium, _heliMedEvac, _heliBig, _heliAttack,
                 _boatSmall, _boatAttack, _boatBig, _sub, _landMedic, _repair, _fuel, _ammo, _truck, _quad,
                 _artiTank, _artiCannon, _artiTube]];

//------------------------------------------------------------ Role Loadout 3CB Equipments
/*
 [uniform, [inUniform]], 
 [vest, [inVest]], 
 [backpack, [inBackpack]], 
 [primWeapon, [muzzle, bipod, acc, scope, ammo2, ammo]] 
 [secWeapon, [muzzle, bipod, acc, scope, ammo]], 
 [handWeapon, [muzzle, bipod, acc, scope, ammo]], 
 helmet, face, comm, terminal, map, bino, nv, watch, compass
*/

RL_UK3CBEQUIP = VC_UK3CBEQUIP + 1;

private _hq = [[(["hq"] call uk3cbEquip_fnc_uniform), []],
               [(["hq"] call uk3cbEquip_fnc_vest), []],
               ["", []],
               ["", []],
               ["", []],
               ["", []],
               (["hq"] call uk3cbEquip_fnc_helmet), nil, nil, nil, nil, nil, "", nil, nil];
               
private _sl = [[(["sl"] call uk3cbEquip_fnc_uniform), []],
			   [(["sl"] call uk3cbEquip_fnc_vest), []],
               [nil, []],
               [nil, []],
               ["", []],
               [nil, []],
               (["sl"] call uk3cbEquip_fnc_helmet), nil, nil, nil, nil, nil, "", nil, nil];

private _tl = [[(["tl"] call uk3cbEquip_fnc_uniform), []],
               [(["tl"] call uk3cbEquip_fnc_vest), []],
               [nil, []],
               [nil, []],
               ["", []],
               [nil, []],
               (["tl"] call uk3cbEquip_fnc_helmet), nil, nil, nil, nil, nil, "", nil, nil];

private _medic = [[(["medic"] call uk3cbEquip_fnc_uniform), []],
                  [(["medic"] call uk3cbEquip_fnc_vest), []],
                  [(["medic"] call uk3cbEquip_fnc_backpack), []],
                  [nil, []],
                  ["", []],
                  [nil, []],
                  (["medic"] call uk3cbEquip_fnc_helmet), nil, nil, nil, nil, nil, "", nil, nil];

private _lmg = [[(["lmg"] call uk3cbEquip_fnc_uniform), []],
                [(["lmg"] call uk3cbEquip_fnc_vest), []],
                ["", []],
                [nil, []],
                ["", []],
                [nil, []],
                (["lmg"] call uk3cbEquip_fnc_helmet), nil, nil, nil, nil, nil, "", nil, nil];

private _hmg = [[(["hmg"] call uk3cbEquip_fnc_uniform), []],
                [(["hmg"] call uk3cbEquip_fnc_vest), []],
                [(["hmg"] call uk3cbEquip_fnc_backpack), []],
                [nil, []],
                ["", []],
                [nil, []],
                (["hmg"] call uk3cbEquip_fnc_helmet), nil, nil, nil, nil, nil, "", nil, nil];

private _assHMG = [[(["assHMG"] call uk3cbEquip_fnc_uniform), []],
                   [(["assHMG"] call uk3cbEquip_fnc_vest), []],
                   [(["assHMG"] call uk3cbEquip_fnc_backpack), []],
                   [nil, []],
                   ["", []],
                   [nil, []],
                   (["assHMG"] call uk3cbEquip_fnc_helmet), nil, nil, nil, nil, nil, "", nil, nil];

private _aa = [[(["aa"] call uk3cbEquip_fnc_uniform), []],
               [(["aa"] call uk3cbEquip_fnc_vest), []],
               [(["aa"] call uk3cbEquip_fnc_backpack), []],
               [nil, []],
               [nil, []],
               [nil, []],
               (["aa"] call uk3cbEquip_fnc_helmet), nil, nil, nil, nil, nil, "", nil, nil];

private _assAA = [[(["assAA"] call uk3cbEquip_fnc_uniform), []],
                  [(["assAA"] call uk3cbEquip_fnc_vest), []],
                  [(["assAA"] call uk3cbEquip_fnc_backpack), []],
                  [nil, []],
                  ["", []],
                  [nil, []],
                  (["assAA"] call uk3cbEquip_fnc_helmet), nil, nil, nil, nil, nil, "", nil, nil];

private _at = [[(["at"] call uk3cbEquip_fnc_uniform), []],
               [(["at"] call uk3cbEquip_fnc_vest), []],
               [(["at"] call uk3cbEquip_fnc_backpack), []],
               [nil, []],
               [nil, []],
               [nil, []],
               (["at"] call uk3cbEquip_fnc_helmet), nil, nil, nil, nil, nil, "", nil, nil];

private _assAT = [[(["assAT"] call uk3cbEquip_fnc_uniform), []],
                  [(["assAT"] call uk3cbEquip_fnc_vest), []],
                  [(["assAT"] call uk3cbEquip_fnc_backpack), []],
                  [nil, []],
                  ["", []],
                  [nil, []],
                  (["assAT"] call uk3cbEquip_fnc_helmet), nil, nil, nil, nil, nil, "", nil, nil];

private _sniper = [[(["sniper"] call uk3cbEquip_fnc_uniform), []],
                   [(["sniper"] call uk3cbEquip_fnc_vest), []],
                   ["", []],
                   [nil, []],
                   ["", []],
                   [nil, []],
                   (["sniper"] call uk3cbEquip_fnc_helmet), nil, nil, nil, nil, nil, "", nil, nil];

private _marksman = [[(["marksman"] call uk3cbEquip_fnc_uniform), []],
                     [(["marksman"] call uk3cbEquip_fnc_vest), []],
                     ["", []],
                     [nil, []],
                     ["", []],
                     [nil, []],
                     (["marksman"] call uk3cbEquip_fnc_helmet), nil, nil, nil, nil, nil, "", nil, nil];

private _repair = [[(["repair"] call uk3cbEquip_fnc_uniform), []],
                   [(["repair"] call uk3cbEquip_fnc_vest), []],
                   [(["repair"] call uk3cbEquip_fnc_backpack), []],
                   [nil, []],
                   ["", []],
                   [nil, []],
                   (["repair"] call uk3cbEquip_fnc_helmet), nil, nil, nil, nil, nil, "", nil, nil];

private _demo = [[(["demo"] call uk3cbEquip_fnc_uniform), []],
	                 [(["demo"] call uk3cbEquip_fnc_vest), []],
	                 [(["demo"] call uk3cbEquip_fnc_backpack), []],
	                 [nil, []],
	                 ["", []],
	                 [nil, []],
	                 (["demo"] call uk3cbEquip_fnc_helmet), nil, nil, nil, nil, nil, "", nil, nil];

private _engineer = [[(["engineer"] call uk3cbEquip_fnc_uniform), []],
	                [(["engineer"] call uk3cbEquip_fnc_vest), []],
	                [(["engineer"] call uk3cbEquip_fnc_backpack), []],
	                [nil, []],
	                ["", []],
	                [nil, []],
	                (["engineer"] call uk3cbEquip_fnc_helmet), nil, nil, nil, nil, nil, "", nil, nil];

private _grenadier = [[(["grenadier"] call uk3cbEquip_fnc_uniform), []],
					  [(["grenadier"] call uk3cbEquip_fnc_vest), []],
					  ["", []],
					  [nil, []],
                      ["", []],
                      [nil, []],
                      (["grenadier"] call uk3cbEquip_fnc_helmet), nil, nil, nil, nil, nil, "", nil, nil];

private _rifleman = [[(["rifleman"] call uk3cbEquip_fnc_uniform), []],
                     [(["rifleman"] call uk3cbEquip_fnc_vest), []],
                     ["", []],
                     [nil, []],
                     [nil, []],
                     [nil, []],
                     (["rifleman"] call uk3cbEquip_fnc_helmet), nil, nil, nil, nil, nil, "", nil, nil];

private _jtac = [[(["jtac"] call uk3cbEquip_fnc_uniform), []],
                 [(["jtac"] call uk3cbEquip_fnc_vest), []],
                 [(["jtac"] call uk3cbEquip_fnc_backpack), []],
                 [nil, []],
                 ["", []],
                 [nil,[]],
                 (["jtac"] call uk3cbEquip_fnc_helmet), nil, nil, nil, nil, nil, "", nil, nil];

private _hPilot = [[(["hPilot"] call uk3cbEquip_fnc_uniform), []],
                   ["", []],
                   ["", []],
                   [nil, []],
                   ["", []],
                   [nil, []],
                   (["hPilot"] call uk3cbEquip_fnc_helmet), nil, nil, nil, nil, nil, "", nil, nil];

private _jPilot = [[(["jPilot"] call uk3cbEquip_fnc_uniform), []],
                  [(["jPilot"] call uk3cbEquip_fnc_vest), []],
                  [nil, []],
                  ["", []],
                  ["", []],
                  [nil, []],
                  (["jPilot"] call uk3cbEquip_fnc_helmet), nil, nil, nil, nil, nil, "", nil, nil];

private _crew = [[(["crew"] call uk3cbEquip_fnc_uniform), []],
                 [(["crew"] call uk3cbEquip_fnc_vest), []],
                 [(["crew"] call uk3cbEquip_fnc_backpack), []],
                 [nil, []],
                 ["", []],
                 [nil, []],
                 (["crew"] call uk3cbEquip_fnc_helmet), nil, nil, nil, nil, nil, "", nil, nil];

private _mortar = [[(["mortar"] call uk3cbEquip_fnc_uniform), []],
                   [(["mortar"] call uk3cbEquip_fnc_vest), []],
                   ["", []],
                   [nil, []],
                   ["", []],
                   [nil, []],
                   (["mortar"] call uk3cbEquip_fnc_helmet), nil, nil, nil, nil, nil, "", nil, nil];

private _uavOp = [[(["uavOp"] call uk3cbEquip_fnc_uniform), []],
                  [(["uavOp"] call uk3cbEquip_fnc_vest), []],
				  ["", []],
                  [nil, []],
                  ["", []],
                  [nil, []],
                  (["uavOp"] call uk3cbEquip_fnc_helmet), nil, nil, "B_UavTerminal", nil, nil, "", nil, nil];

private _spotter = [[(["spotter"] call uk3cbEquip_fnc_uniform), []],
                    [(["spotter"] call uk3cbEquip_fnc_vest), []],
                    ["", []],
                    [nil, []],
                    ["", []],
                    [nil, []],
                    (["spotter"] call uk3cbEquip_fnc_helmet), nil, nil, nil, nil, nil, "", nil, nil];

_out set [RL_UK3CBEQUIP, [_hq, _sl, _tl, _medic, _lmg, _hmg, _assHMG, _aa, _assAA, _at, _assAT, _sniper, _marksman,
                 _repair, _demo, _engineer, _grenadier, _rifleman, _jtac, _hPilot, _jPilot, _crew,
                 _mortar, _uavOp, _spotter]];

//------------------------------------------------------------ Base Atmosphere Role Loadout 3CB Equipments

BALO_UK3CBEQUIP = RL_UK3CBEQUIP + 1;

private _medic = [[(["medic"] call uk3cbEquip_fnc_uniform), []],
                  [(["medic"] call uk3cbEquip_fnc_vest), []],
                  ["", []],
                  ["", []],
                  ["", []],
                  ["", []],
                  "", "", "", "", "", "", "", "", ""];
private _gear = [[(["hq"] call uk3cbEquip_fnc_uniform), []],
                 ["", []],
                 ["", []],
                 ["", []],
                 ["", []],
                 ["", []],
                 "", "", "", "", "", "", "", "", ""];
private _support = [[(["hq"] call uk3cbEquip_fnc_uniform), []],
                    ["", []],
                    ["", []],
                    ["", []],
                    ["", []],
                    ["", []],
                    "", "", "", "", "", "", "", "", ""];
private _default = [[(["hq"] call uk3cbEquip_fnc_uniform), []],
                    ["", []],
                    ["", []],
                    ["", []],
                    ["", []],
                    ["", []],
                    "", "", "", "", "", "", "", "", ""];

_out set [BALO_UK3CBEQUIP, [_medic, _gear, _support, _default]];

//------------------------------------------------------------ FINITO, return

_out