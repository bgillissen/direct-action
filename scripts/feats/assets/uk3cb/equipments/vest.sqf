/*
@filename: feats\assets\rhs\USAF\vest.sqf
Author:
	Ben
Description:
	run on server
	call by feats\assets\rhs\USAF\assets.sqf
	return a RHS USAF vest depending on given role and MAP_KEYWORDS
Params:
	STRING	unit role
Environment:
	missionNamespace:
		MAP_KEYWORDS
Return:
	STRING	a vest classname
*/
params ["_role"];

private _vest = "";

if ( ("desert" in MAP_KEYWORDS) || ("dry" in MAP_KEYWORDS) ) then {
	_vest = "rhsusf_spcs_ocp_rifleman";
	if ( _role in ["tl", "sl", "grenadier"] ) then { _vest = "rhsusf_iotv_ocp_Grenadier"; };
	if ( _role isEqualTo "medic" ) then { _vest = "rhsusf_iotv_ocp_Medic"; };
	if ( _role isEqualTo "hPilot") then { _vest = ""; };
	if ( _role in ["jPilot", "hq"] ) then { _vest = "V_Chestrig_blk"; };
	if ( _role in ["crew", "sniper", "spotter", "uavOp", "mortar"] ) then { _vest = "rhsusf_spcs_ocp"; };
	if ( _role in ["hmg", "lmg", "assHMG"] ) then { _vest = "rhsusf_iotv_ocp_SAW"; };
} else {
    _vest = "rhsusf_spc_corpsman";
	if ( _role in ["tl", "sl"] ) then { _vest = "rhsusf_spc_squadleader"; };
	if ( _role in ["hmg", "lmg"] ) then { _vest = "rhsusf_spc_mg"; };
	if ( _role in ["hPilot", "crew"] ) then { _vest = "rhsusf_spc_crewman"; };
	if ( _role isEqualTo "grenadier" ) then { _vest = "rhsusf_spc_teamleader"; };
	if ( _role isEqualTo "marksman" ) then { _vest = "rhsusf_spc_marksman"; };
	if ( _role isEqualTo "rifleman" ) then { _vest = "rhsusf_spc_rifleman"; };
};

_vest
