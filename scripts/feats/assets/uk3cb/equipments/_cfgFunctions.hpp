class uk3cbEquip {
	tag = "uk3cbEquip";
	class functions {
		class assets { file="feats\assets\uk3cb\equipments\assets.sqf"; };
		class init { file="feats\assets\uk3cb\equipments\init.sqf"; };
		class postInit { file="core\dummy.sqf"; };
		class preInit { file="core\dummy.sqf";  };
		class backpack { file="feats\assets\uk3cb\equipments\backpack.sqf"; };
		class helmet { file="feats\assets\uk3cb\equipments\helmet.sqf"; };
		class uniform { file="feats\assets\uk3cb\equipments\uniform.sqf"; };
		class vest { file="feats\assets\uk3cb\equipments\vest.sqf"; };
	};
};
