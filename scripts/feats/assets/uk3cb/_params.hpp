class spawn_uk3cb {
	title = "Add 3CB units to enemy units spawn pool";
	values[] = {0,1};
	texts[] = {"no", "yes"};
	default = 1;
};
class gear_uk3cb {
	title = "Which 3CB gear is available in arsenal";
	values[] = {0,1,2,3};
	texts[] = {"none", "player side only", "player side + allies", "all"};
	default = 1;
};
class reward_uk3cb {
	title = "Add 3CB vehicles to reward pool";
	values[] = {0,1,2,3};
	texts[] = {"no", "player side only", "player side + allies", "all"};
	default = 2;
};
class bv_uk3cb {
	title = "Add 3CB vehicle to base vehicle pool";
	values[] = {0,1,2,3};
	texts[] = {"no", "player side only", "player side + allies", "all"};
	default = 2;
};
class rl_uk3cb {
	title = "Players spawn with 3CB loadout";
	values[] = {0,1};
	texts[] = {"no", "yes"};
	default = 1;
};
