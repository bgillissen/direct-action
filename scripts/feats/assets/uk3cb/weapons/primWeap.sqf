/*
@filename: feats\assets\uk3cb\weapons\primWeap.sqf
Author:
	Ben
Description:
	run on server
	call by feats\assets\uk3cb\weapons\assets.sqf
	return a 3CB Weapons primary weapon (rifle) depending on given role and MAP_KEYWORDS
Params:
	STRING	unit role
Environment:
	missionNamespace:
		MAP_KEYWORDS
Return:
	STRING	a primary weapon classname
*/

params ["_role"];

if ( _role isEqualTo "hq" ) exitWith { "" };

""