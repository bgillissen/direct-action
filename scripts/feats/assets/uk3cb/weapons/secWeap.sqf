/*
@filename: feats\assets\uk3cb\weapons\secWeap.sqf
Author:
	Ben
Description:
	run on server
	call by feats\assets\uk3cb\weapons\assets.sqf
	return a 3CB Weapons secondary weapon (launcher) depending on given role and MAP_KEYWORDS
Params:
	STRING	unit role
Environment:
	missionNamespace:
		MAP_KEYWORDS
Return:
	STRING	a secondary weapon classname
*/

params ["_role"];

if !( _role in ["aa", "at", "rifleman"] ) exitWith { "" };

""