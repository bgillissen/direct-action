/*
@filename: feats\assets\uk3cb\weapons\handWeap.sqf
Author:
	Ben
Description:
	run on server
	call by feats\assets\uk3cb\weapons\assets.sqf
	return a 3CB Weapons hand weapon (pistol) depending on given role and MAP_KEYWORDS
Params:
	STRING	unit role
Environment:
	missionNamespace:
		MAP_KEYWORDS
Return:
	STRING	a hznd weapon classname
*/

params ["_role"];

if ( _role isEqualTo "hq" ) exitWith { "" };

""