/*
@filename: feats\assets\uk3cb\weapons\assets.sqf
Author:
	Ben
Description:
	run on server
	call by feats\assets\uk3cb\weapons\init.sqf
	return the 3CB Weapons assets
*/

private _isDesert = (["desert", "dry"] call assets_fnc_mapKeywords);
private _isWood = (["jungle", "wood"] call assets_fnc_mapKeywords);
private _isWinter = (["winter"] call assets_fnc_mapKeywords);

private _out = [];

//------------------------------------------------------------ Arsenal 3CB Weapons

A_UK3CBWEAP = 0;

private _backpacks = ["UK3CB_BAF_Weapons"] call assets_fnc_getBackpacks;
_backpacks pushback "B_UAV_01_backpack_F";
private _items = ["UK3CB_BAF_Weapons"] call assets_fnc_getItems;
private _weapons = ["UK3CB_BAF_Weapons"] call assets_fnc_getWeapons;
private _ammo = ["UK3CB_BAF_Weapons"] call assets_fnc_getMagazines;

_out set [A_UK3CBWEAP, [_backpacks, _items, _weapons, _ammo]];

//------------------------------------------------------------ Restricted Gear 3CB Weapons

RG_UK3CBWEAP = A_UK3CBWEAP + 1;

private _launcher = [];
private _mg = [];
private _sRifle = [];
private _mRifle = [];
private _sScope = [];
private _mScope = [];
private _oScope = [];
private _mbrItem = [];
private _backpack = [];

_out set [RG_UK3CBWEAP, [_launcher, _mg, _sRifle, _mRifle, _sScope, _mScope, _oScope, _mbrItem, _backpack]];

//------------------------------------------------------------ supplyDrop 3CB Weapons

SD_UK3CBWEAP = RG_UK3CBWEAP + 1;

private _backpacks = [];
private _items = [];
private _weapons = [];
private _ammo = [];
private _crates = [];
private _vehicles = [];

_out set [SD_UK3CBWEAP, [_backpacks, _items, _weapons, _ammo, _crates, _vehicles]];

                 
//------------------------------------------------------------ Vehicles Cargo 3CB Weapons

VC_UK3CBWEAP = SD_UK3CBWEAP + 1;

private _car = [[],[],[],[]];
private _carArmed = [[],[],[],[]];
private _apc = [[],[],[],[]];
private _tank = [[],[],[],[]];
private _aaTank = [[],[],[],[]];
private _planeCAS = [[],[],[],[]];
private _planeAA = [[],[],[],[]];
private _planeTransport = [[],[],[],[]];
private _uav = [[],[],[],[]];
private _heliSmall = [[],[],[],[]];
private _heliSmallArmed = [[],[],[],[]];
private _heliMedium = [[],[],[],[]];
private _heliMedEvac = [[],[],[],[]];
private _heliBig = [[],[],[],[]];
private _heliAttack = [[],[],[],[]];
private _boatSmall = [[],[],[],[]];
private _boatAttack = [[],[],[],[]];
private _boatBig = [[],[],[],[]];
private _sub = [[],[],[],[]];
private _landMedic = [[],[],[],[]];
private _repair = [[],[],[],[]];
private _fuel = [[],[],[],[]];
private _ammo = [[],[],[],[]];
private _truck = [[],[],[],[]];
private _quad = [[],[],[],[]];
private _artiTank = [[],[],[],[]];
private _artiCannon = [[],[],[],[]];
private _artiTube = [[],[],[],[]];

_out set [VC_UK3CBWEAP, [_car, _carArmed, _apc, _tank, _aaTank, _planeCAS, _planeAA, _planeTransport, _uav,
                 _heliSmall, _heliSmallArmed, _heliMedium, _heliMedEvac, _heliBig, _heliAttack,
                 _boatSmall, _boatAttack, _boatBig, _sub, _landMedic, _repair, _fuel, _ammo, _truck, _quad,
                 _artiTank, _artiCannon, _artiTube]];

//------------------------------------------------------------ Role Loadout 3CB Weapons
/*
 [uniform, [inUniform]], 
 [vest, [inVest]], 
 [backpack, [inBackpack]], 
 [primWeapon, [muzzle, bipod, acc, scope, ammo2, ammo]] 
 [secWeapon, [muzzle, bipod, acc, scope, ammo]], 
 [handWeapon, [muzzle, bipod, acc, scope, ammo]], 
 helmet, face, comm, terminal, map, bino, nv, watch, compass
*/

RL_UK3CBWEAP = VC_UK3CBWEAP + 1;

private _hq = [[nil, []],
               [nil, []],
               [nil, []],
               [(["hq"] call uk3cbWeap_fnc_primWeap), []],
               [(["hq"] call uk3cbWeap_fnc_secWeap), []],
               [(["hq"] call uk3cbWeap_fnc_handWeap), []],
               nil, nil, nil, nil, nil, nil, nil, nil, nil];
               
private _sl = [[nil, []],
			   [nil, []],
               [nil, []],
               [(["sl"] call uk3cbWeap_fnc_primWeap), []],
               [(["sl"] call uk3cbWeap_fnc_secWeap), []],
               [(["sl"] call uk3cbWeap_fnc_handWeap), []],
               nil, nil, nil, nil, nil, nil, nil, nil, nil];

private _tl = [[nil, []],
			   [nil, []],
               [nil, []],
               [(["tl"] call uk3cbWeap_fnc_primWeap), []],
               [(["tl"] call uk3cbWeap_fnc_secWeap), []],
               [(["tl"] call uk3cbWeap_fnc_handWeap), []],
               nil, nil, nil, nil, nil, nil, nil, nil, nil];

private _medic = [[nil, []],
			   	  [nil, []],
                  [nil, []],
                  [(["medic"] call uk3cbWeap_fnc_primWeap), []],
                  [(["medic"] call uk3cbWeap_fnc_secWeap), []],
                  [(["medic"] call uk3cbWeap_fnc_handWeap), []],
                  nil, nil, nil, nil, nil, nil, nil, nil, nil];

private _lmg = [[nil, []],
			    [nil, []],
                [nil, []],
                [(["lmg"] call uk3cbWeap_fnc_primWeap), []],
                [(["lmg"] call uk3cbWeap_fnc_secWeap), []],
                [(["lmg"] call uk3cbWeap_fnc_handWeap), []],
                nil, nil, nil, nil, nil, nil, nil, nil, nil];

private _hmg = [[nil, []],
			    [nil, []],
                [nil, []],
                [(["hmg"] call uk3cbWeap_fnc_primWeap), []],
                [(["hmg"] call uk3cbWeap_fnc_secWeap), []],
                [(["hmg"] call uk3cbWeap_fnc_handWeap), []],
                nil, nil, nil, nil, nil, nil, nil, nil, nil];

private _assHMG = [[nil, []],
			       [nil, []],
                   [nil, []],
                   [(["assHMG"] call uk3cbWeap_fnc_primWeap), []],
                   [(["assHMG"] call uk3cbWeap_fnc_secWeap), []],
                   [(["assHMG"] call uk3cbWeap_fnc_handWeap), []],
                   nil, nil, nil, nil, nil, nil, nil, nil, nil];

private _aa = [[nil, []],
			   [nil, []],
               [nil, []],
               [(["aa"] call uk3cbWeap_fnc_primWeap), []],
               [(["aa"] call uk3cbWeap_fnc_secWeap), []],
               [(["aa"] call uk3cbWeap_fnc_handWeap), []],
               nil, nil, nil, nil, nil, nil, nil, nil, nil];

private _assAA = [[nil, []],
			   	  [nil, []],
               	  [nil, []],
                  [(["assAA"] call uk3cbWeap_fnc_primWeap), []],
                  [(["assAA"] call uk3cbWeap_fnc_secWeap), []],
                  [(["assAA"] call uk3cbWeap_fnc_handWeap), []],
                  nil, nil, nil, nil, nil, nil, nil, nil, nil];

private _at = [[nil, []],
			   [nil, []],
               [nil, []],
               [(["at"] call uk3cbWeap_fnc_primWeap), []],
               [(["at"] call uk3cbWeap_fnc_secWeap), []],
               [(["at"] call uk3cbWeap_fnc_handWeap), []],
               nil, nil, nil, nil, nil, nil, nil, nil, nil];

private _assAT = [[nil, []],
			      [nil, []],
                  [nil, []],
                  [(["assAT"] call uk3cbWeap_fnc_primWeap), []],
                  [(["assAT"] call uk3cbWeap_fnc_secWeap), []],
                  [(["assAT"] call uk3cbWeap_fnc_handWeap), []],
                  nil, nil, nil, nil, nil, nil, nil, nil, nil];

private _sniper = [[nil, []],
			       [nil, []],
                   [nil, []],
                   [(["sniper"] call uk3cbWeap_fnc_primWeap), []],
                   [(["sniper"] call uk3cbWeap_fnc_secWeap), []],
                   [(["sniper"] call uk3cbWeap_fnc_handWeap), []],
                   nil, nil, nil, nil, nil, nil, nil, nil, nil];

private _marksman = [[nil, []],
			   		 [nil, []],
               		 [nil, []],
               		 [(["marksman"] call uk3cbWeap_fnc_primWeap), []],
               		 [(["marksman"] call uk3cbWeap_fnc_secWeap), []],
               		 [(["marksman"] call uk3cbWeap_fnc_handWeap), []],
               		 nil, nil, nil, nil, nil, nil, nil, nil, nil];

private _repair = [[nil, []],
			       [nil, []],
                   [nil, []],
                   [(["repair"] call uk3cbWeap_fnc_primWeap), []],
                   [(["repair"] call uk3cbWeap_fnc_secWeap), []],
                   [(["repair"] call uk3cbWeap_fnc_handWeap), []],
                   nil, nil, nil, nil, nil, nil, nil, nil, nil];

private _demo = [[nil, []],
			     [nil, []],
                 [nil, []],
                 [(["demo"] call uk3cbWeap_fnc_primWeap), []],
                 [(["demo"] call uk3cbWeap_fnc_secWeap), []],
                 [(["demo"] call uk3cbWeap_fnc_handWeap), []],
                 nil, nil, nil, nil, nil, nil, nil, nil, nil];

private _engineer = [[nil, []],
					 [nil, []],
               		 [nil, []],
               		 [(["engineer"] call uk3cbWeap_fnc_primWeap), []],
               		 [(["engineer"] call uk3cbWeap_fnc_secWeap), []],
               		 [(["engineer"] call uk3cbWeap_fnc_handWeap), []],
               		 nil, nil, nil, nil, nil, nil, nil, nil, nil];

private _grenadier = [[nil, []],
			   		  [nil, []],
					  [nil, []],
               		  [(["grenadier"] call uk3cbWeap_fnc_primWeap), []],
               		  [(["grenadier"] call uk3cbWeap_fnc_secWeap), []],
               		  [(["grenadier"] call uk3cbWeap_fnc_handWeap), []],
               		  nil, nil, nil, nil, nil, nil, nil, nil, nil];

private _rifleman = [[nil, []],
			   		 [nil, []],
               		 [nil, []],
               		 [(["rifleman"] call uk3cbWeap_fnc_primWeap), []],
               		 [(["rifleman"] call uk3cbWeap_fnc_secWeap), []],
               		 [(["rifleman"] call uk3cbWeap_fnc_handWeap), []],
               		 nil, nil, nil, nil, nil, nil, nil, nil, nil];

private _jtac = [[nil, []],
				 [nil, []],
                 [nil, []],
               	 [(["jtac"] call uk3cbWeap_fnc_primWeap), []],
               	 [(["jtac"] call uk3cbWeap_fnc_secWeap), []],
               	 [(["jtac"] call uk3cbWeap_fnc_handWeap), []],
               	 nil, nil, nil, nil, nil, nil, nil, nil, nil];

private _hPilot = [[nil, []],
			   	   [nil, []],
               	   [nil, []],
               	   [(["hPilot"] call uk3cbWeap_fnc_primWeap), []],
               	   [(["hPilot"] call uk3cbWeap_fnc_secWeap), []],
               	   [(["hPilot"] call uk3cbWeap_fnc_handWeap), []],
               	   nil, nil, nil, nil, nil, nil, nil, nil, nil];

private _jPilot = [[nil, []],
				   [nil, []],
               	   [nil, []],
               	   [(["jPilot"] call uk3cbWeap_fnc_primWeap), []],
               	   [(["jPilot"] call uk3cbWeap_fnc_secWeap), []],
               	   [(["jPilot"] call uk3cbWeap_fnc_handWeap), []],
               	   nil, nil, nil, nil, nil, nil, nil, nil, nil];

private _crew = [[nil, []],
			     [nil, []],
                 [nil, []],
                 [(["crew"] call uk3cbWeap_fnc_primWeap), []],
                 [(["crew"] call uk3cbWeap_fnc_secWeap), []],
                 [(["crew"] call uk3cbWeap_fnc_handWeap), []],
                 nil, nil, nil, nil, nil, nil, nil, nil, nil];

private _mortar = [[nil, []],
			       [nil, []],
                   [nil, []],
                   [(["motar"] call uk3cbWeap_fnc_primWeap), []],
                   [(["mortar"] call uk3cbWeap_fnc_secWeap), []],
                   [(["mortar"] call uk3cbWeap_fnc_handWeap), []],
                   nil, nil, nil, nil, nil, nil, nil, nil, nil];

private _uavOp = [[nil, []],
			   	  [nil, []],
               	  [nil, []],
               	  [(["uavOp"] call uk3cbWeap_fnc_primWeap), []],
               	  [(["uavOp"] call uk3cbWeap_fnc_secWeap), []],
               	  [(["uavOp"] call uk3cbWeap_fnc_handWeap), []],
               	  nil, nil, nil, nil, nil, nil, nil, nil, nil];

private _spotter = [[nil, []],
			   		[nil, []],
               		[nil, []],
               		[(["spotter"] call uk3cbWeap_fnc_primWeap), []],
               		[(["spotter"] call uk3cbWeap_fnc_secWeap), []],
               		[(["spotter"] call uk3cbWeap_fnc_handWeap), []],
               		nil, nil, nil, nil, nil, nil, nil, nil, nil];

_out set [RL_UK3CBWEAP, [_hq, _sl, _tl, _medic, _lmg, _hmg, _assHMG, _aa, _assAA, _at, _assAT, _sniper, _marksman,
                 _repair, _demo, _engineer, _grenadier, _rifleman, _jtac, _hPilot, _jPilot, _crew,
                 _mortar, _uavOp, _spotter]];

//------------------------------------------------------------ FINITO, return

_out