class uk3cbWeap {
	tag = "uk3cbWeap";
	class functions {
		class assets { file="feats\assets\uk3cb\weapons\assets.sqf"; };
		class init { file="feats\assets\uk3cb\weapons\init.sqf"; };
		class postInit { file="core\dummy.sqf"; };
		class preInit { file="core\dummy.sqf";  };
		class primWeap { file="feats\assets\uk3cb\weapons\primWeap.sqf"; };
		class secWeap { file="feats\assets\uk3cb\weapons\secWeap.sqf"; };
		class handWeap { file="feats\assets\uk3cb\weapons\handWeap.sqf"; };
	};
};
