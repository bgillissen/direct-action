/*
@filename: feats\assets\playerPostInit.sqf
Author:
	Ben
Description:
	run on player,
	make sure player loaded the required mods present on server
*/

#include "_debug.hpp";

waitUntil { !PLAYER_INIT };

private _stop = false;
{
    private _name = getText(_x >> "name");
	if ( _name isEqualto "" ) then { _name = (configName _x); };
	private _cfgPatch = getText(_x >> "cfgPatch");
    private _isClient = ( getNumber(_x >> "clientMod") > 0 );
    private _isPresent = true;
	if !( _cfgPatch isEqualto "" ) then {
		_isPresent = isClass(configFile >> "CfgPatches" >> _cfgPatch);
	};
	_isOnServer = missionNamespace getVariable [format["MOD_%1", _name], false];
    if ( _isOnServer || _isClient ) then {
		private _isRequired = ( getNumber(_x >> "required") > 0 );
		if ( _isRequired && !_isPresent ) exitWith {
            #ifdef DEBUG
            private _debug = format["required mod '%1' is not loaded, calling mission end", _name];
            debug(LL_ERR, _debug);
            #endif
            _stop = true; 
        	endMission _name; 
		}; 
        if ( !_stop && _isPresent ) then {
			private _fnc =  format["%1_fnc_postInit", _name];
			if !( isNil _fnc ) then {
				call compile format["call %1", _fnc];
			#ifdef DEBUG
			} else {
				private _debug = format["%1 postInit failed, function %2 is not defined)", _name, _fnc];
				debug(LL_ERR, _debug);
			#endif	
			};
		};
    };
} forEach ("true" configClasses (missionConfigFile >> "settings" >> "assets"));

nil