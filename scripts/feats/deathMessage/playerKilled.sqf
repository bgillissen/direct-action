/*
@filename: feats\deathMessage\playerKilled.sqf
Author:
	Ben
Description:
	run on player,
	broadcast a message to all other players
*/

#include "_debug.hpp"

if ( (["deathMessage"] call core_fnc_getParam) == 0 ) exitWith {
    #ifdef DEBUG
    debug(LL_DEBUG, "disabled by mission parameter, abording");
    #endif
    nil
};

params ["_victim", "_killer"];

private _targets = allPlayers - [player] - (entities "HeadlessClient_F");

private _msg = format["%1 died", profileName];

[_targets, _msg] call common_fnc_systemChat;

nil