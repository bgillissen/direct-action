
#include "_debug.hpp"

params ["_thing", "_actions"];

private _name = configName _thing;

if ( isNil _name ) exitWith {
	#ifdef DEBUG
	private _debug = format["OBJ %1 is nil!", _name];
	debug(LL_WARN, _debug);
	#endif    
};	

private _obj = missionNamespace getVariable _name;

if ( isNull _obj ) exitWith {
	#ifdef DEBUG
	private _debug = format["OBJ %1 is null!", _name];
	debug(LL_WARN, _debug);
	#endif    
};

BA_obj pushback [_obj, _actions];