class baseAtmosphere {
	npcInsignia = "TFUDA_npc";
	buildingTypes[] = {"Building"};
	class ALTIS {
		#include "Altis\_settings.hpp"
	};
	class MALDEN {
		#include "Malden\_settings.hpp"
	};
	class NAM2 {
		#include "Nam2\_settings.hpp"
	};
	class PREI_KHMAOCH_LUONG {
		#include "PreiKhmaochLuong\_settings.hpp"
	};
	class TANOA {
		#include "Tanoa\_settings.hpp"
	};
	class VR {
		#include "virtualReality\_settings.hpp"
	};
};
