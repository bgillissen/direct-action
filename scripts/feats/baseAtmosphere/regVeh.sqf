
#include "_debug.hpp"

params ["_thing", "_actions"];

private _poolName = getText( _thing >> "pool");
private _pool = missionNamespace getVariable format["BV_%1", _poolName];

if ( isNil "_pool" ) exitWith {
    #ifdef DEBUG
	private _debug = format["vehicle pool %1 is nil!", _poolName];
	debug(LL_WARN, _debug);
	#endif	
};

if ( (count _pool) isEqualTo 0 ) exitWith {
    #ifdef DEBUG
	private _debug = format["vehicle pool %1 is empty!", _poolName];
	debug(LL_WARN, _debug);
	#endif	
};
private _name = configName _thing;
private _veh = createVehicle [(selectRandom _pool), (getMarkerPos _name), [], 0, "CAN_COLLIDE"];
_veh setDir (markerDir _name);
_veh lock 3;
_veh allowDamage false;
clearWeaponCargoGlobal _veh;
clearMagazineCargoGlobal _veh;
clearItemCargoGlobal _veh;
clearBackpackCargoGlobal _veh;

BA_veh pushback [_veh, _actions];