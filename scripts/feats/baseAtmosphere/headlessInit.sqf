/*
@filename: feats\baseAtmosphere\headlessInit.sqf
Author:
	Ben
Description:
	run on headless
 	disable damage on base buildings / npc / obj / veh,
*/

#include "_debug.hpp"

{ _x allowDamage false; } forEach BA_buildings;

{
    _x params ["_npc", "_actions"];
    _npc allowDamage false;
} forEach BA_npc;

{
	_x params ["_thing", "_actions"];
	_thing allowDamage false;
} forEach BA_obj;

{
	_x params ["_grp", "_actions"];
	{ _x allowDamage false; } forEach (units _grp);
} forEach BA_patrol;

{
	_x params ["_unit", "_actions"];
	_unit allowDamage false;
} forEach BA_sentry;

{
    _x params ["_veh", "_actions"];
    _veh allowDamage false;
} forEach BA_veh;

nil