class baseAtmosphere : feat_base {
	class server : featServer_base {
		class destroy : featEvent_enable {};
		class init : featEvent_enable { order = 13; };
	};
	class player : featServer_base {
		class destroy : featEvent_enable {};
		class init : featEvent_enable {};
	};
	class headless : featHeadless_base {
		class destroy : featEvent_enable {};
		class init : featEvent_enable {};
	};
};
