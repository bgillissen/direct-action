class baseAtmosphere {
	tag = "baseAtmosphere";
	class functions {
		class headlessInit { file="feats\baseAtmosphere\headlessInit.sqf"; };
		class headlessDestroy { file="feats\baseAtmosphere\headlessDestroy.sqf"; };
		class serverDestroy { file="feats\baseAtmosphere\serverDestroy.sqf"; };
		class serverInit { file="feats\baseAtmosphere\serverInit.sqf"; };
		class playerDestroy { file="feats\baseAtmosphere\playerDestroy.sqf"; };
		class playerInit { file="feats\baseAtmosphere\playerInit.sqf"; };
		class npcLoadout { file="feats\baseAtmosphere\npcLoadout.sqf"; };
		class regMarker { file="feats\baseAtmosphere\regMarker.sqf"; };
		class regNPC { file="feats\baseAtmosphere\regNPC.sqf"; };
		class regObj { file="feats\baseAtmosphere\regObj.sqf"; };
		class regPatrol { file="feats\baseAtmosphere\regPatrol.sqf"; };
		class regSentry { file="feats\baseAtmosphere\regSentry.sqf"; };
		class regVeh { file="feats\baseAtmosphere\regVeh.sqf"; };
		class regZone { file="feats\baseAtmosphere\regZone.sqf"; };
	};
};
