/*
@filename: feats\baseAtmosphere\playerDestroy.sqf
Author:
	Ben
Description:
	run on player,
 	re-enable damage on base buildings,
*/

#include "_debug.hpp"

{ _x allowDamage true; } forEach BA_buildings;

nil