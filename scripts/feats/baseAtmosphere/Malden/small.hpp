class small {
	name = "Small Outpost";
	aoBlacklist[] = {"Saint Jean", "Goisse"};
	class zones {
		class baseSmall {
			name = "BV_mHeli_1";
			radius = 150;
			types[] = {"cleanForced", "noShot", "noEnemy"};
		};
		class basePort {
			name = "BA_arsenal_4";
			radius = 150 ;
			types[] = {"cleanForced", "mortarSafe", "noEnemy", "noDamage", "baseLight", 'noMissions'};
		};
		class baseLarge {
			name = "BV_mHeli_1";
			radius = 800;
			types[] = {"mortarSafe", "noDamage", "baseLight", 'noMissions'};
		};
		class repair {
			name = "BA_repair";
			radius = 10;
			types[] = {"heli", "land"};
		};
		class shipRepair {
			name = "BA_repair_boat";
			radius = 10;
			types[] = {"boat"};
		};
	};
	class heliPad {
		class things {
			class BA_padCrate { type = "obj"; };
		};
		class actions {
			class vehicleLoadout { marker = "BA_vehLoadout"; };
			class support {};
		};
	};
	class arsenals {
		class things {
			class BA_arsenal_1 { type = "obj"; };
			class BA_arsenal_2 { type = "obj"; };
			class BA_arsenal_3 { type = "obj"; };
			class BA_arsenal_4 { type = "obj"; };
		};
		class actions {
			class arsenal { filtered = -1; };
			class viewDistance {};
			class loadGear {};
			class saveGear {};
			class serverRules {};
		};
	};
	class zeus {
		class things {
			class BA_laptop { type = "obj"; };
		};
		class actions {
			class cleanup {};
			class curator {};
			class memberData {};
			class lockServer {};
			class billboard {
				mode = "fixed";
				texture = "zeus";
			};
		};
	};
	class rightBillboard {
		class things {
			class BA_bb_1 { type = "obj"; };
		};
		class actions {
			class billboard {
				mode = "fixed";
				texture = "tfar";
			};
		};
	};
	class leftBillboard {
		class things {
			class BA_bb_2 { type = "obj"; };
		};
		class actions {
			class billboard { mode = "random"; };
			class dateBillboard { textures[] = {"xmas"}; };
		};
	};
	class briefingBillboard {
		class things {
			class BA_screen { type = "obj"; };
		};
		class actions {
			class billboard {
				mode = "fixed";
				texture = "tfu";
			};
			class lecture { type = "dsp"; };
			class uavRecon { type = "dsp"; };
		};
	};
	class MonLaptop {
		class things {
			class BA_laptop { type = "obj"; };
		};
		class actions {
			class lecture { type = "dsp"; };
			class uavRecon { type = "dsp"; };
		};
	};
	class ctrlLaptop {
		class things {
			class BA_laptop { type = "obj"; };
		};
		class actions {
			class lecture { type = "ctrl"; };
			class uavRecon { type = "ctrl"; };
		};
	};
	class flagTFU {
		class things {
			class BA_flag_2 { type = "obj"; };
		};
		class actions {
			class flag { texture = "tfu"; };
		};
	};
	class flagMain {
		class things {
			class BA_flag_1 { type = "obj"; };
		};
		class actions {
			class flag { texture = "tfu"; };
			class environment {};
			class baseLight {};
			class anthem { sfx = "Sound_anthem_tfu"; };
			class vehicleSelection {
				name = "all";
				classes[] = { "helo", "armedCars", "trucks", "ambulance" };
				roles[] = {};
			};
			class switchSideMission {};
		};
	};
	class markers {
		class things {
			class BA_marker_1 { type = "marker"; };
			class BA_marker_2 { type = "marker"; };
		};
		class actions {};
	};
	class speakers {
		class things {
			class BA_speaker { type = "obj";atl=5; };
		};
		class actions {
			class soundSource {};
		};
	};
};
