/*
@filename: feats\baseAtmosphere\Malden\_params.hpp
Author:
	Ben
Description:
		included by feats\maps\_params.hpp
		VR base location mission parameter
*/

class MALDEN_base {
	title = "Malden 2035 Base Location";
	values[] = {0};
	texts[] = {"Small"};
	default = 0;
};
