
#include "_debug.hpp"

params ["_thing", "_actions"];

private _name = configName _thing;
private _type = getText(_thing >> "type");

if ( isNil _name ) exitWith {
	#ifdef DEBUG
	private _debug = format["NPC %1 is nil!", _name];
	debug(LL_WARN, _debug);
	#endif    
};	

private _npc = missionNamespace getVariable _name;

if ( isNull _npc ) exitWith {
	#ifdef DEBUG
	private _debug = format["NPC %1 is null!", _name];
	debug(LL_WARN, _debug);
	#endif    
};

private _grp = (group _npc);
_grp setGroupIdGlobal [(["NPC", allGroups, {groupId _this}] call common_fnc_getUniqueName)];
_grp setFormDir (_npc getVariable "larsDir");
_grp setVariable ["NOLB", true, true];
_grp allowFleeing 0;
_grp setCombatMode "BLUE";
_grp setBehaviour "CARELESS";
_grp setSpeedMode "LIMITED";

_npc setVariable ["NOAI", true, true];
_npc allowDamage false;

[_npc, format["BALO_%1_%2",getText(_thing >> "balo"), PLAYER_SIDE]] call baseAtmosphere_fnc_npcLoadout;

_npc spawn {
    sleep 5;
    [(group _this), true] call dynSim_fnc_set;
	{ _this disableAI _x; } count ["MOVE", "FSM", "TARGET", "AUTOTARGET", "CHECKVISIBLE", "SUPPRESSION", "COVER"];
};

BA_npc pushback [_npc, _actions];