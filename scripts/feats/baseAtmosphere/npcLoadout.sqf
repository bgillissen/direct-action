/*
@filename: feats\baseAtmosphere\npcLoadout.sqf
Author:
	Ben
Description:
	run on server
 	set the npc loadout
 	loadout depends on linked actions, (arsenal, support, medic)
 	now also set the insignia defined as npc in description.ext
*/

#include "_debug.hpp"

params ["_npc", "_loadout"];

private _lo = missionNamespace getVariable [_loadout, []];

if ( count _lo == 0 ) exitWith {
    #ifdef DEBUG
	private _debug = format["NPC loadout '%1' is not defined", _loadout];
    debug(LL_ERR, _debug);
    #endif
};

_lo params["_u", "_v", "_b", "_pw", "_sw", "_hw", "_h", "_f", "_c", "_t", "_m", "_bino", "_n", "_w", "_cp"];

[_npc, _u, _v, _b, _pw, _sw, _hw, _h, _f, _c, _t, _m, _bino, _n, _w, _cp] call common_fnc_setLoadout;

private _insignia = (["baseAtmosphere", "npcInsignia"] call core_fnc_getSetting);
if !( isNil "_insignia" ) then {
	[_npc, _insignia] call global_fnc_setUnitInsignia;	
}; 