/*
@filename: feats\baseAtmosphere\playerInit.sqf
Author:
	Ben
Description:
	run on player
 	disable damage on base buildings / npc / obj / veh,
	set npcs face randomly
*/

#include "_debug.hpp"

{ _x allowDamage false; } forEach BA_buildings;

private _faces = [];

private _prepNPC = {
	if ( count _faces isEqualTo 0 ) then { _faces = BA_faces; };
    private _face = (selectRandom _faces);
    _this setFace _face;
    //_this addAction [format["My face is : %1", _face], {}, [], 0, false, true, "", "true", 4];
    _faces = _faces - [_face]; 
    _this allowDamage false;
};

{
    _x params ["_npc", "_actions"];
    _npc call _prepNPC;
} forEach BA_npc;

{
	_x params ["_thing", "_actions"];
	_thing allowDamage false;
} forEach BA_obj;

{
	_x params ["_grp", "_actions"];
	{ _x call _prepNPC; } forEach (units _grp);
} forEach BA_patrol;

{
	_x params ["_unit", "_actions"];
	_unit call _prepNPC;
} forEach BA_sentry;

{
    _x params ["_veh", "_actions"];
    _veh allowDamage false;
} forEach BA_veh;



nil