/*
@filename: feats\baseAtmosphere\serverDestroy.sqf
Author:
	Ben
Description:
	run on server,
 	despawn the active base composition using LARs,
	remove static base vehicle,
	re-enable damage on base buildings
*/

#include "_debug.hpp"

[BASE_REF] call compo_fnc_remove;

{ deleteVehicle (_x select 0); } forEach BA_veh;
BA_veh = [];

{ _x allowDamage true; } forEach BA_buildings;

nil