
#include "_debug.hpp"

params ["_thing", "_actions"];

private _name = configName _x;
private _type = getText(_thing >> "zoneType");

private "_area";
switch ( _type ) do {
	case 'empty' : { if ( (getMarkerPos _name) isEqualto [0,0,0] ) exitWith {};
					 private _pos = getMarkerPos _name;
    				 _area = [_pos, 0, 0, 0, false, -1];
    			   };
    case 'marker' : { if ( (getMarkerPos _name) isEqualto [0,0,0] ) exitWith {};
        			  _name setMarkerAlpha 0;
        			  _area = _name;
    				};
    case 'obj' : { private _obj = missionNamespace getVariable [_name, objNull];
    			   if ( isNull _obj ) exitWith {};
                   private _pos = getPos _obj;
                   private _radius = getNumber (_x >> "radius");
                   if ( _radius > 0 ) then { _area = [_pos, _radius, _radius, 0, false, -1]; };
                 }; 
};

if ( isNil "_area" ) exitWith {
	#ifdef DEBUG
	private _debug = format["could not compute area for zone %1", _name];
	debug(LL_WARN, _debug);
	#endif  
};

private _types = [];
{ _types pushback (configName _x); } forEach _actions;

if ( "repair" in _types ) then {
	_types append getArray(_thing >> "repair");
};

BA_zones pushback [_area, _types];