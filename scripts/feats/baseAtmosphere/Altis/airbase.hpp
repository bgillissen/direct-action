class airbase {
	name = "Altis airbase";
	aoBlacklist[] = {"Airbase", "Power plant", "Lakka"};
	class bpAndClean {
		class things {
			class ZONE_base { type="zone";zoneType="marker"; };
			class ZONE_port { type="zone";zoneType="marker"; };
			class ZONE_fob { type="zone";zoneType="marker"; };
		};
		class actions {
			class noShot {};
			class cleanForced {};
		};
	};
	class noSpawn {
		class things {
			class ZONE_noSpawn { type="zone";zoneType="marker"; };
		};
		class actions {
			class noMissions {};
		};
	};
	class safeZones {
		class things {
			class ZONE_baseLarge { type="zone";zoneType="marker"; };
			class ZONE_frLarge { type="zone";zoneType="marker"; };
			class ZONE_fobLarge { type="zone";zoneType="marker"; };
			class ZONE_portLarge { type="zone";zoneType="marker"; };
		};
		class actions {
			class mortarSafe {};
			class lootSafe {};
			class noDamage {};
			class baseLight {};
			class noEnemy {};
		};
	};
	class firingRange {
		class things {
			class ZONE_fr { type="zone";zoneType="marker"; };
		};
		class actions {
			class cleanForced {};
			class noGR {};
		};
	};
	class repairs {
		class things {
			class VR_plane { type="zone";zoneType="empty";repair[] = {"plane"}; };
			class VR_heli { type="zone";zoneType="empty";repair[] = {"heli"}; };
			class VR_uav { type="zone";zoneType="empty";repair[] = {"uav"}; };
			class VR_boat { type="zone";zoneType="empty";repair[] = {"boat"}; };
			class VR_land { type="zone";zoneType="empty";repair[] = {"land"}; };
		};
		class actions {
			class repair {};
		};
	};
	class heliSupport {
		class things {
			class BA_npc_support_1 { type = "npc";balo = "support"; };
		};
		class actions {
			class support {};
			class vehicleSelection {
					name = "Helicopters";
					classes[] = { "littleBird", "pawnee", "blackhawk", "medEvac", "bigHeli" };
					roles[] = { "hPilot" };
			};
		};
	};
	class planeSupport {
		class things {
			class BA_npc_support_1 { type = "npc";balo = "support"; };
		};
		class actions {
			class vehicleSelection {
				name = "Planes";
				classes[] = { "c130", "cas", "aa" };
				roles[] = { "jPilot" };
			};
		};
	};
	class medic {
		class things {
			class BA_npc_medic_1 { type = "npc";balo = "medic"; };
		};
		class actions {
			class medic {};
		};
	};
	class vehLoadout {
		class things {
			class BA_npc_vl_1 { type = "npc";balo = "support"; };
		};
		class actions {
			class vehicleLoadout { marker = "BA_vl_1"; };
			class vehicleDeco {
				hiddeMarker = 1; 
				marker = "BA_vl_1";
				type = "Air";
				camPos[] = {10, 4, 4}; 
			};
		};
	};
	class vehDeco {
		class things {
			class BA_npc_vd_1 { type = "npc";balo = "support"; };
		};
		class actions {
			class vehicleDeco { 
				marker = "BA_vd_1";
				type = "LandVehicle";
				camPos[] = {5, 2, 2}; 
			};
		};
	};
	class uavVehSelection {
		class things {
			class BA_npc_support_1 { type = "npc";balo = "gear"; };
		};
		class actions {
			class vehicleSelection {
				name = "UAVs";
				classes[] = { "uavs" };
				roles[] = {"uavOp"};
			};
		};
	};
	class landVehSelection {
		class things {
			class BA_npc_vs_1 { type = "npc";balo = "gear"; };
		};
		class actions {
			class vehicleSelection {
				name = "Land Vehicles";
				classes[] = { "ambulance", "tanks", "apcs", "trucks" };
				roles[] = {};
			};
		};
	};
	class fobLandVehSelection {
		class things {
			class BA_fob_va_1 { type = "obj"; };
		};
		class actions {
			class vehicleSelection {
				name = "FOB Land Vehicles";
				classes[] = { "fobArmedCars", "fobApcs", "fobTanks"};
				roles[] = {};
			};
		};
	};
	class planeSelection {
		class things {
			class BA_npc_support_1 {};
		};
		class actions {
			class vehicleSelection {
				name = "Helicopters";
				classes[] = { "cas", "aa", "c130" };
				roles[] = { "jPilot" };
			};
		};
	};
	class artiSupport {
		class things {
			class BA_as { type = "npc";balo = "support"; };
		};
		class actions {
			class artiSupport { batId = 0; };
		};
	};
	class arsenals {
		class things {
			class BA_npc_va_1 { type = "npc";balo = "gear"; };
			class BA_npc_va_2 { type = "npc";balo = "gear"; };
			class BA_npc_va_3 { type = "npc";balo = "gear"; };
			class BA_hPilot_va_1 { type = "obj"; };
			class BA_port_va_1 { type = "obj"; };
			class BA_fob_va_1 { type = "obj"; };
			class BA_fr_va_1 { type = "obj"; };
			class BA_helipad_va_1 { type = "obj"; };
			class BA_helipad_va_2 { type = "obj"; };
		};
		class actions {
			class arsenal { filtered = -1; };
			class viewDistance {};
			class loadGear {};
			class saveGear {};
			class serverRules {};
		};
	};
	class zeus {
		class things {
			class BA_zeus_1 { type = "obj"; };
		};
		class actions {
			class cleanup {};
			class curator {};
			class memberData {};
			class lockServer {};
			class billboard {
				mode = "fixed";
				texture = "zeus";
			};
		};
	};
	class rightBillboard {
		class things {
			class BA_bb_1 { type = "obj"; };
		};
		class actions {
			class billboard {
				mode = "fixed";
				texture = "tfar";
			};
		};
	};
	class leftBillboard {
		class things {
			class BA_bb_2 { type = "obj"; };
		};
		class actions {
			class billboard { mode = "random"; };
			class dateBillboard { textures[] = {"xmas"}; };
		};
	};
	class briefingBillboard {
		class things {
			class BA_bb_3 { type = "obj"; };
		};
		class actions {
			class billboard {
				mode = "fixed";
				texture = "tfu";
			};
			class lecture { type = "dsp"; };
			class uavRecon { type = "dsp"; };
		};
	};
	class MonLaptop {
		class things {
			class BA_briefLaptop_1 { type = "obj"; };
		};
		class actions {
			class lecture { type = "dsp"; };
			class uavRecon { type = "dsp"; };
		};
	};
	class ctrlLaptop {
		class things {
			class BA_briefLaptop_2 { type = "obj"; };
		};
		class actions {
			class lecture { type = "ctrl"; };
			class uavRecon { type = "ctrl"; };
		};
	};
	class flagTFU {
		class things {
			class BA_flag_3 { type = "obj"; };
			class BA_flag_4 { type = "obj"; };
			class BA_flag_5 { type = "obj"; };
		};
		class actions {
			class flag { texture = "tfu"; };
		};
	};
	class flagMain {
		class things {
			class BA_flag_1 { type = "obj"; };
			class BA_flag_3 { type = "obj"; };
			class BA_flag_4 { type = "obj"; };
			class BA_flag_5 { type = "obj"; };
		};
		class actions {
			class flag { texture = "tfu"; };
			class environment {};
			class baseLight {};
			class anthem { sfx = "Sound_anthem_tfu"; };
			class vehicleSelection {
				name = "Land Vehicles";
				classes[] = { "cars", "armedCars" };
				roles[] = {};
			};
			class switchSideMission {};
			class nutsKick {};
		};
	};
	class flagBDA {
		class things {
			class BA_flag_2 { type = "obj"; };
		};
		class actions {
			class flag { texture = "bda"; };
		};
	};
	class markers {
		class things {
			class BA_marker_1 { type = "marker"; };
			class BA_marker_2 { type = "marker"; };
			class BA_marker_3 { type = "marker"; };
			class BA_marker_4 { type = "marker"; };
			class BA_marker_5 { type = "marker"; };
			class BA_marker_6 { type = "marker"; };
		};
		class actions {};
	};
	class speakers {
		class things {
			class BA_speaker_1 { type = "obj";atl=5; };
			class BA_speaker_2 { type = "obj";atl=5; };
			class BA_speaker_3 { type = "obj";atl=5; };
			class BA_speaker_4 { type = "obj";atl=5; };
		};
		class actions {
			class soundSource {};
		};
	};
	class vehicles {
		class things {
			class BA_lMedic_1 {
				type = "veh";
				pool = "landMedic";
			};
			class BA_lRepair_1 {
				type = "veh";
				pool = "repair";
			};
			class BA_pRepair_1 {
				type = "veh";
				pool = "repair";
			};
			class BA_pRepair_2 {
				type = "veh";
				pool = "fuel";
			};
		};
		class actions {};
	};
	class defences {
		class things {
			class BA_sentry_1 { type="sentry";loadout="rifleman"; };
			class BA_sentry_2 { type="sentry";loadout="rifleman"; };
			class BA_sentry_3 { type="sentry";loadout="sniper"; };
			class BA_patrol_1 { 
				type="patrol";
				combatMode="WHITE";
				behaviour="SAFE";
				speedMode="LIMITED";
				formation = "STAG COLUMN";
				loadouts[]= {"rifleman", "aa", "at", "grenadier", "lmg"}; 
			};
			class BA_patrol_2 { 
				type="patrol";
				combatMode="WHITE";
				behaviour="SAFE";
				speedMode="LIMITED";
				formation = "STAG COLUMN";
				loadouts[]= {"rifleman", "aa", "at", "grenadier", "lmg"}; 
			};
		};
		class actions {};
	};
};
