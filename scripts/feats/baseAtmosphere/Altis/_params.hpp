/*
@filename: feats\maps\TANOA\_params.hpp
Author:
	Ben
Description:
		included by feats\maps\_params.hpp
		VR base location mission parameter
*/

class ALTIS_base {
	title = "Altis Base Location";
	values[] = {0};
	texts[] = {"Airbase"};
	default = 0;
};
