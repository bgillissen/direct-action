/*
@filename: feats\baseAtmosphere\headlessDestroy.sqf
Author:
	Ben
Description:
	run on headless,
 	re-enable damage on base buildings,
*/

#include "_debug.hpp"

{ _x allowDamage true; } forEach BA_buildings;

nil