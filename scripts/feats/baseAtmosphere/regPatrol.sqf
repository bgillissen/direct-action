
#include "_debug.hpp"

params ["_thing", "_actions"];

private _name = configName _thing;

if ( isNil _name ) exitWith {
	#ifdef DEBUG
	private _debug = format["Patrol %1 is nil!", _name];
	debug(LL_WARN, _debug);
	#endif    
};	

private _grp = missionNamespace getVariable _name;

if ( isNull _grp ) exitWith {
	#ifdef DEBUG
	private _debug = format["Patrol Group %1 is null!", _name];
	debug(LL_WARN, _debug);
	#endif    
};

if !( _grp isEqualType grpNull ) exitWith {
	#ifdef DEBUG
	private _debug = format["Patrol Group %1 is not a group!", _name];
	debug(LL_WARN, _debug);
	#endif    
};

private _loadouts = getArray(_thing >> "loadouts");

{
	[_x, format["RL_%1_%2", (selectRandom _loadouts), PLAYER_SIDE]] call baseAtmosphere_fnc_npcLoadout;
	_x setVariable ["NOAI", true, true];
	_x setVariable["MT_hidden", true, true];
	_x allowDamage false;
	_x setSkill 1;
	
	//todo firedEH to rearm
} forEach (units _grp);

private _newGrp = createGroup [PLAYER_SIDE, false];
[_newGrp, true] call dynSim_fnc_set;
_newGrp setGroupIdGlobal [(["PATROL", allGroups, {groupId _this}] call common_fnc_getUniqueName)];
_newGrp setVariable ["NOLB", true, true];

[(leader _grp)] joinSilent _newGrp;

_newGrp allowFleeing 0;
_newGrp copyWaypoints _grp;

(units _grp) joinSilent _newGrp;

[_newGrp, _thing] spawn {
	params ['_newGrp', '_thing'];
    sleep 5;
    private _combatMode = getText( _thing >> "combatMode" );
	private _behaviour = getText( _thing >> "behaviour" );
	private _speedMode = getText( _thing >> "speedMode" );
	private _formation = getText( _thing >> "formation" );
	if !( _combatMode isEqualTo "" ) then { _newGrp setCombatMode _combatMode; };
	if !( _behaviour isEqualTo "" ) then { _newGrp setBehaviour _behaviour; };
	if !( _speedMode isEqualTo "" ) then { _newGrp setSpeedMode _speedMode; };
	if !( _formation isEqualTo "" ) then { _newGrp setFormation _formation; };
    {
    	private _unit = _x;
		{ _unit disableAI _x; } count ["SUPPRESSION", "COVER"];
		_unit setUnitPos "UP";
	} forEach (units _newgrp);
};

BA_patrol pushback [_newGrp, _actions];