class khesanh {
	name = "Khe Sanh";
	aoBlacklist[] = {"Khe Sanh"};
	class zones {
		class baseSmall {
			name = "BS_inf";
			radius = 400;
			types[] = {"noShot", "noEnemy"};
		};
		class baseLarge {
			name = "BS_inf";
			radius = 1000;
			types[] = {"cleanForced", "mortarSafe", "noDamage", "baseLight"};
		};
		class basePort {
			name = "BA_port_va_1";
			radius = 300;
			types[] = {"cleanForced", "mortarSafe", "noEnemy", "noDamage", "baseLight"};
		};
		class fobDepot {
			name = "BA_marker_?";
			radius = 200;
			types[] = {"cleanForced", "mortarSafe", "noEnemy"};
		};
		class frSmall {
			name = "BA_fr_va_1";
			radius = 20;
			types[] = {"cleanForced", "noGR"};
		};
		class frLarge {
			name = "BA_fr_va_1";
			radius = 500;
			types[] = {"noMortar"};
		};
	};
	class support {
		class things {
			class BA_npc_support_1 { type = "npc";balo = "support"; };
		};
		class actions {
			class support {};
		};
	};
	class medic {
		class things {
			class BA_npc_medic_1 { type = "npc";balo = "medic"; };
		};
		class actions {
			class medic {};
		};
	};
	class arsenals {
		class things {
			class BA_va_1 { type = "obj"; };
			class BA_va_2 { type = "obj"; };
			class BA_va_3 { type = "obj"; };
			class BA_va_4 { type = "obj"; };
		};
		class actions {
			class arsenal { filtered = -1; };
			class viewDistance {};
			class loadGear {};
			class saveGear {};
			class serverRules {};
		};
	};
	class zeus {
		class things {
			class BA_zeus_1 { type = "obj"; };
		};
		class actions {
			class curator {};
			class memberData {};
			class billboard {
				mode = "fixed";
				texture = "zeus";
			};
		};
	};
	class rightBillboard {
		class things {
			class BA_bb_1 { type = "obj"; };
		};
		class actions {
			class billboard {
				mode = "fixed";
				texture = "tfar";
			};
		};
	};
	class leftBillboard {
		class things {
			class BA_bb_2 { type = "obj"; };
		};
		class actions {
			class billboard { mode = "random"; };
			class dateBillboard { textures[] = {"xmas"}; };
		};
	};
	class flagTFU {
		class things {
			class BA_flag_1 { type = "obj"; };
			class BA_flag_5 { type = "obj"; };
		};
		class actions {
			class flag { texture = "tfu"; };
			class environment {};
			class baseLight {};
		};
	};
	class flagBDA {
		class things {
			class BA_flag_3 { type = "obj"; };
			class BA_flag_4 { type = "obj"; };
		};
		class actions {
			class flag { texture = "bda"; };
		};
	};
	class markers {
		class things {
			class BA_marker_1 { type = "marker"; };
			class BA_marker_2 { type = "marker"; };
			class BA_marker_3 { type = "marker"; };
			class BA_marker_4 { type = "marker"; };
		};
		class actions {};
	};
	class speakers {
		class things {
			class BA_speaker_1 { type = "obj";atl=5; };
			class BA_speaker_2 { type = "obj";atl=5; };
			class BA_speaker_3 { type = "obj";atl=5; };
			class BA_speaker_4 { type = "obj";atl=5; };
		};
		class actions {
			class soundSource {};
		};
	};
	class vehicles {
		class things {
			class BA_lMedic_1 {
				type = "veh";
				pool = "landMedic";
			};
			class BA_lRepair_1 {
				type = "veh";
				pool = "repair";
			};
			class BA_pRepair_1 {
				type = "veh";
				pool = "repair";
			};
			class BA_pRepair_2 {
				type = "veh";
				pool = "fuel";
			};
		};
		class actions {};
	};
};
