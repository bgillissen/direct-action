/*
@filename: feats\baseAtmosphere\NAM2\_params.hpp
Author:
	Ben
Description:
		included by feats\baseAtmosphere\_params.hpp
		VR base location mission parameter
*/

class NAM2_base {
	title = "Nam2 Base Locations";
	values[] = {0};
	texts[] = {"Khe Sanh"};
	default = 0;
};
