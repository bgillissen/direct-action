
#include "_debug.hpp"

params ["_thing", "_actions"];

private _name = configName _thing;
private _type = getText(_thing >> "type");

if ( isNil _name ) exitWith {
	#ifdef DEBUG
	private _debug = format["Sentry %1 is nil!", _name];
	debug(LL_WARN, _debug);
	#endif    
};	

private _unit = missionNamespace getVariable _name;

if ( isNull _unit ) exitWith {
	#ifdef DEBUG
	private _debug = format["Sentry unit %1 is null!", _name];
	debug(LL_WARN, _debug);
	#endif    
};

if !( _unit isEqualType objNull ) exitWith {
	#ifdef DEBUG
	private _debug = format["Sentry unit %1 is not a object!", _name];
	debug(LL_WARN, _debug);
	#endif    
};

[_unit, format["RL_%1_%2", getText(_thing >> "loadout"), PLAYER_SIDE]] call baseAtmosphere_fnc_npcLoadout;
_unit setVariable ["NOAI", true, true];
_unit setVariable["MT_hidden", true, true];
_unit allowDamage false;
_unit setSkill 1;
_unit setUnitPos "UP";
//todo firedEH to rearm

private _newGrp = createGroup [PLAYER_SIDE, false];
[_newGrp, true] call dynSim_fnc_set;
_newGrp setGroupIdGlobal [(["SENTRY", allGroups, {groupId _this}] call common_fnc_getUniqueName)];
_newGrp setVariable ["NOLB", true, true];
_newGrp allowFleeing 0;
_newGrp setCombatMode "WHITE";
_newGrp setBehaviour "SAFE";
_newGrp setSpeedMode "LIMITED";
_newGrp setFormDir (_unit getVariable "larsDir");

[_unit] joinSilent _newGrp;

_unit spawn {
    sleep 5;
	{ _this disableAI _x; } count ["MOVE", "SUPPRESSION", "COVER"];
};

BA_sentry pushback [_unit, _actions];