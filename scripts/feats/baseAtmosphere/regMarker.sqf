
params ["_thing", "_actions"];

private _name = configName _thing;

_name setMarkerColor (call common_fnc_getMarkerColor);

BA_markers pushback _name;