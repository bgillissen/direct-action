/*
@filename: feats\baseAtmosphere\serverInit.sqf
Author:
	Ben
Description:
	run on server
 	spawn the active base composition using LARs,
 	configure base things
*/

#include "_debug.hpp"

private _baseParam = [format["%1_base", toUpper(worldName)]] call core_fnc_getParam;
private _bases = getArray(missionConfigFile >> "settings" >> "maps" >> toUpper(worldName) >> "bases");

if ( _baseParam >= count(_bases) ) exitWith {
    #ifdef DEBUG
	private _debug = format["invalid base mission parameter, only got %2 base(s) available on map %1", toUpper(worldName), count(_bases)];
	debug(LL_ERR, _debug);
	#endif
    nil
};

BASE_NAME = (_bases select _baseParam);
BASE_REF = format["%1_%2", toUpper(worldName), BASE_NAME];

private _bool = ([BASE_REF] call compo_fnc_spawn);

if !( _bool ) exitWith {
    #ifdef DEBUG
	private _debug = format["Base composition '%1' failed to load", BASE_REF];
	debug(LL_ERR, _debug);
	#endif
    nil
}; 

//NPC faces + blacklist
if ( isNil "BA_faces" ) then {
	BA_faces = ("true" configClasses (configfile >> "CfgFaces" >> "Man_A3") apply { configName _x });
	{
     	BA_faces = BA_faces - getArray(_x >> "blacklist" >> "faces");
	} forEach (configProperties [(configfile >> "CfgDirectAction"), "isClass _x", true]);
    publicVariable "BA_faces";
};

//things setup
BA_markers = [];
BA_npc = [];
BA_obj = [];
BA_patrol = [];
BA_sentry = [];
BA_veh = [];
BA_zones = [];

private _baseConf = (missionConfigFile >> "settings" >> "baseAtmosphere" >> toUpper(worldName) >> BASE_NAME);
{
		private _actions = "true" configClasses (_x >> "actions");
		{
			private _type = getText(_x >> "type");
            switch (_type) do {
                case 'marker': { [_x, _actions] call baseAtmosphere_fnc_regMarker; };
                case 'npc': { [_x, _actions] call baseAtmosphere_fnc_regNPC; };
                case 'obj': { [_x, _actions] call baseAtmosphere_fnc_regObj; };
                case 'patrol': { [_x, _actions] call baseAtmosphere_fnc_regPatrol; };
                case 'sentry': { [_x, _actions] call baseAtmosphere_fnc_regSentry; };
                case 'veh': { [_x, _actions] call baseAtmosphere_fnc_regVeh; };
                case 'zone': { [_x, _actions] call baseAtmosphere_fnc_regZone; };
            };
	} forEach (configProperties [(_x >> "things"), "isClass _x", true]);
} forEach (configProperties [_baseConf, "isClass _x", true]);

publicVariable "BASE_NAME";
publicVariable "BA_npc";
publicVariable "BA_obj";
publicVariable "BA_patrol";
publicVariable "BA_sentry";
publicVariable "BA_veh";
publicVariable "BA_zones";

//disable damage on buildings in given zones
private _buildingTypes = getArray( missionConfigFile >> "settings" >> "baseAtmosphere" >> "buildingTypes" );
BA_buildings = [];
{
	_x params ["_area", "_actions"];
    if ( "noDamage" in _actions ) then {
        ([_area] call common_fnc_getAreaPosRad) params ["_pos", "_rad"]; 
		private _buildings = _pos nearEntities [_buildingTypes, _rad];
        { 
        	if !( _x inArea _area ) then { _buildings deleteAt _forEachIndex; };
        } forEach _buildings;
        BA_buildings append _buildings;       
    };
} forEach BA_zones;

#ifdef DEBUG
private _debug = format["disabling damage on %1 buildings", (count BA_buildings)];
debug(LL_DEBUG, _debug);
#endif

{ _x allowDamage false; } forEach BA_buildings;

publicVariable "BA_buildings";

nil