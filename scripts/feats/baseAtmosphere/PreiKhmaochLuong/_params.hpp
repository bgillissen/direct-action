/*
@filename: feats\maps\PreiKhmaochLuong\_params.hpp
Author:
	Ben
Description:
		included by feats\maps\_params.hpp
		base location mission parameter
*/

class PREI_KHMAOCH_LUONG_base {
	title = "Prei Khmaoch Luong Base Location";
	values[] = {0};
	texts[] = {"Jungle"};
	default = 0;
};
