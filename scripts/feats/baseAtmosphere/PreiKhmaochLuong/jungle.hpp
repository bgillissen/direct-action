class jungle {
	name = "Prei Khmaoch Luong";
	aoBlacklist[] = {};
	class bpAndClean {
		class things {
			class ZONE_base { type="zone";zoneType="marker"; };
		};
		class actions {
			class noShot {};
			class cleanForced {};
		};
	};
	class noSpawn {
		class things {
			class ZONE_noSpawn { type="zone";zoneType="marker"; };
		};
		class actions {
			
		};
	};
	class safeZones {
		class things {
			class ZONE_baseLarge { type="zone";zoneType="marker"; };
		};
		class actions {
			class noMissions {};
			class mortarSafe {};
			class lootSafe {};
			class noDamage {};
			class baseLight {};
			class noEnemy {};
		};
	};
	class repairs {
		class things {
			class VR_heli { type="zone";zoneType="empty";repair[] = {"heli"}; };
		};
		class actions {
			class repair {};
		};
	};
	class heliSupport {
		class things {
			class BA_npc_support { type = "npc";balo = "support"; };
		};
		class actions {
			class support {};
			class vehicleSelection {
					name = "Helicopters";
					classes[] = { "heli" };
					roles[] = { "hPilot" };
			};
			class vehicleLoadout { marker = "BV_heli"; };
		};
	};
	class artiSupport {
		class things {
			class BA_as { type = "npc";balo = "support"; };
		};
		class actions {
			class artiSupport { batId = 0; };
		};
	};
	class arsenals {
		class things {
			class BA_va_1 { type = "obj"; };
			class BA_va_2 { type = "obj"; };
		};
		class actions {
			class arsenal { filtered = -1; };
			class viewDistance {};
			class loadGear {};
			class saveGear {};
			class serverRules {};
		};
	};
	class zeus {
		class things {
			class BA_zeus { type = "obj"; };
		};
		class actions {
			class cleanup {};
			class curator {};
			class memberData {};
			class lockServer {};
		};
	};
	class rightBillboard {
		class things {
			class BA_bb_1 { type = "obj"; };
		};
		class actions {
			class billboard {
				mode = "fixed";
				texture = "tfar";
			};
		};
	};
	class leftBillboard {
		class things {
			class BA_bb_2 { type = "obj"; };
		};
		class actions {
			class billboard { mode = "random"; };
			class dateBillboard { textures[] = {"xmas"}; };
		};
	};
	class flag {
		class things {
			class BA_flag { type = "obj"; };
		};
		class actions {
			class flag { texture = "tfu"; };
			class environment {};
			class baseLight {};
			class anthem { sfx = "Sound_anthem_tfu"; };
			class vehicleSelection {
				name = "Land Vehicles";
				classes[] = { "cars", "armedCars" };
				roles[] = {};
			};
			class switchSideMission {};
		};
	};
	class speakers {
		class things {
			class BA_speaker { type = "obj";atl=5; };
		};
		class actions {
			class soundSource {};
		};
	};
};
