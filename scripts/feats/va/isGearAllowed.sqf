/*
@filename: feats\va\isGearAllowed.sqf
Author:
	Ben
Description:
	run on player,
	Check if the given class is present in the Arsenal gear type array
*/

#include "_debug.hpp"

params ["_type", "_class"];

private "_allowed";

switch _type do {
	case "backpack" : { _allowed = A_backpacks; };
	case "item" : { _allowed = A_items; };
	case "weap" : { _allowed = A_weapons; };
	case "ammo" : { _allowed = A_ammo; };
};

if ( isNil "_allowed" ) exitWith { false };

private _checkMatch = {
	params ["_array", "_match"];
	private _bool = false;
	{
		if ( _match isEqualTo (toLower _x) ) exitwith { _bool = true; };
	} forEach _array;
	_bool
};

private _bool = false;

if ( _type isEqualTo "item" ) then {
	if ( MOD_tfar ) then {
		{
			if ( ([_class, _x] call TFAR_fnc_isSameRadio)) exitWith { _bool = true; };
		} count TFAR_SR;
	};
	if !( _bool ) then {
		_bool = [_allowed, (toLower _class)] call _checkMatch;
	};
} else {
	private "_normalized";
	if ( _type isEqualTo "weap" ) then {
		_normalized = toLower ([_class] call assets_fnc_baseWeapon);
	} else {
		_normalized = toLower _class;
	};
	_bool = [_allowed, _normalized] call _checkMatch;
};

#ifdef DEBUG
private _debug = format["isGearAllowed : %1 => %2", _type, _class, _bool];
debug(LL_DEBUG, _debug);
#endif
	
_bool