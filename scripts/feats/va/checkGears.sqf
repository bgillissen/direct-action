/*
@filename: feats\va\checkGears.sqf
Author:
	Ben
Description:
	run on player,
	Check that all the gear equiped by player is in the arsenal, thx Nancy...
*/

#include "_debug.hpp"

if ( (["filterArsenal"] call core_fnc_getParam) == 0 ) exitWith {
    #ifdef DEBUG
	debug(LL_DEBUG, "arsenal are not filtered, abording");
	#endif
    nil
};

private _removed = false;

private _goggle = goggles player;
if !( _goggle isEqualTo "" ) then {
	if !( ["item", _goggle] call va_fnc_isGearAllowed ) then {
		systemChat format["Goggle: %1 is not allowed", _goggle];
		_removed = true;
		removeGoggles player;
	};
};

private _bino = binocular player;
if !( _bino isEqualTo "" ) then {
	if !( ["item", _bino] call va_fnc_isGearAllowed ) then { 
		if !( ["weap", _bino] call va_fnc_isGearAllowed ) then {
			systemChat format["Binocular: %1 is not allowed", _bino];
			_removed = true;
			player removeWeapon _bino;
		};
	};
};


private _headgear = headgear player;
if !( _headgear isEqualTo "" ) then {
	if !( ["item", _headgear] call va_fnc_isGearAllowed ) then {
		systemChat format["Headgear: %1 is not allowed", _headgear];
		_removed = true;
		player unassignItem _headgear;
		player removeItem _headgear;
	};
};

//backpack
private _backpack = backpack player;
if !( _backpack isEqualTo "" ) then {
	if !( ["backpack", _backpack] call va_fnc_isGearAllowed ) then {
		systemChat format["Backpack: %1 is not allowed", _backpack];
		_removed = true;
		removeBackpack player;
	} else {
		_removed =  !(["backpack"] call va_fnc_checkContainer);
	};
};

//uniform
private _uniform = uniform player;
if !( _uniform isEqualTo "" ) then {
	if !( ["item", _uniform] call va_fnc_isGearAllowed ) then {
		systemChat format["Uniform: %1 is not allowed", _uniform];
		_removed = true;
		player unassignItem _uniform;
		player removeItem _uniform;
	} else {
		_removed = !(["uniform"] call va_fnc_checkContainer);
	};
};

private _vest = vest player;
if !( _vest isEqualTo "" ) then {
	if !( ["item", _vest] call va_fnc_isGearAllowed ) then {
		systemChat format["Vest: %1 is not allowed", _vest];
		_removed = true;
		player unassignItem _vest;
		player removeItem _vest;
	} else {
		_removed = !(["vest"] call va_fnc_checkContainer);
	};
};

//assigned items
{
	if !( ["item", _x] call va_fnc_isGearAllowed ) then { 
		if !( ["weap", _x] call va_fnc_isGearAllowed ) then {
			systemChat format["Item: %1 is not allowed", _x];
			_removed = true;
			player unassignItem _x;
			player removeItem _x;
		};
	};
} count (assignedItems player);

private _primWeap =  primaryWeapon player;
if !( _primWeap isEqualTo "" ) then {
	if !( ["weap", _primWeap] call va_fnc_isGearAllowed ) then {
		systemChat format["primary weapon : %1 is not allowed", _primWeap];
		_removed = true;
		player removeWeapon _primWeap;
	} else { 
    	//for some reason, 2 members got an issue with this
		private _primItems = primaryWeaponItems player;
		{
			if !( _x isEqualTo "" ) then {
				if !( ["item", _x] call va_fnc_isGearAllowed ) then {
					systemChat format["primary weapon Item: %1 is not allowed", _x];
					_removed = true;
					player removePrimaryWeaponItem _x;
				};
			};
		} count _primItems;
		private _count = player ammo _primWeap;
		if ( _count > 0 ) then {
			private _mags = primaryWeaponMagazine player;
			{
				if !( ["ammo", _x] call va_fnc_isGearAllowed ) then {
					systemChat format["primary weapon magazine: %1 is not allowed", _x];
					player removePrimaryWeaponItem _x;
					//player removeMagazine _x;
					//player setAmmo [_primWeap, 0];
					_removed = true;
				};	
			} count _mags;
		};
	};
};

private _secondWeap =  secondaryWeapon player;
if !( _secondWeap isEqualTo "" ) then {
	if !( ["weap", _secondWeap] call va_fnc_isGearAllowed ) then {
		systemChat format["secondary weapon : %1 is not allowed", _secondWeap];
		_removed = true;
		player removeWeapon _secondWeap;
	} else {
		private _secondItems = secondaryWeaponItems player;
		{
			if !( _x isEqualTo "" ) then {
				if !( ["item", _x] call va_fnc_isGearAllowed ) then {
					systemChat format["secondary weapon item: %1 is not allowed", _x];
					_removed = true;
					player removeSecondaryWeaponItem _x;
				};
			};
		} count _secondItems;
		private _count = player ammo _secondWeap;
		if ( _count > 0 ) then {
			private _mags = secondaryWeaponMagazine player;
			{
				if !( ["ammo", _x] call va_fnc_isGearAllowed ) then {
					systemChat format["secondary weapon magazine: %1 is not allowed", _x];
					player setAmmo [_secondWeap, 0];
					player removeSecondaryWeaponItem _x;
					_removed = true;
				};	
			} count _mags;
		};
	};
};

private _handWeap = handgunWeapon player;
if !( _handWeap isEqualTo "" ) then {
	if !( ["weap", _handWeap] call va_fnc_isGearAllowed ) then {
		systemChat format["hand weapon : %1 is not allowed", _handWeap];
		_removed = true;
		player removeWeapon _handWeap;
	} else {
		private _handItems = handgunItems player;
		{
			if !( _x isEqualTo "" ) then {
				if !( ["item", _x] call va_fnc_isGearAllowed ) then {
					_removed = true;
					systemChat format["handgun item: %1 is not allowed", _x];
					player removeHandgunItem _x;
				};
			};
		} count _handItems;
		private _count = player ammo _handWeap;
		if ( _count > 0 ) then {
			private _mags = handGunMagazine player;
			{
				if !( ["ammo", _x] call va_fnc_isGearAllowed ) then {
					systemChat format["handgun magazine: %1 is not allowed", _x];
					player setAmmo [_handWeap, 0];
					_removed = true;
				};	
			} count _mags;
		};
	};
};

if ( _removed ) then {
	private _msg = ["va", "message"] call core_fnc_getSetting;
	private _duration = ["va", "duration"] call core_fnc_getSetting;
	titleText [_msg, "PLAIN", _duration];
};

!_removed