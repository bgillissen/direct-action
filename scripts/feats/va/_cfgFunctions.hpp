class va {
	tag = "va";
	class functions {
		class add { file="feats\va\add.sqf"; };
		class remove { file="feats\va\remove.sqf"; };
		class checkGears { file="feats\va\checkGears.sqf"; };
		class checkContainer { file="feats\va\checkContainer.sqf"; };
		class isGearAllowed { file="feats\va\isGearAllowed.sqf"; };
		class playerCloseVA { file="feats\va\playerCloseVA.sqf"; };
		class serverInit { file="feats\va\serverInit.sqf"; };
	};
};
