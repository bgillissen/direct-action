/*
@filename: feats\va\checkContent.sqf
Author:
	Ben
Description:
	run on player,
	Check the content of the given container type
*/

params ["_type"];

private _out = true;

private _container = _type call {
	if ( _this isEqualTo "backpack" ) exitWith { (backpackContainer player) };
	if ( _this isEqualTo "uniform" ) exitWith { (uniformContainer player) };
	if ( _this isEqualTo "vest" ) exitWith { (vestContainer player) };
	nil
};

if ( isNil "_container" ) exitWith {};

private _removeItem = _type call {
		if ( _this isEqualTo "backpack" ) exitWith { { player removeItemFromBackpack _this; } };
		{ player unassignItem _this;player removeItem _this; }
};

//items
{
	if !( ["item", _x] call va_fnc_isGearAllowed ) then {
		systemChat format["Item in your %1: %2 is not allowed", _type, _x];
		_out = false;
		_x call _removeItem;
	};
} count ((getItemCargo _container) select 0);

//weapons
private _content = getWeaponCargo _container;
{
	if !( ["weap", _x] call va_fnc_isGearAllowed ) then { 
		_out = false;
		systemChat format["Weapon in your %1: %2 is not allowed", _type, _x];
		for "_i" from 1 to ((_content select 1) select _forEachIndex) do {
			_x call _removeItem;
		};
	};
} forEach (_content select 0);

//ammo
{
	if !( ["ammo", _x] call va_fnc_isGearAllowed ) then {
		_out = false;
		systemChat format["Magazine in your %1: %2 is not allowed", _type, _x];
		player removeMagazines _x;	
	};
} count ((getMagazineCargo _container) select 0);

_out