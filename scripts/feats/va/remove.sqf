/*
@filename: feats\va\remove.sqf
Author:
	ben
Description:
	run on server,
	remove the arsenal from the given thing
*/

params ["_box"];

if ( CTXT_SERVER ) then {
	["AmmoboxExit", [_box]] call BIS_fnc_arsenal;
};

if ( CTXT_PLAYER ) then {
	{
        if ( ((_box actionParams _x) select 0) isEqualTo "Arsenal" ) exitWith { _box removeAction _x; };
    } forEach actionIDs _box;
};