
if ( (["stateMonitor"] call core_fnc_getParam) == 0 ) exitWith {
	#ifdef DEBUG
    debug(LL_INFO, "disabled by mission parameter");
    #endif
    nil    
};

params["_unit", "_id", "_uid", "_name"];

{
	if ( (_x select 0) isEqualTo _name ) exitWith { 
    	stateMonitorData deleteAt _forEachIndex; 
	};
} forEach stateMonitorData;

SM_delListener = _uid;
publicVariableServer "SM_delListener";

nil