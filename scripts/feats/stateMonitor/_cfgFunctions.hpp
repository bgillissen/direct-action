class stateMonitor {
	tag = "stateMonitor";
	class functions {
		class headlessDestroy { file = "feats\stateMonitor\headlessDestroy.sqf"; };
		class headlessInit { file = "feats\stateMonitor\headlessInit.sqf"; };
		class playerDestroy { file="feats\stateMonitor\playerDestroy.sqf"; };
		class playerInit { file="feats\stateMonitor\playerInit.sqf"; };
		class serverDestroy { file = "feats\stateMonitor\serverDestroy.sqf"; };
		class serverInit { file = "feats\stateMonitor\serverInit.sqf"; };
		class serverLeave { file = "feats\stateMonitor\serverLeave.sqf"; };
		class grabData { file = "feats\stateMonitor\grabData.sqf"; };
		class swapState { file="feats\stateMonitor\swapState.sqf"; };
	};
};
