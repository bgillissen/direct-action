class stateMonitor : feat_base  {
	class headless : featHeadless_base {
		class init : featEvent_enable { thread = 1; };
		class destroy : featEvent_enable {};
	};
	class player : featPlayer_base {
		class init : featEvent_enable { thread = 1; };
		class destroy : featEvent_enable {};
	};
	class server : featServer_base {
		class init : featEvent_enable { thread = 1; };
		class leave : featEvent_enable {};
		class destroy : featEvent_enable {};
	};
};
