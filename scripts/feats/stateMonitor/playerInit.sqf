/*
@filename: feats\stateMonitor\playerInit.sqf
Author:
	Ben
Description:
	run on player,
	display server and headless clients status
*/

#include "_debug.hpp"
#include "define.hpp"

if ( (["stateMonitor"] call core_fnc_getParam) == 0 ) exitWith {
	#ifdef DEBUG
    debug(LL_INFO, "disabled by mission parameter");
    #endif
    nil    
};

if ( isNil "stateMonitorData" ) then { stateMonitorData = []; };

stateMonitorDisplay = false;

["stateMonitor", "Toggle server status HUD visibility", "stateMonitor_fnc_swapState"] call keybind_fnc_add;

disableSerialization;

private _server = (["stateMonitor", "server"] call core_fnc_getSetting);
private _headless = (["stateMonitor", "headless"] call core_fnc_getSetting);
private _value = (["stateMonitor", "value"] call core_fnc_getSetting);

private _delay = (["stateMonitor", "gameRefresh"] call core_fnc_getSetting);

#ifdef DEBUG
private _debug = format["player thread is running with a %1s delay", _delay];
debug(LL_DEBUG, _debug);
#endif

private _getNumValue = {
    params ["_val", "_bad", "_warn"];
    if ( _val <= _bad ) exitWith { format[(["stateMonitor", "numBad"] call core_fnc_getSetting), _val] };
    if ( _val <= _warn ) exitWith { format[(["stateMonitor", "numWarn"] call core_fnc_getSetting), _val] };
    format[(["stateMonitor", "numGood"] call core_fnc_getSetting), _val]
};

_vcomAI = !( (["vcomAI"] call core_fnc_getParam) == 0 );
_vcomAIServer = ( _vcomAI && !((["vcomAI_onServer"] call core_fnc_getParam) == 0) );

while { true } do {
    999 cutRsc ["stateMonitor", "PLAIN", 0.01, true];
	private _dsp = uiNameSpace getVariable "stateMonitor";
	private _ctrl = _dsp displayCtrl CONTAINER_IDC;
    private _str = "";
    
	if ( stateMonitorDisplay ) then {
        
        private _states = [];
        {
            private _entries = [];
            if ( _forEachIndex == 0 ) then {
				_x params [["_profile", ""], ["_players", 0], ["_headless", 0], ["_mission", ""], ["_fps", 0], ["_cps", 0], 
						   ["_grpOPFOR", 0], ["_grpBLUFOR", 0], ["_grpIND", 0], ["_grpCIV", 0], 
                           ["_objects", 0], ["_vehicles", 0], ["_localAI", 0], ["_remoteAI", 0], ["_vcom", 0]];
				_entries pushback format["players: %1", format[_value, _players]];
                _entries pushback format["fps: %1", ([_fps, 10, 25] call _getNumValue)]; 
				_entries pushback format["cps: %1", ([_cps, 8, 15] call _getNumValue)];
                private _buff = [];
                { _buff pushback format[_value, _x]; } forEach [_grpOPFOR, _grpBLUFOR, _grpIND, _grpCIV];
                _entries pushback format["groups (opfor | blufor | ind | civ): %1", ( _buff joinString (" | "))];
                _entries pushback format["objects: %1", format[_value, _objects]];
                _entries pushback format["vehicles: %1", format[_value, _vehicles]];
                _entries pushback format["local AI: %1", format[_value, _localAI]];
                _entries pushback format["remote AI: %1", format[_value, _remoteAI]];
                if ( _vcomAIServer ) then {
                	_entries pushback format["vcomAI: %1", format[_value, _vcom]];
				};
                _states pushback format[_server, _profile, (_entries joinString ", ")];
            } else {
				_x params [["_profile", ""], ["_fps", 0], ["_cps", 0], ["_localAI", 0], ["_vcom", 0]];
                _entries pushback format["fps: %1", ([_fps, 10, 25] call _getNumValue)]; 
				_entries pushback format["cps: %1", ([_cps, 8, 15] call _getNumValue)];
                _entries pushback format["local AI: %1", format[_value, _localAI]];
                if ( _vcomAI ) then {
                	_entries pushback format["vcomAI: %1", format[_value, _vcom]];
				};
                _states pushback format[_headless, _profile, (_entries joinString ", ")]; 
            };
        } forEach stateMonitorData;
        _str = (_states joinString "<br/>");
        private _pos = ctrlPosition _ctrl;
        _pos set [3, ((count stateMonitorData) *  LINE_HEIGHT)];
        _pos set [1, (SafeZoneY + SafeZoneH - (_pos select 3))];
        _ctrl ctrlSetPosition _pos;
        _ctrl ctrlSetFade 0;
    } else {
		_ctrl ctrlSetFade 1;
    };
    _ctrl ctrlSetStructuredText parseText _str;
	_ctrl ctrlCommit 0;
    sleep _delay;    
};

nil