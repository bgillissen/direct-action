/*
@filename: feats\stateMonitor\playerDestroy.sqf
Author:
	Ben
Description:
	run on player,
	kill the stateMonitor playerInit thread
 */

if ( (["stateMonitor"] call core_fnc_getParam) == 0 ) exitWith {
	#ifdef DEBUG
    debug(LL_INFO, "disabled by mission parameter");
    #endif
    nil    
};

if ( count(_this) == 0 ) exitWith { nil };

params ["_when", "_thread"];

if ( scriptDone _thread ) exitWith { nil };

terminate _thread;

waitUntil{ scriptDone _thread };

disableSerialization;
private _ui = uiNameSpace getVariable "Hudstates";
private _hud = _ui displayCtrl 88888;
_hud ctrlSetFade 1;
_hud ctrlCommit 0;

stateMonitorDisplay = false;

nil