/*
@filename: feats\stateMonitor\serverInit.sqf
Author:
	Ben
Description:
	run on player,
	keep the state data stack updated and broadcast it 
*/

#include "_debug.hpp"

if ( (["stateMonitor"] call core_fnc_getParam) == 0 ) exitWith {
	#ifdef DEBUG
    debug(LL_INFO, "disabled by mission parameter");
    #endif
    nil    
};

stateMonitorDebug = {};
#ifdef DEBUG
stateMonitorDebug = { debug(DEBUG_LVL, _this); };
#endif

stateMonitor_stop = false;

stateMonitorData = [[]];
publicVariable "stateMonitorData";

if ( isNil "stateMonitorListener" ) then { stateMonitorListener = []; };

if ( isNil "stateMonitorPVEH" ) then {
    private _code = {
        (_this select 1) params ["_name", "_data"];
	    private _trgt = -1;
	    {
	        if ( (_x select 0) isEqualTo _name ) exitWith { _trgt = _forEachIndex; };
	    } forEach stateMonitorData;
	    if ( _trgt < 0 ) then {
			stateMonitorData pushback (_this select 1);   
	    } else {
	        stateMonitorData set [_trgt, (_this select 1)];
		};
	};
	stateMonitorPVEH = ["stateMonitorDataHL", _code] call pveh_fnc_add;    
};

if ( isNil "SM_addListenerPVEH" ) then {
	SM_addListenerPVEH = ["SM_addListener", { 
    	private _player = _this select 1;
    	private _uid = getPlayerUID _player;
    	private _found = false;
	    { 
	    	if (_uid isEqualTo (_x select 1) ) exitWith { _found = true; };
	    } forEach stateMonitorListener;
	    if !( _found ) then { stateMonitorListener pushback [_player, _uid]; };
    }] call pveh_fnc_add;
};

if ( isNil "SM_delListenerPVEH" ) then {
	SM_delListenerPVEH = ["SM_delListener", {
	    private _uid = _this select 1;
	    { 
	    	if (_uid isEqualTo (_x select 1) ) exitWith { stateMonitorListener deleteAt _forEachIndex; };
	    } forEach stateMonitorListener;
    }] call pveh_fnc_add;
};

private _startFSM = ( isNil "stateMonitorFSM" );
if !( _startFSM ) then { _startFSM = ( completedFSM stateMonitorFSM ); };
if ( _startFSM ) then { stateMonitorFSM = execFSM "feats\stateMonitor\fsm\server.fsm"; };


private _vars = (["stateMonitor", "serverData"] call core_fnc_getSetting);
private _delay = (["stateMonitor", "loopDelay"] call core_fnc_getSetting);
private _game = (["stateMonitor", "gameRefresh"] call core_fnc_getSetting);
private _web = (["stateMonitor", "webRefresh"] call core_fnc_getSetting);
private _player = (["stateMonitor", "playerRefresh"] call core_fnc_getSetting);

#ifdef DEBUG
private _debug = format["server thread is running with a %1s delay", _delay];
debug(LL_INFO, _debug);
#endif

private _nextGame = 0;
private _nextWeb = 0;
private _nextPlayer = 0;

while { true } do {
    private _now = time;
    private _doGame = (_now >= _nextGame);
    private _doWeb = ( (_now >= _nextWeb) && !(isNil "stateMonitor_fnc_dataUpload") );
    private _doPlayer = ( (_now >= _nextPlayer) && !(isNil "stateMonitor_fnc_playerUpload") );
    private _data = [];
    if ( _doGame || _doWeb || _doPlayer ) then {
    	_data = [stateMonitorFSM, _vars] call stateMonitor_fnc_grabData;
        stateMonitorData set [0, _data];
	};
	if ( _doGame ) then {
        _nextGame = _now + _game;
        {
        	(owner (_x select 0)) publicVariableClient "stateMonitorData";
        } forEach stateMonitorListener;
    };
    if ( _doWeb ) then {
        _nextWeb = _now + _web;
    	stateMonitorData spawn stateMonitor_fnc_dataUpload;    
    };
    if ( _doPlayer ) then {
        _nextPlayer = _now + _player;
    	[] spawn stateMonitor_fnc_playerUpload;    
    };
	sleep _delay;  
};

nil