/*
@filename: feats\stateMonitor\serverInit.sqf
Author:
	Ben
Description:
	run on player,
	keep the state data stack of this headless updated and broadcast it to the server 
*/

#include "_debug.hpp"

if ( (["stateMonitor"] call core_fnc_getParam) == 0 ) exitWith {
	#ifdef DEBUG
    debug(LL_INFO, "disabled by mission parameter");
    #endif 
    nil   
};

stateMonitorDebug = {};
#ifdef DEBUG
stateMonitorDebug = { debug(DEBUG_LVL, _this, 0); };
#endif

stateMonitor_stop = false;

private _startFSM = ( isNil "stateMonitorFSM" );
if !( _startFSM ) then { _startFSM = ( completedFSM stateMonitorFSM ); };
if ( _startFSM ) then { stateMonitorFSM = execFSM "feats\stateMonitor\fsm\headless.fsm"; };

private _vars = (["stateMonitor", "headlessData"] call core_fnc_getSetting);
private _delay = (["stateMonitor", "gameRefresh"] call core_fnc_getSetting);

#ifdef DEBUG
private _debug = format["headless thread is running with a %1s delay", _delay];
debug(LL_INFO, _debug);
#endif

while { true } do {
    stateMonitorDataHL = [stateMonitorFSM, _vars] call stateMonitor_fnc_grabData;
    publicVariableServer "stateMonitorDataHL";
	sleep _delay;  
};

nil