/*
@filename: feats\stateMonitor\swapState.sqf
Author:
	Ben
Description:
	run on player side by a display event handler
	swap the visibility of the stateMonitor HUD element
*/

params ["_control", "_keyCode", "_shift", "_ctrl", "_alt"];

stateMonitorDisplay = !stateMonitorDisplay;

if !( stateMonitorDisplay ) then {
    disableSerialization;
    private _ui = uiNameSpace getVariable "Hudstates";
	private _hud = _ui displayCtrl 88888;
    _hud ctrlSetFade 1;
    _hud ctrlCommit 0;
    if !( isServer ) then {
    	SM_delListener = getPlayerUID player;
    	publicVariableServer "SM_delListener";
	};
} else {
    if !( isServer ) then {
    	SM_addListener = player;
    	publicVariableServer "SM_addListener";
	};
};