/*
@filename: feats\stateMonitor\serverDestroy.sqf
Author:
	Ben
Description:
	run on player,
	kill the stateMonitor serverInit thread
 */

if ( (["stateMonitor"] call core_fnc_getParam) == 0 ) exitWith {
	#ifdef DEBUG
    debug(LL_INFO, "disabled by mission parameter");
    #endif
    nil    
};

if ( count(_this) == 0 ) exitWith { nil };

params ["_when", "_thread"];

if ( scriptDone _thread ) exitWith { nil };

terminate _thread;

waitUntil{ scriptDone _thread };

stateMonitor_stop = true;

nil