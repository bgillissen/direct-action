
#include "..\..\common\_dik.hpp"

class stateMonitor {
	keycode[] = {DIK_SPACE, 0, 0, 1};
	loopDelay = 1;
	gameRefresh = 2;
	webRefresh = 20;
	playerRefresh = 60;
	server = "<t size='0.6' color='#f0e68c'>Server (%1): %2</t>";
	headless = "<t size='0.6' color='#f0e68c'>Headless (%1): %2</t>";
	value = "<t color='#66DEFF'>%1</t>";
	numGood = "<t color='#00FF00'>%1</t>";
	numWarn = "<t color='#FFFF00'>%1</t>";
	numBad = "<t color='#FF0000'>%1</t>";
	headlessData[] = {"_fps", "_cps", "_localAI", "_vcom"};
	serverData[] = {"_players", "_headless", "_mission", "_fps", "_cps", "_grpOPFOR", "_grpBLUFOR", "_grpIND", "_grpCIV", "_objects", "_vehicles", "_localAI", "_remoteAI", "_vcom"};
};
