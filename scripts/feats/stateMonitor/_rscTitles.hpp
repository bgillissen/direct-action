
#include "define.hpp"

class stateMonitor {
	idd = -1;
    fadeout=0;
    fadein=0;
	duration = 10000000000;
	name= "stateMonitor";
	onLoad = "uiNamespace setVariable ['stateMonitor', _this select 0]";
	class Controls {
		class container: daStructuredText
		{
			idc = CONTAINER_IDC;
			x = SafeZoneX;
			w = SafeZoneW;
			colorBackground[] = {0,0,0,0.4};
			colorText[] = {1,1,1,1};
		};

	};
};
