/*
@filename: feats\startTime\serverInit.sqf
Author:
	Ben
Description:
	run on server,
	set in-game date to real world date
	randomly skipTime
*/

#include "_debug.hpp"

if ( (["realDate"] call core_fnc_getParam) == 1 ) then {
	(call common_fnc_getDate) params [["_curYear", 0], ["_curMonth", 0], ["_curDay", 0], ["_curHour", 0], ["_curMinute", 0]];
	if ( _curYear != 0 ) then {
		setDate [_curYear, _curMonth, _curDay, _curHour, _curMinute];
	#ifdef DEBUG
    	_debug = format["mission date has been set to %1-%2-%3 @ %4:%5", _curYear, _curMonth, _curDay, _curHour, _curMinute];
    	debug(LL_INFO, _debug);
    } else {
		debug(LL_WARN, "real_date extension is not loaded, abording");
	#endif
	};
};

if ( (["randomTime"] call core_fnc_getParam) == 1 ) then {
    #ifdef DEBUG
	debug(LL_INFO, "skipping time randomly");
    #endif
	skipTime (floor random 24); 
};

nil