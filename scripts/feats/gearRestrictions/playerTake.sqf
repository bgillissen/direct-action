/*
@filename: feats\gearRestrictions\playerTake.sqf
Author:
	Ben
Description:
	run on player,
	apply gear restrictions when player grab an item from a container if not in an unrestricted zone
*/

if ( (["restrictGear"] call core_fnc_getParam) == 0 ) exitWith { nil };

if ( GR_disabled ) exitWith { nil };

params ["_unit", "_container", "_item"];

[_container] call gearRestrictions_fnc_checkGears;

nil