class gearRestrictions : feat_base {
	class player : featPlayer_base {
		class closeVA : featEvent_enable {};
		class destroy : featEvent_enable {};
		class init : featEvent_enable { thread = 1; };
		class take : featEvent_enable {};
	};
};
