/*
@filename: feats\gearRestriction\playerDestroy.sqf
Author:
	Ben
Description:
	run on player,
	kill gearRestrictions playerInit thread
*/

if ( count(_this) == 0 ) exitWith { nil };

if ( (["restrictGear"] call core_fnc_getParam) == 0 ) exitWith { nil };

params ["_when", "_thread"];

terminate _thread;

waitUntil{ scriptDone _thread };

nil