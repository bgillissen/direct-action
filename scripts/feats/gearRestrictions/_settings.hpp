class gearRestrictions {
	artiRoles[] = {"mortar", "crew"};
	at = "Only AT/AA soldiers may use this weapon system, launcher removed.";
	mg = "Only autoriflemen may use this machine gun, weapon removed.";
	sRifle = "Only snipers may use this rifle, rifle removed.";
	mRifle = "Only snipers / marksmen / spotter may use this rifle, rifle removed.";
	sScope = "Only snipers may use this optic, optic removed.";
	mScope = "Only snipers / markmen/ spotter may use this optic, optic removed.";
	oScope = "Only squad or team leader may use this optic, optic removed.";
	backpack = "Only AT / AA soldier may use this backpack, backpack removed.";
	mbrItem = "Only TFU members are allowed to use this item, item removed";
	uav = "Only UAV operator may use this item, UAV terminal removed.";
	duration = 5;
	loopDelay = 10;
};
