/*
@filename: feats\gearRestriction\playerInit.sqf
Author:
	Ben
Description:
	run on player side,
	restrict avaibility of the artillery computer to the player with an arti role
	re-apply restrictions once player get out of the Firing Range
*/

#include "_debug.hpp"

private _artiComputer = ( (["artiComputer"] call core_fnc_getParam) isEqualTo 1 );

if ( (["restrictGear"] call core_fnc_getParam) == 0 ) exitWith {
    #ifdef DEBUG
    debug(LL_INFO, "disabled by mission parameter");
    #endif
	enableEngineArtillery !_artiComputer;
    nil
};

if ( (["restrictArti"] call core_fnc_getParam) == 1 ) then {
	private _allowed = (["gearRestrictions", "artiRoles"] call core_fnc_getSetting);
	enableEngineArtillery ( ((player getVariable "role") in _allowed) && !_artiComputer );
} else {
	enableEngineArtillery !_artiComputer;
};

private _delay = (["gearRestrictions", "loopDelay"] call core_fnc_getSetting);
private _zones = [];
{
	_x params ["_area", "_actions"];
    if ( "noGR" in _actions ) then { _zones pushback _area; };
} forEach BA_zones;

GR_disabled = false;
private _wasIn = false;

#ifdef DEBUG
private _debug = format["thread has started with %1s delay between checks, %2 zone(s) to check", _delay, count(_zones)];
debug(LL_DEBUG, _debug);
#endif
    
while { true }  do {
    GR_disabled = ( ({( player inArea _x )} count _zones) > 0 );
	if ( GR_disabled ) then { 
		_wasIn = true; 
	} else {
		if ( _wasIn ) then {
			call gearRestrictions_fnc_checkGears;
			_wasIn = false;
		};
	};
	sleep _delay;
};

nil