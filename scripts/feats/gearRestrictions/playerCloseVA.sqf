/*
@filename: feats\gearRestrictions\playerCloseVA.sqf
Author:
	Ben
Description:
	run on player,
	apply gear restrictions when player close the virtual arsenal if not in an unrestricted zone
*/

#include "_debug.hpp"

if ( (["restrictGear"] call core_fnc_getParam) == 0 ) exitWith {
    #ifdef DEBUG
    debug(LL_INFO, "disabled by mission parameter");
    #endif
    nil
};

if ( GR_disabled ) exitWith {
    #ifdef DEBUG
    debug(LL_DEBUG, "player is in unrestricted area, abording");
    #endif
    nil
};

call gearRestrictions_fnc_checkGears;

nil