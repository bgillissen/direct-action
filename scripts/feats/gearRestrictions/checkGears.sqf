/*
@filename: feats\gearRestriction\checkGear.sqf
Author:
	Ben
Description:
	run on player,
	apply gear restrictions to player by removing or puting back that taken thing in the container it came from
	can be skipped if memberData says so
*/

#include "_debug.hpp"

if !( player call memberData_fnc_gearRestrictions ) exitWith {
    #ifdef DEBUG
    debug(LL_DEBUG, "disabled by memberData, abording");
    #endif
};

params [["_container", objNull]];

private _drop = !(isNull _container);
private _role = player getVariable "role";
private _at = ( (_role isEqualTo "at") || (_role isEqualTo "aa") );
private _mg = ( (_role isEqualTo "hmg") || (_role isEqualTo "lmg") );
private _sniper = (_role isEqualTo "sniper");
private _spotter = (_role isEqualTo "spotter");
private _marksman = ( (_role isEqualTo "marksman") || (_role isEqualTo "spotter") );
private _officer = ( (_role isEqualTo "hq") || (_role isEqualTo "sl") || (_role isEqualTo "tl") );
private _uavOp = (_role isEqualTo "uavOp");
private _isMBR = ((player getVariable["MD_rank", 0]) > 2); 
private _msgs = [];

if ( (["restrictLauncher"] call core_fnc_getParam) == 1 ) then {
	if ( !_at ) then {
		private _secondWeap =  secondaryWeapon player;
		if (({player hasWeapon _x} count RG_launcher) > 0) then {
			if ( _drop ) then {
				player action ["DropWeapon", _container, _secondWeap];
			} else {
				player removeWeapon _secondWeap;
			};
            private _msg = (["gearRestrictions", "at"] call core_fnc_getSetting);
			_msgs pushback _msg;
		};
	};
};

private _primWeap =  primaryWeapon player;

if ( (["restrictMG"] call core_fnc_getParam) == 1 ) then {
	if ( !_mg ) then {
		if (({player hasWeapon _x} count RG_mg) > 0) then {
			if ( _drop ) then {
				player action ["DropWeapon", _container, _primWeap];
			} else {
				player removeWeapon _primWeap;
			};
            private _msg = (["gearRestrictions", "mg"] call core_fnc_getSetting);
			_msgs pushback _msg;
		};
	};
};



if ( (["restrictSniper"] call core_fnc_getParam) == 1 ) then {
	if ( !_sniper ) then {
		if (({player hasWeapon _x} count RG_sRifle) > 0) then {
			if ( _drop ) then {
				player action ["DropWeapon", _container, _primWeap];
			} else {
				player removeWeapon _primWeap;
			};
            private _msg = (["gearRestrictions", "sRifle"] call core_fnc_getSetting);
			_msgs pushback _msg;
    	};
	};
	if ( !_marksman && !_sniper ) then {
		if (({player hasWeapon _x} count RG_mRifle) > 0) then {
			if ( _drop ) then {
				player action ["DropWeapon", _container, _primWeap];
			} else {
				player removeWeapon _primWeap;
			};
            private _msg = (["gearRestrictions", "mRifle"] call core_fnc_getSetting);
			_msgs pushback _msg;
    	};
    };
};

private _primItems = primaryWeaponItems player;

if ( (["restrictLRScope"] call core_fnc_getParam) == 1 ) then {
	if  ( !_marksman && !_sniper ) then {
        {
           if ( _x in RG_mScope ) exitWith {
           		if ( _drop ) then { _container addItemCargoGlobal [_x, 1]; };
				player removePrimaryWeaponItem  _x;
                private _msg = (["gearRestrictions", "mScope"] call core_fnc_getSetting);
				_msgs pushback _msg;
           };
        } forEach _primItems;
   	};
	if  ( !_sniper ) then {
        {
			if ( _x in RG_sScope ) exitWith {
	       		if ( _drop ) then { _container addItemCargoGlobal [_x, 1]; };
				player removePrimaryWeaponItem  _x;
	            private _msg = (["gearRestrictions", "sScope"] call core_fnc_getSetting);
				_msgs pushback _msg;
	       };
       } forEach _primItems;
   	};
};

if ( (["restrictThermalScope"] call core_fnc_getParam) == 1 ) then {	
	if ( !_officer ) then {
    	{
			if ( _x in RG_oScope ) exitWith {
				if ( _drop ) then { _container addItemCargoGlobal [_x, 1]; };
				player removePrimaryWeaponItem  _x;
	            private _msg = (["gearRestrictions", "oScope"] call core_fnc_getSetting);
				_msgs pushback _msg;
			};
    	} forEach _primItems;
	};
};

if ( (["restrictBackpack"] call core_fnc_getParam) == 1 ) then {
	if ( !_at ) then {
		private _backpack = backpack player;
		if ( _backpack in RG_backpack) then {
			if ( _drop ) then { 
				_container addItemCargoGlobal [_backpack, 1];
				private _eContainer = (everyContainer _container);
				private _eBackpack = _eContainer select (_eContainer find _backpack);
				{ _eBackpack addItemCargoGlobal [_x, 1]; } forEach (backpackItems player);
			};
			removeBackpack player;
            private _msg = (["gearRestrictions", "backpack"] call core_fnc_getSetting);
			_msgs pushback _msg;
		};
	};
};

if ( (["restrictUAV"] call core_fnc_getParam) == 1 ) then {
	if ( !_uavOp && !_sniper && !_spotter ) then {
    	private _items = assignedItems player;
        private _terms = ["B_UavTerminal", "I_UavTerminal", "O_UavTerminal"];
    	if (({_x in _terms} count _items) > 0) then {
            {
    			player unassignItem _x;
            	player removeItem _x;
			} forEach _terms;
            private _msg = (["gearRestrictions", "uav"] call core_fnc_getSetting);
            _msgs pushback _msg;
    	};
    };
};

if ( (["restrictMBRItem"] call core_fnc_getParam) == 1 ) then {
	if ( !_isMBR ) then {
    	private _items = assignedItems player;
    	private _removed = false;
    	{
    		if ( _x in RG_mbrItems ) then { 
    			_removed = true;
    			player removeItem _x;
    		};
    		
    	} forEach _items;
    	if ( _removed ) then {
            private _msg = (["gearRestrictions", "mbrItem"] call core_fnc_getSetting);
    		_msgs pushback _msg; 
    	};
    };
};


if ( (count _msgs) > 0 ) then {
	_msgs spawn {
		private _duration = (["gearRestrictions", "duration"] call core_fnc_getSetting);
		titleText [(_this joinString "\n"), "PLAIN"];
    	sleep _duration;
        titleFadeOut 2;	
    };
};