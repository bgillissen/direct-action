/*
@filename: feats\dataLink\playerAssemble.sqf
Author:
	Ben
Description:
	run on player when assemble a weapon,
	enable uav dataLink target report
*/

#include "_debug.hpp"

params ["_unit", "_veh"];

if !( unitIsUAV _veh ) exitWith { nil };

_veh setVehicleReportRemoteTargets true;

nil