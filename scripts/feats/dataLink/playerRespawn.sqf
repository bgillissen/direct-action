/*
@filename: feats\dataLink\playerRespawn.sqf
Author:
	Ben
Description:
	run on player,
	enable player dataLink target report (also triggered when another player join)
*/

player setVehicleReportRemoteTargets true;

nil