/*
@filename: feats\playerSide\serverPreInit.sqf
Author:
	Ben
Description:
	run on server,
	define player and enemy side, 
	compute enemies and allies according to mission paramters
*/

#include "_debug.hpp"

//check what is the players side
missionNamespace setVariable ["PLAYER_SIDE", [east, west, independent] select (["side"] call core_fnc_getParam), true];

if ( PLAYER_SIDE isEqualTo east ) then {
	#ifdef DEBUG
	debug(LL_INFO, "side is EAST");
	#endif
	missionNamespace setVariable ["PLAYER_SIDETXT", "EAST", true];
	missionNamespace setVariable ["PLAYER_SIDEDSP", "OPFOR", true];
	missionNamespace setVariable ["OPFOR_ARE_ENEMY", false, true];
	missionNamespace setVariable ["BLUFOR_ARE_ENEMY", true, true];
	missionNamespace setVariable ["IND_ARE_ENEMY", [false, true] select (["indEnemy"] call core_fnc_getParam), true];
};
if ( PLAYER_SIDE isEqualTo west ) then {
	#ifdef DEBUG
	debug(LL_INFO, "side is WEST");
	#endif
	missionNamespace setVariable ["PLAYER_SIDETXT", "WEST", true];
	missionNamespace setVariable ["PLAYER_SIDEDSP", "BLUFOR", true];
	missionNamespace setVariable ["OPFOR_ARE_ENEMY", true, true];
	missionNamespace setVariable ["BLUFOR_ARE_ENEMY", false, true];
	missionNamespace setVariable ["IND_ARE_ENEMY", [false, true] select (["indEnemy"] call core_fnc_getParam), true];
};
if ( PLAYER_SIDE isEqualTo independent ) then {
	#ifdef DEBUG
	debug(LL_INFO, "side is IND");
	#endif
	missionNamespace setVariable ["PLAYER_SIDETXT", "IND", true];
	missionNamespace setVariable ["PLAYER_SIDEDSP", "INDEPENDENT", true];
	missionNamespace setVariable ["OPFOR_ARE_ENEMY", [false, true] select (["opforEnemy"] call core_fnc_getParam), true];
	missionNamespace setVariable ["BLUFOR_ARE_ENEMY", [false, true] select (["bluforEnemy"] call core_fnc_getParam), true];
	missionNamespace setVariable ["IND_ARE_ENEMY", false, true];
};

//change friendship according to parameters
civilian setFriend [PLAYER_SIDE, 1];
PLAYER_SIDE setFriend [civilian, 1];
if ( PLAYER_SIDE isEqualTo east ) then {
	independent setFriend [east, [1, 0] select IND_ARE_ENEMY];
	east setFriend [independent, [1, 0] select IND_ARE_ENEMY];
	west setFriend [east, [1, 0] select BLUFOR_ARE_ENEMY];
	east setFriend [west, [1, 0] select BLUFOR_ARE_ENEMY];
	independent setFriend [west, [0, 1] select IND_ARE_ENEMY];
	west setFriend [independent, [0, 1] select IND_ARE_ENEMY];
};
if (PLAYER_SIDE isEqualTo west ) then {
	west setFriend [east, [1, 0] select OPFOR_ARE_ENEMY];
	east setFriend [west, [1, 0] select OPFOR_ARE_ENEMY];
	independent setFriend [west, [1, 0] select IND_ARE_ENEMY];
	west setFriend [independent, [1, 0] select IND_ARE_ENEMY];	
	independent setFriend [east, [0, 1] select IND_ARE_ENEMY];
	east setFriend [independent, [0, 1] select IND_ARE_ENEMY];
};
if (PLAYER_SIDE isEqualTo independent ) then {
	independent setFriend [east, [1, 0] select OPFOR_ARE_ENEMY];
	east setFriend [independent, [1, 0] select OPFOR_ARE_ENEMY];
	independent setFriend [west, [1, 0] select BLUFOR_ARE_ENEMY];
	west setFriend [independent, [1, 0] select BLUFOR_ARE_ENEMY];	
	west setFriend [east, [0, 1] select (BLUFOR_ARE_ENEMY && OPFOR_ARE_ENEMY)];
	east setFriend [west, [0, 1] select (BLUFOR_ARE_ENEMY && OPFOR_ARE_ENEMY)];
};

//define enemies array
ENEMIES = [];
ALLIES = [];
if ( OPFOR_ARE_ENEMY ) then {
	OPFOR_ENEMY_KEY = count ENEMIES;
	ENEMIES append [east]; 
} else {
	ALLIES append [east];
};
if ( BLUFOR_ARE_ENEMY ) then {
	BLUFOR_ENEMY_KEY = count ENEMIES;
	ENEMIES append [west];
} else {
	ALLIES append [west];
};
if ( IND_ARE_ENEMY ) then {
	IND_ENEMY_KEY = count ENEMIES;
	ENEMIES append [independent]; 
} else {
	ALLIES append [independent];
};

#ifdef DEBUG
private _debug = format["Enemies are : %1", (ENEMIES joinString ", ")];
debug(LL_DEBUG, _debug);
private _debug = format["Allies are : %1", (ALLIES joinString ", ")];
debug(LL_DEBUG, _debug);
#endif

publicVariable "ALLIES";
publicVariable "ENEMIES";

nil