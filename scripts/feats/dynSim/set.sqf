/*
@filename: feats\dynSim\set.sqf
Author:
	Ben
Description:
	run on anywhere,
	enable / disable dynamic simulation on given thing,
*/

#include "_debug.hpp"

if !( dynSim ) exitWith {};

params ["_thing", "_bool"];

_thing enableDynamicSimulation _bool;