/*
@filename: feats\dynSim\playerAssemble.sqf
Author:
	Ben
Description:
	run on player when assemble a weapon,
	make uav group trigger on dynamic simulation,
*/

#include "_debug.hpp"

params ["_unit", "_veh"];

if ( (["dynSim"] call core_fnc_getParam) == 0 ) exitWith {
    #ifdef DEBUG
   	debug(LL_INFO, "disabled by mission parameter");
    #endif
    nil 
};

if !( unitIsUAV _veh ) exitWith { nil };

[(group driver _veh), true] call dynSim_fnc_setTrigger;
//(group driver _veh) enableDynamicSimulation true;

nil