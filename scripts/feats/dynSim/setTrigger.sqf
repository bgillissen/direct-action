/*
@filename: feats\dynSim\setTrigger.sqf
Author:
	Ben
Description:
	run anywhere,
	make given group units trigger on dynamic simulation of other units
*/

#include "_debug.hpp"

if !( dynSim ) exitWith {};

params ["_group", "_bool"];

{ _x triggerDynamicSimulation _bool; } forEach (units _group);