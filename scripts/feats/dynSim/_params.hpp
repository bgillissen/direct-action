class dynSim {
	title = "Enable dynamic simulation on spawned AI";
	values[] = {0,1};
	texts[] = {"No","Yes"};
	default = 1;
};
