/*
@filename: feats\dynSim\serverPreInit.sqf
Author:
	Ben
Description:
	run on server,
	enable / disable dynamic simulation,
*/

#include "_debug.hpp"

dynSim = !( (["dynSim"] call core_fnc_getParam) isEqualTo 0 );

enableDynamicSimulationSystem dynSim;

if !( dynSim ) exitWith {
    #ifdef DEBUG
   	debug(LL_INFO, "disabled by mission parameter");
    #endif
    nil     
};

{
	(configName _x) setDynamicSimulationDistance (getNumber _x);    
} forEach (configProperties [(missionConfigFile >> 'settings' >> 'dynSim'), 'true', false]);

nil