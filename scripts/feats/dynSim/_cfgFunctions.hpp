class dynSim {
	tag = "dynSim";
	class functions {
		class serverPreInit { file="feats\dynSim\serverPreInit.sqf"; };
		class playerAssemble { file="feats\dynSim\playerAssemble.sqf"; };
		class set { file="feats\dynSim\set.sqf"; };
		class setTrigger { file="feats\dynSim\setTrigger.sqf"; };
	};
};
