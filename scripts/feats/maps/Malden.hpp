class MALDEN {
	keywords[] = {"dry", "water", "urban", "sea"};
	bases[] = {"small"};
	class zones {
		class zoneA {
			consecutiveAOs = 2;
			aos[] = {"Moray",
					 "Lolisse",
					 "Airport",
					 "North Military Base",
					 "Radio Station",
					 "Research Center",
					 "Bosquet",
					 "Saint Louis",
					 "Mont Chauve"};
			fobs[] = {};
		};
		class zoneB {
			consecutiveAOs = 3;
			aos[] = {"Larche",
					 "Goisse",
					 "Saint Jean",
					 "La Trinite",
					 "Le Roi",
					 "Dourdan",
					 "Arudy",
					 "Feas",
					 "Vigny",
					 "La Pessagne",
					 "Eperon",
					 "Guran",
					 "Houdan"};
			fobs[] = {};
		};
		class zoneC {
			consecutiveAOs = 3;
			aos[] = {"Dents du midi",
					 "Corton",
					 "Dorres",
					 "Sainte Marie",
					 "Chapoi",
					 "La Riviere",
					 "Saint Martin",
					 "Faro",
					 "Cancon",
					 "Baie du Nain",
					 "Power Plant",
					 "Le Port",
					 "South Military Base",
					 "Arette"};
			fobs[] = {};
		};
	};
};
