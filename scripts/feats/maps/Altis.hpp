class ALTIS {
	keywords[] = {"dry", "water", "urban", "sea", "blockade"};
	bases[] = {"airbase"};
	class zones {
		class zoneA {
			consecutiveAOs = 3;
			aos[] = {"Oreokastro",
					 "Faros",
					 "Krya Nera Turbines",
					 "Aristi Turbines",
					 "Krya Nera Airfield",
					 "Abdera",
					 "Frini Woodlands",
					 "Frini",
					 "Dump"};
			fobs[] = {"fob_1", "fob_2", "fob_3", "fob_4", "fob_5", "fob_6"};
		};
		class zoneB {
			consecutiveAOs = 3;
			aos[] = {"Research Facility",
					 "Ghost Hotel",
					 "Delfinaki Outpost",
					 "Sofia Powerplant",
					 "Gatolia Solar Farm",
					 "Sofia Radar Station",
					 "Nidasos Woodlands",
					 "Molos",
					 "Molos Airfield"};
			fobs[] = {"fob_7", "fob_8", "fob_9", "fob_10", "fob_11"};
		};
		class zoneC {
			consecutiveAOs = 3;
			aos[] = {"Fotia Turbines",
					 "Pyrsos",
					 "Galati Outpost",
					 "Athira",
					 "Syrta",
					 "Koroni",
					 "Agios Konstantinos",
					 "Orino",
					 "Negades",
					 "Factory",
					 "Kore",
					 "Airbase",
					 "Power plant",
					 "The Stadium",
					 "Topolia",
					 "Agios Dionysios",
					 "Lakka",
					 "Xirolimni Dam",
					 "Alikampos",
					 "Neochori",
					 "Poliakko",
					 "Athanos",
					 "Panachori Bay",
					 "Panachori",
					 "The Crater",
					 "Zaros Power Station",
					 "Zaros Bay Outpost",
					 "Therisa"};
			fobs[] = {"fob_12", "fob_13", "fob_14", "fob_15", "fob_16", "fob_17", "fob_18", "fob_19", "fob_20"};
		};
		class zoneD {
			consecutiveAOs = 3;
			aos[] = {"Kalithea",
					 "Rodopoli",
					 "Charkia",
					 "Limni",
					 "Ochrolimni",
					 "Paros",
					 "Dorida",
					 "Pyrgos",
					 "Chalkeia"};
			fobs[] = {"fob_21", "fob_22", "fob_23", "fob_24"};
		};
		class zoneE {
			consecutiveAOs = 2;
			aos[] = {"Sfaka", "Skopos Castle", "Vikos Outpost", "Eginio"};
			fobs[] = {"fob_25", "fob_26"};
		};
		class zoneF {
			consecutiveAOs = 2;
			aos[] = {"Faronaki",
					 "Didymos Turbines",
					 "Panagia",
					 "Feres",
					 "Selakano Outpost"};
			fobs[] = {"fob_27", "fob_28", "fob_29"};
		};
	};
};
