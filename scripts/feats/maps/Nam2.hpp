class Nam2 {
	keywords[] = {"jungle", "water", "urban"};
	bases[] = {"khesanh"};
	class zones {
		class zoneA {
			consecutiveAOs = 3;
			aos[] = {"Pi Pi Villages",
					 "Outpost Alaska",
					 "NVA Camp",
					 "Date Plantation",
					 "Air CAV Refueling Depot",
					 "Hill 196",
					 "Tan Son Nhut Air Base",
					 "Paddy field",
					 "Hill 80"};
			fobs[] = {};
		};
		class zoneB {
			consecutiveAOs = 3;
			aos[] = {"NVA Regimental HQ",
					 "Chen",
					 "Mauo Sung Island",
					 "FOB Kojak",
					 "NVA River Camp",
					 "POW Camp",
					 "Temple Ruins",
					 "NVA Hill Camp",
					 "River outpost",
					 "Small Village",
					 "Abandoned City",
					 "Canyone Outpost",
					 "Small outpost"};
			fobs[] = {};
		};
		class zoneC {
			consecutiveAOs = 3;
			aos[] = {"Small Camp",
					 "Kwai River Bridge",
					 "Paradise Island",
					 "Japanese Tower",
					 "Rock Area",
					 "Hill 222",
					 "Field Area"};
			fobs[] = {};
		};
		class zoneD {
			consecutiveAOs = 3;
			aos[] = {"Firebase Mac",
					 "Hill 102",
					 "Hen San",
					 "Lost Church",
					 "Hill 143",
					 "Palm Oil Plantation",
					 "North Saigon",
					 "South Saigon",
					 "Lake Camp",
					 "Jungle Town"};
			fobs[] = {};
		};
		class zoneE {
			consecutiveAOs = 3;
			aos[] = {"Island Camp",
					 "Dang Sui",
					 "Agent Orange Area",
					 "Firebase Trinity",
					 "Island Outpost",
					 "Hidden Camp",
					 "Hill Camp",
					 "Plantation",
					 "Hoi Nam",
					 "Patong",
					 "Jungle City"};
			fobs[] = {};
		};
		class zoneF {
			consecutiveAOs = 3;
			aos[] = {"My Lai",
					 "Submerged City",
					 "NVA Comm Camp",
					 "Defense Line",
					 "Hill 147",
					 "NVA Eastern Camp",
					 "Outpost Clark",
					 "Camp Arthur",
					 "Riverbed Village",
					 "Outpost Irene",
					 "Hill 56",
					 "Lakes Area"};
			fobs[] = {};
		};
		class zoneG {
			consecutiveAOs = 3;
			aos[] = {"Camp Alamo",
					 "Outpost Skurniki",
					 "Sung Chi",
					 "Western Town",
					 "Nui Dat Camp",
					 "Open Area",
					 "Ham Song",
					 "Gump Village",
					 "Majung Island",
					 "Hill 111"};
			fobs[] = {};
		};
		class zoneH {
			consecutiveAOs = 3;
			aos[] = {"Puk Po",
					 "Camp Moke",
					 "Wetfoot Village",
					 "Hamburger Hill",
					 "Phon Phi",
					 "Ci Ho Ci",
					 "Area 51",
					 "Lost Camp",
					 "Hill 51",
					 "Hill 90"};
			fobs[] = {};
		};
		class zoneI {
			consecutiveAOs = 3;
			aos[] = {"Camp Iron",
					 "Detroit Village",
					 "Hill 123",
					 "Nixon Village",
					 "Outpost Miami",
					 "Suo Gan",
					 "Scott Area",
					 "Papaye Camp",
					 "Swamp",
					 "Bridge 2Far",
					 "Bien Hoa Airbase",
					 "Broken Trees"};
			fobs[] = {};
		};
	};
};
