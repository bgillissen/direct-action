/*
@filename: feats\earPlugs\playerPostInit.sqf
Author:
	Ben
Description:
	run on player side
	this add the key binding to plugs in/out the earplugs
	optionaly display a hint to let player know about this feature
*/

#include "_debug.hpp";

if ( (["earPlugs"] call core_fnc_getParam) == 0 ) exitWith {
    #ifdef DEBUG
    debug(LL_INFO, "disabled by mission parameter");
    #endif
    nil
};

earPlug_state = false;

if ( MOD_ace ) exitWith {
	#ifdef DEBUG
    debug(LL_INFO, "disabled by ACE presence");
    #endif
    nil
};

if ( (["earPlugs", "showHint"] call core_fnc_getSetting) == 1 ) then {
	hint parseText (["earPlugs", "hint"] call core_fnc_getSetting);
};

["earPlugs", "Toggle earplugs in / out", "earPlugs_fnc_swapState"] call keybind_fnc_add;

nil