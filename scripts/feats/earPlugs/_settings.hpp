
#include "..\..\common\_dik.hpp"

class earPlugs {
	volume = 0.2;
	onMsg = "Earplugs fitted";
	offMsg = "Earplugs removed";
	keycode[] = {DIK_PAUSE, 0, 0 ,0};//pause
	showHint = 0;
	hint = "<t color='#faaf3a' size='1.2' shadow='1' shadowColor='#000000' align='center'>** Earplugs Recieved **</t>          Use [Pause/Break] key to fit and remove them";
};
