class loadBalance {
	tag = "loadBalance";
	class functions {
		class getClientID { file="feats\loadBalance\getClientID.sqf"; };
		class playerAssemble { file="feats\loadBalance\playerAssemble.sqf"; };
		class serverDestroy { file="feats\loadBalance\serverDestroy.sqf"; };
		class serverPostInit { file="feats\loadBalance\serverPostInit.sqf"; };
		class spreadEvenly { file="feats\loadBalance\spreadEvenly.sqf"; };
	};
};
