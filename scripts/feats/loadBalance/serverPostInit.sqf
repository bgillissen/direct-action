/*
@filename: feats\loadBalance\serverInit.sqf
Author:
	Ben
Description:
	run on server
	to evenly distribute AI between headless clients
*/

#include "_debug.hpp"

if ( (["headless"] call core_fnc_getParam) == 0 ) exitWith {
	#ifdef DEBUG
    debug(LL_INFO, "disabled by mission parameter");
    #endif
	nil
};

if !( isMultiplayer ) exitWith {
	#ifdef DEBUG
    debug(LL_INFO, "running in single player mode, abording");
    #endif
	nil
};

private _delay = ["loadBalance", "loopDelay"] call core_fnc_getSetting;

#ifdef DEBUG
private _debug = format["thread is running with %1s delay", _delay];
debug(LL_INFO, _debug);
#endif

LB_lastHC = 0;
LB_lastErr = 0;

while { true } do {
	sleep _delay;
	call loadBalance_fnc_spreadEvenly;
};

nil