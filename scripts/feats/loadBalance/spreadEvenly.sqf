/*
@filename: feats\laodBalancing\spreadEvenly.sqf
Author:
	Ben
Description:
	run on server
	to evenly distribute AI between headless clients
*/

#include "_debug.hpp"
#define EMPTY_ARRAY []

private _ownerMap = [];
private _HCmap = [];
private _buckets = [];

private _HCs = (entities "HeadlessClient_F");
{
    private _id = owner _x;
    _ownerMap pushback _id;
    if ( _x in _HCs ) then {
        _HCmap pushback _id; 
    	_buckets pushback EMPTY_ARRAY;
	};
} forEach allPlayers;

if ( (count _HCmap) isEqualTo 0 ) exitWith {
    #ifdef DEBUG
  	debug(LL_DEBUG, "no headless client available, abording");
	#endif  
};

private _toMove = [];
private _totUnit = 0;
{
	private _grpUnit = count (units _x); 
	private _move = (_grpUnit > 0 );
    if ( _move ) then { _move = !( _x getVariable['NOLB', false]); };
	if ( _move ) then { _move = ( ({ isPlayer _x } count (units _x)) == 0 ); };
    if ( _move ) then {
		private _uavCrews = [];
    	{ _uavCrews append (crew _x) } forEach allUnitsUav; 
    	_move = ( ({_x in _uavCrews} count (units _x)) == 0 ); 
	};
	if ( _move ) then {
		private _grp = _x;
        private _grpUnit = (count (units _grp));
		private _owner = groupOwner _x;
		private _isOnHC = false;
        private _ownerIndex = _ownerMap find _owner;
        _totUnit = _totUnit + _grpUnit;
		if ( _ownerIndex < 0 ) then { //on server, move forced 
            _toMove pushback [_grpUnit, _grp, false];
        } else {
			if ( (_ownerMap select _ownerIndex) in _HCmap ) then { //on HC, add to bucket
                _hcIndex = _HCmap find _owner;
                (_buckets select _hcIndex) append [[_grpUnit, _grp, false]];
            } else { //on a player move forced
            	_toMove pushback [_grpUnit, _grp, false];
            };
		};
	};
} forEach allGroups;

if ( ((count _toMove) isEqualTo 0) && (LB_lastHC isEqualTo (count _HCmap)) && (LB_lastErr isEqualTo 0) ) exitWith {};

private _unitAvg = round (_totUnit / (count _HCmap));

for "_pass" from 1 to 3 do { 

	{ _buckets set [_forEachIndex, ([_x, [], {_x select 0}, "DESCEND"] call BIS_fnc_sortBy)]; } forEach _buckets;

    private _hcAbove = [];
	private _hcAvg = [];
	private _hcUnder = [];	
	{
        private _hcUnits = 0;
        { _hcUnits = (_hcUnits + (_x select 0)); } forEach _x;
    	if ( _hcUnits > _unitAvg ) then { _hcAbove pushback _forEachIndex; };
    	if ( _hcUnits < _unitAvg ) then { _hcUnder pushback _forEachIndex; };
    	if ( _hcUnits == _unitAvg ) then { _hcAvg pushback _forEachIndex; };
	} forEach _buckets;

    {
        private _srcHC = _x;
        private _src = (_buckets select _srcHC);
		private _srcUnits = 0;
        { _srcUnits = _srcUnits + (_x select 0); } forEach _src;
        {
            private _trgtHC = _x;
            private _trgt = (_buckets select _trgtHC);
			private _trgtUnits = 0;
            { _trgtUnits = _trgtUnits + (_x select 0); } forEach _trgt;
            if ( _srcUnits > _unitAvg ) then {
            	{
                	private _srcGrp = _x;
                	if ( ((_trgtUnits + (_srcGrp select 0)) < _unitAvg) && !(_srcGrp select 2) ) then {
                    	_srcUnits = _srcUnits - (_srcGrp select 0);
                        _trgtUnits = _trgtUnits + (_srcGrp select 0);
            			_srcGrp set[2, true];
                    	(_buckets  select _trgtHC) append [_srcGrp];
                    	(_buckets  select _srcHC) deleteAt _forEachIndex;
					};
                        
            	} forEach _src;
			};
            
            {
  				{
                	private _srcGrp = _x;
                	if ( ((_trgtUnits + (_srcGrp select 0)) < _unitAvg) ) then {
                    	_srcUnits = _srcUnits - (_srcGrp select 0);
                        _trgtUnits = _trgtUnits + (_srcGrp select 0);
            			_srcGrp set[2, true];
                    	(_buckets  select _trgtHC) append [_srcGrp];
                    	_toMove deleteAt _forEachIndex;
					};
            	} forEach _src;
			} forEach _toMove;
            
		} forEach _hcUnder;
	} forEach _hcAbove;    
};

if ( (count _toMove) > 0 ) then {
    {
        private _srcGrp = _x;
		private _countSmallest = _totUnit;
		private "_smallest";
		{
            private _hcUnit = 0;
            { _hcUnit = _hcUnit + (_x select 0); } forEach _x;
			if ( _hcUnit < _countSmallest ) then {
				_smallest = _forEachIndex;
				_countSmallest = _hcUnit;
			};
		} forEach _buckets;
		_srcGrp set[2, true];
        (_buckets select _smallest) append [_srcGrp];
		_toMove deleteAt _forEachIndex;
	} forEach _toMove;
};

private _grpMoved = 0;
private _grpFailed = 0;
private _unitMoved = 0;
{
    private _trgtIndex = _forEachIndex;
    {
        _x params ["_totUnits", "_grp", "_hasMoved"];
        if ( _hasMoved ) then {
            private _trgt = (_HCmap select _trgtIndex);
            if ( _grp setGroupOwner _trgt ) then {
                _grpMoved = _grpMoved + 1;
                _unitMoved = _unitMoved + _totUnits;
            } else {
                _grpFailed = _grpFailed + 1;
			};
		};
	} forEach _x;    
} forEach _buckets;

LB_lastHC = count _HCmap;
LB_lastErr = _grpFailed;

#ifdef DEBUG
if ( _grpMoved > 0 || _grpFailed > 0 ) then {
	private _debug = format ["moved %1 group(s), %2 unit(s) to %3 HCs, %4 failed", _grpMoved, _unitMoved, (count _HCmap), _grpFailed];
	debug(LL_INFO, _debug);
	{
    	private _hcUnit = 0;
    	{ _hcUnit = _hcUnit + (_x select 0); } forEach (_buckets select _forEachIndex);
		_debug = format ["clientID %1 now owns %2 group(s), %3 unit(s)", _x, (count (_buckets select _forEachIndex)), _hcUnit]; 
		debug(LL_DEBUG, _debug);
	} forEach _HCmap;
};
#endif