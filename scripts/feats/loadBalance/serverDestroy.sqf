/*
@filename: feats\loadBalance\serverDestroy.sqf
Author:
	Ben
Description:
	run on server,
	kill the loadBalance thread
 */

if ( count(_this) == 0 ) exitWith { nil };

params ["_when", "_thread"];

if ( scriptDone _thread ) exitWith { nil };

terminate _thread;

waitUntil{ scriptDone _thread };

nil