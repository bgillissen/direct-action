/*
@filename: feats\loadBalance\playerAssemble.sqf
Author:
	Ben
Description:
	run on player
	disable load balance on assembled uav
*/

#include "_debug.hpp"

if ( (["headless"] call core_fnc_getParam) == 0 ) exitWith {
	#ifdef DEBUG
    debug(LL_INFO, "disabled by mission parameter");
    #endif
    nil
};

if !( isMultiplayer ) exitWith {
	#ifdef DEBUG
    debug(LL_INFO, "running in single player mode, abording");
    #endif
    nil
};

params ["_unit", "_veh"];

if !( unitIsUAV _veh ) exitWith { nil };

(group (driver _veh)) setVariable ['NOLB', true, true];

nil