class headless {
	title = "Headless Client Load Balancing";
	values[] = {0,1};
	texts[] = {"Disabled","Enabled"};
	default = 1;
};
