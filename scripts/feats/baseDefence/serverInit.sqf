/*
@filename: feats\baseDefence\serverInit.sqf
Author:
	Ben
Description:
	run on server
*/

#include "_debug.hpp"

BD_spawnStack = [];
BD_thread = scriptNull;

private _mode = ["baseDefence"] call core_fnc_getParam;

if ( _mode isEqualTo 0 ) exitWith {
    #ifdef DEBUG
	debug(LL_INFO, "disabled by mission parameter");
    #endif
    nil
};

BD_availAt = time;
BD_active = false;
BD_stop = false;

if ( _mode isEqualTo 1 ) exitWith {
    #ifdef DEBUG
    debug(LL_INFO, "permanent by mission parameter");
    #endif
	[objNull, objNull, -1, true] spawn baseDefence_fnc_spawn;
    nil
};

publicVariable "BD_availAt";
publicVariable "BD_active";

nil