/*
@filename: feats\baseDefence\spawn.sqf
Author:
	Ben
Description:
	run on player,
	spawn the baseDefence, 
	depending on settings it will also wait for a given period before despawning them.  
*/

#include "_debug.hpp"

if !( isServer ) exitWith {
	_this remoteExec ["baseDefence_fnc_spawn", 2];
};

params ["_thing", "_player", "_id", "_arg"];

private _cooldown = (["baseDefence_cooldown"] call core_fnc_getParam);
private _duration = (["baseDefence_duration"] call core_fnc_getParam);

BD_availAt = time + _cooldown;

publicVariable "BD_availAt";
publicVariable "BD_active";

private _locations = [];
for "_x" from 1 to 99 do {
	private _markerName = format["BD_%1", _x];
	if !( getMarkerPos _markerName isEqualTo [0,0,0] ) then { 
		_locations pushback _markerName; 
	};
};

if ( count _locations == 0 ) exitWith {
    #ifdef DEBUG
    debug(LL_ERR, "No baseDefence markers found in active base, abording");
    #endif
	if !( isNull _player ) then {
		[1, "No baseDefence markers found in this base", ["HQ", PLAYER_SIDE]] call global_fnc_chat;
	};
};

if ( count BV_aaTank == 0 ) exitWith {
    #ifdef DEBUG
    debug(LL_ERR, "aaTank base vehicle pool is empty, abording");
    #endif
	if !( isNull _player ) then {
		[1, "aaTank base vehicle pool is empty", ["HQ", PLAYER_SIDE]] call global_fnc_chat;
	};
    
};

BD_active = true;
publicVariable "BD_active";

private _infAmmo = [false, true] select (["baseDefence", "infiniteAmmo"] call core_fnc_getSetting);
private _skill = ["baseDefence", "skill"] call core_fnc_getSetting;
private _crew = ["baseDefence", "crew"] call core_fnc_getSetting;

private _group = createGroup PLAYER_SIDE;
_group setVariable['NOLB', true];
BD_spawnStack pushback _group;

private _vname = format["RL_crew_%1", PLAYER_SIDE];
private _loadout =  missionNamespace getVariable _vname;

if ( isNil "_loadout" ) exitWith {
    private _debug = format["roleLoadout variable '%1' is not defined, abording", _vname];
	#ifdef DEBUG
   	debug(LL_ERR, _debug);
    #endif
    if !( isNull _player ) then { [1, _debug, ["HQ", PLAYER_SIDE]] call global_fnc_chat; };
};

_loadout params["_u", "_v", "_b", "_pw", "_sw", "_hw", "_h", "_f", "_c", "_t", "_m", "_bino", "_n", "_w", "_cp"];

{	
	private _class = selectRandom BV_aaTank;
	private _pos = getMarkerPos _x;
	private _dir = MarkerDir _x;
	private _veh = _class createVehicle _pos;
	_veh setDir _dir;
	_veh setFuel 0;
	_veh allowDamage false;
	_veh lock 3;
    if ( isClass(configFile >> "cfgVehicles" >> _class >> "Components" >> "SensorsManagerComponent") ) then {
		_veh setVehicleReportRemoteTargets true;
    	_veh setVehicleReceiveRemoteTargets true; 
	} else {
		_veh setVehicleReportRemoteTargets false;
    	_veh setVehicleReceiveRemoteTargets false; 
	};
	clearWeaponCargoGlobal _veh;
	clearMagazineCargoGlobal _veh;
	clearItemCargoGlobal _veh;
	clearBackpackCargoGlobal _veh;
	if ( _infAmmo ) then {
		_veh addEventHandler ["Fired", {(_this select 0) setVehicleAmmo 1;}];
	};
	BD_spawnStack pushback _veh;
	_crew createUnit [_pos, _group];
	private _commander = (units _group) select (count (units _group) - 1);
	[_commander, _u, _v, _b, _pw, _sw, _hw, _h, _f, _c, _t, _m, _bino, _n, _w, _cp] call common_fnc_setLoadout;
	_commander allowDamage false;
	_commander assignAsCommander _veh;
	_commander moveInCommander _veh;
	_commander setVariable["NOAI", true, true];
	BD_spawnStack pushback _commander;
	_crew createUnit [_pos, _group];
	private _gunner = (units _group) select (count (units _group) - 1);
	[_gunner, _u, _v, _b, _pw, _sw, _hw, _h, _f, _c, _t, _m, _bino, _n, _w, _cp] call common_fnc_setLoadout;
	_gunner allowDamage false;
	_gunner assignAsGunner _veh;
	_gunner moveInGunner _veh;
	_gunner setVariable["NOAI", true, true];
	BD_spawnStack pushback _gunner;
} forEach _locations;

[(units _group), _skill] call common_fnc_setSkill;

if !( (["baseDefence"] call core_fnc_getParam) isEqualTo 2 ) exitWith {};

BD_thread = _thisScript;

private _msg = ["baseDefence", "message"] call core_fnc_getSetting;
if ( _cooldown < _duration ) then {
	_cooldown = _duration;
};
[1, format[_msg, (name _player), round (_duration / 60), round (_cooldown / 60)], ["HQ", PLAYER_SIDE]] call global_fnc_chat;

private _despawnAt = time + _duration;

waitUntil {
	sleep 5;
	(time > _despawnAt || BD_stop) 
};
 
BD_spawnStack call common_fnc_deleteObjects;
BD_spawnStack = [];

BD_active = false;
publicVariable "BD_active";