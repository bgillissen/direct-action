class baseDefence : feat_base {
	class server : featServer_base {
		class destroy : featEvent_enable {};
		class init : featEvent_enable {};
	};
	class player : featPlayer_base {
		class init : featEvent_enable {};
	};
};
