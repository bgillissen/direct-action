/*
@filename: feats\baseDefence\playerInit.sqf
Author:
	Ben
Description:
	run on player,
	add action to base things to spawn base Defences. 
*/

#include "_debug.hpp"

if ( (["baseDefence"] call core_fnc_getParam) > 1 ) exitWith {
    #ifdef DEBUG
    debug(LL_INFO, "skipping add action, either disabled or permanent");
    #endif
    nil
};

private _action = (["baseDefence", "action"] call core_fnc_getSetting);
{
	{
		_x params ["_thing", "_actions"];
		{
			if ( (configName _x) isEqualTo "baseDefence" ) then {
                #ifdef DEBUG
                private _debug = format["adding action to %1", _thing];
                debug(LL_DEBUG, _debug);
                #endif
				_thing addAction [_action, {_this spawn baseDefence_fnc_spawn}, true, 0, false, true, "", "time > BD_availAt && !DB_active", 4];
                
			};
		} forEach _actions;
	} forEach _x;
} forEach [BA_veh, BA_npc, BA_obj];

nil