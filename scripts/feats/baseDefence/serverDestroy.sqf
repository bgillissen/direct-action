/*
@filename: feats\baseDefence\serverDestroy.sqf
Author:
	Ben
Description:
	run on server,
*/

#include "_debug.hpp"

if !( isNull BD_thread ) then {
    #ifdef DEBUG
    debug(LL_DEBUG, "thread is running, terminating");
    #endif
    BD_stop = true;
	waitUntil { scriptDone BD_thread };
    #ifdef DEBUG
    debug(LL_DEBUG, "thread has finnish");
    #endif
};

if ( (count BD_spawnStack) > 0 ) then {
    #ifdef DEBUG
    debug(LL_DEBUG, "is spawned, removing");
    #endif
	[BD_spawnStack] call common_fnc_deleteObjects;
	BD_spawnStack = [];    
};

nil