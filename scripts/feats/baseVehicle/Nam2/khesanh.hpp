class khesanh {
	class heliSmall {
		pool = "heliSmall";
		respawnDelay = 20;
		markers[] = {"BV_mHeli_1"};
		class actions {};
	};
	class heliMedium {
		pool = "heliMedium";
		respawnDelay = 20;
		markers[] = {"BV_mHeli_1"};
		class actions {};
	};
	class heliMedEvac {
		pool = "heliMedEvac";
		respawnDelay = 20;
		markers[] = {"BV_evacHeli_1"};
		class actions {};
	};
	class heliBig {
		pool = "heliBig";
		respawnDelay = 20;
		markers[] = {"BV_bHeli_1"};
		class actions {};
	};
	class heliAttack {
		pool = "heliAttack";
		respawnDelay = 20;
		markers[] = {"BV_bHeli_1"};
		class actions {};
	};
	class cars {
		pool = "car";
		respawnDelay = 20;
		markers[] = {"BV_car_1", "BV_car_2", "BV_car_3", "BV_car_4"};
		class actions {};
	};
	class armedCars {
		pool = "carArmed";
		respawnDelay = 20;
		markers[] = {"BV_acar_1", "BV_acar_2"};
		class actions {};
	};
	class trucks {
		pool = "truck";
		respawnDelay = 20;
		markers[] = {"BV_truck_1"};
		class actions {};
	};
	class rhibs {
		pool = "boatSmall";
		respawnDelay = 20;
		markers[] = {"BV_sBoat_1", "BV_sBoat_2"};
		class actions {};
	};
	class bigBoats {
		pool = "boatBig";
		respawnDelay = 20;
		markers[] = {"BV_bBoat_1", "BV_bBoat_2"};
		class actions {};
	};
	class tanks {
		pool = "tank";
		respawnDelay = 20;
		markers[] = {"BV_tank_1"};
		class actions {};
	};
	class apc {
		pool = "apc";
		respawnDelay = 20;
		markers[] = {"BV_apc_1", "BV_apc_2"};
		class actions {};
	};
	class ambulance {
		pool = "landMedic";
		respawnDelay = 20;
		markers[] = {"BV_landEvac_1"};
		class actions {};
	};
};
