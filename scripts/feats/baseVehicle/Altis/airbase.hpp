class airbase {
	class littlebird {
		pool = "heliSmall";
		respawnDelay = 20;
		markers[] = {"BV_sHeli_1"};
		class actions {};
	};
	class pawnee {
		pool = "heliSmallArmed";
		respawnDelay = 20;
		markers[] = {"BV_sHeliAtk_1"};
		class actions {};
	};
	class blackhawk {
		pool = "heliMedium";
		respawnDelay = 20;
		markers[] = {"BV_mHeli_1"};
		class actions {};
	};
	/*
	class apache {
			pool = "heliAttack";
			respawnDelay = 20;
			markers[] = {"BV_heliAtk_1"};
			class actions {};
		};
	 */
	class medEvac {
		pool = "heliMedEvac";
		respawnDelay = 20;
		markers[] = {"BV_medHeli_1"};
		class actions {};
	};
	class bigHeli {
		pool = "heliBig";
		respawnDelay = 20;
		markers[] = {"BV_lHeli_1"};
		atls[] = {0.2};
		class actions {};
	};
	class cas {
		pool = "planeCAS";
		respawnDelay = 20;
		markers[] = {"BV_casPlane_1"};
		class actions {};
	};
	class aa {
		pool = "planeAA";
		respawnDelay = 20;
		markers[] = {"BV_aaPlane_1"};
		class actions {};
	};
	class c130 {
		pool = "planeTransport";
		respawnDelay = 20;
		markers[] = {"BV_tPlane_1"};
		class actions {};
	};
	class uavs {
		pool = "uav";
		respawnDelay = 20;
		markers[] = {"BV_uav_1"};
		class actions {};
	};
	class cars {
		pool = "car";
		respawnDelay = 20;
		markers[] = {"BV_car_2"};
		class actions {
			class centerOfMass {
				kinds[] = {"rhsusf_rg33_base", "rhsusf_caiman_base", "rhsusf_RG33L_base"};
				ratios[] = {1, 1, 1.2};
			};
		};
	};
	class armedCars {
		pool = "carArmed";
		respawnDelay = 20;
		markers[] = {"BV_car_1", "BV_car_3"};
		class actions {
			class centerOfMass {
				kinds[] = {"rhsusf_rg33_base", "rhsusf_caiman_base", "rhsusf_RG33L_base"};
				ratios[] = {1, 1, 1.2};
			};
		};
	};
	class fobArmedCars {
			pool = "carArmed";
			respawnDelay = 20;
			markers[] = {"BV_fob_car_1", "BV_fob_car_2"};
			class actions {
				class centerOfMass {
					kinds[] = {"rhsusf_rg33_base", "rhsusf_caiman_base", "rhsusf_RG33L_base"};
					ratios[] = {1, 1, 1.2};
				};
			};
	};
	class apcs {
		pool = "apc";
		respawnDelay = 20;
		markers[] = {"BV_apc_1", "BV_apc_2"};
		class actions {};
	};
	class fobApcs {
		pool = "apc";
		respawnDelay = 20;
		markers[] = {"BV_fob_apc_1"};
		class actions {};
	};
	class tanks {
		pool = "tank";
		respawnDelay = 20;
		markers[] = {"BV_tank_1"};
		class actions {};
	};
	class fobTanks {
		pool = "tank";
		respawnDelay = 20;
		markers[] = {"BV_fob_tank_1"};
		class actions {};
	};
	class subs {
		pool = "sub";
		respawnDelay = 20;
		markers[] = {"BV_sub_1", "BV_sub_2"};
		class actions {
			class paint { color = "#(argb,8,8,3)color(0.75,0.49,0.09,1)"; };
		};
	};
	class rhibs {
		pool = "boatSmall";
		respawnDelay = 20;
		markers[] = {"BV_sBoat_1", "BV_sBoat_2"};
		class actions {};
	};
	class assaultBoats {
		pool = "boatAttack";
		respawnDelay = 20;
		markers[] = {"BV_boatAtk_1"};
		class actions {};
	};
	class mkv {
		pool = "boatBig";
		respawnDelay = 20;
		markers[] = {"BV_lBoat_1"};
		class actions {};
	};
	class trucks {
		pool = "truck";
		respawnDelay = 20;
		markers[] = {"BV_truck_1", "BV_truck_2"};
		class actions {
			class centerOfMass {
				kinds[] = {"rhsusf_rg33_base", "rhsusf_caiman_base", "rhsusf_RG33L_base"};
				ratios[] = {1, 1, 1.2};
			};
		};
	};
	class quads {
		pool = "quad";
		respawnDelay = 20;
		markers[] = {"BV_quad_1", "BV_quad_2", "BV_quad_3", "BV_quad_4", "BV_quad_5", "BV_quad_6",
					 "BV_quad_7", "BV_quad_8", "BV_quad_9", "BV_quad_10", "BV_quad_11", "BV_quad_12",
					 "BV_quad_13", "BV_quad_14", "BV_quad_15"};
		class actions {};
	};
	class repair {
		pool = "repair";
		respawnDelay = 20;
		markers[] = {"BV_service_1"};
		class actions {};
	};
	class fuel {
		pool = "fuel";
		respawnDelay = 20;
		markers[] = {"BV_service_2"};
		class actions {};
	};
	class ambulance {
		pool = "landMedic";
		respawnDelay = 20;
		markers[] = {"BV_landEvac_1"};
		class actions {
			class centerOfMass {
				kinds[] = {"rhsusf_rg33_base", "rhsusf_caiman_base", "rhsusf_RG33L_base"};
				ratios[] = {1, 1, 1.2};
			};
		};
	};
};
