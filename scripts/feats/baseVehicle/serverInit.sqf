/*
@filename: feats\baseVehicle\serverInit.sqf
Author:
	Ben
Description:
	run on server
 	spawn the base vehicles following the pool defined by the active base
 	ask vehicleRespawn to monitor them
*/

#include "_debug.hpp"

if ( BASE_NAME isEqualTo "" ) exitWith {
    #ifdef DEBUG
    debug(LL_ERR, "BASE_NAME is empty, baseAtmosphere probably failed");
    #endif
    nil
};

private _vehConf = ( missionConfigFile >> "settings" >> "baseVehicle" >> toUpper(worldName) >> BASE_NAME);

if !( isClass _vehConf ) exitWith {
	#ifdef DEBUG
	private _debug = format["settings class not found for base %1 (%2)", BASE_NAME, _vehConf];
	debug(LL_ERR, _debug);
	#endif
    nil
};


{
	private _poolName = getText(_x >> "pool");
	private _pool = missionNamespace getVariable format["BV_%1", _poolName];
	if ( !isNil "_pool" ) then {
		if ( count _pool > 0 ) then {
			private _delay = getNumber(_x >> "respawnDelay");
			private _actions = "true" configClasses (_x >> "actions");
			private _atls = getArray(_x >> "atls");
			{
				private _veh = createVehicle [(selectRandom _pool), [0,0,2000], [], 0, "CAN_COLLIDE"];
				_veh setDir (markerDir _x);
				private _pos = (getMarkerPos _x);
				private _atl = 0;
				if ( (count _atls) > _forEachIndex ) then {
					_atl = _atls select _forEachIndex;
					_pos set [2, _atl];	
				};
                _veh setPos _pos;
				[_veh, _delay, _poolName, _x, _atl, _actions] call vehicleRespawn_fnc_monitor;
			} forEach getArray(_x >> "markers");
		#ifdef DEBUG
		} else {
			private _debug = format["pool %1 is empty!", _poolName];
			debug(LL_WARN, _debug);
		#endif	
		};
	#ifdef DEBUG
	} else {
		private _debug = format["pool %1 is nil!", _poolName];
		debug(LL_WARN, _debug);
	#endif				
	};
	true
} forEach ("true" configClasses _vehConf);

nil