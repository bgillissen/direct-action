class small {
	class helo {
		pool = "heliMedium";
		respawnDelay = 20;
		markers[] = {"BV_mHeli_1"};
		class actions {};
	};
	class armedCars {
		pool = "carArmed";
		respawnDelay = 20;
		markers[] = {"BV_car_1"};
		class actions {};
	};
	class trucks {
		pool = "truck";
		respawnDelay = 20;
		markers[] = {"BV_truck_1"};
		class actions {};
	};
	class quads {
		pool = "quad";
		respawnDelay = 20;
		markers[] = {"BV_quad_1", "BV_quad_2", "BV_quad_3", "BV_quad_4", "BV_quad_5",
					 "BV_quad_6", "BV_quad_7", "BV_quad_8", "BV_quad_9"};
		class actions {};
	};
	class rhibs {
		pool = "boatSmall";
		respawnDelay = 20;
		markers[] = {"BV_sBoat_1", "BV_sBoat_2"};
		class actions {};
	};
	class subs {
		pool = "sub";
		respawnDelay = 20;
		markers[] = {"BV_sub_1", "BV_sub_2"};
		class actions {
			class paint { color = "#(argb,8,8,3)color(0.75,0.49,0.09,1)"; };
		};
	};
	class ambulance {
		pool = "landMedic";
		respawnDelay = 20;
		markers[] = {"BV_landEvac_1"};
		class actions {};
	};
};
