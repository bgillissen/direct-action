/*
@filename: feats\main.hpp
Author:
	Ben
Description:
	on all context,
	included by description.ext,
	include features compositions, debriefings, notifications, parameters, custom classes, rscTitles, settings, ui,
*/

#include "define.hpp"

#include "_cfgDebriefing.hpp"
#include "_cfgNotifications.hpp"
#include "_features.hpp"
#include "_include.hpp"
#include "_params.hpp"
#include "_rscTitles.hpp"
#include "_settings.hpp"
#include "_ui.hpp"
