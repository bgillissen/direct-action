/*
@filename: feats\_settings.hpp
Author:
	Ben
Description:
	on all context,
	included by feats\main.hpp,
	declare settings class and include feature's settings,
*/

class settings {
	#ifdef use_anthems
	#include "anthems\_settings.hpp"
	#endif

	#ifdef use_artiComputer
	#include "artiComputer\_settings.hpp"
	#endif

	#ifdef use_artiSupport
	#include "artiSupport\_settings.hpp"
	#endif

	#include "assets\_settings.hpp"
	#include "baseAtmosphere\_settings.hpp"

	#ifdef use_baseDefence
	#include "baseDefence\_settings.hpp"
	#endif

	#ifdef use_baseLight
	#include "baseLight\_settings.hpp"
	#endif

	#ifdef use_baseProtection
	#include "baseProtection\_settings.hpp"
	#endif

	#ifdef use_baseVehicle
	#include "baseVehicle\_settings.hpp"
	#endif

	#ifdef use_checkTS
	#include "checkTS\_settings.hpp"
	#endif

	#ifdef use_cleanUp
	#include "cleanup\_settings.hpp"
	#endif

	#ifdef use_curator
	#include "curator\_settings.hpp"
	#endif

	#include "dynSim\_settings.hpp"

	#ifdef use_earPlugs
	#include "earPlugs\_settings.hpp"
	#endif

	#ifdef use_environment
	#include "environment\_settings.hpp"
	#endif

	#ifdef use_flares
	#include "flares\_settings.hpp"
	#endif

	#ifdef use_gearRestrictions
	#include "gearRestrictions\_settings.hpp"
	#endif

	#ifdef use_gearSave
	#include "gearSave\_settings.hpp"
	#endif

	#include "groups\_settings.hpp"

	#ifdef use_ia
	#include "ia\_settings.hpp"
	#endif

	#ifdef use_loadBalance
	#include "loadBalance\_settings.hpp"
	#endif

	#ifdef use_mapTracker
	#include "mapTracker\_settings.hpp"
	#endif

	#include "maps\_settings.hpp"
	#include "memberData\_settings.hpp"

	#ifdef use_memberOnly
	#include "memberOnly\_settings.hpp"
	#endif

	#ifdef use_nightVision
	#include "nightVision\_settings.hpp"
	#endif
	
	#include "nutsKick\_settings.hpp"
	#include "playerSpawn\_settings.hpp"

	#ifdef use_playerTrait
	#include "playerTrait\_settings.hpp"
	#endif

	#ifdef use_radioFreq
	#include "radioFreq\_settings.hpp"
	#endif

	#ifdef use_revive
	#include "revive\_settings.hpp"
	#endif

	#ifdef use_roleRestrictions
	#include "roleRestrictions\_settings.hpp"
	#endif

	#ifdef use_serverRules
	#include "serverRules\_settings.hpp"
	#endif

	#ifdef use_squadHint
	#include "squadHint\_settings.hpp"
	#endif

	#ifdef use_stateMonitor
	#include "stateMonitor\_settings.hpp"
	#endif

	#ifdef use_supplyDrop
	#include "supplyDrop\_settings.hpp"
	#endif

	#ifdef use_supportCrate
	#include "supportCrate\_settings.hpp"
	#endif

	#ifdef use_va
	#include "va\_settings.hpp"
	#endif

	#ifdef use_vcomai
	#include "vcomai\_settings.hpp"
	#endif

	#ifdef use_vehicleCrew
	#include "vehicleCrew\_settings.hpp"
	#endif

	#ifdef use_vehicleDeco
	#include "vehicleDeco\_settings.hpp"
	#endif
	
	#ifdef use_vehicleLoadout
	#include "vehicleLoadout\_settings.hpp"
	#endif

	#ifdef use_vehicleRepair
	#include "vehicleRepair\_settings.hpp"
	#endif

	#ifdef use_vehicleRespawn
	#include "vehicleRespawn\_settings.hpp"
	#endif

	#ifdef use_vehicleRestrictions
	#include "vehicleRestrictions\_settings.hpp"
	#endif

	#ifdef use_vehicleSelection
	#include "vehicleSelection\_settings.hpp"
	#endif

	#ifdef use_vehicleWatch
	#include "vehicleWatch\_settings.hpp"
	#endif

	#ifdef use_viewDistance
	#include "viewDistance\_settings.hpp"
	#endif

	#ifdef use_voiceControl
	#include "voiceControl\_settings.hpp"
	#endif

	#ifdef use_vonHint
	#include "vonHint\_settings.hpp"
	#endif

	#ifdef use_weaponHolster
	#include "weaponHolster\_settings.hpp"
	#endif

	#ifdef use_zeusMission
	#include "zeusMission\_settings.hpp"
	#endif
};
