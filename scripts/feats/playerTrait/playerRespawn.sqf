/*
@filename: feats\playerTrait\playerRespawn.sqf
Author:
	Ben
Description:
	run on player,
	update player unit trait according to settings
*/

#include "_debug.hpp"

private _role = (player getVariable "role");

if !( isClass(missionConfigFile >> "settings" >> "playerTrait" >> _role) ) exitWith { nil };

private _traitBool = ["engineer", "explosiveSpecialist", "medic", "UAVHacker"]; 
private _traitScalar = ["audibleCoef", "camouflageCoef", "loadCoef"];

{
    private _val = (["playerTrait", _role, _x] call core_fnc_getSetting);
    if !( isNil "_val" ) then {
        #ifdef DEBUG
        private _debug = format["setting trait %1 to %2", _x, (_val == 1)];
        debug(LL_DEBUG, _debug);
        #endif 
    	player setUnitTrait [_x, (_val == 1)]; 
	};
} forEach _traitBool;

{
    private _val = (["playerTrait", _role, _x] call core_fnc_getSetting);
    if !( isNil "_val" ) then {
        #ifdef DEBUG
        private _debug = format["setting trait %1 to %2", _x, _val];
        debug(LL_DEBUG, _debug);
        #endif  
    	player setUnitTrait [_x, _val];
	};
} forEach _traitScalar;

if !( MOD_ace ) exitWith { nil };

private _val = (["playerTrait", _role, "medic"] call core_fnc_getSetting);
private _isMedic = 0;
if !( isNil "_val" ) then {
    if ( _val == 1 ) then { _isMedic = 1; };
};
player setVariable ["ace_medical_medicClass", _isMedic, true];

private _val = (["playerTrait", _role, "engineer"] call core_fnc_getSetting);
private _isEngineer = 0;
if !( isNil "_val" ) then {
	if ( _val == 1 ) then { _isEngineer = 1; };
};
player setVariable ["ace_isEngineer", _isEngineer, true];

nil