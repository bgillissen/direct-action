/*
audibleCoef (scalar)
camouflageCoef (scalar)
engineer (bool)
explosiveSpecialist (bool)
loadCoef (scalar)
medic (bool)
UAVHacker (bool)
*/

class playerTrait {
	class crew {
		engineer = 1;
	};
	class medic {
		medic = 1;
	};
	class engineer {
		engineer = 1;
		explosiveSpecialist = 1;
	};
	class uavop {
		UAVHacker = 1;
	};
};
