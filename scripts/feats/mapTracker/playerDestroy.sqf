/*
@filename: feats\mapTracker\playerDestroy.sqf
Credits: 
	Quiksilver
Author:
	Ben
Description:
	run on client
	kill the waiting threads and remove the registred eventHandlers
*/

if ( (["mapTracker"] call core_fnc_getParam) == 0 ) exitWith { nil };

terminate MT_mapThread;
MT_mapThread = scriptNull;
if ( MT_mapEH != -1 ) then {
	((findDisplay 12) displayCtrl 51) ctrlRemoveEventHandler ["Draw", MT_mapEH];
    MT_mapEH = -1;
};

terminate MT_gpsThread;
MT_gpsThread = scriptNull;
if ( MT_gpsEH != -1 ) then {
    private _display = uiNamespace getVariable ["RscCustomInfoMiniMap", displayNull];
    if !( isNull _display ) then {
		(_display displayctrl 101) ctrlRemoveEventHandler ["Draw", MT_gpsEH];
	};
	MT_gpsEH = -1;
};

nil