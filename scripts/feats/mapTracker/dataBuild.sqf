
private ["_sides", "_unit", "_isRemote"];

if ( isNull MT_remoteUnit ) then {
    _sides = missionNamespace getVariable "ALLIES";
    /*
    if ( captive player ) then {
        _sides = ALLIES;
	} else {
    	_sides = [(side player), [civilian]] call common_fnc_getFriends;
	};
	*/
	_unit = player;
    _isRemote = false;
} else {
	_sides = [(side MT_remoteUnit), [civilian]] call common_fnc_getFriends;
	_unit = MT_remoteUnit;
    _isRemote = true;
};

MT_data = [];

{
	private _veh = vehicle _x;
    private _do = !( _x getVariable["MT_hidden", false] );
    if ( _do ) then { _do = ( (side _x) in _sides ); };
    if ( !_do && !_isRemote ) then { _do = (_x getVariable ["agony", false]); };
    if ( !_do && !_isRemote ) then { _do = (_x getVariable ["ACE_isUnconscious", false]); };
    if ( _do && !(_x isEqualTo _veh) ) then { _do = ( (crew _veh select 0) isEqualTo _x ); };
	if ( _do ) then {
        private _text = [_veh, _x] call mapTracker_fnc_iconText;
		private _type = [_veh] call mapTracker_fnc_iconType;		
		private _size = [_veh] call mapTracker_fnc_iconSize;
        ([_x, _unit, _isRemote] call mapTracker_fnc_iconColor) params ["_mapColor", "_gpsColor"];
		private _pos = getPosASL _veh;		
		private _dir = getDir _veh;
		MT_data pushBack [_veh, _text, _type, _mapColor, _gpsColor, _size, _pos, _dir];
	};
    true
} count allUnits;