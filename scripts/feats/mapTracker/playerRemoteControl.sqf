/*
@filename: feats\mapTracker\remoteControl.sqf
Author:
	Ben
Description:
	run on player,
	define remoteUnit global var as needed 
*/

#include "_debug.hpp"

if ( (["mapTracker"] call core_fnc_getParam) == 0 ) exitWith {
    #ifdef DEBUG
    debug(LL_DEBUG, "disabled by mission parameter");
    #endif
    nil
};

params ["_gm", "_player", "_unit", "_ctrl"];

if ( _ctrl ) then {
	MT_remoteUnit = _unit;
    _unit  setVariable["MT_ctrlBy", _player, true];
    player setVariable ["MT_hidden", true, true];
} else {
    MT_remoteUnit = objNull;
    _unit setVariable["MT_ctrlBy", objNull, true];
};

nil