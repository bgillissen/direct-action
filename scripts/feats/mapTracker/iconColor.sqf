/*
@filename: feats\mapTracker\iconColor.sqf
Credits: 
	Quiksilver
Author:
	Ben
Description:
	run on client
	return the color of the icon to draw.
Params:
	OBJECT,	a unit
	BOOL, isGPS
Environment:
	object:
		agony
		ACE_isUnconscious
	missionConfig:
		mapTracker >> colorNeedRevive
		mapTracker >> colorEast
		mapTracker >> colorWest
		mapTracker >> colorInd
		mapTracker >> colorCiv
		mapTracker >> colorDft
Return:
	ARRAY of SCALAR, color RGB + Alpha
*/
params ["_unit", "_player", "_isRemote"];

if ( _unit getVariable ["agony", false] ) exitWith { 
	[MT_colorUnconcious, MT_colorUnconcious] 
};
if ( _unit getVariable ["ACE_isUnconscious", false] ) exitWith { 
	[MT_colorUnconcious, MT_colorUnconcious] 
};

private _colorFnc = {
	if ( _this isEqualTo east ) exitWith { MT_colorEast };
	if ( _this isEqualTo west ) exitWith { MT_colorWest };
	if ( _this isEqualTo independent ) exitWith { MT_colorInd };
	if ( _this isEqualTo civilian ) exitWith { MT_colorCiv };
	MT_colorDft
};

private _mapColor = (side _unit) call _colorFnc;
private _gpsColor = _mapColor;

private _a = MT_alphaOther;

if ( (group _unit) isEqualTo (group _player) ) then {
    _a  = MT_alphaGroup;
	if ( isPlayer _unit ) then {
        _gpsColor = missionNamespace getVariable [format["MT_color%1", (_unit call groups_fnc_getColor)], _gpsColor];
	};
} else {
	if ( (vehicle _unit) isEqualTo (vehicle _player) ) then { _a  = MT_alphaGroup; };
};

[(_mapColor + [_a]), (_gpsColor + [_a])]