class mapTracker {
	tag = "mapTracker";
	class functions {
		class dataBuild { file="feats\mapTracker\dataBuild.sqf"; };
		class dataUpdate { file="feats\mapTracker\dataUpdate.sqf"; };
		class iconColor { file="feats\mapTracker\iconColor.sqf"; };
		class iconType { file="feats\mapTracker\iconType.sqf"; };
		class iconSize { file="feats\mapTracker\iconSize.sqf"; };
		class iconText { file="feats\mapTracker\iconText.sqf"; };
		class drawIcons { file="feats\mapTracker\drawIcons.sqf"; };
		class playerDestroy { file="feats\mapTracker\playerDestroy.sqf"; };
		class playerInit { file="feats\mapTracker\playerInit.sqf"; };
		class playerRemoteControl { file="feats\mapTracker\playerRemoteControl.sqf"; };
		class playerZeusInterface { file="feats\mapTracker\playerZeusInterface.sqf"; };
		class swapState { file="feats\mapTracker\swapState.sqf"; };
	};
};
