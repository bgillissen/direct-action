/*
@filename: feats\mapTracker\drawIcons.sqf
Credits: 
	Quiksilver
Author:
	Ben
Description:
	run on client
	draw the icons on the given ui control
*/

if !( MT_draw ) exitWith {};

params ["_isGPS", "_control"];

if ( diag_tickTime >= MT_nextData ) then { 
	call mapTracker_fnc_dataBuild;
    MT_nextData = diag_tickTime + MT_dataDelay; 
} else {
    call mapTracker_fnc_dataUpdate;
};

private _playerVeh = (vehicle player);
{
    _x params ["_veh", "_text", "_type", "_mapColor", "_gpsColor", "_size", "_pos", "_dir"];
    if ( _isGPS ) then {
        if !( _veh isEqualTo _playerVeh ) then {
    		_control drawIcon [_type, _gpsColor, _pos, _size, _size, _dir, "", MT_shadow, MT_textSize, MT_textFont, MT_textOffset];
        };    
    } else {
        _control drawIcon [_type, _mapColor, _pos, _size, _size, _dir, _text, MT_shadow, MT_textSize, MT_textFont, MT_textOffset];
    };    
} foreach MT_data;