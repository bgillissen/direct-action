/*
@filename: feats\mapTracker\playerInit.sqf
Credits: 
	Quiksilver
Author:
	Ben
Description:
	run on client
	register map and gps draw eventHandler.
*/

#include "_debug.hpp"

MT_colorUnconcious = ["mapTracker", "colorNeedRevive"] call core_fnc_getSetting;
MT_colorEast = ["mapTracker", "colorEast"] call core_fnc_getSetting;
MT_colorWest = ["mapTracker", "colorWest"] call core_fnc_getSetting;
MT_colorInd = ["mapTracker", "colorInd"] call core_fnc_getSetting;
MT_colorCiv = ["mapTracker", "colorCiv"] call core_fnc_getSetting;
MT_colorDft = ["mapTracker", "colorDft"] call core_fnc_getSetting;
{
    private _color = getArray(_x >> 'config');
    _color deleteAt 3;
    missionNamespace setVariable [format["MT_color%1", (configName _x)], _color, false];
} forEach ("true" configClasses (missionConfigFile >> "settings" >> "groups" >> "colors"));

if ( (["mapTracker"] call core_fnc_getParam) == 0 ) exitWith {
    #ifdef DEBUG
    debug(LL_INFO, "disabled by mission parameter");
    #endif
    nil
};

MT_data = [];
MT_draw = true;
MT_nextData = 0;

MT_dataDelay = ["mapTracker", "dataDelay"] call core_fnc_getSetting;
MT_sizeMan = ["mapTracker", "sizeMan"] call core_fnc_getSetting;
MT_sizeLandVehicle = ["mapTracker", "sizeLandVehicle"] call core_fnc_getSetting;
MT_sizeAir = ["mapTracker", "sizeAir"] call core_fnc_getSetting;
MT_sizeShip = ["mapTracker", "sizeShip"] call core_fnc_getSetting;
MT_shadow = ["mapTracker", "shadow"] call core_fnc_getSetting;
MT_textSize = ["mapTracker", "textSize"] call core_fnc_getSetting;
MT_textFont = ["mapTracker", "textFont"] call core_fnc_getSetting;
MT_textOffset = ["mapTracker", "textOffset"] call core_fnc_getSetting;
MT_colorMAIN = ["mapTracker", "colorMAIN"] call core_fnc_getSetting;
MT_colorRED = ["mapTracker", "colorRED"] call core_fnc_getSetting;
MT_colorGREEN = ["mapTracker", "colorGREEN"] call core_fnc_getSetting;
MT_colorBLUE = ["mapTracker", "colorBLUE"] call core_fnc_getSetting;
MT_colorYELLOW = ["mapTracker", "colorYELLOW"] call core_fnc_getSetting;
MT_alphaOther = ["mapTracker", "alphaOther"] call core_fnc_getSetting;
MT_alphaGroup = ["mapTracker", "alphaGroup"] call core_fnc_getSetting;
MT_remoteUnit = objNull;

["mapTracker", "Toggle map tracker on / off", "mapTracker_fnc_swapState"] call keybind_fnc_add;

MT_mapEH = -1;
MT_mapThread = [] spawn {
	waitUntil {
		sleep 1; 
		!(isNull (findDisplay 12))
	};
	MT_mapEH = ((findDisplay 12) displayCtrl 51) ctrlAddEventHandler ["Draw", {[false, (_this select 0)] call mapTracker_fnc_drawIcons}];
};

if ( (["mapTracker"] call core_fnc_getParam) < 2 ) exitWith {
    #ifdef DEBUG
    debug(LL_INFO, "GPS is disabled by mission parameter");
    #endif
};

MT_gpsDist = ["mapTracker", "gpsDist"] call core_fnc_getSetting;
MT_gpsEH = -1;
MT_gpsThread = [] spawn {
	disableSerialization;
	private _gps = controlNull;
    private _display = displayNull;
	while { isNull _gps } do {
	    _display = uiNamespace getVariable ["RscCustomInfoMiniMap", displayNull];
	    if !( isNull _display ) then { _gps = _display displayctrl 101; };
		sleep 2;
	};
    uiNamespace setVariable ["MT_gpsCtrl",(_display displayctrl 15112)];
	MT_gpsEH = _gps ctrlAddEventHandler ["Draw", {[true, (_this select 0)] call mapTracker_fnc_drawIcons}];
};

nil