/*
@filename: feats\mapTracker\playerZeusInterface.sqf
Author:
	Ben
Description:
	run on player.
	add the keybinding to allow player to keep his tfar pos on his body while zeusing  
*/


#include "_debug.hpp"

if ( (["mapTracker"] call core_fnc_getParam) == 0 ) exitWith {
    #ifdef DEBUG
    debug(LL_DEBUG, "disabled by mission parameter");
    #endif
    nil
};

params ["_inZeus"];

if ( _inZeus ) then {
    if ( isNil "stayInBody" ) then { stayInBody = true; };
    player setVariable ["MT_hidden", !stayInBody, true];
} else {
   	player setVariable ["MT_hidden", !(isNull MT_remoteUnit), true];
};

nil