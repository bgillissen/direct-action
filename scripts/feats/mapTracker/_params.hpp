class mapTracker {
	title = "Display other players on map and/or GPS";
	values[] = {0,1,2};
	texts[] = {"No", "Map only", "Map and GPS"};
	default = 2;
};
