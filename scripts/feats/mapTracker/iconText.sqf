/*
@filename: feats\mapTracker\iconColor.sqf
Credits: 
	Quiksilver
Author:
	Ben
Description:
	run on client
	return the text next to the icon to draw.
*/

params ["_veh", "_unit"];

private _vn = _veh getVariable ["vn", ""];
if ( _vn isEqualTo "" ) then {
	private _role = _veh getVariable ["role", ""];
	if ( (_veh isEqualTo (vehicle _veh)) && !(_role isEqualTo "") ) then {
		_vn = ["mapTracker", "roleDescriptions", _role] call core_fnc_getSetting;
	} else {
		_vn = getText (configFile >> "CfgVehicles" >> typeOf _veh >> "displayName");
	};
	_veh setVariable ["vn", _vn];
};

private _t = "";

private _vehText = {
  	params ["_veh", "_vdn", "_vn"];
    private _t = "";
    private _n = ((count crew _veh) - 1);
	if (_n > 0) then {
		if !( isNull (driver _veh) ) then {
			_t = format ["%1 [%2] +%3", _vdn, _vn, _n];
		} else {
			_t = format ["[%1] %2 +%3", _vdn, _vn, _n];
		};
	} else {
		if !( isNull (driver _veh) ) then {
			_t = format ["%1 [%2]", _vdn, _vn];
		} else {
			_t = format ["[%1] %2", _vn, _vdn];
		};
	};
    _t
};



if !( isPlayer _x ) then {
    private _ctrlBy = _x getVariable ["MT_ctrlBy", objNull];
    if !( isNull _ctrlBy ) then {
		private _vdn = _ctrlBy getVariable ["MD_name", (name _ctrlBy)];
        _t = [_veh, _vdn, _vn] call _vehText;
		//_t = format ["%1 [%2]", _vdn, _vn];
	} else {
		if ( _veh in allUnitsUAV ) then {
			if (isUavConnected _veh) then {
                private _uavCtrlBy = ((UAVControl _veh) select 0);
                private _vdn = _uavCtrlBy getVariable ["MD_name", (name _uavCtrlBy)];
				_t = format ["%1 [%2]", _vdn, _vn];
			} else {
				_t = format ["[AI] [%1]", _vn];
			};
		};
	};
} else {
	private _vdn = ((crew _veh) select 0) getVariable ["MD_name", (name ((crew _veh) select 0))];
    _t = [_veh, _vdn, _vn] call _vehText;
};

_t