/*
@filename: feats\mapTracker\swapState.sqf
Author:
	Ben
Description:
	run on player side by a display event handler
	swap the state of the mapTracker
*/

params ["_control", "_keyCode", "_shift", "_ctrl", "_alt"];

MT_draw = !MT_draw;

if ( MT_draw ) then {
	systemChat (["mapTracker", "onMsg"] call core_fnc_getSetting);
} else {
    systemChat (["mapTracker", "offMsg"] call core_fnc_getSetting);
};

true