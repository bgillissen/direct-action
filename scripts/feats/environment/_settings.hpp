class environment {
	class setTime {
		rank = 4;
		presets[] = {"real", "sunrise", "noon", "sunset", "midnight"};
		class actions {
			real = "Set time to GMT+1";
			sunrise = "Set time to sunrise";
			noon = "Set time to noon";
			sunset = "Set time to sunset";
			midnight = "Set time to midnight";
		};
		class messages {
			real = "Time has been set to GMT+1 by %1";
			sunrise = "Time has been set to sunrise by %1";
			noon = "Time has been set to noon by %1";
			sunset = "Time has been set to sunset by %1";
			midnight = "Time has been set to midnight by %1";
		};
	};
	class clearFog {
		rank = 1;
		action = "Clear fog";
		message = "Fog has been cleared by %1";
	};
	class clearRain {
		rank = 2;
		action = "Stop rain";
		message = "Rain has been stopped by %1";
	};
};
