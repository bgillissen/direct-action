
if ( CTXT_SERVER ) exitWith { true };

if ( (call BIS_fnc_admin) > 0 ) exitWith { true };

private _rank = getNumber(missionConfigFile >> "settings" >> "environment" >> _this >> "rank");

( ((player getVariable["MD_rank", 0]) >= _rank) && !zeusMission )