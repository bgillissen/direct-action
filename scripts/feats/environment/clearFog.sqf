
if ( !isServer ) exitWith {
	_this remoteExec ["environment_fnc_clearFog", 2];
};

params ["_thing", "_caller", "_id", "_arg"];

private _msg = (["environment", "clearFog", "message"] call core_fnc_getSetting);
[1, format[_msg, (name _caller)], ["HQ", PLAYER_SIDE]] call global_fnc_chat;

0 setFog 0;

forceWeatherChange;