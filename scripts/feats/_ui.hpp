/*
@filename: feats\_ui.hpp
Author:
	Ben
Description:
	on all context,
	included by feats\main.hpp,
	include features ui classes
*/

#ifdef use_artiComputer
#include "artiComputer\_ui.hpp"
#endif

#ifdef use_artiSupport
#include "artiSupport\_ui.hpp"
#endif

#include "groups\_ui.hpp"
#include "playerSpawn\_ui.hpp"

#ifdef use_radioFreq
#include "radioFreq\_ui.hpp"
#endif

#ifdef use_revive
#include "revive\_ui.hpp"
#endif

#ifdef use_serverRules
#include "serverRules\_ui.hpp"
#endif

#ifdef use_vehicleDeco
#include "vehicleDeco\_ui.hpp"
#endif

#ifdef use_vehicleLoadout
#include "vehicleLoadout\_ui.hpp"
#endif

#ifdef use_vehicleSelection
#include "vehicleSelection\_ui.hpp"
#endif

#ifdef use_viewDistance
#include "viewDistance\_ui.hpp"
#endif

#ifdef use_zeusCompo
#include "zeusCompo\_ui.hpp"
#endif
