/*
@filename: feats\backpackOnChest\playerRespawn.sqf
Author:
	Ben
Description:
	run on player,
*/

#include "_debug.hpp"

if ( (["backpackOnChest"] call core_fnc_getParam) isEqualTo 0 ) exitWith {
	#ifdef DEBUG
   	debug(LL_INFO, "disabled by mission parameter");
    #endif
    nil  
};

boc_actionOnGround = (player addAction ['Drop chest backpack', {_this call boc_fnc_onGround}, [], 0, false, true, "", "'onGround' call boc_fnc_condition", 4]);

boc_actionOnChest = (player addAction ['Backpack on chest',{_this call boc_fnc_onChest}, [], 0, false, true, "", "'onChest' call boc_fnc_condition", 4]);

boc_actionOnBack = (player addAction ['Backpack on back', {_this call boc_fnc_onBack}, [], 0, false, true, "", "'onBack' call boc_fnc_condition", 4]);

nil