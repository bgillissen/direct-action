/*
@filename: feats\backpackOnChest\condition.sqf
Author:
	Ben
Description:
	run on player,
*/

#include "_debug.hpp"

if !( alive player ) exitWith { false };

if !( (vehicle player) isEqualTo player ) exitWith { false };

if !( (player getVariable ["action", []]) isEqualTo [] ) exitWith { false };

if ( _this isEqualTo "onChest" ) exitWith {
    if ( isNull BOC_fakeBag ) exitWith { !( (backpack player) isEqualTo '' ) };
    ( !BOC_onChest && ((player distance BOC_fakeBag) < 3) )
};

if ( _this isEqualTo "onBack" ) exitWith {
    if !( (backpack player) isEqualTo '' ) exitWith { false };
    if !( BOC_onChest ) exitWith { ( (player distance BOC_fakeBag) < 3 ) };
	true
};

if ( _this isEqualTo "onGround" ) exitWith {
	( !(isNull BOC_fakeBag) && BOC_onChest )
};