/*
@filename: feats\backpackOnChest\playerPreInit.sqf
Author:
	Ben
Description:
	run on player,
*/

#include "_debug.hpp"

if ( (["backpackOnChest"] call core_fnc_getParam) isEqualTo 0 ) exitWith {
	#ifdef DEBUG
   	debug(LL_DEBUG, "disabled by mission parameter");
    #endif  
    nil
};

BOC_fakeBag = objNull;
BOC_onChest = false;
BOC_animEH = -1;

nil