/*
@filename: feats\backpackOnChest\playerDestroy.sqf
Author:
	Ben
Description:
	run on player,
 	remove boc actions
*/

#include "_debug.hpp"

if ( (["backpackOnChest"] call core_fnc_getParam) isEqualTo 0 ) exitWith {
	#ifdef DEBUG
   	debug(LL_DEBUG, "disabled by mission parameter");
    #endif
    nil  
};

player removeAction boc_actionOnGround;
player removeAction boc_actionOnChest;
player removeAction boc_actionOnBack;

nil