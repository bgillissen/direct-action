/*
@filename: feats\backpackOnChest\onGround.sqf
Author:
	Ben
Description:
	run on player,
*/

#include "_debug.hpp"

if ( isNull BOC_fakeBag ) exitWith {};

BOC_fakeBag setVariable ["NOCLEANUP", false, true];

player removeEventHandler ['animChanged', BOC_animEH];
BOC_animEH = -1;

[player, "AinvPercMstpSrasWrflDnon_Putdown_AmovPercMstpSrasWrflDnon", 2] call global_fnc_doAnim;

detach BOC_fakeBag;

([player, 1, (getDir player)] call BIS_fnc_relPos) params ["_x", "_y"];
BOC_fakeBag setPosATL [_x, _y, -0.2];
BOC_fakeBag setVectorDirAndUp [[0,-0.1,-1],[0,0,1]];
BOC_onChest = false;

player forceWalk false;