class backpackOnChest {
	tag = "backpackOnChest";
	class functions {
		class playerPreInit { file="feats\backpackOnChest\playerPreInit.sqf"; };
		class playerDestroy { file="feats\backpackOnChest\playerDestroy.sqf"; };
		class playerGetIn { file="feats\backpackOnChest\playerGetIn.sqf"; };
		class playerGetOut { file="feats\backpackOnChest\playerGetOut.sqf"; };
		class playerInventory { file="feats\backpackOnChest\playerInventory.sqf"; };
		class playerTake { file="feats\backpackOnChest\playerTake.sqf"; };
		class playerKilled { file="feats\backpackOnChest\playerKilled.sqf"; };
		class playerRespawn { file="feats\backpackOnChest\playerRespawn.sqf"; };
		class serverInit { file="feats\backpackOnChest\serverInit.sqf"; };
		class serverLeave { file="feats\backpackOnChest\serverLeave.sqf"; };
		class serverDestroy { file="feats\backpackOnChest\serverDestroy.sqf"; };
	};
};
class boc {
	tag = "boc";
	class functions {
		class animChange { file="feats\backpackOnChest\animChange.sqf"; };
		class playerCleanup { file="feats\backpackOnChest\playerCleanup.sqf"; };
		class serverCleanup { file="feats\backpackOnChest\serverCleanup.sqf"; };
		class onBack { file="feats\backpackOnChest\onBack.sqf"; };
		class onChest { file="feats\backpackOnChest\onChest.sqf"; };
		class onGround { file="feats\backpackOnChest\onGround.sqf"; };
		class condition { file="feats\backpackOnChest\condition.sqf"; };
	};
};
