/*
@filename: feats\backpackOnChest\serverDestroy.sqf
Author:
	Ben
Description:
	run on server,
 	remove all Bakcpacks container
*/

#include "_debug.hpp"

if ( (["backpackOnChest"] call core_fnc_getParam) isEqualTo 0 ) exitWith {
	#ifdef DEBUG
   	debug(LL_DEBUG, "disabled by mission parameter");
    #endif
    nil  
};

{
	_x params ["_puid", "_pFakeBag"];
    _pFakeBag removeEventHandler ['Deleted', (_pFakeBag getVariable['BOC_eh', -1])]; 
	deleteVehicle _pFakeBag;
	BOC_stack deleteAt _forEachIndex;
} forEach BOC_stack;

publicVariable "BOC_stack";

["BOC_add", BOC_addPVEH] call pveh_fnc_del;
["BOC_del", BOC_delPVEH] call pveh_fnc_del;

nil