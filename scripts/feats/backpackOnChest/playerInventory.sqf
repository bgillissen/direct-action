/*
@filename: feats\backpackOnChest\playerInventory.sqf
Author:
	Ben
Description:
	run on player,
*/

#include "_debug.hpp"

if ( (["backpackOnChest"] call core_fnc_getParam) isEqualTo 0 ) exitWith {
	#ifdef DEBUG
   	debug(LL_INFO, "disabled by mission parameter");
    #endif
    false  
};

params ["_unit", "_primCont", "_secCont"];

private _handled = false;
{
    _x params ["_puid", "_fakeBag"];
    if ( !(isNull _fakeBag) && (((_secCont isEqualTo _fakeBag) || (_primcont isEqualTo _fakeBag)) && !BOC_onChest) ) exitWith {
        systemChat "That inventory is locked!"; 
    	_handled = true; 
	};
} foreach BOC_stack;

_handled