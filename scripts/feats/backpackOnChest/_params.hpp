/*
@filename: feats\backpackOnChest\_params.hpp
Author:
	Ben
Description:
		included by feats\_params.hpp
*/

class backpackOnChest {
	title = "Allow players to have 2 backpacks (chest + back)";
	values[] = {0, 1};
	texts[] = {"No", "Yes"};
	default = 1;
};
