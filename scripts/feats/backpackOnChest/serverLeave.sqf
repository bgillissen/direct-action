/*
@filename: feats\backpackOnChest\serverLeave.sqf
Author:
	Ben
Description:
	run on server,
*/

#include "_debug.hpp"

if ( (["backpackOnChest"] call core_fnc_getParam) isEqualTo 0 ) exitWith {
	#ifdef DEBUG
   	debug(LL_DEBUG, "disabled by mission parameter");
    #endif  
    nil
};

params["_unit", "_id", "_uid", "_name"];

[_uid] call boc_fnc_serverCleanup;

nil