/*
@filename: feats\backpackOnChest\onGround.sqf
Author:
	Ben
Description:
	run on player,
*/

#include "_debug.hpp"

if ( isNull BOC_fakeBag ) exitWith {};

if !( (backpack player) isEqualto "" ) exitWith {};

if ( BOC_onChest ) then { 
    player removeEventHandler ['animChanged', BOC_animEH];
	BOC_animEH = -1;
};

player action ["AddBag", BOC_fakeBag, ((backpackCargo BOC_fakeBag) select 0)];

waitUntil { !( (backpack player) isEqualTo "" ) };

[BOC_fakeBag] call boc_fnc_playerCleanup;

player forceWalk false;
