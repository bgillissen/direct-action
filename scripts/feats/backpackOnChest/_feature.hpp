class backpackOnChest : feat_base  {
	class player : featPlayer_base {
		class preInit : featEvent_enable {};
		class getIn : featEvent_enable {};
		class getOut : featEvent_enable {};
		class killed : featEvent_enable {};
		class respawn : featEvent_enable {};
		class inventory : featEvent_enable {};
		class take : featEvent_enable { thread=1; };
		class destroy : featEvent_enable {};
	};
	class server : featServer_base {
		class init : featEvent_enable {};
		class leave : featEvent_enable {};
		class destroy : featEvent_enable {};
	};
};
