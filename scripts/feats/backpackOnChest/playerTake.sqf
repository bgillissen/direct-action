/*
@filename: feats\backpackOnChest\playerTake.sqf
Author:
	Ben
Description:
	run on player,
	if taken item is a TFAR radio, set it up following the settings 
*/

#include "_debug.hpp"

if ( (["backpackOnChest"] call core_fnc_getParam) isEqualTo 0 ) exitWith {
	#ifdef DEBUG
   	debug(LL_INFO, "disabled by mission parameter");
    #endif
    nil  
};

params ["_unit", "_cont", "_item"];

{
    _x params ["_puid", "_fakeBag"];
    if ( !(isNull _fakeBag) && (_cont isEqualTo _fakeBag) ) exitWith { 
    	if ( (getPlayerUid player) isEqualTo _puid ) then {
            if ( count (backpackCargo _cont) isEqualTo 0 ) then { 
                [_fakeBag] call boc_fnc_playerCleanup; 
			};
        } else {
            systemChat "This backpack does not belongs to you !";
			player action ["DropBag", _cont, _item];  
        };
    };
} foreach BOC_stack;

nil