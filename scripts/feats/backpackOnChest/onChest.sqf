/*
@filename: feats\backpackOnChest\onChest.sqf
Author:
	Ben
Description:
	run on player,
*/

#include "_debug.hpp"

private _attachC = [0.1, 0.1, -0.12];
private _vector = [[0,-0.2,-1],[0.2,0.8,0]]; 
private _bag = (backpack player);

if !( _bag isEqualTo "" ) then {
	if ( isNull BOC_fakeBag ) then {   
		BOC_fakeBag = "Weapon_Empty" createVehicle [0,0,0];
        _eh = (BOC_fakeBag addEventHandler ["Deleted", {_this call boc_fnc_playerCleanup}]);
        BOC_fakeBag setVariable ["BOC_eh", _eh];
        BOC_fakeBag setVariable ["BOC_owner", (getPlayerUID player), true];
		player action ["DropBag", BOC_fakeBag, _bag];
        
        waitUntil { ( (backpack player) isEqualTo "" ) };
        
        BOC_add = [(getPlayerUID player), BOC_fakeBag];
        publicVariableServer "BOC_add";
        
        BOC_onChest = true;
	};
} else {
   BOC_onChest = !( isNull BOC_fakeBag );   
};

if ( BOC_onChest ) then {
    BOC_fakeBag setVariable ["NOCLEANUP", true, true];
	player forceWalk true;
    [player, (animationState player)] call boc_fnc_animChange;
    if ( BOC_animEH < 0 ) then {
		BOC_animEH = (player addEventHandler ["AnimChanged", {_this call boc_fnc_animChange}]);
	};
};