
params ["_fakeBag", ["_uid", ""]];

if ( BOC_fakeBag isEqualTo _fakeBag ) then {
	BOC_fakeBag = objNull;
	BOC_onChest = false;
	if ( BOC_animEH >= 0 ) then { 
		player removeEventHandler ['animChanged', BOC_animEH];
    	BOC_animEH = -1;
	};
    _fakeBag removeEventHandler ['Deleted', (_fakeBag getVariable['BOC_eh', -1])];
    deleteVehicle _fakeBag;
    BOC_del = [getPlayerUID player];
	publicVariableServer "BOC_del";
    player forceWalk false;
};