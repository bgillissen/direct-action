/*
@filename: feats\backpackOnChest\playerKilled.sqf
Author:
	Ben
Description:
	run on player,
*/

#include "_debug.hpp"

if ( (["backpackOnChest"] call core_fnc_getParam) isEqualTo 0 ) exitWith {
	#ifdef DEBUG
   	debug(LL_DEBUG, "disabled by mission parameter");
    #endif
    nil  
};

if !( isNull BOC_fakeBag ) then {
	[BOC_fakeBag] call boc_fnc_playerCleanup;
};

nil