/*
@filename: feats\backpackOnChest\playerGetout.sqf
Author:
	Ben
Description:
	run on player,
	called when player leave a vehicle.
*/

#include "_debug.hpp"

if ( (["backpackOnChest"] call core_fnc_getParam) isEqualTo 0 ) exitWith {
	#ifdef DEBUG
   	debug(LL_DEBUG, "disabled by mission parameter");
    #endif  
};

params ["_unit", "_pos", "_veh", "_turret"];

if ( (isNull BOC_fakeBag) || !BOC_onChest ) exitWith {};

[player, (animationState player)] call boc_fnc_animChange;

[BOC_fakeBag, false] call global_fnc_hideObject;

if ( BOC_animEH < 0 ) then {
	BOC_animEH = (player addEventHandler ["AnimChanged", {_this call boc_fnc_animChange}]);
};