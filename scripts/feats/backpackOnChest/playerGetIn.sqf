/*
@filename: feats\backpackOnChest\playerGetIn.sqf
Author:
	Ben
Description:
	run on player,
	called when player enter a vehicle.
*/

#include "_debug.hpp"

if ( (["backpackOnChest"] call core_fnc_getParam) isEqualTo 0 ) exitWith {
	#ifdef DEBUG
   	debug(LL_DEBUG, "disabled by mission parameter");
    #endif
    nil  
};

params ["_unit", "_role", "_veh", ["_turretPath", []]];

if ( (isNull BOC_fakeBag) || !BOC_onChest ) exitWith {};

if (_veh isKindOf "ParachuteBase" ) exitWith {};

[BOC_fakeBag, true] call global_fnc_hideObject;

player removeEventHandler ['animChanged', BOC_animEH];
BOC_animEH = -1;

nil