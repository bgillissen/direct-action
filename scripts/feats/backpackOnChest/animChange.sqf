/*
@filename: feats\backpackOnChest\animChange.sqf
Author:
	Ben
Description:
	run on player,
*/

#include "_debug.hpp"

params["_unit", "_anim"];

private _attachT = objNull;
private _attachC = [];
private _attachP = nil;
private _vector = [];

switch (_anim) do {
    // chute pilot
    case "para_pilot" : {_attachT = (vehicle player);
        				 _attachP = "";
                         _attachC = [0.05, 0, 0];
                         _vector = [[0, -0.2, -1],[0, 1, 0]];
    				    };
    // on the gnees
	case "amovpknlmstpsraswrfldnon";
	case "amovpknlmstpslowwrfldnon";
    case "amovpknlmstpsraswpstdnon";
    case "amovpknlmstpslowwpstdnon";
    case "amovpknlmstpsraswlnrdnon";    
	// vertical in water
    case "asswpercmstpsnonwnondnon";
	case "asdvpercmstpsnonwrfldnon";
	case "asdvpercmstpsnonwnondnon";
	// vertical on the ground
	case "amovpercmstpsnonwnondnon";
	case "amovpercmrunslowwrfldf";
    case "amovpercmstpsraswpstdnon";
    case "amovpercmstpslowwpstdnon";
	case "amovpercmstpslowwrfldnon";
	case "amovpercmstpsraswrfldnon";
	case "advepercmstpsnonwnondnon";
	case "advepercmstpsnonwrfldnon";
    case "amovpercmstpsraswlnrdnon";
	case "aswmpercmstpsnonwnondnon" : {_attachT = player;
    								   _attachP = "pelvis";
    								   _attachC = [0.1, 0.1, -0.12];
                                       _vector = [[0, -0.2, -1],[0.2, 0.8, 0]];
                                      };
	// Free Fall
	case "halofreefall_non";
    // diving (on the belly)
    case "asswpercmstpsnonwnondnon";
 	case "asswpercmwlksnonwnondf";
	case "aswmpercmrunsnonwnondf";
	case "asswpercmstpsnonwnondnon";
	case "asswpercmwlksnonwnondf";
	case "asswpercmstpsnonwnondnon";
    // prone (on the belly)
    case "amovppnemstpsraswpstdnon";
	case "amovppnemstpsraswrfldnon";
	case "amovppnemsprslowwrfldf" : {_attachT = player;
    								 _attachP = "pelvis";
                                     _attachC = [0, 0, -0.15]; 
									 _vector = [[0, -1, -0.15],[0, 0, -1]];
    								};
	// unconscious
	case "unconsciousrevivedefault";    
	// diving on the back
	case "abswpercmwlksnonwnondf";    
    case "abswpercmrunsnonwnondb";
	case "abswpercmstpsnonwnondnon";
	case "abswpercmwlksnonwnondb";
	case "aswmpercmrunsnonwnondb";
	case "aswmpercmstpsnonwnondnon";
	case "abdvpercmwlksnonwrfldf";
	case "asdvpercmwlksnonwrfldf";
	case "abdvpercmstpsnonwrfldnon";
	case "advepercmwlksnonwnondf";
	case "advepercmwlksnonwrfldf";
	case "aswmpercmwlksnonwnondf";
	case "abdvpercmwlksnonwnondb";
	case "abdvpercmwlksnonwrfldb";
	case "advepercmwlksnonwrfldb";
	case "aswmpercmwlksnonwnondb";
	case "asdvpercmwlksnonwrfldb" : {_attachT = player;
                                     _attachP = "pelvis";                                    
    								 _attachC = [0, 0.15, 0.1];
                                     _vector = [[0, 0.75, -0.25],[0, 0.25, 0.75]];
                                    };
    default {};
};

if ( isNull _attachT ) exitWith {};

BOC_fakeBag attachTo [_attachT, _attachC, _attachP]; 
BOC_fakeBag setVectorDirAndUp _vector;