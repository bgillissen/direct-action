
params ["_uid", ["_fakeBag", objNull]];

private _exp = false;
{
	_x params ["_puid", "_pFakeBag"];
    private _do = false;
	if !( isNull _fakeBag ) then { _do = (_fakeBag isEqualTo _pFakeBag); };
    if !( _do ) then { _do = (isNull _pFakeBag); };
    if !( _do ) then { _do = (_uid isEqualTo _puid); };
	if (_do ) then {
        deleteVehicle _pFakeBag;
		BOC_stack deleteAt _forEachIndex;
		_exp = true;    	
	};
} forEach BOC_stack;

if ( _exp ) then { publicVariable "BOC_stack"; };