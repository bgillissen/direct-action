/*
@filename: feats\backpackOnChest\serverInit.sqf
Author:
	Ben
Description:
	run on player,
	called when player enter a vehicle.
*/

#include "_debug.hpp"

if ( (["backpackOnChest"] call core_fnc_getParam) isEqualTo 0 ) exitWith {
	#ifdef DEBUG
   	debug(LL_DEBUG, "disabled by mission parameter");
    #endif
    nil  
};

missionNamespace setVariable ['BOC_stack', [], true];

private _addPVEH = {
    (_this select 1) params ['_uid', '_fakeBag'];
    private _idx = -1;
    {
		_x params ['_puid', '_pFakeBag'];
		if ( _puid isEqualTo _uid ) exitWith { _idx = _forEachIndex; };		    
	} forEach BOC_stack;
    if ( _idx < 0 ) then {
        BOC_stack pushback [_uid, _fakeBag];
    } else {
        BOC_stack set [_idx, [_uid, _fakeBag]];
    };
    publicVariable "BOC_stack";
};

private _delPVEH = {
    (_this select 1) params ['_uid', ['_fakeBag', objNull]];
    private _idx = -1;
    {
		_x params ['_puid', '_pFakeBag'];
		if ( (_puid isEqualTo _uid) || (_pFakebag isEqualTo _fakeBag) ) exitWith { _idx = _forEachIndex; };		    
	} forEach BOC_stack;
    if ( _idx >= 0 ) then { (BOC_stack select _idx) call boc_fnc_serverCleanup; };
};

BOC_addPVEH = ["BOC_add", _addPVEH] call pveh_fnc_add;
BOC_delPVEH = ["BOC_del", _delPVEH] call pveh_fnc_add;

nil