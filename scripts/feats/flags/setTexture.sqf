/*
@filename: feats\flags\setTexture.sqf
Author:
	Ben
Description:
	run on server.
	set flag texture using texture from the direct action addon
*/

#include "_debug.hpp"

params ["_obj", "_type", "_texture"];

private _path = "";
{
	if ( _texture in getArray(_x >> "flags" >> _type >> "entries") ) then {
		_path = getText(_x >> "flags" >> _type >> "path");
	};
} forEach ("true" configClasses (configFile >> "cfgDirectAction"));

if !( _path isEqualTo "" ) exitWith {
    #ifdef DEBUG
	private _debug = format["setting texture %1 to %2", _texture, _obj];
	debug(LL_DEBUG, _debug);
	#endif
    _obj setFlagTexture format[_path, _texture];
};

#ifdef DEBUG
private _debug = format["texture '%1', was not found in any cfgDirectAction class under (flags >> %2)", _texture, _type];
debug(LL_ERR, _debug);
#endif