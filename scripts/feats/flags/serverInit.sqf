/*
@filename: feats\flags\serverInit.sqf
Author:
	Ben
Description:
	run on server,
	define a stack with all the country flags markers
	create the countryFlags 
	register a public variable handler to recreate the country flags when memberData is updated
*/

{
	_x params ["_thing", "_actions"];
	{
		if ( (configName _x) isEqualTo "flag" ) then {
			[_thing, "basic", getText(_x >> "texture")] call flags_fnc_setTexture;
		};
	} forEach _actions;
} forEach BA_obj;

countryFlagMarkers = [];
countryFlags = [];
publicVariable "countryFlags";

for "_i" from 1 to 99 do {
	private _marker = format["BA_cFlag_%1", _i];
	if !( (getMarkerPos _marker) isEqualTo [0,0,0] ) then {
		countryFlagMarkers pushback _marker;
	};
};

call flags_fnc_countryFlags;

if ( isNil "flagPVEH" ) then {
    flagPVEH = ["memberData", {[] spawn flags_fnc_countryFlags;}] call pveh_fnc_add; 
};

nil