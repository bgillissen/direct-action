class flags {
	tag = "flags";
	class functions {
		class serverDestroy { file="feats\flags\serverDestroy.sqf"; };
		class serverInit { file="feats\flags\serverInit.sqf"; };
		class countryFlags { file="feats\flags\countryFlags.sqf"; };
		class setTexture { file="feats\flags\setTexture.sqf"; };
	};
};
