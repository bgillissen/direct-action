/*
@filename: feats\flags\serverDestroy.sqf
Author:
	Ben
Description:
	run on server,
	remove created flags
*/

[countryFlags] call common_fnc_deleteObjects;
countryFlags = [];

nil