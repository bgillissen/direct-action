/*
@filename: feats\flags\countryFlags.sqf
Author:
	Ben
Description:
	run on server,
	create a country Flags foreach memberData entry
*/

#include "_debug.hpp"

if ( (count countryFlags) > 0 ) then {
	[countryFlags] call common_fnc_deleteObjects;
	countryFlags = [];
};

if ( isNil "countryFlagMarkers" ) exitWith {
    #ifdef DEBUG
    debug(LL_ERR, "countryFlagMarkers is nil, abording flag creation");
    #endif
};
if ( (count countryFlagMarkers) == 0 ) exitWith {
    #ifdef DEBUG
    debug(LL_ERR, "countryFlagMarkers is empty, abording flag creation");
    #endif
};

private _countries = [];

{
	if ( (_x select MD_rank) > 2 ) then { _countries pushbackUnique (_x select MD_country); };
} forEach memberData;

if ( (count _countries) == 0 ) exitWith {
    #ifdef DEBUG
    debug(LL_WARN, "no countries found in memberData, abording flag creation");
    #endif
    publicVariable "countryFlags";
};

{
	if ( _forEachIndex >= (count countryFlagMarkers) ) exitWith {
        #ifdef DEBUG
		private _debug = format["No enough countryFlagMarkers for the amount of countries, got: %1, need: %2", count countryFlagMarkers, count _countries];
        debug(LL_WARN, _debug);
		#endif        
	};
	private _flag = createVehicle ["FlagPole_F", (getMarkerPos (countryFlagMarkers select _forEachIndex)), [], 0, "CAN_COLLIDE"];
    [_flag, "countries", toLower(_x)] call flags_fnc_setTexture;
    _flag setVariable["country", toLower(_x), true];
	_flag allowDamage false;
	countryFlags pushback _flag;
} forEach _countries;

#ifdef DEBUG
private _debug = format["%1 country flags created", (count countryFlags)];
debug(LL_INFO, _debug);
#endif

publicVariable "countryFlags";