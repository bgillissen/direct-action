/*
@filename: feats\vehicleWatch\monitor.sqf
Author:
	ben
Description:
	run on server,
	add the given vehicle to the vehicle monitored by vehicleWatch thread
*/

#include "_debug.hpp"

if ( _this isEqualType [] ) exitWith {
    { _x call vehicleWatch_fnc_monitor; } forEach _this;
};

VW pushbackUnique _this;

#ifdef DEBUG	
private _debug = format["vehicle '%1' has been added to the watch list", _this];
debug(LL_DEBUG, _debug);
#endif
