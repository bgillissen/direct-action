class vehicleWatch {
	tag = "vehicleWatch";
	class functions {
		class monitor { file="feats\vehicleWatch\monitor.sqf"; };
		class serverDestroy { file="feats\vehicleWatch\serverDestroy.sqf"; };
		class serverPreInit { file="feats\vehicleWatch\serverPreInit.sqf"; };
		class serverInit { file="feats\vehicleWatch\serverInit.sqf"; };
	};
};
