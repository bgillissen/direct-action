/*
@filename: feats\vehicleWatch\serverInit.sqf
Author:
	ben
Description:
	run on server,
	loop through all the vehicles we got to monitor and remove them when needed
*/

#include "_debug.hpp"

private _delay = ["vehicleWatch", "checkDelay"] call core_fnc_getSetting;
private _respawnDist = ["VehicleRespawn_distance"] call core_fnc_getParam;
private _reckDist = ["ia", "deleteDistance"] call core_fnc_getSetting;
private _repairSize = ["vehicleRepair", "triggerSize"] call core_fnc_getSetting;

private _lootSafeZones = [];
private _cleanForcedZones = [];
{
	_x params ["_area", "_actions"];
    if ( "cleanForced" in _actions ) then { _cleanForcedZones pushback _area; };
    if ( "LootSafe" in _actions ) then { _lootSafeZones pushback _area; };
} forEach BA_zones;

#ifdef DEBUG	
private _debug = format["thread is running with %1s delay", _delay];
debug(LL_DEBUG, _debug);
#endif	

while { true } do {	
	sleep _delay;
    private _removed = 0;
	{	
        private _veh = _x;
        private _remove = (isNull _veh);
        if  !( _remove ) then {
        	if ( alive _veh ) then {
               	//alive, we remove if (nobody in it) and (nobody near it) and (not in lootSafezone) and (not on a repair pad)
   	        	_remove = ( ({ isPlayer _x } count (crew _veh)) == 0 );
				if ( _remove ) then { _remove = ( ({(_x distance _veh) <= _respawnDist } count allPlayers) == 0 ); };
       	    	if ( _remove ) then { _remove = ( ({( _veh inArea _x )} count _lootSafeZones) == 0 ); };
				if ( _remove ) then { _remove = ( ({((getMarkerPos _x) distance _veh) <= _repairSize } count VR_markers) == 0 ); }; 
			} else {
				//dead, we remove if ((nobody near it) or (in a cleanForced zone))
        		_remove = ( ({( _veh inArea _x )} count _cleanForzedZones) > 0 );
                if !( _remove ) then { _remove = ( ({(_x distance _veh) <= _reckDist } count allPlayers) == 0 ); };
    		};
		};
		if  ( _remove ) then {
    		#ifdef DEBUG
			private _debug = format["veh %1 has been removed", _forEachIndex];
			debug(LL_DEBUG, _debug);
			#endif
    		VW deleteAt _forEachIndex;
    		deleteVehicle _veh;
    		_removed = _removed + 1;
		};
	} forEach VW;
    #ifdef DEBUG	
	private _debug = format["%1 monitored, %2 vehicle(s) removed", (count VW), _removed];
	debug(LL_DEBUG, _debug);
	#endif
};

nil