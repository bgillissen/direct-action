/*
@filename: feats\vehicleWatch\serverDestroy.sqf
Author:
	Ben
Description:
	this run on server
	kill the vehicleWatch serverInit thread,
	remove monitored vehicles
 */

if ( count(_this) == 0 ) exitWith { nil };

params ["_when", "_thread"];

if ( scriptDone _thread ) exitWith { nil };

terminate _thread;

waitUntil { scriptDone _thread };

{ deleteVehicle _x; } forEach VW;

VW = [];

nil