/*
@filename: feats\fatigue\playerRespawn.sqf
Author:
	Ben
Description:
	run on player each time player (re)spawn.
	it enable or disable the fatigue system
*/

#include "_debug.hpp"

if ( MOD_ace ) exitWith {
    #ifdef DEBUG
    debug(LL_INFO, "disabled by ACE presence");
    #endif
    nil
};

player enableFatigue ([false, true] select (["fatigue"] call core_fnc_getParam));
player enableStamina ([false, true] select (["fatigue"] call core_fnc_getParam));

nil