
#include "..\..\common\_dik.hpp"

class curator {
	keycode[] = {DIK_PAUSE, 0, 0, 1}; //alt + pause
	unitMsg = "You are hearing and speaking from your unit";
	remoteMsg = "You are hearing and speaking from the controlled unit";
	camMsg = "You are hearing and speaking from your camera";
	slotLimit = -1; //-1: auto, depends on gameMaster module placed in mission
	notification = 0;
	thermalVision = 1;
	msgOnTakeOver = 1;
	birdType = "";
	noSlotMsg = "No more free Zeus slots available (%1 / %2).";
	ascendMsg = "%3 has ascended, %1 / %2 Zeus slots in use.";
	failedMsg = "%1 has tried to ascend.";
	descendMsg = "%3 has descended, %1 / %2 Zeus slots in use.";
	takeOverMsg = "%1 has taken over %2 (%3)";
	releaseMsg = "%1 has released %2 (%3)";
	FixAction = "<t color='#FF0000'>Fix Zeus Slots</t>";
	requestAction = "<t color='#FAAF3A'>Request Zeus powers</t>";
	releaseAction = "<t color='#FAAF3A'>Withdraw Zeus powers</t>";
};

