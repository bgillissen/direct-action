/*
@filename: common\curator\placeGrp.sqf
Author:
	unknown, tweaked by ben
Description:
	used to allow every curators to edit groups placed by an other curator.
*/
if !( isServer ) exitWith {
	_this remoteExec ["curator_fnc_placeGrp", 2, false];
	nil
};

params ["_curator", "_placed"];

[(units _placed), true] call curator_fnc_addEditable;

{ _x setVariable ["NOAI", true, true]; } forEach (units _placed);

[_placed, true] call dynSim_fnc_set;

nil