/*
@filename: feats\curator\actionFix.sqf
Author:
	Ben
Description:
	this script is executed on player side
	it ask the server to unassign all players from the curator modules
*/

[player] remoteExec ["curator_fnc_fix", 2, false];