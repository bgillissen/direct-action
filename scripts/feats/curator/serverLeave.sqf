/*
@filename: feats\curator\serverLeave.sqf
Author:
	Ben
Description:
	run on server,
	unassign game master module that was assigned to player,
	remove the player's curator slot from the assigned stack
*/

params ["_unit", "_id", "_uid", "_name"];

{
	_x params ["_cuid", "_slot"];
	if ( _cuid isEqualTo _uid ) exitWith {
		unassignCurator (missionNamespace getVariable format["zeus_%1", _slot]);
		curatorAssigned deleteAt _forEachIndex;
		publicVariable "curatorAssigned";
        [_uid, ""] call global_fnc_ambientAnim;
	};
} forEach curatorAssigned;

nil