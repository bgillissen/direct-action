/*
@filename: feats\curator\isAssigned.sqf
Author:
	Ben
Description:
	run server/player side.
	tell if the given player is assigned to a curator module.
*/

params ["_player"];

private _puid = (getPlayerUID _player);

( ({( (_x select 0) isEqualTo _puid )} count curatorAssigned) > 0 );