/*
@filename: feats\curator\playerInit.sqf
Author:
	Ben
Description:
	run on player.
	add once the needed variable eventHandlers to handle a dynamic curator alocation,
	add once the needed curator event to share objects/groups between the curators
	add curator action to things in base that got a curator action 
*/

#include "_debug.hpp"

remoteUnit = false;
stayInBody = true;

//eventHandlers
if ( isNil "curator_EH" ) then {
	isAssigned = false;
    private _code = {
        #ifdef DEBUG
        debug(LL_DEBUG, "curatorAssigned updated");
		#endif
		curatorAssigned = _this select 1;
		isAssigned = [player] call curator_fnc_isAssigned;
	};
    curatorAssigned_PVEH = ["curatorAssigned", _code] call pveh_fnc_add;
	{
		_x addEventHandler ["CuratorGroupPlaced", {_this call curator_fnc_placeGrp}];
		_x addEventHandler ["CuratorObjectPlaced", {_this call curator_fnc_placeObj}];
	} forEach (entities "ModuleCurator_F");
	curator_EH = true;
};

//adding actions
private _request = ["curator", "requestAction"] call core_fnc_getSetting;
private _release = ["curator", "releaseAction"] call core_fnc_getSetting;
private _fix = ["curator", "fixAction"] call core_fnc_getSetting;
{
	{
		_x params ["_thing", "_actions"];
		{
			if ( (configName _x) isEqualTo "curator" ) then {
                #ifdef DEBUG
                private _debug = format["adding actions to %1", _thing];
                debug(LL_DEBUG, _debug);
                #endif
				_thing addAction [_fix, {call curator_fnc_actionFix}, [], 0, false, true, '', '[false] call curator_fnc_condition', 4];
                _thing addAction [_request, {call curator_fnc_actionRequest}, [], 0, false, true, '', '[false] call curator_fnc_condition', 4];
				_thing addAction [_release, {call curator_fnc_actionRelease}, [], 0, false, true, '', '[true] call curator_fnc_condition', 4];
			};
			true
		} forEach _actions;
	} forEach _x;
} forEach [BA_npc, BA_obj, BA_veh];

nil