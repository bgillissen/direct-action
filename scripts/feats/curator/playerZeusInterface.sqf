/*
@filename: feats\curator\playerZeusInterface.sqf
Author:
	Ben
Description:
	run on player.
	add the keybinding to allow player to keep his tfar pos on his body while zeusing  
*/

#include "_debug.hpp"

params ["_inZeus"];

if ( _inZeus ) then {
	[player, !stayInBody] call global_fnc_hideObject;
    if ( stayInBody ) then {
	    systemChat (["curator", "unitMsg"] call core_fnc_getSetting);
	} else {
        systemChat (["curator", "camMsg"] call core_fnc_getSetting);
	};
} else {
    [player, remoteUnit] call global_fnc_hideObject;
};

nil