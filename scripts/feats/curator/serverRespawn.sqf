/*
@filename: feats\curator\serverRespawn.sqf
Author:
	Ben
Description:
	reassign player to his gameMaster module if he was assigned
*/

params ["_player"];

[[_player], false] call curator_fnc_addEditable;

_player spawn {
	private _puid = (getPlayerUID _this);
	{
		_x params ["_uid", "_slot"];
		if ( _uid isEqualTo _puid ) exitWith {
			unassignCurator _slot;
			sleep 2;
			_this assignCurator _slot;
			_x set [2, _this];
			curatorAssigned set[_forEachIndex, _x];
			publicVariable "curatorAssigned";
		};
	} forEach curatorAssigned;
};

if ( (isNil 'zeus_adm') || ((admin (owner _player)) isEqualTo 0) ) exitWith { nil };

_player spawn {
    unassignCurator zeus_adm;
	sleep 2;
	_this assignCurator zeus_adm;
};

nil