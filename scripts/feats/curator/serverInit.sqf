/*
@filename: feats\curator\serverInit.sqf
Author:
	Ben
Description:
	run on server.
	initialize assigned curator stack, update allowed curators stack 
*/

#include "_debug.hpp"

if ( isNil "curatorIcons" ) then { curatorIcons = []; };

if ( isNil "curatorAssigned" ) then {
	curatorAssigned = [];
	publicVariable "curatorAssigned";
};

if ( isNil "TOT_CURATOR" ) then {
    private _gms = (entities "ModuleCurator_F");
    TOT_CURATOR = {!(_x getVariable["reserved", false])} count _gms;
    { _x setVehicleVarName format["zeus_%1", _forEachIndex]; } forEach _gms;
    private _slotLimit = (["curator", "slotLimit"] call core_fnc_getSetting);
	if ( _slotLimit > -1 ) then {
        #ifdef DEBUG
   		if ( _slotLimit < TOT_CURATOR ) then {
            debug(LL_DEBUG, "available slot is limited by mission config");
		} else {
            debug(LL_DEBUG, "available slot is limited by the amount of gameMaster module placed in mission");
		};
        #endif
       	TOT_CURATOR = _slotLimit; 
	}; 
    #ifdef DEBUG
	private _debug = format["%1 slot(s) available", (TOT_CURATOR + 1)];
	debug(LL_INFO, _debug);
    #endif 
	publicVariable "TOT_CURATOR";
};

if ( isNil "curatorMemberDataPVEH" ) then {
    #ifdef DEBUG
	debug(LL_DEBUG, "registering memberData PVEH");
    #endif 
	curatorMemberDataPVEH = ["memberData", {call curator_fnc_update}] call pveh_fnc_add;
};

call curator_fnc_update;

nil