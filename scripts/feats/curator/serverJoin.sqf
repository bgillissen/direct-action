/*
@filename: feats\curator\serverJoin.sqf
Author:
	Ben
Description:
	this script is run on server side when a player join
	to make player a zeus editable object
*/

params ["_player"];

if ( isNull _player ) exitWith {
    #ifdef DEBUG
    debug(LL_DEBUG, "serverJoin called with a null _player");
    #endif
    nil
};

[[_player], true] call curator_fnc_addEditable;

nil