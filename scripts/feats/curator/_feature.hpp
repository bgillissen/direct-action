class curator : feat_base {
	class server : featServer_base {
		class init : featEvent_enable {};
		class postInit : featEvent_enable {};
		class destroy : featEvent_enable {};
		class leave : featEvent_enable {};
		class join : featEvent_enable {};
		class remoteControl : featEvent_enable {};
		class respawn : featEvent_enable {};
	};
	class player : featPlayer_base {
		class assemble : featEvent_enable {};
		class init : featEvent_enable {};
		class postInit : featEvent_enable {};
		class remoteControl : featEvent_enable {};
		class zeusInterface : featEvent_enable {};
	};
};
