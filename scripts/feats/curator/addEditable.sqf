
if !( isServer ) exitWith {
    _this remoteExec ["curator_fnc_addEditable", 2, false];
};

params ["_toAdd", "_crew"];

if !( (typeName _toAdd) isEqualTo "ARRAY" ) then { _toAdd = [_toAdd]; };

{
	_x addCuratorEditableObjects [_toAdd, _crew];
} count allCurators;