/*
@filename: feats\curator\release.sqf
Author:
	Ben
Description:
	run on server,
	unassign game master module that was assigned to player,
	remove the player's curator slot from the stack,
	broadcast a sideChat message 
*/

params ["_player"];

{
	_x params ["_uid", "_gm"];
	if ( _uid isEqualTo (getPlayerUID _player) ) exitWith {
		unassignCurator _gm;
		curatorAssigned deleteAt _forEachIndex;
		publicVariable "curatorAssigned";
		if ( CTXT_PLAYER ) then { 
			isAssigned = [player] call curator_fnc_isAssigned;
		};
	};
} forEach curatorAssigned;

private _msg = format[(["curator", "descendMsg"] call core_fnc_getSetting), 
                      (count curatorAssigned), 
  	  	  	  	  	  TOT_CURATOR, 
  	  	  	  	  	  (name _player)];

[1, _msg, ["HQ", PLAYER_SIDE]] call global_fnc_chat;