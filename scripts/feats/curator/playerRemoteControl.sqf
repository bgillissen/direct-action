/*
@filename: feats\curator\playerRemoteControl.sqf
Author:
	Ben
Description:
	run on player.
*/

#include "_debug.hpp"

params ["_gm", "_player", "_unit", "_ctrl"];

remoteUnit = _ctrl;

if ( remoteUnit ) then {
    systemChat (["curator", "remoteMsg"] call core_fnc_getSetting); 
	[player, true] call global_fnc_hideObject; 
};

nil