/*
@filename: feats\curator\serverRemoteControl.sqf
Author:
	Ben
Description:
	run on server,
	we broadcast a systemChat message to all assigned curators
	we define curatorUnit global var on curator remote controlling the unit 
*/

params ["_gm", "_player", "_unit", "_ctrl"];

if ( (["curator", "msgOnTakeOver"] call core_fnc_getSetting) == 0 ) exitWith { nil };

private "_msg"; 

if ( _ctrl ) then {
	_msg = ["curator", "takeOverMsg"] call core_fnc_getSetting;	
} else {
	_msg = ["curator", "releaseMsg"] call core_fnc_getSetting;
};

private _desc = getText (configFile >> "CfgVehicles" >> typeOf _unit >> "displayName");
_msg = format[_msg, (name _player), (name _unit), _desc];

private _targets = [];
{ _targets pushback (_x select 2); } forEach curatorAssigned;
{ 
	if ( (admin (owner _x)) > 0 ) then { _targets pushbackUnique _x; };
} forEach allPlayers;

[_targets, _msg] call common_fnc_systemChat;

if ( CTXT_PLAYER ) then { systemChat _msg; };

nil