class curator_fnc_release {
	allowedTargets=2;
	jip=0;
};
class curator_fnc_reload {
	allowedTargets=2;
	jip=0;
};
class curator_fnc_request {
	allowedTargets=2;
	jip=0;
};
class curator_fnc_fix {
	allowedTargets=2;
	jip=0;
};
class curator_fnc_addEditable {
	allowedTargets=2;
	jip=0;
};
class curator_fnc_placeGrp {
	allowedTargets=2;
	jip=0;
};
class curator_fnc_placeObj {
	allowedTargets=2;
	jip=0;
};
class bis_fnc_curatorRespawn {
	allowedTargets=2;
	jip=0;
};
class BIS_fnc_setDate {
	allowedTargets=2;
	jip=0;
};
class BIS_fnc_setFog {
	allowedTargets=2;
	jip=0;
};
class BIS_fnc_setOvercast {
	allowedTargets=2;
	jip=0;
};
class BIS_fnc_setPPEffectTemplate {
	allowedTargets=2;
	jip=0;
};
