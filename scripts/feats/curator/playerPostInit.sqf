/*
@filename: feats\curator\playerPostInit.sqf
Author:
	Ben
Description:
	run on player.
	define vars to quickly see if player is a curator  
*/

#include "_debug.hpp"

if ( !isMultiplayer ) exitWith {
    #ifdef DEBUG
    debug(LL_INFO, "not multiplayer, player is curator and assigned, no matter what");
    #endif
	isAssigned = true;
	player assignCurator zeus_0;
	curatorAssigned append [["", 0, player]];
    nil
};

isAssigned = [player] call curator_fnc_isAssigned;

["curator", "Toggle Zeus earing / speaking location", "curator_fnc_swapStayInBody"] call keybind_fnc_add;

nil