/*
@filename: feats\curator\condition.sqf
Author:
	Ben
Description:
	run on player,
	request / release curator action condition
*/

params ["_mustBeAssigned"];

if ( CTXT_SERVER ) exitWith { false };

if ( (call BIS_fnc_admin) > 0 ) exitWith { false };

if !( [player, "zeus"] call memberData_fnc_is ) exitWith { false };

if ( _mustBeAssigned ) exitWith { isAssigned };

!isAssigned