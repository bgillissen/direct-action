/*
@filename: feats\curator\update.sqf
Author:
	Ben
Description:
	run on server,
	rebuild allowed curators stack using memberData,
	unassign players that are assigned and not present in the stack anymore
*/

#include "_debug.hpp"

{
	_x params ["_uid", "_gm"];
	if !( [_uid, "zeus"] call memberData_fnc_is ) then {
		unassignCurator _gm;
		curatorAssigned deleteAt _forEachIndex;
	};	
} forEach curatorAssigned;

publicVariable "curatorAssigned";

if ( !isDedicated ) then { 
	isAssigned = [player] call curator_fnc_isAssigned;
};

#ifdef DEBUG
debug(LL_DEBUG, "memberData update");
#endif