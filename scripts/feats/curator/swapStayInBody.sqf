/*
@filename: feats\curator\swapStayInBody.sqf
Author:
	Ben
Description:
	run on player.
	swap curator ears / mouth location (unit or camera)  
*/

params ["_control", "_keyCode", "_shift", "_ctrl", "_alt"];

if !( inZeus ) exitWith {};

if ( stayInBody ) then {
	stayInBody = false;
    systemChat (["curator", "camMsg"] call core_fnc_getSetting);
} else {
	stayInBody = true;
    systemChat (["curator", "unitMsg"] call core_fnc_getSetting);
};

[player, !stayInBody] call global_fnc_hideObject;

player setVariable ["inZeus", !stayInBody, true];

true