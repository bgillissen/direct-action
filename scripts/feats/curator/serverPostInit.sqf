/*
@filename: feats\curator\serverPostInit.sqf
Author:
	Ben
Description:
	run on server
	configure the gameMaster modules (nigth vision, bird, notification)	 
*/

private _notif = [false, true] select (["curator", "notification"] call core_fnc_getSetting);
private _thermal = [false, true] select (["curator", "thermalVision"] call core_fnc_getSetting);
private _doMsg = [false, true] select (["curator", "msgOnTakeOver"] call core_fnc_getSetting);
private _bird = (["curator", "birdType"] call core_fnc_getSetting);
{
	if ( _thermal ) then { [_x, [-1, -2, 0]] call BIS_fnc_setCuratorVisionModes; };
    private _birdObj = objNull;
	if !( _bird isEqualTo "" ) then { _birdObj = _bird createVehicle [0,0,0]; };
    _x setVariable ["birdtime", 0 , true];
    _x setVariable ["bird", _birdObj, true];				
	_x setVariable ["showNotification", _notif, true];
} forEach (entities "ModuleCurator_F");

nil