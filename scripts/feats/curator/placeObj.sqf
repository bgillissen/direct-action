/*
@filename: common\curator\placeObjServer.sqf
Author:
	unknown, tweaked by ben
Description:
	used to allow every curators to edit objects placed by an other curator.
*/

if !( isServer ) exitWith {
	_this remoteExec ["curator_fnc_placeObj", 2, false];
	nil
};

params ["_curator", "_placed"];

[[_placed], true] call curator_fnc_addEditable;

[_placed, true] call dynSim_fnc_set;

nil