/*
@filename: feats\curator\request.sqf
Author:
	Ben
Description:
	run on server,
	check if we got a free slot, then if player is one of our allowed curators 
	if so, search for a free slot and assign it to him.
*/

params ["_player"];

if ( [_player] call curator_fnc_isAssigned ) exitWith {
	[_player, "You have already ascended!"] call common_fnc_systemChat;
};

if ( (count curatorAssigned) isEqualTo TOT_CURATOR ) exitWith {
	_msg = ["curator", "noSlotMsg"] call core_fnc_getSetting;
	[_player, format[_msg, (count curatorAssigned), TOT_CURATOR]] call common_fnc_systemChat;
};

if !( [_player, "zeus"] call memberData_fnc_is ) exitWith {
	_msg = ["curator", "failedMsg"] call core_fnc_getSetting;
	[1, _msg, ["HQ", PLAYER_SIDE]] call global_chatServer;
};

private _freeslot = objNull;
{
    if !( _x getVariable["reserved", false] ) then {
    	private _slot = _x;
    	if ( ({(_x select 1) isEqualTo _slot} count curatorAssigned) isEqualTo 0 ) exitWith { _freeSlot = _slot; };
	};
} forEach (entities "ModuleCurator_F");

if ( isNull _freeSlot ) exitWith {
    [_player, "hmmm, something is wrong, better call ben."] call common_fnc_systemChat;
};

_player assignCurator _freeSlot;

curatorAssigned pushback [(getPlayerUID _player), _freeSlot, _player];

publicVariable "curatorAssigned";

if ( CTXT_PLAYER ) then { 
	isAssigned = [player] call curator_fnc_isAssigned; 
};

_msg = ["curator", "ascendMsg"] call core_fnc_getSetting;
[1, format[_msg, (count curatorAssigned), TOT_CURATOR, (name _player)], ["HQ", PLAYER_SIDE]] call global_fnc_chat;