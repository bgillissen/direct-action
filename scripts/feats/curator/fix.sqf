/*
@filename: feats\curator\fix.sqf
Author:
	Ben
Description:
	run on server,
	reAssign all assigned player.
*/

params ["_player"];

_updated = true;
{
    _x params ["_uid", "_slot", "_player"];
    if ( !(isNull _slot) && !(isNull _player) ) then {
        unassignCurator _slot;
        _slot assignCurator _player;
    } else {
        curatorAssigned deleteAt _forEachIndex;
        _updated = true;
    };
} forEach curatorAssigned;

if ( _updated ) then { publicVariable "curatorAssigned"; };

if ( isNull zeus_adm ) exitWith {};

{
    if ( (admin _x) > 0 ) exitWith {
        unassignCurator zeus_adm; 
    	zeus_adm assignCurator _x; 
	};
} forEach allPlayers;