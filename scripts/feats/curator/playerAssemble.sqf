/*
@filename: feats\curator\playerAssemble.sqf
Author:
	Ben
Description:
	run on player
	make uav an zeus editable object.
*/

#include "_debug.hpp"

params ["_unit", "_veh"];

[[_veh], true] call curator_fnc_addEditable;

nil