class playerSpawn {
	tag = "playerSpawn";
	class functions {
		class playerDestroy { file="feats\playerSpawn\playerDestroy.sqf"; };
		class playerInit { file="feats\playerSpawn\playerInit.sqf"; };
		class playerPostInit { file="feats\playerSpawn\playerPostInit.sqf"; };
		class playerPreInit { file="feats\playerSpawn\playerPreInit.sqf"; };
		class playerRespawn { file="feats\playerSpawn\playerRespawn.sqf"; };
		class serverLeave { file="feats\playerSpawn\serverLeave.sqf"; };
		class loadout { file="feats\playerSpawn\loadout.sqf"; };
		class teleport { file="feats\playerSpawn\teleport.sqf"; };
	};
};
