/*
@filename: feats\playerSpawn\playerInit.sqf
Author:
	Ben
Description:
	run on player,
	give role loadout, store first spawn location
	tp player on the active base spawn marker depending on his role
*/

call playerSpawn_fnc_loadout;

if ( isNil "PSPOS" ) then { PSPOS = getPos player; };

call playerSpawn_fnc_teleport;

nil