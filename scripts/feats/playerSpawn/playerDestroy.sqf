/*
@filename: feats\playerSpawn\playerDestroy.sqf
Author:
	Ben
Description:
	run on player,
	black out player screen, tp player back to where he was on first spawn, empty loadout.
*/

BLACKSCREEN = true;

"loading" cutText ["Please wait, some parameters have been changed, server is reloading...", "BLACK", 0.01, true];

player setPos PSPOS;

removeAllWeapons player;
removeAllItems player;
removeAllAssignedItems player;
removeUniform player;
removeVest player;
removeBackpack player;
removeHeadgear player;
removeGoggles player;

nil