class PS_LOADEND {
    idd = 77777;
    movingEnable = false;
    enableSimulation = true;
    onUnload = "if !( playerSpawn_loadEnd ) then { [] spawn { waitUntil { isNull(findDisplay 77777) }; (findDisplay 46) createDisplay 'PS_LOADEND'; }; };";
    class controlsBackground {};
    class controls {
        class fullscreen : daTxtButton {
            moving = 0;
            colorText[] = {0, 0, 0, 0};
            colorDisabled[] = {0, 0, 0, 0};
            colorShadow[] = {0, 0, 0, 0};
            colorBorder[] = {0, 0, 0, 0};
            colorBackground[] = {0, 0, 0, 0};
            colorBackgroundDisabled[] = {0, 0, 0, 0};
            colorBackgroundActive[] = {0, 0, 0, 0};
            soundEnter[] = {"", 0, 0};
            soundPush[] = {"", 0, 0};
            soundClick[] = {"", 0, 0};
            soundEscape[] = {"", 0, 0};
            x = "safezoneX";
            y = "safezoneY";
            w = "safezoneW";
            h = "safezoneH";
            onButtonClick = "missionNamespace setVariable['playerSpawn_loadEnd', true];(findDisplay 77777) closeDisplay 1;";
        };
    };
};
