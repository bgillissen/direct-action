/*
@filename: feats\playerSpawn\loadout.sqf
Author:
	Ben
Description:
	run on player,
	give role loadout
*/

#include "_debug.hpp"

private _role = player getVariable "role";

if !( isNil "_role" ) then {
    private _lo = missionNamespace getVariable format["RL_%1_%2", _role, PLAYER_SIDE];
	if !( isNil "_lo" ) then {
		_lo params["_u", "_v", "_b", "_pw", "_sw", "_hw", "_h", "_f", "_c", "_t", "_m", "_bino", "_n", "_w", "_cp"];
        //special case to give a LR radio to tank commander
        if ( (_role isEqualTo "crew") && ((roleDescription player) isEqualTo "Tank Commander") ) then {
	        (missionNamespace getVariable[(format["RL_tl_%1", PLAYER_SIDE]), []]) params["_TLu", "_TLv", "_TLb"];
            _b set [0, (_TLb select 0)];
            _TLu = nil;
            _TLv = nil;
            _TLb = nil;
		};
		_lo = nil;
		[player, _u, _v, _b, _pw, _sw, _hw, _h, _f, _c, _t, _m, _bino, _n, _w, _cp] call common_fnc_setLoadout;
    #ifdef DEBUG
	} else {
		private _debug = format["role loadout is not set for 'RL_%1_%2'", _role, PLAYER_SIDE];
    	debug(LL_ERR, _debug);
    #endif
    };
#ifdef DEBUG    
} else {
    private _debug = format["player role was not defined for '%1'", (roleDescription player)];
    debug(LL_ERR, _debug);
#endif
};