class playerSpawn : feat_base  {
	class player : featPlayer_base {
		class destroy : featEvent_enable { order=10; };
		class init :  featEvent_enable { order=99; };
		class postInit : featEvent_enable {
			order=98;
			thread=1;
		};
		class preInit : featEvent_enable { order=10; };
		class respawn : featEvent_enable { order = 10; };
	};
	class server : featServer_base {
		class leave : featEvent_enable { order = 10; };
	};
};
