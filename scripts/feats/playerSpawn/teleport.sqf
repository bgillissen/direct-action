/*
@filename: feats\playerSpawn\teleport.sqf
Author:
	Ben
Description:
	run on player
	tp player on the active base spawn marker depending on his role
*/

private _marker = (player getVariable "role") call {
	if ( _this isEqualTo "hPilot" ) exitWith { "BS_hPilot" };
	if ( _this isEqualTo "jPilot" ) exitWith { "BS_jPilot" };
	if ( _this isEqualTo "crew" ) exitWith { "BS_crew" };
	"BS_inf"
};

player setDir (markerDir _marker);

(getMarkerPos _marker) params ["_x", "_y"];

private _rand = (["playerSpawn", "tpRandom"] call core_fnc_getSetting);
private _xRand = random _rand;
if ( (random 1) < 0.5 ) then { _xRand = (_xRand - (2 * _xRand )); };
private _yRand = random _rand;
if ( (random 1) < 0.5 ) then { _yRand = (_yRand - (2 * _yRand)); };

player setVelocity [0,0,0];
player setPosATL [(_x + _xRand), (_y + _yRand), 0];