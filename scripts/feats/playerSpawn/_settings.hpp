class playerSpawn {
	loading = "Please wait, loading...";
	loadFadeout = 2;
	welcome = "Welcome to %1, you are %2\n\nClick to continue.";
	welcomeDuration = 3.5;
	welcomeFadeOut = 1;
	tpRandom = 2;
};
