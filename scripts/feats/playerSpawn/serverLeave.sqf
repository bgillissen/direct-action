/*
@filename: feats\playerSpawn\serverLeave.sqf
Author:
	Ben
Description:
	run on server,
	remove player _unit (no dead in base ;))
*/

params ["_unit", "_id", "_uid", "_name"];

if !( isNull _unit ) then {
    [7, format["%1 went back to the lobby", _name]] call global_fnc_chat; 
	deleteVehicle _unit; 
};

nil