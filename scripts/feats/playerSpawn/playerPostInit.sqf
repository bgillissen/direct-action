/*
@filename: feats\playerSpawn\playerPostInit.sqf
Author:
	Ben
Description:
	run on player,
	remove blackscreen, and tell player where he is and on which side
*/

waitUntil { sleep 1;!PLAYER_INIT };

if !( isNil "DOLOCK" ) then {
	waitUntil { !DOLOCK };
};

private _name = getText(missionConfigFile >> "settings" >> "baseAtmosphere" >> toUpper(worldName) >> BASE_NAME >> "name");
private _msg = (["playerSpawn", "welcome"] call core_fnc_getSetting);
"welcome" cutText [format[_msg, _name, PLAYER_SIDEDSP], "BLACK", 0.01, true];

private _fade = (["playerSpawn", "loadFadeOut"] call core_fnc_getSetting);
"loading" cutFadeOut _fade;

private _time = time + _fade;

playerSpawn_loadEnd = false;
(findDisplay 46) createDisplay "PS_LOADEND";
waitUntil { playerSpawn_loadEnd };

//private _duration = (["playerSpawn", "welcomeDuration"] call core_fnc_getSetting);
//waitUntil { ( PLAYER_INGAME && (time > (_time + _duration)) ) };

_fade = (["playerSpawn", "welcomeFadeOut"] call core_fnc_getSetting);
"welcome" cutFadeOut _fade;

sleep _fade;

BLACKSCREEN = false;

nil