/*
@filename: feats\playerSpawn\playerRespawn.sqf
Author:
	Ben
Description:
	run on player,
	give role loadout
	tp player on the active base spawn marker depending on his role
*/

//on the first spawn the playerInit take care of it
if ( isNil "FIRST_SPAWN" ) exitWith { 
	FIRST_SPAWN = false; 
    nil 
};

call playerSpawn_fnc_loadout;
call playerSpawn_fnc_teleport;

nil