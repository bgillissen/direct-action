/*
@filename: feats\memberData\playerUpdate.sqf
Author:
	Ben
Description:
	run on player,
	define player's rank info according to memberData array
*/

#include "_debug.hpp"

private _uid = getPlayerUID player;

{
	if ( _uid isEqualTo (_x select MD_uid) ) exitWith {
		#ifdef DEBUG
    	debug(LL_DEBUG, "player is a member");
		#endif
		player setVariable["MD_rank", (_x select MD_rank), true];
		private _shortRank = getText(missionConfigFile >> "settings" >> "memberData" >> "ranks" >> format["rank%1", (_x select MD_rank)] >> "shortName");
		player setVariable["MD_name", format["%1. %2", _shortRank, (name player)], true];
	};
} forEach memberData;