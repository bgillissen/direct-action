class memberData {
	tag = "memberData";
	class functions {
		class baseProtection { file="feats\memberData\baseProtection.sqf"; };
		class gearRestrictions { file="feats\memberData\gearRestrictions.sqf"; };
		class is { file="feats\memberData\is.sqf"; };
		class isMember { file="feats\memberData\isMember.sqf"; };
		class isZeus { file="feats\memberData\isZeus.sqf"; };
		class playerInit { file="feats\memberData\playerInit.sqf"; };
		class playerUpdate { file="feats\memberData\playerUpdate.sqf"; };
		class serverJoin { file="feats\memberData\serverJoin.sqf"; };
		class serverPreInit { file="feats\memberData\serverPreInit.sqf"; };
		class update { file="feats\memberData\update.sqf"; };
		class vehicleRestrictions { file="feats\memberData\vehicleRestrictions.sqf"; };
	};
};
