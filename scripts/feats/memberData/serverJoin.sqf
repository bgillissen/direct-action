/*
@filename: feats\memberData\serverJoin.sqf
Author:
	Ben
Description:
	run on server,
	inform our webAPI that a member as joined 
*/

#include "_debug.hpp"

params ["_player"];

if ( isNull _player ) exitWith {
    #ifdef DEBUG
    debug(LL_DEBUG, "serverJoin called with a null player");
    #endif
    nil
};

if ( isNil "memberData_fnc_lastSeen" ) exitWith {
    #ifdef DEBUG
    debug(LL_DEBUG, "webAPI server mod not loaded, abording");
    #endif
    nil
};

private _uid = getPlayerUID _player;

{
	if ( _uid isEqualTo (_x select 0) ) exitWith {
		_uid spawn memberData_fnc_lastSeen;
    };
} forEach memberData;

nil