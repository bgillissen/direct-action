/*
@filename: feats\memberData\update.sqf
Author:
	Ben
Description:
	run on server and player,
	if on server, redefine the memberData array following the dynamic settings
	if on player, ask server to redefine memberData array 
*/

#include "_debug.hpp"

if !( isServer ) exitWith { nil remoteExec ["memberData_fnc_update", 2, false]; };

memberData = [];

#define EMPTYAR []

private _attribs = ["memberData", "attribs"] call core_fnc_getSetting;
{
    private _mbr = _x;
    private _buff = EMPTYAR;
	_buff set [MD_uid, 		getText(_mbr >> "uid" )];
	_buff set [MD_rank, 	getNumber(_mbr >> "rank" )];
    _buff set [MD_country,	getText(_mbr >> "country")];
    { _buff set [(_forEachIndex + MD_offset), getNumber(_mbr >> _x)]; } forEach _attribs;
	memberData pushback _buff;
} forEach ("true" configClasses ( missionConfigFile >> "settings" >> "memberData" >> "fixed") );

private _dynMode = (["memberData", "dynamic", "mode"] call core_fnc_getSetting);

if ( _dynMode == 0 ) exitWith { publicVariable "memberData"; };

private _dynSrc = (["memberData", "dynamic", "src"] call core_fnc_getSetting);

private _data = "";

if ( _dynMode == 1 ) then { _data = loadFile _dynSrc; };
if ( _dynMode == 2 ) then { _data = [_dynSrc] call common_fnc_urlFetchReturn; };

private _lines = _data splitString (toString[10]);
_data = nil;

{
	private _data = _x splitString ";";
    private _buff = EMPTYAR;
    _buff set [MD_uid, 		(_data select MD_uid)];
	_buff set [MD_rank, 	parseNumber(_data select MD_rank)];
    _buff set [MD_country,	(_data select MD_country)];
	{ _buff set [(_forEachIndex + MD_offset), parseNumber(_data select (_forEachIndex + MD_offset))]; } forEach _attribs;
	private _key = -1;
	{
		if ( (_buff select MD_uid) isEqualTo (_x select MD_uid) ) then { _key = _forEachIndex; };
	} forEach memberData;
	if ( _key >= 0 ) then {
		memberData set [_key, _buff];
	} else {
		memberData pushback _buff;
	};
} forEach _lines;

publicVariableServer "memberData";
publicVariable "memberData";

#ifdef DEBUG
private _debug = format["array has been updated, %1 member(s) found", (count memberData)];
debug(LL_DEBUG, _debug);
#endif