/*
@filename: feats\memberData\playerInit.sqf
Author:
	Ben
Description:
	run on player,
	register memberData PVEH
	add reload memberData action to base things
*/

#include "_debug.hpp"

MD_uid = 0;
MD_rank = 1;
MD_country = 2;
MD_offset = 3;

call memberData_fnc_playerUpdate;

if ( isNil "memberDataPVEH" ) then {
    memberDataPVEH = ["memberData", {call memberData_fnc_playerUpdate}] call pveh_fnc_add;
};

if ( (["memberData", "dynamic", "mode"] call core_fnc_getSetting) == 0 ) exitWith {
    #ifdef DEBUG
    debug(LL_DEBUG, "dynamic memberData is disabled by mission config");
    #endif
    nil
};

private _action = (["memberData", "dynamic", "action"] call core_fnc_getSetting);

{
	{
		_x params ["_thing", "_actions"];
		{
			if ( (configName _x) isEqualTo "memberData" ) then {
                #ifdef DEBUG
                private _debug = format["adding action to %1", _thing];
                debug(LL_DEBUG, _debug);
                #endif
				_thing addAction [_action, {call memberData_fnc_update}, [], 0, false, true, '', "( CTXT_SERVER || ((call BIS_fnc_admin) > 0) || ([player, 'officer'] call memberData_fnc_is) )", 4];
			};
		} forEach _actions;
	} forEach _x;
} forEach [BA_veh, BA_npc, BA_obj];

nil