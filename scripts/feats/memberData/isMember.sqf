/*
@filename: feats\memberData\isMember.sqf
Author:
	Ben
Description:
	run on player / server,
	check if given player uid is found in memberData array
*/

params ["_player", ["_noRecruit", false]];

private _uid = getPlayerUID _player;
private _bool = false;

{
	if ( _uid isEqualTo (_x select MD_uid) ) exitWith {
        if ( _noRecruit ) then {
            _bool = ( (_x select MD_rank) > 2 );
        } else {
    		_bool = ( (_x select MR_rank) > 0 );
		}; 
	};
} forEach memberData;

_bool