class memberData {
	attribs[] = {"founder",	"officer", "zeus", "BP", "GR", "VR", "basic", "cls", "jtac", "eod", "helo", "cas", "sniper", "arti", "tank", "teacher"};
	class dynamic {
		/*
		  Notice that you will have to activate file patching via -filePatching.
		  Otherwise loadFile will not load any files outside your mission folder.
		  For more info see CMA:DevelopmentSetup (since Arma 3 1.49+).
		  Also for mode 2, the url_fetch extension is required!
		*/
		mode = 2;	//0: disabled, 1: loadFile, 2: fetchUrl extension
		src = "http://taskforceunicorn.com/api?do=getMemberData";
		action = "Reload Member Data";
	};
	class fixed {
		class rainman {
			uid = "76561197971304076";
			rank = 10;
			country = "SE";
			founder = 1;
			officer = 1;
			zeus = 1;
			BP = 0;
			GR = 1;
			VR = 1;
			basic = 1;
			cls = 1;
			jstac = 0;
			eod = 0;
			helo = 0;
			cas = 0;
			sniper = 0;
			arti = 0;
			tank = 0;
			teacher = 0;
		};
		class ben {
			uid = "76561198030235789";
			rank = 9;
			country = "BE";
			founder = 0;
			officer = 1;
			zeus = 1;
			BP = 1;
			GR = 1;
			VR = 1;
			basic = 1;
			cls = 1;
			jstac = 0;
			eod = 0;
			helo = 0;
			cas = 0;
			sniper = 0;
			arti = 0;
			tank = 0;
			teacher = 0;
		};
	};
	class ranks {
		class rank0 {
			displayName = "";
			shortName = "";
		};
		class rank1 {
			displayName = "Applicant";
			shortName = "A";
		};
		class rank2 {
			displayName = "Recruit";
			shortName = "R";
		};
		class rank3 {
			displayName = "Private";
			shortName = "Pvt";
		};
		class rank4 {
			displayName = "Private Firstclass";
			shortName = "Pfc";
		};
		class rank5 {
			displayName = "Specialist";
			shortName = "Spc";
		};
		class rank6 {
			displayName = "Corporal";
			shortName = "Cpl";
		};
		class rank7 {
			displayName = "Sergeant";
			shortName = "Sgt";
		};
		class rank8 {
			displayName = "Master Sergeant";
			shortName = "MSg";
		};
		class rank9 {
			displayName = "Lieutenant";
			shortName = "Lt";
		};
		class rank10 {
			displayName = "Captain";
			shortName = "Cpt";
		};
	};
};
