/*
@filename: feats\memberData\serverPreInit.sqf
Author:
	Ben
Description:
	run on server,
	define the memberData array
*/

MD_uid = 0;
MD_rank = 1;
MD_country = 2;
MD_offset = 3;

call memberData_fnc_update;

nil