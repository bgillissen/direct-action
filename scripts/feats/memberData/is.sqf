/*
@filename: feats\memberData\is.sqf
Author:
	Ben
Description:
	run on player / server,
	tell if given player / uid got the given attribute
*/

params ["_player", "_attrib"];

private _attribs = ["memberData", "attribs"] call core_fnc_getSetting;
private _key = _attribs find _attrib;

if ( _key < 0 ) exitWith { false };

_key = _key + MD_offset;

private "_uid";
if ( (typeName _player) isEqualTo "OBJECT" ) then {
	_uid = getPlayerUID _player;
} else {
	_uid = _player;        
};

private _bool = false; 
{
	if ( _uid isEqualTo (_x select MD_uid) ) exitWith { _bool = ( (_x select _key) isEqualTo 1 ); };
} forEach memberData;

_bool