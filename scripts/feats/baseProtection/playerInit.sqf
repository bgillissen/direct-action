/*
@filename: feats\baseProtection\playerInit.sqf
Author:
	Ben
Description:
	run on player,
	keep track of player distance from the active base saveZone
*/

#include "_debug.hpp"

BP_inBase = false;

if ( (["baseProtection"] call core_fnc_getParam) == 0 ) exitWith {
    #ifdef DEBUG
   	debug(LL_INFO, "disabled by mission parameter");
    #endif
    nil 
};

BP_inBase = true;
BP_allowedWeapons = ["baseProtection", "exceptions", "weapons"] call core_fnc_getSetting;
BP_allowedAmmos = ["baseProtection", "exceptions", "ammos"] call core_fnc_getSetting;
BP_allowedMags = ["baseProtection", "exceptions", "mags"] call core_fnc_getSetting;
BP_zones = [];
{	_x params ["_area", "_actions"];
    if ( "noShot" in _actions ) then { BP_zones pushback _area; };
} forEach BA_zones;

if ( BLACKSCREEN ) then { waitUntil { sleep 1;!BLACKSCREEN }; };

private _delay = ["baseProtection", "sleepDelay"] call core_fnc_getSetting;

#ifdef DEBUG
private _debug = format["thread has started with %1s delay between checks, %2 zone(s) to check", _delay, count(BA_zones)];
debug(LL_DEBUG, _debug);
#endif

while { true } do {
	if !( player call memberData_fnc_baseProtection ) then {
		BP_inBase = false; 	
	} else {
        BP_inBase = false;
        if !( remoteUnit ) then {
        	BP_inBase = ( ({( player inArea _x )} count BP_zones) > 0 );    
        };
	};
	sleep _delay;
};

nil