class baseProtection {
	tag = "baseProtection";
	class functions {
		class playerAssemble { file="feats\baseProtection\playerAssemble.sqf"; };
		class playerDestroy { file="feats\baseProtection\playerDestroy.sqf"; };
		class playerInit { file="feats\baseProtection\playerInit.sqf"; };
		class playerShoot { file="feats\baseProtection\playerShoot.sqf"; };
		class serverInit { file="feats\baseProtection\serverInit.sqf"; };
		class showWarning { file="feats\baseProtection\showWarning.sqf"; };
		class unload { file="feats\baseProtection\unload.sqf"; };
		class uavShoot { file="feats\baseProtection\uavShoot.sqf"; };
	};
};
