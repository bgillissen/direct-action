/*
@filename: feats\baseProtection\serverInit.sqf
Author:
	Ben
Description:
	run on server,
	just define the list of zones with protection enabled and some settings
*/

#include "_debug.hpp"

if ( (["baseProtection"] call core_fnc_getParam) isEqualto 0 ) exitWith {
    #ifdef DEBUG
   	debug(LL_INFO, "disabled by mission parameter");
    #endif
    nil
};

BP_allowedWeapons = ["baseProtection", "exceptions", "weapons"] call core_fnc_getSetting;
BP_allowedAmmos = ["baseProtection", "exceptions", "ammos"] call core_fnc_getSetting;
BP_allowedMags = ["baseProtection", "exceptions", "mags"] call core_fnc_getSetting;
BP_zones = [];
{	_x params ["_area", "_actions"];
    if ( "noShot" in _actions ) then { BP_zones pushback _area; };
} forEach BA_zones;

nil