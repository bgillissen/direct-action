/*
@filename: feats\baseProtection\showWarning.sqf
Author:
	Ben
Description:
	run on player,
	show the can not fire in base warning
*/

hintC (["baseProtection", "msg"] call core_fnc_getSetting);

disableSerialization;

hintC_EH = findDisplay 57 displayAddEventHandler ["Unload", {_this spawn baseProtection_fnc_unload;}];