class baseProtection {
	msg = "You are discharging your weapon at base without approval. Cease your actions Immediately!";
	sleepDelay = 3;
	class exceptions {
		weapons[] = {"CMFlareLauncher", "CMFlareLauncher_Singles", "CMFlareLauncher_Triples", "Laserdesignator_pilotCamera", "rhs_weap_MASTERSAFE", "rhs_weap_CMFlareLauncher", "rhsusf_weap_CMFlareLauncher", "rhsusf_weap_ANALQ212", "RHS_LWIRCM_MELB"};
		ammos[] = {};
		mags[] = {};
	};
};
