/*
@filename: feats\baseProtection\playerAssemble.sqf
Author:
	Ben
Description:
	run on player when assemble a weapon,
	rename uav group,
	add a fired eventHandler to an assembled uav
*/

#include "_debug.hpp"

params ["_unit", "_veh"];

if ( (["baseProtection"] call core_fnc_getParam) == 0 ) exitWith {
    #ifdef DEBUG
   	debug(LL_INFO, "disabled by mission parameter");
    #endif
    nil 
};

if !( unitIsUAV _veh ) exitWith { nil };

_veh addEventHandler ["Fired", {_this call baseProtection_fnc_uavShoot;}];

nil