/*
@filename: feats\baseProtection\uavShoot.sqf
Author:
	Ben
Description:
	run on server,
	triggered by a Fired event linked to the uav
*/

params ["_veh", "_weapon", "_muzzle", "_mode", "_ammo", "_mag", "_shell", "_gunner"];

if !( isUAVConnected _veh ) exitWith { nil };

if ( ({( _veh inArea _x )} count BP_zones) isEqualTo 0 ) exitWith { nil };

if ( _veh getVariable['NOBP', false] ) exitWith { nil };
if ( _mag in BP_allowedMags ) exitWith { nil };
if ( _ammo in BP_allowedAmmos ) exitWith { nil };
if ( _weapon in BP_allowedWeapons ) exitWith { nil };

deleteVehicle _shell;

(UAVControl _veh) params ["_unit", "_role"];
if ( isPlayer _unit && !(isNull _unit) ) then {
    nil remoteExec ["baseProtection_fnc_showWarning", (owner _unit), false];
};

nil