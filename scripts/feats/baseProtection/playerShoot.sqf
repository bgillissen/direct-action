/*
@filename: feats\baseProtection\playerShoot.sqf
Author:
	Ben
Description:
	run on player, triggered by a FiredMan event
	if player is in Safe Zone it remove the projectile and display a centered Hint
*/

params ["_unit", "_weapon", "_muzzle", "_mode", "_ammo", "_mag", "_shell", "_veh"];

if !( player call memberData_fnc_baseProtection ) exitWith { nil };

if ( unitIsUAV _veh ) exitWith {
	([_veh, _weapon, _muzzle, _mode, _ammo, _mag, _shell, objNull] call baseProtection_fnc_uavShoot)
};

if !( BP_inBase ) exitWith { nil };

if ( _veh getVariable['NOBP', false] ) exitWith { nil };
if ( _mag in BP_allowedMags ) exitWith { nil };
if ( _ammo in BP_allowedAmmos ) exitWith { nil };
if ( _weapon in BP_allowedWeapons ) exitWith { nil };

deleteVehicle _shell;

hintC (["baseProtection", "msg"] call core_fnc_getSetting);

disableSerialization;

hintC_EH = findDisplay 57 displayAddEventHandler ["Unload", {_this spawn baseProtection_fnc_unload;}];

nil