class vehicleRepair {
	triggerSize = 10;
	timeLimit = 5;
	speedLimit = 2; //m/s
	class marker {
		icon = "mil_triangle";
		text = "Vehicle Service (%1)";
	};
	badType = "Bad vehicle type, this area is only for: %1.";
	nostop = "Once you enter the repair pad you got %1s to stop your vehicle inside the pad.";
	start = "Servicing %1. Stand by...";
	end = "%1 successfully serviced.";
	repairing = "Repairing %1";
	repaired = "Repaired.";
	rearming = "Reloading %1 (%2)";
	pylons = "Rearming pylon %2 with %1";
	rearmed = "Rearmed.";
	refueling = "Refuelling %1";
	refueled = "Refuelled.";
};
