/*
@filename: feats\vehicleRepair\repairOwner.sqf
Author:
	unknown, taken from ahoyWorld I&A
Description:
	run on server,
	repair the given vehicle
*/

params["_veh", "_owners"];

private _isUAV = ( _veh in allUnitsUAV );
private _trgt = _veh;
if ( _isUAV ) then {
	_trgt = objNull;
	if ( isUAVConnected _veh ) then { _trgt = ((UAVControl _veh) select 0) };
};

//store fuel, stop engine, remove fuel, stop vehicle
private _fuel = fuel _veh;
_veh engineOn false;
_veh setFuel 0;
_veh setVelocity [0,0,0];

private _vType = (typeOf _veh);
private _vName = getText(configFile >> "CfgVehicles" >> _vType >> "DisplayName");

private _msg = (["vehicleRepair", "start"] call core_fnc_getSetting);
[format[_msg, _vName], _trgt, _isUAV] call vehicleRepair_fnc_message;

//repair damage
private _damage = getDammage _veh;
private _msg = (["vehicleRepair", "repairing"] call core_fnc_getSetting);
while { _damage > 0 } do {
	sleep 0.5;
	private _prct = 100 - (_damage * 100);
    [format[_msg, floor _prct] + "%", _trgt, _isUAV] call vehicleRepair_fnc_message; 
	if ( (_damage - 0.01) <= 0 ) then {
		_veh setDamage 0;
		_damage = 0;
	} else {
		_veh setDamage (_damage - 0.01);
		_damage = _damage - 0.01;
	};
};
_msg = (["vehicleRepair", "repaired"] call core_fnc_getSetting);
[_msg, _trgt, _isUAV] call vehicleRepair_fnc_message;
_veh setDamage 0;

sleep 2;

//rearm vehicle
private _mags = getArray(configFile >> "CfgVehicles" >> _vType >> "magazines");
_msg = (["vehicleRepair", "rearming"] call core_fnc_getSetting);
private _removed = [];
{
	if !( _x in _removed ) then {
		_veh removeMagazines _x;
		_removed pushback _x;
	};
} forEach _mags;
{
    private _desc = getText(configFile >> "cfgMagazines" >> _x >> "displayName");
    _veh addMagazine _x;
    if !( _desc isEqualTo "" ) then {  
    	[format[_msg, _desc, (_forEachIndex + 1)], _trgt, _isUAV] call vehicleRepair_fnc_message;
	    sleep 1;
	};
} forEach _mags;
{
	private _mags = getArray(_x >> "magazines");
	_removed = [];
	{
		if !( _x in _removed ) then {
			_veh removeMagazines _x;
			_removed pushback _x;
		};
	} forEach _mags;
	{
        private _desc = getText(configFile >> "cfgMagazines" >> _x >> "displayName");
        _veh addMagazine _x;
        if !( _desc isEqualTo "" ) then {  
        	[format[_msg, _desc, (_forEachIndex + 1)], _trgt, _isUAV] call vehicleRepair_fnc_message;
			sleep 1;
		};
	} forEach _mags;
	{
        _mags = getArray(_x >> "magazines");
		_removed = [];
		{
			if (!(_x in _removed)) then {
				_veh removeMagazines _x;
				_removed pushback _x;
			};
		} forEach _mags;
		{
            private _desc = getText(configFile >> "cfgMagazines" >> _x >> "displayName");
            _veh addMagazine _x;
        	if !( _desc isEqualTo "" ) then { 
        		[format[_msg, _desc, (_forEachIndex + 1)], _trgt, _isUAV] call vehicleRepair_fnc_message;
				sleep 1;
			};
		} forEach _mags;
	} forEach ("true" configClasses (_x >> "turrets"));
} forEach ("true" configClasses (configFile >> "CfgVehicles" >> _vType >> "Turrets"));

if ( isClass(configFile >> "cfgVehicles" >> _vType >> "Components" >> "TransportPylonsComponent" >> "Pylons") ) then {
    private _preset = _veh getVariable ["DAVL_preset", ""];
    private _mags = [];
    if ( _preset isEqualTo "" ) then {
        _mags = getArray(configFile >> "cfgVehicles" >> _vType >> "Components" >> "TransportPylonsComponent" >> "Presets" >> "Default" >> "attachment");
    } else {
        {
            _x params ["_name", "_class", "_id", "_ownerUid", "_ownerName", "_pylons"];
            if ( _id isEqualTo _preset ) exitWith { _mags = _pylons; }; 
        } forEach DAVL_serverPresets;
    };
    _msg = (["vehicleRepair", "pylons"] call core_fnc_getSetting);
    {
        if !( isNil "_x" ) then {
        	if !( _x isEqualTo "" ) then {
        		[format[_msg, getText(configFile >> "cfgMagazines" >> _x >> "displayName"), (_forEachIndex + 1)], _trgt, _isUAV] call vehicleRepair_fnc_message;
                sleep 1;
			};
		};
	} forEach _mags;    
};

_veh setVehicleAmmo 1;

{ [_veh] remoteExec ["vehicleRepair_fnc_rearmTurret", _x, false]; } forEach _owners;

_msg = (["vehicleRepair", "rearmed"] call core_fnc_getSetting);
[_msg, _trgt, _isUAV] call vehicleRepair_fnc_message;
sleep 2;

//refuel
_msg = (["vehicleRepair", "refueling"] call core_fnc_getSetting);
while { _fuel < 1 } do {
	sleep 0.5;
	private _prct = (_fuel * 100);
    [format[_msg, floor _prct] + "%", _trgt, _isUAV] call vehicleRepair_fnc_message;
	if ( (_fuel + 0.01) >= 1 ) then {
		_veh setFuel 1;
		_fuel = 1;
	} else {
		_fuel = _fuel + 0.01;
	};
};

_msg = (["vehicleRepair", "refueled"] call core_fnc_getSetting);
[_msg, _trgt, _isUAV] call vehicleRepair_fnc_message;
_veh setFuel 1;

//services vehicle
if ( getNumber(configFile >> _vType >> "transportRepair") > 0 ) then { _veh setRepairCargo 1; };
if ( getNumber(configFile >> _vType >> "transportAmmo") > 0 ) then { _veh setAmmoCargo 1; };
if ( getNumber(configFile >> _vType >> "transportFuel") > 0  ) then { _veh setFuelCargo 1; };

[format[(["vehicleRepair", "end"] call core_fnc_getSetting), _vName], _trgt, _isUAV] call vehicleRepair_fnc_message;