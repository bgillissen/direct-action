
params ["_msg", "_trgt", "_isUAV"];

if !( _isUAV ) exitWith { [4, _msg, _trgt] call global_fnc_chat; };

if ( isNull _trgt ) exitWith {};

[_trgt, _msg] call common_fnc_systemChat;