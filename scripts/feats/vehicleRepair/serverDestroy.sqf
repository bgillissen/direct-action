/*
@filename: feats\vehicleRepair\serverDestroy.sqf
Author:
	Ben
Description:
	run on server,
	remove VR triggers and map markers
 */
 
[VR_triggers] call common_fnc_deleteObjects;

{ deleteMarker _x; } forEach VR_markers;

nil
