/*
@filename: feats\vehicleRepair\rearmTurret.sqf
Author:
	ben
Description:
	run on server,
	just rearm turrets local to player's vehicle
*/

params ["_veh"];

_veh setVehicleAmmo 1;