class vehicleRepair {
	tag = "vehicleRepair";
	class functions {
		class serverDestroy { file="feats\vehicleRepair\serverDestroy.sqf"; };
		class serverInit { file="feats\vehicleRepair\serverInit.sqf"; };
		class rearmTurret { file="feats\vehicleRepair\rearmTurret.sqf"; };
		class message { file="feats\vehicleRepair\message.sqf"; };
		class repair { file="feats\vehicleRepair\repair.sqf"; };
		class repairOwner { file="feats\vehicleRepair\repairOwner.sqf"; };
	};
};
