/*
@filename: feats\vehicleRepair\repair.sqf
Author:
	unknown, taken from ahoyWorld I&A
Description:
	run on server,
	repair the given vehicle
*/

params["_veh", "_types", "_trigger"];

if !( CTXT_SERVER ) exitWith {};

if ( (_veh isKindOf "ParachuteBase") || !(alive _veh) || (_veh isKindOf "Man") ) exitWith {};

private _ok = _types call {
    if ( _veh in allUnitsUAV ) exitWith { ("uav" in _this) };
    if ( _veh isKindOf "Ship" ) exitWith { ("boat" in _this) };
	if ( _veh isKindOf "LandVehicle" ) exitWith { ("land" in _this) };
	if ( _veh isKindOf "Helicopter" ) exitWith { ("heli" in _this) };
	if ( _veh isKindOf "Plane" ) exitWith { ("plane" in _this) };
	false
};

if !( _ok ) exitWith {
    private _msg = (["vehicleRepair", "badType"] call core_fnc_getSetting);
    _msg = format[_msg, _types joinString ", "];
    private _isUAV = ( _veh in allUnitsUAV );
    private _trgt = _veh;
    if ( _isUAV ) then {
        _trgt = objNull;
		if ( isUAVConnected _veh ) then { _trgt = ((UAVControl _veh) select 0) };
    };
    [_msg, _trgt, _isUAV] call vehicleRepair_fnc_message;
};

private _speedLimit = (["vehicleRepair", "speedLimit"] call core_fnc_getSetting);
private _timeLimit = (["vehicleRepair", "timeLimit"] call core_fnc_getSetting);
private _limit = time + _timeLimit;

waitUntil {
	sleep 0.01;
    _veh engineOn false;
    (velocity _veh) params["_vX", "_vY", "_vZ"];
	( (_vX <= _speedLimit && _vY <= _speedLimit && _vZ <= _speedLimit) || (time > _limit) )  
};

private _triggerSize = ["vehicleRepair", "triggerSize"] call core_fnc_getSetting;

if ( (time > _limit) || ((getPos _trigger) distance (getPos _veh) > _triggerSize) ) exitWith {
    [4, format[(["vehicleRepair", "nostop"] call core_fnc_getSetting), _timeLimit], _veh] call global_fnc_chat;
};

private _vehOwner = (owner _veh);
private _owners = [];
private _vType = (typeOf _veh);

{
	private _turretPath = [_forEachIndex];
	private _turretOwner = _veh turretOwner _turretPath;
    if ( !(isNil "_turretOwner") && (getNumber(_x >> "isPersonTurret") isEqualTo 0) ) then {
        if !( _turretOwner isEqualTo _vehOwner ) then { _owners pushBack _turretOwner; };
	};
	{
        private _subPath = _turretPath + [_forEachIndex];
        private _turretOwner = _veh turretOwner _subPath;
    	if ( !(isNil "_turretOwner")  && (getNumber(_x >> "isPersonTurret") isEqualTo 0) ) then {
        	if !( _turretOwner isEqualTo _vehOwner ) then { _owners pushBack _turretOwner; };
		};
	} forEach ("true" configClasses (_x >> "turrets"));
} forEach ("true" configClasses (configFile >> "CfgVehicles" >> _vType >> "Turrets"));


[_veh, _owners] remoteExec ["vehicleRepair_fnc_repairOwner", _vehOwner, false];
