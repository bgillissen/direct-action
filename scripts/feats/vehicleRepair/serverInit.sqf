/*
@filename: feats\vehicleRepair\serverInit.sqf
Author:
	unknown, taken from ahoyWorld I&A
Description:
	run on server,
	create the repair triggers on the base VR_?_? markers
*/

#include "_debug.hpp"

VR_triggers = [];
VR_markers = [];

private _size = ["vehicleRepair", "triggerSize"] call core_fnc_getSetting;
private _color = call common_fnc_getMarkerColor;
private _knownTypes = ["uav", "plane", "heli", "land", "boat"];
private _i = 0;
{
	_x params ["_area", "_actions"];
    private _repairTypes = [];
    {
        if ( _x in _knownTypes ) then { _repairTypes pushback _x; };
    } forEach _actions;
    if ( (count _repairTypes) > 0 ) then {
        ([_area] call common_fnc_getAreaPosRad) params ['_pos', '_rad'];
    	#ifdef DEBUG
        private _debug = format["repair trigger created, type: %1, grid: %2", _x, (mapGridPosition _pos)];
        debug(LL_DEBUG, _debug);
        #endif  
		private _trigger = createTrigger ["EmptyDetector", _pos];
		_trigger setTriggerArea [_size, _size, 0, false, -1];
		_trigger setTriggerActivation [toUpper (str PLAYER_SIDE), "PRESENT", true];
		_trigger setTriggerStatements ["this and ((getPos (thisList select 0)) select 2 < 1)",
	    	                           format["0 = [(thisList select 0), %1, thisTrigger] spawn vehicleRepair_fnc_repair;", _repairTypes],
	        	                       ""];
		VR_triggers pushback _trigger;
        private _marker = format["VR_marker_%1", _i];
		createMarker [_marker, _pos];
		_marker setMarkerColor _color;
		_marker setMarkerShape "ICON";
		_marker setMarkerType (["vehicleRepair", "marker", "icon"] call core_fnc_getSetting);
        private _text = (["vehicleRepair", "marker", "text"] call core_fnc_getSetting);
		_marker setMarkerText format[_text, (_repairTypes joinString ", ")];
		VR_markers pushback _marker;
        _i = _i + 1;
	};
} forEach BA_zones;

nil