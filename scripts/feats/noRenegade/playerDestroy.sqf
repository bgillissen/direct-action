/*
@filename: feats\noRenegade\playerDestroy.sqf
Author:
	Ben
Description:
	run on player,
	remove registered eventHandler
*/

if ( isNil "NR_EH" ) exitWith { nil };

player removeEventHandler ["HandleRating", NR_EH];
NR_EH = nil;

nil