class NOTMEMBER {
	title = "Private session.";
	description = "Sorry, but the server has been locked. Only Task Force Unicorn members are allowed to join, try again later.";
	picture = "";
};
