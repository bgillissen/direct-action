/*
@filename: feats\memberOnly\serverPreInit.sqf
Author:
	Ben
Description:
	run on server,
	define default lock value if not already set 
*/

#include "_debug.hpp"

if !( isNil "memberOnly" ) exitWith { nil };

missionNamespace setVariable ["memberOnly", false, true];

nil