class memberOnly {
	tag = "memberOnly";
	class functions {
		class playerInit { file="feats\memberOnly\playerInit.sqf"; };
		class serverPreInit { file="feats\memberOnly\serverPreInit.sqf"; };
		class setLock { file="feats\memberOnly\setLock.sqf"; };
	};
};
