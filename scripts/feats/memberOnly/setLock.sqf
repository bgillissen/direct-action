/*
@filename: feats\memberOnly\setLock.sqf
Author:
	Ben
Description:
	run on player,
	tell server to lock server to members only 
*/

#include "_debug.hpp"

private _rank = (["memberOnly", "rank"] call core_fnc_getSetting);
if ( (player getVariable['MD_rank', 0]) < _rank ) exitWith {};

params ["_caller", "_target", "_id", "_value"];

if ( _value ) then {
    memberOnly = true;
    TFAR_channel = (["tfar", "prvChannel"] call core_fnc_getSetting);
} else {
    memberOnly = false;
    TFAR_channel = (["tfar", "dftChannel"] call core_fnc_getSetting);    
};

publicVariable "memberOnly";
publicVariable "TFAR_channel";
if ( CTXT_SERVER ) then {
	publicVariableServer "memberOnly";
	publicVariableServer "TFAR_channel";    
};