/*
@filename: feats\memberOnly\playerInit.sqf
Author:
	Ben
Description:
	run on player,
	if server locked and player is not a member call endMission. 
*/

#include "_debug.hpp"

if ( memberOnly ) then {
	if !( [player] call memberData_fnc_isMember ) exitWith { endMission "NOTMEMBER"; };
};

private _checkFnc = {
	if !( memberOnly ) exitWith {};
	if !( [player] call memberData_fnc_isMember ) exitWith { endMission "NOTMEMBER"; };
};

if ( isNil "memberOnlyPVEH" ) then {
	memberOnlyPVEH = ["memberOnly", _checkFnc] call pveh_fnc_add;
};

if ( isNil "momdPVEH" ) then {
	momdPVEH = ["memberData", _checkFnc] call pveh_fnc_add;
};

private _cond = "( (CTXT_SERVER || ((call BIS_fnc_admin) > 0) || ((player getVariable['MD_rank', 0]) >= %1)) && %2 )";
private _rank = (["memberOnly", "rank"] call core_fnc_getSetting);

private _lock = (["memberOnly", "lock"] call core_fnc_getSetting);
private _lockCond = format[_cond, _rank, "!memberOnly"];
private _unlock = (["memberOnly", "unlock"] call core_fnc_getSetting);
private _unlockCond = format[_cond, _rank, "memberOnly"];

{
	{
		_x params ["_thing", "_actions"];
		{
			if ( (configName _x) isEqualTo "lockServer" ) then {
                #ifdef DEBUG
                private _debug = format["adding action to %1", _thing];
                debug(LL_DEBUG, _debug);
                #endif
				_thing addAction [_lock, {_this call memberOnly_fnc_setLock}, true, 0, false, true, '', _lockCond, 4];
                _thing addAction [_unlock, {_this call memberOnly_fnc_setLock}, false, 0, false, true, '', _unlockCond, 4];
			};
		} forEach _actions;
	} forEach _x;
} forEach [BA_veh, BA_npc, BA_obj];

nil