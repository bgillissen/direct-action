/*
@filename: feats\supportCrate\playerInit.sqf
Author:
	Ben
Description:
	run on player,
	add action to base things with a support action,
Params:
	none
Environment:
	missionConfig:
		supportCrate >> types
		supportCrate >> actions >> fuel
		supportCrate >> actions >> medic
		supportCrate >> actions >> repair
		supportCrate >> actions >> supply
Return:
	nothing
*/

#include "_debug.hpp"

if ( (["supportCrate"] call core_fnc_getParam) == 0 ) exitWith {
    #ifdef DEBUG
    debug(LL_INFO, "disabled by mission parameter");
    #endif
    nil 
};

private _types = (["supportCrate", "types"] call core_fnc_getSetting);

{
	{
		_x params ["_thing", "_actions"];
		{
			if ( (configName _x) isEqualTo "support" ) then {
                #ifdef DEBUG
				private _debug = format["adding action to %1", _thing];
				debug(LL_DEBUG, _debug);
				#endif
				{
					private _action = ["supportCrate", "actions", _x] call core_fnc_getSetting;
					_thing addAction [_action, {call supportCrate_fnc_spawn}, _x, 0, false, true, '', 'call supportCrate_fnc_condition', 4];
				} forEach _types;
			};
		} forEach _actions;
	} forEach _x;
} count [BA_veh, BA_npc, BA_obj];

nil