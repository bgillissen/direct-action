/*
@filename: feats\supplyDrop\serverDestroy.sqf
Author:
	Ben
Description:
	run on server side
	kill the supply cooldown thread 
*/

if ( count(_this) == 0 ) exitWith { nil };

if ( (["supportCrate"] call core_fnc_getParam) == 0 ) exitWith { nil };

params ["_when", "_thread"];

terminate _thread;

waitUntil{ scriptDone _thread };

SC_avail = false;
publicVariable "SC_avail";

{ deleteVehicle _x; } count SC_spawnedCrates;
SC_spawnedCrates = [];
publicVariable "SC_spawnedCrates";

nil