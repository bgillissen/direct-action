/*
@filename: feats\supportCrate\serverInitThread.sqf
Author:
	Ben
Description:
	this run on server,
	must be spawn, not called,
	a loop that act as a coolDown for supportCrate
Params:
	none
Environment:
	missionParameters:
		supportCrate
		supportCrate_cooldown
		supportCrate_maxcrate
	missionNamespace:
		SC_avail
		SC_crates
	missionConfig:
		supportCrate >> msgFrom
		supportCrate >> msgAvail
		supportCrate >> checkDelay
Return:
	nothing
*/

#include "_debug.hpp"

if ( (["supportCrate"] call core_fnc_getParam) == 0 ) exitWith {
    #ifdef DEBUG
    debug(LL_INFO, "disabled by mission parameter");
    #endif
    nil 
};

private _cooldown = ["supportCrate_cooldown"] call core_fnc_getParam;
private _maxCrate = ["supportCrate_maxcrate"] call core_fnc_getParam;
private _from =  ["supportCrate", "msgFrom"] call core_fnc_getSetting;
private _msg =  ["supportCrate", "msgAvail"] call core_fnc_getSetting;
private _checkDelay =  ["supportCrate", "checkDelay"] call core_fnc_getSetting;

SC_avail = true;
publicVariable "SC_avail";
SC_spawnedCrates = [];
publicVariable "SC_spawnedCrates";

#ifdef DEBUG
private _debug = format["thread as started with %1s delay, %2s cooldown, 3% crates maximum", _checkDelay, _cooldown, _maxcrate];
debug(LL_DEBUG, _debug);
#endif

while { true } do {
	if ( !SC_avail ) then {
		sleep _cooldown;
		if ( (count SC_crates) > _maxCrate ) then {
            #ifdef DEBUG
			debug(LL_DEBUG, "maximum crates reached, removing the first one spawned");
			#endif
			deleteVehicle (SC_spawnedCrates select 0);
			SC_spawnedCrates deleteAt 0;
			publicVariable "SC_spawnedCrates";
		};
		[1, _msg, [_from, PLAYER_SIDE]] call global_fnc_chat;
		SC_avail = true;
		publicVariable "SC_avail";
	};
	sleep _checkDelay;
};

nil