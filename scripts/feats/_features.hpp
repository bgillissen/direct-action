/*
@filename: feats\_features.hpp
Author:
	Ben
Description:
	on all context,
	included by feats\main.hpp,
	include class for feature's events template class
	declare features class and include features definitions
*/

#include "_templates.hpp"

class features {
	#ifdef use_anthems
	#include "anthems\_feature.hpp"
	#endif

	#ifdef use_artiComputer
	#include "artiComputer\_feature.hpp"
	#endif

	#ifdef use_artiSupport
	#include "artiSupport\_feature.hpp"
	#endif

	#include "assets\_feature.hpp"

	#ifdef use_backpackOnChest
	#include "backpackOnChest\_feature.hpp"
	#endif

	#include "baseAtmosphere\_feature.hpp"

	#ifdef use_baseDefence
	#include "baseDefence\_feature.hpp"
	#endif

	#ifdef use_baseLight
	#include "baseLight\_feature.hpp"
	#endif

	#ifdef use_baseProtection
	#include "baseProtection\_feature.hpp"
	#endif

	#ifdef use_baseVehicle
	#include "baseVehicle\_feature.hpp"
	#endif

	#ifdef use_billboards
	#include "billboards\_feature.hpp"
	#endif

	#ifdef use_checkTS
	#include "checkTS\_feature.hpp"
	#endif

	#ifdef use_cleanUp
	#include "cleanUp\_feature.hpp"
	#endif

	#include "dataLink\_feature.hpp"

	#ifdef use_curator
	#include "curator\_feature.hpp"
	#endif

	#ifdef use_deathMessage
	#include "deathMessage\_feature.hpp"
	#endif

	#include "dynSim\_feature.hpp"

	#ifdef use_earPlugs
	#include "earPlugs\_feature.hpp"
	#endif

	#ifdef use_environment
	#include "environment\_feature.hpp"
	#endif

	#ifdef use_fatigue
	#include "fatigue\_feature.hpp"
	#endif

	#ifdef use_flags
	#include "flags\_feature.hpp"
	#endif

	#ifdef use_flares
	#include "flares\_feature.hpp"
	#endif

	#ifdef use_gearRestrictions
	#include "gearRestrictions\_feature.hpp"
	#endif

	#ifdef use_gearSave
	#include "gearSave\_feature.hpp"
	#endif

	#include "groups\_feature.hpp"
	#include "hostage\_feature.hpp"

	#ifdef use_hallOfShame
	#include "hallOfShame\_feature.hpp"
	#endif

	#ifdef use_ia
	#include "ia\_feature.hpp"
	#endif

	#ifdef use_lectures
	#include "lectures\_feature.hpp"
	#endif

	#ifdef use_loadBalance
	#include "loadBalance\_feature.hpp"
	#endif

	#ifdef use_mapTracker
	#include "mapTracker\_feature.hpp"
	#endif

	#include "memberData\_feature.hpp"

	#ifdef use_memberOnly
	#include "memberOnly\_feature.hpp"
	#endif

	#ifdef use_nightVision
	#include "nightVision\_feature.hpp"
	#endif

	#ifdef use_noCallout
	#include "noCallout\_feature.hpp"
	#endif

	#ifdef use_noRenegade
	#include "noRenegade\_feature.hpp"
	#endif

	#include "nutsKick\_feature.hpp"

	#ifdef use_oneLifeOnly
	#include "oneLifeOnly\_feature.hpp"
	#endif

	#include "playerSide\_feature.hpp"
	#include "playerSpawn\_feature.hpp"

	#ifdef use_playerTrait
	#include "playerTrait\_feature.hpp"
	#endif

	#ifdef use_radioFreq
	#include "radioFreq\_feature.hpp"
	#endif

	#ifdef use_revive
	#include "revive\_feature.hpp"
	#endif

	#ifdef use_roleRestrictions
	#include "roleRestrictions\_feature.hpp"
	#endif

	#include "tasks\_feature.hpp"

	#ifdef use_serverRules
	#include "serverRules\_feature.hpp"
	#endif

	#ifdef use_squadHint
	#include "squadHint\_feature.hpp"
	#endif

	#ifdef use_startTime
	#include "startTime\_feature.hpp"
	#endif

	#ifdef use_stateMonitor
	#include "stateMonitor\_feature.hpp"
	#endif

	#ifdef use_supplyDrop
	#include "supplyDrop\_feature.hpp"
	#endif

	#ifdef use_supportCrate
	#include "supportCrate\_feature.hpp"
	#endif

	#include "uavFix\_feature.hpp"

	#ifdef use_uavRecon
	#include "uavRecon\_feature.hpp"
	#endif

	#ifdef use_va
	#include "va\_feature.hpp"
	#endif

	#ifdef use_vcomai
	#include "vcomai\_feature.hpp"
	#endif

	#ifdef use_vehicleCrew
	#include "vehicleCrew\_feature.hpp"
	#endif
	
	#ifdef use_vehicleDeco
	#include "vehicleDeco\_feature.hpp"
	#endif
	
	#ifdef use_vehicleLoadout
	#include "vehicleLoadout\_feature.hpp"
	#endif

	#ifdef use_vehicleRepair
	#include "vehicleRepair\_feature.hpp"
	#endif

	#ifdef use_vehicleRespawn
	#include "vehicleRespawn\_feature.hpp"
	#endif

	#ifdef use_vehicleRestrictions
	#include "vehicleRestrictions\_feature.hpp"
	#endif

	#ifdef use_vehicleSelection
	#include "vehicleSelection\_feature.hpp"
	#endif

	#ifdef use_vehicleWatch
	#include "vehicleWatch\_feature.hpp"
	#endif

	#ifdef use_viewDistance
	#include "viewDistance\_feature.hpp"
	#endif

	#ifdef use_voiceControl
	#include "voiceControl\_feature.hpp"
	#endif

	#ifdef use_vonHint
	#include "vonHint\_feature.hpp"
	#endif

	#ifdef use_weaponHolster
	#include "weaponHolster\_feature.hpp"
	#endif

	#ifdef use_zeusCompo
	#include "zeusCompo\_feature.hpp"
	#endif

	#ifdef use_zeusMission
	#include "zeusMission\_feature.hpp"
	#endif
};
