
if !( CTXT_SERVER ) exitWith {
	_this remoteExec ["groups_fnc_setLeader", 2, false];    
};

params ["_group", "_player"];

_this remoteExec ["groups_fnc_setLeaderOwner", (groupOwner _group), false];