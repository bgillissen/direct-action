/*
@filename: feats\groups\getColor.sqf
Author:
	Ben
Description:
	run on player,
	returns player's team color, "WHITE" if not defined or objNull is given 
*/

#include "_debug.hpp"

if ( isNull _this ) exitWith { "WHITE" };

private _dftColor = _this getVariable ["color", "WHITE"];

(_this getVariable ["activeColor", _dftColor])