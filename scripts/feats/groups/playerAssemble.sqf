/*
@filename: feats\groups\playerAssemble.sqf
Author:
	Ben
Description:
	run on player when assemble a weapon,
	rename uav group,
*/

#include "_debug.hpp"

params ["_unit", "_veh"];

if !( unitIsUAV _veh ) exitWith { nil };

(group (driver _veh)) setGroupIdGlobal [([(format["uav - %1", (name _unit)]), allGroups, {groupId _this}] call common_fnc_getUniqueName)];

nil