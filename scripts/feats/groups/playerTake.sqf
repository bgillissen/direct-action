/*
@filename: feats\groups\playerTake.sqf
Author:
	Ben
Description:
	run on player,
	reassign squad patch to the new player's uniform
*/

#include "_debug.hpp"

params ["_unit", "_container", "_item"];

if !( getNumber(configFile >> "cfgWeapons" >> _item >> "itemInfo" >> "type") isEqualTo 801 ) exitWith {
    #ifdef DEBUG
	debug(LL_DEBUG, "not an uniform, abording");
    #endif
    nil
};

[player, ((group player) getVariable ['insignia', ''])] call global_fnc_setUnitInsignia;

nil