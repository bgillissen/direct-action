/*
@filename: feats\groups\serverLeave.sqf
Author:
	Ben
Description:
	run on server,
*/

params ["_unit", "_id", "_uid", "_name"];

call groups_fnc_create;

[_uid] call commandChain_fnc_unAssign;

call commandChain_fnc_cleanup;