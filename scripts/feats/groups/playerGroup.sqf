
#include "ui\define.hpp"

params ["_player", "_old", "_new"];

if ( _player isEqualTo player ) then {
    [player, (_new getVariable['insignia', ''])] call global_fnc_setUnitInsignia;
    [player, _old] call commandChain_fnc_unAssign;
    if ( (leader _new) isEqualTo player ) then {
    	private _chain = _new getVariable['commandChain', []];
        if ( (count _chain) isEqualTo 0 ) then {
    	    [objNull, player, 0, false, false] call commandChain_fnc_assign;
		};
    };   
};

disableSerialization;

private _dsp = findDisplay GRP_IDD;
 
if ( isNull _dsp ) exitWith { nil };

[_dsp] spawn groups_fnc_update;

nil