#include "ui\define.hpp"

class groups {
	idd = GRP_IDD;
	name = "groups";
	scriptName = "groups";
	movingEnable = false;
	enableSimulation = true;
	onLoad = "_this spawn groups_fnc_init";
	onUnload = "_this spawn groups_fnc_destroy";
	class controlsBackground {
		class title : daTitle {
			text = "Group Manager";
			x = 0.5 - (TOT_WIDTH / 2);
			y = 0.5 - (TOT_HEIGHT / 2) - YOFFSET;
			h = LINE_HEIGHT;
			w = TOT_WIDTH;
		};
		class background : daContainer {
			x = 0.5 - (TOT_WIDTH / 2);
			y = 0.5 - (TOT_HEIGHT / 2) + LINE_HEIGHT + SPACE - YOFFSET;
			h = TOT_HEIGHT - (2 * LINE_HEIGHT) - (2 * SPACE);
			w = TOT_WIDTH;
		};
	};
	class Controls {
		class totPlayer : daText {
			idc = TOTPLR_IDC;
			style = ST_RIGHT;
			x = 0.5;
			y = 0.5 - (TOT_HEIGHT / 2) - YOFFSET;
			h = LINE_HEIGHT;
			w = (TOT_WIDTH / 2);
		};
		class close : daTxtButton {
			x = 0.5 - (TOT_WIDTH / 2);
			y = 0.5 + (TOT_HEIGHT / 2) - LINE_HEIGHT - YOFFSET;
			w = BUTTON_WIDTH;
			h = LINE_HEIGHT;
			text = "Close";
			action = "closeDialog 0;";
		};
		class grpList : daTree {
			idc = GTREE_IDC;
			x = 0.5 - (TOT_WIDTH / 2);
			y = 0.5 - (TOT_HEIGHT / 2) + LINE_HEIGHT + SPACE - YOFFSET;
			h = TOT_HEIGHT - (4 * LINE_HEIGHT) - (2 * SPACE);
			w = (TOT_WIDTH / 2);
			onTreeCollapsed = "[(_this select 0), (_this select 1), false] call groups_fnc_evtToggleGroup;";
			onTreeExpanded = "[(_this select 0), (_this select 1), true] call groups_fnc_evtToggleGroup;";
			onTreeSelChanged = "_this call groups_fnc_evtSelectGroup;";
		};
		class join : daTxtButton {
			idc = JOIN_IDC;
			x = 0.5 - (TOT_WIDTH / 2);
			y = 0.5 + (TOT_HEIGHT / 2) - (3 * LINE_HEIGHT) - SPACE - YOFFSET;
			h = (2 * LINE_HEIGHT);
			w = (TOT_WIDTH / 2);
			sizeEx = (FONT_sizeEx * 2);
			action = "[] spawn groups_fnc_evtJoin;";
		};
		class details : daControlGroup {
			idc = DETAIL_IDC;
			x = 0.5;
			y = 0.5 - (TOT_HEIGHT / 2) + LINE_HEIGHT + SPACE - YOFFSET;
			h = (LINE_HEIGHT * 2);
			w = (TOT_WIDTH / 2);
			class Controls {
				class icon : daPicture {
					idc = DETAIL_ICON;
					style = 48 + 2048;
					h = (LINE_HEIGHT * 2);
					w = LINE_HEIGHT * 2);
				};
				class name : daText {
					idc = DETAIL_NAME;
					style = ST_CENTER;
					SizeEx = (FONT_sizeEx * 2);
					x = (LINE_HEIGHT * 2);
					h = (LINE_HEIGHT * 2);
					w = (TOT_WIDTH / 2) - (LINE_HEIGHT * 4);

				};
				class side : daText {
					idc = DETAIL_SIDE;
					style = ST_CENTER;
					SizeEx = (FONT_sizeEx * 2);
					x = (TOT_WIDTH / 2) - (LINE_HEIGHT * 2);
					h = (LINE_HEIGHT * 2);
					w = (LINE_HEIGHT * 2);
				};
			};
		};
		class mbrList : daControlGroup {
			idc = MLST_IDC;
			x = 0.5;
			y = 0.5 - (TOT_HEIGHT / 2) + (3 * LINE_HEIGHT) + SPACE - YOFFSET;
			h = TOT_HEIGHT - (6 * LINE_HEIGHT) - (2 * SPACE);
			w = (TOT_WIDTH / 2);
		};
		class moveTo : daTxtButton {
			idc = MOVETO_IDC;
			style = ST_CENTER;
			x = 0.5;
			y = 0.5 + (TOT_HEIGHT / 2) - (3 * LINE_HEIGHT) - SPACE - YOFFSET;
			h = (2 * LINE_HEIGHT);
			w = (TOT_WIDTH / 2) * (2 / 3);
			sizeEx = (FONT_sizeEx * 2);
			action = "[] spawn groups_fnc_evtMoveTo;";
		};
		class promote : daTxtButton {
			idc = PROMOTE_IDC;
			style = ST_CENTER;
			x = 0.5 + ((TOT_WIDTH / 2) * (2 / 3));
			y = 0.5 + (TOT_HEIGHT / 2) - (3 * LINE_HEIGHT) - SPACE - YOFFSET;
			h = (2 * LINE_HEIGHT);
			w = (TOT_WIDTH / 2) * (1 / 3);
			text = "Promote";
			sizeEx = (FONT_sizeEx * 2);
			action = "[] spawn groups_fnc_evtPromote;";
		};
	};
};
