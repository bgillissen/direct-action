/*
@filename: feats\groups\playerRespawn.sqf
Author:
	Ben
Description:
	run on player,
	export init color to other players if this is the first spawn,
	make player rejoin the group he had on death,
	reassign squad patch to the new player's unit
*/

#include "_debug.hpp"

private ["_grp", "_color"];

if !( isNil "GMlastGroup" ) then {
    _grp = GMlastGroup;
    GMlastGroup = nil;
} else {
    _grp = (group player);
};

if !( isNil "GMinitColor" ) then {
    player setVariable ['activeColor', GMinitColor, true];
    GMinitColor = nil;
};

private _old = (group player);

if !( _grp isEqualTo _old ) then { 
	[player] joinSilent _grp;
    ["PLAYER", 'group', [player, _old, _grp]] call core_fnc_featEventPropagate;
	["SERVER", 'group', [player, _old, _grp]] call core_fnc_featEventPropagate;
	["HEADLESS", 'group', [player, _old, _grp]] call core_fnc_featEventPropagate; 
} else {
    [player, (_grp getVariable['insignia', ''])] call global_fnc_setUnitInsignia;
};

nil