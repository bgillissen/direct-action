/*
@filename: feats\groups\playerCloseVA.sqf
Author:
	Ben
Description:
	run on player,
	reassign squad patch to the new player's uniform
*/

#include "_debug.hpp"

[player, ((group player) getVariable ['insignia', ''])] call global_fnc_setUnitInsignia;

nil