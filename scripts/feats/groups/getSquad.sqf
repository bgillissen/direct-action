/*
@filename: feats\groups\getSquad.sqf
Author:
	Ben
Description:
	run on player,
	return player's squads name if it is one of our created squad,
	empty string otherwise
*/

private _grp = group _this;
private _squad = ""; 
{
    private _s = missionNamespace getVariable format["SQUAD_%1", _x];
    if ( _s isEqualTo _grp ) then { _squad = _x; };
    true
} count ("true" configClasses (missionConfigFile >> "settings" >> "groups" >> "squads") apply { configName _x });

_squad