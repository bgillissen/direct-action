

private _broadcast = false;
{
    if ( isNull (SQUADS select _forEachIndex) ) then {
        _broadcast = true;
        private _id = configName _x;
		private _name = getText (_x >> "name");
    	private _insignia = getText (_x >> "insignia");
		private _group = createGroup PLAYER_SIDE;
		_group setGroupIdGlobal [_name];
    	_group setVariable ['insignia', _insignia, true];
    	_group setVariable ['id', _id, true];
        _group setVariable ['NOCLEANUP', true, true];
		missionNamespace setVariable [format["SQUAD_%1", _id], _group, true];
		SQUADS set [_forEachIndex, _group];
	};
} forEach ("true" configClasses (missionConfigFile >> "settings" >> "groups" >> "squads"));

if ( _broadcast ) then { 
	publicVariable "SQUADS";
    if ( CTXT_PLAYER && CTXT_SERVER ) then { publicVariableServer "SQUADS"; }; 
};