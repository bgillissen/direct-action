/*
@filename: feats\groups\serverDestroy.sqf
Author:
	Ben
Description:
	run on server,
	remove created groups
*/

{ deleteGroup _x; } forEach SQUADS;

SQUADS = [];

nil