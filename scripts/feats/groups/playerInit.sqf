/*
@filename: feats\groups\playerInit.sqf
Author:
	Ben
Description:
	run on player,
	define global var so respawn event will make player join the squad defined by his slot (unit init),
	define global var so respawn event will assign team color to player as defined by his slot (unit init),
*/

#include "_debug.hpp"
#include "ui\define.hpp"

player setVariable ["role", (player getVariable "role"), true];

PREINIT_GRP = group player;

call groups_fnc_create;

private _id = player getVariable "groupID";

GMlastGroup =(missionNamespace getVariable [format["SQUAD_%1", _id], grpNull]);

GMinitColor = player getVariable ["color", "WHITE"];
if ( GMinitColor isEqualTo 'MAIN' ) then { GMinitColor = 'WHITE'; };

private _fnc = {
    if ( isNull (findDisplay GRP_IDD) ) then { 
    	createDialog "groups"; 
	} else {
        closeDialog 0;
    };
};

["groups", "Open Group Manager", _fnc] call keybind_fnc_add;

nil