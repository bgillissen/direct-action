/*
@filename: feats\groups\playerLeave.sqf
Author:
	Ben
Description:
	run on server,
*/

#include "ui\define.hpp"

params ["_unit", "_id", "_uid", "_name"];

disableSerialization;

private _dsp = findDisplay GRP_IDD;
 
if ( isNull _dsp ) exitWith { nil };

[_dsp] spawn groups_fnc_update;

nil