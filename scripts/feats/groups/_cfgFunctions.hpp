class groups {
	tag = "groups";
	class functions {
		class playerAssemble { file="feats\groups\playerAssemble.sqf"; };
		class playerCloseVA { file="feats\groups\playerCloseVA.sqf"; };
		class playerDestroy { file="feats\groups\playerDestroy.sqf"; };
		class playerGroup { file="feats\groups\playerGroup.sqf"; };
		class playerInit { file="feats\groups\playerInit.sqf"; };
		class playerKilled { file="feats\groups\playerKilled.sqf"; };
		class playerLeave { file="feats\groups\playerLeave.sqf"; };
		class playerRespawn { file="feats\groups\playerRespawn.sqf"; };
		class playerTake { file="feats\groups\playerTake.sqf"; };

		class serverDestroy { file="feats\groups\serverDestroy.sqf"; };
		class serverLeave { file="feats\groups\serverLeave.sqf"; };
		class serverPreInit { file="feats\groups\serverPreInit.sqf"; };

		class create { file="feats\groups\create.sqf"; };
		class getColor { file="feats\groups\getColor.sqf"; };
		class getSquad { file="feats\groups\getSquad.sqf"; };
		class setLeader { file="feats\groups\setLeader.sqf"; };
		class setLeaderOwner { file="feats\groups\setLeaderOwner.sqf"; };

		class init { file="feats\groups\ui\init.sqf"; };
		class update { file="feats\groups\ui\update.sqf"; };
		class destroy { file="feats\groups\ui\destroy.sqf"; };

		class evtAssign { file="feats\groups\ui\events\assign.sqf"; };
		class evtColor { file="feats\groups\ui\events\color.sqf"; };
		class evtJoin { file="feats\groups\ui\events\join.sqf"; };
		class evtKeybind { file="feats\groups\ui\events\keybind.sqf"; };
		class evtMoveTo { file="feats\groups\ui\events\moveTo.sqf"; };
		class evtPromote { file="feats\groups\ui\events\promote.sqf"; };
		class evtSelectGroup { file="feats\groups\ui\events\selectGroup.sqf"; };
		class evtSelectPlayer { file="feats\groups\ui\events\selectPlayer.sqf"; };
		class evtToggleGroup { file="feats\groups\ui\events\toggleGroup.sqf"; };
	};
};
