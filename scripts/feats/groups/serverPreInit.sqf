/*
@filename: feats\groups\serverPreInit.sqf
Author:
	Ben
Description:
	run on server,
	create player's groups
*/

#include "_debug.hpp"

SQUADS = [];
{
	SQUADS pushback grpNull;
} forEach ("true" configClasses (missionConfigFile >> "settings" >> "groups" >> "squads"));

call groups_fnc_create;

#ifdef DEBUG
private _debug = format["%1 group(s) created for player side (%2)", (count SQUADS), PLAYER_SIDE];
debug(LL_DEBUG, _debug);
#endif

nil