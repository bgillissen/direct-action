class groups : feat_base {
	class server : featServer_base {
		class destroy : featEvent_enable {};
		class leave : featEvent_enable {};
		class preInit : featEvent_enable { order = 2; };
	};
	class player : featPlayer_base {
		class assemble : featEvent_enable {};
		class closeVA : featEvent_enable {};
		class destroy : featEvent_enable {};
		class group : featEvent_enable {};
		class Init : featEvent_enable { order = 10; };
		class killed : featEvent_enable {};
		class leave : featEvent_enable {};
		class respawn : featEvent_enable { order=98; };
		class take : featEvent_enable {};
	};
};
