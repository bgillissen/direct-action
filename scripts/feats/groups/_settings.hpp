
#include "..\..\common\_dik.hpp"

class groups {
	keycode[] = {DIK_U, 0, 0 ,0};//u
	class colors {
		class WHITE {
			html = "#ffffff";
			config[] = {0.7, 0.7, 0.7, 1};
		};
		class RED {
			html = "#ff0000";
			config[] = {0.9, 0, 0, 1};
		};
		class GREEN {
			html = "#00ff00";
			config[] = {0, 0.8, 0, 1};
		};
		class BLUE {
			html = "#0000ff";
			config[] = {0.2 ,0.2 ,1, 1};
		};
		class YELLOW {
			html = "#ffff00";
			config[] = {0.85, 0.85, 0, 1};
		};
	};
	class squads {
		class HQ {
			name = "HQ";
			insignia = "TFUDA_HQ";
		};
		class ALPHA {
			name = "Alpha";
			insignia = "TFUDA_ALPHA";
		};
		class BRAVO {
			name = "Bravo";
			insignia = "TFUDA_BRAVO";
		};
		class CHARLIE {
			name = "Charlie";
			insignia = "TFUDA_CHARLIE";
		};
		class HORNET {
			name = "Eagle";
			insignia = "TFUDA_HORNET";
		};
		class HAMMER {
			name = "Hammer";
			insignia = "TFUDA_HAMMER";
		};
		class SUPPORT {
			name = "Support";
			insignia = "TFUDA_SUPPORT";
		};
	};
	class tooltips {
		promote = "Make %1 the new actual";
		assign = "Assign %1 as %2 in command";
		moveTo = "Move selected players to %1";
		color = "Change %1 team color";
	};
	class slots {
		cc_0 = "1st";
		cc_1 = "2nd";
		cc_2 = "3rd";
		cc_4 = "4th";
		cc_5 = "5th";
	};
	class messages {
		actual = "%1 is the new actual";
		assign = "%1 has been assigned as %2 in command by %3";
		give = "%1 (%2 in command) is in charge";
		take = "%1 (%2 in command) has taken leadership";
	};
};
