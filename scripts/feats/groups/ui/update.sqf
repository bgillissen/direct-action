
#include "define.hpp"

if !( CTXT_PLAYER ) exitWith {};

disableSerialization;

params [['_dsp', displayNull]];

if ( isNull _dsp ) then { _dsp = (findDisplay GRP_IDD); };
if ( isNull _dsp ) exitWith {};

if ( groups_uiQ ) exitWith {};

if ( groups_uiNoEvents ) then {
    groups_uiQ = true;
    waitUntil { !groups_uiNoEvents };
	groups_uiQ = false;
};

groups_uiNoEvents = true;

_grpTree = (_dsp displayCtrl GTREE_IDC);
tvClear _grpTree;

private _totPlayer = 0;
{
    private _grp = _x;
    private _grpIdx = _forEachIndex;
    private _insignia = _grp getVariable ['insignia', ''];
    private _grpPath = [_grpIdx];
    private _grpName = (groupId _grp);
    //private _unitCount = ({ isPlayer _x } count (units _grp));
    //if ( _unitCount > 0 ) then { _grpName = format["%1 - %2", _grpName, _unitcount]; };
    _grpTree tvAdd [[], _grpName];
    _grpTree tvSetValue [_grpPath, _grpIdx];
    if !( _insignia isEqualTo '' ) then {
    	_grpTree tvSetPicture [_grpPath, getText (configFile >> "CfgUnitInsignia" >> _insignia >> "texture")];
    };
    if ( _grp in groups_uiGroups ) then { 
		_grpTree tvExpand _grpPath;
	} else {
        _grpTree tvCollapse _grpPath;
    };
    if ( (_grp getVariable ["cPVEH", -1]) < 0 ) then {
        private _pveh = ['commandChain', {[] spawn groups_fnc_update}, _grp] call pveh_fnc_add;
		_grp setVariable ["cPVEH", _pveh];
    };
    private _c = 0;
    private _leader = leader _grp;
	{
        if ( isPlayer _x ) then {
            if ( (_x getVariable ["colorPVEH", -1]) < 0 ) then {
                private _pveh = ['activeColor', {[] spawn groups_fnc_update}, _x] call pveh_fnc_add;
                _x setVariable ["colorPVEH", _pveh];
    		};
            private _unitPath = [_grpIdx, _c];
            private _name = _x getVariable ['MD_name', (name _x)];
            private _rank = _x getVariable ['MD_rank', 0];
            private _color = ['groups', 'colors', (_x call groups_fnc_getColor), 'config'] call core_fnc_getSetting;
            _grpTree tvAdd [_grpPath, _name];
            _grpTree tvSetValue [_unitPath, _grpIdx];
            private _texture = "a3\Ui_f\data\GUI\Cfg\loadingscreens\loadingstripe_ca.paa";
            if ( _x isEqualTo _leader ) then {
                _texture = "a3\Ui_f\data\GUI\Cfg\Ranks\general_gs.paa";
			} else {                
            	if ( _rank >= 3 ) then { _texture = format["feats\groups\media\rank%1.paa", _rank]; };
            };
            _grpTree tvSetPictureRight [_unitPath, _texture];
            _grpTree tvSetPictureRightColor [_unitPath, _color];
            _c = _c + 1;
        };
    } forEach (units _grp);
    _totPlayer = _totPlayer + _c;    
} forEach SQUADS;

if (_totPlayer > 1 ) then {
	(_dsp displayCtrl TOTPLR_IDC) ctrlSetText format["%1 players online", _totPlayer];
} else {
	(_dsp displayCtrl TOTPLR_IDC) ctrlSetText format["%1 player online", _totPlayer];
};

if ( groups_uiGrp < 0 ) then { groups_uiGrp = 0; }; 
_grpTree tvSetCurSel [groups_uiGrp];
private _selGrp = (SQUADS select groups_uiGrp);
(_dsp displayCtrl JOIN_IDC) ctrlSetText format["Join %1", (groupId _selGrp)];
(_dsp displayCtrl JOIN_IDC) ctrlEnable !((group player) isEqualTo _selGrp);

private _grp = group player;
private _insignia = _grp getVariable ['insignia', ''];
private _detail = (_dsp displayCtrl DETAIL_IDC);
if !( _insignia isEqualTo '' ) then {
	(_detail controlsGroupCtrl DETAIL_ICON) ctrlSetText getText(configFile >> "cfgUnitInsignia" >> _insignia >> 'texture');
};
(_detail controlsGroupCtrl DETAIL_NAME) ctrlSetText (groupId _grp);
(_detail controlsGroupCtrl DETAIL_SIDE) ctrlSetText str ({isPlayer _x} count (units _grp));
(_detail controlsGroupCtrl DETAIL_SIDE) ctrlSetBackgroundColor ([(side _grp), false] call BIS_fnc_sideColor); 


private _mlst = (_dsp displayCtrl MLST_IDC);

//cleanup mbrList
private _idc = MBR_BASE;
while { !isNull (_mlst controlsGroupCtrl _idc) } do {
    ctrlDelete (_mlst controlsGroupCtrl _idc);
    _idc = _idc + 1;
};
  
private _grp = group player;
private _leader = leader _grp;
private _playerUid = (getPlayerUid player);
private _playerIsLeader = ( player isEqualTo _leader );
private _cc = (_grp getVariable['commandChain', []]);
private _playerIC = -1;
{
    _x params ['_cuid', '_isLeading'];
    if ( _cuid isEqualTo _playerUid ) exitWith { _playerIC = _forEachIndex; }
} forEach _cc;

private _colors = [];
{
    _colors pushback [(configName _x), getArray(_x >> 'config')];
} forEach ("true" configClasses (missionConfigFile >> "settings" >> "groups" >> "colors"));

private _idc = MBR_BASE;
{
    if ( isPlayer _x ) then {
        private _unit = _x;
        private _isPlayer = (_unit isEqualTo player); 
        private _isLeader = (_unit isEqualTo _leader);
        private _unitUid = getPlayerUid _unit;
        private _role = _unit getVariable "role";
		private _name = _unit getVariable ['MD_name', (name _unit)];
		private _rank = _unit getVariable ['MD_rank', 0];
		private _unitColor = (_unit call groups_fnc_getColor);
        
		private _entry = _dsp ctrlCreate ["grp_mbrEntry", _idc, _mlst];
        (_entry controlsGroupCtrl MBR_UUID) ctrlsetText _unitUid;
        (_entry controlsGroupCtrl MBR_NAME) ctrlsetText _name;
        if !( isNil "_role" ) then {
        	(_entry controlsGroupCtrl MBR_NAME) ctrlsetTooltip (["mapTracker", "roleDescriptions", (_unit getVariable "role")] call core_fnc_getSetting);    
        };
                
        //background
        if ( _unitUid in groups_uiPlayers ) then {
            (_entry controlsGroupCtrl MBR_BCG) ctrlSetBackgroundColor [(profilenamespace getvariable ['GUI_BCG_RGB_R',0.69]),
						  											   (profilenamespace getvariable ['GUI_BCG_RGB_G',0.75]),
					      											   (profilenamespace getvariable ['GUI_BCG_RGB_B',0.5]), 0.7];
        };
		//color
        if ( _isPlayer || _playerIsLeader || (_playerIC isEqualTo 0) ) then {
            private _ctrl = _dsp ctrlCreate ["grp_dynColor", MBR_COLOR, _entry];
            private _colIdx = -1;
            {
                _x params ['_colName', '_color'];
                if ( _colName isEqualTo _unitColor ) then { _colIdx = _forEachIndex; };
            	_ctrl lbAdd "";
                _ctrl lbSetPicture [_forEachIndex, "feats\groups\media\square.paa"];
                _ctrl lbSetPictureColor [_forEachIndex, _color];
                _ctrl lbSetPictureColorSelected [_forEachIndex, _color];  
                _ctrl lbSetData [_forEachIndex, _colName];
        	} forEach _colors;
            if ( _colIdx >= 0 ) then {
                _ctrl lbSetCurSel _colIdx;
            } else {
				_ctrl lbSetCurSel 0;
            };
            private _tt = getText(missionConfigFile >> "settings" >> "groups" >> "tooltips" >> "color");
            _ctrl ctrlSetTooltip format[_tt, _name];
        } else {
            private _ctrl = _dsp ctrlCreate ["grp_fixColor", MBR_COLOR, _entry];
            private _color = ['groups', 'colors', _unitColor, 'config'] call core_fnc_getSetting;
            if ( isNil '_color' ) then {
            	_color = getArray(missionConfigFile >> "settings" >> "groups" >> "colors" >> "WHITE" >> "config");
            };
            //_ctrl ctrlSetText "a3\Ui_f\data\GUI\Cfg\loadingscreens\loadingstripe_ca.paa";
            _ctrl ctrlSetBackgroundColor _color;
        };
        
        //rank icon
		(_entry controlsGroupCtrl MBR_RANKICON) ctrlShow false;
		private _pos = ctrlPosition  (_entry controlsGroupCtrl MBR_NAME);
        _pos set [0, (LINE_HEIGHT * 2)];
        (_entry controlsGroupCtrl MBR_NAME) ctrlSetPosition _pos;
		(_entry controlsGroupCtrl MBR_NAME) ctrlCommit 0;
        
        //command chain
        private _unitIC = -1;
		{
    		_x params ['_cuid', '_isLeading'];
    		if ( _cuid isEqualTo _unitUid ) exitWith { _unitIC = _forEachIndex; }
		} forEach _cc;
        private _canAssign = ( (_playerIC >= 0) && ((_playerIC < _unitIC) || (_unitIC < 0)) && !(_unitIC isEqualTo (_playerIC + 1)) );
        if ( !_isPlayer && _canAssign ) then {
            private _ctrl = _dsp ctrlCreate ["grp_icAssign", MBR_IC, _entry];
            private _slotName = getText(missionConfigFile >> "settings" >> "groups" >> "slots" >> format["cc_%1", (_playerIC + 1)]);
			_ctrl ctrlSetText format["set %1 IC", _slotName];
            private _tt = getText(missionConfigFile >> "settings" >> "groups" >> "tooltips" >> "assign");
            _ctrl ctrlSetTooltip format[_tt, _name, _slotName];
		} else {
            if ( _unitIC >= 0 ) then {
            	private _ctrl = _dsp ctrlCreate ["grp_icText", MBR_IC, _entry];
                private _slotName = getText(missionConfigFile >> "settings" >> "groups" >> "slots" >> format["cc_%1", _unitIC]);
            	_ctrl ctrlSetText  format["%1 IC", _slotName];
			};
		};
        
        private _pos = ctrlPosition _entry;
        _pos set [1, (LINE_HEIGHT * (_idc - MBR_BASE))];
        _entry ctrlSetPosition _pos;
        _entry ctrlCommit 0;
        
        _idc = _idc + 1;
    };
} forEach (units _grp);

//promote button
private _canPromote = false;
if ( (count groups_uiPlayers) isEqualTo 1 ) then {
    _canPromote = !(_playerUid in groups_uiPlayers);
    if ( _canPromote ) then {
		private _unitIC = -1;
		private _unitUid = (groups_uiPlayers select 0); //getplayerUid (groups_uiPlayers select 0);
		{
    		_x params ['_cuid', '_isLeading'];
    		if ( _cuid isEqualTo _unitUid ) exitWith { _unitIC = _forEachIndex; }
		} forEach _cc;
	    _canPromote = ( (_playerIC isEqualTo 0) || (_playerIsLeader && ((_unitIC < _playerIC) || (_playerIC < 0))) );
	};
};

(_dsp displayCtrl PROMOTE_IDC) ctrlEnable _canPromote;
if ( _canPromote ) then {
 	private _tt = getText(missionConfigFile >> "settings" >> "groups" >> "tooltips" >> "promote");
    private _unit = ((groups_uiPlayers select 0) call common_fnc_playerByUid);
    private _name = (_unit getVariable ['MD_name', (name _unit)]);
	(_dsp displayCtrl PROMOTE_IDC) ctrlSetTooltip format[_tt, _name];
} else {
    (_dsp displayCtrl PROMOTE_IDC) ctrlSetTooltip '';
};


(_dsp displayCtrl MOVETO_IDC) ctrlSetText format["Move to %1", (groupId _selGrp)];
private _canMove = ( (count groups_uiPlayers) > 0 );
if ( _canMove ) then { _canMove = !(_selGrp isEqualTo _grp); };
if ( _canMove ) then { _canMove = ((_playerIC isEqualTo 0) || ((admin owner player) > 0) || ((player getVariable['MD_rank', 0]) >= 8) ); };
(_dsp displayCtrl MOVETO_IDC) ctrlEnable _canMove;    
if ( _canMove ) then {
 	private _tt = getText(missionConfigFile >> "settings" >> "groups" >> "tooltips" >> "moveTo");
	(_dsp displayCtrl MOVETO_IDC) ctrlSetTooltip format[_tt, (groupId _selGrp)];
} else {
    (_dsp displayCtrl MOVETO_IDC) ctrlSetTooltip '';
};

groups_uiNoEvents = false;
