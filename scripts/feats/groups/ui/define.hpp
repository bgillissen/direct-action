#define YOFFSET 0.1
#define SPACE 0.01
#define TOT_WIDTH 1.2
#define TOT_HEIGHT 1.1
#define LINE_HEIGHT (1 / 20)
#define BUTTON_WIDTH (6.25 / 40)


#define GRP_IDD 40000

#define TOTPLR_IDC 40001
#define GTREE_IDC 41000
#define JOIN_IDC 42000
#define MOVETO_IDC 45000
#define PROMOTE_IDC 46000

#define DETAIL_IDC 43000
#define DETAIL_ICON 43001
#define DETAIL_NAME 43002
#define DETAIL_SIDE 43003

#define MLST_IDC 44000
#define MBR_BASE 44100

#define MBR_BCG 44001
#define MBR_UUID 44002
#define MBR_COLOR 44003
#define MBR_RANKICON 44004
#define MBR_NAME 44005
#define MBR_IC 44006
