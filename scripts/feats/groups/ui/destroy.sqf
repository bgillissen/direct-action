
['SQUADS', groups_pveh] call pveh_fnc_del;

{
	private _pveh = (_x getVariable ["colorPVEH", -1]);
	if ( _pveh >= 0 ) then {
		['activeColor', _pveh, _x] call pveh_fnc_del;
    	_x setVariable ["colorPVEH", -1];
	};
} forEach allPlayers;

{
	private _pveh = (_x getVariable ["cPVEH", -1]);
	if ( _pveh >= 0 ) then {
		['commandChain', _pveh, _x] call pveh_fnc_del;
    	_x setVariable ["cPVEH", -1];
	};
} forEach SQUADS;