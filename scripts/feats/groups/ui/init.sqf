
#include "define.hpp"

disableSerialization;

groups_uiNoEvents = false;
groups_uiQ = false;
groups_uiGrp = (SQUADS find (group player));
groups_uiGroups = [group player];
groups_uiCTRL = false;
groups_uiPlayers = [];

waitUntil { !isNull (findDisplay GRP_IDD) };

groups_pveh = ['SQUADS', {[] spawn groups_fnc_update}] call pveh_fnc_add;

private _dsp = (findDisplay GRP_IDD);

_dsp displayAddEventHandler["keyDown", {[true, _this] call groups_fnc_evtKeybind}];
_dsp displayAddEventHandler["keyUp", {[false, _this] call groups_fnc_evtKeybind}];

if ( MOD_cba3 ) then {
	_dsp displayAddEventHandler ["keyDown", "_this call CBA_events_fnc_keyHandlerDown"];
	_dsp displayAddEventHandler ["keyUp", "_this call CBA_events_fnc_keyHandlerUp"];    
} else {
    _dsp displayAddEventHandler ["KeyDown", "[true, _this] call keybind_fnc_event;"];
	_dsp displayAddEventHandler ["KeyUp", "[false, _this] call keybind_fnc_event;"];
};

[_dsp] spawn groups_fnc_update;