
#include "..\..\..\..\common\_dik.hpp"

disableSerialization;

params ["_isDown", "_event"];

if ( _isDown && groups_uiCTRL ) exitWith {};

_event params ["_control", "_keyCode", "_shift", "_ctrl", "_alt"];

if ( _keyCode in [DIK_RCONTROL, DIK_LCONTROL] ) then { 
	groups_uiCTRL = _isDown;
};

false 