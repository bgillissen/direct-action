#include "..\define.hpp"

if ( groups_uiNoEvents ) exitWith {};

disableSerialization;

params ["_ctrl", "_idx"];

private _color = _ctrl lbData _idx;
private _entry = ctrlParentControlsGroup _ctrl;
private _unitUid = ctrlText (_entry controlsGroupCtrl MBR_UUID);
private _unit = _unitUid call common_fnc_playerByUid;

if ( isNull _unit ) exitWith {};

_unit setVariable ['activeColor', _color, true];

[] spawn groups_fnc_update;
