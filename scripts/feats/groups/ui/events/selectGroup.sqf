
if ( groups_uiNoEvents ) exitWith {};

disableSerialization;

params ['_ctrl', '_grpPath'];

groups_uiGrp = (_grpPath select 0);

[] spawn groups_fnc_update;