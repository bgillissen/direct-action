
if ( groups_uiNoEvents ) exitWith {};

disableSerialization;

params ['_ctrl', '_grpPath', '_expanded'];

private _grp = (SQUADS select (_grpPath select 0));

if ( _expanded ) then {
    groups_uiGroups pushbackUnique _grp;
} else {
    groups_uiGroups = groups_uiGroups - [_grp];
};
