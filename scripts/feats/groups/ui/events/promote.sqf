
private _unit = objNull;
{
    if ( (getPlayerUid _x) isEqualTo (groups_uiPlayers select 0)) then { _unit = _x; };
} forEach allPlayers;

if ( isNull _unit ) exitWith {};

[player, _unit, 0, false] call commandChain_fnc_assign;

private _grp = (group player);

[_grp, _unit] call groups_fnc_setLeader;

waitUntil { ( ((leader _grp) isEqualTo _unit) || (isNull _unit) ) };

publicVariable "SQUADS";

[] spawn groups_fnc_update;

false