
private _players = [];
{
    if ( (getPlayerUid _x) in groups_uiPlayers ) then { _players pushback _x; };
} forEach allPlayers;

if ( (count _players) isEqualTo 0 ) exitWith {};

private _old = group player;
private _new = SQUADS select groups_uiGrp;

if ( _old isEqualTo _new ) exitWith {};

_players joinSilent _new;

private _waitFor = _players + [];
waitUntil {
     
	{
        if ( isNull _x ) then { 
        	_waitFor deleteAt _forEachIndex; 
		} else {
            if ( ( (group _x) isEqualTo _new ) ) then {
          		["PLAYER", 'group', [_x, _old, _new], true] call core_fnc_featEventPropagate;
				["SERVER", 'group', [_x, _old, _new]] call core_fnc_featEventPropagate;
				["HEADLESS", 'group', [_x, _old, _new]] call core_fnc_featEventPropagate;       
                _waitFor deleteAt _forEachIndex;
            };
        };
    } forEach _waitFor;
	( (count _waitFor) isEqualto 0 )      
};

groups_uiPlayers = [];

if ( player in _players ) then { groups_uiGroups pushbackUnique _new; };

if ( isNull _old ) then { [] spawn groups_fnc_create; };
/*
[] spawn groups_fnc_update;
publicVariable "SQUADS";
*/
false