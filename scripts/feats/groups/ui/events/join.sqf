
#include "..\define.hpp"

if ( groups_uiNoEvents ) exitWith {};

disableSerialization;

private _dsp = findDisplay GRP_IDD;
private _old = group player;
private _new = SQUADS select groups_uiGrp;

(_dsp displayCtrl JOIN_IDC) ctrlEnable false;

if ( _old isEqualTo _new ) exitWith {
    [] spawn groups_fnc_update;
    false
};

[player] joinSilent _new;

waitUntil { ( (group player) isEqualTo _new ) };

["PLAYER", 'group', [player, _old, _new]] call core_fnc_featEventPropagate;
["SERVER", 'group', [player, _old, _new]] call core_fnc_featEventPropagate;
["HEADLESS", 'group', [player, _old, _new]] call core_fnc_featEventPropagate;

groups_uiPlayers = [];
groups_uiGroups pushbackUnique _new;

if ( isNull _old ) then { [] spawn groups_fnc_create; };

false