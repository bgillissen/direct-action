#include "..\define.hpp"

disableSerialization;

params ["_ctrl"];

_ctrl ctrlEnable false;

private _entry = ctrlParentControlsGroup _ctrl;
private _unitUid = ctrlText (_entry controlsGroupCtrl MBR_UUID);
private _unit = _unitUid call common_fnc_playerByUid;

if ( isNull _unit ) exitWith {
	[] spawn groups_fnc_update;
    false    
};

private _cc = ((group player) getVariable['commandChain', []]);
private _playerUid = (getPlayerUid player);
private _playerIC = -1;
private _unitIC = -1;
{
    _x params ['_cuid', '_isLeading'];
    if ( _cuid isEqualTo _playerUid ) then { _playerIC = _forEachIndex; };
    if ( _cuid isEqualTo _unitUid ) then { _unitIC = _forEachIndex; };
} forEach _cc;

if ( (_playerIC < 0)  || (_unitIC > _playerIC) ) exitWith {
    [] spawn groups_fnc_update;
    false
};

[player, _unit, (_playerIC + 1), false, true] call commandChain_fnc_assign;

[] spawn groups_fnc_update;

false;