
#include "..\define.hpp"

disableSerialization;

params ['_ctrl'];

private _entry = ctrlParentControlsGroup _ctrl;
private _unitUid = ctrlText (_entry controlsGroupCtrl MBR_UUID);

if ( groups_uiCTRL ) then {
    if ( _unitUid in groups_uiPlayers ) then {
        groups_uiPlayers = groups_uiPlayers - [_unitUid]; 
    } else {
        groups_uiPlayers pushback _unitUid;
    };
} else {
	groups_uiPlayers = [_unitUid];
};

[] spawn groups_fnc_update;

false