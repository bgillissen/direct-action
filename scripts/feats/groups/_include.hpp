#include "..\..\common\ui\_colors.hpp"
#include "ui\define.hpp"

class grp_mbrEntry : daControlGroup {
	h = LINE_HEIGHT;
	w = (TOT_WIDTH / 2);
	class Controls {
		class background : daContainer {
			idc = MBR_BCG;
			h = LINE_HEIGHT;
			w = (TOT_WIDTH / 2);
			colorBackground[] = {0,0,0,0};
		};
		class uuid : daText {
			idc = MBR_UUID;
			h = 0;
			w = 0;
		};
		class rankIcon : daPicture {
			idc = MBR_RANKICON;
			style = 48 + 2048;
			x = (LINE_HEIGHT * 1.8);
			h = LINE_HEIGHT;
			w = LINE_HEIGHT;
		};
		class name : daTxtButton {
			idc = MBR_NAME;
			style = ST_LEFT;
			x = (LINE_HEIGHT * 2.8);
			h = LINE_HEIGHT;
			w = (TOT_WIDTH / 2) - (LINE_HEIGHT * 4.3);
			onButtonClick = "_this call groups_fnc_evtSelectPlayer;";
			colorBackground[] = COL_NONE;
			colorBackgroundDisabled[] = COL_NONE;
			colorBackgroundActive[] = COL_NONE;
			colorShadow[] = COL_NONE;
			colorFocused[] = COL_NONE;
			colorBorder[] = COL_NONE;
		};
	};
};

class grp_dynColor : daCombo {
	idc = MBR_COLOR;
	h = LINE_HEIGHT;
	w = (LINE_HEIGHT * 1.8);
	onLbSelChanged = "_this call groups_fnc_evtColor;";
};

class grp_fixColor : daContainer {
	idc = MBR_COLOR;
	x = 0.008;
	h = LINE_HEIGHT;
	w = LINE_HEIGHT - 0.012;
};

class grp_icText : daText {
	idc = MBR_IC;
	style = ST_RIGHT;
	x = (TOT_WIDTH / 2) - (LINE_HEIGHT * 2.5);
	h = LINE_HEIGHT;
	w = (LINE_HEIGHT * 2.5);
};

class grp_icAssign : daTxtButton {
	idc = MBR_IC;
	style = ST_RIGHT;
	x = (TOT_WIDTH / 2) - (LINE_HEIGHT * 2.5);
	h = LINE_HEIGHT;
	w = (LINE_HEIGHT * 2.5);
	onButtonClick = "_this call groups_fnc_evtAssign";
};
