/*
@filename: feats\groups\playerDestroy.sqf
Author:
	Ben
Description:
	run on player,
	make player rejoin his preInit group
*/

[player] joinSilent PREINIT_GRP;

nil