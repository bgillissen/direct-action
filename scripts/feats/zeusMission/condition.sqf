/*
@filename: feats\zeusMission\condition.sqf
Credit:
	Quicksilver
Author:
	Ben
Description:
	run on player,
	start/stop zeusMission action condition
*/

params ["_isStart"];

if ( CTXT_SERVER ) exitWith { (_isStart && !zeusMission) || (!_isStart && zeusMission) };

if ( (call BIS_fnc_admin) > 0 ) exitWith { (_isStart && !zeusMission) || (!_isStart && zeusMission) };

if ( !isAssigned ) exitWith { false };

( (_isStart && !zeusMission) || (!_isStart && zeusMission) )

/*
if ( _isStart ) exitWith { !zeusMission };

zeusMission
*/