/*
@filename: feats\zeusMission\set.sqf
Author:
	Ben
Description:
	run on player
	update the value of the zeusMission public variable and broadcast it
*/

params ["_thing", "_caller", "_id", "_arg"];

if ( _arg ) then { 
	["zeusMissionStart", [name _caller]] call global_fnc_notification; 
} else {
    ["zeusMissionStop"] call global_fnc_notification;
};

missionNamespace setVariable ["zeusMission", _arg, true];