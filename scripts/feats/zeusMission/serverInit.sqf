/*
@filename: feats\zeusMission\serverInit.sqf
Author:
	Ben
Description:
	run on server,
	set the initial value of the zeusMission public variable to false
	set the initial value of the zeusMissionCompo public variable to empty array
*/

missionNamespace setVariable ["zeusMission", false, true];
missionNamespace setVariable ["zeusMissionCompo", [], true];
zeusMissionCompoRefs = [];

nil