/*
@filename: feats\zeusMission\playerInit.sqf
Author:
	Ben
Description:
	run on player,
	add start / stop zeusMission actions to base things with the curator keyword
	add start / stop alarm sound to base things with the curator keyword

*/

#include "_debug.hpp"

private _start = (["zeusMission", "startAction"] call core_fnc_getSetting);
private _stop = (["zeusMission", "stopAction"] call core_fnc_getSetting); 
private _alarm = (["zeusMission", "alarm"] call core_fnc_getSetting); 
private _alarmPlay = (["zeusMission", "playAlarm"] call core_fnc_getSetting);
private _alarmStop = (["zeusMission", "stopAlarm"] call core_fnc_getSetting); 

{
	{
		_x params ["_thing", "_actions"];
		{
			if ( (configName _x) isEqualTo "curator" ) then {
                #ifdef DEBUG
				private _debug = format["adding actions to %1", _thing];
				debug(LL_DEBUG, _debug);
				#endif
				_thing addAction [_start, {call zeusMission_fnc_set}, true, 0, false, true, "", "[true] call zeusMission_fnc_condition", 4];
				_thing addAction [_stop, {call zeusMission_fnc_set}, false, 0, false, true, "", "[false] call zeusMission_fnc_condition", 4];
                _thing addAction [_alarmPlay, {[_this select 3] call common_fnc_playSound}, _alarm, 0, false, true, "", "[true] call zeusMission_fnc_alarmCond", 4];
                _thing addAction [_alarmStop, {call common_fnc_stopSound}, "", 0, false, true, "", "[false] call zeusMission_fnc_alarmCond", 4];
			};
		} forEach _actions;
	} forEach _x;
} forEach [BA_npc, BA_obj, BA_veh];

nil