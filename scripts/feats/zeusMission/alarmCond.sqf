/*
@filename: feats\zeusMission\alarmCond.sqf
Author:
	Ben
Description:
	run on player,
	play/stop alarm action condition
*/

params ["_isPlay"];

if ( !isAssigned ) exitWith { false };

if ( isNil "lastSoundClass" ) exitWith { _isPlay };

private _alarm = (["zeusMission", "alarm"] call core_fnc_getSetting); 

if ( _isPlay ) exitWith {
	!( (lastSoundClass isEqualTo _alarm) && ((count soundObjects) > 0) ) 
};

( (count soundObjects) > 0 )