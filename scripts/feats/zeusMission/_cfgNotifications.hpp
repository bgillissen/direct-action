class zeusMissionStart {
	title = "All operation have been cancelLed";
	iconPicture = "A3\ui_f\data\igui\cfg\simpletasks\types\whiteboard_ca.paa";
	description = "Head back to base and present yourself to %1 for the briefing.";
	color[] = {0.75, 0.49, 0.09, 1};
	priority = 10;
	duration = 5;
};
class zeusMissionStop {
	title = "Normal operations have been resumed";
	iconPicture = "\a3\ui_f\data\gui\cfg\hints\Zeus_ca.paa";
	description = "Wait a little bit for the next assignment";
	color[] = {0.75, 0.49, 0.09, 1};
	priority = 10;
	duration = 5;
};
