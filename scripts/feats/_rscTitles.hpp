/*
@filename: feats\_rsvTitles.hpp
Author:
	Ben
Description:
	on all context,
	included by main.hpp,
	include features ui classes
*/

class rscTitles {
	#ifdef use_artiComputer
	#include "artiComputer\_rscTitles.hpp"
	#endif

	#ifdef use_artiSupport
	#include "artiSupport\_rscTitles.hpp"
	#endif

	#ifdef use_revive
	#include "revive\_rscTitles.hpp"
	#endif

	#ifdef use_serverRules
	#include "serverRules\_rscTitles.hpp"
	#endif

	#ifdef use_stateMonitor
	#include "stateMonitor\_rscTitles.hpp"
	#endif

	#ifdef use_vehicleCrew
	#include "vehicleCrew\_rscTitles.hpp"
	#endif
};
