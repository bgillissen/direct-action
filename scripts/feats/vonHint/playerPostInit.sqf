/*
@filename: feats\vonHint\playerPostInitThread.sqf
Author:
	Ben
Description:
	run on player,
	wait for screen to be "unblacked" then display periodicaly a Hint about server TS
*/

#include "_debug.hpp"

if ( (["vonHint"] call core_fnc_getParam) == 0 ) exitWith {
    #ifdef DEBUG
    debug(LL_INFO, "disabled by mission parameter");
    #endif
    nil
};

waitUntil {
	sleep 1;
	!BLACKSCREEN
};

sleep (["vonHint", "initialDelay"] call core_fnc_getSetting);

private _delay = (["vonHint", "delay"] call core_fnc_getSetting);
private _ts = (["vonHint", "tsAddr"] call core_fnc_getSetting);
private _messages = (["vonHint", "Messages"] call core_fnc_getSetting); 

#ifdef DEBUG
private _debug = format["thread is running with %1s delay, %2 different message", _delay, count(_messages)];
debug(LL_DEBUG, _debug);
#endif

while { true } do {
	{
		hint parseText format[_x, _ts];
		sleep _delay;	
	} count _messages;
};

nil