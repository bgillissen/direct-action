/*
@filename: feats\_params.hpp
Author:
	Ben
Description:
	on all context,
	included by feats\main.hpp,
	declare mission parameters class and include feature's params
*/

class params {
	#ifdef use_artiComputer
	#include "artiComputer\_params.hpp"
	#endif

	#ifdef use_artiSupport
	#include "artiSupport\_params.hpp"
	#endif

	#include "assets\_params.hpp"

	#ifdef use_backpackOnChest
	#include "backpackOnChest\_params.hpp"
	#endif

	#include "baseAtmosphere\_params.hpp"

	#ifdef use_baseDefence
	#include "baseDefence\_params.hpp"
	#endif

	#ifdef use_baseLight
	#include "baseLight\_params.hpp"
	#endif

	#ifdef use_baseProtection
	#include "baseProtection\_params.hpp"
	#endif

	#ifdef use_checkTS
	#include "checkTS\_params.hpp"
	#endif

	#ifdef use_deathMessage
	#include "deathMessage\_params.hpp"
	#endif

	#include "dynSim\_params.hpp"

	#ifdef use_earPlugs
	#include "earPlugs\_params.hpp"
	#endif

	#ifdef use_environment
	#include "environment\_params.hpp"
	#endif

	#ifdef use_fatigue
	#include "fatigue\_params.hpp"
	#endif

	#ifdef use_flares
	#include "flares\_params.hpp"
	#endif

	#ifdef use_gearRestrictions
	#include "gearRestrictions\_params.hpp"
	#endif

	#ifdef use_ia
	#include "ia\_params.hpp"
	#endif

	#ifdef use_loadBalance
	#include "loadBalance\_params.hpp"
	#endif

	#ifdef use_mapTracker
	#include "mapTracker\_params.hpp"
	#endif

	#ifdef use_nightVision
	#include "nightVision\_params.hpp"
	#endif

	#ifdef use_noCallout
	#include "noCallout\_params.hpp"
	#endif

	#ifdef use_noRenegade
	#include "noRenegade\_params.hpp"
	#endif

	#ifdef use_oneLifeOnly
	#include "oneLifeOnly\_params.hpp"
	#endif

	#include "playerSide\_params.hpp"

	#ifdef use_radioFreq
	#include "radioFreq\_params.hpp"
	#endif

	#ifdef use_revive
	#include "revive\_params.hpp"
	#endif

	#ifdef use_serverRules
	#include "serverRules\_params.hpp"
	#endif

	#ifdef use_squadHint
	#include "squadHint\_params.hpp"
	#endif

	#ifdef use_startTime
	#include "startTime\_params.hpp"
	#endif

	#ifdef use_stateMonitor
	#include "stateMonitor\_params.hpp"
	#endif

	#ifdef use_supplyDrop
	#include "supplyDrop\_params.hpp"
	#endif

	#ifdef use_supportCrate
	#include "supportCrate\_params.hpp"
	#endif

	#ifdef use_va
	#include "va\_params.hpp"
	#endif

	#ifdef use_vcomai
	#include "vcomai\_params.hpp"
	#endif

	#ifdef use_vehicleCrew
	#include "vehicleCrew\_params.hpp"
	#endif

	#ifdef use_vehicleDeco
	#include "vehicleDeco\_params.hpp"
	#endif
	
	#ifdef use_vehicleLoadout
	#include "vehicleLoadout\_params.hpp"
	#endif

	#ifdef use_vehicleRespawn
	#include "vehicleRespawn\_params.hpp"
	#endif

	#ifdef use_vehicleRestrictions
	#include "vehicleRestrictions\_params.hpp"
	#endif

	#ifdef use_vehicleSelection
	#include "vehicleSelection\_params.hpp"
	#endif

	#ifdef use_voiceControl
	#include "voiceControl\_params.hpp"
	#endif
	
	#ifdef use_vonHint
	#include "vonHint\_params.hpp"
	#endif
};
