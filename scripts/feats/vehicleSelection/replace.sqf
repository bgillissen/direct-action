/*
@filename: feats\vehicleSelection\replace.sqf
Author:
	ben
Description:
	run on player,
	replace current vehicle by the selected one
*/

#include "_debug.hpp"

params ["_qid", "_newType"];

(VR select _qid) params ["_veh", "_delay", "_poolName", "_marker", "_atl", "_actions", "_jip", "_preset"];

#ifdef DEBUG
private _debug = format["relacing veh %1 (%2) with %3", _qid, _marker, _newtype];
debug(LL_INFO, _debug);
#endif
                
([_qid, _veh, _poolName, _marker, _atl, _actions, _preset, _newType] call vehicleRespawn_fnc_spawn) params ["_nVeh", ["_nJip", ""]];
(VR select _qid) set [0,_nVeh];
if !( _nJip isEqualTo "" ) then {
	nil remoteExec["", _jip];
	(VR select _qid) set [6, _nJip];
};

publicVariable "VR";