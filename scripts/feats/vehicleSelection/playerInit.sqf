/*
@filename: feats\vehicleSelection\playerInit.sqf
Author:
	ben
Description:
	run on player,
	add action on npc to allow player to select a specific vehicle from pools
*/

#include "_debug.hpp"

if ( (["vehicleSelection"] call core_fnc_getParam) == 0 ) exitWith {
	#ifdef DEBUG
    debug(LL_INFO, "disabled by mission parameter");
    #endif
    nil
};

private _action = ["vehicleSelection", "action"] call core_fnc_getSetting;

{
	{
		_x params ["_thing", "_actions"];
		{
			if ( (configName _x) isEqualTo "vehicleSelection" ) then {
                private _name = getText(_x >> "name");
                private _classes = getArray(_x >> "classes");
                private _roles = getArray(_x >> "roles");
				#ifdef DEBUG
				private _debug = format["action has been added to %1 for name %2", _thing, _name];
				debug(LL_DEBUG, _debug);
				#endif
                private _act = format[_action, _name];
                private _cond = format["[%1] call vehicleSelection_fnc_condition", _roles];
				_thing addAction [_act, {(_this select 3) call vehicleSelection_fnc_open}, [_name, _classes], 0, false, true, "", _cond, 4];
			};
		} forEach _actions;
	} forEach _x;
} forEach [BA_npc, BA_obj, BA_veh];

nil