/*
@filename: feats\vehicleSelection\condition.sqf
Author:
	ben
Description:
	run on player,
	action condition
*/

#include "_debug.hpp"

params ["_roles"];

if ( CTXT_SERVER ) exitWith { true };

if ( (call BIS_fnc_admin) > 0 ) exitWith { true };

private _rank = getNumber(missionConfigFile >> "settings" >> "vehicleSelection" >> "rank");

private _bool = ( (player getVariable["MD_rank", 0]) >= _rank );

if !( _bool ) exitWith { false };

if ( count _roles isEqualTo 0 ) exitWith { true };

private _role = player getVariable "role";

( (_role in _roles) || !(player call memberData_fnc_vehicleRestrictions) )  