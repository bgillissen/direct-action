class vehicleSelection {
	title = "Allow players to select a specific vehicle from pool";
	values[] = {0,1};
	texts[] = {"Disabled","Enabled"};
	default = 1;
};
