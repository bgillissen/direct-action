
#include "ui\define.hpp"

class vehicleSelection {
	idd = VS_IDD;
	name= "vehicleSelection";
	movingEnable = false;
	enableSimulation = true;
	onLoad = "[] spawn vehicleSelection_fnc_init;";
	class controlsBackground {
		class background : daContainer {
			x = 0.5 - (TOT_WIDTH / 2);
			y = 0.5 - (((LINE_HEIGHT * 4) - (SPACE * 5)) / 2) + LINE_HEIGHT + SPACE - YOFFSET;
			w = TOT_WIDTH;
			h = (LINE_HEIGHT * 2) + (3 * SPACE);
		};
	};
	class Controls {
		class title : daTitle {
			idc = TITLE_IDC;
			x = 0.5 - (TOT_WIDTH / 2);
			y = 0.5 - (((LINE_HEIGHT * 4) - (SPACE * 5)) / 2) - YOFFSET;
			w = TOT_WIDTH;
			h = LINE_HEIGHT;
		};
		class slots : daCombo {
			idc = SLOT_IDC;
			x = 0.5 - (TOT_WIDTH / 2) + SPACE;
			y = 0.5 - (((LINE_HEIGHT * 4) - (SPACE * 5)) / 2) + LINE_HEIGHT + (2 * SPACE) - YOFFSET;
			w = TOT_WIDTH - (2 * SPACE);
			h = LINE_HEIGHT;
			onLBSelChanged = "_this call vehicleSelection_fnc_evtSlot;";
		};
		class veh : daCombo {
			idc = VEH_IDC;
			x = 0.5 - (TOT_WIDTH / 2) + SPACE;
			y = 0.5 - (((LINE_HEIGHT * 4) - (SPACE * 5)) / 2) + (2 * LINE_HEIGHT) + (3 * SPACE) - YOFFSET;
			w = TOT_WIDTH - (2 * SPACE);
			h = LINE_HEIGHT;
			onLBSelChanged = "[] call vehicleSelection_fnc_update;";
		};
		class close : daTxtButton {
			idc = -1;
			x = 0.5 - (TOT_WIDTH / 2);
			y = 0.5 - (((LINE_HEIGHT * 4) - (SPACE * 5)) / 2) + (3 * LINE_HEIGHT) + (5 * SPACE) - YOFFSET;
			w = BUTTON_WIDTH;
			h = LINE_HEIGHT;
			text = "Close";
			action = "closeDialog 0;";
		};
		class refresh : daTxtButton {
			idc = -1;
			x = 0.5 - (TOT_WIDTH / 2) + BUTTON_WIDTH + SPACE;
			y = 0.5 - (((LINE_HEIGHT * 4) - (SPACE * 5)) / 2) + (3 * LINE_HEIGHT) + (5 * SPACE) - YOFFSET;
			w = BUTTON_WIDTH;
			h = LINE_HEIGHT;
			text = "Refresh";
			action = "[] call vehicleSelection_fnc_update;";
		};
		class Apply : daTxtButton {
			idc = APPLY_IDC;
			x = 0.5 + (TOT_WIDTH / 2) - BUTTON_WIDTH;
			y = 0.5 - (((LINE_HEIGHT * 4) - (SPACE * 5)) / 2) + (3 * LINE_HEIGHT) + (5 * SPACE) - YOFFSET;
			w = BUTTON_WIDTH;
			h = LINE_HEIGHT;
			text = "Apply";
			action = "[] call vehicleSelection_fnc_evtApply;";

		};
	};
};
