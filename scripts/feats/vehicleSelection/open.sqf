/*
@filename: feats\vehicleLoadout\action.sqf
Author:
	ben
Description:
	run on player,
	configure and open the vehicle loadout dialog
*/

#include "_debug.hpp"

params ["_name", "_classes"];

DAVS_name = _name;
DAVS_classes = _classes;

if !( createDialog "vehicleSelection" ) exitWith {
	#ifdef DEBUG
	debug(LL_ERR, "dialog creation failed");
	#endif
};