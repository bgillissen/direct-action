class vehicleSelection {
	tag = "vehicleSelection";
	class functions {
		class playerInit { file = "feats\vehicleSelection\playerInit.sqf"; };
		class condition { file = "feats\vehicleSelection\condition.sqf"; };
		class open { file = "feats\vehicleSelection\open.sqf"; };
		class replace { file = "feats\vehicleSelection\replace.sqf"; };

		class init { file = "feats\vehicleSelection\ui\init.sqf"; };
		class update { file = "feats\vehicleSelection\ui\update.sqf"; };
		class evtApply { file = "feats\vehicleSelection\ui\events\apply.sqf"; };
		class evtSlot { file = "feats\vehicleSelection\ui\events\slot.sqf"; };
	};
};
