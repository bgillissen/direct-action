/*
@filename: feats\vehicleSelection\ui\events\apply.sqf
Author:
	ben
Description:
	run on player,
	ask server to replace current vehicle by the selected one for the active slot
*/

#include "..\define.hpp"

disableSerialization;

private _qid = DAVS_veh getVariable ["VR_qid", -1];

if ( _qid < 0 ) exitWith {
    diag_log "vehicleSelection: apply failed, no qid variable defined on target vehicle";
};

private _dsp = findDisplay VS_IDD;
private _ctrl = (_dsp displayCtrl VEH_IDC);

[_qid, (_ctrl lbData (lbCurSel _ctrl))] remoteExec ["vehicleSelection_fnc_replace", 2, false];

closeDialog 1;