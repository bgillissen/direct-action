/*
@filename: feats\vehicleSelection\ui\events\slot.sqf
Author:
	ben
Description:
	run on player,
	update list of vehicle for the given slot
*/

#include "..\define.hpp"

disableSerialization;

params ["_slotCtrl", "_index"];

DAVS_slot = _index;
(DAVS_slots select _index) params ["_cfg", "_pool", "_marker"];

private _poolVeh = missionNameSpace getVariable format["BV_%1", _pool];

private _dsp = findDisplay VS_IDD;
private _vehCtrl = _dsp displayCtrl VEH_IDC;

_vehCtrl ctrlEnable false;
lbClear _vehCtrl;

DAVS_veh = objNull;
{
    _x params ["_veh", "_delay", "_poolName", "_markr", "_pos", "_dir", "_actions", "_jip", "_preset"];
	if ( _markr isEqualTo _marker ) exitWith {
        DAVS_veh = _veh;
    };
} forEach VR;

private _dft = -1;
{ 
	private _vName = getText(configFile >> "cfgVehicles" >> _x >> "displayName");
    if !( isNull DAVS_veh ) then {
    	if ( _x isEqualTo (typeOf DAVS_veh) ) then {
            _vName = format["%1 *", _vName];
			_dft = _forEachIndex;
    	};
	};
    _vehCtrl lbAdd _vName;
    _vehCtrl lbsetData [_forEachIndex, _x]; 
} forEach _poolVeh;

if ( _dft < 0 ) then { _dft = 0; };

_vehCtrl lbSetCurSel _dft;
_vehCtrl ctrlEnable true;