/*
@filename: feats\vehicleSelection\ui\update.sqf
Author:
	ben
Description:
	run on player,
	check if the new selection can be applied.
*/

#include "define.hpp"

disableSerialization;

params [["_dsp", displayNull]];

if ( isNull _dsp ) then { _dsp = findDisplay VS_IDD; };
if ( isNull _dsp ) exitWith {};

(DAVS_slots select DAVS_slot) params ["_cfg", "_pool", "_marker"];

private _bool = false;

if ( isNull DAVS_veh ) then {
	{
    	_x params ["_veh", "_delay", "_poolName", "_markr", "_pos", "_dir", "_actions", "_jip", "_preset"];
		if ( _markr isEqualTo _marker ) exitWith { DAVS_veh = _veh; };
	} forEach VR;    
};

if !( isNull DAVS_veh ) then {
	_bool = ( (DAVS_veh distance (getMarkerPos _marker)) <= 10 );
    if ( _bool && !(unitIsUAV _veh) ) then {
  		_bool = ( count (fullCrew DAVS_veh) isEqualTo 0 );     
    };
    if ( _bool ) then {
        private _poolVeh = missionNameSpace getVariable format["BV_%1", _pool];
        private _vehCtrl = (_dsp displayCtrl VEH_IDC);
        private _curType =  _vehCtrl lbData (lbCurSel _vehCtrl);
        _bool = !( _curType isEqualTo (typeOf DAVS_veh) );
	};          
};

(_dsp displayCtrl APPLY_IDC) ctrlEnable _bool;