/*
@filename: feats\vehicleSelection\ui\init.sqf
Author:
	ben
Description:
	run on player,
	init vehicle selection dialog
*/

#include "define.hpp"

disableSerialization;

waitUntil { !isNull (findDisplay VS_IDD) };

private _dsp = (findDisplay VS_IDD);

//title
(_dsp displayCtrl TITLE_IDC) ctrlSetText format["Vehicle Selection: %1", DAVS_name];

//slots
DAVS_slots = [];
private _baseCfg = (missionConfigFile >> "settings" >> "baseVehicle" >> (toUpper worldName) >> BASE_NAME);
private _SlotDsp = ["vehicleSelection", "slotDisplay"] call core_fnc_getSetting;
private _slotCtrl = _dsp displayCtrl SLOT_IDC;
private _custom = -1;
lbClear _slotCtrl;
{
    private _class = _x;
    private _pool = getText(_baseCfg >> _class >> "pool");
    {
        DAVS_slots pushBack [_class, _pool, _x];
    	_slotCtrl lbAdd format[_slotDsp, _class, _pool, _x];
    	_slotCtrl lbsetData [_forEachIndex, _x];
    } forEach getArray(_baseCfg >> _class >> "markers");
} forEach DAVS_classes;

_slotCtrl lbSetCurSel 0;

if !( MOD_cba3 ) exitWith {};

_dsp displayAddEventHandler ["keyDown", "_this call CBA_events_fnc_keyHandlerDown"];
_dsp displayAddEventHandler ["keyUp", "_this call CBA_events_fnc_keyHandlerUp"];