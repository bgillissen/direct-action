/*
@filename: feats\nutskick\playerPostInit.sqf
Author:
	Ben
Description:
	run on player side
	this add the key binding to kick a other player in the nuts and make him fly 
*/

["nutsKick", "Kick in the nuts", "nutsKick_fnc_keybind"] call keybind_fnc_add;

{
	{
		_x params ["_thing", "_actions"];
		{
			if ( (configName _x) isEqualTo "nutsKick" ) then {
                #ifdef DEBUG
                private _debug = format["adding actions to %1", _thing];
                debug(LL_DEBUG, _debug);
                #endif
				_thing addAction ["Make me High", {[_this select 1, false] call nutskick_fnc_kick}, false, 0, false, true, "", "true", 4];
			};
		} forEach _actions;
	} forEach _x;
} forEach [BA_npc, BA_obj, BA_veh];

nil