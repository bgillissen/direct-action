/*
@filename: feats\nutsKick\makeHimFly.sqf
Author:
	Ben
Description:
	run on player
*/

params ["_control", "_keyCode", "_shift", "_ctrl", "_alt"];

if ( (player getVariable ["MD_rank", 0]) < 8 ) exitWith {};

private _target = cursorTarget;

if ( isNull _target ) exitWith {};

if !( isPlayer _target ) exitWith {};

if !( (vehicle _target) isEqualTo _target) exitWith {};

if ( (_target distance player ) > 4 ) exitWith {
	systemChat "Too far."; 
};

if ( (_target getVariable ['MD_rank', 0]) > (player getVariable ['MD_rank', 0]) ) exitWith {
    private _msg = ["nutsKick", "rankMsg"] call core_fnc_getSetting;
	[1, format[_msg, (name player), (name _target)], ["HQ", PLAYER_SIDE]] call global_fnc_chat;
    player setHitPointDamage ["hitLegs", 0.5];
};

[player] remoteExec ["nutsKick_fnc_kick", cursorTarget, false];