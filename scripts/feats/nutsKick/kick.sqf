/*
@filename: feats\nutsKick\kick.sqf
Author:
	Ben
Description:
	run on player
	create a dummy object, attach player to it, and send the object in the sky,
	wait for the object to start falling down, detach player, and optionaly give him a chute
*/

params ["_kicker", ["_doMsg", true]];

private _chuteClass = (["nutsKick", "chuteClass"] call core_fnc_getSetting);
private _givechute = !( (["nutsKick", "giveChute"] call core_fnc_getSetting) isEqualTo 0 );
private _doOnChest = false;

if ( _giveChute ) then {
	private _bag = (backpack player);
	if !( _bag isEqualTo _chuteClass ) then {
		_doOnChest = !( _bag isEqualTo "" );
		private _removeBag = false; 
		if ( _doOnChest ) then {
		    if !( isNil "BOC_onChest" ) then {
		        _doOnChest = !(BOC_onChest); 
		    	_removeBag =  BOC_onChest; 
			}; 
		};
		if ( _doOnChest ) then {
			private _bocThread = [] spawn boc_fnc_onChest;
		    waitUntil { isNull _bocThread };
		};
		if ( _removeBag ) then { removeBackpack player; };
	};
};

_launcher = "Land_TacticalBacon_F" createVehicleLocal (getPos player);

player attachTo[_launcher, [0,0,0]];

_launcher setVelocity (["nutsKick", "velocity"] call core_fnc_getSetting);

if ( _doMsg) then {
	private _msg = ["nutsKick", "msg"] call core_fnc_getSetting;
	[1, format[_msg, (name _kicker), (name player)], ["HQ", PLAYER_SIDE]] call global_fnc_chat;
};

waitUntil {
	sleep 1;
	( ((velocity _launcher) select 2) <= 0 )
};

detach player;

deleteVehicle _launcher;

if !( _giveChute ) exitWith {};

if ( _doMsg) then {
	private _chuteMsg = (["nutsKick", "chuteMsg"] call core_fnc_getSetting);
	[_chuteMsg, 0, (0.035 * safezoneH + safezoneY), 5, 0.3] spawn BIS_fnc_dynamicText;
};

player addBackpack _chuteClass;

waitUntil {
    sleep 1;
    ( (isTouchingGround player) || (((getPosASL player) select 2) < 0) )
};

if ( player getVariable ["agony", false] ) exitWith {};

if ( _doOnChest ) then { [] spawn boc_fnc_onBack; };