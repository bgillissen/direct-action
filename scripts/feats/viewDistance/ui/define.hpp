#define TOT_WIDTH 1
#define YOFFSET 0
#define SPACE 0.01
#define BUTTON_WIDTH (6.25 / 40)
#define LINE_HEIGHT (1 / 25)

#define VD_IDD 10000

#define FOOT_IDC 11100
#define VEH_IDC 11101
#define AIR_IDC 11102
#define ZEUS_IDC 11103

#define DESC_IDC 11001
#define VALUE_IDC 11002
#define TERRAIN_IDC 11003
#define ENV_IDC 11004
#define SLIDER_IDC 11005