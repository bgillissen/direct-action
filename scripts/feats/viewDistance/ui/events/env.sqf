
#include "..\define.hpp"

disableSerialization;

if ( vd_uiNoEvents ) exitWith {};

params ['_ctrl', '_state'];

_grp = ctrlParentControlsGroup _ctrl;

(vd_uiBuffer select (ctrlIdc _grp - 11100)) set [2, (_state > 0)];