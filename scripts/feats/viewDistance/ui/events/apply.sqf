

{
	private _name = format["vd_%1", _x];
	missionNamespace setVariable [_name, (vd_uiBuffer select _forEachIndex)]; 	
} forEach ["foot", "veh", "air", "zeus"];

call viewDistance_fnc_update;

systemChat 'View Distance Settings have been applied';