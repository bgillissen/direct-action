#include "..\define.hpp"

disableSerialization;

if ( vd_uiNoEvents ) exitWith {};

params ["_ctrl", "_value"];


_value = floor _value;

_grp = ctrlParentControlsGroup _ctrl;

(_grp controlsGroupCtrl VALUE_IDC) ctrlSetText format["%1m", _value];


(vd_uiBuffer select (ctrlIdc _grp - 11100)) set [0, _value];