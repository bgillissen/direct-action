
#include "..\define.hpp"

disableSerialization;

if ( vd_uiNoEvents ) exitWith {};

params ['_ctrl', '_idx'];

_grp = ctrlParentControlsGroup _ctrl;

(vd_uiBuffer select (ctrlIdc _grp - 11100)) set [1, _idx];