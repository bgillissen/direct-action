
#include "define.hpp"

vd_uiNoEvents = true;
vd_uiBuffer = [];

disableSerialization;

waitUntil { !isNull findDisplay VD_IDD };

private _allowNoGrass = ( (["viewDistance", 'allowNoGrass'] call core_fnc_getSetting) > 0 );
private _terrainOpts = [];

_terrainOpts pushback "Ultra";
_terrainOpts pushback "High";
_terrainOpts pushback "Medium";
_terrainOpts pushback "Normal";
if ( _allowNoGrass ) then { _terrainOpts pushback "Low"; };

private _dsp = findDisplay VD_IDD;

{
	_x params ['_desc', '_name', '_idc'];
	
	private _vName = format["vd_%1", _name];
	private _value = missionNamespace getVariable [_vName, []];
	if ( _value isEqualTo [] ) then {
		_value set [0, (['viewDistance', _name, 'distance'] call core_fnc_getSetting)];
		_value set [1, (['viewDistance', _name, 'terrain'] call core_fnc_getSetting)];
		_value set [2, ((['viewDistance', _name, 'env'] call core_fnc_getSetting) > 0)];
	};
	vd_uiBuffer set [_forEachIndex, _value];
	
	_value params ['_distance', '_terrain', '_env'];
	
	private _grp = _dsp displayCtrl _idc;
	
	(_grp controlsGroupCtrl DESC_IDC) ctrlSetText _desc;
	
	(_grp controlsGroupCtrl VALUE_IDC) ctrlSetText format["%1m", _distance];
	
	(_grp controlsGroupCtrl SLIDER_IDC) slidersetRange [100, 12000];
	(_grp controlsGroupCtrl SLIDER_IDC) sliderSetSpeed [100,100,100];
	(_grp controlsGroupCtrl SLIDER_IDC) sliderSetPosition _distance;
	
	{ (_grp controlsGroupCtrl TERRAIN_IDC) lbAdd _x; } forEach _terrainOpts;
	if ( _terrain > (count _terrainOpts - 1) ) then { _terrain = (count _terrainOpts - 1); };
	(_grp controlsGroupCtrl TERRAIN_IDC) lbSetCurSel _terrain;
	
	(_grp controlsGroupCtrl ENV_IDC) cbSetChecked _env;
	
} forEach [['On foot', 'foot', FOOT_IDC], 
		   ['In land / sea vehicle', 'veh', VEH_IDC], 
		   ['In air vehicle', 'air', AIR_IDC], 
		   ['In Zeus', 'zeus', ZEUS_IDC]];

vd_uiNoEvents = false;