/*
@filename: feats\viewDistance\playerRemoteControl.sqf
Author:
	Ben
Description:
	run on player,
	define remoteUnit global var as needed 
*/

#include "_debug.hpp"

params ["_gm", "_player", "_unit", "_ctrl"];

if ( _ctrl ) then {
	VD_remoteUnit = _unit;
} else {
    VD_remoteUnit = objNull;
};

nil