class viewDistance {
	tag = "viewDistance";
	class functions {
		class init { file="feats\viewDistance\ui\init.sqf"; };
		class evtApply { file="feats\viewDistance\ui\events\apply.sqf"; };
		class evtDistance { file="feats\viewDistance\ui\events\distance.sqf"; };
		class evtEnv { file="feats\viewDistance\ui\events\env.sqf"; };
		class evtSave { file="feats\viewDistance\ui\events\save.sqf"; };
		class evtTerrain { file="feats\viewDistance\ui\events\terrain.sqf"; };
		class update { file="feats\viewDistance\update.sqf"; };
		class playerGetIn { file="feats\viewDistance\update.sqf"; };
		class playerGetOut { file="feats\viewDistance\update.sqf"; };
		class playerZeusInterface { file="feats\viewDistance\update.sqf"; };
		class playerInit { file="feats\viewDistance\playerInit.sqf"; };
	};
};
