/*
@filename: feats\viewDistance\update.sqf
Author: 
	Ben
Description:
	run on player,
	update player view distance, depending on his vehicle type
*/

#include "_debug.hpp"

private _value = [];
if !( isNull curatorCamera ) then {
	#ifdef DEBUG
	debug(LL_DEBUG, "updated to zeus");
	#endif
	_value = vd_zeus;
	
} else {
	private "_veh";
	if ( isNull vd_remoteUnit ) then {
		_veh = vehicle player;
	} else {
		_veh = vehicle vd_remoteUnit;
	};
	if ( _veh isKindOf "Man" ) then {
		#ifdef DEBUG
		debug(LL_DEBUG, "updated to on foot");
		#endif
		_value = vd_foot;
	} else {
		if ( (_veh isKindOf "LandVehicle") || (_veh isKindOf "Ship") ) then {
			#ifdef DEBUG
			debug(LL_DEBUG, "updated to land/sea vehicle");
			#endif
			_value = vd_veh;
		} else {
			if ( _veh isKindOf "Air" ) then {
				#ifdef DEBUG
				debug(LL_DEBUG, "updated to air vehicle");
				#endif
				_value = vd_air;
			};
		};
	};
};

setViewDistance (_value select 0);
setTerrainGrid (["viewDistance", "terrainGrids", format['v%1', (_value select 1)]] call core_fnc_getSetting);
enableEnvironment (_value select 2);