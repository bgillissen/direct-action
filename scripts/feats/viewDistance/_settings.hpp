
#include "..\..\common\_dik.hpp"

class viewDistance {
	keycode[] = {DIK_V, 1, 0, 1};//alt + shift + v
	allowNoGrass = 0;
	action = "View distance settings";
	class terrainGrids {
		v4 = 50;	//low
		v3 = 25;	//normal
		v2 = 12.5;	//medium
		v1 = 6.25;	//high
		v0 = 3.125;	//ultra
	};
	class foot {
		distance = 2200;
		terrain = 1;
		env = 1;
	};
	class veh {
		distance = 3500;
		terrain = 2;
		env = 0;
	};
	class air {
		distance = 5000;
		terrain = 3;
		env = 0;
	};
	class zeus {
		distance = 7000;
		terrain = 2;
		env = 1;
	};
};
