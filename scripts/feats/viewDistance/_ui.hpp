/*
	DirectAction view Distance
*/

#include "ui\define.hpp"

class viewDistance {
	idd = VD_IDD;
	name= "viewDistance";
	scriptName= "viewDistance";
	movingEnable = false;
	enableSimulation = true;
	onLoad = "[] spawn viewDistance_fnc_init;";
	class Controls {
		class title : daTitle {
			x = 0.5 - (TOT_WIDTH / 2);
			y = 0 - LINE_HEIGHT - YOFFSET;
			w = TOT_WIDTH;
			h = LINE_HEIGHT;
			text = "View Distance";
		};
		class bcg : daContainer {
			x = 0.5 - (TOT_WIDTH / 2);
			y = SPACE - YOFFSET;
			w = TOT_WIDTH;
			h = (4 * ((2 * LINE_HEIGHT) + SPACE)) + (2 * SPACE);
		};
		class onFoot : vdEntry {
			idc = FOOT_IDC;
			y = (2 * SPACE) - YOFFSET;
		};
		class inVeh : vdEntry {
			idc = VEH_IDC;
			y = (1 * (2 * LINE_HEIGHT + SPACE)) + (2 * SPACE) - YOFFSET; 
		};
		class inAir : vdEntry {
			idc = AIR_IDC;
			y = (2 * (2 * LINE_HEIGHT + SPACE)) + (2 * SPACE) - YOFFSET; 
		};
		class inZeus : vdEntry {
			idc = ZEUS_IDC;
			y = (3 * (2 * LINE_HEIGHT + SPACE)) + (2 * SPACE) - YOFFSET; 
		};
		class close : daTxtButton {
			x = 0.5 - (TOT_WIDTH / 2);
			y = (4 * ((2 * LINE_HEIGHT) + SPACE)) + (4 * SPACE);
			w = BUTTON_WIDTH;
			h = LINE_HEIGHT; 
			text = "Close";
			action = "closeDialog 0;";
		};
		class apply : daTxtButton {
			x = 0.5 - (TOT_WIDTH / 2) + BUTTON_WIDTH + SPACE;
			y = (4 * ((2 * LINE_HEIGHT) + SPACE)) + (4 * SPACE);
			w = BUTTON_WIDTH;
			h = LINE_HEIGHT;
			text = "Apply"; 
			action = "call viewDistance_fnc_evtApply";
		};
		class save : daTxtButton {
			x = 0.5 + (TOT_WIDTH / 2) - BUTTON_WIDTH;
			y = (4 * ((2 * LINE_HEIGHT) + SPACE)) + (4 * SPACE);
			w = BUTTON_WIDTH;
			h = LINE_HEIGHT; 
			text = "Save";
			action = "call viewDistance_fnc_evtSave";
			
		};
	};
};
