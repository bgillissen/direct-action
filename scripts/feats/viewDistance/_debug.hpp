#include "..\..\core\debugLevels.hpp"

//comment the following line to disable this feature debug
#define DEBUG()

//debug level for this feature
#define DEBUG_LVL LL_INFO
//debug viewDistance
#define DEBUG_CTXT "viewDistance"

#include "..\__debug.hpp"
