
#include "ui\define.hpp"

class vdEntry : daControlGroup {
	x = 0.5 - (TOT_WIDTH / 2);
	w = TOT_WIDTH;
	h = (2 * LINE_HEIGHT);
	class Controls {
		class desc : daText {
			idc = DESC_IDC;
			style = ST_RIGHT;
			w = (BUTTON_WIDTH * 2);
			h = LINE_HEIGHT;
		};
		class value : daText {
			idc = VALUE_IDC;
			w = BUTTON_WIDTH;
			x = (BUTTON_WIDTH * 2);
			h = LINE_HEIGHT;
			text = "";
		};
		class terrain : daCombo {
			idc = TERRAIN_IDC;
			x = TOT_WIDTH - BUTTON_WIDTH - LINE_HEIGHT  - SPACE;
			w = BUTTON_WIDTH;
			H = LINE_HEIGHT;
			tooltip = "Terrain resolution";
			onLBSelChanged = "_this call viewDistance_fnc_evtTerrain;";
		};
		class env : daCheckBox {
			idc = ENV_IDC;
			x = TOT_WIDTH - LINE_HEIGHT - SPACE;
			w = LINE_HEIGHT;
			H = LINE_HEIGHT;
			tooltip = "Enable environmental effects";
			onCheckedChanged = "_this call viewDistance_fnc_evtEnv;"; 
		};
		class slider : daSlider {
			idc = SLIDER_IDC;
			y = LINE_HEIGHT;
			w = TOT_WIDTH - (2 * SPACE);
			h = LINE_HEIGHT;
			tooltip = "View distance";
			onSliderPosChanged = "_this call viewDistance_fnc_evtDistance;";
		};
	};
};