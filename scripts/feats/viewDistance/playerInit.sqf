/*
@filename: feats\viewDistance\playerInit.sqf
Author: 
	Ben
Description:
	run on player,
*/

#include "_debug.hpp"
#include "ui\define.hpp"

{
	private _profName = format["davd_%1", _x];
	private _value = profileNamespace getVariable [_profName, []];
	if ( _value isEqualTo [] ) then {
		_value set [0, (['viewDistance', _x, 'distance'] call core_fnc_getSetting)];
		_value set [1, (['viewDistance', _x, 'terrain'] call core_fnc_getSetting)];
		_value set [2, ((['viewDistance', _x, 'env'] call core_fnc_getSetting) > 0)];
	};
	missionNamespace setVariable [format["vd_%1", _x], _value]; 	
} forEach ["foot", "veh", "air", "zeus"];

vd_remoteUnit = objNull;

call viewDistance_fnc_update;

private _action = ["viewDistance", "action"] call core_fnc_getSetting;
{
	{
		_x params ["_thing", "_actions"];
		{
			if ( (configName _x) isEqualTo "viewDistance" ) then {
				#ifdef DEBUG
				private _debug = format["action has been added to %1", _thing];
				debug(LL_DEBUG, _debug);
				#endif
				_thing addAction [_action, {createDialog "viewDistance"}, [], 0, false, true, "", "true", 4];
			};
		} forEach _actions;
	} forEach _x;
} forEach [BA_npc, BA_obj, BA_veh];

["viewDistance", "Open view distance settings", {if ( isNull (findDisplay VD_IDD)) then { createDialog "viewDistance";};}] call keybind_fnc_add;

nil