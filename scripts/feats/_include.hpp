/*
@filename: feats\_include.hpp
Author:
	Ben
Description:
	on all context,
	included by feats\_main.hpp,
	include features custom classes
*/

#include "artiComputer\_include.hpp"
#include "artiSupport\_include.hpp"
#include "groups\_include.hpp"
#include "vehicleDeco\_include.hpp"
#include "viewDistance\_include.hpp"
#include "zeusCompo\_include.hpp"
