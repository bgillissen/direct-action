/*
@filename: feats\squadHint\playerPostInit.sqf
Author:
	Ben
Description:
	run on player,
	display a global hint when a member join the server.
*/

#include "_debug.hpp"

if ( (["squadHint"] call core_fnc_getParam) == 0 ) exitWith {
    #ifdef DEBUG
    debug(LL_INFO, "disabled by mission parameter");
    #endif
    nil
};

private _puid = (getPlayerUID player);
private _isMember = ( ({( (_x select 0) isEqualTo _puid )} count memberData) > 0 );

if !( _isMember ) exitWith {
    #ifdef DEBUG
    debug(LL_INFO, "player is not a member, abording");
    #endif
    nil
};
		
private _msg = ["squadHint", "msg"] call core_fnc_getSetting;
private _squad = ["squadHint", "name"] call core_fnc_getSetting;
private _url = ["squadHint", "url"] call core_fnc_getSetting;
format[_msg, _squad, (player getVariable "MD_name"), _url] call global_fnc_hint;

nil