class roleRestrictions {
	rank = 2;
	roles[] = {"hPilot", "jPilot", "sniper", "spotter", "uavOp"};
	exceptions[] = {};
	class messages {
		hPilot = "Fighter pilot slot is restricted to members only, please go back to the lobby and pick another slot";
		jPilot = "Helicopter pilot / co-pilot slots are restricted to members only, please go back to the lobby and pick another slot";
		sniper = "Sniper slot is restricted to members only, please go back to the lobby and pick another slot";
		spotter = "Spotter slot is restricted to members only, please go back to the lobby and pick another slot";
		uavOp = "UAV operator slot is restricted to members only, please go back to the lobby and pick another slot";
	};
};
