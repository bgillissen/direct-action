/*
@filename: feats\roleRestrictions\playerPostInit.sqf
Author:
	Ben
Description:
	run on player
	make sure player is in an allowed role
*/

#include "_debug.hpp"

if !( isMultiplayer ) exitWith {
    #ifdef DEBUG
    debug(LL_INFO, "running in single player mode, abording");
    #endif
    nil
};

private _rank = (["roleRestrictions", "rank"] call core_fnc_getSetting);
private _isMember = ( (player getVariable["MD_rank", 0]) > _rank );

if ( _isMember ) exitWith {
    #ifdef DEBUG
    debug(LL_DEBUG, "player is a member, abording");
    #endif
    nil
};

private _roles = (["roleRestrictions", "roles"] call core_fnc_getSetting);
private _role = player getVariable "role";

if !( _role in _roles ) exitWith {
    #ifdef DEBUG
    debug(LL_DEBUG, "player is not in a restricted role, abording");
    #endif
    nil
};

private _exceptions = (["roleRestrictions", "exceptions"] call core_fnc_getSetting);
private _puid = (getPlayerUID player);

if ( _puid in _exceptions ) exitWith {
    #ifdef DEBUG
    debug(LL_DEBUG, "player is in the exception list, abording");
    #endif
    nil
};

DOLOCK = true;
waitUntil {
	sleep 1;
	!PLAYER_INIT 
};

private _msg = ["roleRestrictions", "messages", _role] call core_fnc_getSetting;
"roleRestrictions" cutText [_msg, "BLACK", 0.01, true];

sleep 6;

"roleRestrictions" cutFadeOut 2;

sleep 1.5;

endMission "RESTRICTEDSLOT";

nil