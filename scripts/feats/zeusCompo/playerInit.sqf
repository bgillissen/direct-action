/*
@filename: feats\zeusCompo\playerInit.sqf
Author:
	Ben
Description:
	run on player,
	add the open dialog action to base things with the curator keyword
	register spawned compositions PVEH
*/

#include "_debug.hpp"
{
	{
		_x params ["_thing", "_actions"];
		{
            
			if ( (configName _x) isEqualTo "curator" ) then {
                _thing addAction ["Zeus Compositions", {createDialog "zeusCompo";}, "", 0, false, true, "", "call zeusCompo_fnc_condition", 4];
			};
		} forEach _actions;
	} forEach _x;
} forEach [BA_npc, BA_obj, BA_veh];

zeusCompo_PVEH = ["zeusCompo_spawned", {[] call zeusCompo_fnc_uiUpdate}] call pveh_fnc_add;

nil