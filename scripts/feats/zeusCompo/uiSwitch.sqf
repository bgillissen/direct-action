
#include "define.hpp"

disableSerialization;

params ["_entry", ["_dsp", displayNull]];

if !( _dsp isEqualType displayNull ) then { _dsp = findDisplay ZC_IDD; };

private _detail = _dsp displayCtrl DETAIL_IDC;
private _list = _dsp displayCtrl LIST_IDC;

private _idc = BASE;
{
    private _curEntry = (_list controlsGroupCtrl _idc);
    if !( isNull _curEntry ) then {
		_curEntry ctrlEnable !(_entry isEqualTo _curEntry);        
    };
    _idc = _idc + 1;
} forEach zeusCompositions;

private _idx = (ctrlIDC _entry) - BASE;

(zeusCompositions select _idx) params ["_class", "_name", "_author", "_pix"];

(_detail controlsGroupCtrl NAME_IDC) ctrlSetText _name;
(_detail controlsGroupCtrl AUTHOR_IDC) ctrlSetText _author;

if ( isClass(configFile >> "cfgPatches" >> "DAzc") ) then {
    (_detail controlsGroupCtrl PICTURE_IDC) ctrlShow true;
	(_detail controlsGroupCtrl PICTURE_IDC) ctrlSetText _pix;
	(_detail controlsGroupCtrl NODAZC_IDC) ctrlShow false;
} else {
    (_detail controlsGroupCtrl PICTURE_IDC) ctrlShow false;
	(_detail controlsGroupCtrl NODAZC_IDC) ctrlShow true;
};

zeusCompo_active = _idx;

[_dsp] call zeusCompo_fnc_uiUpdate;