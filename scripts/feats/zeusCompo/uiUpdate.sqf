
#include "define.hpp"

disableSerialization;

params [["_dsp", displayNull]];

if ( isNull _dsp ) then { _dsp = findDisplay ZC_IDD; };

if ( isNull _dsp ) exitWith {};

(zeusCompositions select zeusCompo_active) params ["_class", "_name", "_author", "_pix"];

private _spawned = (_class in zeusCompo_spawned);

(_dsp displayCtrl SPAWN_IDC) ctrlshow !_spawned;
(_dsp displayCtrl SPAWN_IDC) ctrlEnable !_spawned;
(_dsp displayCtrl REMOVE_IDC) ctrlshow _spawned;
(_dsp displayCtrl REMOVE_IDC) ctrlEnable _spawned;