class zeusCompo {
	tag = "zeusCompo";
	class functions {
		class condition { file="feats\zeusCompo\condition.sqf"; };
		class playerInit { file="feats\zeusCompo\playerInit.sqf"; };
		class playerDestroy { file="feats\zeusCompo\playerDestroy.sqf"; };
		class remove { file="feats\zeusCompo\remove.sqf"; };
		class serverDestroy { file="feats\zeusCompo\serverDestroy.sqf"; };
		class serverInit { file="feats\zeusCompo\serverInit.sqf"; };
		class spawn { file="feats\zeusCompo\spawn.sqf"; };
		class uiInit { file="feats\zeusCompo\uiInit.sqf"; };
		class uiRemove { file="feats\zeusCompo\uiRemove.sqf"; };
		class uiSpawn { file="feats\zeusCompo\uiSpawn.sqf"; };
		class uiSwitch { file="feats\zeusCompo\uiSwitch.sqf"; };
		class uiUpdate { file="feats\zeusCompo\uiUpdate.sqf"; };
	};
};
