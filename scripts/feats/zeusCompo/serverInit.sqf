/*
@filename: feats\zeusCompo\serverInit.sqf
Author:
	Ben
Description:
	run on server,
	define available compositions stack
	set the initial value of the zeusCompo_sptack public variable
*/

#include "_debug.hpp"

zeusCompositions = [];
private _i = 0;
private _found = true;

while { _found } do {
	private _compName = format["ZC_%1_comp%2", toUpper(worldName), _i];
    private _conf = (configFile >> "DAcompo" >> _compName);
    _found = (isClass _conf);
    if ( _found ) then {
        private _ok = true;
        {
            if !( isClass(configFile >> "cfgPatches" >> _x) ) exitWith {
            	_ok = false;
				#ifdef DEBUG
				private _debug = format["exluding composition '%1', missing %2", _compName, _x];
				debug(LL_INFO, _debug);
				#endif                    
            };
        } forEach getArray(_conf >> "addons");
        if ( _ok ) then {
            private _name = getText(_conf >> "name"); 
            private _author = getText(_conf >> "author");
            private _pix = getText(_conf >> "picture");
        	zeusCompositions pushback [_compName, _name, _author, _pix]; 
		};
	};
    _i = _i + 1;
};

#ifdef DEBUG
private _debug = format["found %1 composition(s) for map %2", (count zeusCompositions), worldName];
debug(LL_INFO, _debug);
#endif

publicVariable "zeusCompositions";
missionNamespace setVariable ["zeusCompo_spawned", [], true];

nil