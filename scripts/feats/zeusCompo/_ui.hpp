#include "define.hpp"

class zeusCompo {
	idd = ZC_IDD;
	name = "zeusCompo";
	scriptName = "zeusCompo";
	movingEnable = false;
	enableSimulation = true;
	onLoad = "_this call zeusCompo_fnc_uiInit";
	class controlsBackground {
		class title : daTitle {
			text = "Zeus Composition";
			x = 0.5 - (TOT_WIDTH / 2);
			y = 0.5 - (TOT_HEIGHT / 2) - YOFFSET;
			h = LINE_HEIGHT;
			w = TOT_WIDTH;
		};
		class detailBackgr : daContainer {
			x = 0.5 - (TOT_WIDTH / 2) + LIST_WIDTH + SPACE;
			y = 0.5 - (TOT_HEIGHT / 2) + LINE_HEIGHT + SPACE - YOFFSET;
			h = TOT_HEIGHT - (2 * LINE_HEIGHT) - (2 * SPACE);
			w = TOT_WIDTH - LIST_WIDTH - SPACE;
		};
	};
	class Controls {
		class list : daControlGroup {
			idc = LIST_IDC;
			x = 0.5 - (TOT_WIDTH / 2);
			y = 0.5 - (TOT_HEIGHT / 2) + LINE_HEIGHT + SPACE - YOFFSET;
			h = TOT_HEIGHT - (2 * LINE_HEIGHT) - (2 * SPACE);
			w = LIST_WIDTH;
			class controls {};
		};
		class detail : daControlGroup {
			idc = DETAIL_IDC;
			x = 0.5 - (TOT_WIDTH / 2) + LIST_WIDTH + SPACE;
			y = 0.5 - (TOT_HEIGHT / 2) + LINE_HEIGHT + SPACE - YOFFSET;
			h = TOT_HEIGHT - (2 * LINE_HEIGHT) - (2 * SPACE);
			w = TOT_WIDTH - LIST_WIDTH - SPACE;
			class controls {
				class nameTitle : daText {
					w = BUTTON_WIDTH * 0.5;
					h = LINE_HEIGHT;
					text = "Name :";
				};
				class name : daText {
					idc = NAME_IDC;
					x = BUTTON_WIDTH * 0.5;
					w = BUTTON_WIDTH * 2;
					h = LINE_HEIGHT;
				};
				class authorTitle : daText {
					x = TOT_WIDTH - LIST_WIDTH - SPACE - (BUTTON_WIDTH * 2.7);
					w = BUTTON_WIDTH * 0.7;
					h = LINE_HEIGHT;
					text = "Author :";
				};
				class author : daText {
					idc = AUTHOR_IDC;
					x = TOT_WIDTH - LIST_WIDTH - SPACE - (BUTTON_WIDTH * 2);
					w = BUTTON_WIDTH * 2;
					h = LINE_HEIGHT;
				};
				class picture : daPicture {
					idc = PICTURE_IDC;
					x = SPACE;
					y = LINE_HEIGHT + SPACE;
					w = TOT_WIDTH - LIST_WIDTH - (3 * SPACE);
					h = TOT_HEIGHT - (3 * LINE_HEIGHT) - (4 * SPACE);
				};
				class notloaded : daText {
					idc = NODAZC_IDC;
					x = SPACE;
					y = LINE_HEIGHT + SPACE;
					w = TOT_WIDTH - LIST_WIDTH - (3 * SPACE);
					h = TOT_HEIGHT - (3 * LINE_HEIGHT) - (4 * SPACE);
					text = "DirectAction : Zeus Compositions mod is not loaded, no preview available";
				};
			};
		};
		class close : daTxtButton {
			text = "Close";
			x = 0.5 - (TOT_WIDTH / 2);
			y = 0.5 + (TOT_HEIGHT / 2) - LINE_HEIGHT - YOFFSET;
			h = LINE_HEIGHT;
			w = BUTTON_WIDTH;
			action = "closeDialog 0;";
		};
		class spawn : daTxtButton {
			idc = SPAWN_IDC;
			text = "Spawn";
			x = 0.5 - (TOT_WIDTH / 2) + SPACE + BUTTON_WIDTH;
			y = 0.5 + (TOT_HEIGHT / 2) - LINE_HEIGHT - YOFFSET;
			h = LINE_HEIGHT;
			w = BUTTON_WIDTH;
			action = "_this call zeusCompo_fnc_uiSpawn";
		};
		class remove : daTxtButton {
			idc = REMOVE_IDC;
			text = "Remove";
			x = 0.5 - (TOT_WIDTH / 2) + SPACE + BUTTON_WIDTH;
			y = 0.5 + (TOT_HEIGHT / 2) - LINE_HEIGHT - YOFFSET;
			h = LINE_HEIGHT;
			w = BUTTON_WIDTH;
			action = "_this call zeusCompo_fnc_uiRemove";
		};
	};
};
