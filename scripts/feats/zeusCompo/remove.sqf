/*
@filename: feats\zeusCompo\remove.sqf
Author:
	Ben
Description:
	run on server,
	remove given spawned composition
*/

#include "_debug.hpp"

if !( CTXT_SERVER ) exitWith {
  _this remoteExec ["zeusCompo_fnc_remove", 2, false];  
};

if !( _this in zeusCompo_spawned ) exitWith {
    #ifdef DEBUG
    private _debug = format["composition '%1' has not been spawned, abording", _this];
    debug(LL_WARN, _debug);
    #endif
};

[_this] call compo_fnc_remove;

zeusCompo_spawned = zeusCompo_spawned - [_this];
publicVariable "zeusCompo_spawned";        

if ( CTXT_PLAYER ) then { publicVariableServer "zeusCompo_spawned"; };