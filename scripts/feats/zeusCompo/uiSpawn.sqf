
#include "define.hpp"

disableSerialization;

(zeusCompositions select zeusCompo_active) params ["_class", "_name", "_author", "_pix"];

_class remoteExec ["zeusCompo_fnc_spawn", 2, false];


_dsp = findDisplay ZC_IDD;

if ( isNull _dsp ) exitWith {};

private _spawned = (_class in zeusCompo_spawned);

(_dsp displayCtrl SPAWN_IDC) ctrlEnable false;