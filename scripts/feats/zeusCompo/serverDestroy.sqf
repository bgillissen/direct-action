/*
@filename: feats\zeusCompo\serverDestroy.sqf
Author:
	Ben
Description:
	run on server,
	remove spawned compositions
*/

{
	_x params ["_compo", "_ref"];
	[_ref] call comp_fnc_remove;    
} forEach zeusCompo_refs;

zeusCompo_refs = [];
zeusCompo_spawned = [];

nil