/*
@filename: feats\zeusCompo\spawn.sqf
Author:
	Ben
Description:
	run on server,
	spawn given composition
*/

#include "_debug.hpp"

if !( CTXT_SERVER ) exitWith {
  _this remoteExec ["zeusCompo_fnc_spawn", 2, false];  
};

if ( _this in zeusCompo_spawned ) exitWith {
    #ifdef DEBUG
    private _debug = format["composition '%1' has already been spawned, abording", _this];
    debug(LL_WARN, _debug);
    #endif
};

private _bool = [_this] call compo_fnc_spawn;

if !( _bool ) exitWith {
    #ifdef DEBUG
    private _debug = format["composition '%1' failed to spawn", _this];
    debug(LL_ERR, _debug);
    #endif
};

zeusCompo_spawned pushback _this;
publicVariable "zeusCompo_spawned";

if ( CTXT_PLAYER ) then { publicVariableServer "zeusCompo_spawned"; };