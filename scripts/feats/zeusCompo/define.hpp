
#define YOFFSET 0
#define TOT_WIDTH 1.2
#define TOT_HEIGHT 0.7
#define LIST_WIDTH (0.7 * 0.3)

#define ZC_IDD 80000
#define LIST_IDC 81000
#define DETAIL_IDC 82000
#define BASE 81100
#define NAME_IDC 81001
#define AUTHOR_IDC 81002
#define PICTURE_IDC 81003
#define NODAZC_IDC 81004
#define SPAWN_IDC 81005
#define REMOVE_IDC 81006
