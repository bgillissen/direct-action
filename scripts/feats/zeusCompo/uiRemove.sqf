
#include "define.hpp"

disableSerialization;

(zeusCompositions select zeusCompo_active) params ["_class", "_name", "_author", "_pix"];

_class remoteExec ["zeusCompo_fnc_remove", 2, false];

_dsp = findDisplay ZC_IDD;

if ( isNull _dsp ) exitWith {};

private _spawned = (_class in zeusCompo_spawned);

(_dsp displayCtrl REMOVE_IDC) ctrlEnable false;