
#include "..\..\common\ui\_sizes.hpp"
#include "define.hpp"

disableSerialization;

params ["_dsp"];

(_dsp displayCtrl SPAWN_IDC) ctrlshow false;
(_dsp displayCtrl REMOVE_IDC) ctrlshow false;

private _list = _dsp displayCtrl LIST_IDC;
private _idc = BASE;
{
    _x params ["_class", "_name", "_author", "_pix"];
    private _entry = _dsp ctrlCreate ["zcEntry", _idc, _list];
    _entry ctrlSetText _name;
	if ( isNil "zeusCompo_active" ) then { 
        [_entry, _dsp] call zeusCompo_fnc_uiSwitch; 
	} else {
        if ( zeusCompo_active isEqualTo _forEachIndex ) then {
            [_entry, _dsp] call zeusCompo_fnc_uiSwitch;
        };
    };
    private _pos = ctrlPosition _entry;
    _pos set [1, ((LINE_HEIGHT + SPACE) * _forEachIndex)];
    _entry ctrlSetPosition _pos;
    _entry ctrlCommit 0;
    _idc = _idc + 1;
} forEach zeusCompositions;