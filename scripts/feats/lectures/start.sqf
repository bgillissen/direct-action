
params ["_thing", "_caller", "_id", "_arg"];

_arg params ["_title", "_path", "_pages"];

activeLecture = _title;
lectureOwner = (getPlayerUID player);

publicVariableServer "lectureOwner";
publicVariable "activeLecture";

lecturePage = 0;
lecturePath = _path;
lecturePages = _pages;

lectureNextPage = (player addAction ["Next page", {call lectures_fnc_swapPage}, 1, 0, false, true, '', "( lecturePage < ((count lecturePages) - 1) )"]);
lecturePrevPage = (player addAction ["Previous page", {call lectures_fnc_swapPage}, -1, 0, false, true, '', "(lecturePage > 0)"]);
lectureStop = (player addAction ["Stop lecture", {call lectures_fnc_stop}, "", 0, false, true, '', "true"]);

lecturePreTextures = [];
{
    lecturePreTextures pushback ((getObjectTextures _x) select 0);
} forEach lectureDisplays;

publicVariable "lecturePreTextures";

[objNull, objNull, -1, 0] call lectures_fnc_swapPage;