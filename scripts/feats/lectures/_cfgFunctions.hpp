class lectures {
	tag = "lectures";
	class functions {
		class playerInit { file="feats\lectures\playerInit.sqf"; };
		class serverInit { file="feats\lectures\serverInit.sqf"; };
		class serverLeave { file="feats\lectures\serverLeave.sqf"; };
		class start { file="feats\lectures\start.sqf"; };
		class stop { file="feats\lectures\stop.sqf"; };
		class swapPage { file="feats\lectures\swapPage.sqf"; };
	};
};
