/*
@filename: feats\lectures\playerInit.sqf
Author:
	Ben
Description:
	run on player
 	
*/

#include "_debug.hpp"

private _lectures = [];
{
    if ( isClass( _x >> "lectures") ) then {
        {
            _lectures pushback [getText(_x >> "title"), getText(_x >> "path"), getArray(_x >> "pages")];
        } forEach ("true" configClasses (_x >> "lectures"));
    };
} forEach ("true" configClasses (configFile >> "cfgDirectAction"));

if ( (count _lectures) isEqualTo 0 ) exitwith {
	#ifdef DEBUG
	debug(LL_DEBUG, "none found, abording");
	#endif
};

{
    _x params ["_thing", "_actions"];
	{
		if ( (configName _x) isEqualTo "lecture" ) then {
            if ( getText(_x >> "type") isEqualTo "ctrl" ) then {
				{
                    _x params ["_title", "_path", "_pages"];
                    #ifdef DEBUG
            		private _debug = format["adding '%1' to %2", _title, _thing];
            		debug(LL_DEBUG, _debug);
            		#endif
                    private _action = format["Start %1", _title];
					_thing addAction [_action, {call lectures_fnc_start}, _x, 0, false, true, '', "(activeLecture isEqualto '')", 4];
                } forEach _lectures;
			};
		};
	} forEach _actions;
} forEach BA_obj;

nil