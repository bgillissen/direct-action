
params ["_thing", "_caller", "_id", "_arg"];

LecturePage = LecturePage + _arg;

private _texture = format[lecturePath, (lecturePages select lecturePage)];
 
{
	_x setObjectTextureGlobal[0, _texture];
} forEach lectureDisplays;