/*
@filename: feats\lectures\serverInit.sqf
Author:
	Ben
Description:
	run on server, 	
*/

lectureOwner = "";
activeLecture = "";
lecturePage = 0;
publicVariable "activeLecture";
publicVariable "lecturePage";

lectureDisplays = [];
{
    _x params ["_thing", "_actions"];
	{
		if ( (configName _x) isEqualTo "lecture" ) then {
            if ( getText(_x >> "type") isEqualTo "dsp" ) then { lectureDisplays pushback _thing; };
		};
	} forEach _actions;
} forEach BA_obj;

publicVariable "lectureDisplays";

nil