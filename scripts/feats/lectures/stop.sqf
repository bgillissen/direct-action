
if ( CTXT_PLAYER ) then {
	player removeAction lectureNextPage;
	player removeAction lecturePrevPage;
	player removeAction lectureStop;
	lectureNextPage = nil;
	lecturePrevPage = nil;
	lectureStop = nil;
	lecturePages = nil;
};

activeLecture = "";
lectureOwner = "";
publicVariable "activeLecture";
publicVariableServer "lectureOwner";

{
	_x setObjectTextureGlobal[0, (lecturePreTextures select _forEachIndex)];
} forEach lectureDisplays;