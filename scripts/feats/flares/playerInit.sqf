/*
@filename: feats\flares\playerInit.sqf
Author:
	Ben
Description:
	run on player,
*/

if ( (["flares"] call core_fnc_getParam) == 0 ) exitWith { nil };

daFlares = [];
{
	daFlares pushback [configName _x, getArray _x];
} forEach (configProperties [(missionConfigFile >> "settings" >> "flares" >> "colors"), "true", true]);

nil