
if !( CTXT_PLAYER ) exitWith {};

params ["_projectile", '_color'];

private _flareLight = objNull;
private _limit = time + 6;

while { isNull _flareLight && !isNull _projectile } do { 
	private _lastPos = (getPos _projectile); 
	private _lights = (_lastPos nearObjects ["#lightpoint", 50]);
	if ( (count _lights > 0) || (time > _limit) ) exitWith {
		_flareLight = "#lightpoint" createVehicleLocal _lastPos;
		if ( count _lights > 0 ) then { _projectile = _lights select 0; }; 
	};
};

if ( isNull _flareLight ) exitWith {};

private _range = (["flares", "range"] call core_fnc_getSetting);
private _intensity = (["flares", "intensity"] call core_fnc_getSetting);

_flareLight lightAttachObject [_projectile, [0,0,0]];
_flareLight setLightAmbient _color;  
_flareLight setLightColor _color;
_flareLight setLightIntensity _intensity;
_flareLight setLightUseFlare true;
_flareLight setLightFlareSize 10;
_flareLight setLightFlareMaxDistance 2000;
_flareLight setLightAttenuation [_range, 1, 100, 0, 50, (_range - 10)]; 
_flareLight setLightDayLight true;
	
private _inter_flare = 0;
private _flare_brig = 0;
while { !isNull _projectile } do {
	_int_mic = 0.05 + random 0.1;
	sleep _int_mic;
	_flare_brig = ( _intensity + (random 10) );
	_flareLight setLightIntensity _flare_brig;
	_inter_flare = _inter_flare + _int_mic;
};

private _int_mic = 3;
while { _int_mic > 0 } do {
	_flare_brig = ( _flare_brig - 10 );
	_flareLight setLightIntensity _flare_brig;
	_int_mic = _int_mic - 0.03;
	sleep 0.01;
};

deleteVehicle _flareLight;