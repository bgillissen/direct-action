class flares {
	tag = "flares";
	class functions {
		class playerInit { file="feats\flares\playerInit.sqf"; };
		class playerShoot { file="feats\flares\playerShoot.sqf"; };
		class flare { file="feats\flares\flare.sqf"; };
	};
};
