class flares {
	range = 500;
	intensity = 30;
	class colors {
		F_40mm_White[] = {0.7,0.7,0.8};
		F_40mm_Red[] = {0.7,0.15,0.1};
		F_40mm_Yellow[] = {0.7,0.7,0};
		F_40mm_Green[] = {0.2,0.7,0.2};
		UK3CB_BAF_F_40mm_White[] = {0.7,0.7,0.8};
		UK3CB_BAF_F_40mm_Red[] = {0.7,0.15,0.1};
		UK3CB_BAF_F_40mm_Yellow[] = {0.7,0.7,0};
		UK3CB_BAF_F_40mm_Green[] = {0.2,0.7,0.2};
	};
};
