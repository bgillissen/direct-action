class weaponHolster {
	tag = "weaponHolster";
	class functions {
		class playerPostInit { file="feats\weaponHolster\playerPostInit.sqf"; };
		class swapState { file="feats\weaponHolster\swapState.sqf"; };
	};
};
