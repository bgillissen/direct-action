/*
@filename: feats\weaponHolster\swapState.sqf
Author:
	Ben
Description:
	run on player
	un/holster player weapon
*/

params ["_control", "_keyCode", "_shift", "_ctrl", "_alt"];

if ( weaponHolster_state ) then {
	weaponHolster_state = false;
	player action ["SwitchWeapon", player, player, 0];
} else { 
	weaponHolster_state = true;
	player action ["SwitchWeapon", player, player, 299];
};

true