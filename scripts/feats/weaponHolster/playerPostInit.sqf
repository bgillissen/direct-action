/*
@filename: feats\weaponHolster\playerPostInit.sqf
Author:
	Ben
Description:
	run on player
	this add the key binding to put weapon the back or back in hand
*/

#include "_debug.hpp";

if ( MOD_ace ) exitWith {
	#ifdef DEBUG
    debug(LL_INFO, "disabled by ACE presence");
    #endif
    nil
};

weaponHolster_state = false;

["weaponHolster", "Holster Weapon", "weaponHolster_fnc_swapState"] call keybind_fnc_add;

nil