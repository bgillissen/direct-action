/*
@filename: feats\oneLifeOnly\playerRespawn.sqf
Author:
	Ben
Description:
	run on player,
	send player in spectator mode if present in the dead players stack
*/

#include "_debug.hpp"

if ( (["onelifeonly"] call core_fnc_getParam) == 0 ) exitWith {
   	#ifdef DEBUG
	debug(LL_DEBUG, "disabled by mission parameter");
    #endif 
    nil
};

if !( isMultiplayer ) exitWith {
    #ifdef DEBUG
    debug(LL_DEBUG, "running in single player mode, abording");
    #endif
    nil
};

if !( (getPlayerUID player) in DEAD_PLAYERS ) exitWith { nil };

if ( isNil "OLO" ) then {
	if ( BLACKSCREEN ) then { waitUntil { sleep 1;!BLACKSCREEN }; };
	"olo" cutText ["You already died during this mission, sorry but you can only spectate", "BLACK", 0.01, true];
};

[] call bis_fnc_respawnspectator;

if ( isNil "OLO" ) then {
	OLO = true;
	sleep 4;
	"olo" cutFadeOut 2;
};

nil