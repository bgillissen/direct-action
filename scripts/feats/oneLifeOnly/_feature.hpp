class oneLifeOnly : feat_base  {
	class server : featServer_base {
		class init : featEvent_enable {};
		class killed : featEvent_enable {};
		class respawn : featEvent_enable {};
	};
	class player : featPlayer_base {
		class respawn : featEvent_enable {};
	};
};
