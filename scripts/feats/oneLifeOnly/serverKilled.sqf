 /*
@filename: feats\oneLifeOnly\serverKilled.sqf
Author:
	Ben
Description:
	run on server,
	add player's UID to the dead players stack
*/

#include "_debug.hpp"

if ( (["onelifeonly"] call core_fnc_getParam) == 0 ) exitWith {
   	#ifdef DEBUG
	debug(LL_DEBUG, "disabled by mission parameter");
    #endif
    nil 
};

if !( isMultiplayer ) exitWith {
    #ifdef DEBUG
    debug(LL_DEBUG, "running in single player mode, abording");
    #endif
    nil
};

params ["_eventArgs", "_uid"];

DEAD_PLAYERS pushbackUnique _uid;

publicVariable "DEAD_PLAYERS";

nil