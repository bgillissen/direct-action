/*
@filename: feats\oneLifeOnly\serverInit.sqf
Author:
	Ben
Description:
	run on server,
	initialize the dead players stack
*/

#include "_debug.hpp"

if ( (["onelifeonly"] call core_fnc_getParam) == 0 ) exitWith {
    #ifdef DEBUG
    debug(LL_INFO, "disabled by mission parameter");
    #endif
    nil
};

if !( isMultiplayer ) exitWith {
    #ifdef DEBUG
    debug(LL_INFO, "running in single player mode, abording");
    #endif
    nil
};

DEAD_PLAYERS = [];

publicVariable "DEAD_PLAYERS";

nil