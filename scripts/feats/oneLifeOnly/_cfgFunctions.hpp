class oneLifeOnly {
	tag = "oneLifeOnly";
	class functions {
		class playerRespawn { file="feats\oneLifeOnly\playerRespawn.sqf"; };
		class serverInit  { file="feats\oneLifeOnly\serverInit.sqf"; };
		class serverKilled { file="feats\oneLifeOnly\serverKilled.sqf"; };
		class serverRespawn { file="feats\oneLifeOnly\serverRespawn.sqf"; };
	};
};
