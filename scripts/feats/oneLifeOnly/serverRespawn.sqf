/*
@filename: feats\oneLifeOnly\serverRespawn.sqf
Author:
	Ben
Description:
	run on server,
	hide player unit if present in the dead players stack
*/

#include "_debug.hpp"

if ( (["onelifeonly"] call core_fnc_getParam) == 0 ) exitWith {
   	#ifdef DEBUG
	debug(LL_DEBUG, "disabled by mission parameter");
    #endif
    nil 
};

if !( isMultiplayer ) exitWith {
    #ifdef DEBUG
    debug(LL_DEBUG, "running in single player mode, abording");
    #endif
    nil
};

params ["_player"];

if ( (getPlayerUID _player) in DEAD_PLAYERS ) then {
    _player setVariable ['MT_hidden', true, true];
	_player hideObjectGlobal true;
};

nil