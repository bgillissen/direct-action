class vehicleCrew {
	tag = "vehicleCrew";
	class functions {
		class playerDestroy { file="feats\vehicleCrew\playerDestroy.sqf"; };
		class playerInit { file="feats\vehicleCrew\playerInit.sqf"; };
		class swapState { file="feats\vehicleCrew\swapState.sqf"; };
		class seatRestrictions { file="feats\vehicleCrew\seatRestrictions.sqf"; };
	};
};
