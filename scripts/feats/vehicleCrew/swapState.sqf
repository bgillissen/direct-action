/*
@filename: feats\vehicleCrew\swapState.sqf
Author:
	Ben
Description:
	run on player side by a display event handler
	swap the state of the vehicleCrew HUD
*/

params ["_control", "_keyCode", "_shift", "_ctrl", "_alt"];

VC_draw = !VC_draw;

if ( VC_draw ) then {
	systemChat (["vehicleCrew", "onMsg"] call core_fnc_getSetting);
} else {
    systemChat (["vehicleCrew", "offMsg"] call core_fnc_getSetting);
};

true