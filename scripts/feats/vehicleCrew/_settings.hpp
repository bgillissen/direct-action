
#include "..\..\common\_dik.hpp"

class vehicleCrew {
	keycode[] = {DIK_BACK, 0, 0, 1}; //alt + BACKSPACE
	onMsg = "Vehicle crew HUD enabled";
	offMsg = "Vehicle crew HUD disabled";
	updateDelay = 3;
	cargoHeader = "<img size='0.85' color='#6b8e23' image='%1'/><t size='0.85' color='#f0e68c'>%2</t><br/>";
	driverHeader = "<img size='0.85' color='#6b8e23' image='%1'/><t size='0.85' color='#f0e68c'>%2 %3</t><br/>";
	seatDisplay = "%1 / %2";
	seatFullDisplay = "<t color='#ff0000'>%1 / %2</t>";
	cargoLine = "<img size='0.7' color='#6b8e23' image='%1'/> <t size='0.7' color='#f0e68c'>%2</t><br/>";
	gunnerLine = "<img size='0.7' color='#6b8e23' image='%1'/>  <t size='0.7' color='#f0e68c'>%2<t size='0.5'> - %3</t></t><br/>";
	class icons {
		driver = "a3\ui_f\data\IGUI\Cfg\Actions\getindriver_ca.paa";
		gunner = "a3\ui_f\data\IGUI\Cfg\Actions\getingunner_ca.paa";
		commander = "a3\ui_f\data\IGUI\Cfg\Actions\getincommander_ca.paa";
		cargo = "a3\ui_f\data\IGUI\Cfg\Actions\getincargo_ca.paa";
		kia = "\a3\Ui_F_Curator\Data\CfgMarkers\kia_ca.paa";
	};
};
