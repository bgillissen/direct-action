
params ["_veh", "_seats"];

private _type = typeOf _veh;
private _out = [];

private _isException = ( _veh getVariable ["NOVR", false] );

if !( _isException ) then { _isException = ( _veh isKindOf "ParachuteBase" ); };

if !( _isException ) then { 
	{
		private _pools = getArray(_x >> "pools");
	    if ( ({(_type in (missionNamespace getVariable _x))} count _pools) > 0 ) then {
	    	if ( count(getArray(_x >> "roles")) == 0 && count(getArray(_x >> "seats")) == 0 ) exitWith { 
	        	_isException = true;
			};
		};
	} forEach ("true" configClasses (missionConfigFile >> "settings" >> "vehicleRestrictions" >> "exceptions"));
};

if ( _isException ) exitWith {
	{ _out pushback false; } forEach _seats;
    _out    	
};

{
	_x params ["_unit", "_role", "_cargoIdx", "_turretPath", "_personTurret"];
    private _isRestricted = false;
	{
		private _conf = _x;
	   	private _apply = true;
	    {
			if ( ([_x] call core_fnc_getParam) isEqualTo 0 ) then { _apply = false; };        
	    } forEach getArray(_conf >> "params");
	    if ( _apply ) then { _apply = ( ({_veh isKindOf _x} count getArray(_conf >> "kinds")) > 0 ); };
		if ( _apply ) then {
			switch (_role ) do {
              	case "driver";
				case "Driver": { _apply = ( "driver" in getArray(_conf >> "seats") ); };
                case "cargo";
                case "Cargo": { _apply = ( "cargo" in getArray(_conf >> "seats") );  };
                case "turret";
                case "Turret";
                case "gunner";
                case "Gunner":  { _apply = ("turret" in getArray(_conf >> "seats") );
                       			  if ( _apply && (count getArray(_conf >> "turrets") > 0) ) then {
									_apply = (_turretPath in getArray(_conf >> "turrets") );
                                  };
                                };
				case "commander";
                case "Commander": { _apply = ("commander" in getArray(_conf >> "seats") );
                       			    if ( _apply && (count getArray(_conf >> "turrets") > 0) ) then {
									  _apply = (_turretPath in getArray(_conf >> "turrets") );
                                    };
                                  };
				default { _apply = false; };
			};
			if ( _apply ) then {
                _isRestricted = ( (count getArray (_conf >> "roles")) > 0 );
            	if !( _isRestricted ) then { _isRestricted = ( (count getArray (_conf >> "notRoles")) > 0 ); };
            	if !( _isRestricted ) then { _isRestricted = !( getText(_conf >> "desc") isEqualTo "" ); }; 
            };
		};
	} forEach ("true" configClasses (missionConfigFile >> "settings" >> "vehicleRestrictions" >> "rules"));
	_out pushback _isRestricted;
} forEach _seats;

_out