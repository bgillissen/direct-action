class vehicleCrew {
	idd = -1;
    fadeout = 0;
    fadein = 0;
    duration = 1e+1000;
	name= "vehicleCrew";
	onLoad = "uiNamespace setVariable ['vehicleCrew', _this select 0]";
	class controlsBackground {
		class container : daStructuredText {
			idc = 99999;
			x = (SafeZoneX + 0.015);
			y = (SafeZoneY + 0.60);
			w = 0.4;
			h = 0.65;
			shadow = 1;
		};
	};
};
