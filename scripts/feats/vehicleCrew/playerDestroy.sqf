/*
@filename: feats\vehicleCrew\playerDestroy.sqf
Author:
	Ben
Description:
	run on player,
	kill the vehicleCrew playerInit thread
 */

if ( count(_this) == 0 ) exitWith { nil };

params ["_when", "_thread"];

if ( scriptDone _thread ) exitWith { nil };

terminate _thread;

waitUntil{ scriptDone _thread };

nil