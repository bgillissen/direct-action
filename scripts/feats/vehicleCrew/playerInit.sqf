/*
@filename: feats\vehicleCrew\playerInit.sqf
Author:
	unknown, taken from ahoyWorld I&A
Description:
	run on player,
	keeps updating an HUD with the passenger list of the player's vehicle.
*/

#include "_debug.hpp"

if ( (["vehicleCrew"] call core_fnc_getParam) == 0 ) exitWith {
	#ifdef DEBUG
    debug(LL_INFO, "disabled by mission parameter");
    #endif
	nil
};

private _updateDelay = ["vehicleCrew", "updateDelay"] call core_fnc_getSetting;

private _crewNoDsp = ["vehicleCrew", "cargoHeader"] call core_fnc_getSetting;
private _crewDsp = ["vehicleCrew", "driverHeader"] call core_fnc_getSetting;

private _seatDsp = ["vehicleCrew", "seatDisplay"] call core_fnc_getSetting;
private _seatFullDsp = ["vehicleCrew", "seatFullDisplay"] call core_fnc_getSetting;

private _line = ["vehicleCrew", "cargoLine"] call core_fnc_getSetting;
private _gunnerLine = ["vehicleCrew", "gunnerLine"] call core_fnc_getSetting;

private _driver = ["vehicleCrew", "icons", "driver"] call core_fnc_getSetting;
private _gunner = ["vehicleCrew", "icons", "gunner"] call core_fnc_getSetting;
private _commander = ["vehicleCrew", "icons", "commander"] call core_fnc_getSetting;
private _cargo = ["vehicleCrew", "icons", "cargo"] call core_fnc_getSetting;
private _kia = ["vehicleCrew", "icons", "kia"] call core_fnc_getSetting;

VC_draw = true;

["vehicleCrew", "Toggle vehicle crew HUD visibility", "vehicleCrew_fnc_swapState"] call keybind_fnc_add;

waitUntil { sleep 1;!BLACKSCREEN };

#ifdef DEBUG
private _debug = format["thread is running with a %1s delay", _updateDelay];
debug(LL_DEBUG, _debug);
#endif

disableSerialization;

private _dsp = displayNull;
private _ctrl = controlNull; 
private _seatRestrictions = [];

private _detailFnc = {
	_this params ["_veh", "_unit", "_role", "_turretPath", "_personTurret"];
	if ( _role isEqualTo "driver" ) exitWith { [0, _driver] };
	if ( _role isEqualTo "commander" ) exitWith { [2, _commander] };
	if ( _role isEqualTo "cargo" ) exitWith { [4, _cargo] };
	private _cfg = [(typeOf _veh), _turretPath] call common_fnc_getTurretConfig;
	if ( isNull _cfg ) exitWith { [4, _cargo] };
	if ( getNumber(_cfg >> "iscopilot") isEqualTo 1) exitWith { [1, _driver] };
	if ( _personTurret ) exitWith { [4, _cargo, getText(_cfg >> "gunnerName")] };
	[3, _gunner, getText(_cfg >> "gunnerName")]
};

while { true } do {
	if !( VC_draw ) then {
		_seatRestrictions = [];
		if !( isNull _dsp ) then { "vehicleCrew" cutFadeOut 0.01; };
	} else {
		if ( (isNull _dsp) || (isNull _ctrl) ) then {
			"vehicleCrew" cutRsc ["vehicleCrew", "PLAIN", 0.01, false]; 
			_dsp = uiNameSpace getVariable "vehicleCrew";
			_ctrl = _dsp displayCtrl 99999; 
		};
		private _list = "";
		private _veh = (vehicle player);
	    if( !(player isEqualTo _veh) && (alive _veh) && !(_veh isKindOf "ParachuteBase") ) then {
			
			private _vehName = getText (configFile >> "CfgVehicles" >> (typeOf _veh) >> "displayName");
	    	private _vehIcon = getText (configFile >> "CfgVehicles" >> (typeOf _veh) >> "icon");
			private _crew = fullcrew [_veh, "", true];
			private _showAmount = ( (driver _veh) isEqualTo player );
			if !( _showAmount ) then { _showAmount = [_veh, player] call common_fnc_isCopilot; };
	    	if ( _showAmount ) then {
				if ( _seatRestrictions isEqualTo [] ) then { 
					_seatRestrictions = [_veh, _crew] call vehicleCrew_fnc_seatRestrictions;
				};				
				private _totUnrestricted =  { !_x } count _seatRestrictions;
				private _totRestricted =  { _x } count _seatRestrictions;
				private _isRestricted =  ( _totRestricted > 0 );
	    		private _freeCargo = {( ((_x select 1) isEqualTo "cargo") && isNull(_x select 0) )} count _crew;
				private _usedUnrestricted = 0;
				private _usedRestricted  = 0;
				{
					if ( !isNull(_x select 0) ) then {
						if ( _seatRestrictions select _forEachIndex ) then { 
							_usedRestricted = _usedRestricted + 1; 
						} else {
							_usedUnrestricted = _usedUnrestricted + 1;
						};
					};
				} foreach _crew;
				private _seatData = [];
				if ( _totUnrestricted > 0 ) then { _seatData pushback [_usedUnrestricted, _totUnrestricted]; };
				if ( _totRestricted > 1 ) then { _seatData pushback [_usedRestricted, _totRestricted]; };
				private _seatString = "";
				{
					_x params ["_used", "_tot"];
					private "_buff";
					if ( _used >= _tot ) then {
						_buff = format[_seatFullDsp, _used, _tot]; 
					} else {
						_buff = format[_seatDsp, _used, _tot];
					};
					if ( _seatString isEqualTo "" ) then {
						_seatString = _buff; 
					} else {
						_seatString = _seatString + " + " + _buff;
					};
				} forEach _seatData;
				if !( _seatString isEqualTo "" ) then { _seatString = "(" + _seatString + ")"; };
	    		_list = format[_crewDsp, _vehIcon, _vehName, _seatString];
	    	} else {
				_list = format[_crewNoDsp, _vehIcon, _vehName];
			};	    	
	    	private _stack = [[],[],[],[],[]];
	    	{
	    		_x params ["_unit", "_role", "_index", "_turretPath", "_personTurret"];
	    		if !( _unit isEqualTo objNull ) then {
	    			([_veh, _unit, _role, _turretPath, _personTurret] call _detailFnc) params ["_key", "_icon", ["_gunName", ""]];
	    			private "_toFormat";
	    			if !( _gunName isEqualTo "" ) then { 
						_toFormat = _gunnerLine; 
					} else {
						_toFormat = _line;
					};
	    			if !( alive _unit ) then {
						_icon = _kia;
	    				(_stack select _key) pushback format[_toFormat, _icon, "dead body", _gunName];	
	    			} else {
	    				private _name = _unit getVariable ["MD_name", (name _unit)];
	    				if ( _unit getVariable["agony", false] ) then { _icon = _kia; };
						if ( _unit getVariable ["ACE_isUnconscious", false] )  then { _icon = _kia; };
	    				(_stack select _key) pushback format[_toFormat, _icon, _name, _gunName];
	    			};
					
	    		};
	    	} forEach _crew;
	    	{ _list = format["%1%2", _list, (_x joinString "")]; } forEach _stack;
	    } else {
			_seatRestrictions = [];
		};
		_ctrl ctrlSetStructuredText parseText _list;
		_ctrl ctrlCommit 0;
	};

    sleep _updateDelay;
};  

nil