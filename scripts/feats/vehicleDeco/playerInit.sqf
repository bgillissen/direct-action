/*
@filename: feats\vehicleDeco\playerInit.sqf
Author:
	ben
Description:
	run on player,
	add action on npc to allow player to change the vehicle appearance.
*/

#include "_debug.hpp"

if ( (["vehicleDeco"] call core_fnc_getParam) == 0 ) exitWith {
	#ifdef DEBUG
    debug(LL_INFO, "disabled by mission parameter");
    #endif
    nil
};


DAVD_uiNoEvents = false;
DAVD_thread = scriptNull;

private _act = ["vehicleDeco", "action"] call core_fnc_getSetting;

{
	{
		_x params ["_thing", "_actions"];
		{
			if ( (configName _x) isEqualTo "vehicleDeco" ) then {
                private _marker = getText(_x >> "marker");
                private _camPos = getArray(_x >> "camPos");
                private _type = getText(_x >> "type");
				#ifdef DEBUG
				private _debug = format["action has been added to %1 for marker %2", _thing, _marker];
				debug(LL_DEBUG, _debug);
				#endif
                private _cond = format["['%1', '%2'] call vehicleDeco_fnc_condition", _marker, _type];
				_thing addAction [_act, {(_this select 3) call vehicleDeco_fnc_open}, [_marker, _camPos, _type], 0, false, true, "", _cond, 4];
			};
		} forEach _actions;
	} forEach _x;
} forEach [BA_npc, BA_obj, BA_veh];

nil