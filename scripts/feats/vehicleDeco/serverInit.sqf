/*
@filename: feats\vehicleDeco\serverInit.sqf
Author:
	ben
Description:
	run on server,
	create map markers
*/

#include "_debug.hpp"

if ( (["vehicleDeco"] call core_fnc_getParam) == 0 ) exitWith {
	#ifdef DEBUG
    debug(LL_INFO, "disabled by mission parameter");
    #endif
    nil
};

private _color = call common_fnc_getMarkerColor;

DAVD_markers = [];

{
	{
		_x params ["_thing", "_actions"];
		{
			if ( (configName _x) isEqualTo "vehicleDeco" ) then {
				private _marker = getText(_x >> "marker");
				private _coord = getMarkerPos _marker;
	        	if !( _coord isEqualTo [0,0,0] ) then {
	        		private _hidden = ( getNumber (_x >> "hiddeMarker") > 0 );
	        		#ifdef DEBUG
					private _debug = format["creating marker on %1 (hidden: %2)", _marker, _hidden];
    				debug(LL_INFO, _debug);
    				#endif
	        		_marker = format["VD_marker_%1", _marker];
					createMarker [_marker, _coord];
					_marker setMarkerColor _color;
					_marker setMarkerShape "ICON";
					_marker setMarkerType (["vehicleDeco", "markerIcon"] call core_fnc_getSetting);
					_marker setMarkerText (["vehicleDeco", "markerText"] call core_fnc_getSetting);
					if ( _hidden ) then { _marker setMarkerAlpha 0; };
					DAVD_markers pushback _marker;
				#ifdef DEBUG
				} else {
					private _debug = format["marker '%1' was not found", _marker];
    				debug(LL_ERR, _debug);
    			#endif
				};
			};
		} forEach _actions;
	} forEach _x;
} forEach [BA_npc, BA_obj, BA_veh];

nil