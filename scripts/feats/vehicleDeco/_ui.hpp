 /*
	DirectAction Vehicle Deco
*/

#include "ui\define.hpp"

class vehicleDeco {
	idd = VD_IDD;
	name= "vehicleDeco";
	scriptName= "vehicleDeco";
	movingEnable = false;
	enableSimulation = true;
	onLoad = "[] spawn vehicleDeco_fnc_init;";
	onUnload = "[] call vehicleDeco_fnc_destroy;";
	class Controls {
		/*
		class presets : daControlGroup {
			idc = PRESET_IDC;
			x = safeZoneX + SPACE;
			y = safeZoneY + safeZoneH - (2 * LINE_HEIGHT) - (2 * SPACE);
			w = (6 * BUTTON_WIDTH) + SPACE;
			h = (2 * LINE_HEIGHT) + SPACE;
			class Controls {
				class select : daCombo {
					idc = PRESET_SELECT;
					w = BUTTON_WIDTH * 5;
					h = LINE_HEIGHT;
					onLBSelChanged = "_this call vehicleDeco_fnc_evtPreset;";
				};
				class name : daEdit {
					idc = PRESET_NAME;
					y = LINE_HEIGHT + SPACE;
					w = BUTTON_WIDTH * 5;
					h = LINE_HEIGHT;
					onKillFocus = "_this call vehicleDeco_fnc_evtName;";
				};
				class remove : daTxtButton {
					idc = PRESET_REMOVE;
					x = (BUTTON_WIDTH * 5) + SPACE;
					w = BUTTON_WIDTH;
					h = LINE_HEIGHT;
					text = "Remove";
					action = "call vehicleDeco_fnc_evtRemove;";
				};
				class save : daTxtButton {
					idc = PRESET_SAVE;
					x = (BUTTON_WIDTH * 5) + SPACE;
					y = LINE_HEIGHT + SPACE;
					w = BUTTON_WIDTH;
					h = LINE_HEIGHT;
					text = "Save";
					action = "call vehicleDeco_fnc_evtSave;";
				};
			};
		};
		*/
		class color : daCombo {
			idc = COLOR_IDC;
			x = safeZoneX + SPACE;
			y = safeZoneY + SPACE;
			w = BUTTON_WIDTH * 3;
			h = LINE_HEIGHT;
			onLBselChanged = "_this call vehicleDeco_fnc_evtTexture";
		};
		class anims : daControlGroup {
			idc = ANIMS_IDC;
			x = safeZoneX + safeZoneW - LINE_HEIGHT - (2 * BUTTON_WIDTH) - (2 * SPACE);
			y = safeZoneY + SPACE;
			w = LINE_HEIGHT + SPACE + (2 * BUTTON_WIDTH);
			h = safeZoneH - LINE_HEIGHT - SPACE;
			class controls {};
		};
		class close : daTxtButton {
			x = safeZoneX + safeZoneW - BUTTON_WIDTH - SPACE;
			y = safeZoneY + safeZoneH - LINE_HEIGHT - SPACE;
			w = BUTTON_WIDTH;
			h = LINE_HEIGHT;
			text = "Cancel";
			action = "call vehicleDeco_fnc_evtCancel;closeDialog 0;";
		};
		class apply : daTxtButton {
			idc = APPLY_IDC;
			x = safeZoneX + safeZoneW - (2 * BUTTON_WIDTH) - (2 * SPACE);
			y = safeZoneY + safeZoneH - LINE_HEIGHT - SPACE;
			w = BUTTON_WIDTH;
			h = LINE_HEIGHT;
			text = "Apply";
			action = "closeDialog 0;";
		};
	};
};
