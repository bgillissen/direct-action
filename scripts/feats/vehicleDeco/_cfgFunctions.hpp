class vehicleDeco {
	tag = "vehicleDeco";
	class functions {
		class condition { file = "feats\vehicleDeco\condition.sqf"; };
		class distThread { file = "feats\vehicleDeco\distThread.sqf"; };
		class open { file = "feats\vehicleDeco\open.sqf"; };
		class playerInit { file = "feats\vehicleDeco\playerInit.sqf"; };
		class serverDestroy { file = "feats\vehicleDeco\serverDestroy.sqf"; };
		class serverInit { file = "feats\vehicleDeco\serverInit.sqf"; };

		class destroy { file = "feats\vehicleDeco\ui\destroy.sqf"; };
		class hasChanged { file = "feats\vehicleDeco\ui\hasChanged.sqf"; };
		class init { file = "feats\vehicleDeco\ui\init.sqf"; };
		class update { file = "feats\vehicleDeco\ui\update.sqf"; };

		class evtCancel { file = "feats\vehicleDeco\ui\events\cancel.sqf"; };
		class evtName { file = "feats\vehicleDeco\ui\events\name.sqf"; };
		class evtPreset { file = "feats\vehicleDeco\ui\events\preset.sqf"; };
		class evtAnim { file = "feats\vehicleDeco\ui\events\anim.sqf"; };
		class evtTexture { file = "feats\vehicleDeco\ui\events\texture.sqf"; };
		class evtRemove { file = "feats\vehicleDeco\ui\events\remove.sqf"; };
		class evtSave { file = "feats\vehicleDeco\ui\events\save.sqf"; };
	};
};
