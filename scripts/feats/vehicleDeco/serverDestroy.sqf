/*
@filename: feats\vehicleDeco\serverDestroy.sqf
Author:
	Ben
Description:
	run on server,
	remove map markers
 */
 
{ deleteMarker _x; } forEach DAVD_markers;

nil