/*
@filename: feats\vehicleDeco\distThread.sqf
Author:
	ben
Description:
	run on player,
	check the vehicle player is configuring is still close to the marker.
*/

params ["_pos", "_radius", "_veh"];

private _ok = true;

while { _ok } do {
	
	_ok = alive _veh;
	if ( _ok ) then { _ok = ( (_veh distance _pos) <= _radius ); };
};

closeDialog 0;