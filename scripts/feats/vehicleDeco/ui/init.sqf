/*
@filename: feats\vehicleDeco\ui\init.sqf
Author:
	ben
Description:
	run on player,
	init vehicle appearance dialog
*/

#include "define.hpp"
#define EMPTY_ARRAY []

disableSerialization;

DAVD_cam = "camera" camCreate (getPos player);
DAVD_cam cameraEffect ["Internal", "BACK"];
DAVD_cam camSetTarget DAVD_veh;
DAVD_cam camSetRelPos DAVD_camPos;
DAVD_cam camSetFov 1;
cameraEffectEnableHUD true;
DAVD_cam camCommit 0; 

waitUntil { !isNull findDisplay VD_IDD };

if ( (isNull DAVD_veh) || !(alive DAVD_veh) ) exitWith { closeDialog 0; };

private _dsp = findDisplay VD_IDD;
private _vType = typeOf DAVD_veh;
private _cfg = (configFile >> "cfgVehicles" >> _vtype);

DAVD_startAnims = EMPTY_ARRAY;
DAVD_startTextures = getObjectTextures DAVD_veh;
DAVD_uiPreset = "";

private _textureMatch = {
	params ["_ar1", "_ar2"];
	private _bool = true;
	{
		private _v1 = toLower _x;
		if !( (_v1 select [0,1]) isEqualTo "\" ) then { _v1 = format["%1%2", "\", _v1]; };
		private _v2 = toLower (_ar2 select _forEachIndex);
		if !( (_v2 select [0,1]) isEqualTo "\" ) then { _v2 = format["%1%2", "\", _v2]; };
		if !( _v1 isEqualTo _v2 ) exitWith { _bool = false; };
	} forEach _ar1;
	_bool
};


//colors
private _colorCtrl = _dsp displayCtrl COLOR_IDC;
private _c = 0;
private _idx = -1;
{
	private _bool = false;
	private _factions = getArray(_x >> "factions");
	private _textures = getArray(_x >> "textures");
	{
		private _side = getNumber(configFile >> "cfgFactionClasses" >> _x >> "side");
		_side = _side call common_fnc_numberToSide;
		if ( (_side in ALLIES) || (_side isEqualTo PLAYER_SIDE) ) exitWith { _bool = true; };		
	} forEach _factions;
	if ( _bool ) then {
		_colorCtrl lbAdd getText(_x >> "displayName");
		_colorCtrl lbsetData [_c, (configName _x)];
		if ( (_idx < 0) && ([_textures, DAVD_startTextures] call _textureMatch) ) then { _idx = _c; };
		_c = _c + 1;
	};
//} forEach ("true" configClasses(_cfg >> "textureSources"));
} forEach (configProperties [_cfg >> "textureSources", "isClass _x", true]);

if ( _idx >= 0 ) then {
	DAVD_uiNoEvents = true; 
	_colorCtrl lbSetCurSel _idx;
	DAVD_uiNoEvents = false; 
};

//animations
private _anims = _dsp displayCtrl ANIMS_IDC;
private _idc = ANIM_BASE;
private _idx = -1;
{
	private _desc = getText(_x >> "displayName");
	if !( _desc isEqualTo "" ) then {
		private _ctrl = _dsp ctrlCreate ["animEntry", _idc, _anims];
		private _pos = ctrlPosition _ctrl;
		_pos set [1, ((_idc - ANIM_BASE) * LINE_HEIGHT)];
		_ctrl ctrlSetPosition _pos;
		_ctrl ctrlCommit 0;
		(_ctrl controlsGroupCtrl ANIM_DESC) ctrlSetText _desc;
		(_ctrl controlsGroupCtrl ANIM_SOURCE) ctrlSetText (configName _x);
		private _bool = ( (DAVD_veh animationPhase (configName _x)) > 0 );
		DAVD_startAnims pushback _bool;
		if ( _bool ) then {
			DAVD_uiNoEvents = true;
			(_ctrl controlsGroupCtrl ANIM_CKB) cbSetChecked true; 
			DAVD_uiNoEvents = false;
		};
		_idc = _idc + 1;
	};
//} forEach ("true" configClasses(_cfg >> "animationSources"));
} forEach (configProperties [_cfg >> "animationSources", "isClass _x", true]);

[_dsp] call vehicleDeco_fnc_update;