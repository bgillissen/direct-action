
#include "define.hpp"

class animEntry : daControlGroup {
	w = LINE_HEIGHT + SPACE + (2 * BUTTON_WIDTH);
	h = LINE_HEIGHT;
	class controls {
		class bcg : daContainer {
			w = LINE_HEIGHT + SPACE + (2 * BUTTON_WIDTH);
			h = LINE_HEIGHT;
		};
		class ckb : daCheckbox {
			idc = ANIM_CKB;
			w = LINE_HEIGHT;
			h = LINE_HEIGHT;
			onCheckedChanged = "_this call vehicleDeco_fnc_evtAnim;";
		};
		class desc : daText {
			idc = ANIM_DESC;
			x = LINE_HEIGHT + SPACE;
			w = 2 * BUTTON_WIDTH;
			h = LINE_HEIGHT;
		};
		class source : daText {
			idc = ANIM_SOURCE;
			w = 0;
			h = 0;
		};
	};
};
