/*
@filename: feats\vehicleDeco\ui\hasChanged.sqf
Author:
	ben
Description:
	run on player,
*/

#include "define.hpp"

disableSerialization;

params [["_dsp", displayNull]];

if ( isNull _dsp ) then { _dsp = findDisplay VD_IDD; };
if ( isNull _dsp ) exitWith {};