/*
@filename: feats\vehicleDeco\ui\anim.sqf
Author:
	ben
Description:
	run on player,
*/

#include "..\define.hpp"

if ( DAVD_uiNoEvents ) exitWith {};

disableSerialization;

params ["_ctrl", "_state"];

DAVD_veh animate [(ctrlText ((ctrlParentControlsGroup _ctrl) controlsGroupCtrl ANIM_SOURCE)), _state];