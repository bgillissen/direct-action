/*
@filename: feats\vehicleDeco\ui\texture.sqf
Author:
	ben
Description:
	run on player,
*/

#include "..\define.hpp"

if ( DAVD_uiNoEvents ) exitWith {};

disableSerialization;

params ["_ctrl", "_idx"];

{
	DAVD_veh setObjectTextureGlobal [_forEachIndex, _x];
} forEach getArray(configFile >> "cfgVehicles" >> (typeOf DAVD_veh) >> "textureSources" >> (_ctrl lbData _idx) >> "textures");
