/*
@filename: feats\vehicleDeco\ui\cancel.sqf
Author:
	ben
Description:
	run on player,
*/

#include "..\define.hpp"


{
	DAVD_veh setObjectTextureGlobal [_forEachIndex, _x];
} forEach DAVD_startTextures;

private _c = 0;
{
	private _desc = getText(_x >> "displayName");
	if !( _desc isEqualTo "" ) then {
		DAVD_veh animate [(configName _x), ([0,1] select (DAVD_startAnims select _c))];
		_c = _c + 1;
	}; 
} forEach ("true" configClasses(configFile >> "cfgVehicles" >> (typeOf DAVD_veh) >> "animationSources"));
