
#define TOT_WIDTH 1.2
#define YOFFSET 0
#define SPACE 0.01
#define BUTTON_WIDTH (6.25 / 40)
#define LINE_HEIGHT (1 / 25)
#define PYLON_WIDTH (6.25 / 32)
#define RATIO (4 / 3)

#define VD_IDD 10000
#define TITLE_IDC 10001
#define APPLY_IDC 10002

#define PRESET_IDC 11000
#define PRESET_SELECT 11001
#define PRESET_NAME 11002
#define PRESET_SAVE 11003
#define PRESET_REMOVE 11004

#define COLOR_IDC 12000
#define ANIMS_IDC 13000
#define ANIM_BASE 13100
#define ANIM_CKB 13001
#define ANIM_DESC 13002
#define ANIM_SOURCE 13003