/*
@filename: feats\vehicleDeco\ui\destroy.sqf
Author:
	ben
Description:
	run on player,
	destroy vehicle appearance dialog
*/

DAVD_veh setVariable ["DAVD_lock", false, true];

DAVD_cam cameraEffect ["terminate","back"];
camDestroy DAVD_cam;

terminate DAVD_thread;