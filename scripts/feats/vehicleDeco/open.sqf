/*
@filename: feats\vehicleDeco\open.sqf
Author:
	ben
Description:
	run on player,
	configure and open the vehicle appearance dialog
*/

#include "_debug.hpp"

params ["_marker", "_camPos", "_type"];

private _pos = getMarkerPos _marker;

if ( _pos isEqualTo [0,0,0] ) exitWith {};

private _radius = ["vehicleDeco", "radius"] call core_fnc_getSetting;

private _objs = _pos nearEntities [_type, _radius];

if ( (count _objs) isEqualTo 0 ) exitWith {};

DAVD_veh = objNull;
DAVD_camPos = [0,0,0];
DAVD_marker = "";

private _veh = _objs select 0;
private _vType = typeOf _veh;
private _cfg = (configFile >> "cfgVehicles" >> _vtype);

{
	if !( getText(_x >> "displayName") isEqualTo "" ) exitWith { DAVD_veh = _veh; };
} forEach (configProperties [_cfg >> "animationSources", "isClass _x", true]);

if ( isNull DAVD_veh ) then {
	{
		private _factions = getArray(_x >> "factions");
		{
			private _side = getNumber(configFile >> "cfgFactionClasses" >> _x >> "side");
			_side = _side call common_fnc_numberToSide;
			if ( (_side in ALLIES) || (_side isEqualTo PLAYER_SIDE) ) exitWith { DAVD_veh = _veh };
		} forEach _factions;
		if !( isNull DAVD_veh ) exitWith {};
	} forEach (configProperties [_cfg >> "textureSources", "isClass _x", true]);
};

if ( isNull DAVD_veh ) exitWith {};

DAVD_veh setVariable ["DAVD_lock", true, true];
DAVD_camPos = _camPos;
DAVD_marker = _marker;

createDialog "vehicleDeco";

DAVD_thread = [_pos, _radius, DAVD_veh] spawn vehicleDeco_fnc_distThread;