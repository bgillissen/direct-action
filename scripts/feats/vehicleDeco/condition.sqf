 /*
@filename: feats\vehicleDeco\condition.sqf
Author:
	ben
Description:
	run on player,
	action condition
*/

#include "_debug.hpp"

params ["_marker", "_type"];

private _pos = getMarkerPos _marker;

if ( _pos isEqualTo [0,0,0] ) exitWith { false };

private _radius = ["vehicleDeco", "radius"] call core_fnc_getSetting;

private _objs = _pos nearEntities [_type, _radius];

if ( (count _objs == 0) ) exitWith { false };

private _veh = (_objs select 0);

if ( _veh getVariable ["DAVD_lock", false] ) exitWith { false };

private _vType = typeOf _veh;
private _cfg = (configFile >> "cfgVehicles" >> _vtype);

private _bool = false;
{
	if !( getText(_x >> "displayName") isEqualTo "" ) exitWith { _bool = true; };
} forEach (configProperties [_cfg >> "animationSources", "isClass _x", true]);

{
	private _factions = getArray(_x >> "factions");
	{
		private _side = getNumber(configFile >> "cfgFactionClasses" >> _x >> "side");
		_side = _side call common_fnc_numberToSide;
		if ( (_side in ALLIES) || (_side isEqualTo PLAYER_SIDE) ) exitWith { _bool = true; };
	} forEach _factions;
	if ( _bool ) exitWith {};
} forEach (configProperties [_cfg >> "textureSources", "isClass _x", true]);

_bool