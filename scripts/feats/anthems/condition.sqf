/*
@filename: feats\anthems\condition.sqf
Author:
	Ben
Description:
	run on player,
	determine if a play anthems action is available or not
*/

params ["_sfx", "_isPlay"];

if !( isClass(configFile >> "CfgVehicles" >> _sfx) ) exitWith { false };

private _rank = (["anthems", "rank"] call core_fnc_getSetting);
if ( player getVariable ["MD_rank", 0] < _rank ) exitWith { false };

if ( isNil "soundObjects" ) then {
    soundObjects = [];
    publicVariable "soundObjects";
};

if ( _isPlay ) exitWith {
    private _cooldown = (["anthems", "cooldown"] call core_fnc_getSetting);
    if ( isNil "lastSoundEndTime" ) exitWith { ( (count soundObjects) == 0 ) };
	( (serverTime > (lastSoundEndTime + _cooldown)) && ((count soundObjects) == 0) && !noAnthems )
};

if ( isNil "lastSoundClass" ) exitWith { false };
    
( (lastSoundClass isEqualTo _sfx) && ((count soundObjects) > 0) )