/*
@filename: feats\anthems\addCountryFlagActions.sqf
Author:
	Ben
Description:
	run on player,
	add play anthem actions to countryFlags
*/

#include "_debug.hpp"

{
    if ( (_x getVariable["anthemAction", -1]) isEqualto -1 ) then {
        private _country = tolower(_x getVariable "country");
        private _sfx = format["Sound_anthem_%1", _country];
        if ( isClass(configFile >> "CfgVehicles" >> _sfx) ) then {
        	private _name =  getText(configFile >> "CfgVehicles" >> _sfx >> "displayName");
	        #ifdef DEBUG
	        private _debug = format["adding play %1 (%2) action to countryFlag %3", _name, _sfx, _country];
	    	debug(LL_DEBUG, _debug);
	        #endif
	    	private _action = format["Play %1", _name];
	        private _cond = format["['%1', true] call anthems_fnc_condition", _sfx];
			private _id = _x addAction [_action, {call anthems_fnc_play}, _sfx, 0, false, true, "", _cond, 4];
	        
	        private _action = format["Stop %1", _name];
	        private _cond = format["['%1', false] call anthems_fnc_condition", _sfx];
			private _id = _x addAction [_action, {call common_fnc_stopSound}, "", 0, false, true, "", _cond, 4];
	        
	    	_x setVariable ["anthemAction", _id];
		#ifdef DEBUG
		} else {
	       private _debug = format["missing vehicle entry for the anthem of %1 (%2)", _country, _sfx];
	       debug(LL_DEBUG, _debug);
        #endif 
        };
	};    
} forEach countryFlags;