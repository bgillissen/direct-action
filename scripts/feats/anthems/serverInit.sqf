/*
@filename: feats\anthems\serverInit.sqf
Author:
	Ben
Description:
	run on server,
	define the noAnthems public variable
*/

#include "_debug.hpp"

noAnthems = false;
publicVariable "noAnthems";

nil