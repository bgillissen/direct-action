/*
@filename: feats\anthems\play.sqf
Author:
	Ben
Description:
	run on player,
	play the given anthem
*/

#include "_debug.hpp"

params ["_flag", "_caller", "_id", "_sfx"];

if !( isClass(configFile >> "CfgVehicles" >> _sfx) ) exitWith {
    private _msg = (["anthems", "failMsg"] call core_fnc_getSetting);
	systemChat format[_msg, _sfx];
};

[_sfx, serverTime] call common_fnc_playSound;

private _name = getText(configFile >> "CfgVehicles" >> _sfx >> "displayName");
private _msg = (["anthems", "playMsg"] call core_fnc_getSetting);
[1, format[_msg, (name _caller), _name], ["HQ", PLAYER_SIDE]] call global_fnc_chat;