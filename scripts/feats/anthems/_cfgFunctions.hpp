class anthems {
	tag = "anthems";
	class functions {
		class addcountryFlagActions { file="feats\anthems\addCountryFlagActions.sqf"; };
		class condition { file="feats\anthems\condition.sqf"; };
		class play { file="feats\anthems\play.sqf"; };
		class playerInit { file="feats\anthems\playerInit.sqf"; };
		class serverInit { file="feats\anthems\serverInit.sqf"; };
	};
};
