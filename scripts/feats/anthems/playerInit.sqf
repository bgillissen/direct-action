/*
@filename: feats\anthems\playerInit.sqf
Author:
	Ben
Description:
	run on player,
	add the play anthem on base things with a anthem action,
	add disable/enable anthem play on things with a curator action
	add play anthem action on each countryFlags
	register an event handler on countryFlags to re-add the actions
*/

#include "_debug.hpp"

{
	{
        _x params ["_thing", "_actions"];
		{
			if ( (configName _x) isEqualTo "anthem" ) then {
                
	        	private _sfx = getText(_x >> "sfx");
                if ( isClass(configFile >> "CfgVehicles" >> _sfx) ) then {
       				private _name =  getText(configFile >> "CfgVehicles" >> _sfx >> "displayName");
	        		#ifdef DEBUG
	                private _debug = format["adding play/stop %1 (%2) action to %3", _name, _sfx, _thing];
	    			debug(LL_DEBUG, _debug);
	    			#endif
	   				private _action = format["Play %1", _name];
	       			private _cond = format["['%1', true] call anthems_fnc_condition", _sfx];
					_thing addAction [_action, {call anthems_fnc_play}, _sfx, 0, false, true, "", _cond, 4];
	        
	       			private _action = format["Stop %1", _name];
	       			private _cond = format["['%1', false] call anthems_fnc_condition", _sfx];
					_thing addAction [_action, {call common_fnc_stopSound}, "", 0, false, true, "", _cond, 4];
				} else {
                    #ifdef DEBUG
	                private _debug = format["no vehicle entry for sfx '%1",_sfx];
	    			debug(LL_WARN, _debug);
	    			#endif
                };
			};
            if ( (configName _x) isEqualTo "curator" ) then {
                private _action = "Prevent anthems to be played";
				_thing addAction [_action, {missionNamespace setVariable["noAnthems", true, true]}, "", 0, false, true, "", "(isAssigned && !noAnthems)", 4];
				private _action = "Allow anthems to be played";
				_thing addAction [_action, {missionNamespace setVariable["noAnthems", false, true]}, "", 0, false, true, "", "(isAssigned && noAnthems)", 4];
            };
		} forEach _actions;
    } forEach _x;
} forEach [BA_obj, BA_npc, BA_veh];

call anthems_fnc_addCountryFlagActions;

if ( isNil "anthemPVEH" ) then {
    #ifdef DEBUG
    debug(LL_DEBUG, "add coutryFlags variable event handler");
    #endif
	anthemPVEH = ["countryFlags", {call anthems_fnc_addCountryFlagActions;}] call pveh_fnc_add;
};

nil