/*
@filename: feats\vehicleRespawn\playerInit.sqf
Author:
	Ben
Description:
	run on player,
	build a stack with all the arsenals
*/

VR_clearNear = [];

{
	{
		_x params ["_thing", "_actions"];
		{
			if ( (configName _x) isEqualTo "arsenal" ) then { VR_clearNear pushback _thing; };
		} forEach _actions;
	} forEach _x;
} forEach [BA_npc, BA_obj, BA_veh];

nil