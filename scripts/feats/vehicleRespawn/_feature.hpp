class vehicleRespawn : feat_base  {
	class player : featPlayer_base {
		class init : featEvent_enable {};
	};
	class server : featServer_base {
		class init : featEvent_enable { thread = 1; };
		class preInit : featEvent_enable {};
		class destroy : featEvent_enable {};
	};
};
