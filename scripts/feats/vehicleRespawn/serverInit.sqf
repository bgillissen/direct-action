/*
@filename: feats\vehicleRespawn\serverInit.sqf
Author:
	ben
Description:
	run on server,
	loop through all the vehicles we got to monitor and respawn them when needed
*/

#include "_debug.hpp"

private _grp = createGroup CIVILIAN;
_grp setVariable ['NOLB', true];
VR_cleanUnit = _grp createUnit ["C_man_polo_2_F_afro",[0,0,2000],[],0,"NONE"];
VR_cleanUnit setVariable ['NOAI', true, true];
VR_cleanUnit allowDamage false;
VR_cleanUnit enableSimulation false;
VR_cleanUnit hideObjectGlobal true;

private _dist = ["VehicleRespawn_distance"] call core_fnc_getParam;
private _queue = [];

private _delay = ["vehicleRespawn", "checkDelay"] call core_fnc_getSetting;
private _distFromSpawn = ["vehicleRespawn", "distanceFromSpawn"] call core_fnc_getSetting;

private _triggerSize = ["vehicleRepair", "triggerSize"] call core_fnc_getSetting;

#ifdef DEBUG	
private _debug = format["thread is running with %1s delay and %1 vehicle(s)) monitored", _delay, count(VR)];
debug(LL_DEBUG, _debug);
#endif	

while { true } do {
	
	sleep _delay;

	{	
        _x params ["_veh", "_delay", "_poolName", "_marker", "_atl", "_actions", "_jip", "_preset"];
		private _inqueue = false;
		{
			_x params ["_qid", "_respawnAt"];
			if ( _forEachIndex == _qid) then { _inqueue = true; };
			if ( time > _respawnAt ) then {
                private _entry = VR select _qid;
				#ifdef DEBUG
				private _debug = format["respawning veh %1 (%2)", _qid, (_entry select 3)];
				debug(LL_INFO, _debug);
				#endif
				([_qid, _entry select 0, _entry select 2, _entry select 3, _entry select 4, _entry select 5, _entry select 7] call vehicleRespawn_fnc_spawn) params ["_nVeh", ["_nJip", ""]];
				_entry set [0,_nVeh];
                if !( _nJip isEqualTo "" ) then {
                    nil remoteExec["", (_entry select 6)];
                    _entry set [6, _nJip];
                };
				VR set [_qid, _entry];
                publicVariable "VR";
				_queue = _queue - [_x];
			};
		} count _queue;
		
		if !( _inqueue ) then {
			if !( alive _veh ) then {
				#ifdef DEBUG
				private _debug = format["veh %1 (%2) has been added to queue (dead)", _forEachIndex, _marker];
				debug(LL_DEBUG, _debug);
				#endif
				_queue append [[_forEachIndex, (time + _delay)]];
			} else {
				if (({ alive _x } count (crew _veh)) == 0) then {
                    private _pos = getMarkerPos _marker;
					if ((_veh distance _pos) > _distFromSpawn) then {
						if (({(_veh distance _x) < _dist} count (allPlayers - entities "HeadlessClient_F")) < 1) then {
							#ifdef DEBUG
							private _debug = format["veh %1 (%2) has been added to queue (alone)", _forEachIndex, _marker];
							debug(LL_DEBUG, _debug);
							#endif
							_queue pushback [_forEachIndex, (time + _delay)];
						};
					} else {
                        if ( !(canMove _veh) && ({ (_veh distance _x) < _triggerSize } count VR_triggers == 0) ) then {
                            #ifdef DEBUG
							private _debug = format["veh %1 (%2) has been added to queue (can't move)", _forEachIndex, _marker];
							debug(LL_DEBUG, _debug);
							#endif
							_queue pushback [_forEachIndex, (time + _delay)];
                        };
					};
				};
			};
		};
	} forEach VR;
};

nil