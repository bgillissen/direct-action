/*
@filename: feats\vehicleRespawn\spawn.sqf
Author:
	ben
Description:
	run on server
	respawn an abandoned/destroyed vehicle
*/

params ["_qid", "_veh", "_poolName", "_marker", "_atl", "_actions", "_preset", ["_type", ""]];

private _pos = getMarkerPos _marker;
_pos set [2, _atl];
private _dir = markerDir _marker;

if ( (alive _veh) && !(_veh isKindOf "Plane") && (_type isEqualTo "") ) exitWith {
    _veh allowDamage false;
    _veh hideObjectGlobal true;
    //private _grp = createGroup CIVILIAN;
	//private _tmpUnit = _grp createUnit ["C_man_polo_2_F_afro",[0,0,2000],[],0,"NONE"];
    //_tmpUnit allowDamage false;
	//_tmpUnit hideObjectGlobal true; 
    if ( ({ !alive _x } count (crew _veh)) > 0 ) then {
    	VR_cleanUnit enableSimulation true;
		{
            _x params ['_unit', '_role','_index', '_turretPath', '_personTurret'];
    		if !( isNull _unit ) then {
                switch( _role ) do {
                    case 'driver';
                    case 'Driver': { VR_cleanUnit moveInDriver _veh;moveOut VR_cleanUnit; };
                    case 'commander';
                    case 'Commander': { VR_cleanUnit moveInCommander _veh;moveOut VR_cleanUnit; };
                    case 'gunner';
                    case 'Gunner': { VR_cleanUnit moveInGunner _veh;moveOut VR_cleanUnit; };
                    case 'turret';
                    case 'Turret': { VR_cleanUnit moveInTurret [_veh, _turretPath];moveOut VR_cleanUnit; };
                    case 'cargo';
                    case 'Cargo': { VR_cleanUnit moveInCargo [_veh, _index];moveOut VR_cleanUnit; };
                };
			};
    	} forEach (fullcrew _veh);
    	VR_cleanUnit enableSimulation false;
    	VR_cleanUnit setPos [0,0,2000];
    };
    _veh setDamage 0;
	_veh setVehicleAmmo 1;
    _veh setFuel 1;
	_veh engineOn false;
	_veh setCollisionLight false;
	_veh setPilotLight false;
    _veh hideObjectGlobal false;    
	_veh setDir _dir;
	_veh setPos _pos;
	_veh allowDamage true;    
    [_veh]
};

if !( isNull _veh ) then {
    private _isLocal = local _veh; 
	deleteVehicle _veh;
    if !( _isLocal ) then { sleep 2; }; 
};

_veh = objNull;
if ( _type isEqualTo "" ) then {
	private _pool = missionNamespace getVariable format["BV_%1", _poolName];
    _veh = createVehicle [(selectRandom _pool), [0,0,2000], [], 0, "NONE"];    
} else {
    _veh = createVehicle [_type, [0,0,2000], [], 0, "NONE"];
};
_veh allowDamage false;
_veh setVariable ["DAVL_preset", _preset, true];
_veh setVariable ["VR_qid", _qid, true];
_veh setDir _dir;
_veh setPos _pos;
_veh allowDamage true;

private _jip = [_veh, _poolName, _actions, _preset] call vehicleRespawn_fnc_setup;

[_veh, _jip]