/*
@filename: feats\vehicleRespawn\serverDestroy.sqf
Author:
	Ben
Description:
	this run on server
	kill the vehicleRespawn serverInit thread,
	remove monitored vehicles
 */

if ( count(_this) == 0 ) exitWith { nil };

params ["_when", "_thread"];

if ( scriptDone _thread ) exitWith { nil };

terminate _thread;

waitUntil { scriptDone _thread };

deleteVehicle VR_cleanUnit;

{ 
	deleteVehicle (_x select 0); 
	nil remoteExec["", (_x select 6)];
} forEach VR;

VR = [];

nil