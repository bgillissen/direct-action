/*
@filename: feats\vehicleRespawn\setup.sqf
Author:
	Ben
Description:
	run on server,
	called each time a base vehicle (re)spawn.
*/

#include "_debug.hpp"

params ["_veh", "_poolName", "_actions", "_preset"];

private _type = typeOf _veh;

private _args = [_veh];
_args append (missionNamespace getVariable format["VC_%1", _poolName]); 
_args call common_fnc_setCargo;
_args = nil;

{
	if ( (_type isKindOf _x) && !(_type in BV_heliMedEvac) ) then {
		_veh setVariable ["supplyDrop", true, true];
	};
} count SD_vehicles;

//Actions
{
	if ( configName(_x) isEqualTo "paint" ) then {
		#ifdef DEBUG
		private _debug = format["Changing color of '%1'", _veh];
		debug(LL_DEBUG, _debug)
		#endif
		_veh setObjectTextureGlobal [0, getText(_x >> "color")];
	};
	if ( configName(_x) isEqualTo "centerOfMass" ) then {
		private _do = false;
		{
			if ( _veh isKindOf _x ) exitWith { _do = true; };
		} forEach getArray(_x >> "kinds");
		if ( _do ) then {
			#ifdef DEBUG
			private _debug = format["Changing center of mass of '%1'", _veh];
			debug(LL_DEBUG, _debug)
			#endif
			private _new = [];
			private _ratios = getArray(_x >> "ratios");
			{ 
				_new pushBack ( _x * (_ratios select _forEachIndex));
			} forEach (getCenterOfMass _veh);
			_veh setCenterOfMass _new;
		};
	};
} forEach _actions;

//UAV specific
if ( unitIsUAV _veh ) then {
	{ _veh deleteVehicleCrew _x; } forEach (crew _veh);
	createVehicleCrew _veh;
    private _grp = createGroup [PLAYER_SIDE, true];
    _grp setVariable ['NOLB', true, true];
    _grp setGroupIdGlobal [["uav", allGroups, {groupId _this}] call common_fnc_getUniqueName];
    { 
    	_x setVariable ["NOAI", true, true];
		[_x] joinSilent _grp;
	} forEach (crew _veh);
    [_grp, true] call dynSim_fnc_setTrigger;
    if !( isNil "BP_zones" ) then {
    	_veh addEventHandler ["Fired", {_this call baseProtection_fnc_uavShoot;}];
    };
};

//dataLink
if ( isClass(configFile >> "cfgVehicles" >> _type >> "Components" >> "SensorsManagerComponent") ) then {
	_veh setVehicleReportRemoteTargets true;
    _veh setVehicleReceiveRemoteTargets true; 
} else {
	_veh setVehicleReportRemoteTargets false;
    _veh setVehicleReceiveRemoteTargets false; 
};

//pylon setting
if !( _preset isEqualTo "" ) then {
    private _baseConf = (configFile >> "cfgVehicles" >> _type >> "Components" >> "TransportPylonsComponent"); 
    private "_pylons";
    if ( isClass(_baseConf >> "Presets" >> _preset) ) then {
	 	_pylons = getArray( _baseConf >> "Presets" >> _preset >> "attachment");      
    } else {
		{
			_x params ["_name", "_class", "_id", "_ownerUid", "_ownerName", "_presetPylons"];
		    if ( _class isEqualTo _type ) then {
		    	if ( _id isEqualTo _preset ) exitWith { _pylons = _presetPylons; };
		    };    
		} forEach DAVL_serverPresets;
	};
    if !( isNil "_pylons" ) then {
    	{
            private _turret = getArray (_x >> "turret");
            private _mag = "";
            if ( _forEachIndex < (count _pylons) ) then {
            	_mag = _pylons select _forEachIndex;    
            };
			_veh setPylonLoadout [(_forEachIndex + 1), _mag, false, _turret];
		} forEach ( "true" configClasses(_baseConf >> "Pylons") );
	};
};

//add to Zeus
[[_veh], true] call curator_fnc_addEditable;

//clearInventory JIP
_jip = _veh remoteExec ["vehicleRespawn_fnc_addAction", 0, true];

_jip