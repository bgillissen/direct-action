/*
@filename: feats\vehicleRespawn\clearInventory.sqf
Author:
	Ben
Description:
	run on player,
	clear vehicle inventory
*/

#include "_debug.hpp"

params ["_veh", "_caller", "_id"];

clearWeaponCargoGlobal _veh;
clearMagazineCargoGlobal _veh;
clearItemCargoGlobal _veh;
clearBackpackCargoGlobal _veh;