/*
@filename: feats\vehicleRespawn\addAction.sqf
Author:
	ben
Description:
	run on player
	add clear inventory action to given veh
*/

if !( CTXT_PLAYER ) exitWith {};

if !( alive _this ) exitWith {};

_this addAction ["clean inventory", {call vehicleRespawn_fnc_clearInventory}, [], 0, false, true, "", "( ({( (_x distance _target) < 15 )} count VR_clearNear) > 0 )", 4];