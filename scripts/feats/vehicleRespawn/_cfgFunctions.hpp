class vehicleRespawn {
	tag = "vehicleRespawn";
	class functions {
		class addAction { file="feats\vehicleRespawn\addAction.sqf"; };
		class clearInventory { file="feats\vehicleRespawn\clearInventory.sqf"; };
		class monitor { file="feats\vehicleRespawn\monitor.sqf"; };
		class playerInit { file="feats\vehicleRespawn\playerInit.sqf"; };
		class serverDestroy { file="feats\vehicleRespawn\serverDestroy.sqf"; };
		class serverInit { file="feats\vehicleRespawn\serverInit.sqf"; };
		class serverPreInit { file="feats\vehicleRespawn\serverPreInit.sqf"; };
		class setup { file="feats\vehicleRespawn\setup.sqf"; };
		class spawn { file="feats\vehicleRespawn\spawn.sqf"; };
	};
};
