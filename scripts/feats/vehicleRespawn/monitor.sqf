/*
@filename: feats\vehicleRespawn\monitor.sqf
Credit: 
	Tophe for earlier monitor script
Author:
	Quiksilver, tweaked by ben
Description:
	run on server,
	add the given vehicle to the vehicle monitored by vehicleRespawn thread
*/

params ["_veh", "_delay", "_poolName", "_marker", "_atl", "_actions"];

private _jip = [_veh, _poolName, _actions, ""] call vehicleRespawn_fnc_setup;

private _qid = VR pushback [_veh, _delay, _poolName, _marker, _atl,_actions, _jip, ""];
_veh setVariable ["VR_qid", _qid, true];

publicVariable "VR";