/*
@filename: feats\_cfgNotifications.hpp
Author:
	Ben
Description:
		on all context,
		included by main.hpp,
		include features notifications
*/

class CfgNotifications {
	#ifdef use_artiSupport
	#include "artiSupport\_cfgNotifications.hpp"
	#endif

	#ifdef use_ia
	#include "ia\_cfgNotifications.hpp"
	#endif

	#include "tasks\_cfgNotifications.hpp"

	#include "zeusMission\_cfgNotifications.hpp"
};
