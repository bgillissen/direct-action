/*
@filename: feats\serverRules\playerPostInit.sqf
Author:
	Ben
Description:
	run on player,
	display the server's rules dialog on join.
Params:
	none
Environment:
	Paramaters
		serverRules
	Settings
		serverRules >> showOnce
		serverRules >> skipRank
		serverRules >> profileVariable
	missionNamespace:
		DOLOCK
		BLACKSCREEN
	profileNamespace:
		TFU_SRVRULES (use the value defined in settings)
Return:
	nothing	
*/

#include "_debug.hpp"

if ( (["serverRules"] call core_fnc_getParam) == 0 ) exitWith {
    #ifdef DEBUG
    debug(LL_INFO, "disabled by mission parameter");
    #endif
    nil
};

if ( (["serverRules", "skipHost"] call core_fnc_getSetting > 0) && CTXT_SERVER ) exitWith {
    #ifdef DEBUG
    debug(LL_DEBUG, "server host, abording");
    #endif
    nil
};

if !( isNil "SRVRULES_SHOWN" ) exitWith {
    #ifdef DEBUG
    debug(LL_DEBUG, "already displayed, abording");
    #endif
    nil
};

private _showOnce = (["serverRules", "showOnce"] call core_fnc_getSetting > 0);
private _profileVar = ["serverRules", "profileVariable"] call core_fnc_getSetting;
private _boolVar = format["%1_bool", _profileVar];
if ( profileNamespace getVariable [_boolVar, false] && _showOnce ) exitWith {
    #ifdef DEBUG
    debug(LL_DEBUG, "player already seen it, abording");
    #endif
};

private _skipRank = ["serverRules", "skipRank"] call core_fnc_getSetting;
private _rank = (player getVariable["MD_rank", 0]);
if ( (_rank >= _skipRank) && (_skipRank >= 0) ) exitWith {
    #ifdef DEBUG
    debug(LL_DEBUG, "player's rank is above skipRank, abording");
    #endif
};

private _skipLastSeenRank = ["serverRules", "skipLastSeenRank"] call core_fnc_getSetting;
private _skipLastSeen = ["serverRules", "skipLastSeen"] call core_fnc_getSetting;
private _timeVar = format["%1_last", _profileVar];
private _lastSeen = profileNamespace getVariable [_timeVar, 0];
if ( (_skipLastSeen > 0) && (_rank >= _skipLastSeenRank) && ((SRVRULES_stamp + _skipLastSeen - _lastSeen) > 0) ) exitWith {
 	#ifdef DEBUG
    debug(LL_DEBUG, "player's already seen it before lastSeen expires, abording");
    #endif   
};

if !( isNil "DOLOCK" ) then {
	waitUntil {
		sleep 0.1;
		!DOLOCK
	};
};

if ( BLACKSCREEN ) then { waitUntil { sleep 0.1;!BLACKSCREEN }; };

[true] spawn serverRules_fnc_display;

SRVRULES_SHOWN = true;

if ( _showOnce ) then { 
	profileNamespace setVariable [_boolVar, true];
    profileNamespace setVariable [_timeVar, SRVRULES_stamp];
    saveProfileNamespace; 
};

nil