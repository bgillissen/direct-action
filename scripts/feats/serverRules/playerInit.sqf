/*
@filename: feats\serverRules\playerInit.sqf
Description:
	run on client
	add action to base things to display the serverRules
Params:
	none
Environment:
	Paramaters
		serverRules
	Settings
		serverRules >> action
	missionNamespace
		BA_npc
		BA_obj
		BA_veh
*/

#include "_debug.hpp"

if ( (["serverRules"] call core_fnc_getParam) == 0 ) exitWith {
    #ifdef DEBUG
    debug(LL_INFO, "disabled by mission parameter");
    #endif
	nil
};

private _act = ["serverRules", "action"] call core_fnc_getSetting;

{
	{
		_x params ["_thing", "_actions"];
		{
			if ( (configName _x) isEqualTo "serverRules" ) then {
				#ifdef DEBUG
				private _debug = format["action has been added to %1", _thing];
				debug(LL_DEBUG, _debug);
				#endif
				_thing addAction [_act, {[false] spawn serverRules_fnc_display}, [], 0, false, true, "", "true", 4];
			};
		} forEach _actions;
	} forEach _x;
} forEach [BA_npc, BA_obj, BA_veh];

nil