class serverRules {
	showOnce = 0;
	profileVariable = "TFU_SRVRULES";
	skipLastSeen = 2592000; //30days in seconds
	skipLastSeenRank = 3; //private
	skipRank = -1;
	skipHost = 1;
	timeout = 10;
	action = "Show server rules";
	class dynamic {
		/*
		  Notice that you will have to activate file patching via -filePatching.
		  Otherwise loadFile will not load any files outside your mission folder.
		  For more info see CMA:DevelopmentSetup (since Arma 3 1.49+).
		  Also for mode 2, the url_fetch extension is required!
		*/
		mode = 2; //1 loadFile, 2 fetch_url
		src = "http://taskforceunicorn.com/api?do=getServerRules";
	};
	class display {
		countdown = ": <t font='PuristaBold'>%1</t> second(s) left";
		header = "<t size='1.4' color='#f0e68c' font='PuristaBold' align='center'>Task Force Unicorn Public Server Rules</t><br/>";
		rule = "<t size='1' color='#f0e68c'><t font='PuristaBold'>%1.</t> %2</t>";
		title = "<t size='0.85'>Server Rules %1</t>";
	};
};
