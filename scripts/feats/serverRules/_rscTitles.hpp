#include "_sizes.hpp"

class SRVRULES_RSCTITLES {
	idd = -1;
    fadeout = 0;
    fadein = 0;
    duration = 1e+1000;
	name= "SRVRULES_RSCTITLES";
	onLoad = "uiNamespace setVariable ['SRVRULES_RSCTITLES', _this select 0]";
	class controlsBackground {
		//class background : RscPicture {
		class background : daPicture {
			style = 0x90;
			tileH = 1;
			tileW = 1;
			text = "media\loading.jpg";
			colorText[] = {1,1,1,0.8};
			x = (MIDDLE - (WIDTH / 2));
			y = SPACE;
			w = WIDTH;
			h = HEIGHT;
		};
	};
	class controls {
		class rules : daStructuredText {
			idc = 88887;
			size = 0.040;
			lineSpacing = 3;
			x = (MIDDLE - (WIDTH / 2));
			y = SPACE;
			w = WIDTH;
			h = HEIGHT;
			colorText[] = {1,1,1,1};
			colorBackground[] = {0,0,0,0.5};
			text = "";
			font = "PuristaLight";
			shadow = 2;
		};
		class title : daStructuredText {
			idc = 88888;
			size = 0.040;
			lineSpacing = 3;
			x = (MIDDLE - (WIDTH / 2));
			y = -LINE_HEIGHT;
			w = WIDTH;
			h = LINE_HEIGHT;
			colorText[] = {1,1,1,1};
			colorBackground[] = {"(profilenamespace getvariable ['GUI_BCG_RGB_R',0.69])","(profilenamespace getvariable ['GUI_BCG_RGB_G',0.75])","(profilenamespace getvariable ['GUI_BCG_RGB_B',0.5])",1};
			text = "";
			font = "PuristaLight";
			shadow = 2;
		};
	};
};
