/*
@filename: feats\serverRules\display.sqf
Description:
	run on client,
	display the server rules
Params:
	_forced [BOOL] countdown before we allow player to close it
Environment:
	Settings
		serverRules >> display >> countdown
		serverRules >> display >> header
		serverRules >> display >> rule
		serverRules >> display >> title
		serverRules >> timeout
	missionNamespace
		SRVRULES_rules
	uiNamespace
		SRVRULES_RSCTITLES 
	ui
		SRVRULES_DIALOG
	rscTitles
		SRVRULES_RSCTITLE 
Return:
	nothing 
*/

#include "_debug.hpp"

params[["_forced", false]];

private _rules = [];
private _ruleDsp = ["serverRules", "display", "rule"] call core_fnc_getSetting;
_rules pushback (["serverRules", "display", "header"] call core_fnc_getSetting);
{ _rules pushback format[_ruleDsp, (_forEachIndex + 1), _x]; } forEach SRVRULES_rules;
_rules = _rules joinString "<br/>";

disableSerialization;

"srvRules" cutRsc ["SRVRULES_RSCTITLES", "PLAIN", 0.01, true];

private _ui = uiNamespace getVariable ["SRVRULES_RSCTITLES", displayNull];

private _ctrl = _ui displayCtrl 88887;
_ctrl ctrlSetStructuredText parseText _rules;
_ctrl ctrlCommit 0;

_ctrl = _ui displayCtrl 88888;

private _title = ["serverRules", "display", "title"] call core_fnc_getSetting;

if ( _forced ) then {
	private _limit = time + (["serverRules", "timeout"] call core_fnc_getSetting);
	private _countdown = ["serverRules", "display", "countdown"] call core_fnc_getSetting;
	while { time < _limit } do {
	    private _left = round(_limit - time);
	    if ( _left < 0 ) then { _left = 0; };
	    _ctrl ctrlSetStructuredText parseText format[_title, format[_countdown, _left]];
		_ctrl ctrlCommit 0;
	    sleep 0.5;
	};
};

_ctrl ctrlSetStructuredText parseText format[_title, ""];

createdialog "SRVRULES_DIALOG";