/*
@filename: feats\serverRules\serverInit.sqf
Author:
	Ben
Description:
	run on server,
	load the server rules from defined source and broadcast it.
Params:
	none
Environment:
	Paramaters
		serverRules
	Settings
		serverRules >> dynamic >> mode
		serverRules >> dynamic >> src
	missionNamespace
		SRVRULES_rules
Return:
	nothing 
*/

#include "_debug.hpp"

if ( (["serverRules"] call core_fnc_getParam) == 0 ) exitWith {
    #ifdef DEBUG
    debug(LL_INFO, "disabled by mission parameter");
    #endif
    nil
};

SRVRULES_rules = [];

private _dynMode = (["serverRules", "dynamic", "mode"] call core_fnc_getSetting);
private _dynSrc = (["serverRules", "dynamic", "src"] call core_fnc_getSetting);
                                    
private _data = "";

if ( _dynMode == 1 ) then { _data = loadFile _dynSrc; };
if ( _dynMode == 2 ) then { _data = [_dynSrc] call common_fnc_urlFetchReturn; };

missionNamespace setVariable['SRVRULES_rules', (_data splitString (toString[10])), true];

missionNamespace setVariable['SRVRULES_stamp', (call common_fnc_getTimestamp), true];

nil