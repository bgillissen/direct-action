class serverRules {
	tag = "serverRules";
	class functions {
		class display { file="feats\serverRules\display.sqf"; };
		class serverInit { file="feats\serverRules\serverInit.sqf"; };
		class playerInit { file="feats\serverRules\playerInit.sqf"; };
		class playerPostInit { file="feats\serverRules\playerPostInit.sqf"; };
	};
};
