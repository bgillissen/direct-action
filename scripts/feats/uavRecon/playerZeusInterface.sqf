/*
@filename: feats\uavRecon\playerZeusInterface.sqf
Author:
	Ben
Description:
	run on player.
	set a global variable used in the draw3D EH  
*/

#include "_debug.hpp"

params ["_inZeus"];
 
if !( isNil "uavReconInZeus" ) then { 
	uavReconCamFixed = ( _inZeus isEqualTo uavReconInZeus );
} else {
    uavReconCamFixed = false;
};

uavReconInZeus = _inZeus; 

nil