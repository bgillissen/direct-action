/*
@filename: feats\uavRecon\playerDestroy.sqf
Author:
	Ben
Description:
	run on player,
*/

#include "_debug.hpp"

private _actions = ["stopFeed", "changeMode", "incAlt", "decAlt", "incRad", "decRad", "incFov", "decFov"]; 
{
    private _thing = _x;
    { _thing removeAction (_thing getVariable [_x, -1]); } forEach _actions;
} forEach uavReconCtrls;

uavRecon = -1;
call uavRecon_fnc_playerSwitch;

nil