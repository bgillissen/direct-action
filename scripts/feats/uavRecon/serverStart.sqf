/*
@filename: feats\uavRecon\serverStart.sqf
Author:
	Ben
Description:
	run on server,
 	create the uav following given setting and add it to the stack
	inform players we added one to the stack 
*/

#include "_debug.hpp"

params ["_pos", "_name", "_type", "_alt", "_loiter", "_rad", "_bearing", "_mode", "_fov"];

_pos params ["_x", "_y", "_z"];

([_bearing, _rad, _x, _y] call {
    params ["_bearing", "_rad", "_x", "_y"];
    if ( _bearing isEqualTo "N" ) exitWith { [_x, (_y + _rad)] };
    if ( _bearing isEqualTo "NE" ) exitWith { [(_x - (_rad / 2)), (_y - (_rad / 2))]; };
    if ( _bearing isEqualTo "E" ) exitWith { [(_x - _rad), _y]; };
    if ( _bearing isEqualTo "SE" ) exitWith { [(_x - (_rad / 2)), (_y + (_rad / 2))]; };
    if ( _bearing isEqualTo "S" ) exitWith { [_x, (_y + _rad)]; };
    if ( _bearing isEqualTo "SW" ) exitWith { [(_x + (_rad / 2)),(_y + (_rad / 2))]; };
    if ( _bearing isEqualTo "W" ) exitWith { [(_x + _rad), _y]; };
    if ( _bearing isEqualTo "NW" ) exitWith { [(_x + (_rad / 2)), (_y - (_rad / 2))]; };
}) params ["_x", "_y"];


private _uav = createVehicle [_type, [_x, _y, (_z + _alt)], [], 0, "FLY"];
createVehicleCrew _uav;
_uav lockCameraTo [_pos, [0]];
_uav flyInHeight _alt;
_uav allowDamage false;

private _grp = createGroup civilian;
{
     [_x] joinSilent _grp;
     _x allowDamage false;
} forEach (crew _uav);
_grp setGroupIdGlobal [_name];
_grp setCombatMode "BLUE";

private _wp = [];
if ( _loiter ) then {
	_wp = _grp addWaypoint [_pos, 0];
	_wp setWaypointType "LOITER";
	_wp setWaypointLoiterType "CIRCLE_L";
	_wp setWaypointLoiterRadius _rad;
};

if ( _fov < 0.01 ) then { _fov = 0.01; };
if ( _fov > 2 ) then { _fov = 2; };

private _id = floor serverTime;
private _camPos = getText( configfile >> "CfgVehicles" >> _type >> "uavCameraGunnerPos");
              
uavReconStack pushback [_name, _id, _camPos, _uav, _pos, _wp, _mode, _alt, _rad, _fov];

publicVariable "uavReconStack";

[_name, _id] remoteExec ["uavRecon_fnc_playerStart", 0, false];