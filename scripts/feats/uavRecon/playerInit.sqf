/*
@filename: feats\uavRecon\playerInit.sqf
Author:
	Ben
Description:
	run on player,
	define default value for needed vars,
	register a perframe event to update camera position and settings,
	register a PVEH to update the active camera,
	add actions to base things to control camera settings
*/

#include "_debug.hpp"

uavReconName = "";
uavReconMode = 0;
uavReconAlt = 2000;
uavReconRad = 1500;
uavReconFov = 2;
uavReconCams = [];
uavReconCam = objNull;
uavReconLoiter = false;

if ( isNil "uavReconEH" ) then {
	uavReconEH = addMissionEventHandler ["Draw3D", {
    	if ( uavRecon < 0 ) exitWith {};
        if ( isNull uavReconCam ) exitWith {};
		(uavReconStack select uavRecon) params ["_name", "_id", "_camPos", "_uav", "_pos", "_wp", "_mode", "_alt", "_rad", "_fov"];
        if ( isNil "_name" ) exitWith {};
    	if ( isNull _uav || !alive _uav ) exitWith {};
        private _pip = format["uavrecon_%1", uavRecon];
		if !( uavReconCamFixed ) then {
        	uavReconCamFixed = true;
        	uavReconCam cameraEffect ["Internal", "BACK", _pip];
    	};
        _pip setPiPEffect [uavReconMode];
    	uavReconCam camSetFov uavReconFov;
        uavReconTurret params ["_pos", "_dir", "_up"];
        uavReconCam setPosASL _pos;
    	uavReconCam setVectorDirAndUp [_dir, _up];
	}];
};

if ( isNil "uavReconPlayerPVEH" ) then {
    uavReconPlayerPVEH = ["uavRecon", {call uavRecon_fnc_playerSwitch;}] call pveh_fnc_add;
};

uavReconCtrls = [];
{
	_x params ["_thing", "_actions"];
	{
		if ( (configName _x) isEqualTo "uavRecon" ) then {
            if ( (getText (_x >> "type")) isEqualTo "ctrl" ) then { uavReconCtrls pushback _thing; };
		};
	} forEach _actions;
} forEach BA_obj;

private _stopFnc = {
  uavRecon = -1;
  publicVariable "uavRecon";
  if ( isServer ) then { publicVariableServer "uavRecon"; };
  call uavRecon_fnc_playerSwitch;
};	

private _modeFnc = {
  uavReconMode = uavReconMode + 1;
  if (uavReconMode > 2) then { uavReconMode = 0; };
  (uavReconStack select uavRecon) set [5, uavReconMode];
  publicVariable "uavReconStack";
  publicVariable "uavReconMode";
};

private _fovFnc = {
	if ( (_this select 3) > 0 ) then {
        uavReconFov = uavReconFov - 0.05;
    } else {
        uavReconFov = uavReconFov + 0.05;
    };
    if ( uavReconFov < 0.01 ) then { uavReconFov = 0.01; };
    if ( uavReconFov > 2 ) then { uavReconFov = 2; };
    (uavReconStack select uavRecon) set [8, uavReconFov];
  	publicVariable "uavReconStack";
    publicVariable "uavReconFov";   
};

private _altFnc = {
	if ( (_this select 3) > 0 ) then {
        uavReconAlt = uavReconAlt + 250;
	} else {
        uavReconAlt = uavReconAlt - 250;
    };
    if ( uavReconAlt < 500 ) then { uavReconAlt = 500; };
    if ( uavReconAlt > 2000 ) then { uavReconAlt = 2000; };
    systemChat format["UAV altitude set to %1", uavReconAlt];
    (uavReconStack select uavRecon) set [6, uavReconAlt];
  	publicVariable "uavReconStack";
    publicVariable "uavReconAlt";    
	((uavReconStack select uavRecon) select 4) flyInHeight uavReconAlt;   
};

private _radFnc = {
	if ( (_this select 3) > 0 ) then {
        uavReconRad = uavReconRad + 250;
	} else {
        uavReconRad = uavReconRad - 250;
    };
    if ( uavReconRad < 200 ) then { uavReconRad = 200; };
    if ( uavReconRad > 1500 ) then { uavReconRad = 1500; };
    systemChat format["UAV loiter radius set to %1", uavReconRad];
    (uavReconStack select uavRecon) set [7, uavReconRad];
  	publicVariable "uavReconStack";
    publicVariable "uavReconRad"; 
	((uavReconStack select uavRecon) select 4) setWaypointLoiterRadius uavReconRad;   
};

private "_action";

{
    _action = _x addAction ["Stop camera feed", _stopFnc, [], 0, false, true, "", "(uavRecon >= 0)", 4];
	_x setVariable ["stopFeed", _action];
	_action = _x addAction ["Change Camera Mode", _modeFnc, [], 0, false, true, "", "(uavRecon >= 0)", 4];
	_x setVariable ["changeMode", _action];
	_action = _x addAction ["Increase Altitude", _altFnc, 1, 0, false, true, "", "( (uavRecon >= 0) && (uavReconAlt < 2000) )", 4];
	_x setVariable ["incAlt", _action];
	_action = _x addAction ["Decrease Altitude", _altFnc, -1, 0, false, true, "", "( (uavRecon >= 0) && (uavReconAlt > 500) )", 4];
	_x setVariable ["decAlt", _action];
	_action = _x addAction ["Increase Radius", _radFnc, 1, 0, false, true, "", "( (uavRecon >= 0) && (uavReconRad < 1500) && uavReconLoiter)", 4];
	_x setVariable ["incRad", _action];
	_action = _x addAction ["Decrease Radius", _radFnc, -1, 0, false, true, "", "( (uavRecon >= 0) && (uavReconRad > 200) && uavReconLoiter)", 4];
	_x setVariable ["decRad", _action];
	_action = _x addAction ["Increase Zoom", _fovFnc, 1, 0, false, true, "", "( (uavRecon >= 0) && (uavReconFov > 0.01) )", 4];
	_x setVariable ["incFov", _action];
	_action = _x addAction ["Decrease Zoom", _fovFnc, -1, 0, false, true, "", "( (uavRecon >= 0) && (uavReconFov < 2) )", 4];
	_x setVariable ["decFov", _action];
    
} forEach uavReconCtrls;

{
    [(_x select 0), (_x select 1)] call uavRecon_fnc_playerStart;
} forEach uavReconStack;

if ( uavRecon >= 0 ) then {
	call uavRecon_fnc_playerSwitch;
};

nil