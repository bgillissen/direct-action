/*
@filename: feats\uavRecon\playerStop.sqf
Author:
	Ben
Description:
	run on player,
 	remove the actions for the given id, 
*/

#include "_debug.hpp"

if !( CTXT_PLAYER ) exitWith {};

private _actions = [format["switchTo_%1", _this], format["delete_%1", _this]];

{
    private _thing = _x;
	{ _thing removeAction (_thing getVariable [_x, -1]); } forEach _actions;
} forEach uavReconCtrls;

{
    _x params ["_camId", "_cam"];
    if ( _camId isEqualTo _this ) exitWith {
 		camDestroy _cam;
        uavReconCams deleteAt _forEachIndex;
    };
} forEach uavReconCams;