class uavRecon {
	tag = "uavRecon";
	class functions {
		class playerDestroy { file="feats\uavRecon\playerDestroy.sqf"; };
		class playerInit { file="feats\uavRecon\playerInit.sqf"; };
		class playerStart { file="feats\uavRecon\playerStart.sqf"; };
		class playerStop { file="feats\uavRecon\playerStop.sqf"; };
		class playerSwitch { file="feats\uavRecon\playerSwitch.sqf"; };
		class playerZeusInterface  { file="feats\uavRecon\playerZeusInterface.sqf"; };
		class serverDestroy { file="feats\uavRecon\serverDestroy.sqf"; };
		class serverInit { file="feats\uavRecon\serverInit.sqf"; };
		class serverStart { file="feats\uavRecon\serverStart.sqf"; };
		class serverStop { file="feats\uavRecon\serverStop.sqf"; };
	};
};
