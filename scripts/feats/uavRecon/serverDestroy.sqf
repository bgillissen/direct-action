/*
@filename: feats\uavRecon\serverDestroy.sqf
Author:
	Ben
Description:
	run on server,
	kill uavRecon serverInit thread,
	remove spawned uav
*/

if ( count(_this) == 0 ) exitWith { nil };

params ["_when", "_thread"];

terminate _thread;

waitUntil { scriptDone _thread };

uavRecon = -1;
publicVariable "uavRecon";

{
	_x params ["_name", "_id", "_camPos", "_uav", "_wp", "_mode", "_alt", "_rad", "_fov"];
	private _grp = group ((crew _uav) select 0);
	deleteVehicle _uav;
	deleteGroup _grp;
    uavReconStack deleteAt _forEachIndex; 
} forEach uavReconStack;

publicVariable "uavReconStack";

uavReconDisplays = [];
publicVariable "uavReconDisplays";

nil