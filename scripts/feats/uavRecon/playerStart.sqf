/*
@filename: feats\uavRecon\playerStart.sqf
Author:
	Ben
Description:
	run on player,
 	create actions on control objects
*/

#include "_debug.hpp"

if !( CTXT_PLAYER ) exitWith {};

params ["_name", "_id"];

private _switchDsp = format["Display %1", _name];
private _switchCond = format["!(uavReconName isEqualTo '%1')", _name];
private _switchExec = {
	{
    	_x params ["_name", "_id"];
    	if ( _id isEqualTo (_this select 3) ) exitWith { 
        	uavRecon = _forEachIndex;
            publicVariable "uavRecon";
            if ( isServer ) then { publicVariableServer "uavRecon"; };
			call uavRecon_fnc_playerSwitch;
		};
	} forEach uavReconStack;
};

private _stopDsp = format["Delete %1", _name];
private _stopExec = { (_this select 3) remoteExec ["uavRecon_fnc_serverStop", 2, false]; };

{
	_action = _x addAction [_switchDsp, _switchExec, _id, 0, false, true, "", _switchCond, 4];
	_x setVariable [format["switchTo_%1", _id], _action];
    _action = _x addAction [_stopDsp, _stopExec, _id, 0, false, true, "", "true", 4];
	_x setVariable [format["delete_%1", _id], _action];
} forEach uavReconCtrls;