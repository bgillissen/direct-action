class uavRecon : feat_base  {
	class server : featServer_base {
		class destroy : featEvent_enable {};
		class init : featEvent_enable { thread = 1; };
	};
	class player : featPlayer_base {
		class destroy : featEvent_enable {};
		class init : featEvent_enable {};
		class zeusInterface : featEvent_enable {};
	};
};
