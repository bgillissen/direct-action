/*
@filename: feats\uavRecon\serverInit.sqf
Author:
	Ben
Description:
	run on server,
	create a stack with base things using a display for the uav feed, and store texture applied to them,
	keep track of active uav position and broadcast it
*/

#include "_debug.hpp"

missionNamespace setVariable ["uavRecon", -1, true];
missionNamespace setVariable ["uavReconStack", [], true];

uavReconDisplays = [];
{
	_x params ["_thing", "_actions"];
	{
		if ( (configName _x) isEqualTo "uavRecon" ) then {
            if ( (getText (_x >> "type")) isEqualTo "dsp" ) then {
				uavReconDisplays pushback [_thing, ((getObjectTextures _thing) select 0)];
			};
		};
	} forEach _actions;
} forEach BA_obj;

publicVariable "uavReconDisplays";

uavReconTurret = [[0,0,0],[0,0,0],[0,0,0]];

publicVariable "uavReconTurret";

private _posUpdateFnc = {
    if ( uavRecon < 0 ) exitWith {};
    (uavReconStack select uavRecon) params ["_name", "_id", "_camPos", "_uav", "_pos", "_wp", "_mode", "_alt", "_rad", "_fov"];
    if !( isNil "_name" ) then  {
		if ( !isNull _uav && alive _uav ) then {
            private _uavPos = AGLToASL (_uav modelToWorld (_uav selectionPosition _camPos));
            private _dir = _uavPos vectorFromTo _pos; //(getWPPos _wp);
            private _up = (_dir vectorCrossProduct [-(_dir select 1), (_dir select 0), 0]);
            uavReconTurret set [0, _uavPos];
            uavReconTurret set [1, _dir];
            uavReconTurret set [2, _up];
            publicVariable "uavReconTurret";        
        };
    };
};

if ( isNil "uavReconServerPVEH" ) then {
    uavReconServerVEH = ["uavRecon", _posUpdateFnc] call pveh_fnc_add;
};



while { true } do {
    if ( uavRecon >= 0 ) then { call _posUpdateFnc; };
    {
       _x params ["_name", "_id", "_camPos", "_uav", "_wp", "_mode", "_alt", "_rad", "_fov"];
       _uav setFuel 1;
    } forEach uavReconStack;
    sleep 2; 
};

nil