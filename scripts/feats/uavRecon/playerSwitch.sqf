/*
@filename: feats\uavRecon\playerSwitch.sqf
Author:
	Ben
Description:
	run on player,
 	create cam for the active uav if it does not exists yet, 
	make the active uav's cam active.
*/

#include "_debug.hpp"

if ( uavRecon < 0 ) exitWith {
    uavReconName = "";
	uavReconMode = 0;
	uavReconAlt = 2000;
	uavReconRad = 1500;
	uavReconFov = 2; 
	uavReconCam = objNull;
    uavReconLoiter = false;
    {
        _x params ["_thing", "_texture"];
        _thing setObjectTexture [0, _texture];
    } forEach uavReconDisplays;
     
};

(uavReconStack select uavRecon) params ["_name", "_id", "_camPos", "_uav", "_pos", "_wp", "_mode", "_alt", "_rad", "_fov"];

private _activeCam = objNull;
{
    _x params ["_camId", "_cam"];
    if ( _camId isEqualTo _id ) exitWith { _activeCam = _cam; };
} forEach uavReconCams;

if ( isNull _activeCam ) then {
	_activeCam = "camera" camCreate [0,0,0];
	uavReconCams pushback [_id, _activeCam];
};

private _pipTexture = format["#(argb,512,512,1)r2t(uavrecon_%1,2)", uavRecon];
{
	(_x select 0) setObjectTexture [0, _pipTexture];
} forEach uavReconDisplays;

uavReconCam = _activeCam;
uavReconCamFixed = false;
uavReconName = _name;
uavReconMode = _mode;
uavReconAlt = _alt;
uavReconRad = _rad;
uavReconFov = _fov;
uavReconLoiter = !(_wp isEqualTo []);