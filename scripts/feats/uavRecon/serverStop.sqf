/*
@filename: feats\uavRecon\serverStop.sqf
Author:
	Ben
Description:
	run on server,
 	remove the uav,
	remove the entry from the stack 
*/

#include "_debug.hpp"

#ifdef DEBUG
private _debug = format["trying to remove uav id %1", _this];
debug(LL_DEBUG, _debug);
#endif

private "_newIdx";
{
	_x params ["_name", "_id", "_camPos", "_uav", "_wp", "_mode", "_alt", "_rad", "_fov"];
	if ( _id isEqualTo _this ) then {
        #ifdef DEBUG
		private _debug = format["removing uav %1 (%2)", _name, _forEachIndex];
        debug(LL_DEBUG, _debug);
        #endif
    	if ( uavRecon >= 0 ) then {
            if ( uavRecon isEqualTo _forEachIndex ) then { _newIdx = -1; };
            if ( uavRecon > _forEachIndex ) then { _newIdx = uavRecon - 1; };
            if !( isNil "_newIdx" ) then {
                #ifdef DEBUG
                private _debug = format["switching to index %1", _newIdx];
                debug(LL_DEBUG, _debug);
                #endif   
    			uavRecon = _newIdx;
				publicVariable "uavRecon";
				if ( CTXT_PLAYER ) then { publicVariableServer "uavRecon"; };
			};
        };
        _id remoteExec ["uavRecon_fnc_playerStop", 0, false];
		private _grp = group ((crew _uav) select 0);
    	deleteVehicle _uav;
        deleteGroup _grp;
        uavReconStack deleteAt _forEachIndex;
    };
} forEach uavReconStack;

publicVariable "uavReconStack";