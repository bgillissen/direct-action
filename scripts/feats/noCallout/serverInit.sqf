/*
@filename: feats\noCallout\serverInit.sqf
Author:
	Ben
Description:
	run on server,
	disable npc callout
*/

#include "_debug.hpp"

if ( (["noCallout"] call core_fnc_getParam) == 0 ) exitWith {
    #ifdef DEBUG
    debug(LL_INFO, "disabled by mission parameter");
    #endif
    nil
};

{
    _x params ["_npc", "_actions"];
    #ifdef DEBUG
    private _debug = format["disabling callout on %1", _npc];
    debug(LL_DEBUG, _debug);
    #endif
    _npc setSpeaker "NoVoice";
} forEach BA_npc;

nil
