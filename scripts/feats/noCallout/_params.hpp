class noCallout {
	title = "Disable player enemy / order callout to be heard and seen on screen";
	values[] = {0,1};
	texts[] = {"No","Yes"};
	default = 1;
};
