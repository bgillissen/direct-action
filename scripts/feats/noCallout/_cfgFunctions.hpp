class noCallout {
	tag = "noCallout";
	class functions {
		class playerRespawn { file="feats\noCallout\playerRespawn.sqf"; };
		class serverInit { file="feats\noCallout\serverInit.sqf"; };
	};
};
