/*
@filename: feats\noCallout\playerRespawn.sqf
Author:
	Ben
Description:
	run on player
	disable player enemy and order callout
*/

#include "_debug.hpp"

if ( (["noCallout"] call core_fnc_getParam) == 0 ) exitWith {
    #ifdef DEBUG
    debug(LL_INFO, "disabled by mission parameter");
    #endif
    nil
};

enableSentences false;
showSubtitles false;

nil