class featEvent_base {
	order = 20;
	enable = 0;
	thread = 0;
};

class featEvent_enable : featEvent_base { enable = 1; };

class featContext_base {
	class destroy : featEvent_base {};
	class group : featEvent_base {};
	class init : featEvent_base {};
	class join : featEvent_base {};
	class leave : featEvent_base {};
	class postInit : featEvent_base {};
	class preInit : featEvent_base {};
};

class featServer_base : featContext_base {
	class killed : featEvent_base {};
	class remoteControl : featEvent_base {};
	class respawn : featEvent_base {};
};

class featPlayer_base : featContext_base {
	class assemble : featEvent_base {};
	class closeVA : featEvent_base {};
	class getIn : featEvent_base {};
	class getOut : featEvent_base {};
	class inventory : featEvent_base {};
	class killed : featEvent_base {};
	class remoteControl : featEvent_base {};
	class respawn : featEvent_base {};
	class shoot  : featEvent_base {};
	class switchSeat : featEvent_base {};
	class take : featEvent_base {};
	class zeusInterface : featEvent_base {};
};

class featHeadless_base : featContext_base {};

class feat_base {
	class headless : featHeadless_base {};
	class player : featPlayer_base {};
	class server : featServer_base {};
};
