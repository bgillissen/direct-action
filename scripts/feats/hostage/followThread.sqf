
#include "_debug.hpp"

params [['_unit', objNull], ['_player', objNull]];

#ifdef DEBUG
private _debug = format["followThread: %1 is following %2", _unit, _player];
debug(LL_DEBUG, _debug);
#endif

if ( isNull _unit ) exitWith {};
if ( isNull _player ) exitWith {};

private _grp = (group _unit);
private _lastPos = [0,0,0];
      
while { true } do {
    if ( _player getVariable ['agony', false] ) exitWith {
    	#ifdef DEBUG
		private _debug = format["followThread: followed player '%1' entered agony, abording", _player];
		debug(LL_DEBUG, _debug);
		#endif
    };
    if !( alive _player ) exitWith {
    	#ifdef DEBUG
		private _debug = format["followThread: followed player '%1' is dead, abording", _player];
		debug(LL_DEBUG, _debug);
		#endif
    };
    if ( isNull _player ) exitWith {
    	#ifdef DEBUG
		debug(LL_DEBUG, "followThread: followed player is null, abording");
		#endif
    };
    if !( alive _unit ) exitWith {
    	#ifdef DEBUG
    	private _debug = format["followThread: unit '%1' is dead, abording", _unit];
		debug(LL_DEBUG, _debug);
		#endif
    };
    if ( isNull _unit ) exitWith {
    	#ifdef DEBUG
		debug(LL_DEBUG, "followThread: unit is null, abording");
		#endif
    };
    private _veh = (vehicle _player);
    if ( _veh isEqualTo _player ) then {
        if !( (vehicle _unit) isEqualTo _unit ) then {
			#ifdef DEBUG
    		private _debug = format["followThread: asking unit '%1' to getOut", _unit];
			debug(LL_DEBUG, _debug);
			#endif
           	private _thread = [(vehicle _unit), objNull, -1, _unit] spawn hostage_fnc_getOut;
			waitUntil { isNull _thread };
			#ifdef DEBUG
    		private _debug = format["followThread: unit '%1' is out", _unit];
			debug(LL_DEBUG, _debug);
			#endif
        };
    	if ( ((_player distance _lastPos) > 2)  || ((count (waypoints _grp)) isEqualTo 0) ) then {
			_grp call common_fnc_removeWP;
        	_lastPos = getPos _player;
        	#ifdef DEBUG
    		private _debug = format["followThread: asking unit '%1' to moveTo %2", _unit, _lastPos];
			debug(LL_DEBUG, _debug);
			#endif        
        	private _wp = _grp addWaypoint [_lastPos, 0, 0];
        	_wp setWaypointTimeout [600, 600, 600];
        	_wp setWaypointCompletionRadius 1; 
    	};
        private "_stance";
        switch ( stance _player ) do {
            case "STAND" : {_stance = "UP"; };
            case "CROUCH" : {_stance = "MIDDLE"; };
            case "PRONE" : {_stance = "DOWN"; };
        };
        if !( _stance  isEqualTo (_unit getVariable ["hostageStance", "STAND"]) ) then {
            #ifdef DEBUG
    		private _debug = format["followThread: asking unit '%1' to change stance to %2", _unit, _stance];
			debug(LL_DEBUG, _debug);
			#endif
            [_unit, _player, -1, _stance] call hostage_fnc_setStance;
        };
	} else {
        if !( (vehicle _unit) isEqualTo _veh ) then {
            #ifdef DEBUG
    		private _debug = format["followThread: asking unit '%1' to getIn %2", _unit, _veh];
			debug(LL_DEBUG, _debug);
			#endif
            _grp call common_fnc_removeWP;
			_unit assignAsCargo _veh;
			[_unit] orderGetin true;
			waitUntil {
				if !( _veh isEqualTo (vehicle _player) ) exitWith {
					#ifdef DEBUG
    				private _debug = format["followThread: getIn aborded, player '%1' is not in '%2' anymore", _player, _veh];
					debug(LL_DEBUG, _debug);
					#endif
					private _thread = ([(vehicle _unit), objNull, -1, _unit] spawn hostage_fnc_getOut);
					waitUntil { isNull _thread };
					true	
				};
				( ((vehicle _unit) isEqualTo _veh) || !(alive _veh) || !(alive _unit) || (isNull _unit) ) 
			};
			if ( ((vehicle _unit) isEqualTo _veh) && (alive _unit) && (alive _veh) ) then {
				#ifdef DEBUG
    			private _debug = format["unit '%1' is in %2", _unit, _veh];
				debug(LL_DEBUG, _debug);
				#endif
				[_unit, _veh] remoteExec ['hostage_fnc_playerAddVehActions', 0, false];
			};
		};
    }; 
    sleep 2;
};

if ( isNull _unit ) exitWith {};

_unit setVariable ["hostageFollow", objNull, true];

_grp call common_fnc_removeWP;
