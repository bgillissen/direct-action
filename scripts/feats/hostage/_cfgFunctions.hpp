class hostage {
	tag = "hostage";
	class functions {
		class serverAdd { file="feats\hostage\serverAdd.sqf"; };
		class serverInit { file="feats\hostage\serverInit.sqf"; };
		class serverCleanup { file="feats\hostage\serverCleanup.sqf"; };
		class serverDestroy { file="feats\hostage\serverDestroy.sqf"; };
		
		class playerAdd { file="feats\hostage\playerAdd.sqf"; };
		class playerAddVehActions { file="feats\hostage\playerAddVehActions.sqf"; };
		class playerDelVehActions { file="feats\hostage\playerDelVehActions.sqf"; };
		class playerInit { file="feats\hostage\playerInit.sqf"; };
		class playerCleanup { file="feats\hostage\playerCleanup.sqf"; };
		class playerDestroy { file="feats\hostage\playerDestroy.sqf"; };	
		
		class follow  { file="feats\hostage\actions\follow.sqf"; };
		class followThread  { file="feats\hostage\followThread.sqf"; };
		class setAbilities { file="feats\hostage\setAbilities.sqf"; };
		class ownerSetup { file="feats\hostage\ownerSetup.sqf"; };
		
		class condition { file="feats\hostage\actions\condition.sqf"; };
		class getOut { file="feats\hostage\actions\getOut.sqf"; };
		class setStance { file="feats\hostage\actions\setStance.sqf"; };
		class setFree { file="feats\hostage\actions\setFree.sqf"; };
		class stayIn { file="feats\hostage\actions\stayIn.sqf"; };
		class stop { file="feats\hostage\actions\stop.sqf"; };
		
	};
};
