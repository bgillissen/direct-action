
#include "_debug.hpp"

if !( CTXT_PLAYER ) exitWith {};

if ( isNil "hostage_playerStack" ) then { hostage_playerStack = []; };

if ( isNull _this ) exitWith {
	#ifdef DEBUG
	debug(LL_WARN, "playerAdd: given unit is NULL, abording");
	#endif
};

if !( alive _this ) exitWith {
	#ifdef DEBUG
	debug(LL_WARN, "playerAdd: given unit is dead, abording");
	#endif
};

#ifdef DEBUG
private _debug = format["playerAdd: adding actions for unit '%1'", _this];
debug(LL_DEBUG, _debug);
#endif

if !( _this getVariable ['hostageFreed', false]) then {
	[_this, //target
	 "Free Hostage",	//title
	 "\a3\ui_f\data\IGUI\Cfg\HoldActions\holdAction_unbind_ca.paa", //idle Icon
	 "\a3\ui_f\data\IGUI\Cfg\HoldActions\holdAction_unbind_ca.paa",	//prg Icon
	 "['setFree', _target] call hostage_fnc_condition",	//show condition
	 "true",	//prg condition
	 {},	//start code		
	 {},	//tick code
	 { _this remoteExec ['hostage_fnc_setFree', 2, false] }, //completion code
	 {}, //interrupt code
	 [],	//args
	 3,	//duration
	 100,	//priority
	 true,	//remove on completion
	 false	//show unconscious
	] call BIS_fnc_holdActionAdd;
};

private _stackEntry = [_this];
private _name = name _this;



private ['_cond', '_action'];
_cond = "['follow', _target] call hostage_fnc_condition";
_action = _this addAction [format["%1 - Follow me", _name], {_this remoteExec ['hostage_fnc_follow', 2, false]}, "", 20, false, true, "", _cond, 10];
_stackEntry set [1, _action];

_cond = "['stop', _target] call hostage_fnc_condition";
_action = _this addAction [format["%1 - Stay here", _name], {_this remoteExec ['hostage_fnc_stop', 2, false]}, "", 19, false, true, "", _cond, 10];
_stackEntry set [2, _action];

_cond = "['stance', _target, 'DOWN'] call hostage_fnc_condition";
_action = _this addAction [format["%1 - Go prone", _name], {_this remoteExec ['hostage_fnc_setStance', 2, false]}, "DOWN", 18, false, true, "", _cond, 10];
_stackEntry set [3, _action];

_cond = "['stance', _target, 'MIDDLE'] call hostage_fnc_condition";
_action = _this addAction [format["%1 - Go crouch", _name], {_this remoteExec ['hostage_fnc_setStance', 2, false]}, "MIDDLE", 17, false, true, "", _cond, 10];
_stackEntry set [4, _action];

_cond = "['stance', _target, 'UP'] call hostage_fnc_condition";
_action = _this addAction [format["%1 - Stand up", _name], {_this remoteExec ['hostage_fnc_setStance', 2, false]}, "UP", 16, false, true, "", _cond, 10];
_stackEntry set [5, _action];

hostage_playerStack pushback _stackEntry;

if !( (vehicle _this) isEqualTo _this ) then {
	[_this, (vehicle _this)] call hostage_fnc_playerAddVehActions;
};