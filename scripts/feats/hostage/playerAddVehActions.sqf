
#include "_debug.hpp"

if !( CTXT_PLAYER ) exitWith {};

params ["_unit", "_veh"];

if ( isNull _unit ) exitWith {
	#ifdef DEBUG
	debug(LL_WARN, "playerAddVehActions: given unit is NULL, abording");
	#endif
};
if !( alive _unit ) exitWith {
	#ifdef DEBUG
	debug(LL_WARN, "playerAddVehActions: given unit is dead, abording");
	#endif
};

if ( isNull _veh ) exitWith {
	#ifdef DEBUG
	debug(LL_WARN, "playerAddVehActions: given vehicle is NULL, abording");
	#endif
	};
if !( alive _veh ) exitWith {
	#ifdef DEBUG
	debug(LL_WARN, "playerAddVehActions: given vehicle is dead, abording");
	#endif
};

if ( _unit isEqualTo _veh ) exitWith {
	#ifdef DEBUG
	debug(LL_WARN, "playerAddVehActions: given unit is equal to given vehicle, abording");
	#endif
};

private _name = (name _unit);

private _cond = format["['getOut', _target, '%1'] call hostage_fnc_condition", _name];
private _out = (_veh addAction [format["%1 - Getout", _name], {_this remoteExec ['hostage_fnc_getOut', 2, false]}, _unit, 15, false, true, "", _cond, 10]);

private _cond = format["['stayIn', _target, '%1'] call hostage_fnc_condition", _name];
private _stay = (_veh addAction [format["%1 - Stay In", _name], {_this remoteExec ['hostage_fnc_stayIn', 2, false]}, _unit, 15, false, true, "", _cond, 10]);

{
	_x params ["_sUnit"];
	if ( _sUnit isEqualTo _this ) exitWith {
		(hostage_playerStack select _forEachIndex) set [6, _veh];
		(hostage_playerStack select _forEachIndex) set [7, _out];
		(hostage_playerStack select _forEachIndex) set [8, _stay];
	};
} forEach hostage_playerStack;