/*
@filename: feats\hostage\serverDestroy.sqf
Author:
	Ben
Description:
	this run on server
	kill the hostage serverInit thread,
	remove all hostages
 */

if ( count(_this) == 0 ) exitWith { nil };

params ["_when", "_thread"];

if ( scriptDone _thread ) exitWith { nil };

terminate _thread;

waitUntil { scriptDone _thread };

{ 
	[_x] call hostage_fnc_cleanup;
	deleteVehicle _x;
} forEach hostage_stack;

hostage_stack = [];

nil