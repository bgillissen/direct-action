
#include "_debug.hpp"

if !( CTXT_PLAYER ) exitWith {};

{
	_x params ["_unit", "_follow", "_stop", "_down", "_middle", "_up", ["_veh", objNull], ["_out", -1], ["_stay", -1]];
	if ( isNull _unit || !alive _unit || (_unit isEqualTo _this) ) then {
		#ifdef DEBUG
		private _debug = format ["playerCleanup: removing unit actions for '%1' (%2)", _unit, _forEachIndex];
		debug(LL_DEBUG, _debug);
		#endif 
		_unit removeAction _follow;
		_unit removeAction _stop;
		_unit removeAction _down;
		_unit removeAction _middle;
		_unit removeAction _up;
		if !( isNull _veh ) then {
			#ifdef DEBUG
			private _debug = format ["playerCleanup: removing vehicle actions from '%3' for '%1' (%2)", _unit, _forEachIndex, _veh];
			debug(LL_DEBUG, _debug);
			#endif
			if ( _out >= 0 ) then { _veh removeAction _out; };
			if ( _stay >= 0 ) then { _veh removeAction _stay; };
		};
		hostage_playerStack deleteAt _forEachIndex;
	};
} forEach hostage_playerStack;