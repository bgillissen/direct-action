
if !( CTXT_SERVER ) exitWith { _this remoteExec ['hostage_fnc_serverAdd', 2, false]; };

#include "_debug.hpp"

{
    private _unit = _x;
    private _ok = true;
    {
    	if ( (_x select 0) isEqualTo _unit ) exitWith { _ok = false; };
    } forEach hostage_serverStack;
	if ( _ok ) then {
	    #ifdef DEBUG
		private _debug = format ["serverAdd: adding unit '%1' to serverStack", _unit];
		debug(LL_DEBUG, _debug);
		#endif 
    	_unit setVariable ["NOAI", true, true];
        _unit setVariable ["hostageFreed", false, true];
        _unit setVariable ["hostageFollow", objNull, true];
        _unit setVariable ["hostageStance", true, true];
        private _grp = createGroup [civilian, true];
        _grp setVariable ["NOLB", true];
        _grp setGroupIdGlobal [(['Hostage', allGroups, {groupId _this}] call common_fnc_getUniqueName)];
        [_unit] joinSilent _grp;
		_grp setBehaviour "CARELESS";
		_grp setCombatMode "BLUE";
        _grp allowFleeing 0;
        _unit call hostage_fnc_ownerSetup;
        _unit addEventHandler ["killed", {[] call hostage_fnc_serverCleanup;}];
       
		private _jip = _unit remoteExec ["hostage_fnc_playerAdd", 0, true];
        hostage_serverStack pushback [_unit, _jip, scriptNull];
	#ifdef DEBUG
	} else {
		private _debug = format ["serverAdd: unit '%1' is already in serverStack, skipping", _unit];
		debug(LL_DEBUG, _debug);
	#endif          
	};            	
} forEach _this;