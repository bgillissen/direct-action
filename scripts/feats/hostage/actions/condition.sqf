
params ["_action", "_target", ["_extra", ""]];

if !( alive _target ) exitWith { false };

if ( player getVariable ['agony', false] ) exitWith { false };

if ( _action isEqualTo "setFree" ) exitWith { 
	if ( _target getVariable ['hostageFreed', false] ) exitWith { false };
    ( (player distance _target) <= 2 ) 
};

if ( _action isEqualTo "getOut" ) exitWith {
	private _unit = objNull;
    {
		if ( _extra isEqualTo (name (_x select 0)) ) exitWith { _unit = (_x select 0); };
    } forEach hostage_serverStack;
    if ( isNull _unit ) exitWith { false };
    if !( alive _unit ) exitWith { false };
    if !( _unit getVariable ['hostageFreed', false] ) exitWith { false };
    if !( isNull (_unit getVariable ['hostageFollow', objNull]) ) exitWith { false };
	( _target isEqualTo (vehicle _unit) )
};

if ( _action isEqualTo "stayIn" ) exitWith {
	private _unit = objNull;
    {
		if ( _extra isEqualTo (name (_x select 0)) ) exitWith { _unit = (_x select 0); };
    } forEach hostage_serverStack;
    if ( isNull _unit ) exitWith { false };
    if !( alive _unit ) exitWith { false };
    if !( _unit getVariable ['hostageFreed', false] ) exitWith { false };
    if ( isNull (_unit getVariable ['hostageFollow', objNull]) ) exitWith { false };
    if !( _target isEqualTo (vehicle _unit) ) exitWith { false };
    ( (_unit getVariable ['hostageFollow', objNull]) isEqualTo player )
};

if !( _target getVariable ['hostageFreed', false] ) exitWith { false };

if !( _target isEqualTo (vehicle _target) ) exitWith { false };


if ( _action isEqualTo "follow" ) exitWith {    
	( isNull (_target getVariable ['hostageFollow', objNull]) ) 
};

if ( _action isEqualTo "stop" ) exitWith {  
	( (_target getVariable ['hostageFollow', objNull]) isEqualTo player ) 
};

if ( _action isEqualTo "stance" ) exitWith {
    if ( _extra isEqualTo (_target getVariable ["hostageStance", 'UP']) ) exitWith { false };
    if !( isNull (_target getVariable ['hostageFollow', objNull]) ) exitWith { false };
	true        
};