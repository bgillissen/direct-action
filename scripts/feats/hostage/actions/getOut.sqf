
if !( CTXT_SERVER ) exitWith { _this remoteExec ['hostage_fnc_getOut', 2, false]; };

#include "..\_debug.hpp"

params ["_target", "_caller", "_id", "_unit"];

#ifdef DEBUG
private _debug = format ["getOut: asking unit '%1' to getOut of '%2'", _unit, _target];
debug(LL_DEBUG, _debug);
#endif
			
_unit leaveVehicle _target;
[_unit] orderGetin false;
unassignVehicle _unit;

private _veh = (vehicle _unit);

waitUntil { ( (vehicle _unit) isEqualTo _unit ) };

#ifdef DEBUG
private _debug = format ["getOut: unit '%1' is out of '%2'", _unit, _target];
debug(LL_DEBUG, _debug);
#endif

if ( isNull (_unit getVariable ["hostageFollow", objNull]) ) then {
	[_unit, false, ["MOVE"]] call hostage_fnc_setAbilities;
};

_unit remoteExec ["hostage_fnc_playerDelVehActions", 0, false];