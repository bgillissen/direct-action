
if !( CTXT_SERVER ) exitWith { _this remoteExec ['hostage_fnc_stayIn', 2, false]; };

#include "..\_debug.hpp"

params ["_veh", "_caller", "_id", "_unit"];

if ( isNull (_unit getVariable ["hostageFollow", objNull]) ) exitWith {
	#ifdef DEBUG
	private _debug = format["stay: unit '%1' is not following a player, abording", _unit];
	debug(LL_WARN, _debug);
	#endif
};

{
	_x params ["_sUnit", "_jip", "_thread"];
	if ( _sUnit isEqualTo _unit ) exitwith {
		#ifdef DEBUG
		private _debug = format["stay: asking unit '%1' to stay in '%2'", _unit, _veh];
		debug(LL_DEBUG, _debug);
		#endif
		_unit setVariable ["hostageFollow", objNull, true];
		terminate _thread;
	};
} forEach hostage_serverStack;