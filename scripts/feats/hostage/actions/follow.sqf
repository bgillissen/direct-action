
if !( CTXT_SERVER ) exitWith { _this remoteExec ['hostage_fnc_follow', 2, false]; };

#include "..\_debug.hpp"

params ["_target", "_caller", "_id", "_arg"];

if !( isNull (_target getVariable ["hostageFollow", objNull]) ) exitWith {
	#ifdef DEBUG
	private _debug = format["follow: unit '%1' (%2) is already following someone, abording", _target, _forEachIndex];
	debug(LL_DEBUG, _debug);
	#endif
};

{
	_x params ["_unit", "_jip", "_thread"];
    if ( _target isEqualTo _unit ) then {
 		#ifdef DEBUG
		private _debug = format["follow: unit '%1' (%2) is now following '%3'", _target, _forEachIndex, _caller];
		debug(LL_DEBUG, _debug);
		#endif   
    	[_target, true, ["MOVE"]] call hostage_fnc_setAbilities;
		_target setVariable ["hostageFollow", _caller, true];
		if !( isNull _thread ) then { terminate _thread; };
		private _thread = ([_target, _caller] spawn hostage_fnc_followThread);
       	(hostage_serverStack select _forEachIndex) set [2, _thread];
	};
} forEach hostage_serverStack;