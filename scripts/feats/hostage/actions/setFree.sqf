
if !( CTXT_SERVER ) exitWith { _this remoteExec ['hostage_fnc_setFree', 2, false]; };

#include "..\_debug.hpp"

params ["_target", "_caller", "_id", "_arg"];

if ( _target getVariable ['hostageFreed', false] ) exitWith {
	#ifdef DEBUG
	debug(LL_WARN, "setFree: unit has already been freed, abording");
	#endif
};

#ifdef DEBUG
private _debug = format["setFree: unit '%1' has been set free by '%2'", _target, _caller];
debug(LL_DEBUG, _debug);
#endif
	
[_target, _id] remoteExec ["BIS_fnc_holdActionRemove", 0, false];

_target setVariable ['hostageFreed', true, true];

if ( (animationState _target) isEqualTo "Acts_ExecutionVictim_Loop" ) then {
    [_target, "Acts_ExecutionVictim_Unbow", 0] call global_fnc_doAnim;
} else {
	[_target, "Acts_AidlPsitMstpSsurWnonDnon_out", 2] call global_fnc_doAnim;
};

[7, format['%1 - Thanks you !', (name _target)]] call global_fnc_chat;

_target setUnitPos "UP";

_target setVariable ["hostageStance", "UP", true];