
if !( CTXT_SERVER ) exitWith { _this remoteExec ['hostage_fnc_stop', 2, false]; };

#include "..\_debug.hpp"

params ["_target", "_caller", "_id", "_arg"];

if ( isNull (_target getVariable ["hostageFollow", objNull]) ) exitWith {
	#ifdef DEBUG
	private _debug = format["stop: unit '%1' is not following a player, abording", _target];
	debug(LL_WARN, _debug);
	#endif
};


{
	_x params ["_unit", "_jip", "_thread"];
	if ( _unit isEqualTo _target ) exitwith {
		#ifdef DEBUG
		private _debug = format["stop: asking unit '%1' (%2) to stop", _unit, _forEachIndex];
		debug(LL_DEBUG, _debug);
		#endif
		[_unit, false, ["MOVE"]] call hostage_fnc_setAbilities;
		_unit setVariable ["hostageFollow", objNull, true];
		terminate _thread;
		private _grp = group _unit;
		{ deleteWaypoint [_grp, _forEachIndex] } forEach (waypoints _grp);
	};
} forEach hostage_serverStack;