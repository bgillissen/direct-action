
params ["_target", "_caller", "_id", "_arg"];

if ( isNull _target ) exitWith {};

if !( local _target ) exitWith {
    _this remoteExec ["hostage_fnc_setStance", (owner _target), false];
};

_target setUnitPos _arg;

_target setVariable ["hostageStance", _arg, true];