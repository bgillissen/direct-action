class hostage_fnc_serverAdd {
	allowedTargets = 2;
	jip = 0;
};
class hostage_fnc_ownerSetup {
	allowedTargets = 0;
	jip = 0;
};
class hostage_fnc_setAbilities {
	allowedTargets = 0;
	jip = 0;
};
class hostage_fnc_playerCleanup {
	allowedTargets = 0;
	jip = 0;
};
class hostage_fnc_follow {
	allowedTargets = 2;
	jip = 0;
};
class hostage_fnc_getOut {
	allowedTargets = 2;
	jip = 0;
};
class hostage_fnc_setStance {
	allowedTargets = 0;
	jip = 0;
};
class hostage_fnc_setFree {
	allowedTargets = 2;
	jip = 0;
};
class hostage_fnc_stop {
	allowedTargets = 2;
	jip = 0;
};
class hostage_fnc_playerAdd {
	allowedTargets = 0;
	jip = 1;
};
class hostage_fnc_playerAddVehActions {
	allowedTargets = 0;
	jip = 0;
};
class hostage_fnc_playerDelVehActions {
	allowedTargets = 0;
	jip = 0;
};