
if !( local _this ) exitWith {
    _this remoteExec['hostage_fnc_setup', (owner _this), false];
};

{ _this disableAI _x } forEach ["MOVE", "AUTOTARGET"];

removeAllWeapons _this;
removeBackpack _this;
removeHeadgear _this; 

private _anims = ["Acts_AidlPsitMstpSsurWnonDnon01",
				 "Acts_AidlPsitMstpSsurWnonDnon02",
                 "Acts_AidlPsitMstpSsurWnonDnon03",
                 "Acts_AidlPsitMstpSsurWnonDnon04",
                 "Acts_AidlPsitMstpSsurWnonDnon05",
                 "Acts_ExecutionVictim_Loop"];

[_this, (selectRandom _anims), 2] call global_fnc_doAnim;