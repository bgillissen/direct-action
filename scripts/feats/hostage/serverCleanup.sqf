
#include "_debug.hpp"

{
	_x params ["_unit", "_jip", "_thread"];
    if ( isNull _unit || !alive _unit ) then {
    	#ifdef DEBUG
		private _debug = format ["serverCleanup: removing unit '%1' (%2) from serverStack", _unit, _forEachIndex];
		debug(LL_DEBUG, _debug);
		#endif 
    	nil remoteExec ["", _jip];
    	_unit remoteExec ["hostage_fnc_playerCleanup", 0, false];
    	if !( isNull _thread ) then { terminate _thread; }; 
    	hostage_serverStack deleteAt _forEachIndex;
	};
} forEach hostage_serverStack;