
params ['_unit', '_bool', '_abilities'];

if !( local _unit ) exitWith {
    _this remoteExec ["hostage_fnc_setAbilities", (owner _unit), false];
};

if ( _bool ) then {
	{ _unit enableAI _x; } forEach _abilities;
} else {
    { _unit disableAI _x; } forEach _abilities;
};