/*
@filename: feats\hostage\playerDestroy.sqf
Author:
	Ben
Description:
	run on player,
	remove all hostage related actions
 */

{ [_x] call hostage_fnc_cleanup; } forEach hostage_stack;

nil