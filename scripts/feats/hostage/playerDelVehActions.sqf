
#include "_debug.hpp"

if !( CTXT_PLAYER ) exitWith {};

{
	_x params ["_unit", "_follow", "_stop", "_down", "_middle", "_up", ["_veh", objNull], ["_out", -1], ["_stay", -1]];
	if ( _unit isEqualTo _this ) exitWith {
		#ifdef DEBUG
		private _debug = format ["playerDelVehAction: removing vehicle actions from '%1' for unit '%2'", _veh, _unit];
		debug(LL_DEBUG, _debug);
		#endif
		if ( _out >= 0 ) then { 
			_veh removeAction _out;
			(hostage_playerStack select _forEachIndex) set [7, -1];
		};
		if ( _stay >= 0 ) then {
			_veh removeAction _stay;
			(hostage_playerStack select _forEachIndex) set [8, _stay];
		};
		(hostage_playerStack select _forEachIndex) set [6, objNull];
	};
} forEach hostage_playerStack;