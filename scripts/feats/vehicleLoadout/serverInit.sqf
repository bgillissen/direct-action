/*
@filename: feats\vehicleLoadout\serverInit.sqf
Author:
	ben
Description:
	run on server,
	initialize the server presets stack
*/

#include "_debug.hpp"

if ( (["vehicleLoadout"] call core_fnc_getParam) == 0 ) exitWith {
	#ifdef DEBUG
    debug(LL_INFO, "disabled by mission parameter");
    #endif
    nil
};

missionNamespace setVariable ["DAVL_serverPresets", [], true];

private _color = call common_fnc_getMarkerColor;

VL_markers = [];

{
	{
		_x params ["_thing", "_actions"];
		{
			if ( (configName _x) isEqualTo "vehicleLoadout" ) then {
				private _marker = getText(_x >> "marker");
				private _coord = getMarkerPos _marker;
	        	if !( _coord isEqualTo [0,0,0] ) then {
	        		#ifdef DEBUG
					private _debug = format["creating marker on %1", _marker];
    				debug(LL_INFO, _debug);
    				#endif
	                _marker = format["VL_marker_%1", _marker];
					createMarker [_marker, _coord];
					_marker setMarkerColor _color;
					_marker setMarkerShape "ICON";
					_marker setMarkerType (["vehicleLoadout", "markerIcon"] call core_fnc_getSetting);
					_marker setMarkerText (["vehicleLoadout", "markerText"] call core_fnc_getSetting);
					VL_markers pushback _marker;
				#ifdef DEBUG
				} else {
					private _debug = format["marker '%1' was not found", _marker];
    				debug(LL_ERR, _debug);
    			#endif
				};
			};
		} forEach _actions;
	} forEach _x;
} forEach [BA_npc, BA_obj, BA_veh];

nil