 /*
@filename: feats\vehicleLoadout\condition.sqf
Author:
	ben
Description:
	run on player,
	action condition
*/

#include "_debug.hpp"

params ["_marker"];

private _pos = getMarkerPos _marker;

if ( _pos isEqualTo [0,0,0] ) exitWith { false };

private _radius = ["vehicleLoadout", "radius"] call core_fnc_getSetting;

private _objs = _pos nearEntities ["AllVehicles", _radius];

if ( (count _objs == 0) ) exitWith { false };

_bool = false;
{
	if ( isClass(configFile >> "cfgVehicles" >> (typeOf _x) >> "Components" >> "TransportPylonsComponent") ) then {
		_bool = ( count ("true" configClasses(configFile >> "cfgVehicles" >> (typeOf _x) >> "Components" >> "TransportPylonsComponent" >> "Pylons")) > 0 );
	};  
	if ( _bool ) exitWith {};  
} forEach _objs;

if !( _bool ) exitWith { false };

_bool = isAssigned;
if !( _bool ) then { _bool = CTXT_SERVER; };
if !( _bool ) then { _bool = (call BIS_fnc_admin > 0); };
if !( _bool ) then { _bool = !(player call memberData_fnc_vehicleRestrictions); };
if !( _bool ) then { _bool = ( (player getVariable ["role", ""]) in DAVL_roles ); };

_bool