/*
@filename: feats\vehicleLoadout\serverDestroy.sqf
Author:
	Ben
Description:
	run on server,
	remove VL map markers
 */
 
{ deleteMarker _x; } forEach VL_markers;

nil