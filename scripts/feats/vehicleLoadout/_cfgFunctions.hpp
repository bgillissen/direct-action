class vehicleLoadout {
	tag = "vehicleLoadout";
	class functions {
		class condition { file = "feats\vehicleLoadout\condition.sqf"; };
		class open { file = "feats\vehicleLoadout\open.sqf"; };
		class playerInit { file = "feats\vehicleLoadout\playerInit.sqf"; };
		class serverDestroy { file = "feats\vehicleLoadout\serverDestroy.sqf"; };
		class serverInit { file = "feats\vehicleLoadout\serverInit.sqf"; };
		class setPylons { file = "feats\vehicleLoadout\setPylons.sqf"; };

		class destroy { file = "feats\vehicleLoadout\ui\destroy.sqf"; };
		class hasChanged { file = "feats\vehicleLoadout\ui\hasChanged.sqf"; };
		class init { file = "feats\vehicleLoadout\ui\init.sqf"; };
		class update { file = "feats\vehicleLoadout\ui\update.sqf"; };

		class evtApply { file = "feats\vehicleLoadout\ui\events\apply.sqf"; };
		class evtMirror { file = "feats\vehicleLoadout\ui\events\mirror.sqf"; };
		class evtName { file = "feats\vehicleLoadout\ui\events\name.sqf"; };
		class evtPreset { file = "feats\vehicleLoadout\ui\events\preset.sqf"; };
		class evtPylon { file = "feats\vehicleLoadout\ui\events\pylon.sqf"; };
		class evtRemove { file = "feats\vehicleLoadout\ui\events\remove.sqf"; };
		class evtSave { file = "feats\vehicleLoadout\ui\events\save.sqf"; };
	};
};
