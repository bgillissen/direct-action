/*
@filename: feats\vehicleLoadout\ui\update.sqf
Author:
	ben
Description:
	run on player,
	update pylon ordnance dialog
*/

#include "define.hpp"

disableSerialization;

params [["_dsp", displayNull]];

if ( isNull _dsp ) then { _dsp = findDisplay VL_IDD; };
if ( isNull _dsp ) exitWith {};

private _presetSCR = _dsp displayCtrl PRESET_IDC;
private _pylonSCR = _dsp displayCtrl PYLON_IDC;

private _idx = lbCurSel (_presetSCR controlsGroupCtrl PRESET_SELECT);

(DAVL_presets select _idx) params ["_name", "_id", "_pylons", "_mirrored", "_isShared", "_isBIS"];
DAVL_uiBuffer params ["_bName", "_bPylons", "_bMirrored"];

private _isNew = ( _id isEqualTo "New" );
private _isCurrent = ( _id isEqualTo "Current" );
private _hasChanged = _isNew;
if !( _hasChanged ) then { _hasChanged = !( _name isEqualTo _bName ); };
if !( _hasChanged ) then { _hasChanged = !(_mirrored isEqualTo _bMirrored); };
if !( _hasChanged ) then { _hasChanged = !(_pylons isEqualTo _bPylons); };

if ( _hasChanged && (_isBIS || _isShared || _isCurrent) && !_isNew ) exitWith {
	DAVL_uiPreset = "New";
	[_dsp] call vehicleLoadout_fnc_update;
};

if ( _isNew ) then {
	private _presets = (profileNamespace getVariable ["DAVL_presets", []]) + [];
	{
		_x params ["_name", "_class", "_attachment", ["_mirrored", false]];
		if !( _class isEqualTo (typeOf DAVL_veh) ) then { _presets deleteAt _forEachIndex; };
	} forEach _presets;
	DAVL_uiNoEvents = true;
	private _curName = ctrlText (_presetSCR controlsGroupCtrl PRESET_NAME);
	private _uName = ([_curName, _presets, {_this select 0}] call common_fnc_getUniqueName);
	(_presetSCR controlsGroupCtrl PRESET_NAME) ctrlSetText _uName;
	DAVL_uiBuffer set [0, _uName];
	DAVL_uiNoEvents = false;
};

if ( _isNew || _isShared || _isBIS ) then {
	(_presetSCR controlsGroupCtrl PRESET_SAVE) ctrlSetText "Save as";
} else {
	(_presetSCR controlsGroupCtrl PRESET_SAVE) ctrlSetText "Save";	
};

(_presetSCR controlsGroupCtrl PRESET_SAVE) ctrlEnable ( _hasChanged && ((count ((toArray _bName) - [32])) > 0) );

(_presetSCR controlsGroupCtrl PRESET_REMOVE) ctrlEnable !( _isShared || _isBIS || _isNew || _isCurrent );

(_dsp displayCtrl APPLY_IDC) ctrlEnable !( _bPylons isEqualTo (getPylonMagazines DAVL_veh));