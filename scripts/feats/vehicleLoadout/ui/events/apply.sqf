/*
@filename: feats\vehicleLoadout\ui\apply.sqf
Author:
	ben
Description:
	run on player,
*/

#include "..\define.hpp"

if ( DAVL_uiNoEvents ) exitWith {};

if !( alive DAVL_veh ) exitWith {};

private _radius = ["vehicleLoadout", "radius"] call core_fnc_getSetting;

if ( (DAVL_veh distance (getMarkerPos DAVL_marker)) > _radius ) exitWith {
	systemChat "Vehicle has moved away, abording";    
};

disableSerialization;

private _dsp = findDisplay VL_IDD;
private _presetSCR = _dsp displayCtrl PRESET_IDC;
private _pylonSCR = _dsp displayCtrl PYLON_IDC;

private _idx = lbCurSel (_presetSCR controlsGroupCtrl PRESET_SELECT);
private _id = (_presetSCR controlsGroupCtrl PRESET_SELECT) lbData _idx;

(DAVL_presets select _idx) params ["_name", "_id", "_pylons", "_mirrored", "_isShared", "_isBIS"];
DAVL_uiBuffer params ["_bName", "_bPylons", "_bMirrored"];

private _hasChanged = ( _id isEqualTo "New" );
if !( _hasChanged ) then { _hasChanged = !( _name isEqualTo _bName ); };
if !( _hasChanged ) then { _hasChanged = !(_mirrored isEqualTo _bMirrored); };
if !( _hasChanged ) then { _hasChanged = !(_pylons isEqualTo _bPylons); };

private _toSet = [];
{
    _x params ["_pos", "_mags", "_mirror", "_maxload", "_turret"];
	_toSet pushback [(_bPylons select _forEachIndex), _turret]; 
} forEach DAVL_pylons;
[player, DAVL_veh, _toSet] spawn vehicleLoadout_fnc_setPylons;

if !( _isShared ) then {
	if ( _id isEqualto "New" ) then { _id = format["%1_%2", (typeOf DAVL_veh), time]; };
	DAVL_serverPresets pushBack [_id, _name, (typeOf DAVL_veh), (getPlayerUid player), (name player), _pylons, _mirrored];
	publicVariable "DAVL_serverPresets";
} else {
	if ( _hasChanged ) then {
		private _uid = getPlayerUid player;
		private _srvIdx = -1;
		{
			_x params ["_sid", "_name", "_class", "_ownerUid", "_ownerName", "_attachment", "_mirrored"];
			if ( (_class isEqualTo (typeOf DAVL_veh)) && (_ownerUid isEqualTo _uid) && (_sid isEqualTo _id) ) exitWith { _srvIdx = _forEachIndex; };
		} forEach DAVL_serverPresets;
		if ( _srvIdx < 0 ) then {
			if ( _id isEqualto "New" ) then { _id = format["%1_%2", (typeOf DAVL_veh), time]; };
			DAVL_serverPresets pushBack [_id, _name, (typeOf DAVL_veh), (getPlayerUid player), (name player), _pylons, _mirrored];
			publicVariable "DAVL_serverPresets";
		} else {
			(DAVL_serverPresets select _srvIdx) set [1, _bName];
			(DAVL_serverPresets select _srvIdx) set [5, _bPylons];
			(DAVL_serverPresets select _srvIdx) set [6, _bMirrored];
			publicVariable "DAVL_serverPresets";
		};
	};
};

DAVL_veh setVariable ["DAVL_preset", _id, true];

private _qid = (DAVL_veh getVariable ["VR_qid", -1]);

if ( _qid < 0 ) exitWith {};

(VR select _qid) set [7, _id];

publicVariable "VR";