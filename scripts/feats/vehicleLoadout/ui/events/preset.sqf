/*
@filename: feats\vehicleLoadout\ui\events\preset.sqf
Author:
	ben
Description:
	run on player,
*/

#include "..\define.hpp"

disableSerialization;

if ( DAVL_uiNoEvents ) exitWith {};

params ["_ctrl", "_idx"];

(DAVL_presets select _idx) params ["_name", "_id", "_pylons", "_mirrored", "_isShared", "_isBIS"];

DAVL_uiPreset = _id;
DAVL_uiBuffer set [0, _name];
DAVL_uiBuffer set [1, (_pylons + [])];
DAVL_uiBuffer set [2, _mirrored];

[] call vehicleLoadout_fnc_update;