/*
@filename: feats\vehicleLoadout\ui\events\name.sqf
Author:
	ben
Description:
	run on player,
*/

#include "..\define.hpp"

disableSerialization;

if ( DAVL_uiNoEvents ) exitWith {};

params ["_ctrl"];

DAVL_uiBuffer set [0, (ctrlText _ctrl)];

[] call vehicleLoadout_fnc_hasChanged;