/*
@filename: feats\vehicleLoadout\ui\events\pylon.sqf
Author:
	ben
Description:
	run on player,
*/

#include "..\define.hpp"

disableSerialization;

if ( DAVL_uiNoEvents ) exitWith {};

params ["_ctrl", "_idx"];

private _pylonIdx = (ctrlIdc _ctrl) - PYLON_BASE;
private _mag = _ctrl lbData _idx;

DAVL_uiBuffer params ["_name", "_pylons", "_mirrored"];

_pylons set [_pylonIdx, _mag];

DAVL_uiBuffer set [1, _pylons];

[] call vehicleLoadout_fnc_hasChanged;