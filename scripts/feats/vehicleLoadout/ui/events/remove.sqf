/*
@filename: feats\vehicleLoadout\ui\remove.sqf
Author:
	ben
Description:
	run on player,
*/

#include "..\define.hpp"

if ( DAVL_uiNoEvents ) exitWith {};

disableSerialization;

private _dsp = findDisplay VL_IDD;
private _presetSCR = _dsp displayCtrl PRESET_IDC;
private _pylonSCR = _dsp displayCtrl PYLON_IDC;

private _idx = lbCurSel (_presetSCR controlsGroupCtrl PRESET_SELECT);
private _id = (_presetSCR controlsGroupCtrl PRESET_SELECT) lbData _idx;

(DAVL_presets select _idx) params ["_name", "_id", "_pylons", "_mirrored", "_isShared", "_isBIS"];

if ( _isShared || _isBIS || (_id isEqualTo "New") ) exitWith {
	[] call vehicleLoadout_fnc_hasChanged;
};

private _presets = (profileNamespace getVariable ["DAVL_presets", []]);
{
    _x params ["_pid", "_name", "_class", "_attachment", "_mirrored"];
    if ( _class isEqualTo (typeOf DAVL_veh) ) then {
    	if ( _id isEqualTo _pid ) exitWith { _presets deleteAt _forEachIndex; };
	};
} forEach _presets;

profileNamespace setVariable ["DAVL_presets", _presets];
saveProfileNamespace;

DAVL_uiPreset = "";
DAVL_uiBuffer = [];

[_dsp] call vehicleLoadout_fnc_update;