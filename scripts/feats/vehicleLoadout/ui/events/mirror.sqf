/*
@filename: feats\vehicleLoadout\ui\events\mirror.sqf
Author:
	ben
Description:
	run on player,
*/

#include "..\define.hpp"

disableSerialization;

if ( DAVL_uiNoEvents ) exitWith {};

params ["_ctrl", "_state"];

DAVL_uiBuffer set [2, ( _state > 0 )];

DAVL_uiBuffer params ["_name", "_pylons", "_mirrored"];

private _dsp = findDisplay VL_IDD;
private _pylonSCR = _dsp displayCtrl PYLON_IDC;

{
	_x params ["_uiPos", "_mags", "_mirror", "_maxLoad", "_turret"];
	private _ctrl = (_pylonSCR controlsGroupCtrl (PYLON_BASE + _forEachIndex));
	if ( _mirror < 0 ) then {
		_ctrl ctrlEnable true;
	} else {
		if ( _mirrored ) then {
			_pylons set [_forEachIndex, (_pylons select _mirror)];
			private _magIdx = _mags find (_pylons select _mirror);
			_ctrl lbSetCurSel _magIdx;
		};
		_ctrl ctrlEnable !_mirrored;
	};
} forEach DAVL_pylons;

DAVL_uiBuffer set [1, _pylons];

[] call vehicleLoadout_fnc_hasChanged;