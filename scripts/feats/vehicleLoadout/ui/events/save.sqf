/*
@filename: feats\vehicleLoadout\ui\save.sqf
Author:
	ben
Description:
	run on player,
*/

#include "..\define.hpp"

if ( DAVL_uiNoEvents ) exitWith {};

disableSerialization;

private _dsp = findDisplay VL_IDD;
private _presetSCR = _dsp displayCtrl PRESET_IDC;
private _pylonSCR = _dsp displayCtrl PYLON_IDC;

private _idx = lbCurSel (_presetSCR controlsGroupCtrl PRESET_SELECT);
private _id = (_presetSCR controlsGroupCtrl PRESET_SELECT) lbData _idx;

DAVL_uiBuffer set [0, (ctrlText (_presetSCR controlsGroupCtrl PRESET_NAME))];

DAVL_uiBuffer params ["_bName", "_bPylons", "_bMirrored"];

private _presets = profileNamespace getVariable ["DAVL_presets", []];
private _profIdx = -1;
{
    _x params ["_pId", "_name", "_class", "_pylons", "_mirrored"];
    if ( _class isEqualTo (typeOf DAVL_veh) ) then {
    	if ( _id isEqualTo _pId ) then { _profIdx = _forEachIndex; };
	};
} forEach _presets;

if ( _profIdx < 0 ) then {
	_id = format["%1_%2", (typeOf DAVL_veh), time];
    _presets pushBack [_id, _bName, (typeOf DAVL_veh), _bPylons, _bMirrored];
} else {
	(_presets select _profIdx) set [1, _bName];
    (_presets select _profIdx) set [3, (_bPylons + [])];
    (_presets select _profIdx) set [4, _bMirrored];
};

profileNamespace setVariable ["DAVL_presets", _presets];
saveProfileNamespace;

DAVL_uiPreset = _id;
DAVL_uiBuffer = [];

[_dsp] call vehicleLoadout_fnc_update;