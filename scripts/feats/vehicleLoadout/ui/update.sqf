/*
@filename: feats\vehicleLoadout\ui\update.sqf
Author:
	ben
Description:
	run on player,
	update pylon ordnance dialog
*/

#include "define.hpp"

disableSerialization;

params [["_dsp", displayNull]];

if ( isNull _dsp ) then { _dsp = findDisplay VL_IDD; };
if ( isNull _dsp ) exitWith {};
if ( DAVL_uiNoEvents ) exitWith {};

DAVL_uiNoEvents = true;

DAVL_presets = [];
private _current = (getPylonMagazines DAVL_veh);

private _pylons = [];
{ _pylons pushback ""; } forEach DAVL_pylons;
DAVL_presets pushback ["New", "New", _pylons, false, false, false];
DAVL_presets pushback ["Current", "Current", _current, false, false, false];

{
    DAVL_presets pushback [getText(_x >> "displayName"), (configName _x), getArray(_x >> "attachment"), false, true, true];
} forEach ( "true" configClasses(configFile >> "cfgVehicles" >> (typeOf DAVL_veh) >> "Components" >> "TransportPylonsComponent" >> "Presets") );

{
    _x params ["_id", "_name", "_class", "_attachment", "_mirrored"];
    if ( _class isEqualTo (typeOf DAVL_veh) ) then {
    	DAVL_presets pushback [_name, _id, _attachment, _mirrored, false, false];
	};
} forEach (profileNamespace getVariable ["DAVL_presets", []]);

{
    _x params ["_id", "_name", "_class", "_ownerUid", "_ownerName", "_attachment", "_mirrored"];
    if ( _class isEqualTo (typeOf DAVL_veh) ) then {
        if ( !(_ownerUid isEqualTo (getPlayerUID player)) && (({(_x select 1) isEqualTo _id} count DAVL_presets) isEqualTo 0) ) then {
	    	DAVL_presets pushback [(format["%1 (%2)", _name, _ownerName]), _id, _attachment, _mirrored, true, false];
		};
	};
} forEach DAVL_serverPresets;


private _idx = -1;
{
    _x params ["_name", "_id", "_pylons", "_mirrored", "_isShared", "_isBIS"];
    if ( DAVL_uiPreset isEqualTo "" ) then {
    	if ( (_current isEqualTo _pylons) && (_idx < 0) ) then { _idx = _forEachIndex; };
    	if ( (DAVL_veh getVariable ["DAVL_preset", ""]) isEqualTo _id ) then { _idx = _forEachIndex; };
    } else {
    	if ( DAVL_uiPreset isEqualTo _id ) then { _idx = _forEachIndex; };
    };
} forEach DAVL_presets;

if ( _idx < 0 ) then { _idx = 1; };

//presets
private _presetSCR = _dsp displayCtrl PRESET_IDC;
private _ctrl = _presetSCR controlsGroupCtrl PRESET_SELECT;
lbClear _ctrl;
{
    _x params ["_name", "_id", "_pylons", "_mirrored", "_isShared", "_isBIS"];
    if ( _isBIS ) then { _name = format["%1 *", _name]; };
    _ctrl lbAdd _name;
    _ctrl lbsetData [_forEachIndex, _id];
} forEach DAVL_presets;

_ctrl lbSetCurSel _idx;

(DAVL_presets select _idx) params ["_name", "_id", "_pylons", "_mirrored", "_isShared", "_isBIS"];

if ( DAVL_uiBuffer isEqualTo [] ) then {
	DAVL_uiBuffer set [0, _name];
	DAVL_uiBuffer set [1, (_pylons + [])];
	DAVL_uiBuffer set [2, _mirrored];
};

DAVL_uiBuffer params ["_name", "_pylons", "_mirrored"];

(_presetSCR controlsGroupCtrl PRESET_NAME) ctrlSetText _name;

private _pylonSCR = _dsp displayCtrl PYLON_IDC;
{
	_x params ["_uipos", "_mags", "_mirror", "_maxload", "_turret"];
	private _ctrl = (_pylonSCR controlsGroupCtrl (PYLON_BASE + _forEachIndex));
	_ctrl ctrlEnable true;
	private _idx = _mags find (_pylons select _forEachIndex);
	if ( _mirrored && (_mirror >= 0) ) then {
		_idx = _mags find (_pylons select _mirror);
		_ctrl ctrlEnable false; 
	};
	if ( _idx < 0 ) then { _idx = (count _mags); };
	_ctrl lbSetCurSel _idx;
	_ctrl ctrlEnable true;	
} forEach DAVL_pylons;

(_presetSCR controlsGroupCtrl PYLON_MIRROR) cbSetChecked _mirrored;

DAVL_uiNoEvents = false;

[_dsp] call vehicleLoadout_fnc_hasChanged;