/*
@filename: feats\vehicleLoadout\ui\init.sqf
Author:
	ben
Description:
	run on player,
	init pylon ordnance dialog
*/

#include "define.hpp"
#define EMPTY_ARRAY []

disableSerialization;

waitUntil { !isNull findDisplay VL_IDD };

if ( (isNull DAVL_veh) || !(alive DAVL_veh) ) exitWith { closeDialog 0; };

private _dsp = findDisplay VL_IDD;
private _vType = typeOf DAVL_veh;


DAVL_uiBuffer = [];
DAVL_uiPreset = "";

//title
private _title = format["Ordnance Editor : %1", getText(configFile >> "cfgVehicles" >> _vType >> "displayName")];
(_dsp displayCtrl TITLE_IDC) ctrlSetText _title;

//grab pylon data.
private _hpStack = [];

{
    { _hpStack pushbackUnique _x; } forEach getArray(_x >> "hardpoints");
} forEach ( "true" configClasses(configFile >> "cfgVehicles" >> _vType >> "Components" >> "TransportPylonsComponent" >> "Pylons") );

private _magStack = [];

{
    private _mag = _x;
    private _dsp = getText(_mag >> "displayName");
	if !( _dsp isEqualto "" ) then {
    	private _hp = getArray(_mag >> "hardpoints");
    	{ 
			if ( _x in _hpStack ) exitWith { _magStack pushBack [(configName _mag), _hp]; }; 
    	} forEach _hp;
	};
} forEach ( "true" configClasses(configfile >> "CfgMagazines") );

_hpStack = nil;

DAVL_pylons = [];
{
    private _pylon = _x;    
    private _vehps = getArray(_pylon >> "hardpoints");
    private _uipos = getArray(_pylon >> "UIposition");
    private _mirror = (getNumber(_pylon >> "mirroredMissilePos") - 1);
    private _maxload = getNumber(_pylon >> "maxweight");
    private _turret = getArray (_pylon >> "turret");
    private _mags = EMPTY_ARRAY;
    {
		_x params ["_mag", "_maghps"];
        { 
			if ( _x in _maghps ) then {
                private _mass = getNumber(configfile >> "cfgMagazines" >> _mag >> "mass");
        		if ( (_mass <= _maxload) || (_maxload isEqualTo 0) ) then { _mags pushBack _mag; }; 
            };
     	} forEach _vehps;
     } forEach _magStack;
     DAVL_pylons pushback [_uipos, _mags, _mirror, _maxload, _turret];
} forEach ( "true" configClasses(configFile >> "cfgVehicles" >> _vType >> "Components" >> "TransportPylonsComponent" >> "Pylons") );

_pylonSCR = (_dsp displayCtrl PYLON_IDC); 

private _texture = getText(configFile >> "cfgVehicles" >> _vType >> "Components" >> "TransportPylonsComponent" >> "uiPicture");
(_pylonSCR controlsGroupCtrl PYLON_BCG) ctrlSetText _texture;

//create pylons ctrl
private _idc = PYLON_BASE;
{
    _x params ["_uiPos", "_mags", "_mirror", "_maxLoad", "_turret"];
    private _ctrl = _dsp ctrlCreate ["daCombo", _idc, _pylonSCR];
    {
        if ( isClass(configfile >> "cfgMagazines" >> _x) && !(_x isEqualTo "") ) then {
        	_ctrl lbAdd getText(configfile >> "cfgMagazines" >> _x >> "displayName");
        	_ctrl lbsetData [_forEachIndex, _x];     
        };
    } forEach _mags;
    _ctrl lbAdd "Empty";
    _ctrl lbsetData [(count _mags), ""];
    _ctrl ctrlSetPosition [(RATIO * TOT_WIDTH * (_uiPos select 0)), 
    					   (RATIO * (_uiPos select 1)), 
    					   PYLON_WIDTH, 
    					   LINE_HEIGHT];
	_ctrl ctrlCommit 0;
    private _event = format["[(_this select 0), (_this select 1), %1] call vehicleLoadout_fnc_evtPylon;", _forEachIndex];
    _ctrl ctrlSetEventHandler["LBSelChanged", _event];
    _idc = _idc + 1;
} forEach DAVL_pylons;

DAVL_pveh = ["DAVL_serverPresets", {[] call vehicleLoadout_fnc_update}] call pveh_fnc_add;

[_dsp] call vehicleLoadout_fnc_update;