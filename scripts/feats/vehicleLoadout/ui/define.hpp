
#define TOT_WIDTH 1.2
#define YOFFSET 0
#define SPACE 0.01
#define BUTTON_WIDTH (6.25 / 40)
#define LINE_HEIGHT (1 / 25)
#define PYLON_WIDTH (6.25 / 32)
#define RATIO (4 / 3)

#define VL_IDD 20000
#define TITLE_IDC 20001
#define APPLY_IDC 20002

#define PRESET_IDC 21000
#define PRESET_SELECT 21001
#define PRESET_NAME 21002
#define PRESET_SAVE 21003
#define PRESET_REMOVE 21004

#define PYLON_IDC 22000
#define PYLON_BCG 22001
#define PYLON_MIRROR 22002
#define PYLON_BASE 22100



