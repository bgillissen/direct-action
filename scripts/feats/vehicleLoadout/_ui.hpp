 /*
	DirectAction Vehicle Loadout
*/

#include "ui\define.hpp"

class vehicleLoadout {
	idd = VL_IDD;
	name= "vehicleLoadout";
	scriptName= "vehicleLoadout";
	movingEnable = false;
	enableSimulation = true;
	onLoad = "[] spawn vehicleLoadout_fnc_init;";
	onUnload = "[] call vehicleLoadout_fnc_destroy;";
	class Controls {
		class title : daTitle {
			idc = TITLE_IDC;
			x = 0.5 - (TOT_WIDTH / 2);
			y = 0 - LINE_HEIGHT - YOFFSET;
			w = TOT_WIDTH;
			h = LINE_HEIGHT;
		};
		class presets : daControlGroup {
			idc = PRESET_IDC;
			x = 0.5 - (TOT_WIDTH / 2);
			y = 0 + SPACE - YOFFSET;
			w = TOT_WIDTH;
			h = (LINE_HEIGHT * 2) + (SPACE * 3);
			class Controls {
				class bcg : daContainer {
					w = TOT_WIDTH;
					h = (LINE_HEIGHT * 2) + (SPACE * 3);
				};
				class select : daCombo {
					idc = PRESET_SELECT;
					x = SPACE;
					y = SPACE;
					w = TOT_WIDTH - (SPACE * 3) - BUTTON_WIDTH;
					h = LINE_HEIGHT;
					onLBSelChanged = "_this call vehicleLoadout_fnc_evtPreset;";
				};
				class name : daEdit {
					idc = PRESET_NAME;
					x = SPACE;
					y = LINE_HEIGHT + (SPACE * 2);
					w = TOT_WIDTH - (SPACE * 3) - BUTTON_WIDTH;
					h = LINE_HEIGHT;
					onKillFocus = "_this call vehicleLoadout_fnc_evtName;";
				};
				class remove : daTxtButton {
					idc = PRESET_REMOVE;
					x = TOT_WIDTH - SPACE - BUTTON_WIDTH;
					y = SPACE;
					w = BUTTON_WIDTH;
					h = LINE_HEIGHT;
					text = "Remove";
					action = "call vehicleLoadout_fnc_evtRemove;";
				};
				class save : daTxtButton {
					idc = PRESET_SAVE;
					x = TOT_WIDTH - SPACE - BUTTON_WIDTH;
					y = LINE_HEIGHT + (SPACE * 2);
					w = BUTTON_WIDTH;
					h = LINE_HEIGHT;
					text = "Save";
					action = "call vehicleLoadout_fnc_evtSave;";
				};
			};
		};
		class pylons : daControlGroup {
			idc = PYLON_IDC;
			x = 0.5 - (TOT_WIDTH / 2);
			y = 0 + (LINE_HEIGHT * 2) + (SPACE * 5) - YOFFSET;
			w = TOT_WIDTH;
			h = RATIO * (TOT_WIDTH / 2);
			class Controls {
				class mirrorTitle : daText {
					x = TOT_WIDTH - BUTTON_WIDTH - SPACE;
					y = SPACE;
					w = BUTTON_WIDTH;
					h = LINE_HEIGHT;
					text = "Mirror";
					color[] = {1,1,1,1}; 
				};
				class bcg : daContainer {
					w = TOT_WIDTH;
					h = RATIO * (TOT_WIDTH / 2);
					colorBackground[] = {0.5, 0.5, 0.5, 0.7};
				};
				class picture : daPicture {
					idc = PYLON_BCG;
					style = ST_PICTURE + ST_KEEP_ASPECT_RATIO;
					w = TOT_WIDTH;
					h = RATIO * (TOT_WIDTH / 2);
				};
				class mirror : daCheckBox {
					idc = PYLON_MIRROR;
					x = TOT_WIDTH - BUTTON_WIDTH - LINE_HEIGHT - SPACE;
					y = SPACE;
					w = LINE_HEIGHT;
					h = LINE_HEIGHT;
					onCheckedChanged = "_this call vehicleLoadout_fnc_evtMirror;";
				};
			};
		};
		class close : daTxtButton {
			x = 0.5 - (TOT_WIDTH / 2);
			y = 0 + (LINE_HEIGHT * 2) + (SPACE * 6) + (RATIO * (TOT_WIDTH / 2)) - YOFFSET;
			w = BUTTON_WIDTH;
			h = LINE_HEIGHT;
			text = "Close";
			action = "closeDialog 0;";
		};
		class apply : daTxtButton {
			idc = APPLY_IDC;
			x = 0.5 - (TOT_WIDTH / 2) + BUTTON_WIDTH + SPACE;
			y = 0 + (LINE_HEIGHT * 2) + (SPACE * 6) + (RATIO * (TOT_WIDTH / 2)) - YOFFSET;
			w = BUTTON_WIDTH;
			h = LINE_HEIGHT;
			text = "Apply";
			action = "call vehicleLoadout_fnc_evtApply;";
		};
	};
};
