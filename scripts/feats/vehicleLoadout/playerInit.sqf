/*
@filename: feats\vehicleLoadout\playerInit.sqf
Author:
	ben
Description:
	run on player,
	add action on npc to allow player to change the loadout of the vehicle parked on the marker
*/

#include "_debug.hpp"

if ( (["vehicleLoadout"] call core_fnc_getParam) == 0 ) exitWith {
	#ifdef DEBUG
    debug(LL_INFO, "disabled by mission parameter");
    #endif
    nil
};

private _presets = (profileNamespace getVariable ["DAVL_presets", []]);
private _updated = false;
{
	if ( count _X < 5 ) then {
		private _buff = [];
		_buff set [0, format["%1_%2", (_x select 1), time]];
		_buff set [1, _x select 0]; 
		_buff set [2, _x select 1];
		_buff set [3, ((_x select 2) + [])];
		_buff set [4, false];
		_presets set [_forEachIndex, _buff];
		_updated = true;
	};
} forEach _presets;

if ( _updated ) then {
	profileNamespace setVariable ["DAVL_presets", _presets];
	saveProfileNamespace;
};


DAVL_roles = (["vehicleLoadout", "roles"] call core_fnc_getSetting);
DAVL_pveh = -1;
DAVL_uiNoEvents = false;

private _act = ["vehicleLoadout", "action"] call core_fnc_getSetting;

{
	{
		_x params ["_thing", "_actions"];
		{
			if ( (configName _x) isEqualTo "vehicleLoadout" ) then {
                private _marker = getText(_x >> "marker");
				#ifdef DEBUG
				private _debug = format["action has been added to %1 for marker %2", _thing, _marker];
				debug(LL_DEBUG, _debug);
				#endif
                private _cond = format["['%1'] call vehicleLoadout_fnc_condition", _marker];
				_thing addAction [_act, {(_this select 3) call vehicleLoadout_fnc_open}, _marker, 0, false, true, "", _cond, 4];
			};
		} forEach _actions;
	} forEach _x;
} forEach [BA_npc, BA_obj, BA_veh];

nil