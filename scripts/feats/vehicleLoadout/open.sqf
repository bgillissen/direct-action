/*
@filename: feats\vehicleLoadout\action.sqf
Author:
	ben
Description:
	run on player,
	configure and open the vehicle loadout dialog
*/

#include "_debug.hpp"

private _pos = getMarkerPos _this;

if ( _pos isEqualTo [0,0,0] ) exitWith {};

private _radius = ["vehicleLoadout", "radius"] call core_fnc_getSetting;

private _objs = _pos nearEntities ["AllVehicles", _radius];

if ( (count _objs) isEqualTo 0 ) exitWith {};

DAVL_veh = objNull;

{
	if ( isClass(configFile >> "cfgVehicles" >> (typeOf _x) >> "Components" >> "TransportPylonsComponent") ) then {
		if ( count ("true" configClasses(configFile >> "cfgVehicles" >> (typeOf _x) >> "Components" >> "TransportPylonsComponent" >> "Pylons")) > 0 ) then {
			DAVL_veh = _x;
		};
	};  
	if !( isNull DAVL_veh ) exitWith {};     
} forEach _objs;

if ( isNull DAVL_veh ) exitWith {};

DAVL_marker = _this;

createDialog "vehicleLoadout";