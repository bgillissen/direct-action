class vehicleLoadout {
	roles[] = {"hPilot", "jPilot", "uavOp"};
	action = "Change Pylon ordnance";
	radius = 10;
	sleep = 3;
	markerText = "Pylon ordnance";
	markerIcon = "mil_triangle";
	start = "Servicing %1. %2 pylons to reconfigure. Stand by...";
	progress = "Placing a %1 on pylon %2.";
	emptyProgress = "Emptying pylon %1.";
	end = "%1 successfully serviced.";
};
