/*
@filename: feats\vehicleLoadout\setPylons.sqf
Author:
	ben, Matthew
Description:
	run anywhere,
	apply given pylons mag selection to the given vehicle
*/

params ["_player", "_veh", "_pylons"];

if ( isNull _player || isNull _veh ) exitWith { diag_log "hmmm"; };

if !( local _veh ) exitWith {
     diag_log "not local, remote execution";
	_this remoteExec ["vehicleLoadout_fnc_setPylons", _veh, false];    
};

diag_log "local, doing it";

private _isUAV = ( _veh in allUnitsUAV );
private _trgt = _veh;


if ( _isUAV ) then {
	_trgt = objNull;
	if ( isUAVConnected _veh ) then { _trgt = ((UAVControl _veh) select 0); };
};

//store fuel, stop engine, remove fuel, stop vehicle
private _fuel = fuel _veh;
_veh engineOn false;
_veh setFuel 0;
_veh setVelocity [0,0,0];

private _vType = (typeOf _veh);
private _vName = getText(configFile >> "CfgVehicles" >> _vType >> "DisplayName");
private _sleep = (["vehicleLoadout", "sleep"] call core_fnc_getSetting);

private _msg = format[(["vehicleLoadout", "start"] call core_fnc_getSetting), _vName, (count _pylons)];
[_msg, _trgt, _isUAV] call vehicleRepair_fnc_message;
[_player, _msg] call common_fnc_systemChat;

//Need to be able to remove the weapons that are no longer going to be used.
private _pylonMags = getPylonMagazines _veh;
private _nonPylonMags = (magazines _veh) - _pylonMags;
 
{
    private _weaponMags = getArray (configFile >> "cfgWeapons" >> _x >> "Magazines");
    if (_nonPylonMags arrayIntersect _weaponMags isEqualTo []) then {
         _veh removeWeapon _x;
    };
} forEach (weapons _veh);

sleep _sleep;

{
    _x params ["_mag", "_turret"];
    
    private _current = (_forEachIndex + 1);	
    if ( _mag isEqualTo "" ) then {
        _msg = format[(["vehicleLoadout", "emptyProgress"] call core_fnc_getSetting), _current];
	} else {
    	private _magName = getText(configFile >> "cfgMagazines" >> _mag >> "displayName");
    	_msg = format[(["vehicleLoadout", "progress"] call core_fnc_getSetting), _magName, _current];    
    };
	[_msg, _trgt, _isUAV] call vehicleRepair_fnc_message;
	[_player, _msg] call common_fnc_systemChat;
        
	_veh setPylonLoadout [(_forEachIndex + 1), _mag, false, _turret];
    
    sleep _sleep;
    
} forEach _pylons;

_msg = format[(["vehicleLoadout", "end"] call core_fnc_getSetting), _vName];
[_msg, _trgt, _isUAV] call vehicleRepair_fnc_message;
[_player, _msg] call common_fnc_systemChat;

_veh setFuel _fuel;