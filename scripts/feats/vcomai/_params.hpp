class vcomai {
	title = "vcomAI";
	values[] = {0,1};
	texts[] = {"Disabled","Enabled"};
	default = 1;
};

class vcomai_onServer {
	title = "vcomAI enabled only on headless clients";
	values[] = {0,1};
	texts[] = {"Yes","No"};
	default = 0;
};
