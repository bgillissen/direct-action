/*
@filename: feats\vcomai\destroy.sqf
Author:
	Ben
Description:
	run on server and headless client,
	kill postInit thread
*/

if ( (["vcomAI"] call core_fnc_getParam) == 0 ) exitWith {
	#ifdef DEBUG
    debug(LL_INFO, "disabled by mission parameter");
    #endif    
};

if ( count(_this) == 0 ) exitWith {};

params ["_when", "_thread"];

if ( scriptDone _thread ) exitWith {};

terminate _thread;

waitUntil{ scriptDone _thread };

VCOM_stop = true;

nil