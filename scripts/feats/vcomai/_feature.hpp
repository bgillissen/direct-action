class vcomai : feat_base  {
	class player : featPlayer_base {
		class assemble : featEvent_enable {};
	};
	class headless : featHeadless_base {
		class postInit : featEvent_enable { thread=1; };
	};
	class server : featServer_base {
		class postInit : featEvent_enable { thread=1; };
	};
};
