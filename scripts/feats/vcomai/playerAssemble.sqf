/*
@filename: feats\vcomai\playerAssemble.sqf
Author:
	Ben
Description:
	run on player
	disable vcomAI on uav AI.
*/

#include "_debug.hpp"

if ( (["vcomAI"] call core_fnc_getParam) == 0 ) exitWith {
	#ifdef DEBUG
    debug(LL_INFO, "disabled by mission parameter");
    #endif    
    nil
};

params ["_unit", "_veh"];

if !( unitIsUAV _veh ) exitWith { nil };

{ _x setVariable ["NOAI", true, true]; } forEach (crew _veh);

nil