
params [['_unit', objNull]];

if ( VCOM_stop ) exitWith { true };
if ( isNull _unit ) exitWith { true };
if !( alive _unit ) exitWith { true };
if !( local _unit ) exitWith { true };
if !( simulationEnabled _unit ) exitWith { true };
if ( _unit getVariable ["NOAI", false] ) exitWith { true };

false