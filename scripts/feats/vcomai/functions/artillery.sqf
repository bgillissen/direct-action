//AI will use artillery/mortars when possible. This script makes it so each AI gets checked if they are arty capable or not
//First hash 6/14/2014
//Modified 8/15/14 - 8/5/15

//Find unit to be doing check upon.
_Unit = _this;

if ( isNil "VCOM_ArtilleryArray" ) then { VCOM_ArtilleryArray = []; };

_Vehicle = (vehicle _Unit);
if ( _Vehicle in VCOM_ArtilleryArray ) exitWith {};
//Pull the vehicle the unit is in.

//Get the vehicles class name.
_class = typeOf _Vehicle;
if (isNil ("_class")) exitWith {};
//player sidechat format ["%1",_class];
//Figure out if it is defined as artillery
_ArtyScan = getNumber(configfile >> "CfgVehicles" >> _class >> "artilleryScanner");

//Exit the script if it is not defined as artillery
if ( _ArtyScan isEqualTo 0 ) exitWith  {
	//Check if unit somehow is in the ArtilleryArray and remove them.  This can happen to units who were inside artillery pieces but ejected.
	if (_Vehicle in VCOM_ArtilleryArray) then {
		VCOM_ArtilleryArray = VCOM_ArtilleryArray - [_Vehicle];
	};

};

if (_ArtyScan isEqualTo 1) then { VCOM_ArtilleryArray pushBack _Vehicle; };