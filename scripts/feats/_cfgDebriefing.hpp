/*
@filename: feats\_cfgDebriefing.hpp
Author:
	Ben
Description:
	on all context,
	included by main.hpp,
	include features debriefing messages
*/

class CfgDebriefing {
	#include "assets\_cfgDebriefing.hpp"

	#ifdef use_checkTS
	#include "checkTS\_cfgDebriefing.hpp"
	#endif

	#ifdef use_memberOnly
	#include "memberOnly\_cfgDebriefing.hpp"
	#endif

	#ifdef use_roleRestrictions
	#include "roleRestrictions\_cfgDebriefing.hpp"
	#endif
};
