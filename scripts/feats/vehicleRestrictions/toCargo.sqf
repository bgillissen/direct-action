/*
@filename: feats\vehicleRestrictions\toCargo.sqf
Author:
	Ben
Description:
	run on player and server
	if on player, ask server to do it,
	if on server, try to find an empty cargo seat
	and move given player to it, if failed, just kick him out
*/

if !( isServer ) exitWith {
	_this remoteExec ["vehicleRestrictions_fnc_toCargo", 2, false];
};

params ["_veh", "_player"];

private _attempt = 0;
private _inCargo = false;

while { !_inCargo }  do {
	private _crew = fullcrew [_veh, "cargo", true];
	private	_freeSeats = [];
	{
		_x params ["_unit", "_role", "_index"];
		if ( _unit isEqualTo objNull ) then { _freeSeats pushback _index; };
		true;
	} count _crew;
	if ( _attempt > 10 ) exitWith { 
		_player action ["getOut", _veh];
	};
	if ( count _freeSeats == 0 ) exitWith {
		_player action ["getOut", _veh];
	};
	_seat = selectRandom _freeSeats;
	_player action ["moveToCargo", _veh, _seat];
	sleep 0.3;
	private _crew = fullcrew [_veh, "cargo", true];
	{
		_x params ["_unit"];
		if ( _unit isEqualTo _player ) exitWith { _inCargo = true; };
		true;
	} count _crew;
	_attempt = _attempt + 1 ;
};