
#include "_debug.hpp"

params ["_veh", "_seat"];

private _isException = false;

private _role = player getVariable "role";
private _uid = getPlayerUID player;
private _type = typeOf _veh;

{
	private _pools = getArray(_x >> "pools");
    if ( ({(_type in (missionNamespace getVariable _x))} count _pools) > 0 ) then {
        private _roles = getArray(_x >> "roles");
    	private _uids = getArray(_x >> "uids");
    	private _seats = getArray(_x >> "seats");
        private _ok = true;
    	if ( count(_roles) > 0 ) then { _ok = (_role in _roles); };
    	if ( _ok && count(_uids) > 0 ) then { _ok = (_uid in _uids); };
        if ( _ok && count(_seats) > 0 ) then { _ok = (_seat in _seats); };
        if ( _ok ) exitWith {
            #ifdef DEBUG
            debug(LL_INFO, "is Exception");
            #endif 
        	_isException = true; 
		};
	};
} forEach ("true" configClasses (missionConfigFile >> "settings" >> "vehicleRestrictions" >> "exceptions"));


_isException