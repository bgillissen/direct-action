class vehicleRestrictions {
	class rules {
		class CoPilot {
			params[] = {"restrictHeli"};
			kinds[] = {"Helicopter"};
			roles[] = {"hPilot"};
			seats[] = {"driver"};
			desc = "Helicopter Co-pilot (member restricted)";
			msg = "Pilot seat is restricted on this helicopter";
		};
		class hPilot {
			params[] = {"restrictHeli"};
			kinds[] = {"Helicopter"};
			notRoles[] = {"hPilot"};
			seats[] = {"driver", "turret"};
			turrets[] = {{0}};
			msg = "That seat is restricted to Helicopter pilot";
		};
		class jPilot {
			params[] = {"restrictPlane"};
			kinds[] = {"Plane"};
			notRoles[] = {"jPilot"};
			seats[] = {"driver", "turret"};
			turrets[] = {{0}};
			msg = "That seat is restricted to Jet pilot";
		};
		class hPilotFlyPlane {
			params[] = {"restrictPlane", "HPilot_cantFly_jet"};
			kinds[] = {"Plane"};
			roles[] = {"hPilot"};
			seats[] = {"driver", "turret"};
			turrets[] = {{0}};
			msg = "That seat is restricted to Jet Pilot";
		};
		class jPilotFlyHeli {
			type = 0;
			params[] = {"restrictHeli", "JPilot_cantFly_heli"};
			kinds[] = {"Helicopter"};
			roles[] = {"jPilot"};
			seats[] = {"driver", "turret"};
			turrets[] = {{0}};
			msg = "That seat is restricted to Helicopter Pilot";
		};
		class crew {
			params[] = {"restrictTank"};
			kinds[] = {"Tank", "IFV"};
			notRoles[] = {"crew"};
			seats[] = {"driver", "commander", "turret"};
			turrets[] = {{0}, {0,0}, {0,1}};
			msg = "That seat is restricted to tank crew";
		};
	};
	class exceptions {
		class allowed {
			pools[] = {"AV_heli", "AV_tank", "AV_plane"};
			roles[] = {};
			uids[] = {};
			seats[] = {};
		};
		class landMedEvac {
			pools[] = {"BV_landMedic"};
			roles[] = {"medic"};
			uids[] = {};
			seats[] = {};
		};
	};
};
