/*
@filename: feats\vehicleRestrictions\playerSwitchSeat.sqf
Author:
	Ben
Description:
	called player side when a player switch seat in a vehicle.
	check that player can drive/gun/copilot that vehicle
*/

params ["_unit", "_notneeded", "_veh"];

if ( _veh getVariable ["NOVR", false] ) exitWith { nil };

if ( _veh isKindOf "ParachuteBase" ) exitWith { nil };

if !( player call memberData_fnc_vehicleRestrictions ) exitWith { nil };

if ( [_veh, _pos] call vehicleRestrictions_fnc_isException ) exitWith { nil };

(assignedVehicleRole player) params ["_pos", "_turret"];

private _msg = ([_veh, _pos, _turret] call vehicleRestrictions_fnc_checkSeat);

if !( _msg isEqualTo "" ) then {
    systemChat _msg;
	[_veh, true] call vehicleRestrictions_fnc_kickOut;
};

nil