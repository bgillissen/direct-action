
#include "_debug.hpp"

params ["_veh", "_pos", "_turret"];

private _role = player getVariable "role";
private _desc = roleDescription player;

#ifdef DEBUG
private _debug = format["checking seat : %1 --- %2 --- %3", (typeOf _veh), _pos, _turret];
debug(LL_DEBUG, _debug);
#endif

private _msg = "";

{
    #ifdef DEBUG
    private _debug = format["checking rule %1", (configName _x)];
    debug(LL_DEBUG, _debug);
    #endif
    private _apply = true; 
    {
		if ( ([_x] call core_fnc_getParam) isEqualTo 0 ) then { _apply = false; };        
    } forEach getArray(_x >> "params");
	if ( _apply ) then {
        #ifdef DEBUG
		debug(LL_DEBUG, "param check apply");
    	#endif
		_apply = ( ({_veh isKindOf _x} count getArray(_x >> "kinds")) > 0 );
		if ( _apply ) then {
            #ifdef DEBUG
			debug(LL_DEBUG, "kind check apply");
    		#endif
			_apply = false;
			private _roles = getArray (_x >> "roles");
			if ( (count _roles > 0) && (_role in _roles) ) then { _apply = true; };
			private _notRoles = getArray (_x >> "notRoles");
			if ( (count _notRoles > 0) && !(_role in _notRoles) ) then { _apply = true; };
			if ( _apply ) then {
                #ifdef DEBUG
				debug(LL_DEBUG, "role check apply");
    			#endif
				if !( getText(_x >> "desc") isEqualTo "" ) then {
					_apply = (_desc isEqualTo getText(_x >> "desc"));
               	};
				if ( _apply ) then {
                    #ifdef DEBUG
					debug(LL_DEBUG, "description check apply");
    				#endif
                    if ( _pos in ["driver", "Driver"] ) then {
                      _apply = ("driver" in getArray(_x >> "seats"));  
                    };
                    if ( _pos isEqualTo "cargo" ) then {
                        _apply = ("cargo" in getArray(_x >> "seats"));
                    };
                    if ( _pos in ["turret", "Turret", "gunner"] ) then {
                        if ( player isEqualTo (commander _veh) ) then {
							_apply = ("commander" in getArray(_x >> "seats"));   
                        } else {
                            _apply = ("turret" in getArray(_x >> "seats"));
                            if ( _apply && (count getArray(_x >> "turrets") > 0) ) then {
								_apply = (_turret in getArray(_x >> "turrets") );
							};
						};
					};
                    if ( _apply ) then {
						_msg = getText(_x >> "msg");
                    };
                #ifdef DEBUG
				} else {
    				debug(LL_DEBUG, "description check does not apply");
    			#endif
                };
			#ifdef DEBUG
			} else {
    			debug(LL_DEBUG, "role check does not apply");
    		#endif
			};
		#ifdef DEBUG
		} else {
    		debug(LL_DEBUG, "kind check does not apply");
    	#endif
        };
	#ifdef DEBUG
	} else {
    	debug(LL_DEBUG, "param check does not apply");
    #endif
    };
    if !( _msg isEqualTo "" ) exitWith {
        #ifdef DEBUG
    	private _debug = format["rule %1 is preventing player to use that seat", (configName _x)];
    	debug(LL_INFO, _debug);
    	#endif 
    };
} forEach ("true" configClasses (missionConfigFile >> "settings" >> "vehicleRestrictions" >> "rules"));

_msg