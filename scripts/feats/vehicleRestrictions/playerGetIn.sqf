/*
@filename: feats\vehicleRestrictions\playerGetIn.sqf
Author:
	Ben
Description:
	tun on player,
	called when player enter a vehicle.
	check that player can drive/gun/copilot that vehicle
*/

#include "_debug.hpp"

params ["_unit", "_pos", "_veh", "_turret"];

if ( _veh getVariable ["NOVR", false] ) exitWith { nil };
		
if ( _veh isKindOf "ParachuteBase" ) exitWith { nil };

if !( player call memberData_fnc_vehicleRestrictions ) exitWith { nil };

if ( ([_veh, _pos] call vehicleRestrictions_fnc_isException) ) exitWith { nil };

private _msg = ([_veh, _pos, _turret] call vehicleRestrictions_fnc_checkSeat);

if !( _msg isEqualTo "" ) then {
    systemChat _msg;
	[_veh] call vehicleRestrictions_fnc_kickOut;
};

nil