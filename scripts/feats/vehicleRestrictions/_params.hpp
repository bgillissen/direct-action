class restrictHeli {
	title = "Restrict Helicopters to helicopter pilot slots";
	values[] = {0,1};
	texts[] = {"Disabled","Enabled"};
	default = 1;
};

class HPilot_cantfly_jet {
	title = "Helicopter pilot can't fly planes";
	values[] = {0,1};
	texts[] = {"no","yes"};
	default = 1;
};

class restrictPlane {
	title = "Restrict Planes to jet pilot slots";
	values[] = {0,1};
	texts[] = {"Disabled","Enabled"};
	default = 1;
};

class JPilot_cantfly_heli {
	title = "Jet pilot can't fly helicopters";
	values[] = {0,1};
	texts[] = {"no","yes"};
	default = 1;
};

class restrictTank {
	title = "Restrict Tanks to crew slots";
	values[] = {0,1};
	texts[] = {"Disabled","Enabled"};
	default = 1;
};
