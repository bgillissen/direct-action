class vehicleRestrictions {
	tag = "vehicleRestrictions";
	class functions {
		class checkSeat { file="feats\vehicleRestrictions\checkSeat.sqf"; };
		class kickOut { file="feats\vehicleRestrictions\kickOut.sqf"; };
		class isException { file="feats\vehicleRestrictions\isException.sqf"; };
		class playerGetIn { file="feats\vehicleRestrictions\playerGetIn.sqf"; };
		class playerSwitchSeat { file="feats\vehicleRestrictions\playerSwitchSeat.sqf"; };
		class toCargo { file="feats\vehicleRestrictions\toCargo.sqf"; };
	};
};
