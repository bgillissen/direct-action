#define BOTH 1
#define LEFT 2
#define RIGHT 3

#include "..\..\common\_dik.hpp"

class radioChart {
	keycode[] = {DIK_PAUSE, 1, 0, 0}; //shift + pause
};

class radioFreq {
	keycode[] = {DIK_CTRLPAUSE, 0, 1, 0}; //ctrl + pause
	onCloseVA = 1;
	onGetIn = 1;
	onGroup = 1;
	onTake = 1;
	delay = 5;
	class vehFilters {
		class air {
			types[] = {"Helicopter", "Plane"};
			seats[] = {"driver", "gunner"};
		};
		class armor {
			types[] = {"Tank", "APC", "IFV"};
			seats[] = {"commander"};
		};
		class other {
			types[] = {"Car", "Truck", "Ship"};
			seats[] = {"commander", "driver"};
		};
	};
	class channels {
		class hq {
			name = "HQ";
			channel = 1;
		};
		class alpha {
			name = "Alpha";
			channel = 2;
		};
		class bravo {
			name = "Bravo";
			channel = 3;
		};
		class charlie {
			name = "Charlie";
			channel = 4;
		};
		class eagle {
			name = "Eagle";
			channel = 5;
		};
		class hammer {
			name = "Hammer";
			channel = 6;
		};
		class support {
			name = "Support";
			channel = 7;
		};
	};
	class presets {
		class all {
			DDFreqs[] = {"32", "33", "34", "35", "36", "37", "38", ""};
			LRFreqs[] = {"30", "35", "40", "45", "50", "55", "60", "", ""};
			SRFreqs[] = {"300", "310", "320", "330", "340", "350", "360", ""};
			SRStereo = BOTH;
			SR2Stereo = BOTH;
			LRStereo = BOTH;
			LR2Stereo = BOTH;
		};
		class sl {
			role = "sl";
			SRStereo = LEFT;
			SR2Stereo = LEFT;
			LRStereo = RIGHT;
			LR2Stereo = RIGHT;
		};
		class tl {
			role = "tl";
			SRStereo = LEFT;
			SR2Stereo = LEFT;
			LRStereo = RIGHT;
			LR2Stereo = RIGHT;
		};
		class hq {
			role = "hq";
			DD = "hq";
			SR = "hq";
			SR2 = "hq";
			SRStereo = LEFT;
			SR2Stereo = LEFT;
			LR = "hq";
			LR2 = "hq";
			LRStereo = RIGHT;
			LR2Stereo = RIGHT;
		};
		class alpha {
			squad = "ALPHA";
			squadName = "Alpha";
			DD = "alpha";
			SR = "alpha";
			SR2 = "alpha";
			LR = "alpha";
			LR2 = "alpha";
		};
		class bravo {
			squad = "BRAVO";
			squadName = "Bravo";
			DD = "bravo";
			SR = "bravo";
			SR2 = "bravo";
			LR = "bravo";
			LR2 = "bravo";
		};
		class charlie {
			squad = "CHARLIE";
			squadName = "Charlie";
			DD = "charlie";
			SR = "charlie";
			SR2 = "charlie";
			LR = "charlie";
			LR2 = "charlie";
		};
		class eagle {
			squad = "HORNET";
			squadName = "Eagle";
			DD = "eagle";
			SR = "eagle";
			SR2 = "eagle";
			SRStereo = LEFT;
			SR2Stereo = LEFT;
			LR = "eagle";
			LR2 = "eagle";
			LRStereo = RIGHT;
			LR2Stereo = RIGHT;
		};
		class hammer {
			squad = "HAMMER";
			squadName = "Hammer";
			DD = "hammer";
			SR = "hammer";
			SR2 = "hammer";
			LR = "hammer";
			LR2 = "hammer";
		};
		class tankCmdr {
			desc = "Tank Commander";
			SRStereo = LEFT;
			SR2Stereo = LEFT;
			LRStereo = RIGHT;
			LR2Stereo = RIGHT;
		};
		class support {
			squad = "SUPPORT";
			squadName = "Support";
			DD = "support";
			SR = "support";
			SR2 = "support";
			LR = "support";
			LR2 = "support";
		};
		class jtac {
			role = "jtac";
			squad = "SUPPORT";
			squadName = "Support";
			SRStereo = LEFT;
			SR2Stereo = LEFT;
			LRStereo = RIGHT;
			LR2Stereo = RIGHT;
		};
		class spotter {
			role = "spotter";
			squad = "SUPPORT";
			squadName = "Support";
			SRStereo = LEFT;
			SR2Stereo = LEFT;
			LRStereo = RIGHT;
			LR2Stereo = RIGHT;
		};
	};
};
