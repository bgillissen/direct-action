class radioFreq : feat_base  {
	class player : featPlayer_base {
		class closeVA : featEvent_enable {};
		class getIn : featEvent_enable {};
		class group : featEvent_enable {};
		class init : featEvent_enable {};
		class killed : featEvent_enable {};
		class remoteControl : featEvent_enable {};
		class respawn : featEvent_enable { order=99; };
		class take : featEvent_enable {};
	};
};
