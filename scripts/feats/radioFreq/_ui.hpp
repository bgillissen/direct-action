
#include "define.hpp"

class radioFreq {
	idd = RADIOFREQ_IDD;
	movingenable=false;
	class controls {
		class chart : daPicture {
			style = 48 + 2048;
			text = "\TFUDA\ui\freq-cheat-sheet.paa";
			x = 0.5 - (WIDTH / 2);
			y = 0.5 - (HEIGTH / 2) - YOFFSET;
			w = WIDTH;
			h = HEIGHT;
		};
	};
};
