/*
@filename: feats\radioFreq\playerGroup.sqf
Author:
	Ben
Description:
	run on player,
	setup the owned radios following the settings
*/

#include "_debug.hpp"

params ['_player', '_old', '_new'];

if !( _player isEqualTo player ) exitWith { nil };

if ( isNil 'TF_firstSpawn' ) exitWith { nil };

if ( TF_firstSpawn ) exitWith { nil };

if ( BLACKSCREEN ) exitWith { nil };

if ( (["radioFreq"] call core_fnc_getParam) == 0 ) exitWith {
	#ifdef DEBUG
	debug(LL_DEBUG, "disabled by mission parameter");
    #endif
    nil      
};

if ( (["radioFreq", "onGroup"] call core_fnc_getSetting) == 0 ) exitWith {
	#ifdef DEBUG
	debug(LL_DEBUG, "onGroup, disabled by feature setting");
	#endif
    nil      
};

if !( MOD_tfar ) exitWith {
   #ifdef DEBUG
    debug(LL_DEBUG, "TFAR is not present, abording");
    #endif
    nil  
};

radioFreq_ctxt = "groupChange";
if ( call TFAR_fnc_haveSWRadio ) then { ["shortRange"] call radioFreq_fnc_prog; };
if ( call TFAR_fnc_haveLRRadio ) then { ["longRange"] call radioFreq_fnc_prog; };
if ( call TFAR_fnc_haveDDRadio ) then { ["underWater"] call radioFreq_fnc_prog; };

nil