/*
@filename: feats\radioFreq\playerInit.sqf
Author:
	Ben
Description:
	run on player,
	to prevent take action to be called alot of time while setting up loadout
*/

#include "define.hpp"
#include "_debug.hpp"

if ( (["radioFreq"] call core_fnc_getParam) == 0 ) exitWith {
	#ifdef DEBUG
	debug(LL_DEBUG, "disabled by mission parameter");
	#endif
    nil      
};

if !( MOD_tfar ) exitWith {
	#ifdef DEBUG
	debug(LL_DEBUG, "TFAR is not present, abording");
    #endif
    nil  
};

private _radioProgFnc = {
    radioFreq_ctxt = "keyBind";
	if ( call TFAR_fnc_haveSWRadio ) then { ["shortRange"] call radioFreq_fnc_prog; };
	if ( call TFAR_fnc_haveLRRadio ) then { ["longRange"] call radioFreq_fnc_prog; };
    if ( call TFAR_fnc_haveDDRadio ) then { ["underWater"] call radioFreq_fnc_prog; };
};
["radioFreq", "Configure radios", _radioProgFnc] call keybind_fnc_add;

private _openChartFnc = {
    if ( isNull (findDisplay RADIOFREQ_IDD) ) then { 
    	createDialog "radioFreq"; 
	} else {
        closeDialog 0;
    };
};

["radioChart", "Open radio frequencies cheat sheet", _openChartFnc] call keybind_fnc_add;

radioFreqDisableTake = true;

nil