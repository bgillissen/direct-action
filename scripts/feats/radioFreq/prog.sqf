/*
@filename: feats\radioFreq\prog.sqf
Author:
	Ben
Description:
	run on player,
	programe the given or active player's radio
Params:
	STRING, radio type (shortRange, longRange)
	Mixed, 	STRING, shortRange radio class name (not prototype), 
			ARRAY, [object, number] for longRange, radio object and radio ID
Environment:
	missionConfig:
		radioFreq >> channels
		radioFreq >> presets
Return:
	nothing 
*/

#include "_debug.hpp"

params ["_type", ["_radio", ""]];

private _prole = player getVariable "role";
private _pdesc = roleDescription player;
private _psquad = (player call groups_fnc_getSquad);
private _psquadName = groupId (group player);
if ( _psquad isEqualTo "" ) then { _psquad = "__custom__"; };

#ifdef DEBUG
private _debug = format["player squad is %1", _psquad];
debug(LL_DEBUG, _debug);
#endif
        
private _configs = [];
private _configNames = [];
{
    private _skip = false;
    private _squad = getText(_x >> "squad");
    private _squadName = getText(_x >> "squadName");
    private _role = getText(_x >> "role");
    private _desc = getText(_x >> "desc");
    private _toCheck = [];
    if !( _squad isEqualTo "" ) then { _toCheck pushback [_squad, _psquad]; };
	if !( _squadName isEqualTo "" ) then { _toCheck pushback [_squadName, _psquadName]; };
    if ( (count _toCheck) > 0 ) then {
        _skip = true;
    	{
        	if ( (_x select 0) isEqualTo (_x select 1) ) then { _skip = false; };
    	} forEach _toCheck;
	};
    if !( _skip ) then {
    	if !( _role isEqualTo "" ) then {
        	_skip = !(_prole isEqualTo _role);    
        };
	};
    if !( _skip ) then {
        if !( _desc isEqualTo "" ) then {
        	_skip = !(_pdesc isEqualTo _desc);    
        };
    };
    if !( _skip ) then { 
    	_configs pushback _x;
        _configNames pushback (configName _x); 
    };
} forEach ("true" configClasses (missionConfigFile >> "settings" >> "radioFreq" >> "presets") );

if ( (count _configs) == 0 ) exitWith {
    #ifdef DEBUG
	debug(LL_DEBUG, "no setting apply to player squad, abording");
	#endif
};

#ifdef DEBUG
private _debug = format["radio presets applyed to player: %1", (_configNames joinString ", ")];
debug(LL_DEBUG, _debug);
#endif

private ["_s",
		 "_f", 
		 "_p", 
         "_pStereo",
         "_pChanCall", 
         "_pStereoCall", 
         "_a",
         "_aStereo", 
         "_aChanCall", 
         "_aStereoCall", 
         "_cChanCall", 
         "_radObj", 
         "_radId", 
         "_msg"];
if ( _type isEqualTo "shortRange" ) then {
    if ( _radio isEqualTo "" ) then {
        _radio = nil;
        if ( call TFAR_fnc_haveSWRadio ) then { _radio = call TFAR_fnc_activeSwRadio; };
	};
    if ( isNil "_radio" ) exitWith {};
    if ( _radio isEqualTo "" ) exitWith { _radio = nil; };
    _s = true;
    _f = "SRFreqs";
	_p = "SR";
    _pStereo = "SRStereo";
    _pChanCall = "[_radio, _chan] call TFAR_fnc_setSwChannel";
    _pStereoCall = "[_radio, %1] call TFAR_fnc_setSwStereo";
	_a = "SR2";
    _aStereo = "SR2Stereo";
    _aChanCall = "[_radio, _chan] call TFAR_fnc_setAdditionalSwChannel";
    _cChanCall = "_radio call TFAR_fnc_getAdditionalSwChannel";
    _aStereoCall = "[_radio, %1] call TFAR_fnc_setAdditionalSwStereo";    
    _msg = "on your SR radio";
};
if ( _type isEqualTo "longRange" ) then {
    if ( _radio isEqualTo "" ) then { _radio =  call TFAR_fnc_activeLrRadio; };
    if ( _radio isEqualTo [] ) exitWith { _radio = nil; };
    _radObj = (_radio select 0);
    _radId = (_radio select 1);
    _s = true;
    _f = "LRFreqs";
	_p = "LR";
    _pStereo = "LRStereo";
    _pChanCall = "[_radObj, _radId, _chan] call TFAR_fnc_setLrChannel";
    _pStereoCall = "[_radObj, _radId, %1] call TFAR_fnc_setLrStereo";
	_a = "LR2";
    _aStereo = "LR2Stereo";
    _aChanCall = "[_radObj, _radId, _chan] call TFAR_fnc_setAdditionalLrChannel";
    _cChanCall = "[_radObj, _radId] call TFAR_fnc_getAdditionalLrChannel";
    _aStereoCall = "[_radObj, _radId, %1] call TFAR_fnc_setAdditionalLrStereo";
    _msg = "on your LR radio";
};
if ( _type isEqualTo "underWater" ) then {
    if ( _radio isEqualTo "" ) then { _radio = (vest player); };
    if !( _radio isEqualTo "V_RebreatherB" ) exitWith { _radio = nil; };
    _s = false;
    _f = "DDFreqs";
	_p = "DD";
    _pStereo = "";
    _pChanCall = "TF_dd_frequency = _frequencies select _chan";
    _pStereoCall = "";
	_a = "";
    _aStereo = "";
    _aChanCall = "";
    _cChanCall = "";
    _aStereoCall = "";
    _msg = "on your DD radio";
};
if ( isNil "_radio" ) exitWith {
	#ifdef DEBUG
	debug(LL_WARN, "no active radio found, prog aborded");
    #endif
};
    
private _frequencies = [];
private _primChan = 0;
private _primStereo = 0;
private _primName = "";
private _addChan = 0;
private _addStereo = 0;
private _addName = "";
{
    private _freqs = getArray(_x >> _f);
    if !( _freqs isEqualTo [] ) then { 
    	_frequencies = _freqs; 
    };
    
    if !( _pStereo isEqualTo "" ) then {
    	private _stereo = getNumber(_x >> _pStereo);
    	if ( _stereo > 0 ) then { _primStereo = _stereo; };
	};
    if !( _p isEqualTo "" ) then {
    	private _prim = getText(_x >> _p);
		if !( _prim isEqualTo "" ) then {
			_primChan = getNumber(missionConfigFile >> "settings" >> "radioFreq" >> "channels" >> _prim >> "channel");
        	_primName = getText(missionConfigFile >> "settings" >> "radioFreq" >> "channels" >> _prim >> "name");
   		};
	};
    
    if !( _aStereo isEqualTo "" ) then {
    	private _stereo = getNumber(_x >> _aStereo);
    	if ( _stereo > 0 ) then { _addStereo = _stereo; };
	};
    if !( _a isEqualTo "" ) then {
    	private _add = getText(_x >> _a);
   		if !( _add isEqualTo "" ) then {
			_addChan = getNumber(missionConfigFile >> "settings" >> "radioFreq" >> "channels" >> _add >> "channel");
			_addName = getText(missionConfigFile >> "settings" >> "radioFreq" >> "channels" >> _add >> "name");
		};
   	};
} forEach _configs;

if ( _s ) then {
	{
		if !( _x isEqualto "" ) then { 
	    	[_radio, (_forEachIndex + 1), _x] call TFAR_fnc_SetChannelFrequency;
		}; 
	} forEach _frequencies;
};

private _msgs = [];

if ( _primChan > 0 ) then {
    private _chan = (_primChan - 1);
	call compile _pChanCall;//set the primary channel
    if ( _primchan isEqualTo _addChan ) then {  
		_msgs pushBack format["Primary and additional channel have been set to channel %2 (%1 squad %3 Mhz)", _primName, _primChan, (_frequencies select (_primChan - 1))];
	} else {
        _msgs pushBack format["Primary channel have been set to channel %2 (%1 squad %3 Mhz)", _primName, _primChan, (_frequencies select (_primChan - 1))];
	};
};
if !( _pStereoCall isEqualTo "" ) then {
	call compile format[_pStereoCall, (_primStereo - 1)];
};

if ( _addChan > 0 ) then {
    private _chan = (_addChan - 1);
    private _curAdd = call compile _cChanCall;
    if !( _curAdd isEqualTo _chan ) then { 
		call compile _aChanCall;//set the additional channel     
	};
    if !( _primchan isEqualTo _addChan ) then {
		_msgs pushBack format["Additionnal channel has been set to channel %2 (%1 squad %3 Mhz)", _addName, _addChan, (_frequencies select (_addChan - 1))];
	};
};
if !( _aStereoCall isEqualTo "" ) then {
	call compile format[_pStereoCall, (_addStereo - 1)];
};

//{ systemChat format["%1 %2 (%3)", _x, _msg, radioFreq_ctxt]; } forEach _msgs;
{ systemChat format["%1 %2", _x, _msg]; } forEach _msgs;