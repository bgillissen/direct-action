/*
@filename: feats\radioFreq\playerTake.sqf
Author:
	Ben
Description:
	run on player,
	if taken item is a TFAR radio, set it up following the settings 
*/

#include "_debug.hpp"

if ( (["radioFreq"] call core_fnc_getParam) == 0 ) exitWith {
	#ifdef DEBUG
	debug(LL_DEBUG, "disabled by mission parameter");
    #endif
    nil      
};

if ( (["radioFreq", "onTake"] call core_fnc_getSetting) == 0 ) exitWith {
	#ifdef DEBUG
	debug(LL_DEBUG, "onTake, disabled by feature setting");
	#endif
    nil      
};

if !( MOD_tfar ) exitWith {
   #ifdef DEBUG
    debug(LL_DEBUG, "TFAR is not present, abording");
    #endif
    nil  
};

if ( radioFreqDisableTake ) exitWith {
   #ifdef DEBUG
   debug(LL_DEBUG, "take event is disabled, abording");
   #endif
   nil  
};

params ["_unit", "_cont", "_item"]; 

if !( getText(configFile >> "cfgWeapons" >> _item >> "simulation") isEqualTo "ItemRadio" ) exitWith {
    #ifdef DEBUG
    private _debug = format["item taken is not a radio (%1), abording", _item];
	debug(LL_DEBUG, _debug);
	#endif
    nil  
};

if ( getNumber (configFile >> "CfgWeapons" >> _item >> "tf_prototype") isEqualTo 0 ) exitWith {
    #ifdef DEBUG
    private _debug = format["item taken is not a radio prototype (%1), abording", _item];
	debug(LL_DEBUG, _debug);
	#endif
    nil  
};

_item spawn {
    if ( _this in TFAR_LR ) exitWith {
        #ifdef DEBUG
    	private _debug = format["item taken is a LR radio (%1)", _this];
		debug(LL_DEBUG, _debug);
		#endif  
        if ( call TFAR_fnc_haveLRRadio ) then { ["longRange"] call radioFreq_fnc_prog; };
    };
    if ( _this isEqualTo "V_RebreatherB" ) exitWith {
        #ifdef DEBUG
    	private _debug = format["item taken is a DD radio (%1)", _this];
		debug(LL_DEBUG, _debug);
		#endif  
		if ( call TFAR_fnc_haveDDRadio ) then { ["underWater"] call radioFreq_fnc_prog; };
    };
    #ifdef DEBUG
	private _debug = format["waiting for server to acknoledge (%1)", _this];
	debug(LL_DEBUG, _debug);
	#endif
	waitUntil {		
		sleep 2;
		( count ([true, _this] call radioFreq_fnc_searchRadios) == 0);
	};
	sleep 3;
    private _radios = ([false, _this] call radioFreq_fnc_searchRadios);
    #ifdef DEBUG
	private _debug = format["programing %1 SR radio(s) (%2)", count(_radios), _this];
	debug(LL_DEBUG, _debug);
	#endif
    radioFreq_ctxt = "take";
	{ ["shortRange", _x] call radioFreq_fnc_prog; } forEach _radios ;
};

nil