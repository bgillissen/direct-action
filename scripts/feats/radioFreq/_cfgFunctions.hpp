class radioFreq {
	tag = "radioFreq";
	class functions {
		class playerCloseVA { file="feats\radioFreq\playerCloseVA.sqf"; };
		class playerGetIn { file="feats\radioFreq\playerGetIn.sqf"; };
		class playerGroup { file="feats\radioFreq\playerGroup.sqf"; };
		class playerInit { file="feats\radioFreq\playerInit.sqf"; };
		class playerKilled { file="feats\radioFreq\playerKilled.sqf"; };
		class playerRemoteControl { file="feats\radioFreq\playerRemoteControl.sqf"; };
		class playerRespawn { file="feats\radioFreq\playerRespawn.sqf"; };
		class playerTake { file="feats\radioFreq\playerTake.sqf"; };
		class searchRadios { file="feats\radioFreq\searchRadios.sqf"; };
		class prog { file="feats\radioFreq\prog.sqf"; };
	};
};
