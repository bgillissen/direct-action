/*
@filename: feats\radioFreq\playerRemoteControl.sqf
Author:
	Ben
Description:
	run on player,
	update TFAR_currentUnit and broadcast tf_controlled_unit player's variable 
*/

#include "_debug.hpp"

params ["_gm", "_player", "_unit", "_ctrl"];

if !( MOD_tfar ) exitWith {
    #ifdef DEBUG
	debug(LL_DEBUG, "disabled by absence of tfar");
	#endif
    nil
};

if ( _ctrl ) then {
	TFAR_currentUnit = _unit; 
    _player setVariable["tf_controlled_unit", _unit, true];
} else {
	TFAR_currentUnit = player;
    _player setVariable["tf_controlled_unit", nil, true];
};

nil