/*
@filename: feats\radioFreq\playerRespawn.sqf
Author:
	Ben
Description:
	run on player,
	check that the player's radio has not been switched with a RF-7800
	setup the owned radios following the settings
*/

#include "_debug.hpp"

if ( (["radioFreq"] call core_fnc_getParam) == 0 ) exitWith {
	#ifdef DEBUG
	debug(LL_INFO, "disabled by mission parameter");
    #endif
    nil      
};

if !( MOD_tfar ) exitWith {
   #ifdef DEBUG
	debug(LL_DEBUG, "TFAR is not present, abording");
	#endif
    nil  
};

radioFreqDisableTake = false;

[] spawn {
	/*
    if ( isNil 'TF_firstSpawn' ) then { TF_firstSpawn = true; };
    if ( TF_firstSpawn ) then {
    	#ifdef DEBUG
		debug(LL_DEBUG, "waiting for first radio request to be completed");
		#endif
    	waitUntil { sleep 1;!TF_firstSpawn };
	};
	*/
	[] spawn {
		if ( BLACKSCREEN ) then { waitUntil { sleep 1;!BLACKSCREEN }; };
		if ( call TFAR_fnc_haveSWRadio ) then {
			private _radio = call TFAR_fnc_activeSwRadio;
			private _found = false;
			{
				if ( ([_radio, _x] call TFAR_fnc_isSameRadio)) exitWith { _found = true; };
			} count TFAR_SR;
			if ( !_found ) then {
				player unlinkItem _radio;
				player linkItem (TFAR_SR select 0);
			};
		};
	    if ( count ([true, (TFAR_SR select 0)] call radioFreq_fnc_searchRadios) > 0 ) then {
	    	#ifdef DEBUG
			debug(LL_DEBUG, "SR radio prototype found, waiting for server to acknoledge");
			#endif
	        waitUntil {		
				sleep 2;
				( count ([true, (TFAR_SR select 0)] call radioFreq_fnc_searchRadios) == 0 );
			};
		};
        sleep 3;
	    #ifdef DEBUG
		debug(LL_DEBUG, "programing SR radio");
		#endif  
        radioFreq_ctxt = 'respawn';
	    ["shortRange"] call radioFreq_fnc_prog;
	};
	
	[] spawn {
	    if ( BLACKSCREEN ) then { waitUntil { sleep 1;!BLACKSCREEN }; };
		if ( call TFAR_fnc_haveLRRadio ) then {
	        #ifdef DEBUG
			debug(LL_DEBUG, "programing LR radio");
			#endif 
            radioFreq_ctxt = 'respawn';
	        ["longRange"] call radioFreq_fnc_prog; 
		};
	};

    [] spawn {
	    if ( BLACKSCREEN ) then { waitUntil { sleep 1;!BLACKSCREEN }; };
		if ( call TFAR_fnc_haveDDRadio ) then {
	        #ifdef DEBUG
			debug(LL_DEBUG, "programing DD radio");
			#endif 
            radioFreq_ctxt = 'respawn';
	        ["underWater"] call radioFreq_fnc_prog; 
		};
	};
};

nil