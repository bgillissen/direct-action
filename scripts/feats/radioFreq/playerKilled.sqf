/*
@filename: feats\radioFreq\playerKilled.sqf
Author:
	Ben
Description:
	run on player,
	prevent player to speak globally upon death
*/

#include "_debug.hpp"

if ( (["radioFreq"] call core_fnc_getParam) == 0 ) exitWith {
	#ifdef DEBUG
	debug(LL_DEBUG, "disabled by mission parameter");
	#endif
    nil      
};

if !( MOD_tfar ) exitWith {
	#ifdef DEBUG
	debug(LL_DEBUG, "TFAR is not present, abording");
    #endif
    nil  
};

radioFreqDisableTake = true;

nil