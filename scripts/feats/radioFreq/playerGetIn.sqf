/*
@filename: feats\radioFreq\playerGetIn.sqf
Author:
	Ben
Description:
	run on player,
	setup the vehicle's long range radio following the settings
*/

#include "_debug.hpp"

if ( (["radioFreq"] call core_fnc_getParam) == 0 ) exitWith {
	#ifdef DEBUG
	debug(LL_DEBUG, "disabled by mission parameter");
	#endif
    nil      
};

if ( (["radioFreq", "onGetIn"] call core_fnc_getSetting) == 0 ) exitWith {
	#ifdef DEBUG
	debug(LL_DEBUG, "onGetIn, disabled by feature setting");
	#endif
    nil      
};


if !( MOD_tfar ) exitWith {
	#ifdef DEBUG
	debug(LL_DEBUG, "TFAR is not present, abording");
    #endif
    nil  
};

params ["_unit", "_pos", "_veh", "_turret"];

if !( _veh call TFAR_fnc_hasVehicleRadio ) exitWith {
    #ifdef DEBUG
    debug(LL_DEBUG, "no LR in player's vehicle, abording");
    #endif
    nil    
};                      

if ( _veh getVariable['tf_radioFreqSet', false] ) exitWith {
    #ifdef DEBUG
    debug(LL_DEBUG, "player's vehicle radio have already been configured, abording");
    #endif
    nil    
}; 
                                                                		
if ( _pos isEqualTo "cargo" ) exitWith {
	#ifdef DEBUG
    debug(LL_DEBUG, "player is in a cargo seat, abording");
    #endif
    nil    
}; 

if !( count(player call TFAR_fnc_lrBackpack) > 0 ) exitWith {
	#ifdef DEBUG
   	debug(LL_DEBUG, "player got a LR backpack radio, abording");
    #endif
    nil    
};

if ( player getVariable ["agony", false] ) exitWith {
    #ifdef DEBUG
    debug(LL_DEBUG, "player is unconcious, abording");
    #endif
    nil    
};
    
private _vehConf = (player call TFAR_fnc_vehicleLr);
if ( (count _vehConf) == 0 ) exitWith {
    #ifdef DEBUG
   	debug(LL_DEBUG, "no LR config linked to the player's seat, abording");
    #endif
    nil    
};

private _vehConf = (((player call TFAR_fnc_vehicleLr) select 1) splitString "_") select 0;
private _do = false;
{
    if ( ({ _veh isKindOf _x } count getArray(_x >> "types")) > 0 ) then {
        if ( ({ _vehConf isEqualTo _x } count getArray(_x >> "seats")) > 0 ) then {
			_do = true;
		};
	};
} forEach ("true" configClasses(missionConfigFile >> "settings" >> "radioFreq" >> "vehfilters"));

if !( _do ) exitWith {
	#ifdef DEBUG
    debug(LL_DEBUG, "filtered out, abording");
    #endif
    nil
};

private _side = PLAYER_SIDE call {
	if ( _this isEqualTo west ) exitWith { "west" };
	if ( _this isEqualTo east ) exitWith { "east" };
	"independent"
};

_veh setVariable ["tf_side", _side, true];
_veh setVariable['tf_radioFreqSet', true, true];

radioFreq_ctxt = "getIn";
["longRange"] call radioFreq_fnc_prog;

nil