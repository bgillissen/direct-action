/*
@filename: feats\radioFreq\playerCloseVA.sqf
Author:
	Ben
Description:
	run on player,
	setup the owned radios following the settings
*/

#include "_debug.hpp"

if ( (["radioFreq"] call core_fnc_getParam) == 0 ) exitWith {
	#ifdef DEBUG
	debug(LL_DEBUG, "disabled by mission parameter");
    #endif
    nil      
};

if ( (["radioFreq", "onCloseVA"] call core_fnc_getSetting) == 0 ) exitWith {
	#ifdef DEBUG
	debug(LL_DEBUG, "onCloseVA, disabled by feature setting");
	#endif
    nil      
};

if !( MOD_tfar ) exitWith {
	#ifdef DEBUG
    debug(LL_DEBUG, "TFAR is not present, abording");
    #endif
    nil  
};

[] spawn {
	waitUntil {		
		sleep 0.5;
		( count ([true] call radioFreq_fnc_searchRadios) == 0);
	};
	sleep 2;
    radioFreq_ctxt = "closeVA";
	if ( call TFAR_fnc_haveSWRadio ) then { ["shortRange"] call radioFreq_fnc_prog; };
	if ( call TFAR_fnc_haveLRRadio ) then { ["longRange"] call radioFreq_fnc_prog; };
};

nil