class taskNew {
	title = "New objective";
	description = "%1";
	iconPicture = "A3\ui_f\data\igui\cfg\simpletasks\types\%2_ca.paa";
	//color[] = {1, 0.81, 0.06, 1};
	color[] = {0.75, 0.49, 0.09, 1};
	priority = 9;
	duration = 5;
};

class taskSucceeded {
	title = "Objective complete";
	description = "%1";
	iconPicture = "A3\ui_f\data\igui\cfg\simpletasks\types\%2_ca.paa";
	color[] = {0.3, 0.8, 0.06, 1};
	priority = 9;
	duration = 4;
};

class taskFailed {
	title = "Objective failed";
	description = "%1";
	iconPicture = "A3\ui_f\data\igui\cfg\simpletasks\types\%2_ca.paa";
	color[] = {0.8, 0.13, 0.14, 1};
	priority = 9;
	duration = 4;
};

class taskCanceled {
	title = "Objective canceled";
	description = "%1";
	iconPicture = "A3\ui_f\data\igui\cfg\simpletasks\types\%2_ca.paa";
	color[] = {1, 1, 1, 0.7};
	priority = 9;
	duration = 4;
};

class subTaskNew {
	title = "New Sub-Objective";
	description = "%1";
	iconPicture = "A3\ui_f\data\igui\cfg\simpletasks\types\%2_ca.paa";
	//color[] = {1, 0.81, 0.06, 1};
	color[] = {0.75, 0.49, 0.09, 1};
	priority = 8;
	duration = 5;
};

class subTaskSucceeded {
	title = "Sub-Objective complete";
	description = "%1";
	iconPicture = "A3\ui_f\data\igui\cfg\simpletasks\types\%2_ca.paa";
	color[] = {0.3, 0.8, 0.06, 1};
	priority = 8;
	duration = 4;
};

class subTaskFailed {
	title = "Sub-Objective failed";
	description = "%1";
	iconPicture = "A3\ui_f\data\igui\cfg\simpletasks\types\%2_ca.paa";
	color[] = {0.8, 0.13, 0.14, 1};
	priority = 8;
	duration = 4;
};

class subTaskCanceled {
	title = "Sub-Objective canceled";
	description = "%1";
	iconPicture = "A3\ui_f\data\igui\cfg\simpletasks\types\%2_ca.paa";
	color[] = {1, 1, 1, 0.7};
	priority = 8;
	duration = 4;
};
