/*
@filename: feats\tasks\playerInit.sqf
Author:
	Ben
Description:
	run on player,
	create the tasks using serverTasks stack
*/

#include "_debug.hpp"

playerTasks = [];

call tasks_fnc_playerImport;

tasksPVEH = ["serverTasks", {call tasks_fnc_playerImport}] call pveh_fnc_add; 

nil