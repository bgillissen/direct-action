class tasks {
	tag = "tasks";
	class functions {
		class serverPreInit { file="feats\tasks\serverPreInit.sqf"; };
		class serverDestroy { file="feats\tasks\serverDestroy.sqf"; };
		class serverAdd { file="feats\tasks\server\add.sqf"; };
		class serverExport { file="feats\tasks\server\export.sqf"; };
		class serverRemove { file="feats\tasks\server\remove.sqf"; };
		class serverSetDesc { file="feats\tasks\server\setDesc.sqf"; };
		class serverSetState { file="feats\tasks\server\setState.sqf"; };
		class playerInit { file="feats\tasks\playerInit.sqf"; };
		class playerDestroy { file="feats\tasks\playerDestroy.sqf"; };
		class playerImport { file="feats\tasks\player\import.sqf"; };
		class playerCreate { file="feats\tasks\player\create.sqf"; };
		class playerRemove { file="feats\tasks\player\remove.sqf"; };
		class playerSetDesc { file="feats\tasks\player\setDesc.sqf"; };
		class playerSetState { file="feats\tasks\player\setState.sqf"; };
	};
};
