
if !( CTXT_PLAYER ) exitWith {};

#include "..\_debug.hpp"

params ["_taskId", ["_recur", true]];

private _isRoot = ( isNil "tasks_recursiveRemoveRoot" );
if ( _isRoot ) then { tasks_recursiveRemoveRoot = true; };

private _result = true;
private _gotChild = false;
{
    _x params ["_id", "_cTask", "_parent"];
    if ( _parent isEqualTo _taskId ) then {
        _gotChild = true;
        if ( _recur ) then {
        	_result = [_id, _recur] call tasks_fnc_playerRemove;
		} else {
            _result = false;
        };        
    };
} forEach playerTasks;

if !( _result ) exitWith {
    #ifdef DEBUG
    private "_debug";
    if ( _gotChild && !_recur ) then {
    	_debug = ["non-recursive delete impossible, tasks '%1' got child!", _taskId];
    } else {
        _debug = ["failed to remove at least one child tasks of task '%1'", _taskId];
    };
    debug(LL_ERR, _debug);
    #endif
    false
};

_result = false;
{
    _x params ["_id", "_cTask", "_parent"];
    if ( _id isEqualTo _taskId ) exitWith {
        _result = true;
        player removeSimpleTask _cTask; 
		playerTasks deleteAt _forEachIndex;        
    };
} forEach playerTasks;

if ( _isRoot ) then { tasks_recursiveRemoveRoot = nil; };

_result