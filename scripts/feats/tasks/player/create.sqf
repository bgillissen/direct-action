
if !( CTXT_PLAYER ) exitWith {};

#include "..\_debug.hpp"

params ["_id", "_name", "_type", "_title", "_desc", ["_parent", -1], ["_pos",[]], ["_hud", ""], ["_state", "None"]];

private _exists = false;
{
    _x params ["_taskId", "_cTask", "_pId"];
    if ( _taskId isEqualTo _id ) exitWith { _exists = true; };
} forEach playerTasks;

if ( _exists ) exitWith {
	#ifdef DEBUG
    private _debug = format["task '%1' already exists", _id];
    debug(LL_DEBUG, _debug);
    #endif
};

private "_task";
if ( _parent >= 0 ) then {
    private _found = false;
    private "_pTask";
    {
        _x params ["_taskId", "_cTask", "_pId"];
        if ( _taskId isEqualTo _parent ) exitWith { 
        	_pTask = _cTask;
            _found = true; 
		};           
    } forEach playerTasks;
	if !( _found ) then {
        #ifdef DEBUG
        private _debug = format["parent task '%1' of task '%2', does not exists, creating it", _parent, _id];
    	debug(LL_DEBUG, _debug);
        #endif
    	{
            if ( (_x select 0) isEqualTo _parent ) exitWith { _x call tasks_fnc_playerCreate; };
        } forEach serverTasks;
		{
        	_x params ["_taskId", "_cTask", "_pId"];
        	if ( _taskId isEqualTo _parent ) exitWith { 
        		_pTask = _cTask;
            	_found = true; 
			};           
    	} forEach playerTasks;
    };
    if ( _found ) then {
    	_task = player createSimpleTask [_name, _pTask];
	#ifdef DEBUG
    } else {
		private _debug = format["failed to create parent task '%1' of task '%2'", _parent, _id];
    	debug(LL_DEBUG, _debug);
	#endif
    }; 
} else {
	_task = player createSimpleTask [_name];    
};

if ( isNil "_task" ) exitwith {
	#ifdef DEBUG
	private _debug = format["failed to create task '%1'", _id];
    debug(LL_DEBUG, _debug);
    #endif
};

if !( _pos isEqualTo [] ) then { _task setSimpleTaskDestination _pos; };
 
_task setSimpleTaskType _type;
_task setTaskState _state;
_task setSimpleTaskDescription [_desc, _title, _hud];

playerTasks pushback [_id, _task, _parent];

#ifdef DEBUG
private _debug = format["task '%1' has been created", _id];
debug(LL_DEBUG, _debug);
#endif