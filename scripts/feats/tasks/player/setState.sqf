
if !( CTXT_PLAYER ) exitWith {};

if ( isNil "playerTasks" ) exitWith {};

params ["_taskId", "_newState", ["_recur", false]];

private _found = false;
private _isRoot = ( isNil "tasks_recursiveStateRoot" );

if ( _isRoot ) then { tasks_recursiveStateRoot = true; };

{
    _x params ["_id", "_task", "_parent"];
    if ( _id isEqualTo _taskId ) then { 
    	_task setTaskState _newState;
	};
    if ( _recur && (_parent isEqualTo _taskId) ) then {
        [_id, _newState, true] call tasks_fnc_playerSetState;        
    };
} forEach playerTasks;

if ( _isRoot ) then { tasks_recursiveStateRoot = nil; };