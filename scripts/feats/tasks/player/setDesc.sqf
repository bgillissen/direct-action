
if !( CTXT_PLAYER ) exitWith {};

if ( isNil "playerTasks" ) exitWith {};

params ["_taskId", "_newDesc"];

private _found = false;
{
    _x params ["_id", "_task", "_parent"];
    if ( _id isEqualTo _taskId ) exitWith {
        private _data = taskDescription _task;
        _data set [0, _newDesc];
        _task setSimpleTaskDescription _data;  
	};
} forEach playerTasks;