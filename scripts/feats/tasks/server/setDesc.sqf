
if !( CTXT_SERVER ) exitWith {
    _this remoteExec ["tasks_fnc_serverSetDesc", 2, false];
};

params ["_taskId", "_newDesc"];

private _found = false;
{
    _x params ["_id", "_name", "_type", "_title", "_desc", "_parent", "_pos", "_hud", "_state"];
    if ( _id isEqualTo _taskId ) exitWith {
        _found = true;
        (serverTasks select _forEachIndex) set [4, _newDesc];
	};
} forEach serverTasks;

if !( _found ) exitWith {};

publicVariable "serverTasks";
_this remoteExec ["tasks_fnc_playerSetDesc", 0, false];
