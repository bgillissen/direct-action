
if !( CTXT_SERVER ) exitWith {
    _this remoteExec ["tasks_fnc_serverSetState", 2, false];
};

params ["_taskId", "_newState", ["_recur", false], ["_notif", false]];

private _found = false;
private _isRoot = ( isNil "tasks_recursiveStateRoot" );

if ( _isRoot ) then { tasks_recursiveStateRoot = true; };

{
    _x params ["_id", "_name", "_type", "_title", "_desc", "_parent", "_pos", "_hud", "_state"];
    if ( _id isEqualTo _taskId ) then {
        _found = true;
        (serverTasks select _forEachIndex) set [8, _newState];
        if ( _notif ) then {
            private '_notifType';
            if ( _isroot ) then {
                _notifType = format['task%1', _newState];
            } else {
                _notifType = format['subTask%1', _newState];
            };
	        [_notifType, [_title, _type]] call global_fnc_notification;
        };
    };
    if ( _recur && (_parent isEqualTo _taskId) ) then {
        [_id, _newState, true] call tasks_fnc_serverSetState;        
    };
} forEach serverTasks;

if ( _isRoot ) then { tasks_recursiveStateRoot = nil; };

if ( _found && _isRoot ) then {
    publicVariable "serverTasks";
	_this remoteExec ["tasks_fnc_playerSetState", 0, false];
};

