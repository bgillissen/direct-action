
if !( CTXT_SERVER ) exitWith {
    _this remoteExec ["tasks_fnc_serverExport", 2, false];
};

#include "..\_debug.hpp"

#ifdef DEBUG
debug(LL_DEBUG, "serverExport call");
#endif

publicVariable "serverTasks";

if ( CTXT_PLAYER ) then {
    publicVariableServer "serverTasks";
};