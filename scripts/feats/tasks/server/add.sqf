
if !( CTXT_SERVER ) exitWith {
    _this remoteExec ["tasks_fnc_serverAdd", 2, false];
};

#include "..\_debug.hpp"

params ["_name", "_type", "_title", "_desc", ["_notif", false], ["_parent", -1], ["_pos",[]], ["_hud", ""], ["_state", "None"]];

private _taskId = serverTime * random [1, 5, 10];

private _entry = [_taskId, _name, _type, _title, _desc, _parent, _pos, _hud, _state];

serverTasks pushback _entry;

if ( _notif ) then {
	private '_notifType';
   if ( _parent < 0 ) then {
        _notifType = 'taskNew';
    } else {
        _notifType = 'subTaskNew';
    };
    [_notifType, [_title, _type]] call global_fnc_notification;
};

_taskId