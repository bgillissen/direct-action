
if !( CTXT_SERVER ) exitWith {
    _this remoteExec ["tasks_fnc_serverRemove", 2, false];
};

params ["_taskId", ["_recur", false]];

private _found = false;
private _isRoot = ( isNil "tasks_recursiveRemoveRoot" );
if ( _isRoot ) then { tasks_recursiveRemoveRoot = true; };

{
    _x params ["_id", "_type", "_desc", "_title", "_hud", "_state", "_pos", "_parent"];
    if ( _id isEqualTo _taskId ) then {
        _found = true;
		serverTasks deleteAt _forEachIndex;        
    };
    if ( _recur && (_parent isEqualTo _taskId) ) then {
        [_id, _recur] call tasks_fnc_serverRemove;        
    };
} forEach serverTasks;

if ( _isRoot ) then { tasks_recursiveRemoveRoot = nil; };

if ( _found && _isRoot ) then {
    publicVariable "serverTasks";
	_this remoteExec ["tasks_fnc_playerRemove", 0, false];
};