

#include "_debug.hpp"

private _pos = [];
{
	private _types = getArray (_x >> "types");
	private _cpos = getArray (_x >> "pos");
	{
		if ( _this isKindOf _x ) exitWith { _pos = _cpos; }; 
	} forEach _types;
	if ( count _pos > 0 ) exitWith {
		#ifdef DEBUG
		private _debug = format["Using crate position for %1 @ %2", (configName _x), _pos];
		debug(LL_DEBUG, _debug);
		#endif
	};
} forEach ("true" configClasses (missionconfigFile >> "settings" >> "supplyDrop" >> "Positions"));

_pos