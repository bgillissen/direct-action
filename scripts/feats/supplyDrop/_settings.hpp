class supplyDrop {
	minChuteAltitude = 50;
	msgFrom = "HQ";
	msgAvail = "Supply Drop is available.";
	msgDeployed = "Supply Drop has been deployed by %1, next drop available in %2min.";
	checkDelay = 20;
	action = "<t color='#0000ff'>Drop supply crate</t>";
	smoke = "SmokeShellPurple";
	light = "Chemlight_blue";
	class Positions {
		class chinook {
			types[] = {"Heli_Transport_03_base_F", "RHS_CH_47F_10"};
			pos[] = {0, -10, -2};
		};
		class seaKing {
			types[] = {"Heli_Transport_02_base_F", "rhsusf_CH53E_USMC_D"};
			pos[] = {0, -11, 0}; 
		};
		class hawk {
			types[] = {"Heli_light_03_base_F", "Heli_Light_02_base_F", "Heli_Transport_01_base_F", "RHS_UH60M"};
			pos[] = {5, 1.5, -1};
		};
		class taru {
			types[] = {"Heli_Transport_04_base_F"};
			pos[] = {0, -3, -8};
		};
		class default {
			types[] = {"All"};
			pos[] = {0, -15, -1};
		};
	};
};
