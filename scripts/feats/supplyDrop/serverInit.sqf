/*
@filename: feats\supplyDrop\serverInit.sqf
Author:
	Ben
Description:
	run on server,
	must be spawn, not called,
	a loop that act as a coolDown for supplyDrop avaibility
Params:
	none
Environment:
	missionParameters:
		supplyDrop
		supplyDrop_cooldown
		supplyDrop_maxcrate
	missionNamespace:
		SD_avail
		SD_crates
	missionConfig:
		supplyDrop >> msgFrom
		supplyDrop >> msgAvail
		supplyDrop >> checkDelay
Return:
	nothing
*/

#include "_debug.hpp"

if ( (["supplyDrop"] call core_fnc_getParam) == 0 ) exitWith {
    #ifdef DEBUG
    debug(LL_INFO, "disabled by mission parameter");
    #endif
    nil
};

private _cooldown = ["supplyDrop_cooldown"] call core_fnc_getParam;
private _maxcrate = ["supplyDrop_maxcrate"] call core_fnc_getParam;
private _from =  ["supplyDrop", "msgFrom"] call core_fnc_getSetting;
private _msg =  ["supplyDrop", "msgAvail"] call core_fnc_getSetting;
private _checkDelay =  ["supplyDrop", "checkDelay"] call core_fnc_getSetting;



SD_avail = true;
publicVariable "SD_avail";
SD_spawnedCrates = [];
publicVariable "SD_spawnedCrates";
SD_exclusionZones = [];
{
	_x params ["_area", "_actions"];
    if ( "cleanForced" in _actions ) then { SD_exclusionZones pushback _area; };
} forEach BA_zones;
publicVariable "SD_exclusionZones";

#ifdef DEBUG
private _debug = format["thread as started with %1s delay, %2s cooldown, 3% crates maximum", _checkDelay, _cooldown, _maxcrate];
debug(LL_DEBUG, _debug);
#endif

while { true } do {
	if ( !SD_avail ) then {
		sleep _cooldown;
		if ( (count SD_spawnedCrates) > _maxCrate ) then {
            #ifdef DEBUG
			debug(LL_DEBUG, "maximum crates reached, removing the first one spawned");
			#endif
			private _crate = SD_spawnedCrates select 0;
			{ deleteVehicle _x; } count _crate;
			SD_spawnedCrates deleteAt 0;
			publicVariable "SD_spawnedCrates";
		};
		[1, _msg, [_from, PLAYER_SIDE]] call global_fnc_chat;
		SD_avail = true;
		publicVariable "SD_avail";
	};
	sleep _checkDelay;
};

nil