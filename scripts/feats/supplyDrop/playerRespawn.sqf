/*
@filename: feats\supplyDrop\playerOnRespawn.sqf
Author:
	Ben
Description:
	run on player
	Add action to heli/jet pilots to allow them to do a supply drop.
Params:
	none
Environment:
	missionParameters:
		SupplyDrop
		JPilot_fly_heli
	player:
		role
	missionConfig:
		supplyDrop >> action
Return:
	nothing
*/

#include "_debug.hpp"

if ( (["supplyDrop"] call core_fnc_getParam) == 0 ) exitWith {
	#ifdef DEBUG
    debug(LL_DEBUG, "disabled by mission parameter");
    #endif
    nil    
};

private _isPilot = ( (player getVariable "role") in ["hPilot", "jPilot"] );
if !( _isPilot ) then { _isPilot =  !( player call memberData_fnc_vehicleRestrictions ); };

if !( _isPilot ) exitWith {
    #ifdef DEBUG
    debug(LL_DEBUG, "player can not fly, abording");
    #endif
    nil
};

private _action = ["supplyDrop", "action"] call core_fnc_getSetting;
player addAction [_action, supplyDrop_fnc_drop, [], 0, false, true, '', 'call supplyDrop_fnc_condition'];

nil