/*
@filename: feats\supplyDrop\serverDestroy.sqf
Author:
	Ben
Description:
	run on server side
	kill the supply cooldown thread 
*/

if ( count(_this) == 0 ) exitWith { nil };

if ( (["supplyDrop"] call core_fnc_getParam) == 0 ) exitWith { nil };

params ["_when", "_thread"];

terminate _thread;

waitUntil{ scriptDone _thread };

SD_avail = false;
publicVariable "SD_avail";

{
    [_x] call common_fnc_deleteObjects;
} forEach SD_spawnedCrates; ;

SD_spawnedCrates = [];

publicVariable "SD_spawnedCrates";

nil