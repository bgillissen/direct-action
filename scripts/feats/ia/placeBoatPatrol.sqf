/*
@filename: feats\ia\placeBoatPatrol.sqf
Author:
	Ben
Description:
	run on server,
	spawn attack boats inside the given coordonate
*/

#include "_debug.hpp"

params ["_coord", "_size", "_amount", "_skill", "_patrolSize", ["_ctxt", "unknown"]];

private _groups = [];

if ( _amount <= 0 ) exitWith { _groups };

#ifdef DEBUG
private _debug = format["%1 | spawning %2 boat(s)", _ctxt, _amount];
debug(LL_DEBUG, _debug);
#endif

private _pool = [];
{
    switch (_x) do {
        case east : { _pool pushBack "O_Boat_Armed_01_hmg_F"; };
        case west : { _pool pushBack "B_Boat_Armed_01_minigun_F"; };
        case independent : { _pool pushBack "I_Boat_Armed_01_minigun_F"; };
    };
} forEach ENEMIES;

private _doLock = ( (["ia", "lockVeh"] call core_fnc_getSetting) == 1 );

for "_x" from 1 to _amount do {
    private _side = selectRandom ENEMIES;
    private _group = createGroup [_side, true];
    _group setGroupIdGlobal [([(format["%1 - Boat", _ctxt]), allGroups, {groupId _this}] call common_fnc_getUniqueName)];
    private _randomPos = [];
	private _class = (selectRandom _pool);
    private _found = false;
    private _expireAt = time + 20;
	while { !_found  && (time <= _expireAt) } do {
		_randomPos = ([_coord, 50, _size / 2, (sizeOf _class), 2] call BIS_fnc_findSafePos);
    	_found = ( (getTerrainHeightASL _randomPos) <= -3 );
	};
    if  !( _found ) exitWith {};
	private _veh = createVehicle [_class, _randomPos, [], 0, "NONE"];
    createVehicleCrew _veh;
    _veh engineOn true;
	if ( _doLock ) then { _veh lock 3; };
    (crew _veh) joinSilent _group;
	_group setCombatMode "RED";   
	[_group, _randomPos, _patrolSize] call common_fnc_seaPatrol;
    [(units _group), _skill] call common_fnc_setSkill;
    [_veh, true] call curator_fnc_addEditable;
    [_group, true] call dynSim_fnc_set;
    _groups pushback _veh;
    _groups pushback _group;
};

_groups