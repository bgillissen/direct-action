
class casNew {
	title = "Enemy CAS Inbound";
	iconPicture =  "A3\ui_f\data\igui\cfg\simpletasks\types\plane_ca.paa";
	description = "The enemy has called air support, you better get rid of the radio tower ASAP!";
	color[] = {0.8, 0.13, 0.14, 1};
	priority = 8;
	duration = 5;
};

class casDestroyed {
	title = "Enemy CAS Neutralized";
	iconPicture =  "A3\ui_f\data\igui\cfg\simpletasks\types\plane_ca.paa";
	description = "Good job, the enemy cas has been destroyed";
	color[] = {0.3, 0.8, 0.06, 1};
	priority = 8;
	duration = 4;
};

