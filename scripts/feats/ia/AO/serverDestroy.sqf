/*
@filename: feats\ia\AO\serverDestroy.sqf
Author:
	Ben
Description:
	run on server side
	tell main AO to stop, wait some, kill it 
*/

if ( count(_this) == 0 ) exitWith { nil };

params ["_when", "_thread"];

if ( (["AO"] call core_fnc_getParam) == 0 ) exitWith { nil };

AO_stop = true;

if !( isNil "AO_thread" ) then {
	private _limit = time + 20;
	waitUntil { ( (time > _limit) || (scriptDone AO_thread) ) };
    terminate AO_thread;
};

if !( isNull AO_casThread ) then {
    private _limit = time + 20;
	waitUntil {
		( (time > _limit) || (scriptDone AO_casThread) )
	};
	terminate AO_casThread;
};

if !( isNull AO_casGroup ) then {
	deleteGroup AO_casGroup;
	AO_casGroup = grpNull;
};

terminate _thread;

waitUntil { scriptDone _thread };

nil