/*
@filename: feats\ia\AO\threadAO.sqf
Author:
	Ben
Description:
	this run on server,
	it start a main AO on the given marker, and wait for it to complete 
*/

#include "_debug.hpp"

params ["_ao"];

private _coord = getMarkerPos _ao;
private _size = ["AO_size"] call core_fnc_getParam;
//spawn units
private _units = [_coord, _size] call AO_fnc_placeEnemies;

//create detection triggers
private _triggers = [];
{
	private _trigger = createTrigger ["EmptyDetector", _coord];
	_trigger setTriggerArea [_size, _size, 0, false];
	_trigger setTriggerActivation [toUpper (str _x), "PRESENT", false];
	_trigger setTriggerStatements ["this","",""];
	_triggers pushback _trigger;
} foreach ENEMIES;

//spawn radioTower
private _radioTower = [_coord, _size, _triggers] call AO_fnc_placeRadioTower;

//create markers
private _circle = ["ia", "ao", "circle"] call core_fnc_getSetting;
private _label = ["ia", "ao", "label"] call core_fnc_getSetting;
private _color = ["colorBLUFOR", "colorOPFOR"] select (PLAYER_SIDE isEqualTo west);
private _mainTitle = format[(["ia", "ao", "mainTitle"] call core_fnc_getSetting), _ao];
createMarker [_circle, _coord];
_circle setMarkerColor _color;
_circle setMarkerShape "ELLIPSE";
_circle setMarkerSize [_size, _size];
_circle setMarkerBrush "FDiagonal";
createMarker [_label, _coord];
_label setMarkerColor _color;
_label setMarkerShape "ICON";
_label setMarkerType "hd_dot";
_label setMarkerText _mainTitle;
_circle = nil;
_label = nil;
_color = nil;

private _unitThreshold  = ["AO_unitThreshold"] call core_fnc_getParam;

//marker, tasks
private _mainDesc = format[(["ia", "ao", "mainDesc"] call core_fnc_getSetting), _ao];
private _infTitle = (["ia", "ao", "infTitle"] call core_fnc_getSetting);
private _infDesc = (["ia", "ao", "infDesc"] call core_fnc_getSetting);
private _rtTitle = (["ia", "ao", "rtTitle"] call core_fnc_getSetting);
private _rtDesc = (["ia", "ao", "rtDesc"] call core_fnc_getSetting); 
private _mainTask = (["AO", "attack", _mainTitle, _mainDesc, true] call tasks_fnc_serverAdd);
private _infTask = (["INF", "kill", _infTitle, format[_infDesc, _unitThreshold], false, _mainTask] call tasks_fnc_serverAdd);
private _rtTask = (["RT", "destroy", _rtTitle, _rtDesc, false, _mainTask] call tasks_fnc_serverAdd);
call tasks_fnc_serverExport;
_mainTitle = nil;
_mainDesc = nil;
_infTitle = nil;
_infDesc = nil;
_rtTitle = nil;
_rtDesc = nil;


//spawn cas thread
private _doCAS = [false, true] select (["AO_cas"] call core_fnc_getParam);
if ( _doCAS ) then {
	AO_casThread = [_radioTower] spawn AO_fnc_threadCAS;
#ifdef DEBUG    
} else {
    debug(LL_DEBUG, "CAS is disabled by mission parameter");
#endif
};

private _firstLoop = true; //needed because sometimes the trigger does not count all the units right after spawn.
private _rtCheck = true;
private _infCheck = true;
private _converging = false;
private _delay = ["ia", "checkDelay"] call core_fnc_getSetting;

while { (_rtCheck || _infCheck) && !zeusMission && !AO_stop } do {
	if ( !(alive _radioTower) && _rtCheck ) then {
        _rtCheck = false;
        //radioTower has been destroyed
		deleteMarker (["ia", "ao", "radioTower", "circle"] call core_fnc_getSetting);
		deleteMarker (["ia", "ao", "radioTower", "label"] call core_fnc_getSetting);
		[_rtTask, "Succeeded", false, true] call tasks_fnc_serverSetState;
	};
    if ( _infcheck ) then {
		private _sum = 0;
		{ 
        	if ( isNil "_sum" ) exitWith { _sum = _unitThreshold; };
        	_sum = _sum + (count list _x); 
        } forEach _triggers;
        if ( !isNil "_sum" && !_firstLoop ) then {
	   		if ( _sum < _unitThreshold ) then {
        		_infCheck = false;
        		[_infTask, "Succeeded", false, false] call tasks_fnc_serverSetState;
    		} else {
    			if ( !_converging && (_sum <= (_unitThreshold * 2)) ) then {
    				_converging = true;
    				{
                		if ( _x isEqualType grpNull ) then {
                			if ( ({alive _x} count(units _x)) > 0 ) then {
		                    	_x call common_fnc_removeWP;
		                    	_x setBehaviour "COMBAT";
		                    	_x setCombatMode "RED";
			                    private _wp = _x addWaypoint [_coord, (_size / 3), 0];
			                    _wp setWaypointType "DESTROY";
		    	    			_wp setWaypointCompletionRadius (random[5, 25, 50]);
		        	            _wp setWaypointBehaviour "COMBAT";
		            	        _wp setWaypointCombatMode "RED";
		                	    _wp setWaypointSpeed "FULL";
		                    	_wp setWaypointStatements ["true", "(group this) call common_fnc_removeWP;[(group this), AO_coord, (random[100, 250, 400])] call BIS_fnc_taskPatrol"];
		                    	_x setCurrentWaypoint _wp;
							}; 
		                };
		            } forEach _units;
    			};
    		};
		};
	};
    sleep _delay;
    _firstLoop = false;
};

if ( !_infCheck && !_rtCheck ) then {
    [_mainTask, "Succeeded", false, true] call tasks_fnc_serverSetState;
} else {
    [_mainTask, "Canceled", true, false] call tasks_fnc_serverSetState;
};

//cleanUp
[_coord, _triggers, _radioTower, _units, (zeusMission || AO_stop)] spawn AO_fnc_cleanup;