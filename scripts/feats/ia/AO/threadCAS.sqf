/*
@filename: feats\ia\AO\threadCAS.sqf
Author:
	Ben
Description:
	this run on server,
	it keep respawning CAS over AO until the radioTower is alive or a zeusMission has not started
*/

#include "_debug.hpp"

params ["_radioTower"];

if ( isNull AO_casGroup ) then {
	AO_casGroup = createGroup (selectRandom ENEMIES);
    AO_casGroup setVariable ["NOCLEANUP", true];
    AO_casGroup setVariable ["NOLB", true];
	AO_casGroup setGroupIdGlobal ["AO CAS"];
    #ifdef DEBUG    
	debug(LL_DEBUG, "casGroup has been re-created");
	#endif
};

private _delay = ["ia", "checkDelay"] call core_fnc_getSetting;
private _checkDelay = ["ia", "ao", "cas", "checkDelay"] call core_fnc_getSetting; 
private _cooldown = ["ia", "ao", "cas", "cooldown"] call core_fnc_getSetting;
private _infAmmo = [false, true] select (["ia", "ao", "cas", "infiniteAmmo"] call core_fnc_getSetting);
private _infFuel = [false, true] select (["ia", "ao", "cas", "infiniteFuel"] call core_fnc_getSetting);
private _range = ["ia", "ao", "cas", "searchRadius"] call core_fnc_getSetting;

[(_cooldown + ((random  _cooldown) / 2)), _checkDelay, "(zeusMission || AO_stop)"] call common_fnc_smartSleep;
if ( zeusMission || AO_stop ) exitWith {};
    
while { (alive _radioTower) && !AO_stop } do {
	if ( !AO_cas ) then {
        #ifdef DEBUG    
		debug(LL_DEBUG, "no CAS in the air, spawning one");
		#endif
		(["cas"] call ia_fnc_randomSide) params ["_side", "_vehPool", "_key"];
		private _spawnPos = [(random 10000),(random 10000),3000];
		private _cas = createVehicle [(selectRandom _vehPool), _spawnPos, [] , 0, "NONE"];
        createVehicleCrew _cas;
        
        if ( isClass(configFile >> "cfgVehicles" >> (typeOf _cas) >> "Components" >> "SensorsManagerComponent") ) then {
			_cas setVehicleReportRemoteTargets true;
    		_cas setVehicleReceiveRemoteTargets true; 
		};
		_cas engineOn TRUE;
		_cas allowCrewInImmobile true;
		_cas flyInHeight 1000;
		_cas lock 2;
        private _pilot = (driver _cas);
        _pilot setRank "COLONEL";
        _pilot setVariable ["NOAI", true, true];
        (crew _cas) joinSilent AO_casGroup;
		AO_casGroup setCombatMode "RED";
		AO_casGroup setBehaviour "COMBAT";
		AO_casGroup setSpeedMode "FULL";
		[(units AO_casGroup), (["ia", "ao", "cas", "skill"] call core_fnc_getSetting)] call common_fnc_setSkill;
        [[_pilot, _cas], false] call curator_fnc_addEditable;
		["casNew", []] call global_fnc_notification;
		AO_cas = true;		 
		waitUntil {
			if ( _InfAmmo ) then { _cas setVehicleAmmo 1; };
			if ( _InfFuel ) then { _cas setFuel 1; };
			_cas flyInHeight (200 + (random 850));
			private _casPos = getPosATL _cas;
			private _targets = _casPos nearEntities [["Air"], _range];
            if !( AO_coord isEqualTo [0,0,0] ) then {
                AO_casGroup call common_fnc_removeWP;
            	[AO_casGroup, AO_coord] call BIS_fnc_taskAttack;
			} else {
                if !( SIDE_coord isEqualTo [0,0,0] ) then {
                    AO_casGroup call common_fnc_removeWP; 
                	[AO_casGroup, SIDE_coord] call BIS_fnc_taskAttack; 
				};
            };
			{ AO_casGroup reveal [_x, 4]; } count _targets;
			[_checkDelay, 5, "(zeusMission || AO_stop)", "CAS"] call common_fnc_smartSleep;
			( !alive _cas || !alive _pilot || !canMove _cas || zeusMission || AO_stop )
		};
        #ifdef DEBUG    
		debug(LL_DEBUG, "cas waitUntil passed");
		#endif
        
		AO_cas = false;
		if ( !alive _cas || !alive _pilot || !canMove _cas ) then {
            ["casDestroyed", []] call global_fnc_notification;
		};
	};
	[(_cooldown + ((random  _cooldown) / 2)), _checkDelay, "(zeusMission || AO_stop)"] call common_fnc_smartSleep;
	if ( zeusMission || AO_stop ) exitWith {
		deleteVehicle _pilot;
		deleteVehicle _cas;
    };
};