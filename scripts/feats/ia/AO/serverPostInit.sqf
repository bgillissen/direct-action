/*
@filename: feats\ia\AO\serverPostInit.sqf
Author:
	Ben
Description:
	run on server,
	keeps track of the active AO thread, and create a new one when needed 
*/

#include "_debug.hpp"

AO_coord = [0,0,0];

if ( isNil "AO_zones" ) then {
	AO_zones = "true" configClasses (missionConfigFile >> "settings" >> "maps" >> toUpper(worldName) >> "zones");
};

if ( (["AO"] call core_fnc_getParam) == 0 ) exitWith {
    #ifdef DEBUG
    debug(LL_INFO, "disabled by mission parameter");
    #endif
    nil
};

AO_stop = false;
AO_zone = nil;
AO_thread = scriptNull;
AO_cas = false;
AO_casThread = scriptNull;
AO_casGroup = grpNull; 

private _zones = [];
private _checkDelay = ["ia", "checkDelay"] call core_fnc_getSetting;
private _cooldown = ["ia", "ao", "cooldown"] call core_fnc_getSetting;

#ifdef DEBUG
private _debug = format["thread has started with %1s delay, %2s cooldown", _checkDelay, _cooldown];
debug(LL_INFO, _debug);
#endif


while { !AO_stop } do {	
	
	if ( count _zones == 0 ) then { 
		_zones = AO_zones;
	};
	
	AO_zone = selectRandom _zones;
	_zones = _zones - [AO_zone];
    #ifdef DEBUG
	private _debug = format["active zone is %1", (configName AO_zone)];
	debug(LL_DEBUG, _debug);
	#endif  
	
	private _markbuff = getArray(AO_zone >> "aos");
	private _blacklist = getArray(missionConfigFile >> "settings" >> "baseAtmosphere" >> toUpper(worldName) >> BASE_NAME >> "aoBlacklist");
    #ifdef DEBUG
	private _debug = format["blacklist for active base : %1", _blacklist];
	debug(LL_DEBUG, _debug);
	#endif
    _markbuff = _markbuff - _blacklist;
	#ifdef DEBUG
	private _debug = format["%1 AOs left in active zone after blacklist", (count _markbuff)];
	debug(LL_DEBUG, _debug);
	#endif
    
	if ( (count _markbuff) > 0 ) then {
        private _consecutive = getNumber(AO_zone >> "consecutiveAOS");
        #ifdef DEBUG
		private _debug = format["%1 consecutive AOs in active zone", _consecutive];
		debug(LL_DEBUG, _debug);
		#endif
        private _markers = [];
		for "_i" from 0 to (_consecutive - 1) do {
			private _m = selectRandom _markbuff;
			_markers = _markers + [_m];
			_markbuff = _markbuff - [_m];
		};
		_markbuff = nil;
        #ifdef DEBUG
		private _debug = format["selected AOs in active zone: %1", (_markers joinString ", ")];
		debug(LL_DEBUG, _debug);
		#endif
		{
			[true, "AO_stop"] call zeusMission_fnc_checkAndWait;
			if ( AO_stop ) exitWith {};
			if !( (getMarkerPos _x) isEqualTo [0,0,0] ) then {
                #ifdef DEBUG
				private _debug = format["starting '%1'", _x];
				debug(LL_DEBUG, _debug);
				#endif
                AO_coord = getMarkerPos _x;
				AO_thread = [_x] spawn AO_fnc_threadAO;
				waitUntil {
					sleep _checkDelay;
					scriptDone AO_thread
				};
                AO_coord = [0,0,0];
            #ifdef DEBUG
				private _debug = format["'%1' is done", _x];
				debug(LL_DEBUG, _debug);
            } else {
                private _debug = format["marker '%1' was not found", _x];
				debug(LL_ERR, _debug);
			#endif
			};
			if ( !zeusMission ) then {
				[_cooldown, _checkDelay, "AO_stop"] call common_fnc_smartSleep;
				if ( AO_stop ) exitWith {};
			};
		} count _markers;
	} else {
    	sleep _checkDelay;
	};
};

nil