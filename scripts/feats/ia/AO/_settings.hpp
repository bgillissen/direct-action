class ao {
	cooldown = 120;
	circle = "aoCircle";
	label = "aoLabel";
	patrolSkill = 2;
	sniperSkill = 3;
	garrisonSkill = 2;
	staticSkill = 3;
	aaSkill = 5;
	tankSkill = 3;
	apcSkill = 2;
	carSkill = 4;
	airSkill = 4;
	airAltitude = 150;
	mainTitle = "Take %1";
	mainDesc = "New Target<br/><br/>We have located a large concentration of enemy forces at %1. They are heavily armed; expect tank, anti-air and motorised infantry support. Your objectives are to destroy the enemy radio tower being used to coordinate air support and to eliminate all hostiles in the target area.";
	infTitle = "Eliminate hostile forces";
	infDesc = "Eliminate as much enemy as possible, we will consider this task done once less than %1 enemies remain.";
	rtTitle = "Destroy the radio tower";
	rtDesc = "Destroy the enemy radio tower.<br/>This will prevent the enemies to call in CAS support.<br/>Bring explosives or coordinate an airstrike on the base of the tower.<br/>Expect the tower to be mined.";
	class radioTower {
		circle = "rtCircle";
		label = "rtLabel";
		size = 500;
		mines = 60;
		mineTypes[]={"APERSBoundingMine", "APERSMine", "ATMine"};
	};
	class cas {
		skill = 2;
		checkDelay = 20;
		cooldown = 1200;
		infiniteAmmo = 0;
		infiniteFuel = 1;
		searchRadius = 5000;
	};
};
