/*
@filename: feats\ia\AO\cleanup.sqf
Credit:
	Quiksilver
Author:
	Ben
Description:
	run on server,
	remove the objects spawned for an AO if no players are near 
*/

params ["_aoCoord", "_triggers", "_radioTower", "_enemies", "_force"];

deleteMarker (["ia", "ao", "circle"] call core_fnc_getSetting);
deleteMarker (["ia", "ao", "label"] call core_fnc_getSetting);
deleteMarker (["ia", "ao", "radioTower", "circle"] call core_fnc_getSetting);
deleteMarker (["ia", "ao", "radioTower", "label"] call core_fnc_getSetting);

[_triggers] call common_fnc_deleteObjects;

if ( !_force ) then {
	private _delay = ["ia", "checkDelay"] call core_fnc_getSetting;
	private _dist = ["ia", "deleteDistance"] call core_fnc_getSetting;
	waitUntil {
		sleep _delay;
		(({((_x distance _aoCoord) < _dist)} count allPlayers) isEqualTo 0)
	};
};

[[_radioTower]] call common_fnc_deleteObjects;

[AO_minefield] call common_fnc_deleteObjects;

private _lootSafeZones = [];
{
	_x params ["_area", "_actions"];
    if ( "LootSafe" in _actions ) then { _lootSafeZones pushback _area; };
} forEach BA_zones;

{
    _veh = _x;
    if ( _veh isEqualType objNull ) then {
        private _veh = _x;
        private _keep = ( (alive _x) && (({ isPlayer _x } count (crew _veh)) > 0) );
        if ! ( _keep ) then {
            {
	    		if ( _veh inArea _x ) exitWith { _keep = true; };
			} forEach _lootSafeZones;
        };
        if ( _keep ) then {
        	_enemies deleteAt _forEachIndex;
            _veh call vehicleWatch_fnc_monitor;
        };
    };
} forEach _enemies;

[_enemies] call common_fnc_deleteObjects;