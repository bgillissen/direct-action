/*
@filename: feats\ia\placeSniper.sqf
Credit:
	Quiksilver
Author:
	Ben
Description:
	run on server,
	spawn sniper patrols inside the given coordonate 
*/

#include "_debug.hpp"

params ["_coord", "_size", "_amount", "_skill", ["_ctxt", "unknow"]];

if ( _amount <= 0 ) exitWith { [] };

#ifdef DEBUG
private _debug = format["%1 | placing %2 sniper groups", _ctxt, _amount];
debug(LL_DEBUG, _debug);
#endif

private _groups = [];

private _baseCfg = ["configFile", "CfgGroups"];

for "_x" from 1 to _amount do {
	(["sGroups"] call ia_fnc_randomSide) params ["_side", "_pool"];
	if ( isNil "_side" ) exitWith { [grpNull] };
	private _sniperGroup = (_baseCfg + (selectRandom _pool));
	private _cfgPath = [_sniperGroup] call BIS_fnc_configPath;
	if ( isClass(_cfgPath) ) then {
		private _randomPos = [_coord, _size, 100, 10] call BIS_fnc_findOverwatch;
		private _group = [_randomPos, _side, _cfgPath] call BIS_fnc_spawnGroup;
        [_group, true] call dynSim_fnc_set;
        _group setGroupIdGlobal [([(format["%1 - sniper", _ctxt]), allGroups, {groupId _this}] call common_fnc_getUniqueName)];
		_group setBehaviour "COMBAT";
		_group setCombatMode "RED";
		[(units _group), _skill] call common_fnc_setSkill;
		[(units _group), false] call curator_fnc_addEditable;
		_groups pushback _group;
	};
};

_groups