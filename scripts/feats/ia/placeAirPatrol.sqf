/*
@filename: feats\ia\placeAirPatrol.sqf
Credit:
	Quiksilver
Author:
	Ben
Description:
	run on server,
	it spawn a helicopter with crew inside the given coordonate 
*/

#include "_debug.hpp"

params ["_coord", "_size", "_amount", "_skill", "_patrolSize", "_altitude", ["_ctxt", "unknow"]];

if ( _amount <= 0 ) exitWith { [] };

#ifdef DEBUG
private _debug = format["%1 | placing %2 air patrols", _ctxt, _amount];
debug(LL_DEBUG, _debug);
#endif

private _doLock = ( (["ia", "lockVeh"] call core_fnc_getSetting) == 1 );

private _groups = [];

for "_x" from 1 to _amount do {
	(["aPatrol"] call ia_fnc_randomSide) params ["_side", "_pool", "_key"];
	if ( isNil "_side" ) exitWith { 
    	#ifdef DEBUG
		private _debug = format["%1 | air patrol spawn failed, no side could be determined for aPatrol spawn pool, probably empty", _ctxt];
		debug(LL_ERR, _debug);
		#endif
    	[grpNull] 
	};
	private _group = createGroup [_side, true];	
	_group setGroupIdGlobal [([(format["%1 - airPatrol", _ctxt]), allGroups, {groupId _this}] call common_fnc_getUniqueName)];    
	private _randomPos = [[[_coord, _size]], []] call BIS_fnc_randomPos;
	private _veh = createVehicle [(selectRandom _pool), [_randomPos select 0, _randomPos select 1, 2000], [], 0, "FLY"];
    createVehicleCrew _veh;   
    if ( isClass(configFile >> "cfgVehicles" >> (typeOf _veh) >> "Components" >> "SensorsManagerComponent") ) then {
		_veh setVehicleReportRemoteTargets true;
    	_veh setVehicleReceiveRemoteTargets true; 
	};
	_veh engineOn true;
	if ( _doLock ) then { _veh lock 3; };
    (crew _veh) joinSilent _group;
	_group setCombatMode "RED";
	_veh flyInHeight _altitude;
	[_group, _coord, _patrolSize] call common_fnc_airPatrol;
    [[_veh], true] call curator_fnc_addEditable;
    _groups pushback _veh;
    _groups pushback _group;
};

_groups