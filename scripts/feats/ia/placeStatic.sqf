/*
@filename: feats\ia\placeStatic.sqf
Credit:
	Quiksilver
Author:
	Ben
Description:
	this script is executed on server side,
	it spawn static weapons and a gunner for each inside the given coordonate
*/

#include "_debug.hpp"

params ["_coord", "_size", "_amount", "_skill", ["_ctxt", "unknown"]];

if ( _amount <= 0 ) exitWith{[]};

#ifdef DEBUG
private _debug = format["%1 | spawning %2 statics", _ctxt, _amount];
debug(LL_DEBUG, _debug);
#endif

private _groups = [];

for "_x" from 1 to _amount do {
	(["static"] call ia_fnc_randomSide) params ["_side", "_gunPool", "_key"];
    if ( isNil "_side" ) exitWith { 
		#ifdef DEBUG
		private _debug = format["%1 | static spawn failed, no side could be determined for static spawn pool, probably empty", _ctxt];
		debug(LL_ERR, _debug);
		#endif
		[] 
	};
    private _randomPos = [[[_coord, (_size * 0.8)]], ["water"]] call BIS_fnc_randomPos;
	_randomPos = [_randomPos, (_size * 0.5), 0, 5] call BIS_fnc_findOverwatch;
	private _veh = (selectRandom _gunPool) createVehicle _randomPos;
	_veh setDir random 360;
	_veh setVectorUp [0,0,1];
	if ( (["ia", "lockStatic"] call core_fnc_getSetting) == 1 ) then { 
    	_veh lock 3;
	};
	if (random 100 >= (["ia", "crewStayInProb"] call core_fnc_getSetting) ) then {
		_veh allowCrewInImmobile true;
	};
	[[_veh], false] call curator_fnc_addEditable;
	_groups pushback _veh;
	
	private _group = createGroup [_side, true];
    _group setGroupIdGlobal [([(format["%1 - static", _ctxt]), allGroups, {groupId _this}] call common_fnc_getUniqueName)];
	(selectRandom (S_garrison select _key)) createUnit [_randomPos, _group];
	((units _group) select 0) assignAsGunner _veh;
	((units _group) select 0) moveInGunner _veh;	
	_group setBehaviour "COMBAT";
	_group setCombatMode "RED";
	[_group, true] call dynSim_fnc_set;
	[(units _group), false] call curator_fnc_addEditable;	
	_groups pushback _group;	
};

_groups