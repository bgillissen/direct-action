/*
@filename: feats\ia\forcedGarrison.sqf
Credit:
	Quiksilver
Author:
	Ben
Description:
	run on server,
	spawn garrisoned units in the given building 
*/

params ["_building", "_skill", ["_ctxt", "unknow"]];

#include "forcedGarrison.hpp"

private _positions = (GARRISON_BUILDINGS select ((GARRISON_BUILDINGS find (typeOf _building)) + 1));

if ( isNil "_positions" ) exitWith {
	#ifdef DEBUG
	private _debug = format["%1 | forced garrison failed, no position defined for building type %2", _ctxt, typeOf _building];
	debug(LL_ERR, _debug);
	#endif	
	grpNull
};

(["garrison"] call ia_fnc_randomSide) params ["_side", "_pool"]; 

if ( isNil "_side" ) exitWith { 
	#ifdef DEBUG
	private _debug = format["%1 | forced garrison failed, no side could be determined for garrison spawn pool, probably empty", _ctxt];
	debug(LL_ERR, _debug);
	#endif
	grpNull 
};

#ifdef DEBUG
private _debug = format["%1 | spawning %3 forced garrison units in %2", _ctxt, _building, count(_positions) ];
debug(LL_DEBUG, _debug);
#endif

private _group = createGroup [_side, true];
_group setGroupIdGlobal [([(format["%1 - forcedGarrison", _ctxt]), allGroups, {groupId _this}] call common_fnc_getUniqueName)];
{
	_x params ["_p0", "_p1", "_p2", "_p3"];
	private _pos =  [_building, _p1, _p0 + direction _building] call BIS_fnc_relPos;
	_pos = [_pos select 0, _pos select 1, (((getPosASL _building) select 2) - _p2)];
	UNIT_TRICK = nil;
	(selectRandom _pool) createUnit [_pos, _group, "UNIT_TRICK = this"];
	waitUntil { !isNil "UNIT_TRICK" };
	private _unit = UNIT_TRICK;
	doStop _unit;
	commandStop _unit;
	_unit setPosASL _pos;
	_unit setUnitPos "UP";
	_unit doWatch ([_unit, 1000, direction _building + _p3] call BIS_fnc_relPos);
	_unit setDir direction _building + _p3;
    _unit setVariable["NOAI", true, true];
} count _positions;

_group setBehaviour "COMBAT";
_group setCombatMode "RED";
[_group, true] call dynSim_fnc_set;
[(units _group), _skill] call common_fnc_setSkill;

[(units _group), false] call curator_fnc_addEditable;

_group