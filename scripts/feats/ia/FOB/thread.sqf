/*
@filename: feats\ia\FOB\thread.sqf
Author:
	Ben
Description:
	this run on server,
	it start a FOB mission, and wait for it to complete 
*/

#include "_debug.hpp"

params ["_fob", "_type", "_aoZone"];

private _coord = getMarkerPos _fob;
private _depotCoord = getMarkerPos "FOB_depot";

if ( _depotCoord isEqualTo [0,0,0] ) exitWith {
    #ifdef DEBUG
	debug(LL_WARN, "no FOB_depot marker available for the active base, abording");
    #endif
};

private _vMarker1 = format["%1_veh1", _fob];
private _veh1 = (selectRandom BV_carArmed) createVehicle (getMarkerPos _vMarker1);
_veh1 setDir (markerDir _vMarker1);
([_veh1] + VC_carArmed) call common_fnc_setCargo;
[[_veh1], false] call curator_fnc_addEditable;

private _vMarker2 = format["%1_veh2", _fob];
private _veh2 = (selectRandom BV_carArmed) createVehicle (getMarkerPos _vMarker2);
_veh2 setDir (markerDir _vMarker2);
([_veh2] + VC_carArmed) call common_fnc_setCargo;
[[_veh2], false] call curator_fnc_addEditable;

private _pad = "Land_HelipadSquare_F" createVehicle _coord;
_pad setDir (markerDir _fob);

private _truck = (selectRandom (missionNamespace getVariable format["BV_%1", _type])) createVehicle _depotCoord;
_truck setDir (markerDir "FOB_depot");
([_truck] + (missionNamespace getVariable format["VC_%1", _type])) call common_fnc_setCargo;
[[_truck], false] call curator_fnc_addEditable;

private _maxTime = ["ia", "fob", "maxTime"] call core_fnc_getSetting;

//marker, task
private _marker = ["ia", "fob", "marker"] call core_fnc_getSetting;
private _color = call common_fnc_getMarkerColor;
createMarker [_marker, _coord];
_marker setMarkerColor _color;
_marker setMarkerShape "ICON";
_marker setMarkerType "mil_end";
_marker setMarkerText "FOB";
private _title = ["ia", "fob", "title"] call core_fnc_getSetting;
private _desc = ["ia", "fob", "desc"] call core_fnc_getSetting;
private "_taskType";
switch ( _type ) do {
    case "ammo": { _taskType = "rearm"; };
    case "repair": { _taskType = "repair"; };
    case "landMedic": { _taskType = "heal"; };
    case "fuel": { _taskType = "refuel"; };
};
private _taskId = (["FOB", _taskType, _title, format[_desc, (_maxTime / 60)], true] call tasks_fnc_serverAdd);
call tasks_fnc_serverExport;
_title = nil;
_taskType = nil;

private _endTime = time + _maxTime;
private _skill = ["ia", "fob", "skill"] call core_fnc_getSetting;
private _maxGrp = ["ia", "fob", "maxGroup"] call core_fnc_getSetting;
private _maxVeh  = ["ia", "fob", "maxVehicle"] call core_fnc_getSetting;
private _spawnDelay = ["ia", "fob", "waveDelay"] call core_fnc_getSetting;
private _updateDelay = ["ia", "fob", "updateDelay"] call core_fnc_getSetting;
private _lockVeh = ["ia", "lockVeh"] call core_fnc_getSetting;
private _loopDelay = ["ia", "loopDelay"] call core_fnc_getSetting;
private _minDistToDepot = ["ia", "fob", "minDistToDepot"] call core_fnc_getSetting;
private _baseCfg = ["configFile", "CfgGroups"];
private _nextUpdate = time + _updateDelay;
private _nextSpawn = 0;
private _lastSpawnTime = 0;
private _lastSpawnCoord = [0,0,0];

private _groups = [];
private _vehs = [];

#ifdef DEBUG
private _debug = format["active fob thread is running with %1s delay between waves", _spawnDelay];
debug(LL_INFO, _debug);
#endif

while { true } do {
	if ( (count _groups) <= _maxGrp ) then {
		if ( time > _nextSpawn ) then {
			if ( (_truck distance _depotCoord > _minDistToDepot)  && (_truck distance _coord > 100) ) then {
				private _distance = 200 + (random 100);
				private _spawnCoord = [_truck, _distance, getDir (_truck)] call BIS_fnc_relPos;
				private _spawnVeh = false;
				if !( _lastSpawnCoord isEqualTo [0,0,0] ) then {
					//check if they moved a little since the last spawn, if not mutch let's punish them
					_spawnVeh = ( (_truck distance _lastSpawnCoord < 400) && (count _vehs <= _maxVeh) );
				};
				private ["_group", "_veh"];
				if ( !_spawnVeh ) then {
                    #ifdef DEBUG
					debug(LL_DEBUG, "spawning an infantry wave");
    				#endif
                    (["pGroups"] call ia_fnc_randomSide) params ["_side", "_pool"];
                    private _infGroup = (_baseCfg + (selectRandom _pool));
					private _cfgPath = [_infGroup] call BIS_fnc_configPath;
					private _class = selectRandom (_cfgPath call Bis_fnc_getCfgSubClasses);
					_group = [_spawnCoord, _side, (_cfgPath >> _class)] call BIS_fnc_spawnGroup;
                    _group setGroupIdGlobal [(["FOB - infantry", allGroups] call common_fnc_getUniqueName)];
                    [(units _group), true] call curator_fnc_addEditable;
				} else {
                    #ifdef DEBUG
					debug(LL_DEBUG, "spawning a vehicle wave");
    				#endif
					(["apc"] call ia_fnc_randomSide) params ["_side", "_pool", "_key"];
					_group = createGroup [_side, true];
                    _group setGroupIdGlobal [(["FOB - vehicle", allGroups] call common_fnc_getUniqueName)];
					_veh = (selectRandom _pool) createVehicle _spawnCoord;
					(selectRandom IA_crew select _key) createUnit [_spawnCoord, _group];
					(selectRandom IA_crew select _key) createUnit [_spawnCoord, _group];
					((units _group) select 0) assignAsDriver _veh;
					((units _group) select 0) moveInDriver _veh;
					((units _group) select 1) assignAsGunner _veh;
					((units _group) select 1) moveInGunner _veh;
					if ( _lockVeh ) then { _veh lock 3; };
					_vehs pushback _veh;
                    [(units _group), true] call curator_fnc_addEditable;
                    [_veh, true] call curator_fnc_addEditable;
				};
				//set skill
				[(units _group), _skill] call common_fnc_setSkill;                
                //make them attack the truck position
				[_group, (getPos _truck)] call BIS_fnc_taskAttack;
				_groups append [_group];
				//remove empty groups from the stack, cleanup will remove the groups
				{
					if (({alive _x} count units _x) == 0) then { _groups deleteAt _forEachIndex; };
				} forEach _groups;
                //remove dead / disabled vehs from the stack
                {
					if ( !(alive _x) || !(canFire _x) ) then { _vehs deleteAt _forEachIndex; };
				} forEach _vehs;
				//update lastspawn time and coord, next time
				_lastSpawn = time;
				_lastSpawnCoord = _spawnCoord;
				_nextSpawn = time + _spawnDelay;
			};
		};
	};
	sleep _loopDelay;   
	if ( (alive _truck) && ((_truck distance _coord) <= 10) ) exitWith {
		[_veh1, 30, "carArmed", _vMarker1, []] call vehicleRespawn_fnc_monitor;
		[_veh2, 30, "carArmed", _vMarker2, []] call vehicleRespawn_fnc_monitor;
		private _reward = call ia_fnc_giveReward; 
		private _hint = ["ia", "fob", "successHint"] call core_fnc_getSetting;
		format[_hint, _reward] call global_fnc_hint;
        [_taskId, "Succeeded", false, true] call tasks_fnc_serverSetState;
		[false, _coord, _groups, _vehs, _pad, _truck, _marker] spawn FOB_fnc_cleanup;
		private _marker = format["%1_success", _fob];
		createMarker [_marker, _coord];
		_marker setMarkerColor _color;
		_marker setMarkerShape "ICON";
		_marker setMarkerType "mil_flag";
		(FOB_deployed select AO_key) pushback _fob;
		private _done = count (FOB_deployed select AO_key); 
		private _avail = count (getArray(AO_zone >> "fobs"));
		if ( _done == _avail ) then {
			sleep 5;
			_hint = ["ia", "fob", "zoneHint"] call core_fnc_getSetting;
			format[_hint] call global_fnc_hint;
		};		
	};
	if ( !alive _truck || (time > _endTime) ) exitWith {
        [_taskId, "Failed", false, true] call tasks_fnc_serverSetState;
        if ( time > _endTime ) then { [_taskId, true] call tasks_fnc_serverRemove; };
		[false, _coord, _groups, _vehs, _pad, _truck, _marker, _veh1, _veh2] spawn FOB_fnc_cleanup;
	};
	if ( AO_zone != _aoZone ) exitWith {
        [_taskId, "Canceled", false, true] call tasks_fnc_serverSetState;
		[false, _coord, _groups, _vehs, _pad, _truck, _marker, _veh1, _veh2] spawn FOB_fnc_cleanup;
	};
	if ( FOB_stop || zeusMission ) exitWith {
        [_taskId, "Canceled", false, false] call tasks_fnc_serverSetState;
		[true, _coord, _groups, _vehs, _pad, _truck, _marker, _veh1, _veh2] spawn FOB_fnc_cleanup;
	};
	if ( time >= _nextUpdate ) then {
        _nextUpdate = time + _updateDelay;
        [_taskId, format[_desc, floor((_endTime - time) / 60)]] call tasks_fnc_serverSetDesc;
    };
};