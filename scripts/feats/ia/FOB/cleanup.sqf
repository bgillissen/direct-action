/*
@filename: feats\ia\FOB\cleanup.sqf
Author:
	Ben
Description:
	this run on server,
	it cleanup an FOB mission 
*/

params ["_force", "_coord", "_groups", "_vehs", "_pad", "_truck", "_marker", "_veh1", "_veh2"];

deleteMarker _marker;
deleteVehicle _pad;

if ( ({ isPlayer _x } count (crew _truck)) > 0 ) then { 
	_truck call vehicleWatch_fnc_monitor;
} else {
	deleteVehicle _truck;
};

if ( !_force ) then {
	private _delay = ["ia", "checkDelay"] call core_fnc_getSetting;
	private _dist = ["ia", "deleteDistance"] call core_fnc_getSetting;
	waitUntil {
		sleep _delay;
		( ({((_x distance _coord) < _dist)} count allPlayers) == 0 )
	};
};

{
	{ deleteVehicle _x; } count (units _x);
	_x remoteExec["cleanup_fnc_delGroup", (groupOwner _x), false];
} forEach _groups;

if ( (count _this) > 7 ) then {
  	 _vehs pushback _veh1;
	 _vehs pushback _veh2;
};

private _lootSafeZones = [];
{
	_x params ["_area", "_actions"];
    if ( "LootSafe" in _actions ) then { _lootSafeZones pushback _area; };
} forEach BA_zones;

{
    private _veh =_x;
    private _keep = ( (alive _veh) && (({ isPlayer _x } count (crew _veh)) > 0) );
    if !(  _keep ) then {
    	{
    		if ( _veh inArea _x ) exitWith { _keep = true; };
		} forEach _lootSafeZones;
	};
    if ( _keep ) then { 
    	_veh call vehicleWatch_fnc_monitor;
	} else {
    	{ deleteVehicle _x; } count (crew _x);
		deleteVehicle _x;    
	};
} forEach _vehs;