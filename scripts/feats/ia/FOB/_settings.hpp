class fob {
	marker = "fobIcon";
	pool[]={"ammo", "repair", "landMedic", "fuel"};
	maxTime = 7200;
	cooldown = 900;
	updateDelay = 300;
	maxGroup = 5;
	maxVehicle = 2;
	title = "Forward Operation Base";
	desc = "FOB created<br/><br/>We have created a Forward Operating Base for you to use.<br/>You got ~ %1 minutes left to bring the supplies there.";
	successHint = "<t align='center'><t size='2.2'>FOB Mission</t><br/><t size='1.5' color='#FFCF11'>SUCCESS</t><br/>____________________<br/>Good job getting the supplies to the FOB. You have been rewarded with<br/>%1<br/>This FOB is now deployed.</t>";
	zoneHint = "<t align='center'><t size='2.2'>FOB Mission</t><br/><t size='1.5' color='#FFCF11'>CONGRATULATION</t><br/>____________________<br/>It's amazing !, you have deployed all the FOB is this region.<br/>%1</t>";
	waveDelay = 300;
	skill = 2;
	minDistToDepot = 500;
};
