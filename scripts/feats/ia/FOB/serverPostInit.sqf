/*
@filename: feats\ia\FOB\serverPostInit.sqf
Author:
	Ben
Description:
	this run on server,
	it keeps track of the active FOB thread, and spawn a new one when needed 
*/

#include "_debug.hpp"

if ( (["FOBMission"] call core_fnc_getParam) == 0 ) exitWith {
	#ifdef DEBUG
    debug(LL_INFO, "disabled by mission parameter");
    #endif    
};

#define EMPTYARRAY []

if ( isNil "AO_zones" ) then {
	AO_zones = "true" configClasses (missionConfigFile >> "settings" >> "maps" >> toUpper(worldName) >> "zones");
}; 

FOB_stop = false;
FOB_deployed = [];
{
	FOB_deployed set [_forEachIndex, EMPTYARRAY];
} forEach AO_zones;

FOB_main = scriptNull;

private _checkDelay = ["ia", "checkDelay"] call core_fnc_getSetting;
private _cooldown = ["ia", "fob", "cooldown"] call core_fnc_getSetting;
private _markers = [];
private _types = [];

#ifdef DEBUG
private _debug = format["thread has started with %1s delay, %2s cooldown", _checkDelay, _cooldown];
debug(LL_INFO, _debug);
#endif    

while { true } do {
	[false, "FOB_stop"] call zeusMission_fnc_checkAndWait;
	if ( FOB_stop ) exitWith {};
	
	if ( (["AO"] call core_fnc_getParam) == 0 ) then {
		AO_zone = selectRandom AO_zones;
		AO_key = AO_zones find AO_zone;
	} else {
		#ifdef DEBUG
    	debug(LL_DEBUG, "waiting for an AO zone with FOBs");
    	#endif    
		waitUntil {
			sleep _checkDelay;
			private _out = false;
			if !( isNil "AO_zone") then {
				_out = ( (count getArray(AO_zone >> "fobs")) != 0 );
			};
			( _out || FOB_stop )
		};
		if ( FOB_stop ) exitWith {};
		AO_key = AO_zones find AO_zone;
	};
	
	if ( count _markers == 0 ) then {
		_markers = getArray(AO_zone >> "fobs");
        #ifdef DEBUG
		private _debug = format["%1 marker(s) available in active AO zone", (count _markers)];
		debug(LL_DEBUG, _debug);
		#endif 
	};
	if ( count _types == 0 ) then {
		_types = ["ia", "fob", "pool"] call core_fnc_getSetting;
	};
	if ( count _markers > 0 ) then {
		private _pool = [];
		{
			if !( _x in (FOB_deployed select AO_key) ) then { 
				_pool pushback _x; 
			};
		} forEach _markers;
		#ifdef DEBUG
		private _debug = format["%1 marker(s) in active AO zone pool", (count _pool)];
		debug(LL_DEBUG, _debug);
		#endif
		if ( count _pool > 0 ) then {
			private _marker =  selectRandom _pool;
			_markers = _markers - [_marker];
	
			private _type = selectRandom _types;
			_types = _types -[_type];
			FOB_main = [_marker, _type, AO_zone] spawn FOB_fnc_thread;
			waitUntil {
				sleep _checkDelay;
				scriptDone FOB_main
			};
            #ifdef DEBUG
			private _debug = format["'%1' is done", _marker];
			debug(LL_DEBUG, _debug);
			#endif
		};
	};
	[_cooldown, _checkDelay, "FOB_stop"] call common_fnc_smartSleep;
	if ( FOB_stop ) exitWith {};
};

nil