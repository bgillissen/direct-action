/*
@filename: feats\ia\placeSeaPatrol.sqf
Author:
	Ben
Description:
	run on server,
	spawn divers inside the given coordonate
*/

#include "_debug.hpp"

params ["_coord", "_size", "_amount", "_skill", "_patrolSize", ["_ctxt", "unknown"]];

private _groups = [];

if ( _amount <= 0 ) exitWith { _groups };

#ifdef DEBUG
private _debug = format["%1 | spawning %2 diver group", _ctxt, _amount];
debug(LL_DEBUG, _debug);
#endif

private _pool = [];
{
    switch (_x) do {
        case east : { _pool pushBack (configfile >> "CfgGroups" >> "East" >> "OPF_F" >> "SpecOps" >> "OI_diverTeam"); };
        case west : { _pool pushBack (configfile >> "CfgGroups" >> "West" >> "BLU_F" >> "SpecOps" >> "BUS_DiverTeam"); };
        case independent : { _pool pushBack (configfile >> "CfgGroups" >> "Indep" >> "IND_F" >> "SpecOps" >> "HAF_DiverTeam"); };
    };
} forEach ENEMIES;

for "_x" from 1 to _amount do {
	private _randomPos = ([[[_coord, _size]], ["ground"]] call BIS_fnc_randomPos);
    private _side = selectRandom ENEMIES;
    private _group = ([_randomPos, _side, (selectRandom _pool)] call BIS_fnc_spawnGroup);
    [_group, true] call dynSim_fnc_set;
	_group setGroupIdGlobal [([(format["%1 - Diver", _ctxt]), allGroups, {groupId _this}] call common_fnc_getUniqueName)];
	[_group, _randomPos, _patrolSize] call common_fnc_seaPatrol;
    [(units _group), _skill] call common_fnc_setSkill;
    [(units _group), false] call curator_fnc_addEditable;
    _groups pushback _group;
};

_groups