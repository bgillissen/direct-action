/*
@filename: feats\ia\placeCivPatrol.sqf
Credit:
	Quiksilver
Author:
	Ben
Description:
	this script is executed on server side,
	it spawn Infantry Patrol inside the given coordonate 
*/

#include "_debug.hpp"

params ["_coord", "_size", "_amount", "_patrolSize", ["_ctxt", "unknow"]];

if ( _amount <= 0 ) exitWith { [] };

#ifdef DEBUG
private _debug = format["%1 | placing %2 civilian patrols", _ctxt, _amount];
debug(LL_DEBUG, _debug);
#endif

private _groups = [];

for "_x" from 1 to _amount do {
	
    private _group = createGroup [civilian, true];
    _group setGroupIdGlobal [([(format["%1 - civPatrol", _ctxt]), allGroups, {groupId _this}] call common_fnc_getUniqueName)];
	private _randomPos = [[[_coord, (_size * 0.8)]], ["water"]] call BIS_fnc_randomPos;
	(selectRandom (S_civ select 0)) createUnit [_randomPos, _group];
	[_group, _randomPos, _patrolSize, [["water"]]] call BIS_fnc_taskPatrol;
	[(units _group), false] call curator_fnc_addEditable;
	[_group, true] call dynSim_fnc_set;
	_groups pushback _group;
};

_groups