/*
@filename: feats\ia\placeVehicle.sqf
Credit:
	Quiksilver
Author:
	Ben
Description:
	run server side,
	spawn vehicles with crew inside the given coordonate using the given pool
*/

#include "_debug.hpp"

params ["_coord", "_size", "_amount", "_skill", "_patrolSize", "_poolName", "_commander", ["_ctxt", "unknown"]];

private _groups = [];

if ( _amount <= 0 ) exitWith { _groups };

#ifdef DEBUG
private _debug = format["%1 | spawning %2 vehicle from pool %3", _ctxt, _amount, _poolName];
debug(LL_DEBUG, _debug);
#endif


for "_x" from 1 to _amount do {
	([_poolName] call ia_fnc_randomSide) params ["_side", "_vehPool", "_key"];
	if ( !isNil "_vehPool" ) then {
		private _randomPos = [[[_coord, (_size * 0.8)]], ["water"]] call BIS_fnc_randomPos;
		private _veh = (selectRandom _vehPool) createVehicle _randomPos;
        if ( isClass(configFile >> "cfgVehicles" >> (typeOf _veh) >> "Components" >> "SensorsManagerComponent") ) then {
			_veh setVehicleReportRemoteTargets true;
    		_veh setVehicleReceiveRemoteTargets true; 
		};
		if ( !isNil "_veh" ) then {
			if ( (["ia", "lockVeh"] call core_fnc_getSetting) == 1 ) then { 
				_veh lock 3; 
			};
			if (random 100 >= (["ia", "crewStayInProb"] call core_fnc_getSetting) ) then {
				_veh allowCrewInImmobile true;
			};
			[[_veh], false] call curator_fnc_addEditable;
			_groups pushback _veh;
	
			private _group = createGroup [_side, true];
            _group setGroupIdGlobal [([(format["%1 - %2", _ctxt, _poolName]), allGroups, {groupId _this}] call common_fnc_getUniqueName)];
			(selectRandom (S_crew select _key)) createUnit [_randomPos, _group];
			(selectRandom (S_crew select _key)) createUnit [_randomPos, _group];
			((units _group) select 0) assignAsDriver _veh;
			((units _group) select 0) moveInDriver _veh;
			((units _group) select 1) assignAsGunner _veh;
			((units _group) select 1) moveInGunner _veh;
			if ( _commander ) then {
				(selectRandom (S_crew select _key)) createUnit [_randomPos, _group];
				((units _group) select 2) assignAsCommander _veh;
				((units _group) select 2) moveInCommander _veh;
			};
			[_group, _randomPos, _patrolSize, [["water"]]] call BIS_fnc_taskPatrol;
			[_group, true] call dynSim_fnc_set;
			[(units _group), _skill] call common_fnc_setSkill;
			[(units _group), false] call curator_fnc_addEditable;	
			_groups pushback _group;
		};
	#ifdef DEBUG
    } else {
		private _debug = format["%1 | spawning vehicle failed, valid pool could not be determined for %2, probably empty", _ctxt, _poolName];
		debug(LL_ERR, _debug); 
    #endif
	};
};

_groups