/*
@filename: feats\ia\SIDE\sinkBoat.sqf
Credits : 
	Magirot
Author:
	Ben
Description:
	run on where given boat is local,
	sink a trawler ship
*/

#include "_debug.hpp"

params ["_ship", ["_sinkSide", "RANDOM"], ["_sinkTime", 120], ["_smoke", true]];

if ( isNull _ship ) exitWith {
    #ifdef DEBUG
    debug(LL_ERR, "sinkBoat: given boat is NULL, abording");
    #endif
};

// Although it would be nice to implement damage to multiple locations, not today
if ( _ship getVariable ["sinkBoat", false] ) exitWith {
    #ifdef DEBUG
    debug(LL_WARN, "sinkBoat: boat is already sinking, abording");
    #endif
};

if !( local _ship ) exitWith {
    _this remoteExec ["side_fnc_sinkBoat", _ship, false];
};

_ship setVariable ["sinkBoat", true, true];

// If _sinktime is 0, randomise the time between 1-3 minutes.
if ( _sinktime <= 0 ) then {
	#ifdef DEBUG
    debug(LL_WARN, "sinkBoat: invalid sinkTime given, randomising between 1-3 minutes");
    #endif
	_sinktime = 60 + random 120 
};

// Convert the string in _sinkside to upper case
_sinkSide = toUpper (_sinkSide);

if !( _sinkSide in ["RANDOM", "LEFT", "RIGHT", "LEFT FRONT", "RIGHT FRONT", "LEFT BACK", "RIGHT BACK"] ) then {
    #ifdef DEBUG
    debug(LL_WARN, "sinkBoat: invalid sinkSide given, using random");
    #endif
    _sinkSide = "RANDOM";
};

// If _sinkside is "RANDOM", choose a random side from the array
if (_sinkSide == "RANDOM") then { _sinkside = selectRandom ["LEFT", "RIGHT", "LEFT FRONT", "RIGHT FRONT", "LEFT BACK", "RIGHT BACK"]; };

#ifdef DEBUG
private _debug = format["sinkBoat: sinking boat '%1', side: %2, _time: %3, smoke: %4", _ship, _sinkSide, _sinkTime, _smoke];
debug(LL_DEBUG, _debug);
#endif

// Get the default CenterOfMass of the trawler (which is [-0.17766, -1.72015, -8.49908])
(getCenterOfMass _ship) params ["_x", "_y", "_z"];

// Z values for sinking are the same for all sides
private ["_x1", "_x2", "_x3", "_y1", "_y2", "_y3"];
private _z1 = _z + 1.0;
private _z2 = _z + 4.0;
private _z3 = _z + 8.5;

private _smokeloc = [0, 0, -6];

// Declare the variables for setCenterOfMass
switch ( _sinkSide ) do {
	case "LEFT": {  _x1 = _x - 1.0; _y1 = _y; 
					_x2 = _x - 1.7; _y2 = _y - 2.5;   
					_x3 = _x - 1.7; _y3 = _y;        
					_smokeloc = [-3, 0, -6]; };
	case "RIGHT": { _x1 = _x + 1.0; _y1 = _y;
					_x2 = _x + 1.7; _y2 = _y - 2.5;  
					_x3 = _x + 1.7; _y3 = _y;        
					_smokeloc = [3, 0, -6]; };
	case "LEFT FRONT": {_x1 = _x - 0.6; _y1 = _y + 3.0; 
						_x2 = _x - 1.5; _y2 = _y + 5.0;   
						_x3 = _x - 1.7; _y3 = _y;        
						_smokeloc = [-3, 8, -6]; };
	case "RIGHT FRONT": {	_x1 = _x + 0.6; _y1 = _y + 3.0; 
							_x2 = _x + 1.5; _y2 = _y + 5.0;   
							_x3 = _x + 1.7; _y3 = _y;        
							_smokeloc = [3, 8, -6]; };
	case "LEFT BACK": {	_x1 = _x - 0.4; _y1 = _y - 2.5; 
						_x2 = _x - 1.5; _y2 = _y - 5.0;   
						_x3 = _x - 1.7; _y3 = _y;        
						_smokeloc = [-3, -8, -6]; };
	case "RIGHT BACK": {	_x1 = _x + 0.4; _y1 = _y - 2.5; 
							_x2 = _x + 1.5; _y2 = _y - 5.0;   
							_x3 = _x + 1.7; _y3 = _y;        
							_smokeloc = [3, -8, -6]; };
};

// Create the smoke emitter
private _smoke_e = objNull;
if ( _smoke ) then {
	_smoke_e = "#particlesource" createVehicle (getPosATL _ship);
	_smoke_e setParticleClass "BigDestructionSmoke";
	_smoke_e attachTo [_ship, _smokeloc];
};

sleep 2;

if ( isNull _ship ) exitWith {};

// Slight tilt
_ship setCenterOfMass [[_x1, _y1, _z1], _sinktime * 0.33];
_ship setMass         [ 1500000,        _sinktime * 0.33]; // Normal mass of the trawler is around 1 270 000.

sleep (_sinkTime * 0.33 + 0.5);

if ( isNull _ship ) exitWith {};

// Water reaches the deck, fire extinguished, tilt and mass increasing
if ( _smoke ) then {
     // Delete the smoke emitter after a few seconds. 
	_smoke_e spawn {
		sleep 3; 
		deleteVehicle _this;
	}; 
};

_ship setCenterOfMass [[_x2, _y2, _z2], _sinktime * 0.66];
_ship setMass         [ 3300000,        _sinktime * 0.66];

sleep (_sinktime * 0.66 + 0.5);

if ( isNull _ship ) exitWith {};

// Make sure the ship sinks to the bottom instead of hanging halfway through.
_ship setCenterOfMass [[_x3, _y3, _z3], _sinktime];
_ship setMass         [ 7000000,        _sinktime];  // Required mass depends on depth.
