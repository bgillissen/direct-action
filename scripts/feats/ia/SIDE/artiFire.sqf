/*
@filename: feats\ia\SIDE\artiFire.sqf
Credit:
	Quiksilver
Author:
	Ben
Description:
	run on server,
	used by priority side mission when arti type has been picked
	it search for a player in range and not in a safe zone,
	and send him some salve :D 
Params:
	_batteries	ARRAY of objects, list of arti tanks
	_szCoords	ARRAY of ARRAY of SCALAR, [x,y,z] coordonates of the safe zones
Environment:
	missionParams:
		ArtilleryTargetTickWarning
	missionConfig:
		ia >> checkDelay
		ia >> side >> priority >> arti >> firingMsg
		ia >> side >> priority >> arti >> salve
		ia >> side >> priority >> arti >> salveDelay
		ia >> side >> priority >> arti >> radius
Return:
	nothing
*/

#include "_debug.hpp"

params ["_batteries", "_safeZones"];

if ( ({ (alive _x) && (canFire _x) && alive (gunner _x) } count _batteries) isEqualTo 0 ) exitWith { 
    #ifdef DEBUG
    debug(LL_DEBUG, "artiFire : no batteries able to shoot, abording");
    #endif
};  

private _targets = [];
{
    private _trgt = _x;
    private _isValid = !( ((vehicle _trgt) isKindOf "Air") && !(isTouchingGround (vehicle _trgt)) ); //is air but on the ground 
  	if (_isValid ) then { _isValid = ( (side _trgt) isEqualTo PLAYER_SIDE ); }; //is player side
    if (_isValid ) then { _isValid = !(captive _trgt); }; //is not captive
    if (_isValid ) then { _isValid = ( ({( _trgt inArea _x )} count _safeZones) isEqualTo 0 ); }; //is not in a mortarSafe zone
    if (_isValid ) then { _targets pushback _trgt; };
} forEach (allPlayers - (entities "HeadlessClient_F"));

if ( (count _targets) isEqualTo 0 ) exitWith {
    #ifdef DEBUG
    debug(LL_DEBUG, "artiFire : no target found, abording");
    #endif
};

private _batteryData = [];
{
    private _battery = _x;
	private _gunner = (gunner _battery);        
	if ( (alive _battery) && (canFire _battery) && (alive _gunner) ) then {			
		private _weapon = [_gunner] call arti_fnc_getWeapon;
		if !( _weapon isEqualTo "" ) then {
			private _mags = [_battery] call arti_fnc_getMags;
            if ( count _mags > 0 ) then {
                private _muzzles = [_gunner] call arti_fnc_getMuzzles;
                if ( count _muzzles > 0 ) then {
                	(assignedVehicleRole player) params[["_role", ""], ["_turretPath", []]]; 
					_batteryData pushback [_battery, _gunner, _weapon, (_mags select 0), (_muzzles select 0), _turretPath];
				#ifdef DEBUG
				} else {
            		private _debug = format["artiFire : no muzzle found for battery %1", _battery];
       				debug(LL_WARN, _debug);    
				#endif
				};
			#ifdef DEBUG
			} else {
            	private _debug = format["artiFire : no magazine found for battery %1", _battery];
       			debug(LL_WARN, _debug);    
			#endif  
			};
        #ifdef DEBUG
		} else {
			private _debug = format["artiFire : weapon not found for battery %1, gunner: %2, weapon: %3", _battery, _gunner, _weapon];	
           	debug(LL_WARN, _debug);    
		#endif
		};
	#ifdef DEBUG
	} else {
        private _debug = format["artiFire : battery %1 can't fire", _battery];
		debug(LL_DEBUG, _debug);    
	#endif
	};
} forEach _batteries;

if ( (count _batteryData) isEqualTo 0 ) exitWith {
 	#ifdef DEBUG
    debug(LL_ERR, "artiFire : no batteryData could be gathered, abording");
    #endif    
};

private _salve = ["ia", "side", "priority", "arti", "salve"] call core_fnc_getSetting;
private _delay = ["ia", "side", "priority", "arti", "salveDelay"] call core_fnc_getSetting;
private _radius = ["ia", "side", "priority", "arti", "radius"] call core_fnc_getSetting;
private _fired = false;
private _sent = false;
private _target = selectRandom _targets;
(getPos _target) params ["_targetX", "_targetY", "_targetZ"];

#ifdef DEBUG
private _debug = format["artiFire : firing %1 shells from %2 batteries, on %3", _salve, {alive _x && canFire _x && alive (gunner _x)} count _batteries, _target];
debug(LL_DEBUG, _debug);
#endif

#define G 9.81

for "_c" from 1 to _salve do {
    private _toFire = [];
	{
        _x params ["_battery", "_gunner", "_weapon", "_mag", "_muzzle", "_turretPath"];
		_aimX = _targetX - _radius + (2 * random _radius);
		_aimY = _targetY - _radius + (2 * random _radius);
		([(_battery call arti_fnc_getTubePosition), [_aimX, _aimY, _targetZ]] call arti_fnc_getDistance) params ["_dist", "_trgtHeight", "_altDiff"];
		private _modes = [_battery, _turretPath, _mag, _dist, _altDiff] call arti_fnc_getModes;
		if ( {_x select 4} count _modes > 0 ) then {
			private "_fireMode";
		    {
		    	if ( _x select 4 ) exitWith { _fireMode = _x; };
		    } forEach _modes;
			_fireMode params ["_mode", "_charge", "_minRange", "_maxRange", "_inRange"];
        	private _speed = getNumber(configfile >> "CfgMagazines" >> _mag >> "initSpeed") * _charge;
        	if ( (_speed^4 - G * (G * _dist^2 + 2 * _trgtHeight * _speed^2)) >= 0 ) then {
	        	private _angle = atan( (_speed^2 + sqrt(_speed^4 - G * (G * _dist^2 + 2 * _trgtHeight * _speed^2)))/(G * _dist));
	            private _aimZ = _dist * (tan _angle);
	            private _eta = _dist / (_speed * cos(_angle));
	            #ifdef DEBUG
				private _debug = format["artiFire : %1 is aiming at [%1, %2, %3], distance: %4, mode: %5, weapon: %6, angle: %7, eta: %8", _aimX, _aimY, _aimZ, _dist, _mode, _weapon, _angle, _eta];
	       		debug(LL_DEBUG, _debug);    
				#endif 
				//_gunner disableAI "AUTOTARGET";
				_gunner setSkill 1;
	            _battery setWeaponReloadingTime [_gunner, _muzzle, 0];
	            /*
	            if ( isNil "artiFireMarkers" ) then { artiFireMarkers = []; };
	            private _mm = format["artiFireMarker_%1", (count artiFireMarkers)];
	            createMarker [_mm, [_aimX, _aimY, 0]];
				_mm setMarkerShape "ICON"; 
	    		_mm setMarkerColor "ColorRed";
	            _mm setMarkerAlpha 0.4;
	    		_mm setMarkerType "hd_dot";
	            artiFireMarkers pushback _mm;
				*/        
	            _gunner doWatch [_aimX, _aimY, _aimZ];
	            _toFire pushback [_battery, _gunner, _weapon, _mag, _muzzle, _mode];
            #ifdef DEBUG
            } else {
				private _debug = format["artiFire, target is not in range for battery '%1' (%2m)", _battery, _dist];
	    		debug(LL_ERR, _debug);
	    		#endif
            };         
		#ifdef DEBUG
		} else {
            private _debug = format["artiFire : no fire mode found for battery '%1' with weapon '%2' at %3m of distance", _battery, _weapon, _dist];
       		debug(LL_WARN, _debug);    
		#endif                
        };
	} forEach _batteryData;
    //delay to wait for the AIs to aim at their targets.
    if ( _c isEqualTo 1 ) then {
		sleep 20;
	} else {
    	sleep 5;        
	};
    {
        _x params ["_battery", "_gunner", "_weapon", "_mag", "_muzzle", "_mode"];
        _battery fire [_muzzle, _mode, _mag];       
		_fired = true;
    } forEach _toFire;
    if ( _fired && !_sent && ((["ArtiTargetTickWarning"] call core_fnc_getParam) == 1) ) then {
        _sent = true;
        if ( ((["ArtiTargetTickWarning"] call core_fnc_getParam) == 1) ) then {
			private _msgs = ["ia", "side", "priority", "arti", "firingMsg"] call core_fnc_getSetting;
			[1, (selectRandom _msgs), ["HQ", PLAYER_SIDE]] call global_fnc_chat;
			_msgs = nil;
		};
	};
	sleep _delay;
};

{
    _x params ["_battery", "_gunner", "_weapon", "_mag", "_muzzle"];
    _gunner doWatch objNull;
    //_gunner EnableAI "AUTOTARGET";
} forEach _batteryData;