class side {
	switchRank = 4;
	cooldown = 120;
	minDistFromAO = 2000;
	missions[]= {"urban", "hqCoast", "research", "priority", "secure", "intel", "hostage", "ship", "blockade"};
	size = 500;
	circle = "sideCircle";
	label = "sideLabel";
	patrolSkill = 2;
	diverSkill = 3;
	boatSkill = 2;
	sniperSkill = 2;
	garrisonSkill = 3;
	staticSkill = 2;
	aaSkill = 4;
	tankSkill = 2;
	apcSkill = 1;
	carSkill = 3;
	airSkill = 3;
	airAltitude = 150;
	boomDelay = 30;
	plantAnim = "AinvPercMstpSrasWrflDnon_Putdown_AmovPercMstpSrasWrflDnon";
	table = "Land_CampingTable_small_F";
	laptop[]= {"Land_Laptop_F", "Land_Laptop_device_F", "Land_laptop_unfolded_F", "Land_Tablet_02_F"};
	successHint = "<t align='center'><t size='2.2'>Side Mission</t><br/><t size='1.5' color='#08b000'>COMPLETE</t><br/>____________________<br/>Side Mission Complete. Great work! The tactical bacon has blessed you with %1 to help you in the struggle.</t>";
	class hqCoast {
		mapKeywords[] = {"water"};
		title = "Secure Smuggled Explosives";
		briefing = "The enemy has a been smuggling in explosives and storing them in a secured military facility. Head over to the facility secure the shipment and set charges to blow up the installation. CAUTION: The use of rockets and bombs to destroy the building will result in a mission failure because of 'reasons'.";
		planted = "The charges have been planted. You have %1 seconds to clear the blast radius. Stand back well back this will be a big one.";
		action = "<t color='#ffff00'>Plant Charge</t>";
		hqType = "Land_Cargo_HQ_V1_F";
	};
	class ship {
		mapKeywords[] = {"sea"};
		title = "Destroy Ship";
		briefing = "The enemy has a been smuggling in explosives and storing them in a ship anchored near the %1. Head over there, find the ship and plant charges to blow it up. CAUTION: The use of rockets and bombs to destroy the ship will result in a mission failure because of 'reasons'.";
		planted = "The charges have been planted. You have %1 seconds to clear the blast radius. Stand back well back this will be a big one.";
		action = "<t color='#ffff00'>Plant Charge</t>";
	};
	class blockade {
		mapKeywords[] = {"blockade"};
		title = "Stop blockade";
		mineMin = 5;
		mineMid = 7;
		mineMax = 10;
		threshold = 10;
		mineTitle = "Desarm sea-mines";
		mineDesc = "Our intelligence service has been able to stole the sea mines map. Your objective is to desarm them all to allow the shipping traffic to reach the port safely";
		infTitle= "Eliminate hostile forces";
		infDesc = "Eliminate as much enemy as possible, we will consider this task done once less than %1 enemies remain.";
		briefing = "The enemy has a been blockading a harbor, your mission is to stop the blockade by elimating all enemy forces and desarming the mines";
	};
	class hostage {
		mapKeywords[] = {};
		title = "Hostage Rescue";
		briefing = "The enemy have taken hostages, they are keeping them in an outpost.<br/>Your mission is to bring them back at base.<br/>Mission will be considered successfull if at least one of them make it back to base (next to the main flag) alive!.";
		subTitle = "Rescue %1";
		subDesc = "Bring %1 back to base ALIVE!";
		hqType = "Land_Cargo_HQ_V1_F";
		min = 1;
		max = 3;
		hostage = "C_man_pilot_F";
		alarm = "Sound_Alarm2";
	};
	class urban {
		mapKeywords[] = {"urban"};
		title = "Destroy Weapons Shipment";
		briefing = "The enemy have been supplying local insurgents with weapons in populated areas. We have located one of these caches. Move in and destroy the cache. CAUTION: Area is populated with civilians as well as insurgents. Check your fire!";
		planted = "Charges have been set. You have %1 seconds to clear the blast radius.";
		action = "<t color='#ffff00'>Plant Charge</t>";
	};
	class research {
		mapKeywords[] = {};
		title = "Seize Research Data";
		briefing = "The enemy have been developing new military hardware at one of there facilities. We think they are developing some kind of computer programme to desensitise their combatants. We need those data files. Secure the data and then destroy any evidence on site. CAUTION: Failure to retrieve the data files will result in a Mission failure.";
		planted = "Data secured. The charges have been set. You have %1 seconds to clear the blast radius.";
		action = "<t color='#ffff00'>Secure Data</t>";
		hqType = "Land_Research_HQ_F";
	};
	class priority {
		mapKeywords[] = {};
		maxDistFromAO = 4000;
		artiProb = 50;
		infiniteAmmo = 1;
		extraHealth = 0;
		HBarrier = "Land_HBarrierBig_F";
		subTitle = "Disable target %1";
		subDesc = "Make sure the enemy will not be able to use that asset anymore";
		class arti {
			title = "Artillery Battery";
			briefing = "The enemy is setting up an artillery battery to hit you guys damned hard! We've picked up their positions with thermal imaging scans and have marked it on your map.<br/><br/>This is a priority target, boys! They're just setting up now; they'll be firing soon!";
			radius = 25;
			salve = 5;
			salveDelay = 2;
			firingMsg[]={"Thermal scans are picking up those enemy Artillery firing! Heads down!"};
		};
		class aa {
			title = "Anti-Air Battery";
			briefing = "The enemy is setting up an anti-air battery to hit you guys damned hard! We've picked up their positions with thermal imaging scans and have marked it on your map.<br/><br/>This is a priority target, boys!";
			range = 4000;
			minAltitude = 25;
		};
	};
	class secure {
		mapKeywords[] = {};
		radarProb = 50;
		planted = "Data secured. The charges have been set. You have %1 seconds to clear the blast radius.";
		action = "<t color='#ffff00'>Secure Data</t>";
		cargoType = "Land_Cargo_House_V3_F";
		class radar {
			title = "Secure Radar";
			briefing = "The enemy have deployed a small radar dome and a mobile tracking station. This installation has been gathering intelligence on our air movements and coordinating enemy aircraft. Move to the location, secure the data and set charges on the radar dome.";
		};
		class chopper {
			title = "Secure Enemy Chopper";
			briefing = "The enemy has been developing a new attack helicopter. They have deployed a prototype for combat testing. Its still grounded and we have found the location of the hangar. Move in, secure any data and set charges on the helicopter. CAUTION: Blowing up the hangar before locating the data will result in a mission failure.";
		};
	};
	class intel {
		mapKeywords[] = {};
		title = "Secure Intel";
		briefing = "Enemy intelligence is being exchanged at this location. Move in and secure any documentation either from an officer or their vehicles. They will attempt to flee with this information if spotted so stay hidden for as long as possible.";
		vehicleProb = 40;
		triggerSize = 600;
		spotted = "You have been spotted. The officer is attempting to escape with the information! Stop him!";
		fail = "The target has escaped with the intelligence. Mission failed!";
		secured = "Intelligence secured. Great work! We will put this to good use.";
		action = "<t color='#ff0000'>Secure Intel</t>";
	};
};
