
params ["_object", "_caller", "_id"];

private _anim = ["ia", "side", "plantAnim"] call core_fnc_getSetting;
[_caller, _anim, 2] call global_fnc_doAnim;

_object setVariable ["SIDE_activated", true, true];

SIDE_success=true;
publicVariableServer 'SIDE_success'; 

nil remoteExec["side_fnc_delAction", 0, false];