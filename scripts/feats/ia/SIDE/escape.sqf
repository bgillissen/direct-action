
#include "_debug.hpp"

params ["_sidePos", "_size", "_group"];

#ifdef DEBUG
private _debug = format["intel: escape, loop start for group '%1'", _group];
debug(LL_DEBUG, _debug);
#endif

while { ({ alive _x } count (units _group)) > 0 } do {
    if ( ({ local _x } count (units _group)) <= 0 ) exitWith {
        #ifdef DEBUG
		private _debug = format["intel: escape, group '%1' is not local anymore, exiting", _group];
		debug(LL_DEBUG, _debug);
		#endif
        if ( CTXT_SERVER ) then {
            _this remoteExec ["side_fnc_escape", groupOwner _group, false];
		} else {
        	_this remoteExec ["side_fnc_escape", 2, false];
		};
    };
	if ( (count (waypoints _group)) <= 1 ) then {
		private _moveTo = [nil, [[_sidePos, _size], "water"]] call BIS_fnc_randomPos;
		#ifdef DEBUG
	   	private _debug = format["intel: creating waypoint for group '%1' to %2", _group, _moveTo];
		debug(LL_DEBUG, _debug);
		#endif
		private _wp = _group addWaypoint [_moveTo, 0];	
		_wp setWaypointType "MOVE";
		_wp setWaypointBehaviour "CARELESS";
		_wp setWaypointSpeed "FULL";
        _wp setWaypointStatements ["true","deleteWaypoint [group this, currentWaypoint (group this)]"]
	};
    sleep 5;
};

#ifdef DEBUG
private _debug = format["intel: escape, loop end for group '%1'", _group];
debug(LL_DEBUG, _debug);
#endif