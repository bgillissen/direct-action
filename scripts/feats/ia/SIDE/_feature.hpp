class side : feat_base {
	class server : featServer_base {
		class postinit : featEvent_enable {
			thread = 1;
			order = 99;
		};
		class destroy : featEvent_enable {};
	};
	class player : featPlayer_base {
		class postInit : featEvent_enable {};
	};
};
