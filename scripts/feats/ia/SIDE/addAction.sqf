/*
@filename: feats\ia\SIDE\addAction.sqf
Author:
	Ben
Description:
	run on player,
	add the action to finish a side mission on the given object (JIP)
*/

if !( CTXT_PLAYER ) exitWith {};

params ["_obj", "_label", ["_dist", 4]];

private _id = _obj addAction[_label, {_this call SIDE_fnc_doAction}, [], 6, true, true, "", "( !(_target getVariable['SIDE_activated', false]) && (alive _target) && ((vehicle player) isEqualTo player) )", _dist, false];

SIDE_action = [_obj, _id];