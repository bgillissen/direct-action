/*
@filename: feats\ia\SIDE\serverPostInit.sqf
Author:
	Ben
Description:
	run on server,
	it keep track of the active SIDE mission thread, and spawn a new one when needed 
*/

#include "_debug.hpp"

if ( isNil "URBAN_markers" ) then {
	URBAN_markers = [];
	private _ok = true;
	private _x = 1;
	while { _ok } do {
		private _markerName = format["Urban_%1", _x];
		if !( (getMarkerPos _markerName) isEqualTo [0,0,0] ) then {
			_markerName setMarkerAlpha 0;
			URBAN_markers pushback _markerName;
			_x = _x + 1;
		} else {
			_ok = false;
		};
	};
};
if ( isNil "BLOCKADE_markers" ) then {
	BLOCKADE_markers = [];
	private _ok = true;
	private _x = 1;
	while { _ok } do {
		private _markerName = format["Blocus_%1", _x];
		if !( (getMarkerPos _markerName) isEqualTo [0,0,0] ) then {
			_markerName setMarkerAlpha 0;
			BLOCKADE_markers pushback _markerName;
			_x = _x + 1;
		} else {
			_ok = false;
		};
	};
};

 
SIDE_coord = [0,0,0];

if ( (["sideMission"] call core_fnc_getParam) == 0 ) exitWith {
	#ifdef DEBUG
    debug(LL_INFO, "disabled by mission parameter");
    #endif 
    nil    
};

SIDE_jip = [];
SIDE_stop = false;
SIDE_success = false;
SIDE_mineMarker = "";
SIDE_switch = objNull;
publicVariable "SIDE_switch";
publicVariable "SIDE_success";

private _sideMissions = ["ia", "side", "missions"] call core_fnc_getSetting;
{
	private _mission = _x;
	private _keyWords = ["ia", "side", _x, "mapKeywords"] call core_fnc_getSetting;
	{
		if !( _x in MAP_KEYWORDS ) then {
			_sideMissions = _sideMissions - [_mission];
		}
	} forEach _keyWords;
} forEach _sideMissions;


if ( (count _sideMissions) == 0 ) exitWith {
	#ifdef DEBUG
	debug(LL_WARN, "No mission available after map keyword filter");
	#endif     		
};
    
#ifdef DEBUG
private _debug = format["%1 mission(s) available after map keyword filter", (count _sideMissions)];
debug(LL_INFO, _debug);
#endif 

SIDE_exclusionZones = ["water"];
{	_x params ["_area", "_actions"];
    if ( "noMissions" in _actions ) then { SIDE_exclusionZones pushback _area; };
} forEach BA_zones;

#ifdef DEBUG
private _debug = format["%1 urban mission marker have been found", (count URBAN_markers)];
debug(LL_DEBUG, _debug);
#endif 

private _missions = [];
private _checkDelay = ["ia", "checkDelay"] call core_fnc_getSetting;
private _cooldown = ["ia", "side", "cooldown"] call core_fnc_getSetting;

#ifdef DEBUG
private _debug = format["thread has started with %1s delay, %2s cooldown", _checkDelay, _cooldown];
debug(LL_INFO, _debug);
#endif  
while { true } do {
	[false, "SIDE_stop"] call zeusMission_fnc_checkAndWait;
	if ( SIDE_stop ) exitWith {};
	if ( (count _missions) == 0 ) then {
		_missions = _sideMissions;
	};
	private _type = selectRandom _missions;
	_missions = _missions - [_type];
	private _fncName = format["SIDE_fnc_%1", _type];
    if !( isNil _fncName ) then {
		private _code = compile format["[] spawn SIDE_fnc_%1", _type];
        SIDE_success = false;
        publicVariable "SIDE_success";
        #ifdef DEBUG
    	private _debug = format["spawning mission '%1'", _type];
		debug(LL_INFO, _debug);
        #endif
		SIDE_main = call _code;
		waitUntil {
			sleep _checkDelay;
			scriptDone SIDE_main
		};
        #ifdef DEBUG
    	private _debug = format["'%1' is done", _type];
		debug(LL_INFO, _debug);
        #endif
        { nil remoteExec["", _x]; } forEach SIDE_jip;
        nil remoteExec["side_fnc_delAction", 0, false];
        SIDE_jip = [];
        if !( isNull SIDE_switch ) then {
        	[1, format["Active Side mission has been cancelled by %1", (name SIDE_switch)], ["HQ", PLAYER_SIDE]] call global_fnc_chat;
        	missionNamespace setVariable['SIDE_switch', objNull, true];
		};
		if ( !zeusMission ) then {
			[_cooldown, _checkDelay, "SIDE_stop"] call common_fnc_smartSleep;
			if ( SIDE_stop ) exitWith {};
		};
	#ifdef DEBUG
	} else {
        private _debug = format["mission's function '%1' is not defined", _fncName];
		debug(LL_ERR, _debug);
	#endif
	};
};

nil