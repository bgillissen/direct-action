/*
@filename: feats\ia\SIDE\cleanup.sqf
Credit:
	Quiksilver
Author:
	Ben
Description:
	run on server,
	it remove the objects spawned for a SIDE mission, if no players are near; can be force 
*/

params ["_force", "_coord", "_radius", "_enemies", "_objects"];

deleteMarker (["ia", "side", "circle"] call core_fnc_getSetting);
deleteMarker (["ia", "side", "label"] call core_fnc_getSetting);

if ( !_force ) then {
	private _delay = ["ia", "checkDelay"] call core_fnc_getSetting;
	private _dist = ["ia", "deleteDistance"] call core_fnc_getSetting;
	waitUntil {
		sleep _delay;
		( ({((_x distance _coord) < _dist)} count allPlayers) isEqualTo 0 )
	};
	_delay = nil;
	_dist = nil;
};

private _lootSafeZones = [];
{
	_x params ["_area", "_actions"];
    if ( "LootSafe" in _actions ) then { _lootSafeZones pushback _area; };
} forEach BA_zones;

{
	private _list = _x;
	{
    if ( _x isEqualType objNull ) then {
        private _veh = _x;
        private _keep = ( (alive _x) && (({ isPlayer _x } count (crew _veh)) > 0) );
        if !( _keep ) then {
            {
	    		if ( _veh inArea _x ) exitWith { _keep = true; };
			} forEach _lootSafeZones;
        };
        if ( _keep ) then {
        	_list deleteAt _forEachIndex;
            _veh call vehicleWatch_fnc_monitor;
        };
    };
    } forEach _list;
} forEach [_enemies, _objects];

[_enemies] call common_fnc_deleteObjects;
[_objects] call common_fnc_deleteObjects;
[_coord, _radius] call common_fnc_deleteRuins;