/*
@filename: feats\ia\SIDE\misHQCoast.sqf
Credit:
	Quiksilver
Author:
	Ben
Description:
	run on server,
	spawn by feats\ia\SIDE\serverPostInitTheard.sqf, 
	a HQ bunker near coast with a crate to "activate" on top. 
Params:
	none
Environment:
	missionNamespace:
		S_crates
        SIDE_exclusionZones
		SIDE_stop
		zeusMission
	missionConfig:
		ia >> side >> minDistFromAO
		ia >> side >> hqCoast >> action
		ia >> side >> size
		ia >> side >> garrisonSkill
		ia >> side >> hqCoast >> title
		ia >> side >> briefing
		ia >> side >> hqCoast >> briefing
		ia >> checkDelay
		ia >> side >> failHint
		ia >> side >> hqCoast >> planted
		ia >> side >> boomDelay
		ia >> side >> successHint
Return:
		nothing 
*/

private _hqType = ["ia", "side", "hqcoast", "hqType"] call core_fnc_getSetting;
private "_flatPos";
private _found = false;
private _blackList = SIDE_exclusionZones + [];
if !( AO_coord isEqualTo [0,0,0] ) then {
	private _dist = (["ia", "side", "minDistFromAO"] call core_fnc_getSetting);
    //_blackList pushback [AO_coord, _dist, _dist, 0, false, -1];
    _blackList pushback [AO_coord, _dist];
};
private _limit = time + 20;

//find a flat position, near coast
while { !_found } do {
    private _position = [nil, _blackList] call BIS_fnc_randomPos;
	_flatPos = _position isFlatEmpty [2, 0, 0.3, (sizeOf _hqType), 1, true];
    _found = !(_flatPos isEqualTo [] );
	if ( !_found && time > _limit ) exitWith {};
};
if !( _found ) exitWith {};
_found = nil;

SIDE_coord = _flatPos;

//objective building
private _hq = _hqtype createVehicle _flatPos;
_hq setDir (random 360);
_hq setVectorUp [0,0,1];
SIDE_jip pushback (_hq remoteExec ["side_fnc_openDoors", 0, true]);
_hqtype = nil;

(getPos _hq) params["_hqX", "_hqY", "_hqZ"];
_hq setPos [_hqX, _hqY, (_hqZ + 0.5)];

//objective crate
(["crates"] call ia_fnc_randomSide) params ["_side", "_pool", "_key"];
private _crate = (selectRandom _pool) createVehicle [0,0,0];
_crate allowDamage false;
_crate setPos [_hqX, _hqY, (_hqZ + 5)];
private _action = ["ia", "side", "hqCoast", "action"] call core_fnc_getSetting;
SIDE_jip pushback ([_crate, _action] remoteExec ["SIDE_fnc_addAction", 0, true]);
_action =nil;

//ambiance objects
private _expireAt = time + 10;
private _trawler = objNull;
while { (time < _expireAt) } do {
	private _coord = ([_flatPos, 50, 200, 0, 2, 0] call BIS_fnc_findSafePos);
    if ( (getTerrainHeightASL _coord) <= -10 ) exitWith {
    	_trawler = "C_Boat_Civil_04_F" createVehicle (_coord + [0]);
		_trawler setDir random 360;   
    };
};

private _coord = [_flatPos, 20, 30, (sizeOf "O_Boat_Transport_01_F"), 0, 1, 0] call BIS_fnc_findSafePos;
_boat = "O_Boat_Transport_01_F" createVehicle _coord;
_boat setDir random 360;
_boat allowDamage false;
_boat lock 3;
_coord = nil;

//spawn units
private _size = ["ia", "side", "size"] call core_fnc_getSetting;
private _skill = ["ia", "side", "garrisonSkill"] call core_fnc_getSetting;
private _groups = [_flatPos, 0, 6, 2, [3,15], 1, 0, 0, 1, 2, 0, 0, 1, (_size + (random 150))] call SIDE_fnc_placeEnemies;
_groups pushback ([_hq, _skill, "hqCoast"] call IA_fnc_forcedGarrison);
_skill = nil;

//markers, tasks
private _title = ["ia", "side", "hqCoast", "title"] call core_fnc_getSetting; 
[_flatPos, _title, _size] call SIDE_fnc_placeMarkers;
private _desc = ["ia", "side", "hqCoast", "briefing"] call core_fnc_getSetting;
private _taskId = (["SIDE", "destroy", _title, _desc, true] call tasks_fnc_serverAdd);
call tasks_fnc_serverExport;
_title = nil;
_desc = nil;

private _checkDelay = ["ia", "checkDelay"] call core_fnc_getSetting; 

while { true } do {
	if (!alive _hq) exitWith {
        [_taskId, "Failed", false, true] call tasks_fnc_serverSetState;
		[false, _flatPos, _size, _groups, [_boat, _trawler, _crate, _hq]] spawn SIDE_fnc_cleanup;
	};
	if ( SIDE_success ) exitWith {
		private _planted = ["ia", "side", "hqCoast", "planted"] call core_fnc_getSetting;
		private _delay = ["ia", "side", "boomDelay"] call core_fnc_getSetting;
		[1, format[_planted, _delay], ["HQ", PLAYER_SIDE]] call global_fnc_chat;
		sleep _delay;
		[getPos _crate, true] spawn SIDE_fnc_boom;
		deleteVehicle _crate;
		private _reward = call IA_fnc_giveReward;
		private _hint = ["ia", "side", "successHint"] call core_fnc_getSetting;
		format[_hint, _reward] call global_fnc_hint;
        [_taskId, "Succeeded", false, true] call tasks_fnc_serverSetState;
		[false, _flatPos, _size, _groups, [_boat, _trawler, _hq]] spawn SIDE_fnc_cleanup;
	};
	if ( SIDE_stop || zeusMission || !isNull SIDE_switch ) exitWith {
        [_taskId, "Canceled", false, false] call tasks_fnc_serverSetState;
		[true, _flatPos, _size, _groups, [_boat, _trawler, _crate, _hq]] spawn SIDE_fnc_cleanup;
	};
	sleep _checkDelay;
};