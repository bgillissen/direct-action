/*
@filename: feats\ia\SIDE\missions\hostage.sqf
Author:
	Ben
Description:
	run on server,
	spawn by feats\ia\SIDE\serverPostInit.sqf, 
	a bunker with some hostage, they got to bring them back at base ALIVE. 
Params:
	none
Environment:
	missionNamespace:
		SIDE_stop
		SIDE_exclusionZones
		zeusMission
	missionConfig:
		ia >> side >> research >> hqType
		ia >> side >> minDistFromAO
		ia >> side >> table
		ia >> side >> laptop
		ia >> side >> research >> action
		ia >> side >> size
		ia >> side >> garrisonSkill
		ia >> side >> research >> title
		ia >> side >> briefing
		ia >> side >> research >> briefing
		ia >> checkDelay
		ia >> side >> failHint
		ia >> side >> research >> planted
		ia >> side >> boomDelay
		ia >> side >> successHint
Return:
		nothing
*/

private _hqType = ["ia", "side", "hostage", "hqType"] call core_fnc_getSetting;
private "_flatPos";
private _found = false;
private _blackList = SIDE_exclusionZones + [];
if !( AO_coord isEqualTo [0,0,0] ) then {
    private _dist = (["ia", "side", "minDistFromAO"] call core_fnc_getSetting);
    //_blackList = _blackList + [AO_coord, _dist, _dist, 0, false, -1]; 
    _blackList = _blackList + [AO_coord, _dist];
};
//find a flat position
while { !_found } do {
	private _position = [nil, _blackList] call BIS_fnc_randomPos;
	_flatPos = _position isFlatEmpty [5, 1, 0.2, (sizeOf _hqType), 0, false];
	_found = !( _flatPos isEqualTo [] );
};
_found = nil;

SIDE_coord = _flatPos;

//objective building
private _hq = _hqType createVehicle _flatPos;
_hq setDir (random 360);
_hq setVectorUp [0,0,1];
SIDE_hq = _hq;
//SIDE_jip pushback (_hq remoteExec ["side_fnc_openDoors", 0, true]);
_hqType = nil;

(getPos _hq) params["_hqX", "_hqY", "_hqZ"];
_hq setPos [_hqX, _hqY, (_hqZ + 0.2)];

//hostages
private _min = ["ia", "side", "hostage", "min"] call core_fnc_getSetting;
private _max = ["ia", "side", "hostage", "max"] call core_fnc_getSetting;
private _amount = floor random [_min, (_max * 0.75), _max];
_min = nil;
_max = nil;

private _hostages = [];
private _hostageGroups = [];
private _groups = [];
private _class = ["ia", "side", "hostage", "hostage"] call core_fnc_getSetting;

private _posToWorlds = [[5, 2, -3.28],[4, 2, -3.28],[4, 1, -3.28]];

for "_i" from 1 to _amount do {
    private _grp = createGroup [civilian, true];
    private _hostage = _grp createUnit [_class, _flatPos, [], 0, "NONE"];
    [_hostage, format['BALO_medic_%1', PLAYER_SIDE]] call baseAtmosphere_fnc_npcLoadout;
    [_hostage] call hostage_fnc_serverAdd;
    _hostage setPosASL (AGLToASL (_hq modelToWorld(_posToWorlds select _i)));
	_hostage setDir (getDir _hq);
    (group _hostage) setGroupIdGlobal [(['SIDE - Hostage', allGroups, {groupId _this}] call common_fnc_getUniqueName)];
	_hostages pushback _hostage;
    _hostageGroups pushback (group _hostage);    
    _groups pushback (group _hostage);
};

//spawn units
private _size = ["ia", "side", "size"] call core_fnc_getSetting;
private _skill = ["ia", "side", "garrisonSkill"] call core_fnc_getSetting;
_groups append ([_flatPos, 0, 6, 2, [3,15], 1, 0, 0, 1, 2, 0, 0, 0, (_size + (random 150))] call SIDE_fnc_placeEnemies);
_groups pushback ([_hq, _skill, "hostage"] call IA_fnc_forcedGarrison);
_skill = nil;

//markers, tasks
private _title = ["ia", "side", "hostage", "title"] call core_fnc_getSetting; 
[_flatPos, _title, _size] call SIDE_fnc_placeMarkers;
private _mainDesc = ["ia", "side", "hostage", "briefing"] call core_fnc_getSetting;
private _mainTask = (["SIDE", "run", _title, _mainDesc, true] call tasks_fnc_serverAdd);
private _objTasks = [];
private _subTitle = ["ia", "side", "hostage", "subTitle"] call core_fnc_getSetting; 
private _subDesc = ["ia", "side", "hostage", "subDesc"] call core_fnc_getSetting;
{ 
	_objTasks pushback (["HOSTAGE", "defend", format[_subTitle, (name _x)], format[_subDesc, (name _x)], false, _mainTask] call tasks_fnc_serverAdd);
} forEach _hostages;
call tasks_fnc_serverExport;
_title = nil;
_briefing = nil;
_subtitle = nil;
_subDesc = nil;

private _baseCoord = getMarkerPos "BS_INF";
private _checkDelay = ["ia", "checkDelay"] call core_fnc_getSetting;
private _sfx = ["ia", "side", "hostage", "alarm"] call core_fnc_getSetting;
private _alarm = objNull;
private _freed = false;
private _hostageKilled = [];
private _hostageSaved = 0;

while { true } do {
    {
        if ( !(alive _x) && !(_x in _hostageKilled) ) then {
            _hostageKilled pushbackUnique _x;
            [(_objTasks select _forEachIndex), "Failed", false, true] call tasks_fnc_serverSetState;
        };
    } forEach _hostages;
    private _hostageAlive = (count _hostages) - (count _hostageKilled);
    if ( _hostageAlive == 0 ) exitWith {
        [_mainTask, "Failed", false, true] call tasks_fnc_serverSetState;
		[false, _flatPos, _size, _groups, [_hq]] spawn SIDE_fnc_cleanup;
	};    
    {
        if ( (_x getVariable ['hostageFreed', false]) && !_freed ) then {
            if ( count _hostages > 1 ) then {
                [7, format["%1 - We were trapped, they are all rushing in, we got to GTFO!", (name _x)]] call global_fnc_chat;
            } else {
            	[7, format["%1 - I was trapped, they are all rushing in, we got to GTFO!", (name _x)]] call global_fnc_chat;
            };
            _alarm = createSoundSource [_sfx, _hq, [], 0];
            _freed = true;
            {
                if ( (_x isEqualType grpNull) && !(_x in _hostagegroups) ) then {
                    _x call common_fnc_removeWP;
                    _x setBehaviour "COMBAT";
                    _x setCombatMode "RED";
                    _x setSpeedMode "FULL";
                    private _wp = _x addWaypoint [_hq, 70, 0];
        			//_wp setWaypointTimeout [300, 300, 300];
                    _wp setWaypointType "DESTROY";
        			_wp setWaypointCompletionRadius 20;
                    _wp setWaypointBehaviour "COMBAT";
                    _wp setWaypointCombatMode "RED";
                    _wp setWaypointSpeed "FULL";
                    _wp setWaypointStatements ["true", "(group this) call common_fnc_removeWP;[(group this), SIDE_hq, (random[100, 250, 500])] call BIS_fnc_taskPatrol"];
                    _x setCurrentWaypoint _wp; 
                };
            } forEach _groups;
        };
        if ( (alive _x) && ((_x distance _baseCoord) < 30)  && ((vehicle _x) isEqualTo _x) && !(_x getVariable['SIDE_saved', false]) ) then {
            _hostageSaved = _hostageSaved + 1;
            _x setVariable['SIDE_saved', true];
            [_x] remoteExec ['hostage_fnc_cleanup', 0, false];
            _x setUnitPos "UP";
            (group _x) setSpeedMode "FULL";
            (group _x) call common_fnc_removeWP;
            (group _x) addWaypoint [(getMarkerPos 'BS_HPILOT'), 0, 0];
            [7, format['%1 - FREEEEEEEEEEEEEEEEEEEEEEEDOOOOOOOOOOOOOOOOM!', (name _x)]] call global_fnc_chat;
            _x spawn {
                sleep 30;
            	_this hideObjectGlobal true;
                (group _this) call common_fnc_removeWP;
			};
            [(_objTasks select _forEachIndex), "Succeeded", false, true] call tasks_fnc_serverSetState;
        };
    } forEach _hostages;
    if ( _hostageSaved >= _hostageAlive ) exitWith {
        SIDE_success = true;
		private _reward = call IA_fnc_giveReward;
		private _hint = ["ia", "side", "successHint"] call core_fnc_getSetting;
		format[_hint, _reward] call global_fnc_hint;
        [_mainTask, "Succeeded", false, true] call tasks_fnc_serverSetState;
		[false, _flatPos, _size, _groups, [_hq, _alarm]] spawn SIDE_fnc_cleanup;
	};
	if ( SIDE_stop || zeusMission || !isNull SIDE_switch ) exitWith {
        {
            if !( _x getVariable['SIDE_saved', false] ) then {
                [_x] remoteExec ['hostage_fnc_cleanup', 0, false];
			}; 
        } forEach _hostages;
        [_mainTask, "Canceled", true, false] call tasks_fnc_serverSetState;
		[true, _flatPos, _size, _groups, [_hq, _alarm]] spawn SIDE_fnc_cleanup;
	};
	sleep _checkDelay;
};

SIDE_hq = nil;