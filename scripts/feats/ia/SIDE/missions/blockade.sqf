if ( (count BLOCKADE_markers) == 0 ) exitWith {};

if ( isNil "BLOCKADE_pool" ) then { BLOCKADE_pool = []; };
if ( (count BLOCKADE_pool) == 0 ) then { BLOCKADE_pool = BLOCKADE_markers; };

private "_coord";
private _blackList = SIDE_exclusionZones - ["water"];
if !( AO_coord isEqualTo [0,0,0] ) then {
    private _dist = (["ia", "side", "minDistFromAO"] call core_fnc_getSetting);
    _blackList pushback [AO_coord, _dist, _dist, 0, false, -1]; 
};
private _found = false;
private _tempPool = BLOCKADE_pool + [];
private "_marker"; 
while {( !_found && ((count _tempPool) > 0) )} do {
	_marker = selectRandom _tempPool;
    _tempPool = _tempPool - [_marker];  
	_coord = getMarkerPos _marker;
	if ( ({( _coord inArea _x )} count _blacklist) isEqualTo 0 ) then {
		_found = true;
		BLOCKADE_pool = BLOCKADE_pool - [_marker];
	};
};

if !( _found ) exitWith {};

_tempPool = nil;
_found = nil;

SIDE_mineMarker = _marker;
SIDE_coord = getMarkerPos _marker;
_coord = SIDE_coord + [];

//spawn mines
private _max = ["ia", "side", "blockade", "mineMax"] call core_fnc_getSetting;
private _mid = ["ia", "side", "blockade", "mineMid"] call core_fnc_getSetting;
private _min = ["ia", "side", "blockade", "mineMin"] call core_fnc_getSetting;
private _mineCount = random [_min, _mid, _max];
private _mines = [];
private _mineMarkers = [];
private _mineBlacklist = ["ground"];

for "_x" from 1 to _mineCount do {
    private _pos = [];
    private _ok = false;
    private _bottom = 0;
    while { !_ok } do {
    	_pos = [[_marker], _mineBlacklist] call BIS_fnc_randomPos;
        _bottom = (getTerrainHeightASL _pos);
        _ok = (_bottom < -3 );
	};
    if ( _bottom < -5 ) then { _bottom = -5; };
    private _surface = -0.5;
    private _depth = random [_bottom, (_bottom / 2),_surface];
    _pos = [_pos select 0, _pos select 1, _depth];
    _mineBlacklist pushback [_pos, 100, 100, 0, false, -1];
    private _mm = format["mine_%1", _x]; 
    createMarker [_mm, _pos];
	_mm setMarkerShape "ICON"; 
    _mm setMarkerColor "ColorRed";
    _mm setMarkerAlpha 0.4;
    _mm setMarkerType "hd_dot";
    private _mine = createMine ["UnderwaterMine", _pos, [], 0];
    [_mine, false] call curator_fnc_addEditable;
    _mines pushback _mine;
    
    _mineMarkers pushback _mm;
};
_mineBlacklist = nil;

//spawn units
private _size = ["ia", "side", "size"] call core_fnc_getSetting;
private _groups = [_coord, 0, 4, 2, [3, 15], 2, 1, 0, 1, 2, 0, 3, 3, _size] call SIDE_fnc_placeEnemies;
private _divers = ["O_diver_TL_F", "O_diver_exp_F", "O_diver_F",
				   "B_diver_TL_F", "B_diver_exp_F", "B_diver_F",
				   "I_diver_TL_F", "I_diver_exp_F", "I_diver_F"];
private _units = []; 
{
    if ( _x isEqualType grpNull ) then {
        private _grpUnits = (units _x);
        private _add = true;
		{ 
			if ( (typeOf _x) in _divers ) exitWith { _add = false; };
		} forEach _grpUnits;
        if ( _add ) then { _units append _grpUnits; };  
	};
} forEach _groups;

private _infThreshold = ["ia", "side", "blockade", "threshold"] call core_fnc_getSetting;

//markers, tasks
private _title = ["ia", "side", "blockade", "title"] call core_fnc_getSetting; 
[_coord, _title, _size] call SIDE_fnc_placeMarkers;
private _mainDesc = ["ia", "side", "blockade", "briefing"] call core_fnc_getSetting;
private _mainTask = (["SIDE", "container", _title, _mainDesc, true] call tasks_fnc_serverAdd);
private _mineTitle = ["ia", "side", "blockade", "mineTitle"] call core_fnc_getSetting;
private _mineDesc = ["ia", "side", "blockade", "mineDesc"] call core_fnc_getSetting;
private _mineTask = (["SIDE", "mine", _mineTitle, _mineDesc, false, _mainTask] call tasks_fnc_serverAdd);
private _infTitle = ["ia", "side", "blockade", "infTitle"] call core_fnc_getSetting;
private _infDesc = ["ia", "side", "blockade", "infDesc"] call core_fnc_getSetting;
private _infTask = (["SIDE", "kill", _infTitle, format[_infDesc, _infThreshold], false, _mainTask] call tasks_fnc_serverAdd);
call tasks_fnc_serverExport;
_mainDesc = nil;
_infTitle = nil;
_infDesc = nil;
_mineTitle = nil;
_mineDesc = nil;

private _checkDelay = ["ia", "checkDelay"] call core_fnc_getSetting;
private _mineOK = false;
private _infOK = false;

while { true } do {
    if !( _mineOK ) then {
        private _mineActive = 0;
        {
            if !( mineActive _x ) then {
                (_mineMarkers select _forEachIndex) setMarkerAlpha 0;
            } else {
                _mineActive = _mineActive + 1;
                (_mineMarkers select _forEachIndex) setMarkerAlpha 0.4;
            };
        } forEach _mines;
    	if ( _mineActive isEqualTo 0 ) then {
            SIDE_mineMarker = "";
	        _mineOK = true;
    	    [_mineTask, "Succeeded", false, true] call tasks_fnc_serverSetState;
		};
    };
    if !( _infOK ) then {
    	if ( ({alive _x} count _units) <= _infThreshold ) then {
    		_infOK = true;
			[_infTask, "Succeeded", false, false] call tasks_fnc_serverSetState;
    	};
	};
    if ( _infOK && _mineOK ) exitWith {
    	private _reward = call IA_fnc_giveReward;
		private _hint = ["ia", "side", "successHint"] call core_fnc_getSetting;
		format[_hint, _reward] call global_fnc_hint;
        [_mainTask, "Succeeded", false, true] call tasks_fnc_serverSetState;
        { deleteMarker _x; } forEach _mineMarkers;
		[false, _coord, _size, _groups, _mines] spawn SIDE_fnc_cleanup;    
    };
    if ( SIDE_stop || zeusMission || !isNull SIDE_switch ) exitWith {
        SIDE_mineMarker = "";
        [_mainTask, "Canceled", true, false] call tasks_fnc_serverSetState;
        { deleteMarker _x; } forEach _mineMarkers;
		[true, _coord, _size, _groups, _mines] spawn SIDE_fnc_cleanup;
	};
	sleep _checkDelay;
};