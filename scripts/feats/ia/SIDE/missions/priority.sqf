/*
@filename: feats\ia\SIDE\misPriotiry.sqf
Credit:
	Quiksilver
Author:
	Ben
Description:
	run on server,
	spawn by feats\ia\SIDE\serverPostInitTheard.sqf,
	fortified and protected Arti or AA batteries
Params:
	none
Environment:
	missionNamespace:
		SIDE_stop
		SIDE_exclusionZones
		zeusMission
	missionConfig:
		ia >> checkDelay
		ia >> side >> size
		ia >> side >> minDistFromAO
		ia >> side >> priority >> maxDistFromAO
		ia >> side >> priority >> artiProb
		ia >> side >> priority >> infiniteAmmo
		ia >> side >> priority >> extraHealth
		ia >> side >> priority >> HBarrier
		ia >> side >> priority >> arti >> title
		ia >> side >> priority >> aa >> title
		ia >> side >> priority >> briefing
		ia >> side >> priority >> arti >> briefing
		ia >> side >> priority >> aa >> briefing		
		ia >> side >> priority >> success
		ia >> side >> priority >> arti >> notification
		ia >> side >> priority >> aa >> notification
	missionParameters:
		ArtilleryTargetTickTimeMax
		ArtilleryTargetTickTimeMin
Return:
	nothing
*/

#include "..\_debug.hpp"

private _sizeType = "Land_Dome_Small_F";
private "_flatPos";
private _found = false;
private _blackList = SIDE_exclusionZones + [];
private "_whiteList";
if !( AO_coord isEqualTo [0,0,0] ) then {
	private _min = (["ia", "side", "minDistFromAO"] call core_fnc_getSetting);
    //_blackList pushback [AO_coord, _min, _min, 0, false, -1];
    _blackList pushback [AO_coord, _min];
    private _max = (["ia", "side", "priority", "maxDistFromAO"] call core_fnc_getSetting);
    //_whiteList = [[AO_coord, _max, _max, 0, false, -1]]; 
    _whiteList = [[AO_coord, _max]];
};

//find a flat position, not too close from base, and not too far, not too close from the active AO (if one)
while { !_found } do {
    private "_position";
    if ( isNil "_whiteList" ) then {
		_position = [nil, _blackList] call BIS_fnc_randomPos;
	} else {
		_position = [_whiteList, _blackList] call BIS_fnc_randomPos;
	};
	_flatPos = _position isFlatEmpty [5, 0, 0.2, (sizeOf _sizeType), 0, false];
    _found = !( _flatPos isEqualTo [] );
};
_found = nil;

SIDE_coord = _flatPos;
_flatPos params["_cX", "_cY", "_cZ"];

private _tankCoords = [[_cX - 2, _cY - 2, _cZ], [_cX + 2, _cY + 2, _cZ]];
private _tankDir = (random 360);
private _truckCoord = [_cX + 20, _cY + random 20, _cZ];

//arti or AA ?
private _isArti = (random 100 <= (["ia", "side", "priority", "artiProb"] call core_fnc_getSetting));
//_isArti = true;

private "_poolName";
if ( _isArti ) then {
	_poolName = "arti";
} else {
	_poolName = "aa";
};

([_poolName] call ia_fnc_randomSide) params ["_side", "_pool", "_key"];

if ( (count _pool) == 0 ) exitWith {
    #ifdef DEBUG
    debug(LL_WARN, "priotity : pool is empty, abording");
    #endif
};
//spawn objective vehicles
private _tank1 = (selectRandom _pool) createVehicle (_tankCoords select 0);
_tank1 setDir _tankDir;
private _tank2 = (selectRandom _pool) createVehicle (_tankCoords select 1);
_tank2 setDir _tankDir;
_pool = nil;

private _infAmmo = [false, true] select (["ia", "side", "priority", "infiniteAmmo"] call core_fnc_getSetting);
private _stronger = [false, true] select (["ia", "side", "priority", "extraHealth"] call core_fnc_getSetting);
private _groups = [];
{
	_x lock 3;
	_x allowCrewInImmobile true;
    createVehicleCrew _x;
    _x engineOn true;
    deleteVehicle (driver _x);
    {
        _x setVariable ["NOAI", true, true];
        [_x, 6] call common_fnc_setSkill;
    } forEach (crew _x);
    private _gunner = gunner _x;
    private _group = group _gunner;
    _group setVariable ['NOLB', true, true];
    _group setBehaviour "COMBAT";
	_group setCombatMode "RED";	
	_group allowFleeing 0;
    _group setGroupIdGlobal [(['SIDE - priority', allGroups, {groupId _this}] call common_fnc_getUniqueName)];
	_groups append [_group];
    [_x, true] call curator_fnc_addEditable;
    if ( _infAmmo ) then {
		_gunner setVariable ["fired_EH", (_gunner addEventHandler ["FiredMan",{ (_this select 7) setVehicleAmmo 1 }])];
	};
    if ( _stronger ) then {
		_x setVariable ["damage_EH", ([_x] call SIDE_fnc_reduceDamage)];
    };
} forEach [_tank2, _tank1];

//H-barrier ring
private _distance = 16;
private _dir = 0;
private _protect = ["ia", "side", "priority", "HBarrier"] call core_fnc_getSetting;
for "_c" from 0 to 7 do {
	private _pos = [_flatPos, _distance, _dir] call BIS_fnc_relPos;
	private _barrier = _protect createVehicle _pos;
	_barrier setDir _dir;
	_barrier setVectorUP (surfaceNormal (getPosATL _barrier));
	_dir = _dir + 45;
	_barrier allowDamage false; 
	_barrier enableSimulation false;
	_groups append [_barrier];
	_barrier = nil;
};
_distance = nil;
_dir = nil;
_protect = nil;

//spawn units
private _size = ["ia", "side", "size"] call core_fnc_getSetting;
private _aaCount = 0;
if ( _isArti ) then { _aaCount = 2; };
_groups append ([_flatPos, 0, 6, 2, [3,15], 1, _aaCount, 0, 1, 2, 0, 0, 0, (_size + (random 150))] call SIDE_fnc_placeEnemies);

//markers, tasks
private _cfg  = ["aa", "arti"] select (_isArti);
private _title = ["ia", "side", "priority", _cfg, "title"] call core_fnc_getSetting;
[_flatPos, _title, _size] call SIDE_fnc_placeMarkers;
private _mainDesc = ["ia", "side", "priority", _cfg, "briefing"] call core_fnc_getSetting;
private _mainTask = (["SIDE", "target", _title, _mainDesc, true] call tasks_fnc_serverAdd);
private _subTasks = [];
private _subTitle = ["ia", "side", "priority", "subTitle"] call core_fnc_getSetting;
private _subDesc = ["ia", "side", "priority", "subDesc"] call core_fnc_getSetting;
_subTasks set [0, (["TRGT1", "destroy", format[_subTitle, 1], _subDesc, false, _mainTask] call tasks_fnc_serverAdd)];
_subTasks set [1, (["TRGT2", "destroy", format[_subTitle, 2], _subDesc, false, _mainTask] call tasks_fnc_serverAdd)];
call tasks_fnc_serverExport;
_title = nil;
_briefing = nil;
_desc = nil;

private ["_tickMax", "_tickMin", "_safeZones"];
if ( _isArti ) then {
	_tickMax = (["ArtiTargetTickTimeMax"] call core_fnc_getParam);
	_tickMin = (["ArtiTargetTickTimeMin"] call core_fnc_getParam);
    _safeZones = [];
    { 
    	_x params ["_area", "_actions"];
    	if ( "mortarSafe" in _actions ) then { _safeZones pushback _area; };
    } forEach BA_zones;
};

private _checkDelay = ["ia", "checkDelay"] call core_fnc_getSetting;
private _strykeThread = scriptNull;
private _killed = [];

while { true } do {
	{
		if ( !(alive _x) && !(isNull _x) ) then {
			if ( _stronger ) then { _x removeEventHandler ["HandleDamage", (_x getVariable "damage_EH")]; };
			if ( _infAmmo ) then { _x removeEventHandler ["Fired", (_x getVariable "fired_EH")]; };
            if !( _x in _killed ) then {
            	[(_subTasks select _forEachIndex), "Succeeded", false, false] call tasks_fnc_serverSetState;
                _killed pushback _x;    
            };
		};
	} forEach [_tank1, _tank2];
	
	if ( (!alive _tank1) && (!alive _tank2) ) exitWith {
        [_mainTask, "Succeeded", true, true] call tasks_fnc_serverSetState;
		_notif = nil;
		[false, _flatPos, _size, _groups, [_tank1, _tank2]] spawn SIDE_fnc_cleanup;
	};
	if ( SIDE_stop || zeusMission || !isNull SIDE_switch ) exitWith {
        [_mainTask, "Canceled", true, false] call tasks_fnc_serverSetState;
		[true, _flatPos, _size, _groups, [_tank1, _tank2]] spawn SIDE_fnc_cleanup;
	};
	if ( _isArti ) then {
		private "_tick"; 
		if (_tickMax <= _tickMin) then {
			_tick = _tickMin;
		} else {
			_tick = (_tickMin + (random (_tickMax - _tickMin)));
		};
        if ( isNull _strykeThread ) then {
			private _end = time + _tick;
	        #ifdef DEBUG
	        private _debug = format["priotity : waiting %1s for the next stryke", _tick];
	    	debug(LL_DEBUG, _debug);
	    	#endif
			waitUntil {
				sleep _checkDelay;
				( (time >= _end) || SIDE_stop || zeusMission || (({( (alive _x) && !(isNull _x) )} count [_tank1, _tank2]) isEqualTo 0) || !isNull SIDE_switch )
			};
	        if ( !SIDE_stop && !zeusMission && isNull SIDE_switch  ) then { 
	        	_strykeThread = [[_tank1, _tank2], _safeZones] spawn SIDE_fnc_artiFire;
			};
        };
	} else {
		[[_tank1, _tank2]] call SIDE_fnc_aaFire;
	};
	sleep _checkDelay;
};