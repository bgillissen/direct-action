/*
@filename: feats\ia\SIDE\misResearch.sqf
Credit:
	Quiksilver
Author:
	Ben
Description:
	run on server,
	spawn by feats\ia\SIDE\serverPostInitTheard.sqf, 
	a Research bunker with a laptop to "activate" inside. 
Params:
	none
Environment:
	missionNamespace:
		SIDE_stop
		SIDE_exclusionZones
		zeusMission
	missionConfig:
		ia >> side >> research >> hqType
		ia >> side >> minDistFromAO
		ia >> side >> table
		ia >> side >> laptop
		ia >> side >> research >> action
		ia >> side >> size
		ia >> side >> garrisonSkill
		ia >> side >> research >> title
		ia >> side >> briefing
		ia >> side >> research >> briefing
		ia >> checkDelay
		ia >> side >> failHint
		ia >> side >> research >> planted
		ia >> side >> boomDelay
		ia >> side >> successHint
Return:
		nothing
*/

private _hqType = ["ia", "side", "research", "hqType"] call core_fnc_getSetting;
private "_flatPos";
private _found = false;
private _blackList = SIDE_exclusionZones + [];
if !( AO_coord isEqualTo [0,0,0] ) then {
	private _dist = (["ia", "side", "minDistFromAO"] call core_fnc_getSetting);
    //_blackList pushback [AO_coord, _dist, _dist, 0, false, -1];
    _blackList pushback [AO_coord, _dist];
};
//find a flat position
while { !_found } do {
	private _position = [nil, _blackList] call BIS_fnc_randomPos;
	_flatPos = _position isFlatEmpty [5, 1, 0.2, (sizeOf _hqType), 0, false];
	_found = !( _flatPos isEqualTo [] );
};
_found = nil;

SIDE_coord = _flatPos;

//objective building
private _hq = _hqType createVehicle _flatPos;
_hq setDir (random 360);
_hq setVectorUp [0,0,1];
SIDE_jip pushback (_hq remoteExec ["side_fnc_openDoors", 0, true]);
_hqType = nil;

(getPos _hq) params["_hqX", "_hqY", "_hqZ"];
_hq setPos [_hqX, _hqY, (_hqZ + 0.2)];

//objective table, laptop
private _tableType = ["ia", "side", "table"] call core_fnc_getSetting;
private _table = _tableType createVehicle [_hqX, _hqY, (_hqZ + 5)];
_table setPosASL (AGLToASL (_hq modelToWorld [5, 2, -3.28]));
_table setDir (getDir _hq);
_table enableSimulationGlobal false;
_tableType = nil;

private _laptopType = ["ia", "side", "laptop"] call core_fnc_getSetting;
private _laptop = (selectRandom _laptopType) createVehicle [_hqX, _hqY, (_hqZ+5)];
_laptopType = nil;

[_table, _laptop, [0,0,0.80]] call BIS_fnc_relPosObject;
_laptop enableSimulationGlobal false;
_laptop setDir (getdir _table - 180);

private _action = ["ia", "side", "research", "action"] call core_fnc_getSetting;
SIDE_jip pushback ([_table, _action] remoteExec ["SIDE_fnc_addAction", 0, true]);
_action = nil;

//spawn units
private _size = ["ia", "side", "size"] call core_fnc_getSetting;
private _skill = ["ia", "side", "garrisonSkill"] call core_fnc_getSetting;
private _groups = [_flatPos, 0, 6, 2, [3,15], 1, 0, 0, 1, 2, 0, 0, 0, (_size + (random 150))] call SIDE_fnc_placeEnemies;
_groups pushback ([_hq, _skill, "research"] call IA_fnc_forcedGarrison);
_skill = nil;


//markers, tasks
private _title = ["ia", "side", "research", "title"] call core_fnc_getSetting; 
[_flatPos, _title, _size] call SIDE_fnc_placeMarkers;
private _desc = ["ia", "side", "research", "briefing"] call core_fnc_getSetting;
private _taskId = (["SIDE", "download", _title, _desc, true] call tasks_fnc_serverAdd);
call tasks_fnc_serverExport;
_title = nil;
_desc = nil;

private _checkDelay = ["ia", "checkDelay"] call core_fnc_getSetting;

while { true } do {
	if (!alive _hq) exitWith {
        [_taskId, "Failed", false, true] call tasks_fnc_serverSetState;
		[false, _flatPos, _size, _groups, [_laptop, _table, _hq]] spawn SIDE_fnc_cleanup;
	};
	if ( SIDE_success ) exitWith {
		private _planted = ["ia", "side", "research", "planted"] call core_fnc_getSetting;
		private _delay = ["ia", "side", "boomDelay"] call core_fnc_getSetting;
		[1, format[_planted, _delay], ["HQ", PLAYER_SIDE]] call global_fnc_chat;
		sleep _delay;
		[[_hqX, _hqY, (_hqZ+2)], false] spawn SIDE_fnc_boom;
		deleteVehicle _laptop;
		deleteVehicle _table;
		private _reward = call IA_fnc_giveReward;
		private _hint = ["ia", "side", "successHint"] call core_fnc_getSetting;
		format[_hint, _reward] call global_fnc_hint;
        [_taskId, "Succeeded", false, true] call tasks_fnc_serverSetState;
		[false, _flatPos, _size, _groups, [_hq]] spawn SIDE_fnc_cleanup;
	};
	if ( SIDE_stop || zeusMission || !isNull SIDE_switch ) exitWith {
        [_taskId, "Canceled", false, false] call tasks_fnc_serverSetState;
		[true, _flatPos, _size, _groups, [_laptop, _table, _hq]] spawn SIDE_fnc_cleanup;
	};
	sleep _checkDelay;
};