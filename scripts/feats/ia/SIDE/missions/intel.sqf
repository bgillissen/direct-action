/*
@filename: feats\ia\SIDE\misIntel.sqf
Credit:
	Quiksilver
Author:
	Ben
Description:
	run on server,
	spawn by feats\ia\SIDE\serverPostInitTheard.sqf,
	an officer to "active" inside a car or not
Params:
	none
Environment:
	missionNamespace:
		SIDE_stop
		SIDE_exclusionZones
		zeusMission
		S_officer
		S_car
		S_crew
	missionConfig:
		ia >> side >> minDistFromAO
		ia >> side >> intel >> vehicleProb
		ia >> side >> intel >> action
		ia >> side >> intel >> triggerSize
		ia >> side >> size 
		ia >> side >> intel >> title
		ia >> side >> briefing
		ia >> side >> intel >> briefing
		ia >> checkDelay
		ia >> side >> failHint
		ia >> side >> intel >> fail
		ia >> side >> intel >> spotted
		ia >> side >> intel >> secured
		ia >> side >> successHint
Return:
		nothing
*/

#include "..\_debug.hpp" 

SIDE_intelFiredNear = false;

private _firedNearFnc = {
    params ["_unit", "_src", "_dist", "_weap", "_muzzle", "_mode", "_ammo", "_gunner"];
    if !( alive _unit ) exitWith {};
    if ( !(_muzzle isEqualTo "") && (_dist > 40) ) exitWith {};
    SIDE_intelFiredNear = true;
};

private _sizeType = "Land_Dome_Small_F";
private "_flatPos";
private _found = false;
private _blackList = SIDE_exclusionZones + [];
if !( AO_coord isEqualTo [0,0,0] ) then {
	private _dist = (["ia", "side", "minDistFromAO"] call core_fnc_getSetting);
    //_blackList pushback [AO_coord, _dist, _dist, 0, false, -1]; 
    _blackList pushback [AO_coord, _dist];
};
//find a flat position
while { !_found } do {
	private _position = [nil, _blackList] call BIS_fnc_randomPos;
	_flatPos = _position isFlatEmpty [5, 1, 0.2, (sizeOf _sizeType), 0, false];
	_found = !( _flatPos isEqualTo [] );
};
_sizeType = nil;
_found = nil;

SIDE_coord = _flatPos;

private _inVehicle = (random 100 <= (["ia", "side", "intel", "vehicleProb"] call core_fnc_getSetting));

private _pos1 = [_flatPos, 3, random 360] call BIS_fnc_relPos;
private _pos2 = [_flatPos, 10, random 360] call BIS_fnc_relPos;
private _pos3 = [_flatPos, 15, random 360] call BIS_fnc_relPos;

//Intel
(["officer"] call ia_fnc_randomSide) params ["_side", "_pool", "_key"];
private _intelGroup = createGroup [_side, true];
[_intelGroup, true] call dynSim_fnc_set;
_intelGroup setGroupIdGlobal [(["SIDE - intel", allGroups, {groupId _this}] call common_fnc_getUniqueName)];
private _intel = _intelGroup createUnit [(selectRandom _pool), _pos1, [], 0, "NONE"];
_intel setVariable ["NOAI", true, true];
_intel addEventHandler["FiredNear", _firedNearFnc];
if !( (primaryWeapon _intel) isEqualTo "" ) then { _intel removeWeaponGlobal (primaryWeapon _intel); }; 
if !( (secondaryWeapon _intel) isEqualTo "" ) then { _intel removeWeaponGlobal (secondaryWeapon _intel); };
if !( (handgunWeapon _intel) isEqualTo "" ) then { _intel removeWeaponGlobal (handgunWeapon _intel); };
removeAllWeapons _intel;
_intelGroup selectLeader _intel;

removeAllWeapons _intel;

//Intel driver, car
private _intelDriver = _intelGroup createUnit [(selectRandom (S_crew select _key)), _flatPos, [], 0, "NONE"];
_intelDriver setVariable["NOAI", true, true];
private _intelCar = (selectRandom (S_car select _key)) createVehicle _flatPos;
_intelCar setDir (random 360);
if ( (["ia", "lockVeh"] call core_fnc_getSetting) == 1 ) then { _intelCar lock 3; };
_intelDriver assignAsDriver _intelCar;
_intelDriver moveInDriver _intelCar;

//Fake 1 driver, car
private _fake1Group = createGroup [_side, true];
[_fake1Group, true] call dynSim_fnc_set;
_fake1Group setGroupIdGlobal [(["SIDE - intel", allGroups, {groupId _this}] call common_fnc_getUniqueName)];
private _fake1Driver = _fake1Group createUnit [(selectRandom (S_crew select _key)), _flatPos, [], 0, "NONE"];
_fake1Driver setVariable["NOAI", true, true];
private _fake1Car = (selectRandom (S_car select _key)) createVehicle _pos2;
if ( (["ia", "lockVeh"] call core_fnc_getSetting) == 1 ) then { _fake1Car lock 3; };
_fake1Driver assignAsDriver _fake1Car;
_fake1Driver moveInDriver _fake1Car;


//Fake 2 driver, car
private _fake2Group = createGroup [_side, true];
[_fake2Group, true] call dynSim_fnc_set;
_fake2Group setGroupIdGlobal [(["SIDE - intel", allGroups, {groupId _this}] call common_fnc_getUniqueName)];
private _fake2Driver = _fake2Group createUnit [(selectRandom (S_crew select _key)), _flatPos, [], 0, "NONE"];
_fake2Driver setVariable["NOAI", true, true];
private _fake2Car = (selectRandom (S_car select _key)) createVehicle _pos3;
if ( (["ia", "lockVeh"] call core_fnc_getSetting) == 1 ) then { _fake2Car lock 3; };
_fake2Driver assignAsDriver _fake2Car;
_fake2Driver moveInDriver _fake2Car;

private ["_guard1", "_guard2", "_addActionTo"];
if ( _inVehicle ) then {
    _intelCar addEventHandler["FiredNear", _firedNearFnc];
	_intel moveInCargo _intelCar;
	_addActionTo = _intelCar;
} else {
    _guard1 = _intelGroup createUnit [(selectRandom _pool), _pos1, [], 0, "NONE"];
	_guard1 setVariable ["NOAI", true, true];
    _guard1 addEventHandler["FiredNear", _firedNearFnc];
    _guard2 = _intelGroup createUnit [(selectRandom _pool), _pos1, [], 0, "NONE"];
	_guard2 setVariable ["NOAI", true, true];
    _guard2 addEventHandler["FiredNear", _firedNearFnc];
	_addActionTo = _intel;
};

//set skill
[(units _intelGroup), 4] call common_fnc_setSkill;
[(units _fake1Group), 4] call common_fnc_setSkill;
[(units _fake2Group), 4] call common_fnc_setSkill;

//add to zeus
[(units _intelGroup), true] call curator_fnc_addEditable;
[(units _fake1Group), true] call curator_fnc_addEditable;
[(units _fake2Group), true] call curator_fnc_addEditable;
[[_intelCar, _fake1Car, _fake2Car], true] call curator_fnc_addEditable;

private _action = ["ia", "side", "intel", "action"] call core_fnc_getSetting;
SIDE_jip pushback ([_addActionTo, _action] remoteExec ["SIDE_fnc_addAction", 0, true]);
_addActionTo = nil;
_action = nil;

private _groups = [_intelGroup, _fake1Group, _fake2Group];

//spawn units
_size = ["ia", "side", "size"] call core_fnc_getSetting;
_groups append ([_flatPos, 0, 6, 0, [3,15], 0, 0, 0, 1, 2, 0, 0, 0, (_size - (random 50))] call SIDE_fnc_placeEnemies);

//markers, tasks
private _title = ["ia", "side", "intel", "title"] call core_fnc_getSetting;
[_flatPos, _title, _size] call SIDE_fnc_placeMarkers;
private _desc = ["ia", "side", "intel", "briefing"] call core_fnc_getSetting;
private _taskId = (["SIDE", "intel", _title, _desc, true] call tasks_fnc_serverAdd);
call tasks_fnc_serverExport;
_title = nil;
_desc = nil;

private _isFleing = false;
private _sleep = true;
private _escaped = false;
private _escapeThread = false;
private _escapeDistance = ["ia", "side", "intel", "triggerSize"] call core_fnc_getSetting;
private _checkDelay = ["ia", "checkDelay"] call core_fnc_getSetting;


#ifdef DEBUG
debug(LL_DEBUG, "intel: loop start");
#endif
 
while { true } do {
	private "_cond";
	if ( _inVehicle ) then {
		_cond = (alive _intelCar);
	} else {
		_cond = (alive _intel);
	};
	if ( !_cond || _escaped ) exitWith {
        if ( _escaped ) then {
			private _chat = ["ia", "side", "intel", "fail"] call core_fnc_getSetting;
			[1, _chat, ["HQ", PLAYER_SIDE]] call global_fnc_chat;
		};
        [_taskId, "Failed", false, true] call tasks_fnc_serverSetState;
		[false, _flatPos, _size, _groups, [_intelCar, _fake1Car, _fake2Car]] spawn SIDE_fnc_cleanup;
	};
    if !( (primaryWeapon _intel) isEqualTo "" ) then { _intel removeWeaponGlobal (primaryWeapon _intel); }; 
	if !( (secondaryWeapon _intel) isEqualTo "" ) then { _intel removeWeaponGlobal (secondaryWeapon _intel); };
	if !( (handgunWeapon _intel) isEqualTo "" ) then { _intel removeWeaponGlobal (handgunWeapon _intel); };
	removeAllWeapons _intel;
	if ( _isFleing ) then {
        _escaped = ( { (_x distance _intel) < _escapeDistance } count allPlayers <= 0 );
        if ( !_escaped && !_escapeThread ) then {
            _escapeThread = true;
	        {
                [_flatPos, _size, _x] spawn side_fnc_escape;
			} forEach [_intelGroup, _fake1Group, _fake2Group];
            _sleep = true;
        };
	} else {
        private _spotted = (_intel call BIS_fnc_enemyDetected);
		if !( _inVehicle ) then {
            if ( !_spotted && (alive _guard1) ) then { _spotted = (_guard1 call BIS_fnc_enemyDetected); };
            if ( !_spotted && (alive _guard2) ) then { _spotted = (_guard2 call BIS_fnc_enemyDetected); };
        };
		if ( _spotted || SIDE_intelFiredNear ) then {
            #ifdef DEBUG
            debug(LL_DEBUG, "intel: fleing start");
            #endif
			private _chat = ["ia", "side", "intel", "spotted"] call core_fnc_getSetting;
			[1, _chat, ["HQ", PLAYER_SIDE]] call global_fnc_chat;
			_chat = nil;
            if !( _inVehicle ) then {
	            if ( (alive _intelCar) && (canMove _intelCar) ) then {
					if ( alive _intelDriver ) then {
						_intel assignAsCargo _intelCar;
					} else {
	                    _intel assignAsDriver _intelCar;
	                };
                    _guard1 assignAsCargo _intelCar;
                    _guard2 assignAsCargo _intelCar;
	                [_intel, _guard1, _guard2] orderGetIn true;
                    #ifdef DEBUG
            		debug(LL_DEBUG, "intel: waiting for intelGroup to get in");
            		#endif
	                waitUntil {
                      sleep 1;
	                  ( ((vehicle _intel) isEqualTo _intelCar) || !(alive _intel) || !(alive _intelCar)  || !(canMove _intelCar) || SIDE_success || zeusMission )  
	                };
                    #ifdef DEBUG
            		debug(LL_DEBUG, "intel: waiting is done");
            		#endif
					if ( !(alive _intelCar) || !(canMove _intelCar) ) then {
                        #ifdef DEBUG
            			debug(LL_DEBUG, "intel: intelCar is disabled, ordering to get out.");
            			#endif 
                    	[_intel, _intelDriver, _guard1, _guard2] orderGetIn false; 
					};
	            };
			} else {
                if !( canMove _intelCar ) then {
                    #ifdef DEBUG
            		debug(LL_DEBUG, "intel: intelCar is disabled, ordering to get out.");
					#endif 
                    [_intel] orderGetIn false;
                    [_intelDriver] orderGetIn false;
                };
			};
            _sleep = false;
			_isFleing = true;
		};
	};
	if ( SIDE_success ) exitWith {
		private _chat = ["ia", "side", "intel", "secured"] call core_fnc_getSetting;
		[1, _chat, ["HQ", PLAYER_SIDE]] call global_fnc_chat;
		private _reward = call IA_fnc_giveReward;
		private _hint = ["ia", "side", "successHint"] call core_fnc_getSetting;
		format[_hint, _reward] call global_fnc_hint;
        [_taskId, "Succeeded", false, true] call tasks_fnc_serverSetState;
		[false, _flatPos, _size, _groups, [_intelCar, _fake1Car, _fake2Car]] spawn SIDE_fnc_cleanup;
	};
	if ( SIDE_stop || zeusMission || !isNull SIDE_switch  ) exitWith {
        [_taskId, "Canceled", false, false] call tasks_fnc_serverSetState;
		[true, _flatPos, _size, _groups, [_intelCar, _fake1Car, _fake2Car]] spawn SIDE_fnc_cleanup;
	};
    if ( _sleep ) then { sleep _checkDelay; };
};