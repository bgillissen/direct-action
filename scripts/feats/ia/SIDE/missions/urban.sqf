/*
@filename: feats\ia\SIDE\missions\urban.sqf
Credit:
	Quiksilver
Author:
	Ben
Description:
	this run on server,
	spawn by feats\ia\SIDE\serverPostInitTheard.sqf, 
	a crate to "activate" on a predefined marker in an urban area.
Params:
	none
Environment:
	missionNamespace:
		URBAN_MARKERS
		S_crates
		SIDE_stop
		zeusMission
	missionConfig:
		ia >> oa >> circle
		ia >> side >> research >> hqType
		ia >> side >> minDistFromBase
		ia >> side >> minDistFromAO
		ia >> side >> urban >> action
		ia >> side >> size
		ia >> side >> urban >> title
		ia >> side >> briefing
		ia >> side >> urban >> briefing
		ia >> checkDelay
		ia >> side >> urban >> planted
		ia >> side >> boomDelay
		ia >> side >> successHint
Return:
		nothing 
*/

if ( (count URBAN_markers) == 0 ) exitWith {};

if ( isNil "URBAN_pool" ) then { URBAN_pool = []; };
if ( (count URBAN_pool) == 0 ) then { URBAN_pool = URBAN_markers; };

private _baseCoord = getMarkerPos "BS_INF";
private "_coord";
private _blackList = SIDE_exclusionZones - ["water"];
if !( AO_coord isEqualTo [0,0,0] ) then {
    private _dist = (["ia", "side", "minDistFromAO"] call core_fnc_getSetting);
    _blackList pushback [AO_coord, _dist, _dist, 0, false, -1]; 
};
private _found = false;
private _tempPool = URBAN_pool + [];
 
while {( !_found && ((count _tempPool) > 0) )} do {
	private _marker = selectRandom _tempPool;
    _tempPool = _tempPool - [_marker];  
	_coord = getMarkerPos _marker;
    if ( ({( _coord inArea _x )} count _blacklist) isEqualTo 0 ) then {
		_found = true;
		URBAN_pool = URBAN_pool - [_marker];
	};
};

if !( _found ) exitWith {};

SIDE_coord = _coord + [];

_tempPool = nil;
_found = nil;

//objective crate
(["crates"] call ia_fnc_randomSide) params ["_side", "_pool", "_key"];
private _crate = (selectRandom _pool) createVehicle _coord;
[_crate, false] call curator_fnc_addEditable;

private _action = ["ia", "side", "urban", "action"] call core_fnc_getSetting;
SIDE_jip pushback ([_crate, _action] remoteExec ["SIDE_fnc_addAction", 0, true]);
_action= nil;

//spawn units
private _groups = [_coord, 15, 6, 2, [3, 25], 0, 0, 0, 0, 2, 0, 0, 0, (100 + random 200)] call SIDE_fnc_placeEnemies;

//markers, tasks
private _title = ["ia", "side", "urban", "title"] call core_fnc_getSetting;
private _size = ["ia", "side", "size"] call core_fnc_getSetting;
[_coord, _title, _size] call SIDE_fnc_placeMarkers;
private _desc = ["ia", "side", "urban", "briefing"] call core_fnc_getSetting;
private _taskId = (["SIDE", "rifle", _title, _desc, true] call tasks_fnc_serverAdd);
call tasks_fnc_serverExport;
_title = nil;
_desc = nil;

private _checkDelay = ["ia", "checkDelay"] call core_fnc_getSetting;

while { true } do {
    if !( alive _crate ) exitWith {
        [_taskId, "Failed", false, true] call tasks_fnc_serverSetState;
		[false, _coord, _size, _groups, [_crate]] spawn SIDE_fnc_cleanup;        
    };
	if ( SIDE_success ) exitWith {
		private _planted = ["ia", "side", "urban", "planted"] call core_fnc_getSetting;
		private _delay = ["ia", "side", "boomDelay"] call core_fnc_getSetting;
		[1, format[_planted, _delay], ["HQ", PLAYER_SIDE]] call global_fnc_chat;
		sleep _delay;
		[getPos _crate, true] spawn SIDE_fnc_boom;
		deleteVehicle _crate;
		private _reward = call IA_fnc_giveReward;
		private _hint = ["ia", "side", "successHint"] call core_fnc_getSetting;
		format[_hint, _reward] call global_fnc_hint;
        [_taskId, "Succeeded", false, true] call tasks_fnc_serverSetState;
		[false, _coord, _size, _groups, []] spawn SIDE_fnc_cleanup;
	};
	if ( SIDE_stop || zeusMission || !isNull SIDE_switch ) exitWith {
        [_taskId, "Canceled", false, false] call tasks_fnc_serverSetState;
		[true, _coord, _size, _groups, [_crate]] spawn SIDE_fnc_cleanup;
	};
	sleep _checkDelay;
};