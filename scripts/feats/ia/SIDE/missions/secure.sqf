/*
@filename: feats\ia\SIDE\misSecure.sqf
Credit:
	Quiksilver
Author:
	Ben
Description:
	run on server,
	spawn by feats\ia\SIDE\serverPostInitTheard.sqf,
	a radar or chopper to secure, we put a laptop to "activate" in a cargo house 
	and some maned guard tower around.
Params:
	none
Environment:
	missionNamespace:
		SIDE_stop
		SIDE_exclusionZones
		zeusMission
		S_airPatrol
	missionConfig:
		ia >> side >> minDistFromAO
		ia >> side >> secure >> radarProb
		ia >> side >> secure >> radar >> title
		ia >> side >> secure >> radar >> briefing
		ia >> side >> secure >> chopper >> title
		ia >> side >> secure >> chopper >> briefing
		ia >> side >> table
		ia >> side >> laptop
		ia >> side >> secure >> action
		ia >> side >> garrisonSkill
		ia >> side >> size
		ia >> side >> failHint
		ia >> side >> secure >> planted
		ia >> side >> boomDelay
		ia >> side >> successHint
Return:
		nothing
*/

private _cargoType = ["ia", "side", "secure", "cargoType"] call core_fnc_getSetting;
private "_flatPos";
private _found = false;
private _blackList = SIDE_exclusionZones + [];
if !( AO_coord isEqualTo [0,0,0] ) then {
	private _dist = (["ia", "side", "minDistFromAO"] call core_fnc_getSetting);
    //_blackList pushback [AO_coord, _dist, _dist, 0, false, -1];
    _blackList pushback [AO_coord, _dist];
};
//find a flat position
while { !_found } do {
	private _position = [nil, _blackList] call BIS_fnc_randomPos;
	_flatPos = _position isFlatEmpty [5, 1, 0.2, ((sizeOf _cargoType) * 4), 0, false];
	_found = !( _flatPos isEqualTo [] );
};
_found = nil;

SIDE_coord = _flatPos;

private _isRadar = (random 100 <= (["ia", "side", "secure", "radarProb"] call core_fnc_getSetting));
private ["_title", "_desc", "_pool"];
if ( _isRadar ) then {
	_title = ["ia", "side", "secure", "radar", "title"] call core_fnc_getSetting;
	_desc = ["ia", "side", "secure", "radar", "briefing"] call core_fnc_getSetting;
	 _pool = ["Land_Radar_Small_F"];
} else {
	_title = ["ia", "side", "secure", "chopper", "title"] call core_fnc_getSetting;
	_desc = ["ia", "side", "secure", "chopper", "briefing"] call core_fnc_getSetting;
	_pool = (["aPatrol"] call ia_fnc_randomSide) select 1;
};

//spawn objective
private _obj = (selectRandom _pool) createVehicle ([_flatPos, 25, 35, 10, 0, 0.5, 0] call BIS_fnc_findSafePos);
_obj setDir random 360;
if ( !_isRadar ) then {
	_obj lock 3;
};

//cargo house
private _cargo = _cargoType createVehicle _flatPos;
_cargo setDir random 360;
SIDE_jip pushback (_cargo remoteExec ["side_fnc_openDoors", 0, true]);
_cargoType = nil;

(getPos _cargo) params ["_cargoX", "_cargoY", "_cargoZ"];
_cargo setPos [_cargoX, _cargoY, (_cargoZ + 0.2)];

//objective table, laptop
private _tableType = ["ia", "side", "table"] call core_fnc_getSetting;
private _table = _tableType createVehicle [_cargoX, _cargoY, (_cargoZ + 5)];
_table setPosASL (AGLToASL (_cargo modelToWorld [0, 3.75, 0.03]));
_table setDir (getDir _cargo);
_table enableSimulationGlobal false;
_tableType = nil;

private _laptopType = selectRandom (["ia", "side", "laptop"] call core_fnc_getSetting);
private _laptop = _laptopType createVehicle [_cargoX, _cargoY, (_cargoZ + 15)];

_laptopType = nil;

[_table, _laptop, [0,0,0.80]] call BIS_fnc_relPosObject;
_laptop enableSimulationGlobal false;
_laptop setDir (getdir _table - 180);

private _action = ["ia", "side", "secure", "action"] call core_fnc_getSetting;
SIDE_jip pushback ([_table, _action] remoteExec ["SIDE_fnc_addAction", 0, true]);
_action = nil;

//watchTower
private _tower1 = "Land_Cargo_Patrol_V3_F" createVehicle ([_flatPos, 50, 0] call BIS_fnc_relPos);
_tower1 setDir 180;
private _tower2 = "Land_Cargo_Patrol_V3_F" createVehicle ([_flatPos, 50, 120] call BIS_fnc_relPos);
_tower2 setDir 300;
private _tower3 = "Land_Cargo_Patrol_V3_F" createVehicle ([_flatPos, 50, 240] call BIS_fnc_relPos);
_tower3 setDir 60;
_skill = nil;

private _groups = [];

//spawn units in watch towers
private _skill = ["ia", "side", "garrisonSkill"] call core_fnc_getSetting;
{
	_groups append [([_x, _skill, "secure"] call IA_fnc_forcedGarrison)];
	true
} count [_tower1, _tower2, _tower3];
_skill = nil;

//spawn patrols
private _size = ["ia", "side", "size"] call core_fnc_getSetting;
_groups append ([_flatPos, 0, 6, 2, [3,15], 2, 0, 0, 2, 3, 0, 0, 0, (_size + (random 100))] call SIDE_fnc_placeEnemies);

//markers, tasks
[_flatPos, _title, _size] call SIDE_fnc_placeMarkers;
private _taskId = (["SIDE", "download", _title, _desc, true] call tasks_fnc_serverAdd);
call tasks_fnc_serverExport;
_title = nil;
_desc = nil;

private _checkDelay = ["ia", "checkDelay"] call core_fnc_getSetting;

while { true } do {
	if ( !(alive _cargo) || !(alive _obj) ) exitWith {
        [_taskId, "Failed", false, true] call tasks_fnc_serverSetState;
		[false, _flatPos, _size, _groups, [_laptop, _table, _cargo, _obj, _tower1, _tower2, _tower3]] spawn SIDE_fnc_cleanup;
	};
	if ( SIDE_success ) exitWith {
		private _planted = ["ia", "side", "secure", "planted"] call core_fnc_getSetting;
		private _delay = ["ia", "side", "boomDelay"] call core_fnc_getSetting;
		[1, format[_planted, _delay], ["HQ", PLAYER_SIDE]] call global_fnc_chat;
		sleep _delay;
		(getPos _obj) params ["_objX", "_objY", "_objZ"];
		[[_cargoX, _cargoY, (_cargoZ+2)], false] spawn SIDE_fnc_boom;		
		[[_objX, _objY, (_objZ+15)], false] spawn SIDE_fnc_boom;
		deleteVehicle _laptop;
		deleteVehicle _table;
		private _reward = call IA_fnc_giveReward;
		private _hint = ["ia", "side", "successHint"] call core_fnc_getSetting;
		format[_hint, _reward] call global_fnc_hint;
        [_taskId, "Succeeded", false, true] call tasks_fnc_serverSetState;
		[false, _flatPos, _size, _groups, [_cargo, _obj, _tower1, _tower2, _tower3]] spawn SIDE_fnc_cleanup;
	};
	if ( SIDE_stop || zeusMission || !isNull SIDE_switch ) exitWith {
        [_taskId, "Canceled", false, false] call tasks_fnc_serverSetState;
		[true, _flatPos, _size, _groups, [_laptop, _table, _cargo, _obj, _tower1, _tower2, _tower3]] spawn SIDE_fnc_cleanup;
	};
	sleep _checkDelay;
};