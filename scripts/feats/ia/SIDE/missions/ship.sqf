/*
@filename: feats\ia\SIDE\missions\ship.sqf
Author:
	Ben
Description:
	run on server,
	spawn by feats\ia\SIDE\serverPostInitTheard.sqf, 
	a ship to destroy using an action
Params:
	none
Environment:
	missionNamespace:
		SHIP_LOCATIONS
		SIDE_stop
		zeusMission
	missionConfig:
		ia >> oa >> circle
		ia >> side >> research >> hqType
		ia >> side >> minDistFromBase
		ia >> side >> minDistFromAO
		ia >> side >> urban >> action
		ia >> side >> size
		ia >> side >> urban >> title
		ia >> side >> briefing
		ia >> side >> urban >> briefing
		ia >> checkDelay
		ia >> side >> urban >> planted
		ia >> side >> boomDelay
		ia >> side >> successHint
Return:
		nothing 
*/

if ( isNil "SHIP_locations" ) then { SHIP_locations = []; };
if ( (count SHIP_locations) == 0 ) then { 
	SHIP_locations = (nearestLocations [[0,0,0], ["nameMarine"], 20000]); 
};
private _blackList = SIDE_exclusionZones - ["water"];
if !( AO_coord isEqualTo [0,0,0] ) then {
    private _dist = (["ia", "side", "minDistFromAO"] call core_fnc_getSetting);
    _blackList pushback [AO_coord, _dist, _dist, 0, false, -1]; 
};

private _found = false;
private _tempPool = SHIP_locations + [];
private "_location";
private "_coord";
 
while {( !_found && ((count _tempPool) > 0) )} do {
	private _loc = selectRandom _tempPool;
    _tempPool = _tempPool - [_loc];  
	_coord = locationPosition _loc;
    if ( ({( _coord inArea _x )} count _blacklist) isEqualTo 0 ) then {
		_found = true;
		SHIP_locations = SHIP_locations - [_loc];
        _location = (name _loc);
	};
};

if !( _found ) exitWith {};

_tempPool = nil;
_found = nil;

SIDE_coord = _coord + [];

//objective ship
_found = false;
private _safePos = []; 
while { !_found } do {
	_safePos = ([_coord, 0, 400, (sizeOf "C_Boat_Civil_04_F"), 2] call BIS_fnc_findSafePos);
    _found = ( abs (getTerrainHeightASL _safePos) > 10 );
};

private _trawler = "C_Boat_Civil_04_F" createVehicle _safePos;
_trawler setDir random 360;
private _action = ["ia", "side", "ship", "action"] call core_fnc_getSetting;
SIDE_jip pushback ([_trawler, _action, 10] remoteExec ["SIDE_fnc_addAction", 0, true]);
_action =nil;

//spawn units
private _size = ["ia", "side", "size"] call core_fnc_getSetting;
private _groups = [(getPos _trawler), 0, 0, 0, [0, 0], 0, 0, 0, 0, 0, 2, 6, 2, _size] call SIDE_fnc_placeEnemies;

//markers, tasks
private _title = ["ia", "side", "ship", "title"] call core_fnc_getSetting; 
[_coord, _title, _size] call SIDE_fnc_placeMarkers;
private _desc = ["ia", "side", "ship", "briefing"] call core_fnc_getSetting;
private _taskId = (["SIDE", "boat", _title, _desc, true] call tasks_fnc_serverAdd);
call tasks_fnc_serverExport;
_title = nil;
_desc = nil;

private _checkDelay = ["ia", "checkDelay"] call core_fnc_getSetting; 

while { true } do {
	if (!alive _trawler) exitWith {
        [_taskId, "Failed", false, true] call tasks_fnc_serverSetState;
		[false, _coord, _size, _groups, [_trawler]] spawn SIDE_fnc_cleanup;
	};
	if ( SIDE_success ) exitWith {
		private _planted = ["ia", "side", "ship", "planted"] call core_fnc_getSetting;
		private _delay = ["ia", "side", "boomDelay"] call core_fnc_getSetting;
		[1, format[_planted, _delay], ["HQ", PLAYER_SIDE]] call global_fnc_chat;
		sleep _delay;
		[getPos _trawler, true] spawn SIDE_fnc_boom;
		[_trawler] spawn side_fnc_sinkBoat;
		private _reward = call IA_fnc_giveReward;
		private _hint = ["ia", "side", "successHint"] call core_fnc_getSetting;
		format[_hint, _reward] call global_fnc_hint;
        [_taskId, "Succeeded", false, true] call tasks_fnc_serverSetState;
		[false, _coord, _size, _groups, [_trawler]] spawn SIDE_fnc_cleanup;
	};
	if ( SIDE_stop || zeusMission || !isNull SIDE_switch ) exitWith {
        [_taskId, "Canceled", false, false] call tasks_fnc_serverSetState;
		[true, _coord, _size, _groups, [_trawler]] spawn SIDE_fnc_cleanup;
	};
	sleep _checkDelay;
};



