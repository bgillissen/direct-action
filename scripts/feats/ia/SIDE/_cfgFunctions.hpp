class SIDE {
	tag = "SIDE";
	class functions {
		class playerPostInit { file="feats\ia\SIDE\playerPostInit.sqf"; };
		class serverPostInit { file="feats\ia\SIDE\serverPostInit.sqf"; };
		class serverDestroy { file="feats\ia\SIDE\serverDestroy.sqf"; };
		class aaFire { file="feats\ia\SIDE\aaFire.sqf"; };
		class artiFire { file="feats\ia\SIDE\artiFire.sqf"; };
		class addAction { file="feats\ia\SIDE\addAction.sqf"; };
		class delAction { file="feats\ia\SIDE\delAction.sqf"; };
		class doAction { file="feats\ia\SIDE\doAction.sqf"; };
		class escape { file="feats\ia\SIDE\escape.sqf"; };
		class boom { file="feats\ia\SIDE\boom.sqf"; };
		class cleanup { file="feats\ia\SIDE\cleanup.sqf"; };
		class openDoors { file="feats\ia\SIDE\openDoors.sqf"; };
		class placeMarkers { file="feats\ia\SIDE\placeMarkers.sqf"; };
		class placeEnemies { file="feats\ia\SIDE\placeEnemies.sqf"; };
		class sinkBoat { file="feats\ia\SIDE\sinkBoat.sqf"; };

		class blockade { file="feats\ia\SIDE\missions\blockade.sqf"; };
		class hostage { file="feats\ia\SIDE\missions\hostage.sqf"; };
		class hqCoast { file="feats\ia\SIDE\missions\hqCoast.sqf"; };
		class intel { file="feats\ia\SIDE\missions\intel.sqf"; };
		class priority { file="feats\ia\SIDE\missions\priority.sqf"; };
		class research { file="feats\ia\SIDE\missions\research.sqf"; };
		class secure { file="feats\ia\SIDE\missions\secure.sqf"; };
		class ship { file="feats\ia\SIDE\missions\ship.sqf"; };
		class urban { file="feats\ia\SIDE\missions\urban.sqf"; };
	};
};
