/*
@filename: feats\ia\SIDE\playerPostInit.sqf
Author:
	Ben
Description:
	run on player,
	add the action to switch active side on base things. 
*/

#include "_debug.hpp"

if ( (["sideMission"] call core_fnc_getParam) == 0 ) exitWith {
	#ifdef DEBUG
    debug(LL_INFO, "disabled by mission parameter");
    #endif 
    nil    
};

private _rank = ["ia", "side", "switchRank"] call core_fnc_getSetting;

if ( (player getVariable ['MD_rank', 0]) < _rank ) exitWith { nil };

{
	{
		_x params ["_thing", "_actions"];
		{
			if ( (configName _x) isEqualTo "switchSideMission" ) then {
                #ifdef DEBUG
                private _debug = format["adding action to %1", _thing];
    			debug(LL_DEBUG, _debug);
    			#endif
				_thing addAction ["Switch Side Mission", {missionNamespace setVariable['SIDE_switch', player, true]}, "", 0, false, true, "", "( isNull SIDE_switch && !zeusMission )", 4];
			};
		} forEach _actions;
	} forEach _x;
} forEach [BA_obj, BA_npc, BA_veh];

nil