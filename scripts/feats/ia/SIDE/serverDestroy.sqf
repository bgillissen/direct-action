/*
@filename: feats\ia\AO\serverDestroy.sqf
Author:
	Ben
Description:
	run on server side
	tell main AO to stop, wait some, kill it 
*/

if ( count(_this) == 0 ) exitWith { nil };

params ["_when", "_thread"];

if ( (["sideMission"] call core_fnc_getParam) == 0 ) exitWith { nil };

SIDE_stop = true;

if !( isNil "SIDE_main" ) then {
    private _limit = time + 20;
	waitUntil { (( time > _limit) || (scriptDone SIDE_main) ) };
    terminate SIDE_main;
};

terminate _thread;

waitUntil{ scriptDone _thread };

{
    remoteExec["", _x];
    SIDE_jip deleteAt _forEachIndex;
} forEach SIDE_jip;

nil