
#include "_debug.hpp"

_doorCount = getNumber (configFile >> "CfgVehicles" >> (typeOf _this) >> "numberOfDoors");

#ifdef DEBUG
private _debug = format["opening %1 doors on %2", _doorCount, _this];
debug(LL_INFO, _debug);
#endif

for "_i" from 0 to _doorCount do {
	_this animate [format["Door_%1_rot", _i], 1];
};