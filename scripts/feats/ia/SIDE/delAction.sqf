/*
@filename: feats\ia\SIDE\delAction.sqf
Author:
	Ben
Description:
	run on player,
	add the action to finish a side mission on the given object (JIP)
*/

if !( CTXT_PLAYER ) exitWith {};

if ( isNil "SIDE_action" ) exitWith {};

SIDE_action params [["_obj", objNull], ["_id", -1]];

#ifdef DEBUG
private _debug = format["removing action on %1, id %2", _obj, _id];
debug(LL_DEBUG, _debug);
#endif

if !( isNull _obj ) then { _obj removeAction _id; };

SIDE_action = [];