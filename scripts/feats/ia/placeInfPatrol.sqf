/*
@filename: feats\ia\placeInfPatrol.sqf
Credit:
	Quiksilver
Author:
	Ben
Description:
	run server side,
	it spawn Infantry Patrol inside the given coordonate 
*/

#include "_debug.hpp"

params ["_coord", "_size", "_amount", "_patrolSize", "_skill", ["_ctxt", "unknow"]];

if ( _amount <= 0 ) exitWith {[]};

#ifdef DEBUG
private _debug = format["%1 | placing %2 infantry patrols", _ctxt, _amount];
debug(LL_DEBUG, _debug);
#endif

private _groups = [];

private _baseCfg = ["configFile", "CfgGroups"];

for "_x" from 1 to _amount do {
	(["pGroups"] call ia_fnc_randomSide) params ["_side", "_pool"];
	if ( isNil "_side" ) exitWith { [grpNull] };
	private _infGroup = (_baseCfg + (selectRandom _pool));
	private _cfgPath = [_infGroup] call BIS_fnc_configPath;
	if ( isClass(_cfgPath) ) then {
		private _class = selectRandom (_cfgPath call Bis_fnc_getCfgSubClasses);
		if ( isClass(_cfgPath >> _class) ) then {
			private _randomPos = [[[_coord, (_size * 0.8)]], ["water"]] call BIS_fnc_randomPos;
			private _group = [_randomPos, _side, (_cfgPath >> _class)] call BIS_fnc_spawnGroup;
			[_group, true] call dynSim_fnc_set;
            _group setGroupIdGlobal [([(format["%1 - infPatrol", _ctxt]), allGroups, {groupId _this}] call common_fnc_getUniqueName)];
			private _bool = [_group, _randomPos, _patrolSize, [["water"]]] call BIS_fnc_taskPatrol;
			[(units _group), _skill] call common_fnc_setSkill;
			[(units _group), false] call curator_fnc_addEditable;
			_groups pushback _group;
			
		};
	#ifdef DEBUG
	} else {
		private _debug = format["%1 | Invalid infantry group: %2", _ctxt, _cfgPath];
        debug(LL_ERR, _debug);
	#endif
	};
};

_groups