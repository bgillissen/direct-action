#include "..\..\core\debugLevels.hpp"

//comment the following line to disable common ia functions debug
#define DEBUG()

//debug level for this feature
#define DEBUG_LVL LL_INFO
//debug context
#define DEBUG_CTXT "ia"

#include "__debug.hpp"
