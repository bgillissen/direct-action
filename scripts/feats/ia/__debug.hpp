#include "..\..\core\debugLevels.hpp"

//comment the following line to disable all ia feature debug
#define DEBUG_IA

//default ia feature debug level
#ifndef DEBUG_LVL
#define DEBUG_LVL LL_INFO
#endif

#ifndef DEBUG_IA
#undef DEBUG
#endif

#include "..\__debug.hpp"
