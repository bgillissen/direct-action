/*
@filename: feats\ia\placeGarrison.sqf
Credit:
	Quiksilver
Author:
	Ben
Description:
	this script is executed on server side,
	it spawn units and garrison them inside the given coordonate 
*/

params ["_coord", "_size", "_amount", "_skill", "_canPatrol", ["_ctxt", "unknow"]];

if !( _amount isEqualType [] ) then { _amount = [_amount]; };

_amount params ["_byBuilding", ["_maxUnits", 20]];

if ( _byBuilding <= 0 ) exitWith {
    #ifdef DEBUG
	debug(LL_DEBUG, "placeGarrison, byBuilding is 0, abording");
	#endif 
	[] 
};

(["garrison"] call ia_fnc_randomSide) params ["_side", "_pool"];

if ( isNil "_side" ) exitWith { 
	#ifdef DEBUG
	private _debug = format["%1 | garrisoned units failed, no side could be determined for garrison spawn pool, probably empty", _ctxt];
	debug(LL_ERR, _debug);
	#endif
	[] 
};

private _buildings = _coord nearObjects ["House", _size];

if ( count _buildings <= 0 ) exitWith {
    #ifdef DEBUG
	debug(LL_DEBUG, "placeGarrison, no building in radius");
	#endif
	[] 
};

private _groups = [];
private _totUnits = 0;

while { true } do {
    if ( _totUnits >= _maxUnits ) exitWith {};
    private _positions = [];
    private _amount = floor random [1, (_byBuilding / 2), _byBuilding];
    while { ( ((count _positions) isEqualTo 0) && ((count _buildings) > 0) ) } do { 
    	private _building = selectRandom _buildings;
    	_positions = [_building, _amount] call BIS_fnc_buildingPositions;
        _buildings = _buildings - [_building];
	};
    if ( _positions isEqualTo [] ) exitWith {
    	#ifdef DEBUG
		debug(LL_DEBUG, "placeGarrison, no more building with positions available");
		#endif    
    };
	private _group = createGroup [_side, true];
    _group setGroupIdGlobal [([(format["%1 - garrison", _ctxt]), allGroups, {groupId _this}] call common_fnc_getUniqueName)];
    _totUnits = _totUnits + (count _positions);
    { (selectRandom _pool) createUnit [_x, _group]; } forEach _positions;
    {
        _x setPosATL (_positions select _forEachIndex);
        _x setVariable ["NOAI", true, true];
        [_x, _skill] call common_fnc_setSkill;
		[_x, false] call curator_fnc_addEditable;
    } forEach (units _group);
    [_group, true] call dynSim_fnc_set;
	_groups pushback _group;             
};

#ifdef DEBUG
private _debug = format["%1 | place garrison, %2 group(s) of %3 units placed.", _ctxt, (count _groups), _byBuilding];
debug(LL_DEBUG, _debug);
#endif

_groups