
#include "define.hpp"

class acGridTitle : daText {
	style = ST_CENTER;
	x = (TOT_WIDTH / 2) - BUTTON_WIDTH;
	y = SPACE;
	h = LINE_HEIGHT;
	w = (BUTTON_WIDTH * 2);
	text = "Target's location";
};

class acGridContainer : daContainer {
	x = (TOT_WIDTH / 2) - (((3 * BUTTON_WIDTH) + (4 * SPACE)) / 2);
	y = LINE_HEIGHT + (2 * SPACE);
	h = (2 * LINE_HEIGHT) + (3 * SPACE);
	w = (3 * BUTTON_WIDTH) + (4 * SPACE);
};

class acGridXTitle : daText {
	style = ST_CENTER;
	x = (TOT_WIDTH / 2) - (((3 * BUTTON_WIDTH) + (4 * SPACE)) / 2) + SPACE;
	y = LINE_HEIGHT + (3 * SPACE);
	h = LINE_HEIGHT;
	w = BUTTON_WIDTH;
	text = "Latitude";
};

class acGridX : daEdit {
	idc = GRIDX_IDC;
	x = (TOT_WIDTH / 2) - (((3 * BUTTON_WIDTH) + (4 * SPACE)) / 2) + SPACE;
	y = (2 * LINE_HEIGHT) + (4 * SPACE);
	h = LINE_HEIGHT;
	w = BUTTON_WIDTH;
	tooltip = "5 digits max";
	onKillFocus = "_this call artiComputer_fnc_evtGridChange";
};

class acGridYTitle : daText {
	style = ST_CENTER;
	x = (TOT_WIDTH / 2) - (((3 * BUTTON_WIDTH) + (4 * SPACE)) / 2) + (2 * SPACE) + BUTTON_WIDTH;
	y = LINE_HEIGHT + (3 * SPACE);
	h = LINE_HEIGHT;
	w = BUTTON_WIDTH;
	text = "Longitude";
};

class acGridY : daEdit {
	idc = GRIDY_IDC;
	x = (TOT_WIDTH / 2) - (((3 * BUTTON_WIDTH) + (4 * SPACE)) / 2) + (2 * SPACE) + BUTTON_WIDTH;
	y = (2 * LINE_HEIGHT) + (4 * SPACE);
	h = LINE_HEIGHT;
	w = BUTTON_WIDTH;
	tooltip = "5 digits max";
 	onKillFocus = "_this call artiComputer_fnc_evtGridChange";
};

class acGridZTitle : daText {
	style = ST_CENTER;
	x = (TOT_WIDTH / 2) - (((3 * BUTTON_WIDTH) + (4 * SPACE)) / 2) + (3 * SPACE) + (2 * BUTTON_WIDTH);
	y = LINE_HEIGHT + (3 * SPACE);
	h = LINE_HEIGHT;
	w = BUTTON_WIDTH;
	text = "Altitude";
};

class acGridZ : daEdit {
	idc = GRIDZ_IDC;
	x = (TOT_WIDTH / 2) - (((3 * BUTTON_WIDTH) + (4 * SPACE)) / 2) + (3 * SPACE) + (2 * BUTTON_WIDTH);
	y = (2 * LINE_HEIGHT) + (4 * SPACE);
	h = LINE_HEIGHT;
	w = BUTTON_WIDTH;
	onKillFocus = "_this call artiComputer_fnc_evtGridChange";
};

class acCorTitle : daText {
	style = ST_CENTER;
	x = (TOT_WIDTH / 2) - BUTTON_WIDTH;
	y = ((2 + COR_LINE_OFFSET) * LINE_HEIGHT) + (10 * SPACE);
	h = LINE_HEIGHT;
	w = (BUTTON_WIDTH * 2);
	text = "Corrections (in meters)";
};

class acCorContainer : daContainer {
	x = (TOT_WIDTH / 2) - (((3.4 * BUTTON_WIDTH) + (6 * SPACE)) / 2);
	y = ((3 + COR_LINE_OFFSET) * LINE_HEIGHT) + (11 * SPACE);
	h = (2 * LINE_HEIGHT) + (2 * SPACE);
	w = (3.4 * BUTTON_WIDTH) + (6 * SPACE);
};

class acCorXTitle : daText {
	style = ST_CENTER;
	text = "Latitude";
	x = (TOT_WIDTH / 2) - (((3.4 * BUTTON_WIDTH) + (6 * SPACE)) / 2) + SPACE + ((((1.7 * BUTTON_WIDTH) + SPACE)) / 2) - (BUTTON_WIDTH / 2);
	y = ((3 + COR_LINE_OFFSET) * LINE_HEIGHT) + (11 * SPACE);
	h = LINE_HEIGHT;
	w = BUTTON_WIDTH;
};

class acCorXWay : daCombo {
	idc = XWAY_IDC;
	x = (TOT_WIDTH / 2) - (((3.4 * BUTTON_WIDTH) + (6 * SPACE)) / 2) + SPACE;
	y = ((4 + COR_LINE_OFFSET) * LINE_HEIGHT) + (12 * SPACE);
	h = LINE_HEIGHT;
	w = (BUTTON_WIDTH * 0.7);
	onLBSelChanged = "_this call artiComputer_fnc_evtWayChange";
};

class acCorX : daEdit {
	idc = CORX_IDC;
	x = (TOT_WIDTH / 2) - (((3.4 * BUTTON_WIDTH) + (6 * SPACE)) / 2) + (BUTTON_WIDTH * 0.7) + (2 * SPACE);
	y = ((4 + COR_LINE_OFFSET) * LINE_HEIGHT) + (12 * SPACE);
	h = LINE_HEIGHT;
	w = BUTTON_WIDTH;
	onKillFocus = "_this call artiComputer_fnc_evtCorChange";
};

class acCorYTitle : daText {
	style = ST_CENTER;
	text = "Longitude";
	x = (TOT_WIDTH / 2) - (((3.4 * BUTTON_WIDTH) + (6 * SPACE)) / 2) + (1.7 * BUTTON_WIDTH) + (4 * SPACE) + ((((1.7 * BUTTON_WIDTH) + SPACE)) / 2) - (BUTTON_WIDTH / 2);
	y = ((3 + COR_LINE_OFFSET) * LINE_HEIGHT) + (11 * SPACE);
	h = LINE_HEIGHT;
	w = BUTTON_WIDTH;
};

class acCorYWay : daCombo {
	idc = YWAY_IDC;
	x = (TOT_WIDTH / 2) - (((3.4 * BUTTON_WIDTH) + (6 * SPACE)) / 2) + (1.7 * BUTTON_WIDTH) + (4 * SPACE);
	y = ((4 + COR_LINE_OFFSET) * LINE_HEIGHT) + (12 * SPACE);
	h = LINE_HEIGHT;
	w = (BUTTON_WIDTH * 0.7);
	onLBSelChanged = "_this call artiComputer_fnc_evtWayChange";
};

class acCorY : daEdit {
	idc = CORY_IDC;
	x = (TOT_WIDTH / 2) - (((3.4 * BUTTON_WIDTH) + (6 * SPACE)) / 2) + (2.4 * BUTTON_WIDTH) + (5 * SPACE);
	y = ((4 + COR_LINE_OFFSET) * LINE_HEIGHT) + (12 * SPACE);
	h = LINE_HEIGHT;
	w = BUTTON_WIDTH;
	onKillFocus = "_this call artiComputer_fnc_evtCorChange";
};

class acConfTitle : daText {
	style = ST_CENTER;
	text = "Shot Settings";
	x = (TOT_WIDTH / 2) - (BUTTON_WIDTH / 2);
	y = ((5 + COR_LINE_OFFSET + CONF_LINE_OFFSET) * LINE_HEIGHT) + (13 * SPACE);
	h = LINE_HEIGHT;
	w = BUTTON_WIDTH;
};

class acConfContainer : daContainer {
	x = (TOT_WIDTH / 2) - (2 * BUTTON_WIDTH) - (2 * SPACE);
	y = ((6 + COR_LINE_OFFSET + CONF_LINE_OFFSET) * LINE_HEIGHT) + (14 * SPACE);
	h = (2 * LINE_HEIGHT) + (2 * SPACE);
	w = (4 * BUTTON_WIDTH) + (4 * SPACE);
};

class acModeTitle : daText {
	style = ST_CENTER;
	text = "Mode";
	x = (TOT_WIDTH / 2) - (((2 * BUTTON_WIDTH) - SPACE) / 2) - (BUTTON_WIDTH / 2);
	y = ((6 + COR_LINE_OFFSET + CONF_LINE_OFFSET) * LINE_HEIGHT) + (14 * SPACE);
	h = LINE_HEIGHT;
	w = BUTTON_WIDTH;
};

class acMode : daCombo {
	idc = MODE_IDC;
	x = (TOT_WIDTH / 2) - (2 * BUTTON_WIDTH) - SPACE;
	y = ((7 + COR_LINE_OFFSET + CONF_LINE_OFFSET) * LINE_HEIGHT) + (15 * SPACE);
	h = LINE_HEIGHT;
	w = (2 * BUTTON_WIDTH);
	onLBSelChanged = "_this call artiComputer_fnc_evtModeChange";
};

class acAngleTitle : daText {
	style = ST_CENTER;
	text = "Angle";
	x = (TOT_WIDTH / 2);
	y = ((6 + COR_LINE_OFFSET + CONF_LINE_OFFSET) * LINE_HEIGHT) + (14 * SPACE);
	h = LINE_HEIGHT;
	w = BUTTON_WIDTH;
};

class acAngle : daCombo {
	idc = ANGLE_IDC;
	x = (TOT_WIDTH / 2);
	y = ((7 + COR_LINE_OFFSET + CONF_LINE_OFFSET) * LINE_HEIGHT) + (15 * SPACE);
	h = LINE_HEIGHT;
	w = BUTTON_WIDTH;
	onLBSelChanged = "_this call artiComputer_fnc_evtAngleChange";
};

class acFuseTitle : daText {
	style = ST_CENTER;
	text = "Fuse";
	x = (TOT_WIDTH / 2) + BUTTON_WIDTH + SPACE;
	y = ((6 + COR_LINE_OFFSET + CONF_LINE_OFFSET) * LINE_HEIGHT) + (14 * SPACE);
	h = LINE_HEIGHT;
	w = BUTTON_WIDTH;
};

class acFuse : daCombo {
	idc = FUSE_IDC;
	x = (TOT_WIDTH / 2) + BUTTON_WIDTH + SPACE;
	y = ((7 + COR_LINE_OFFSET + CONF_LINE_OFFSET) * LINE_HEIGHT) + (15 * SPACE);
	h = LINE_HEIGHT;
	w = BUTTON_WIDTH;
	onLBSelChanged = "_this call artiComputer_fnc_evtFuseChange";
};

//---------------------- GUNNER INFO BELLOW

class acInfoTitle : daText {
	style = ST_CENTER;
	w = 3 * ENTRY_WIDTH;
	h = ENTRY_HEIGHT;
	SizeEx = ENTRY_FONTSIZE;
	colorText[] = {1, 1, 1, 1};
};
class acInfoDesc : daText {
	style = ST_CENTER;
	w = ENTRY_WIDTH;
	h = ENTRY_HEIGHT;
	SizeEx = ENTRY_FONTSIZE * 0.7;
	colorText[] = {1, 1, 1, 0.6};
};
class acInfoEntry : daText {
	style = ST_CENTER;
	w = ENTRY_WIDTH;
	h = ENTRY_HEIGHT;
	SizeEx = ENTRY_FONTSIZE * 0.85;
	colorText[] = {1, 1, 1, 0.6};
};
class acInfoDyn : daText {
	style = ST_CENTER;
	w = ENTRY_WIDTH;
	h = ENTRY_HEIGHT;
	SizeEx = ENTRY_FONTSIZE * 0.85;
	colorText[] = COL_USR_BCG;
};


class acShellEntry : daControlGroup {
	w = 3 * ENTRY_WIDTH;
	h = 1.5 * ENTRY_HEIGHT;
	class VScrollbar {
		width = 0;
		autoScrollSpeed = -1;
		autoScrollDelay = 5;
		autoScrollRewind = 0;
		autoScrollEnabled = 0;
		scrollSpeed = 0.06;
		#include "..\..\common\ui\_scrollbar.hpp"
	};
	class HScrollbar {
		height = 0;
		autoScrollSpeed = -1;
		autoScrollDelay = 5;
		autoScrollRewind = 0;
		autoScrollEnabled = 0;
		scrollSpeed = 0.06;
		#include "..\..\common\ui\_scrollbar.hpp"
	};
	class Controls {
		class ammo : daText {
			idc = SHELL_AMMO_IDC;
			SizeEx = ENTRY_FONTSIZE * 0.85;
			colorText[] = {1, 1, 1, 0.6};
			h = ENTRY_HEIGHT;
			w = (2 * ENTRY_WIDTH);
		};
		class time : daText {
			style = ST_RIGHT;
			idc = SHELL_TIME_IDC;
			SizeEx = ENTRY_FONTSIZE * 0.85;
			colorText[] = {1, 1, 1, 1};
			x = 2 * ENTRY_WIDTH;
			w = ENTRY_WIDTH;
			h = ENTRY_HEIGHT;
		};
		class prgb : daContainer {
			idc = SHELL_PRGB_IDC;
			y = ENTRY_HEIGHT;
			h = ENTRY_HEIGHT / 2;
			colorBackground[] = COL_USR_BCG;
		};
	};
};
