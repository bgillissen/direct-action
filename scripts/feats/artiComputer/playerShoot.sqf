/*
@filename: feats\artiSupport\playerShoot.sqf
Author:
	Ben
Description:
	run on player,
*/

#include "_debug.hpp"

if ( (["artiSupport"] call core_fnc_getParam) isEqualTo 0 ) exitWith {
	#ifdef DEBUG
   	debug(LL_INFO, "disabled by mission parameter");
    #endif
    nil  
};

params ["_unit", "_weapon", "_muzzle", "_mode", "_ammo", "_mag", "_shell", "_veh"];

if ( (artiComputer_solution isEqualTo []) || (isNull _veh) ) exitWith {};

artiComputer_solution params ["_bearings", "_angles", "_solCharge", "_mag", "_distance", "_altDiff", "_eta"];

artiComputer_shells pushback [_mag, _shell, time, _eta];

if !( artiComputer_fuse isEqualTo 0 ) then {
    private _fuse = ((["artiComputer","fuseAlt"] call core_fnc_getSetting) select artiComputer_fuse);
	[_veh, _shell, _altDiff, _fuse] spawn {
        _this params ["_veh", "_shell", "_altDiff", "_fuse"];
		private _alt = ((getPosASL _veh) select 2) + _altDiff + _fuse;
    	private _shellAlt = _alt;
		private _safe = false;
		while { alive _shell } do {
	    	_shellAlt = (getPosASL _shell) select 2;
	    	if ( _shellAlt > _alt && !_safe ) then { _safe = true; };
	    	if ( (_shellAlt <= _alt) && _safe ) exitWith {
            	_shell setDamage 1; 
            	systemChat format["Fuse Detonation @ %1m", _shellAlt]; 
			}; 
		};
	};
};

nil