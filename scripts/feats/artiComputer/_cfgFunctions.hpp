class artiComputer {
	tag = "artiComputer";
	class functions {
		class playerInit { file="feats\artiComputer\playerInit.sqf"; };
		class playerDestroy { file="feats\artiComputer\playerDestroy.sqf"; };
		class playerGetIn { file="feats\artiComputer\playerGetIn.sqf"; };
		class playerGetOut { file="feats\artiComputer\playerGetOut.sqf"; };
		class playerSwitchSeat { file="feats\artiComputer\playerSwitchSeat.sqf"; };
		class playerShoot { file="feats\artiComputer\playerShoot.sqf";  };
	};
};

class acComputer {
	tag = "acComputer";
	class functions {
		class init { file="feats\artiComputer\ui\computer\init.sqf"; };
		class update { file="feats\artiComputer\ui\computer\update.sqf"; };
		class destroy { file="feats\artiComputer\ui\computer\destroy.sqf"; };
		class evtAbord { file="feats\artiComputer\ui\computer\events\abord.sqf"; };
		class evtAngle { file="feats\artiComputer\ui\computer\events\angle.sqf"; };
		class evtCompute { file="feats\artiComputer\ui\computer\events\compute.sqf"; };
		class evtCorrection { file="feats\artiComputer\ui\computer\events\correction.sqf"; };
		class evtGrid { file="feats\artiComputer\ui\computer\events\grid.sqf"; };
		class evtMag { file="feats\artiComputer\ui\computer\events\mag.sqf"; };
		class evtMode { file="feats\artiComputer\ui\computer\events\mode.sqf"; };
		class evtWay { file="feats\artiComputer\ui\computer\events\way.sqf"; };
	};
};

class acGunner {
	tag = "acGunner";
	class functions {
		class init { file="feats\artiComputer\ui\gunner\init.sqf"; };
		class thread { file="feats\artiComputer\ui\gunner\thread.sqf"; };
		class updateState { file="feats\artiComputer\ui\gunner\updateState.sqf"; };
		class updateSolution { file="feats\artiComputer\ui\gunner\updateSolution.sqf"; };
		class updateShell { file="feats\artiComputer\ui\gunner\updateShell.sqf"; };
		class buildShell { file="feats\artiComputer\ui\gunner\buildShell.sqf"; };
	};
};

class acDriver {
	tag = "acDriver";
	class functions {
		class init { file="feats\artiComputer\ui\driver\init.sqf"; };
		class thread { file="feats\artiComputer\ui\driver\thread.sqf"; };
	};
};
