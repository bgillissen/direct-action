/*
@filename: feats\artiComputer\playerGetIn.sqf
Author:
	Ben
Description:
	run on player,
	called when player enter a vehicle.
	add the action to open the arti computer
*/

#include "_debug.hpp"

if ( (["artiComputer"] call core_fnc_getParam) isEqualTo 0 ) exitWith {
	#ifdef DEBUG
   	debug(LL_DEBUG, "disabled by mission parameter");
    #endif
    nil  
};

params ["_unit", "_role", "_veh", ["_turretPath", []]];

if !( [_unit] call arti_fnc_isArtillery ) exitWith { nil };

if ( _role in ["driver", "Driver"] ) exitWith {
	"acDriver" cutRsc ["acDriver", "PLAIN", 0.01, false];
};

artiComputer_action = (_unit addAction ["Artillery Computer (WIP)", {createDialog "acComputer"}, true, 0, false, true, "", "true", 4]);

"acGunner" cutRsc ["acGunner", "PLAIN", 0.01, false];

nil