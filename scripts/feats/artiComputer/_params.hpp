/*
@filename: feats\artiComputer\_params.hpp
Author:
	Ben
Description:
		included by feats\_params.hpp
*/

class artiComputer {
	title = "Manual Artillery Computer (replace Bohemia's click and kill)";
	values[] = {0, 1};
	texts[] = {"Disabled", "Enabled"};
	default = 1;
};
