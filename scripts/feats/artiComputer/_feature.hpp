class artiComputer : feat_base  {
	class player : featPlayer_base {
		class init : featEvent_enable {};
		class getIn : featEvent_enable {};
		class getOut : featEvent_enable {};
		class shoot : featEvent_enable {};
		class switchSeat : featEvent_enable {};
		class destroy : featEvent_enable {};
	};
};
