/*
@filename: feats\artiComputer\playerSwitchSeat.sqf
Author:
	Ben
Description:
	run on player,
	called when player enter a vehicle.
*/

#include "_debug.hpp"

if ( (["artiComputer"] call core_fnc_getParam) isEqualTo 0 ) exitWith {
	#ifdef DEBUG
   	debug(LL_DEBUG, "disabled by mission parameter");
    #endif
    nil  
};

params ["_unit", "_unit2", "_veh"];

if !( [_unit] call arti_fnc_isArtillery ) exitWith { nil };
    
call artiComputer_fnc_playerGetOut;

(assignedVehicleRole _unit) params ["_role", ["_turretPath", []]];

[_unit, _role, _veh, _turretPath] call artiComputer_fnc_playerGetIn;

nil