/*
@filename: feats\artiComputer\playerDestroy.sqf
Author:
	Ben
Description:
	run on player,
*/

#include "_debug.hpp"

if ( (["artiComputer"] call core_fnc_getParam) isEqualTo 0 ) exitWith {
	#ifdef DEBUG
   	debug(LL_DEBUG, "disabled by mission parameter");
    #endif
    nil  
};

player removeAction artiComputer_action;
   
terminate artiComputer_infoThread;

nil