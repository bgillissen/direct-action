/*
@filename: feats\artiSupport\playerInit.sqf
Author:
	Ben
Description:
	run on player,
*/

#include "_debug.hpp"

if ( (["artiComputer"] call core_fnc_getParam) isEqualTo 0 ) exitWith {
	#ifdef DEBUG
   	debug(LL_INFO, "disabled by mission parameter");
    #endif
    nil  
};

#ifdef DEBUG
if ( DEBUG_LVL isEqualTo LL_DEBUG ) then {
	artiComputer_trgtArrow = "Sign_Arrow_Large_F" createVehicle [0,0,0];
};
#endif

artiComputer_uiNoEvents = false;
artiComputer_grid = ["", "", ""];
artiComputer_correction = [0, "", 0, ""];
artiComputer_mode = "";
artiComputer_charge = 0;
artiComputer_highAngle = true;
artiComputer_vType = "";
artiComputer_mag = "";
artiComputer_magIdx = -1;
artiComputer_solution = [];
artiComputer_shells = [];
artiComputer_action = -1;
artiComputer_abord = false;
artiComputer_computeThread = scriptNull;
artiComputer_driverThread = scriptNull;
artiComputer_gunnerThread = scriptNull;

nil