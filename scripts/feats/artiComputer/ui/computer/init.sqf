
#include "define.hpp"

disableSerialization;

waitUntil { !(isNull (findDisplay AC_IDD)) };

private _dsp = findDisplay AC_IDD;
private _grp = _dsp displayCtrl TARGET_IDC;

private _xWay = (_grp controlsGroupCtrl XWAY_IDC);
_xWay lbAdd "East";
_xWay lbAdd "West";

private _yWay = (_grp controlsGroupCtrl YWAY_IDC);
_yWay lbAdd "North";
_yWay lbAdd "South";

private _angle = (_grp controlsGroupCtrl ANGLE_IDC);
(assignedVehicleRole player) params[["_role", ""], ["_turretPath", []]];
private _veh = (vehicle player);
([_veh, _turretPath] call arti_fnc_getTurretLimits) params ["_minElev", "_maxElev"];
if ( _minElev < 45 ) then {
	_angle lbAdd "Low (< 45�)";
} else {
	if !( artiComputer_highAngle ) then { artiComputer_highAngle = true; };
};
_angle lbAdd "High (> 45�)";

private _magCtrl = (_grp controlsGroupCtrl MAG_IDC);
private _mags = [vehicle player] call arti_fnc_getMags;
private _vType = typeOf (vehicle player);
if !( _vType isEqualTo artiComputer_vType ) then {
	private _weapon = [player] call arti_fnc_getWeapon;
	(weaponState [_veh, _turretPath, _weapon]) params ["_weapon", "_muzzle", "_fireMode", "_mag", "_ammoCount"];
	artiComputer_vType = _vType; 
	artiComputer_magIdx = (_mags find _mag); 
	artiComputer_mag = _mag;
};
{
	_magCtrl lbAdd getText(configFile >> "cfgMagazines" >> _x >> "displayName");
	_magCtrl lbSetData [_forEachIndex, _x];
} forEach _mags;

[_dsp] call acComputer_fnc_update;

_dsp displayAddEventHandler ["keyDown", "_this call CBA_events_fnc_keyHandlerDown"];
_dsp displayAddEventHandler ["keyUp", "_this call CBA_events_fnc_keyHandlerUp"];