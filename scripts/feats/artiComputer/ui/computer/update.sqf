
#include "define.hpp"

disableSerialization;

params [['_grp', displayNull]];

if ( isNull _grp ) then {
	waitUntil { !(isNull (findDisplay AC_IDD)) };	
};

private _dsp = findDisplay AC_IDD;
_grp = _dsp displayCtrl TARGET_IDC;
	
artiComputer_uiNoEvents = true;

(_grp controlsGroupCtrl GRIDX_IDC) ctrlSetText (artiComputer_grid select 0);
(_grp controlsGroupCtrl GRIDY_IDC) ctrlSetText (artiComputer_grid select 1);
(_grp controlsGroupCtrl GRIDZ_IDC) ctrlSetText (artiComputer_grid select 2);

(_grp controlsGroupCtrl XWAY_IDC) lbSetCurSel (artiComputer_correction select 0);
(_grp controlsGroupCtrl CORX_IDC) ctrlSetText (artiComputer_correction select 1);

(_grp controlsGroupCtrl YWAY_IDC) lbSetCurSel (artiComputer_correction select 2);
(_grp controlsGroupCtrl CORY_IDC) ctrlSetText (artiComputer_correction select 3);

(_grp controlsGroupCtrl MAG_IDC) lbSetCurSel artiComputer_magIdx;
(_grp controlsGroupCtrl ANGLE_IDC) lbSetCurSel ([0,1] select artiComputer_highAngle);

(assignedVehicleRole player) params[["_role", ""], ["_turretPath", []]];
private _modeArgs = [(vehicle player), _turretPath, artiComputer_mag];
private _modeCtrl = (_grp controlsGroupCtrl MODE_IDC);
private _modeDsp = (["artiComputer", "modes"] call core_fnc_getSetting);
private _gotGrid = ( count artiComputer_grid > 0 );
{
	if ( _x isEqualTo "") exitWith { _gotGrid = false; };
} forEach artiComputer_grid;

if ( _gotGrid ) then {
	private _gridX = parseNumber (artiComputer_grid select 0);
	private _gridY = parseNumber  (artiComputer_grid select 1);
	private _gridZ = parseNumber  (artiComputer_grid select 2);
	private _corX = parseNumber (artiComputer_correction select 1);
	if ( (artiComputer_correction select 0) isEqualTo 1 ) then { _corX = 0 - _corX; };
	private _corY = parseNumber (artiComputer_correction select 3);
	if ( (artiComputer_correction select 2) isEqualTo 1 ) then { _corY = 0 - _corY; };
	private _trgt = ([_gridX, _gridY, _corX, _corY] call arti_fnc_gridToCoord);
	_trgt pushback _gridZ;
	private _tube = (vehicle player) call arti_fnc_getTubePosition;
	([_tube, _trgt] call arti_fnc_getDistance) params ["_dist", "_trgtHeight", "_altDiff"];
	(_grp controlsGroupCtrl DIST_IDC) ctrlSetText format["%1m", ([_dist, 2] call common_fnc_roundTo)];
	(_grp controlsGroupCtrl HEIGHT_IDC) ctrlSetText format["%1m", ([_trgtHeight, 2] call common_fnc_roundTo)];
	_modeArgs append [_dist, _altDiff];
} else {
	_modeArgs append [-1, 0]; 
	(_grp controlsGroupCtrl DIST_IDC) ctrlSetText "NaN";
	(_grp controlsGroupCtrl HEIGHT_IDC) ctrlSetText "NaN";
};
_modeArgs pushback artiComputer_highAngle;
private _modes = (_modeArgs call arti_fnc_getModes);
private _gotValidMode = ( {_x select 4} count _modes > 0 );

lbclear _modeCtrl;

if ( _gotValidMode ) then {
	private _modeIdx = -1;
	private _c = 0;
	{
		_x params ["_name", "_charge", "_minRange", "_maxRange", "_inRange"];
		if ( _inRange ) then {
	    	_modeCtrl lbAdd format["%1 (%2m - %3m)", (_modeDsp select _forEachIndex), floor _minRange, floor _maxRange];
	    	_modeCtrl lbSetData [_c, str _charge];
	    	if ( _charge isEqualTo artiComputer_charge ) then { _modeIdx = _c; };
	    	_c = _c + 1;
		};
	} forEach _modes;
	if ( _modeIdx < 0 ) then { _modeIdx = 0; };
	_modeCtrl lbSetCurSel _modeIdx;
	artiComputer_charge = parseNumber (_modeCtrl lbData _modeIdx); 
};

(_dsp displayCtrl COMPUTE_IDC) ctrlEnable ( _gotGrid && _gotValidMode );
(_dsp displayCtrl ABORD_IDC) ctrlEnable false;

artiComputer_uiNoEvents = false;
