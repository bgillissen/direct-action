
#define YOFFSET 0
#define TOT_WIDTH 1.2
#define TOT_HEIGHT 0.9
#define LINE_HEIGHT (1 / 25)
#define COR_LINE_OFFSET 1
#define INFO_LINE_OFFSET 1
#define CONF_LINE_OFFSET 1

#define AC_IDD 70000
#define TARGET_IDC 71000
#define COMPUTE_IDC 70001
#define PRGB_IDC 70002
#define ABORD_IDC 70003

#define GRIDX_IDC 71001
#define GRIDY_IDC 71002
#define GRIDZ_IDC 71003
#define XWAY_IDC 71004
#define CORX_IDC 71005
#define YWAY_IDC 71006
#define CORY_IDC 71007
#define DIST_IDC 71008
#define HEIGHT_IDC 71009
#define MAG_IDC 71010
#define MODE_IDC 71011
#define ANGLE_IDC 71012
