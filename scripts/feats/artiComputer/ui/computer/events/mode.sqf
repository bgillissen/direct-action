
#include "..\define.hpp"

disableSerialization;

params ["_ctrl", "_idx"];

if ( artiComputer_uiNoEvents ) exitWith {};

private _charge = parseNumber (_ctrl lbData _idx);

if !( _charge isEqualTo artiComputer_charge ) then { artiComputer_solution = []; };

artiComputer_charge = _charge;