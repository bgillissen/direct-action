
#include "..\..\..\..\..\common\ui\_sizes.hpp"
#include "..\define.hpp"

disableSerialization;

private _dsp = findDisplay AC_IDD;
private _grp = _dsp displayCtrl TARGET_IDC;
private _compute = _dsp displayCTRL COMPUTE_IDC;
private _abord = _dsp displayCTRL ABORD_IDC;

artiComputer_grid params ["_gridX", "_gridY", "_gridZ"];
artiComputer_correction params ["_xWay", "_corX", "_yWay", "_corY"];
artiComputer_solution = [];
artiComputer_buffer = [];
artiComputer_abord = false;

if ( _gridX isEqualTo "" || _gridY isEqualTo "" ) exitWith {};

_gridX = parseNumber _gridX;
_gridY = parseNumber _gridY;
_gridZ = parseNumber _gridZ;

_corX = parseNumber _corX;
if ( _xWay isEqualTo 1 ) then { _corX = 0 - _corX; };
_corY = parseNumber _corY;
if ( _yWay isEqualTo 1 ) then { _corY = 0 - _corY; };

private _turretPath = (assignedVehicleRole player) select 1;
if ( _turretPath isEqualTo [] ) exitwith {};

{
	(_grp controlsGroupCtrl _x) ctrlEnable false;
} forEach [GRIDX_IDC, GRIDY_IDC, GRIDZ_IDC, XWAY_IDC, CORX_IDC, YWAY_IDC, CORY_IDC, MAG_IDC, MODE_IDC, ANGLE_IDC];
_compute ctrlEnable false;
_abord ctrlEnable true;


artiComputer_computeThread = [(vehicle player), _turretPath, artiComputer_mag, artiComputer_charge, _gridX, _gridY, _gridZ, _corX, _corY, artiComputer_highAngle] spawn {
    artiComputer_buffer = (_this call arti_fnc_getFireSolution);
};

private _dur = (['artiComputer', 'computeDuration'] call core_fnc_getSetting);
private _endTime = time + _dur;
private _prgBar = (_dsp displayCtrl PRGB_IDC);
private _prgPos = ctrlPosition _prgBar;
 
_prgPos set [2, 0];
_prgBar ctrlSetBackgroundColor [1, 1, 1, 0.7];
_prgBar ctrlSetPosition _prgPos;
_prgBar ctrlCommit 0;

_prgPos set [2, (TOT_WIDTH - (3 * (BUTTON_WIDTH + SPACE)))];
_prgBar ctrlSetPosition _prgPos;
_prgBar ctrlCommit _dur;

private _failed = false;

waitUntil {
    sleep 0.2;
    _failed = ( (isNull artiComputer_computeThread) && (artiComputer_buffer isEqualTo []) && !artiComputer_abord );
	( ((isNull artiComputer_computeThread) && (time > _endTime)) || artiComputer_abord || _failed )    
};

_failed = ( artiComputer_buffer isEqualTo [] );

artiComputer_solution = artiComputer_buffer;

_prgPos = ctrlPosition _prgBar;
_prgBar ctrlSetPosition _prgPos;
_prgBar ctrlCommit 0;

private _color = [1, 1, 1, 0.7];
if ( _failed ) then {
    _color = [1, 0, 0, 0.7];
} else {
	if !( artiComputer_abord ) then {
    	_color = [(profilenamespace getvariable ['GUI_BCG_RGB_R',0.69]),
				  (profilenamespace getvariable ['GUI_BCG_RGB_G',0.75]),
				  (profilenamespace getvariable ['GUI_BCG_RGB_B',0.5]), 0.7];
	};
};
_prgBar ctrlSetBackgroundColor _color;
_prgBar ctrlCommit 0;

_prgPos set [2, 0];
_prgBar ctrlSetPosition _prgPos;
_prgBar ctrlCommit 1;

{
	(_grp controlsGroupCtrl _x) ctrlEnable true;
} forEach [GRIDX_IDC, GRIDY_IDC, GRIDZ_IDC, XWAY_IDC, CORX_IDC, YWAY_IDC, CORY_IDC, MAG_IDC, MODE_IDC, ANGLE_IDC];

sleep 2;

_compute ctrlEnable true;
_abord ctrlEnable false;
