
#include "..\define.hpp"

disableSerialization;

params ["_ctrl", "_idx"];

if ( artiComputer_uiNoEvents ) exitWith {};

if !( _idx isEqualTo artiComputer_magIdx ) then { artiComputer_solution = []; };

artiComputer_magIdx = _idx;
artiComputer_mag = (_ctrl lbData _idx);

[] spawn acComputer_fnc_update;