
#include "..\define.hpp"

disableSerialization;

params ["_ctrl"];

if ( artiComputer_uiNoEvents ) exitWith {};

artiComputer_uiNoEvents = true;

private _idc = ctrlIDC _ctrl;
private _grp = ctrlParentControlsGroup _ctrl;
private _way = _grp controlsGroupCtrl ([XWAY_IDC,YWAY_IDC] select (_idc isEqualto CORY_IDC));
private _key = [1,3] select (_idc isEqualto CORY_IDC);
private _val = parseNumber (ctrlText _ctrl);

_way lbSetCurSel ([0,1] select (_val < 0));

_val = str (abs _val);

if !( _val isEqualTo (artiComputer_correction select _key) ) then {
	artiComputer_solution = [];    
};

artiComputer_correction set[_key, _val];

_ctrl ctrlSetText _val;

artiComputer_uiNoEvents = false;

[] spawn acComputer_fnc_update;