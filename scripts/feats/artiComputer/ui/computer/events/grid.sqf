
#include "..\define.hpp"

disableSerialization;

params ["_ctrl"];

if ( artiComputer_uiNoEvents ) exitWith {};

private _key = 0; 

switch (ctrlIDC _ctrl) do {
	case GRIDX_IDC : { _key = 0; };
    case GRIDY_IDC : { _key = 1; };
    case GRIDZ_IDC : { _key = 2; };   
}; 

private _val = ctrlText _ctrl;

if !( _val isEqualTo (artiComputer_grid select _key) ) then { artiComputer_solution = []; };

artiComputer_grid set[_key, _val];

[] spawn acComputer_fnc_update;