
#include "..\define.hpp"

disableSerialization;

params ["_ctrl", "_idx"];

if ( artiComputer_uiNoEvents ) exitWith {};

if !( (_idx > 0) isEqualTo artiComputer_highAngle ) then { artiComputer_solution = []; };

artiComputer_highAngle = (_idx > 0);

[] spawn acComputer_fnc_update;