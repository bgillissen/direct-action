
#include "..\define.hpp"

disableSerialization;

params ["_ctrl", "_idx"];

if ( artiComputer_uiNoEvents ) exitWith {};

private _idc = ctrlIDC _ctrl;

private _key = [0,2] select ( _idc isEqualto YWAY_IDC );

if ( !(_idx isEqualTo (artiComputer_correction select _key)) && !((artiComputer_correction select (_key + 1)) isEqualTo "0") ) then {
	artiComputer_solution = [];    
};

artiComputer_correction set[_key,_idx];

[] spawn acComputer_fnc_update;