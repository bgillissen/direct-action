
#include "define.hpp"

class acComputer {
	idd = AC_IDD;
	name = "acComputer";
	scriptName = "acComputer";
	movingEnable = false;
	enableSimulation = true;
	onLoad = "_this spawn acComputer_fnc_init";
	onUnLoad = "_this call acComputer_fnc_destroy";
	class controlsBackground {
		class title : daTitle {
			text = "Artillery Computer";
			x = 0.5 - (TOT_WIDTH / 2);
			y = 0.5 - (TOT_HEIGHT / 2) - YOFFSET;
			h = LINE_HEIGHT;
			w = TOT_WIDTH;
		};
		class background : daContainer {
			x = 0.5 - (TOT_WIDTH / 2);
			y = 0.5 - (TOT_HEIGHT / 2) + LINE_HEIGHT + SPACE - YOFFSET;
			h = TOT_HEIGHT - (2 * LINE_HEIGHT) - (2 * SPACE);
			w = TOT_WIDTH;
		};
	};
	class Controls {
		class target : daControlGroup {
			idc = TARGET_IDC;
			x = (0.5 - (TOT_WIDTH / 2));
			y = (0.5 - (TOT_HEIGHT / 2)) + LINE_HEIGHT + SPACE - YOFFSET;
			h = TOT_HEIGHT - (2 * LINE_HEIGHT) - (2 * SPACE);
			w = TOT_WIDTH;
			class Controls {
				class gridTitle : daText {
					style = ST_CENTER;
					x = (TOT_WIDTH / 2) - BUTTON_WIDTH;
					y = SPACE;
					h = LINE_HEIGHT;
					w = (BUTTON_WIDTH * 2);
					text = "Target's location";
				};
				class gridContainer : daContainer {
					x = (TOT_WIDTH / 2) - (((3 * BUTTON_WIDTH) + (4 * SPACE)) / 2);
					y = LINE_HEIGHT + (2 * SPACE);
					h = (2 * LINE_HEIGHT) + (3 * SPACE);
					w = (3 * BUTTON_WIDTH) + (4 * SPACE);
				};
				class gridXTitle : daText {
					style = ST_CENTER;
					x = (TOT_WIDTH / 2) - (((3 * BUTTON_WIDTH) + (4 * SPACE)) / 2) + SPACE;
					y = LINE_HEIGHT + (3 * SPACE);
					h = LINE_HEIGHT;
					w = BUTTON_WIDTH;
					text = "Latitude";
				};
				class gridX : daEdit {
					idc = GRIDX_IDC;
					x = (TOT_WIDTH / 2) - (((3 * BUTTON_WIDTH) + (4 * SPACE)) / 2) + SPACE;
					y = (2 * LINE_HEIGHT) + (4 * SPACE);
					h = LINE_HEIGHT;
					w = BUTTON_WIDTH;
					tooltip = "3 digits + decimals";
					onKillFocus = "_this call acComputer_fnc_evtGrid";
				};
				class gridYTitle : daText {
					style = ST_CENTER;
					x = (TOT_WIDTH / 2) - (((3 * BUTTON_WIDTH) + (4 * SPACE)) / 2) + (2 * SPACE) + BUTTON_WIDTH;
					y = LINE_HEIGHT + (3 * SPACE);
					h = LINE_HEIGHT;
					w = BUTTON_WIDTH;
					text = "Longitude";
				};
				class gridY : daEdit {
					idc = GRIDY_IDC;
					x = (TOT_WIDTH / 2) - (((3 * BUTTON_WIDTH) + (4 * SPACE)) / 2) + (2 * SPACE) + BUTTON_WIDTH;
					y = (2 * LINE_HEIGHT) + (4 * SPACE);
					h = LINE_HEIGHT;
					w = BUTTON_WIDTH;
					tooltip = "3 digits + decimals";
				 	onKillFocus = "_this call acComputer_fnc_evtGrid";
				};
				class gridZTitle : daText {
					style = ST_CENTER;
					x = (TOT_WIDTH / 2) - (((3 * BUTTON_WIDTH) + (4 * SPACE)) / 2) + (3 * SPACE) + (2 * BUTTON_WIDTH);
					y = LINE_HEIGHT + (3 * SPACE);
					h = LINE_HEIGHT;
					w = BUTTON_WIDTH;
					text = "Altitude";
				};
				class gridZ : daEdit {
					idc = GRIDZ_IDC;
					x = (TOT_WIDTH / 2) - (((3 * BUTTON_WIDTH) + (4 * SPACE)) / 2) + (3 * SPACE) + (2 * BUTTON_WIDTH);
					y = (2 * LINE_HEIGHT) + (4 * SPACE);
					h = LINE_HEIGHT;
					w = BUTTON_WIDTH;
					onKillFocus = "_this call acComputer_fnc_evtGrid";
				};
				
				class corTitle : daText {
					style = ST_CENTER;
					x = (TOT_WIDTH / 2) - BUTTON_WIDTH;
					y = ((2 + COR_LINE_OFFSET) * LINE_HEIGHT) + (10 * SPACE);
					h = LINE_HEIGHT;
					w = (BUTTON_WIDTH * 2);
					text = "Corrections (in meters)";
				};
				class corContainer : daContainer {
					x = (TOT_WIDTH / 2) - (((3.4 * BUTTON_WIDTH) + (6 * SPACE)) / 2);
					y = ((3 + COR_LINE_OFFSET) * LINE_HEIGHT) + (11 * SPACE);
					h = (2 * LINE_HEIGHT) + (2 * SPACE);
					w = (3.4 * BUTTON_WIDTH) + (6 * SPACE);
				};
				class corXTitle : daText {
					style = ST_CENTER;
					text = "Latitude";
					x = (TOT_WIDTH / 2) - (((3.4 * BUTTON_WIDTH) + (6 * SPACE)) / 2) + SPACE + ((((1.7 * BUTTON_WIDTH) + SPACE)) / 2) - (BUTTON_WIDTH / 2);
					y = ((3 + COR_LINE_OFFSET) * LINE_HEIGHT) + (11 * SPACE);
					h = LINE_HEIGHT;
					w = BUTTON_WIDTH;
				};
				class corXWay : daCombo {
					idc = XWAY_IDC;
					x = (TOT_WIDTH / 2) - (((3.4 * BUTTON_WIDTH) + (6 * SPACE)) / 2) + SPACE;
					y = ((4 + COR_LINE_OFFSET) * LINE_HEIGHT) + (12 * SPACE);
					h = LINE_HEIGHT;
					w = (BUTTON_WIDTH * 0.7);
					onLBSelChanged = "_this call artiComputer_fnc_evtWay";
				};
				class corX : daEdit {
					idc = CORX_IDC;
					x = (TOT_WIDTH / 2) - (((3.4 * BUTTON_WIDTH) + (6 * SPACE)) / 2) + (BUTTON_WIDTH * 0.7) + (2 * SPACE);
					y = ((4 + COR_LINE_OFFSET) * LINE_HEIGHT) + (12 * SPACE);
					h = LINE_HEIGHT;
					w = BUTTON_WIDTH;
					onKillFocus = "_this call acComputer_fnc_evtCorrection";
				};
				class corYTitle : daText {
					style = ST_CENTER;
					text = "Longitude";
					x = (TOT_WIDTH / 2) - (((3.4 * BUTTON_WIDTH) + (6 * SPACE)) / 2) + (1.7 * BUTTON_WIDTH) + (4 * SPACE) + ((((1.7 * BUTTON_WIDTH) + SPACE)) / 2) - (BUTTON_WIDTH / 2);
					y = ((3 + COR_LINE_OFFSET) * LINE_HEIGHT) + (11 * SPACE);
					h = LINE_HEIGHT;
					w = BUTTON_WIDTH;
				};
				class corYWay : daCombo {
					idc = YWAY_IDC;
					x = (TOT_WIDTH / 2) - (((3.4 * BUTTON_WIDTH) + (6 * SPACE)) / 2) + (1.7 * BUTTON_WIDTH) + (4 * SPACE);
					y = ((4 + COR_LINE_OFFSET) * LINE_HEIGHT) + (12 * SPACE);
					h = LINE_HEIGHT;
					w = (BUTTON_WIDTH * 0.7);
					onLBSelChanged = "_this call acComputer_fnc_evtWay";
				};
				class corY : daEdit {
					idc = CORY_IDC;
					x = (TOT_WIDTH / 2) - (((3.4 * BUTTON_WIDTH) + (6 * SPACE)) / 2) + (2.4 * BUTTON_WIDTH) + (5 * SPACE);
					y = ((4 + COR_LINE_OFFSET) * LINE_HEIGHT) + (12 * SPACE);
					h = LINE_HEIGHT;
					w = BUTTON_WIDTH;
					onKillFocus = "_this call acComputer_fnc_evtCorrection";
				};
				
				class infoTitle : daText {
					style = ST_CENTER;
					x = (TOT_WIDTH / 2) - BUTTON_WIDTH;
					y = ((5 + COR_LINE_OFFSET + INFO_LINE_OFFSET) * LINE_HEIGHT) + (13 * SPACE);
					h = LINE_HEIGHT;
					w = (BUTTON_WIDTH * 2);
					text = "Target's informations";
				};
				class infoContainer : daContainer {
					x = (TOT_WIDTH / 2) - (((2 * BUTTON_WIDTH) + (3 * SPACE)) / 2);
					y = ((6 + COR_LINE_OFFSET + INFO_LINE_OFFSET) * LINE_HEIGHT) + (14 * SPACE);
					h = (2 * LINE_HEIGHT) + (2 * SPACE);
					w = (2 * BUTTON_WIDTH) + (3 * SPACE);
				};
				class distTitle : daText {
					style = ST_CENTER;
					text = "Distance";
					x = (TOT_WIDTH / 2) - BUTTON_WIDTH - (0.5 * SPACE));
					y = ((6 + COR_LINE_OFFSET + INFO_LINE_OFFSET) * LINE_HEIGHT) + (14 * SPACE);
					h = LINE_HEIGHT;
					w = BUTTON_WIDTH;
				};
				class dist : daText {
					idc = DIST_IDC;
					style = ST_CENTER;	
					x = (TOT_WIDTH / 2) - BUTTON_WIDTH - (SPACE * 0.5);
					y = ((7 + COR_LINE_OFFSET + INFO_LINE_OFFSET) * LINE_HEIGHT) + (15 * SPACE);
					h = LINE_HEIGHT;
					w = BUTTON_WIDTH;
				};
				class heightTitle : daText {
					style = ST_CENTER;
					text = "Elevation";
					x = (TOT_WIDTH / 2) + (0.5 * SPACE);
					y = ((6 + COR_LINE_OFFSET + INFO_LINE_OFFSET) * LINE_HEIGHT) + (14 * SPACE);
					h = LINE_HEIGHT;
					w = BUTTON_WIDTH;
				};
				class height : daText {
					idc = HEIGHT_IDC;
					style = ST_CENTER;
					x = (TOT_WIDTH / 2) + (SPACE * 0.5);
					y = ((7 + COR_LINE_OFFSET + INFO_LINE_OFFSET) * LINE_HEIGHT) + (15 * SPACE);
					h = LINE_HEIGHT;
					w = BUTTON_WIDTH;
				};				
				
				class confTitle : daText {
					style = ST_CENTER;
					text = "Shot Settings";
					x = (TOT_WIDTH / 2) - (BUTTON_WIDTH / 2);
					y = ((8 + COR_LINE_OFFSET + CONF_LINE_OFFSET + INFO_LINE_OFFSET) * LINE_HEIGHT) + (16 * SPACE);
					h = LINE_HEIGHT;
					w = BUTTON_WIDTH;
				};
				class confContainer : daContainer {
					x = (TOT_WIDTH / 2) - (2.55 * BUTTON_WIDTH) - (2 * SPACE);
					y = ((9 + COR_LINE_OFFSET + CONF_LINE_OFFSET + INFO_LINE_OFFSET) * LINE_HEIGHT) + (17 * SPACE);
					h = (2 * LINE_HEIGHT) + (2 * SPACE);
					w = (5.1 * BUTTON_WIDTH) + (4 * SPACE);
				};
				class magTitle : daText {
					style = ST_CENTER;
					text = "Shell";
					x = (TOT_WIDTH / 2) - (2.5 * BUTTON_WIDTH) - ( 2 * SPACE);
					y = ((9 + COR_LINE_OFFSET + CONF_LINE_OFFSET + INFO_LINE_OFFSET) * LINE_HEIGHT) + (17 * SPACE);
					h = LINE_HEIGHT;
					w = (2 * BUTTON_WIDTH);
				};
				class mag : daCombo {
					idc = MAG_IDC;
					x = (TOT_WIDTH / 2) - (2.5 * BUTTON_WIDTH) - ( 2 * SPACE);
					y = ((10 + COR_LINE_OFFSET + CONF_LINE_OFFSET + INFO_LINE_OFFSET) * LINE_HEIGHT) + (18 * SPACE);
					h = LINE_HEIGHT;
					w = (2 * BUTTON_WIDTH);
					onLBSelChanged = "_this call acComputer_fnc_evtMag";
				};
				class modeTitle : daText {
					style = ST_CENTER;
					text = "Mode";
					x = (TOT_WIDTH / 2) - (0.5 * BUTTON_WIDTH) - SPACE;
					y = ((9 + COR_LINE_OFFSET + CONF_LINE_OFFSET + INFO_LINE_OFFSET) * LINE_HEIGHT) + (17 * SPACE);
					h = LINE_HEIGHT;
					w = (2 * BUTTON_WIDTH);
				};
				class mode : daCombo {
					idc = MODE_IDC;
					x = (TOT_WIDTH / 2) - (0.5 * BUTTON_WIDTH) - SPACE;
					y = ((10 + COR_LINE_OFFSET + CONF_LINE_OFFSET + INFO_LINE_OFFSET) * LINE_HEIGHT) + (18 * SPACE);
					h = LINE_HEIGHT;
					w = (2 * BUTTON_WIDTH);
					onLBSelChanged = "_this call acComputer_fnc_evtMode";
				};
				class angleTitle : daText {
					style = ST_CENTER;
					text = "Angle";
					x = (TOT_WIDTH / 2) + (1.5 * BUTTON_WIDTH);
					y = ((9 + COR_LINE_OFFSET + CONF_LINE_OFFSET + INFO_LINE_OFFSET) * LINE_HEIGHT) + (17 * SPACE);
					h = LINE_HEIGHT;
					w = (1.1 * BUTTON_WIDTH);
				};
				class angle : daCombo {
					idc = ANGLE_IDC;
					x = (TOT_WIDTH / 2) + (1.5 * BUTTON_WIDTH);
					y = ((10 + COR_LINE_OFFSET + CONF_LINE_OFFSET + INFO_LINE_OFFSET) * LINE_HEIGHT) + (18 * SPACE);
					h = LINE_HEIGHT;
					w = (1.1 * BUTTON_WIDTH);
					onLBSelChanged = "_this call acComputer_fnc_evtAngle";
				};
			};
		};
		class close : daTxtButton {
			x = 0.5 - (TOT_WIDTH / 2);
			y = 0.5 + (TOT_HEIGHT / 2) - LINE_HEIGHT - YOFFSET;
			h = LINE_HEIGHT;
			w = BUTTON_WIDTH;
			text = "Close";
			action = "closeDialog 0;";

		};
		class compute : daTxtButton {
			idc = COMPUTE_IDC;
			x = 0.5 - (TOT_WIDTH / 2) + SPACE + BUTTON_WIDTH;
			y = 0.5 + (TOT_HEIGHT / 2) - LINE_HEIGHT - YOFFSET;
			h = LINE_HEIGHT;
			w = BUTTON_WIDTH;
			text = "Compute";
			action = "_this spawn acComputer_fnc_evtCompute";
		};
		class prgBar : daContainer {
			idc = PRGB_IDC;
			colorBackground[] = COL_USR_TXT;
			x = 0.5 - (TOT_WIDTH / 2) + (2 * (BUTTON_WIDTH + SPACE));
			y = 0.5 + (TOT_HEIGHT / 2) - LINE_HEIGHT - YOFFSET;
			h = LINE_HEIGHT;
			w = 0;
		};
		class abord : daTxtButton {
			idc = ABORD_IDC;
			x = 0.5 + (TOT_WIDTH / 2) - BUTTON_WIDTH;
			y = 0.5 + (TOT_HEIGHT / 2) - LINE_HEIGHT - YOFFSET;
			h = LINE_HEIGHT;
			w = BUTTON_WIDTH;
			text = "Abord";
			action = "_this call acComputer_fnc_evtAbord";
		};
	};
};
