#include "define.hpp"

disableSerialization;

params [["_dsp", displayNull]];

private _ctrl = _dsp displayCtrl IDC;
private _veh = vehicle player;
private _driverInfo = (["artiComputer", "driverInfo"] call core_fnc_getSetting);

while { true } do {
  	(_veh call BIS_fnc_getPitchBank) params ["_pitch", "_bank"];
    _pitch = (round (_pitch * 100)) / 100;
	_bank = (round (_bank * 100)) / 100;
    _ctrl ctrlSetStructuredText parseText format[_driverInfo, _pitch, _bank];
	_ctrl ctrlCommit 0;
    sleep (1 / 25);        
};