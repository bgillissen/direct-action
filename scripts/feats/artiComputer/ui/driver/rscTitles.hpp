
#include "define.hpp"

class acDriver {
	idd = IDD;
	fadeout = 0;
	fadein = 0;
	duration = 1e+1000;
	name= "acDriver";
	onLoad = "_this spawn acDriver_fnc_init";
	class Controls {
		class container : daStructuredText {
			idc = IDC;
			x = safeZoneX + safeZoneW - 0.28;
			y = SafeZoneY + 0.07;
			w = 0.2;
			h = 0.1;
			colorText[] = {1,1,1,1};
			colorBackground[] = {0,0,0,0};
			text = "";
		};
	};
};
