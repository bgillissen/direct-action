
#include "define.hpp"

class acGunner {
	idd = IDD;
	fadeout = 0;
	fadein = 0;
	duration = 1e+1000;
	name= "acGunner";
	onLoad = "_this spawn acGunner_fnc_init";
	class Controls {
		class background : daContainer {
			idc = BCG_IDC; 
			x = safeZoneX + safeZoneW - (3 * ENTRY_WIDTH);
			y = safeZoneY;
			w = 3 * ENTRY_WIDTH;
			h = safeZoneH;
			colorBackground[] = {0,0,0,1};	
		};
		class container : daControlGroup {
			idc = PANEL_IDC;
			x = safeZoneX + safeZoneW - (3 * ENTRY_WIDTH) - XOFFSET;
			y = 0.5 - ((19 * (ENTRY_HEIGHT + ENTRY_SPACE)) / 2) - YOFFSET;
			w = 3 * ENTRY_WIDTH;
			h = safeZoneH - 0.5 - ((19 * (ENTRY_HEIGHT + ENTRY_SPACE)) / 2) - YOFFSET;
			class VScrollbar {
				width = 0;
				autoScrollSpeed = -1;
				autoScrollDelay = 5;
				autoScrollRewind = 0;
				autoScrollEnabled = 0;
				scrollSpeed = 0.06;
				#include "..\..\..\..\common\ui\_scrollbar.hpp"
			};
			class HScrollbar {
				height = 0;
				autoScrollSpeed = -1;
				autoScrollDelay = 5;
				autoScrollRewind = 0;
				autoScrollEnabled = 0;
				scrollSpeed = 0.06;
				#include "..\..\..\..\common\ui\_scrollbar.hpp"
			};
			class controls {
				class PitchTitle : acInfoTitle {
					text = "Pitch";
					w = 1.5 * ENTRY_WIDTH;
				};
				class BankTitle : acInfoTitle {
					text = "Bank";
					x = 1.5 * ENTRY_WIDTH;
					w = 1.5 * ENTRY_WIDTH;
				};
				class Pitch : acInfoEntry {
					idc = PITCH_IDC;
					text = "NaN";
					y = ENTRY_HEIGHT + ENTRY_SPACE;
					w = 1.5 * ENTRY_WIDTH;
				};
				class Bank : acInfoEntry {
					idc = BANK_IDC;
					text = "NaN";
					x = 1.5 * ENTRY_WIDTH;
					y = ENTRY_HEIGHT + ENTRY_SPACE;
					w = 1.5 * ENTRY_WIDTH;
				};


				class DirTitle : acInfoTitle {
					text = "Bearings";
					y = 2 * (ENTRY_HEIGHT + ENTRY_SPACE);
				};
				class CurDirDesc : acInfoDesc {
					text = "Current";
					y = 3 * (ENTRY_HEIGHT + ENTRY_SPACE);
				};
				class AbsDirDesc : acInfoDesc {
					text = "Absolute";
					x = ENTRY_WIDTH;
					y = 3 * (ENTRY_HEIGHT + ENTRY_SPACE);
				};
				class RelDirDesc : acInfoDEsc {
					text = "Relative";
					x = 2 * ENTRY_WIDTH;
					y = 3 * (ENTRY_HEIGHT + ENTRY_SPACE);
				};
				class CurDir : acInfoDyn {
					idc = CUR_DIR_IDC;
					text = "NaN";
					y = 4 * (ENTRY_HEIGHT + ENTRY_SPACE);
				};
				class AbsDir : acInfoEntry {
					idc = ABS_DIR_IDC;
					text = "NaN";
					x = ENTRY_WIDTH;
					y = 4 * (ENTRY_HEIGHT + ENTRY_SPACE);

				};
				class RelDir : acInfoEntry {
					idc = REL_DIR_IDC;
					text = "NaN";
					x = 2 * ENTRY_WIDTH;
					y = 4 * (ENTRY_HEIGHT + ENTRY_SPACE);
				};


				class ElevTitle : acInfoTitle {
					text = "Tube Elevations";
					y = 5 * (ENTRY_HEIGHT + ENTRY_SPACE);
				};
				class CurElevDesc : acInfoDesc {
					text = "Current";
					y = 6 * (ENTRY_HEIGHT + ENTRY_SPACE);
				};
				class AbsElevDesc : acInfoDesc {
					text = "Absolute";
					x = ENTRY_WIDTH;
					y = 6 * (ENTRY_HEIGHT + ENTRY_SPACE);
				};
				class RelElevDesc : acInfoDesc {
					text = "Relative";
					x = 2 * ENTRY_WIDTH;
					y = 6 * (ENTRY_HEIGHT + ENTRY_SPACE);
				};
				class CurElev : acInfoDyn {
					idc = CUR_ELEV_IDC;
					text = "NaN";
					y = 7 * (ENTRY_HEIGHT + ENTRY_SPACE);
				};
				class AbsElev : acInfoEntry {
					idc = ABS_ELEV_IDC;
					text = "NaN";
					x = ENTRY_WIDTH;
					y = 7 * (ENTRY_HEIGHT + ENTRY_SPACE);
				};
				class RrelElev : acInfoEntry {
					idc = REL_ELEV_IDC;
					text = "NaN";
					x = 2 * ENTRY_WIDTH;
					y = 7 * (ENTRY_HEIGHT + ENTRY_SPACE);
				};


				class ModeTitle : acInfoTitle {
					text = "Fire Modes";
					y = 8 * (ENTRY_HEIGHT + ENTRY_SPACE);
				};
				class CurModeDesc : acInfoDesc {
					text = "Current";
					y = 9 * (ENTRY_HEIGHT + ENTRY_SPACE);
					w = 1.5 * ENTRY_WIDTH;
				};
				class SolModeDesc : acInfoDesc {
					text = "Computed";
					x = 1.5 * ENTRY_WIDTH;
					y = 9 * (ENTRY_HEIGHT + ENTRY_SPACE);
					w = 1.5 * ENTRY_WIDTH;

				};
				class CurMode : acInfoDyn {
					idc = CUR_MODE_IDC;
					text = "NaN";
					y = 10 * (ENTRY_HEIGHT + ENTRY_SPACE);
					w = 1.5 * ENTRY_WIDTH;
				};
				class SolMode : acInfoEntry {
					idc = SOL_MODE_IDC;
					text = "NaN";
					x = 1.5 * ENTRY_WIDTH;
					y = 10 * (ENTRY_HEIGHT + ENTRY_SPACE);
					w = 1.5 * ENTRY_WIDTH;
				};


				class MagTitle : acInfoTitle {
					text = "Ammunitions";
					y = 11 * (ENTRY_HEIGHT + ENTRY_SPACE);
				};
				class CurMagDesc : acInfoDesc {
					text = "Current";
					y = 12 * (ENTRY_HEIGHT + ENTRY_SPACE);
					w = 1.5 * ENTRY_WIDTH;
				};
				class SolMagDesc : acInfoDesc {
					text = "Computed";
					x = 1.5 * ENTRY_WIDTH;
					y = 12 * (ENTRY_HEIGHT + ENTRY_SPACE);
					w = 1.5 * ENTRY_WIDTH;
				};
				class CurMag : acInfoDyn {
					idc = CUR_SHELL_IDC;
					text = "NaN";
					y = 13 * (ENTRY_HEIGHT + ENTRY_SPACE);
					w = 1.5 * ENTRY_WIDTH;
				};
				class SolMag : acInfoEntry {
					idc = SOL_SHELL_IDC;
					text = "NaN";
					x = 1.5 * ENTRY_WIDTH;
					y = 13 * (ENTRY_HEIGHT + ENTRY_SPACE);
					w = 1.5 * ENTRY_WIDTH;
				};

				class InfoTitle : acInfoTitle {
					text = "Target Info";
					y = 14 * (ENTRY_HEIGHT + ENTRY_SPACE);
				};
				class DistDesc : acInfoDesc {
					text = "Distance";
					y = 15 * (ENTRY_HEIGHT + ENTRY_SPACE);
				};
				class AltDesc : acInfoDesc {
					text = "Elevation";
					x = ENTRY_WIDTH;
					y = 15 * (ENTRY_HEIGHT + ENTRY_SPACE);
				};
				class EtaDesc : acInfoDesc {
					text = "ETA";
					x = 2 * ENTRY_WIDTH;
					y = 15 * (ENTRY_HEIGHT + ENTRY_SPACE);
				};
				class Dist : acInfoEntry {
					idc = DIST_IDC;
					text = "NaN";
					y = 16 * (ENTRY_HEIGHT + ENTRY_SPACE);
				};
				class Elev : acInfoEntry {
					idc = ELEV_IDC;
					text = "NaN";
					x = ENTRY_WIDTH;
					y = 16 * (ENTRY_HEIGHT + ENTRY_SPACE);
				};
				class Eta : acInfoEntry {
					idc = ETA_IDC;
					text = "NaN";
					x = 2 * ENTRY_WIDTH;
					y = 16 * (ENTRY_HEIGHT + ENTRY_SPACE);
				};

				class ShellTitle : acInfoTitle {
					idc = SHELL_CNT_IDC;
					text = "In-Flight Shells (NaN)";
					y = 17 * (ENTRY_HEIGHT + ENTRY_SPACE);
				};
				class ShellContainer : daControlGroup {
					idc = SHELLS_IDC;
					y = 18 * (ENTRY_HEIGHT + ENTRY_SPACE);
					w = 3 * ENTRY_WIDTH;
					h = safeZoneH - 0.5 - ((19 * (ENTRY_HEIGHT + ENTRY_SPACE)) / 2) - YOFFSET;
					class VScrollbar {
						width = 0.02;
						autoScrollSpeed = -1;
						autoScrollDelay = 5;
						autoScrollRewind = 0;
						autoScrollEnabled = 0;
						scrollSpeed = 0.06;
						#include "..\..\..\..\common\ui\_scrollbar.hpp"
					};
					class HScrollbar {
						height = 0;
						autoScrollSpeed = -1;
						autoScrollDelay = 5;
						autoScrollRewind = 0;
						autoScrollEnabled = 0;
						scrollSpeed = 0.06;
						#include "..\..\..\..\common\ui\_scrollbar.hpp"
					};
				};
			};
		};
	};
};
