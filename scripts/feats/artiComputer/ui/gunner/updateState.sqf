
#include "define.hpp"

params [["_grp", controlNull], ["_veh", objNull], ["_turretPath", []], ["_weapon", ""], ["_modes", []], ["_elevOffset", 0], ["_anim", ""], ["_solution", []]];

_solution params [["_bearings", []], ["_angles", []], ["_solCharge", "NaN"], ["_shell", "NaN"], ["_distance", "NaN"], ["_altDiff", "NaN"], ["_eta", "NaN"]];
_bearings params [["_absDir", "NaN"], ["_relDir", "NaN"]];
_angles params [["_absElev", "NaN"], ["_relElev", "NaN"]];


(_veh call BIS_fnc_getPitchBank) params ["_pitch", "_bank"];
(_grp controlsGroupCtrl PITCH_IDC) ctrlSetText str ([_pitch, 2] call common_fnc_roundTo);
(_grp controlsGroupCtrl BANK_IDC) ctrlSetText str ([_bank, 2] call common_fnc_roundTo);


private _dir = 0 - deg(_veh animationPhase "mainturret");
if ( _dir < 0 ) then { _dir = 360 + _dir; };
_dir = ((getDir(_veh) + _dir) % 360);
(_grp controlsGroupCtrl CUR_DIR_IDC) ctrlSetText str ([_dir, 2] call common_fnc_roundTo);
private _color = COL_WHITE;
if ( count _solution > 0 ) then {
	private _diff = abs (_dir - _absDir);
	_color = COL_RED;
	if ( _diff < 0.5 ) then { _color = COL_YELLOW; };
	if ( _diff < 0.05 ) then { _color = COL_GREEN; };
};
(_grp controlsGroupCtrl CUR_DIR_IDC) ctrlSetTextColor _color; 


private _elev = 0;
if !( isNil "_anim" ) then {
	_elev = _elevOffset + deg (_veh animationPhase _anim);
};
(_grp controlsGroupCtrl CUR_ELEV_IDC) ctrlSetText str ([_elev, 2] call common_fnc_roundTo);
private _color = COL_WHITE;
if ( count _solution > 0 ) then {
	private _diff = abs (_elev - _absElev);
	_color = COL_RED;
	if ( _diff < 0.5 ) then { _color = COL_YELLOW; };
	if ( _diff < 0.05 ) then { _color = COL_GREEN; };
};
(_grp controlsGroupCtrl CUR_ELEV_IDC) ctrlSetTextColor _color; 


(weaponState [_veh, _turretPath, _weapon]) params ["_weapon", "_muzzle", "_fireMode", "_mag", "_ammoCount"];
_fireMode = currentWeaponMode player; 
(_grp controlsGroupCtrl CUR_SHELL_IDC) ctrlSetText getText(configFile >> "cfgMagazines" >> _mag >> "displayName");
private _color = COL_WHITE;
if ( count _solution > 0 ) then {
	_color = COL_RED;
	if ( _mag isEqualTo _shell ) then { _color = COL_GREEN; };
};
(_grp controlsGroupCtrl CUR_SHELL_IDC) ctrlSetTextColor _color; 


private _curMode = "";
private _curCharge = -1;
{
	_x params ["_mode", "_charge", "_dsp"];
    if ( _fireMode isEqualTo _mode ) exitWith { 
    	_curMode = _dsp;
    	_curCharge = _charge; 
	};
} forEach _modes;
(_grp controlsGroupCtrl CUR_MODE_IDC) ctrlSetText _curMode;
private _color = COL_WHITE;
if ( count _solution > 0 ) then {
	_color = COL_RED;
	if ( _curCharge isEqualTo _solCharge ) then { _color = COL_GREEN; };
};
(_grp controlsGroupCtrl CUR_MODE_IDC) ctrlSetTextColor _color; 
