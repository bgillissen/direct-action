
#include "define.hpp" 

params [["_dsp", displayNull], ["_grp", controlNull]];

private _shellCnt = _grp controlsGroupCtrl SHELL_CNT_IDC;
private _shellGrp = _grp controlsGroupCtrl SHELLS_IDC;

private _idc = SHELL_BASE_IDC;
while { !isNull (_shellGrp controlsGroupCtrl _idc) } do {
	ctrlDelete (_shellGrp controlsGroupCtrl _idc);
	_idc = _idc + 1;
};

private _c = 0;
{
    _x params ["_mag", "_shell", "_shotAt", "_eta"];
    private _timeLeft = (_shotAt + _eta - time);
    if ( _timeLeft > SHELL_SHOWN_AFTER_IMPACT ) then {
    	private _ctrl = _dsp ctrlCreate ["acShellEntry", (SHELL_BASE_IDC + _c), _shellGrp];
		private _pos = ctrlPosition _ctrl;
    	_pos set [1, (1.5 * ENTRY_HEIGHT * _c)];
    	_ctrl ctrlSetPosition _pos;
        _ctrl ctrlCommit 0;
    	(_ctrl controlsGroupCtrl SHELL_AMMO_IDC) ctrlSetText getText(configFile >> "cfgMagazines" >> _mag >> "displayName");
    	if ( _timeLeft < 0 ) then { _timeLeft = 0; };  
    	(_ctrl controlsGroupCtrl SHELL_TIME_IDC) ctrlSetText format["%1s", ([_timeLeft, 1] call common_fnc_roundTo)];
    	private _prgB = (_ctrl controlsGroupCtrl SHELL_PRGB_IDC);
    	private _prct = (_eta - _timeLeft) / _eta;
    	if ( _prct >= 1 ) then {
        	_prgB ctrlSetBackgroundColor [1, 0.4, 0.4, 0.7];
    		_prct = 1; 
		};
    	private _pos = ctrlPosition _prgB;
		_pos set [2, (3 * ENTRY_WIDTH * _prct)];
    	_prgB ctrlSetPosition _pos;
    	_prgB ctrlCommit 0;
    	if ( _prct < 1 ) then {
    		_pos set [2, (3 * ENTRY_WIDTH)];
    		_prgB ctrlSetPosition _pos;
    		_prgB ctrlCommit _timeLeft;
		};
        _c = _c + 1;
	} else {
        artiComputer_shells deleteAt _forEachIndex;
    };     
} forEach artiComputer_shells;

_shellCnt ctrlSetText format["In-Flight Shells (%1)", _c];