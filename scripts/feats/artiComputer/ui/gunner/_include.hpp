
#include "define.hpp"

class acInfoTitle : daText {
	style = ST_CENTER;
	w = 3 * ENTRY_WIDTH;
	h = ENTRY_HEIGHT;
	SizeEx = ENTRY_FONTSIZE;
	colorText[] = {1, 1, 1, 1};
};
class acInfoDesc : daText {
	style = ST_CENTER;
	w = ENTRY_WIDTH;
	h = ENTRY_HEIGHT;
	SizeEx = ENTRY_FONTSIZE * 0.7;
	colorText[] = {1, 1, 1, 0.6};
};
class acInfoEntry : daText {
	style = ST_CENTER;
	w = ENTRY_WIDTH;
	h = ENTRY_HEIGHT;
	SizeEx = ENTRY_FONTSIZE * 0.85;
	colorText[] = {1, 1, 1, 0.6};
};
class acInfoDyn : daText {
	style = ST_CENTER;
	w = ENTRY_WIDTH;
	h = ENTRY_HEIGHT;
	SizeEx = ENTRY_FONTSIZE * 0.85;
	colorText[] = COL_USR_BCG;
};


class acShellEntry : daControlGroup {
	w = 3 * ENTRY_WIDTH;
	h = 1.5 * ENTRY_HEIGHT;
	class VScrollbar {
		width = 0;
		autoScrollSpeed = -1;
		autoScrollDelay = 5;
		autoScrollRewind = 0;
		autoScrollEnabled = 0;
		scrollSpeed = 0.06;
		#include "..\..\..\..\common\ui\_scrollbar.hpp"
	};
	class HScrollbar {
		height = 0;
		autoScrollSpeed = -1;
		autoScrollDelay = 5;
		autoScrollRewind = 0;
		autoScrollEnabled = 0;
		scrollSpeed = 0.06;
		#include "..\..\..\..\common\ui\_scrollbar.hpp"
	};
	class Controls {
		class ammo : daText {
			idc = SHELL_AMMO_IDC;
			SizeEx = ENTRY_FONTSIZE * 0.85;
			colorText[] = {1, 1, 1, 0.6};
			h = ENTRY_HEIGHT;
			w = (2 * ENTRY_WIDTH);
		};
		class time : daText {
			style = ST_RIGHT;
			idc = SHELL_TIME_IDC;
			SizeEx = ENTRY_FONTSIZE * 0.85;
			colorText[] = {1, 1, 1, 1};
			x = 2 * ENTRY_WIDTH;
			w = ENTRY_WIDTH;
			h = ENTRY_HEIGHT;
		};
		class prgb : daContainer {
			idc = SHELL_PRGB_IDC;
			y = ENTRY_HEIGHT;
			h = ENTRY_HEIGHT / 2;
			colorBackground[] = COL_USR_BCG;
		};
	};
};
