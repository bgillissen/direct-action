
#include "define.hpp"

params [["_grp", controlNull], ["_modes", []], ["_data", []]];

_data params [["_bearings", []], ["_angles", []], ["_solCharge", "NaN"], ["_mag", "NaN"], ["_distance", "NaN"], ["_altDiff", "NaN"], ["_eta", "NaN"]];
_bearings params [["_absDir", "NaN"], ["_relDir", "NaN"]];
_angles params [["_absElev", "NaN"], ["_relElev", "NaN"]];

(_grp controlsGroupCtrl ABS_DIR_IDC) ctrlSetText str _absDir;
(_grp controlsGroupCtrl REL_DIR_IDC) ctrlSetText str _relDir;
(_grp controlsGroupCtrl ABS_ELEV_IDC) ctrlSetText str _absElev;
(_grp controlsGroupCtrl REL_ELEV_IDC) ctrlSetText str _relElev;
(_grp controlsGroupCtrl DIST_IDC) ctrlSetText format["%1m", _distance];
(_grp controlsGroupCtrl ELEV_IDC) ctrlSetText format["%1m", _altDiff];
(_grp controlsGroupCtrl ETA_IDC) ctrlSetText format["%1s", _eta];
(_grp controlsGroupCtrl SOL_SHELL_IDC) ctrlSetText getText(configFile >> "cfgMagazines" >> _mag >> "displayName");

private _solMode = "NaN";
{
	_x params ["_mode", "_charge", "_dsp"];
    if ( _solCharge isEqualTo _charge ) exitWith { _solMode = _dsp; };
} forEach _modes;
(_grp controlsGroupCtrl SOL_MODE_IDC) ctrlSetText _solMode;