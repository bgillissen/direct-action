#include "define.hpp"

disableSerialization;

params [["_dsp", displayNull]];

private _grp = _dsp displayCtrl PANEL_IDC;
private _bcg = _dsp displayCtrl BCG_IDC;

private _veh = vehicle player;
private _weapon = [player] call arti_fnc_getWeapon;
private _customModes = (["artiComputer", "modes"] call core_fnc_getSetting);
(assignedVehicleRole player) params[["_role", ""], ["_turretPath", []]];
private _modes = [];
{
    _x params ["_mode", "_charge", "_minRange", "_maxRange", "_inRange"];
	_modes pushback [_mode, _charge, (_customModes select _forEachIndex)];
} forEach ([_veh, _turretPath] call arti_fnc_getModes);
_customModes = nil;

private _knownAnims = ['artiComputer', 'anims'] call core_fnc_getSetting;
private "_anim";
{
	if ( _x in _knownAnims ) exitWith { _anim = _x; };
} forEach (animationNames _veh);
_knownAnims = nil;
private _elevOffset = 0;
if ( _veh isKindOf "StaticMortar" ) then {
	{
		if ( _veh isKindOf (configName _x) ) exitWith { _elevOffset = getNumber _x; };
	} forEach (configProperties [missionconfigFile >> "settings" >> "artiComputer" >> "elevOffsets"]); 
};

private _drawnSolution = [];
private _drawnShells = [];

while { true } do {
	private _shown = ( (cameraOn isEqualTo _veh) && (cameraView isEqualTo "GUNNER") );
    _grp ctrlShow _shown;
    _bcg ctrlShow _shown;
	if ( _shown ) then {
        if !( _drawnSolution isEqualTo artiComputer_solution ) then {
             _drawnSolution = artiComputer_solution + [];
             [_grp, _modes, _drawnSolution] call acGunner_fnc_updateSolution;
        };
        if !( _drawnShells isEqualTo artiComputer_shells ) then {
            _drawnShells = artiComputer_shells + [];
            [_dsp, _grp] call acGunner_fnc_buildShell;
        } else {
            [_grp, _drawnShells] call acGunner_fnc_updateShell;
        };
        [_grp, _veh, _turretPath, _weapon, _modes, _elevOffset, _anim, _drawnSolution] call acGunner_fnc_updateState;
    };        
    sleep (1 / 25);      
};