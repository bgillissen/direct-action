

#include "define.hpp" 

params [["_grp", controlNull], ["_shells", []]];

private _shellCnt = _grp controlsGroupCtrl SHELL_CNT_IDC;
private _shellGrp = _grp controlsGroupCtrl SHELLS_IDC;

private _c = 0;
{
    _x params ["_mag", "_shell", "_shotAt", "_eta"];
    private _ctrl = _shellGrp controlsGroupCtrl (SHELL_BASE_IDC + _forEachIndex);
    private _time = (_ctrl controlsGroupCtrl SHELL_TIME_IDC);
    private _prgB = (_ctrl controlsGroupCtrl SHELL_PRGB_IDC);
    private _timeLeft = (_shotAt + _eta - time);
    if ( _timeLeft < SHELL_SHOWN_AFTER_IMPACT ) then {
        artiComputer_shells deleteAt _forEachIndex;
    } else {
        if ( _timeLeft <= 0 ) then {
            _timeLeft = "0s";
            _prgB ctrlSetBackgroundColor [1, 0.4, 0.4, 0.7];
            _prgB ctrlCommit 0;
		} else {
            _c = _c + 1;
            _timeLeft = format["%1s", ([_timeLeft, 1] call common_fnc_roundTo)];
        };
		_time ctrlSetText _timeLeft;             
    };
} forEach _shells;
  
_shellCnt ctrlSetText format["In-Flight Shells (%1)", _c];