/*
@filename: feats\artiComputer\playerGetout.sqf
Author:
	Ben
Description:
	run on player,
	called when player leave a vehicle.
	terminate the instrument panel update thread,
	remove the artillery computer action,
	remove the fire event handler,
	reset the fire solution
*/

#include "_debug.hpp"

if ( (["artiComputer"] call core_fnc_getParam) isEqualTo 0 ) exitWith {
	#ifdef DEBUG
   	debug(LL_DEBUG, "disabled by mission parameter");
    #endif
    nil  
};
           		
if !( isNull artiComputer_driverThread ) then { 
	"acDriver" cutFadeOut 0.01; 
	terminate artiComputer_driverThread;
	artiComputer_driverThread = scriptNull;
};

if !( isNull artiComputer_gunnerThread ) then { 
	"acGunner" cutFadeOut 0.01;
	terminate artiComputer_gunnerThread;
	artiComputer_gunnerThread = scriptNull; 
};

if ( artiComputer_action >= 0 ) then {
	player removeAction artiComputer_action;
	artiComputer_action = -1;
};

artiComputer_solution = [];

nil