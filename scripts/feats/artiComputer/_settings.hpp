
class artiComputer {
	computeDuration = 30;
	modes[] = {"Close", "Medium", "Far", "Further", "Extreme", "Insane", "Orbital"};
	fuseDsp[] = {"Impact", "Low", "Medium", "High"};
	fuseAlt[] = {0, 5, 10, 20};
	driverInfo = "<t size='1' color='#ffffff'>Pitch: %1<br/>Bank: %2</t>";
	anims[] = {"maingun", "uplegs_zeroing", "MainGun"};
	class elevOffsets {
		Mortar_01_base_F = 75;
		RHS_M252_Base = 44.37;
		rhs_2b14_82mm_Base = 54.9999;
	};
};
