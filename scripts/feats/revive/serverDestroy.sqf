/*
@filename: feats\revive\serverDestroy.sqf
Author:
	Ben
Description:
	run on server,
	kill serverInit thread,
	empty JIP queue, empty bleedStack, remove spawned healEquipment
*/

if ( MOD_ace ) exitWith { nil };

if ( count(_this) == 0 ) exitWith { nil };

params ["_when", "_thread"];

if ( scriptDone _thread ) exitWith { nil };

terminate _thread;

waitUntil{ scriptDone _thread };

{
	_x params ["_uid", "_jip"];
	remoteExec["", _jip];
} forEach reviveJIPServer;
reviveJIPServer = [];

reviveBleedStackServer = [];

{
	_x params ["_obj", "_expire"];
	deleteVehicle _obj;
} forEach reviveHealEquipmentServer;

reviveHealEquipmentServer = [];

nil