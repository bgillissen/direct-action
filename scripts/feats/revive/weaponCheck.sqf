
private _primWeapon = primaryWeapon player;
private _curWeapon = currentWeapon player;

if ( _primWeapon isEqualTo "" ) exitWith {
	player addWeapon "FakeWeapon";
	player selectWeapon "FakeWeapon";
	true
};

if !( _primWeapon isEqualTo _curWeapon ) exitWith {
	player selectWeapon _primWeapon;
    true
};

false