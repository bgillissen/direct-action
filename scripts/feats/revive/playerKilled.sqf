/*
@filename: feats\radioFreq\playerKilled.sqf
Author:
	Ben
Description:
	run on player,
	create a camera span around player and display a random quote
*/

#include "_debug.hpp"

if ( (["revive", "deadCam"] call core_fnc_getSetting) == 0 ) exitWith {
    #ifdef DEBUG
	debug(LL_DEBUG, "deadCam is disabled by mission setting");
    #endif
    nil
};

params ["_victim", "_killer"];
 
(getPosATL player) params ["_x", "_y", "_z"];
(vectorDir player) params ["_vx", "_vy"];
private _pos = [(_x - _vx * 3), (_y - _vy * 3), (_z + 1)];

titleCut ["", "BLACK IN", 1];

private _deadcam = "Camera" camCreate (position player);
_deadcam cameraEffect ["internal", "back"];
showCinemaBorder true;
_deadcam camPrepareTarget player;
_deadcam camPreparePos _pos;
_deadcam camPrepareFOV 0.7;
_deadcam camCommitPrepared 0;

private _quoteConf = selectRandom ("true" configClasses (missionConfigFile >> "settings" >> "revive" >> "quotes"));
private _quote = getText(_quoteConf >> "quote");
private _author = getText(_quoteConf >> "author");
private _respawnDelay = getNumber(missionConfigFile >> "respawndelay") max 1;
[_quote, _author, (_respawnDelay - 1)] spawn revive_fnc_deadCamQuote;

waitUntil { camCommitted _deadcam };

if ( (_killer isEqualTo player) || !(alive _killer) || (isNull _killer) ) then {
	_deadcam camPrepareTarget player;
	_deadcam camsetrelpos [-3, 20, 10];
	_deadcam camPrepareFOV 0.474;
	_deadcam camCommitPrepared 20;
} else {
	sleep 1;
	_deadcam camCommand "inertia on";
	_deadcam camPrepareTarget (vehicle player);
	_deadcam camsetrelpos [-3, 20, 10];
	_deadcam camPrepareFOV 1;
	_deadcam camCommitPrepared 10;
};

waitUntil { alive player };

showCinemaBorder false;
player cameraEffect ["terminate", "back"];
camDestroy _deadcam;

nil