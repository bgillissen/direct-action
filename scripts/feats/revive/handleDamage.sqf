
BIS_hitArray = _this;
BIS_wasHit = true;

#include "_debug.hpp"

#define THRESHOLD 1
#define DEADLYPARTS ["head", "face_hub", "pelvis", "body", "neck", "spine", "spine1", "spine2", "spine3", ""]

params ["_unit", "_bodypart", "_damage", "_source", "_projectile", "_hitPartIndex", ["_instigator", objNull], "_hitPoint"];

if ( _damage == 0 ) exitWith { 0 };
if !( alive _unit ) exitWith {	0 };

private _wasInAgony = _unit getVariable ["agony", false];
private _hasMoved = _unit getVariable ["hasMoved", false];

private _allHitPoints = (getAllHitPointsDamage _unit);

private "_hitIndex";
if ( _bodypart isEqualTo "" ) then {
    _hitIndex = (count (_allHitPoints select 2)) - 1;
} else {
    _hitIndex = (_allHitPoints select 1) find _bodyPart;
};

private ["_hitPoints", "_oldDamage", "_hitDamage", "_agonyDamage"];
if ( _wasInAgony ) then {
	_hitPoints = _unit getVariable "hitPoints";
    _agonyDamage = (reviveAgonyHitPoints select _hitIndex);
	_oldDamage =  ( (_hitPoints select _hitIndex) - _agonyDamage );
	_hitDamage = ( _damage / reviveResistance );
} else {
	_hitPoints = _allHitPoints select 2;
     _agonyDamage = 0;
    _oldDamage =  _hitPoints select _hitIndex;
    _hitDamage = ( (_damage - _oldDamage) / reviveResistance );    
};

private _newDamage = ( _oldDamage + _hitDamage );

#ifdef DEBUG
_debug = format ["handleDamage : part: %1 (%2) | arg: %3 | old: %4 | hit: %5 | agony: %6 | new: %7", 
				_bodypart, _hitIndex, _damage, _oldDamage, _hitDamage, _agonyDamage, _newDamage];
debug(LL_DEBUG, _debug);
#endif

if ( reviveImpactEffect ) then { 
	_hitDamage call revive_fnc_sfxImpactEffect; 
};

if ( _newDamage > reviveBloodThreshold && reviveBloodParticle && !(_bodypart isEqualTo "") ) then { 
	_bodyPart call revive_fnc_addBleeding; 
};

private _dead = false;
if ( _wasInAgony && _hasMoved ) then {
	private "_threshold";
	if ( (toLower _bodyPart) in DEADLYPARTS ) then { 
    	_threshold = ( THRESHOLD * reviveDeadlyPartThreshold ); 
     } else {
         _threshold = ( THRESHOLD  * revivePartThreshold );
     };    
	_dead = ( _newDamage >= _threshold );
};

if ( _dead ) exitWith {
    #ifdef DEBUG
    debug(LL_DEBUG, "handleDamage : death");
    #endif
	[_unit, _source, _projectile, _instigator] call revive_fnc_tkScore;
	1.01
};

private _agony = ( _wasInAgony || (_newDamage >= THRESHOLD) );

if ( _agony ) exitWith { 
    if !( _wasInAgony ) then {
		#ifdef DEBUG
    	debug(LL_DEBUG, "handleDamage : enter agony");
    	#endif
        _unit setVariable ["agony", true, true];
        _hitPoints set [_hitIndex, _newDamage];
        { reviveAgonyHitPoints set [_forEachIndex, (_x + 0)]; } forEach _hitPoints;
		[_unit, _source, _projectile, _instigator] call revive_fnc_tkScore;
	} else {
	   _hitPoints set [_hitIndex, (_newDamage + _agonyDamage)];
    };
    _unit setVariable ["hitPoints", _hitPoints, true];
    0
};

_newDamage