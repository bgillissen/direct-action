/*
@filename: feats\revive\actions\inVehHeal.sqf
Author:
	Ben
Description:
	run on player,
*/

#include "..\_debug.hpp"

params ["_veh", "_healer", "_id", "_wounded"];

#ifdef DEBUG
private _debug = format["action inVehHeal: %1 --- %2 --- %3", time, _veh, _wounded];
debug(LL_DEBUG, _debug);
#endif

player setVariable ["action", [], true];
player setVariable ["finished", false, true];
player setVariable ["stopped", false, true];

private _inMedEvac = ( (getNumber(configFile >> "cfgVehicles" >> (typeof _veh) >> "attendant") > 0) || ((typeOf _veh) in reviveMedEvacs) );
if ( _inMedEvac ) then { _wounded setVariable ["medEvac", _veh, true]; };
_wounded setVariable ["healer", player, true];

waitUntil {
    sleep 0.1;
    ( !((player getVariable "action") isEqualTo []) || !(alive _wounded) || (isNull _wounded) || !(alive _veh) || !((vehicle player) isEqualTo _veh) || !((vehicle _wounded) isEqualTo _veh))
};

if ( !(alive _wounded) || (isNull _wounded) ) exitWith {
	#ifdef DEBUG
	debug(LL_DEBUG, "action inVehHeal: wounded is gone, abording");
	#endif
    reviveActionThread = scriptNull;    
};

if (  !((vehicle _wounded) isEqualTo _veh) || !((vehicle player) isEqualTo _veh) || !(alive _veh) ) exitWith {
    #ifdef DEBUG
	debug(LL_DEBUG, "action inVehHeal: player or wounded disembarked or veh got destroyed, abording");
	#endif
    reviveActionThread = scriptNull;
};

reviveActionThread = [_wounded, _veh] spawn revive_fnc_playerHealing;