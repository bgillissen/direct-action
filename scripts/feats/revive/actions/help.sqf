/*
@filename: feats\revive\actions\help.sqf
Author:
	Ben
Description:
	run on player,
*/

#include "..\_debug.hpp"

params ["_wounded"];

player setVariable ["action", [], true];
player setVariable ["stopped", false, true];
_wounded setVariable ["helper", player, true];

waitUntil {
    sleep 0.1;
    ( !((player getVariable "action") isEqualTo []) || !(alive _wounded) || (isNull _wounded)  )
};

if ( !(alive _wounded) || (isNull _wounded) ) exitWith {
	#ifdef DEBUG
	debug(LL_DEBUG, "action help: wounded is gone, abording");
	#endif
    reviveActionThread = scriptNull;    
};

reviveActionThread = [_wounded] spawn revive_fnc_playerHelping;