/*
@filename: feats\revive\actions\vehHeal.sqf
Author:
	Ben
Description:
	run on player,
*/

#include "..\_debug.hpp"

params ["_wounded"];

private _medEvacs = (player nearEntities [reviveMedEvacs, reviveLoadDistance]);
{
	if ( getNumber(configFile >> "cfgVehicles" >> (typeof _x) >> "attendant") > 0 ) then { _medEvacs pushbackUnique _x; };
} forEach (player nearEntities [reviveMedEvacEntities, reviveLoadDistance]);
    
if ( count _medEvacs == 0 ) exitWith {
    (["revive", "medEvac", "noneNearby"] call core_fnc_getSetting) call revive_fnc_dynamicText;
};

{
    (velocity _x) params ["_vX", "_vY", "_vZ"];
	if ( _vx > 0 || _vY > 0 || _vZ > 0 ) then { _medEvacs deleteAt _forEachIndex; };
} forEach _medEvacs;

if ( count _medEvacs == 0 ) exitWith {
   (["revive", "medEvac", "noneStopped"] call core_fnc_getSetting) call revive_fnc_dynamicText;  
};

private _medEvac = (_medEvacs select 0);

player setVariable ["action", [], true];
player setVariable ["finished", false, true];
player setVariable ["stopped", false, true];
_wounded setVariable ["medEvac", _medEvac, true];
_wounded setVariable ["healer", player, true];

waitUntil {
    sleep 0.1;
    ( !((player getVariable "action") isEqualTo []) || !(alive _wounded) || (isNull _wounded)  )
};

if ( !(alive _wounded) || (isNull _wounded) ) exitWith {
	#ifdef DEBUG
	debug(LL_DEBUG, "action vehHeal: wounded is gone, abording");
	#endif
    reviveActionThread = scriptNull;    
};

reviveActionThread = [_wounded] spawn revive_fnc_playerHealing;