/*
@filename: feats\revive\actions\condition.sqf
Author:
	Ben
Description:
	run on player,
*/

#include "..\_debug.hpp"

params ["_action", "_target", ["_uid",""]];

//no action when _target is dead
if !( alive _target ) exitWith { false };

//to load while carrying, dragging
if ( _action isEqualTo "CDLoad" ) exitwith {
    (player getVariable ["action", []]) params [["_act", ""], ["_wnd", objNull]];
    if !( isNull (_wnd getVariable['inVeh', objNull]) ) exitWith { false };
    private _handler = objNull;  
    switch ( _act ) do {
        case "drag": {_handler = (_wnd getVariable ["dragger", objNull]); };
        case "carry": {_handler = (_wnd getVariable ["carrier", objNull]); };
    };
	if ( (isNull _handler) || !(_handler isEqualto player) ) exitWith { false };
	( ({(locked _x < 2)} count (player nearEntities [reviveLoadable, reviveLoadDistance])) > 0 )
};

//we already started an action, still waiting for reply from wounded to trigger the action
if !( isNull reviveActionThread ) exitWith { false };

//search for the wounded for the vehicle actions
private _wounded = _target;
if ( _action in ["unload", "inVehHeal", "moveToCargo"] ) then { 
    {
        if ( _uid isEqualTo (getPlayerUID _x) ) exitWith { _wounded = _x; };
    } forEach allPlayers;
};

//checking that wounded has not already been taken in charge
if ( ({!isNull(_wounded getVariable [_x, objNull])} count ["carrier", "dragger", "healer", "helper", "unloader"]) > 0 ) exitWith { false };

//to load, we need a vehicle close enough
if ( _action isEqualTo "load" ) exitwith {
    if !( isNull (_wounded getVariable['inVeh', objNull]) ) exitWith { false };
	( ({(locked _x < 2)} count (player nearEntities [reviveLoadable, reviveLoadDistance])) > 0 )
};

//we are already doing something
if !( (player getVariable ["action", []]) isEqualTo [] ) exitWith { false };

//to unload, vehicle got to be almost stopped
if ( _action isEqualTo "unload" ) exitwith {
	(velocity _target) params ["_vX", "_vY", "_vZ"];
	( _vx <= 1 && _vY <= 1 && _vZ <= 1 )
};

//to heal inside a vehicle
if ( _action isEqualTo "inVehHeal" ) exitWith {
    if !( (vehicle player) isEqualTo _target ) exitWith { false };
	if ( player isEqualTo (driver _target) ) exitWith { false };
    if ( isNull _wounded ) exitWith { false };
    if !( (vehicle _wounded) isEqualTo _target ) exitWith { false };
    if ( (getNumber(configFile >> "cfgVehicles" >> (typeof _target) >> "attendant") > 0) || ((typeOf _target) in reviveMedEvacs) ) exitWith { true };
    private _out = true;
    if ( reviveRequireMedikit ) then {
        if !( [_target, reviveMedikit] call revive_fnc_gotItem ) then {
            if !( [_wounded, reviveMedikit] call revive_fnc_gotItem ) then {
            	if !( [player, reviveMedikit] call revive_fnc_gotItem ) then { _out = false; };
			};             
		};
    };
    if !( _out ) exitWith { false };
    if ( reviveConsumeFaks ) then {
		if !( [_target, reviveFaks] call revive_fnc_gotItem ) then {
            if !( [_wounded, reviveFaks] call revive_fnc_gotItem ) then {
            	if !( [player, reviveFaks] call revive_fnc_gotItem ) then { _out = false; };
			};
		};
	};
    _out
};

//to move wounded to a cargo seat
if ( _action isEqualTo "moveToCargo" ) exitWith {
    if !( (vehicle player) isEqualTo _target ) exitWith { false };
	if !( _wounded isEqualTo (driver _target) ) exitWith { false };
    if ( isNull _wounded ) exitWith { false };
    if !( (vehicle _wounded) isEqualTo _target ) exitWith { false };
    //any free seat other than driver and gunner ?
    private _crew = fullcrew [_target, "", true];
	private _bool = false;
	{
    	_x params ["_unit", "_role", "_index", "_turret", "_pTurret"];
    	if ( !( _role in ["driver", "Driver", "gunner", "Gunner"]) && (isNull _unit) ) then { 
        	if ( _role in ["turret", "Turret"] ) then {
        		if ( _pTurret ) then { _bool = true; };    
        	} else {
    			_bool  = true;
			};
		};
	} forEach _crew;
	_bool
};

//not too far from _target
if ( player distance _wounded > 3 ) exitWith { false };

//check water depth
private _pos = (getPosASL player);
private _bool = true;
if ( surfaceIsWater _pos ) then {
    if ( (_pos select 2) < 0 ) then {
        private "_maxDepth";
		if ( _action isEqualTo 'drag' ) then { _maxDepth = 0.8; };
		if ( _action isEqualTo 'carry' ) then { _maxDepth = 1.45; };
		if ( _action in ['help', 'heal', 'vehHeal'] ) then { _maxDepth = 0.2; };
        _bool = ((abs getTerrainHeightASL _pos) < _maxDepth );
	} else {
		_bool = (isTouchingGround player);            
    };
};
//not with a backpack on chest
if ( _bool && BOC_onChest ) then { 
	_bool = !( _action in ['drag', 'carry', 'help'] ) 
};

if !( _bool ) exitWith { false };

//to heal
if ( _action isEqualTo "heal" ) exitWith {
    private _out = true;
    if ( reviveRequireMedikit ) then {
        if !( [_wounded, reviveMedikit] call revive_fnc_gotItem ) then {
            if !( [player, reviveMedikit] call revive_fnc_gotItem ) then { _out = false; };             
		};
    };
    if !( _out ) exitWith { false };
    if ( reviveConsumeFaks ) then {
		if !( [_wounded, reviveFaks] call revive_fnc_gotItem ) then {
            if !( [player, reviveFaks] call revive_fnc_gotItem ) then { _out = false; };
		};
	};
    _out
};

//to heal at a medEvac, check proximity of one the vehicle from the pools or an attendant vehicle
if ( _action isEqualTo "vehHeal" ) exitWith {
    private _medEvacs = (player nearEntities [reviveMedEvacs, reviveLoadDistance]);
	{
   		if ( getNumber(configFile >> "cfgVehicles" >> (typeof _x) >> "attendant") > 0 ) then { _medEvacs pushbackUnique _x; };
	} forEach (player nearEntities [reviveMedEvacEntities, reviveLoadDistance]);
	if ( count _medEvacs <= 0 ) exitWith { false };
    true
};

true