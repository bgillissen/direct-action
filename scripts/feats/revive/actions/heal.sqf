/*
@filename: feats\revive\actions\heal.sqf
Author:
	Ben
Description:
	run on player,
*/

#include "..\_debug.hpp"

params ["_wounded"];

#ifdef DEBUG
private _debug = format["action heal: %1 --- %2", time, _wounded];
debug(LL_DEBUG, _debug);
#endif

player setVariable ["action", [], true];
player setVariable ["finished", false, true];
player setVariable ["stopped", false, true];
_wounded setVariable ["healer", player, true];

waitUntil {
    sleep 0.1;
    ( !((player getVariable "action") isEqualTo []) || !(alive _wounded) || (isNull _wounded)  )
};

if ( !(alive _wounded) || (isNull _wounded) ) exitWith {
	#ifdef DEBUG
	debug(LL_DEBUG, "action heal: wounded is gone, abording");
	#endif
    reviveActionThread = scriptNull;    
};

reviveActionThread = [_wounded] spawn revive_fnc_playerHealing;