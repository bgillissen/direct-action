/*
@filename: feats\revive\actions\moveToCargo.sqf
Author:
	Ben
Description:
	run on player,
*/

#include "..\_debug.hpp"

params ["_veh", "_mover", "_id", "_wounded"];

#ifdef DEBUG
private _debug = format["action moveToCargo: %1 --- %2 --- %3", time, _wounded, (owner _wounded)];
debug(LL_DEBUG, _debug);
#endif

private _vehName = getText(configFile >> "cfgVehicles" >> (typeOf _veh) >> "displayName");

private _crew = fullcrew [_veh, "", true];
private _freeSeats = [];
{
    _x params ["_unit", "_role", "_index", "_turret", "_pTurret"];
    if ( !( _role in ["driver", "Driver", "gunner", "Gunner"]) && (isNull _unit) ) then {
        if ( _role in ["turret", "Turret"] ) then {
        	if ( _pTurret ) then {
                _freeSeats pushback _x;
            };    
        } else {
    		_freeSeats pushback _x;
		}; 
	};
} forEach _crew;

if ( (count _freeSeats) isEqualTo 0 ) exitWith {
    #ifdef DEBUG
    private _debug = format["action moveToCargo : veh crew: %1", _crew];
    debug(LL_DEBUG, _debug)
    #endif
    private _msg = (["revive", "moveToCargo", "noCargoSeat"] call core_fnc_getSetting);
	format[_msg, _vehName] call revive_fnc_dynamicText; 
};

private _seat = (selectRandom _freeSeats);

[_veh, _seat, _mover] remoteExec ["revive_fnc_woundedMoved", _wounded, false];