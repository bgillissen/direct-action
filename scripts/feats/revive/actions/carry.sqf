/*
@filename: feats\revive\actions\carry.sqf
Author:
	Ben
Description:
	run on player,
*/

#include "..\_debug.hpp"

params ["_wounded"];

#ifdef DEBUG
private _debug = format["action carry: %1 --- %2", time, _wounded];
debug(LL_DEBUG, _debug);
#endif

if ( (["revive", "carry", "withLauncher"] call core_fnc_getSetting) == 0 ) then {
	if !( (secondaryWeapon player) isEqualTo "" ) exitWith {
     	(["revive", "carry", "message"] call core_fnc_getSetting) call revive_fnc_dynamicText; 
	};
};

player setVariable["action", [], true];
_wounded setVariable ["carrier", player, true];

waitUntil {
    sleep 0.1;
    ( !((player getVariable "action") isEqualTo []) || !(alive _wounded) || (isNull _wounded)  )
};

if ( !(alive _wounded) || (isNull _wounded) ) exitWith {
	#ifdef DEBUG
	debug(LL_DEBUG, "action carry: wounded is gone, abording");
	#endif
    reviveActionThread = scriptNull;    
};

reviveActionThread = [_wounded] spawn revive_fnc_playerCarrying;