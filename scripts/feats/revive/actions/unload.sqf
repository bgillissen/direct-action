/*
@filename: feats\revive\actions\unload.sqf
Author:
	Ben
Description:
	run on player,
*/

#include "..\_debug.hpp"

params ["_veh", "_unloader", "_id", "_wounded"];

if !( (vehicle _wounded) isEqualTo _veh ) exitWith {
    [(getPlayerUID _wounded), _veh] call revive_fnc_removeVehActions;
};

_wounded setVariable ["unloader", player, true];