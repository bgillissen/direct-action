/*
@filename: feats\revive\actions\drag.sqf
Author:
	Ben
Description:
	run on player,
*/

#include "..\_debug.hpp"

params ["_wounded"];

#ifdef DEBUG
private _debug = format["action drag: %1 --- %2", time, _wounded];
debug(LL_DEBUG, _debug);
#endif

player setVariable["action", [], true];
_wounded setVariable ["dragger", player, true];

waitUntil {
    sleep 0.1;
    (	!(alive _wounded) 
        || (isNull _wounded)
		|| !(alive player) 
      	|| (player getVariable["agony", false]) 
        || !((player getVariable "action") isEqualTo []) )
};

if ( !(alive _wounded) || (isNull _wounded) ) exitWith {
	#ifdef DEBUG
	debug(LL_DEBUG, "action drag: wounded is gone, abording");
	#endif
    reviveActionThread = scriptNull;    
};

reviveActionThread = [_wounded] spawn revive_fnc_playerDragging;