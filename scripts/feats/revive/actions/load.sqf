/*
@filename: feats\revive\actions\load.sqf
Author:
	Ben
Description:
	run on player,
*/

#include "..\_debug.hpp"

params ["_wounded"];

#ifdef DEBUG
private _debug = format["action load: %1 --- %2 --- %3", time, _wounded, (owner _wounded)];
debug(LL_DEBUG, _debug);
#endif

private _unsorted = (position player) nearEntities [reviveLoadable, reviveLoadDistance];

if ( (count _unsorted) isEqualTo 0 ) exitWith {
	(["revive", "load", "noneNearby"] call core_fnc_getSetting) call revive_fnc_dynamicText;
};

private _filtered = [];
{
    (velocity _x) params ["_vX", "_vY", "_vZ"];
    if ( _vx <= 1 && _vY <= 1 && _vZ <= 1 && (locked _x < 2) ) then { _filtered pushback _x; };
} forEach _unsorted;
_unsorted = nil;

if ( (count _filtered) isEqualTo 0 ) exitWith {
	 (["revive", "load", "noneStopped"] call core_fnc_getSetting) call revive_fnc_dynamicText;
};

private _pos = getPos player;
private _closest = _filtered select 0;
{
   	if ((getPos _x distance _pos) < (getPos _closest distance _pos)) then { _closest = _x; };
} forEach _filtered;

private _vehName = getText(configFile >> "cfgVehicles" >> (typeOf _closest) >> "displayName");

private _crew = fullcrew [_closest, "", true];
private _freeSeats = [];
{
    _x params ["_unit", "_role", "_index"];
    if ( !( _role in ["driver", "Driver", "gunner", "Gunner"]) && (isNull _unit) ) then { _freeSeats pushback _index; };
} forEach _crew;

(player getVariable ["action", []]) params [["_action", ""], ["_actWounded", objNull]];
private _wasHandling = ( !(_action isEqualTo "") && (_actWounded isEqualTo _wounded) );

if ( (count _freeSeats) isEqualTo 0 ) exitWith {
    #ifdef DEBUG
    private _debug = format["action load : veh crew: %1", _crew];
    debug(LL_DEBUG, _debug)
    #endif
    private _msg = (["revive", "load", "noCargoSeat"] call core_fnc_getSetting);
	format[_msg, _vehName] call revive_fnc_dynamicText; 
    if ( _wasHandling ) then { [player, player, (reviveDropAction select 1), _wounded] call revive_fnc_actionDrop; };
};

if ( _wasHandling ) then {
    terminate reviveActionThread;
	reviveActionThread = scriptNull;
	player setVariable ["action", [], true];
	[_wounded] call revive_fnc_removeDropAction;
    //player removeAction reviveDropAction;
    //player removeAction reviveCDLAction;
	//reviveDropAction = -1;
    //reviveCDLAction = -1;
    if ( (vehicle player) isEqualTo player ) then { [player, revivePreAnim, 1] call common_fnc_doAnim; };
    if ( (primaryWeapon player) isEqualTo "FakeWeapon" ) then { player removeWeapon "FakeWeapon"; };
};

private _msg = (["revive", "load", "loading"] call core_fnc_getSetting);
format[_msg, (name _wounded), _vehName] call revive_fnc_dynamicText;

_wounded setVariable ["loadInVeh", [_closest, _freeSeats, player], true];