/*
@filename: feats\revive\actions\drop.sqf
Author:
	Ben
Description:
	run on player,
*/

#include "..\_debug.hpp"

params ["_target", "_caller", "_id", "_wounded"];

#ifdef DEBUG
private _debug = format["action drop: %1 --- %2", time, _wounded];
debug(LL_DEBUG, _debug);
#endif

_wounded setVariable ["droper", player, true];