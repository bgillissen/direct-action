
params ["_unit", "_source", "_projectile", "_instigator"];

private _killer = _instigator;

if ( isNull _instigator ) then {
    _killer = _source;
	if !( (vehicle _source) isEqualTo _source ) then {
        if ( _projectile isEqualTo "" ) then {
            _killer = (((fullCrew [_source, "driver", false]) select 0) select 0);
            if ( isNull _killer ) then {
                _killer = (((fullCrew [_source, "gunner", false]) select 0) select 0);
            };
        } else {
            _killer = (((fullCrew [_source, "commander", false]) select 0) select 0);
            if ( isNull _killer ) then {
                _killer = (((fullCrew [_source, "gunner", false]) select 0) select 0);
                if ( isNull _killer ) then {
                    _killer = (((fullCrew [_source, "turret", false]) select 0) select 0);
                };
            };
		};
	};        
};
if ( _killer in allPlayers ) then {
	reviveScore = [_killer, -1];
	publicVariableServer "reviveScore";
};