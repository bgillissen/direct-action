/*
@filename: feats\revive\playerInit.sqf
Credit:
	psycho 
Author:
	Ben
Description:
	run on player,
	define some global variables,
	define hitpoints array on player unit,
	start blood FSM if needed
	start wounded icons drawing if needed
*/

#include "_debug.hpp"

if ( MOD_ace ) exitWith {
    #ifdef DEBUG
    debug(LL_INFO, "disabled by the presence of ACE");
    #endif
    nil
};

if ( isNil "reviveVehActionStack" ) then { reviveVehActionStack = []; };

reviveDebug = {};
#ifdef DEBUG
reviveDebug = { debug(LL_DEBUG, _this); };
#endif

reviveOnlyMedic = (["reviveOnlyMedic"] call core_fnc_getParam isEqualTo 1);
reviveRequireMedikit = (["reviveRequireMedikit"] call core_fnc_getParam isEqualTo 1);
reviveConsumeFaks = (["reviveConsumeFaks"] call core_fnc_getParam isEqualTo 1);
reviveMediKit = (["revive", "mediKitClass"] call core_fnc_getSetting);
reviveFaks = (["revive", "faksClass"] call core_fnc_getSetting);
reviveLoadable = (["revive", "loadable"] call core_fnc_getSetting);
reviveMoveable = (["revive", "moveable"] call core_fnc_getSetting);
reviveLoadDistance = (["revive", "loadDistance"] call core_fnc_getSetting);
reviveImpactEffect = [false, true] select (["revive", "impact"] call core_fnc_getSetting);
reviveBloodParticle	= [false, true] select (["revive", "bloodParticle"] call core_fnc_getSetting);
reviveResistance = (["revive", "resistance"] call core_fnc_getSetting);
reviveBloodThreshold = ["revive", "bloodThreshold"] call core_fnc_getSetting;
revivePartThreshold = (["revive", "agony", "partThreshold"] call core_fnc_getSetting);
reviveDeadlyPartThreshold = (["revive", "agony", "deadlyPartThreshold"] call core_fnc_getSetting);
reviveMedEvacEntities = (["revive", "medEvac", "entities"] call core_fnc_getSetting);
reviveLadderAnims = (["revive", "ladderAnims"] call core_fnc_getSetting);
reviveMedEvacs = BV_heliMedEvac + BV_landMedic + ["Land_MedicalTent_01_MTP_closed_F", "B_Slingload_01_Medevac_F"];
reviveAgonyHitPoints = [];
reviveDropAction = [objNull, -1, -1];
reviveNoMoveEH = -1;
private _mats = getArray(configfile >> "CfgVehicles" >> (typeOf player) >> "Wounds" >> "mat");
reviveCleanMat = _mats select 0;
reviveBloodyMat = _mats  select 1;

reviveJIPAgonyStart = "";
reviveJIPHasMoved = "";
reviveJIPHasShot = "";
reviveJIPInVeh = "";

if ( isNil "reviveDamageEH" ) then {
	[] spawn {
		waitUntil {
			private _var = (player getVariable "BIS_fnc_feedback_hitArrayHandler");
			!(isNil "_var") 
		};
        #ifdef DEBUG
    	debug(LL_DEBUG, "damage handler has been registered");
    	#endif
		player removeAllEventHandlers "handleDamage";
		reviveDamageEH = player addEventHandler ["HandleDamage", {_this call revive_fnc_handleDamage}];
	};
};

reviveBleedStack = [];
reviveBloodParticle = false;
if ( (["revive", "bloodParticle"] call core_fnc_getSetting) == 1 ) then {
    if ( isNil "reviveBloodPVEH" ) then {
        reviveBloodPVEH = true;
		"reviveAddBlood" addPublicVariableEventHandler {
            (_this select 1) params ["_nunit", "_nuid", "_nbodyPart"];
			private _add = true;
			{
				_x params ["_unit", "_uid", "_bodyPart"];
				if ( _unit isEqualTo _nunit ) then {
					if ( _bodyPart isEqualTo _nbodyPart ) then { _add = false; };
				};
			} forEach reviveBleedStack;
			if ( _add ) then {
                #ifdef DEBUG
        		private _debug = format["bloodParticle : adding --- unit: %1 --- part: %2", _nunit, _nbodyPart];
        		debug(LL_DEBUG, _debug);
        		#endif 
            	reviveBleedStack pushback (_this select 1); 
			};   
		};
		"reviveDelBlood" addPublicVariableEventHandler {
            (_this select 1) params ["_wounded", ["_part", ""]];
           	{
				_x params ["_unit", "_uid", "_bodyPart"];
				if ( _unit isEqualTo _wounded ) then {
					if ( (_part isEqualTo _bodyPart) || (_part isEqualTo "") ) then {
                        #ifdef DEBUG
        				private _debug = format["bloodParticle : stopping --- unit: %1 --- part: %2", _unit, _bodyPart];
        				debug(LL_DEBUG, _debug);
        				#endif
                        _x set [5, true]; 
						reviveBleedStack set [_forEachIndex, _x];
					};
				};
			} forEach reviveBleedStack;  
		};
	};
    reviveBleedStack = reviveBleedStackServer;          
    reviveBloodParticle = true;
	reviveBloodFSM = execFSM ("feats\revive\fsm\bloodParticles.fsm");
};

reviveIconDistance = (["reviveIconDistance"] call core_fnc_getParam);
if ( reviveIconDistance > 0 ) then {
	if ( (isNil "reviveIconEH") && ((player getVariable "role") isEqualto "medic") ) then {
        #ifdef DEBUG
        private _debug = format["wounded icons in 3D space added, distance: %1", reviveIconDistance];
		debug(LL_DEBUG, _debug);
    	#endif
		reviveIconEH  = addMissionEventHandler ["Draw3D", {call revive_fnc_drawIcon3D}];
	};
#ifdef DEBUG
} else {
	debug(LL_DEBUG, "wounded icons in 3D space disabled by mission parameter");
#endif
};

nil