class revive_fnc_otherAgonyStart {
	allowedTargets=0;
	jip=1;
};
class revive_fnc_otherAgonyStop {
	allowedTargets=0;
	jip=0;
};
class revive_fnc_otherHasMoved {
	allowedTargets=0;
	jip=1;
};
class revive_fnc_otherHasShot {
	allowedTargets=0;
	jip=1;
};
class revive_fnc_otherMoved {
	allowedTargets=0;
	jip=0;
};
class revive_fnc_otherInVeh {
	allowedTargets=0;
	jip=1;
};
class revive_fnc_otherUnloaded {
	allowedTargets=0;
	jip=0;
};
class revive_fnc_woundedLoaded {
	allowedTargets=1;
	jip=0;
};
class revive_fnc_woundedMoved {
	allowedTargets=1;
	jip=0;
};
class revive_fnc_playerRelease {
	allowedTargets=0;
	jip=0;
};
