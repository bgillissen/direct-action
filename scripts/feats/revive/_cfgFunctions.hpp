class revive {
	tag = "revive";
	class functions {
		class playerDestroy { file="feats\revive\playerDestroy.sqf"; };
		class playerInit  { file="feats\revive\playerInit.sqf"; };
		class playerKilled { file="feats\revive\playerKilled.sqf"; };
		class playerLeave { file="feats\revive\playerLeave.sqf"; };
		class playerRespawn { file="feats\revive\playerRespawn.sqf"; };
		class serverDestroy { file="feats\revive\serverDestroy.sqf"; };
		class serverInit { file="feats\revive\serverInit.sqf"; };
		class serverKilled { file="feats\revive\serverKilled.sqf"; };
		class serverLeave { file="feats\revive\serverLeave.sqf"; };

		class actionCarry { file="feats\revive\actions\carry.sqf"; };
		class actionCondition { file="feats\revive\actions\condition.sqf"; };
		class actionDrag { file="feats\revive\actions\drag.sqf"; };
		class actionDrop { file="feats\revive\actions\drop.sqf"; };
		class actionHeal { file="feats\revive\actions\heal.sqf"; };
		class actionHelp { file="feats\revive\actions\help.sqf"; };
		class actionInVehHeal { file="feats\revive\actions\inVehHeal.sqf"; };
		class actionLoad { file="feats\revive\actions\load.sqf"; };
		class actionMoveToCargo { file="feats\revive\actions\moveToCargo.sqf"; };
		class actionUnload { file="feats\revive\actions\unload.sqf"; };
		class actionVehHeal { file="feats\revive\actions\vehHeal.sqf"; };

		class addBleeding { file="feats\revive\addBleeding.sqf"; };
		class callHelp { file="feats\revive\callHelp.sqf"; };
		class carryAnim { file="feats\revive\carryAnim.sqf"; };
		class closestMedic { file="feats\revive\closestMedic.sqf"; };
		class dragAnim { file="feats\revive\dragAnim.sqf"; };
		class deadCamQuote { file="feats\revive\deadCamQuote.sqf"; };
		class drawIcon3D { file="feats\revive\drawIcon3D.sqf"; };
		class dynamicText { file="feats\revive\dynamicText.sqf"; };
		class gotItem { file="feats\revive\gotItem.sqf"; };
		class handleDamage { file="feats\revive\handleDamage.sqf"; };
		class healAnimEH { file="feats\revive\healAnimEH.sqf"; };
		class helpAnimEH { file="feats\revive\helpAnimEH.sqf"; };
		class progressBar { file="feats\revive\progressBar.sqf"; };
		class progressBarInit { file="feats\revive\progressBarInit.sqf"; };
		class removeDropAction {file="feats\revive\removeDropAction.sqf"; };
		class removeFaks { file="feats\revive\removeFaks.sqf"; };
		class removeHealEquipment { file="feats\revive\removeHealEquipment.sqf"; };
		class removeVehActions {file="feats\revive\removeVehActions.sqf"; };
		class sfxBloodParticles { file="feats\revive\sfxBloodParticles.sqf"; };
		class sfxImpactEffect { file="feats\revive\sfxImpactEffect.sqf"; };
		class sfxInjuredEffect { file="feats\revive\sfxInjuredEffect.sqf"; };
		class stopBleeding { file="feats\revive\stopBleeding.sqf"; };
		class tkScore { file="feats\revive\tkScore.sqf"; };
		class weaponCheck { file="feats\revive\weaponCheck.sqf"; };

		class playerCarrying { file="feats\revive\player\carrying.sqf"; };
		class playerCarryCanceled { file="feats\revive\player\carryCanceled.sqf"; };
		class playerDragCanceled { file="feats\revive\player\dragCanceled.sqf"; };
		class playerDragging { file="feats\revive\player\dragging.sqf"; };
		class playerHealing { file="feats\revive\player\healing.sqf"; };
		class playerHelping { file="feats\revive\player\helping.sqf"; };
		class playerKeyUnbind { file="feats\revive\player\keyUnbind.sqf"; };
		class playerRelease { file="feats\revive\player\release.sqf"; };

		class otherAgonyStart { file="feats\revive\others\agonyStart.sqf"; };
		class otherAgonyStop { file="feats\revive\others\agonyStop.sqf"; };
		class otherHasMoved { file="feats\revive\others\hasMoved.sqf"; };
		class otherHasShot { file="feats\revive\others\hasShot.sqf"; };
		class otherInVeh { file="feats\revive\others\inVeh.sqf"; };
		class otherMoved { file="feats\revive\others\moved.sqf"; };
		class otherUnloaded { file="feats\revive\others\unloaded.sqf"; };

		class woundedAborded { file="feats\revive\wounded\aborded.sqf"; };
		class woundedAgonyStart { file="feats\revive\wounded\agonyStart.sqf"; };
		class woundedAgonyStop { file="feats\revive\wounded\agonyStop.sqf"; };
		class woundedCarried { file="feats\revive\wounded\carried.sqf"; };
		class woundedCarryFall { file="feats\revive\wounded\carryFall.sqf"; };
		class woundedDied { file="feats\revive\wounded\died.sqf"; };
		class woundedDragFall { file="feats\revive\wounded\dragFall.sqf"; };
		class woundedDragged { file="feats\revive\wounded\dragged.sqf"; };
		class woundedHasMoved { file="feats\revive\wounded\hasMoved.sqf"; };
		class woundedHasShot { file="feats\revive\wounded\hasShot.sqf"; };
		class woundedHealed { file="feats\revive\wounded\healed.sqf"; };
		class woundedHelped { file="feats\revive\wounded\helped.sqf"; };
		class woundedinVeh { file="feats\revive\wounded\inVeh.sqf"; };
		class woundedinWater { file="feats\revive\wounded\inWater.sqf"; };
		class woundedKeyUnbind { file="feats\revive\wounded\keyUnbind.sqf"; };
		class woundedLoaded { file="feats\revive\wounded\loaded.sqf"; };
		class woundedMoved { file="feats\revive\wounded\moved.sqf"; };
		class woundedMute { file="feats\revive\wounded\mute.sqf"; };
		class woundedNoRadio { file="feats\revive\wounded\noRadio.sqf"; };
		class woundedOnLadder { file="feats\revive\wounded\onLadder.sqf"; };
		class woundedOnLand { file="feats\revive\wounded\onLand.sqf"; };
		class woundedreleased { file="feats\revive\wounded\released.sqf"; };
		class woundedUnloaded { file="feats\revive\wounded\unloaded.sqf"; };
	};
};
