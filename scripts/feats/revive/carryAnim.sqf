
#include "_debug.hpp"

private _carrier = _this;

private _still = true;

private _nextMove = time + 0.735;
while { !(isNull _carrier) } do {
	if ( (speed _carrier) isEqualTo  0 ) then {
		if !( _still ) then {
            #ifdef DEBUG
			debug(LL_DEBUG, "carryAnim : still");
            #endif
			[player, "AinjPfalMstpSnonWnonDnon_carried_still", 1] call common_fnc_doAnim;
			_still = true;
		};
	} else {
		if ( _still || (time > _nextMove) ) then {
            #ifdef DEBUG
			debug(LL_DEBUG, "carryAnim : moving");
            #endif
			[player, "AinjPfalMstpSnonWnonDf_carried", 1] call common_fnc_doAnim;
			_nextMove = time + 0.735; 
			_still = false;
		};				
	};
	sleep 0.1;
	_carrier = player getVariable ["carrier", objNull];
};