
params [['_wounded', objNull]];

if ( isNil "reviveDropAction" ) exitWith {};

reviveDropAction params ['_handled', '_drop', '_cdl'];

private _do = true;
if !( isNull _wounded ) then {
	_do = ( _wounded isEqualTo _handled );
};

if !( _do ) exitWith {};

if ( _drop >= 0 ) then {
	player removeAction _drop;
	reviveDropAction set [1, -1];
};

if ( _cdl >= 0 ) then {
	player removeAction _cdl;
	reviveDropAction set [2, -1];
};

reviveDropAction set [0, objNull];