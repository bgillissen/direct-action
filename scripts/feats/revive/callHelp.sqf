// by psycho

if ( !(isNull(player getVariable "healer")) || 
	 !(isNull(player getVariable "dragger")) ||
	 !(isNull(player getVariable "carrier")) ||
	 !(isNull(player getVariable "helper")) ) exitWith {
	 (["revive", "agony", "helpInCharge"] call core_fnc_getSetting) call revive_fnc_dynamicText;
};
if ( (player getVariable ["nextCallHelp", 0]) > time) exitWith {
    (["revive", "agony", "helpTooSoon"] call core_fnc_getSetting) call revive_fnc_dynamicText;
};

player setVariable ["nextCallHelp", time + 30];

(["revive", "agony", "help"] call core_fnc_getSetting) call revive_fnc_dynamicText;

//TODO playSound3D....