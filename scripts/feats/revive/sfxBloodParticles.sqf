
#include "_debug.hpp"
 
{
	_x params ["_unit", "_uid", "_bodyPart", "_bodyPos", "_memPoint", ["_toStop", false], ["_logic", objNull], ["_blood", objNull], ["_nextTime", -1]];
	
    if ( _toStop || (isNull _unit) || !(alive _unit) ) then {
        #ifdef DEBUG
        private _debug = format["bloodParticle : removing --- unit: %1 --- part: %2", _unit, _bodyPart];
        debug(LL_DEBUG, _debug);
        #endif
        deleteVehicle _logic;
        deleteVehicle _blood;
        reviveBleedStack deleteAt _forEachIndex;
	} else {
        if ( isNull _logic ) then { 
        	_logic = "logic" createVehicleLocal (getpos _unit);
            _logic attachTo [_unit, _bodyPos, _memPoint]; 
		};
        if ( isNull _blood ) then { 
        	_blood = "#particlesource" createVehicleLocal (getpos _logic); 
		};
		if ( (_unit getVariable ["agony", false]) 
        	&& ((vehicle _unit) isEqualTo _unit)
			&& (isNull (_unit getVariable ["healer", objNull])) 
            && (isNull (_unit getVariable ["helper", objNull]))
			&& (diag_tickTime >= _nextTime) ) then {
            _nextTime = ( diag_tickTime + (1 + random 2) );
			_blood setParticleParams [["\a3\Data_f\ParticleEffects\Universal\Universal", 16, 13, 1],
			   	                      "",
			       	                  "Billboard",
			           	              0.5,
			               	          0.1,																//lifetime
			                   	      [0,0,0],
			                       	  [(0.3 - (random 0.6)),(0.3 - (random 0.6)),(0.2 + (random 0.3))],	//velocity
			                          1, 																//rotationVel
			                          0.32, 															//weight
			                          0.2, 																//volume
			                          0.05,																//rubbing
			                          [0.05,0.25],
			                          [[0.2,0.01,0.01,1],[0.2,0.01,0.01,0]],							//color
			   	                      [0.1],
			       	                  1,																//ran dir
			           	              0.04,																//ran intensity
			               	          "",
			                   	      "",
			                       	  _logic];
			_blood setParticleRandom [2, [0, 0, 0], [0.0, 0.0, 0.0], 0, 0.5, [0, 0, 0, 0.1], 0, 0, 10];
			_blood setDropInterval 0.02;
		} else {
            deletevehicle _logic;
			deletevehicle _blood;
            _logic = objNull;
            _blood = objNull;
		};
        reviveBleedStack set [_forEachIndex, [_unit, _uid, _bodyPart, _bodyPos, _memPoint, _toStop, _logic, _blood, _nextTime]];
	};
} forEach reviveBleedStack;