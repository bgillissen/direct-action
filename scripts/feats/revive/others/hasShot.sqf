/*
@filename: feats\revive\others\hasShot.sqf
Author:
	Ben
Description:
	run on all context,
*/

#include "..\_debug.hpp"

#ifdef DEBUG
debug(LL_DEBUG, "others hasShot");
#endif

params ["_wounded"];

if ( _wounded isEqualTo player ) exitWith {
    #ifdef DEBUG
	debug(LL_DEBUG, "others hasShot: wounded is player, abording");
	#endif
};

_wounded setCaptive false;