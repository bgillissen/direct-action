/*
@filename: feats\revive\others\hasMoved.sqf
Author:
	Ben
Description:
	run on all context,
*/

#include "..\_debug.hpp"

#ifdef DEBUG
debug(LL_DEBUG, "others hasMoved");
#endif

params ["_wounded"];

if ( _wounded isEqualTo player ) exitWith {
    #ifdef DEBUG
	debug(LL_DEBUG, "others hasMoved: wounded is player, abording");
	#endif
};

//_wounded allowDamage true;