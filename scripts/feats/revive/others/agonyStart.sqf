/*
@filename: feats\revive\others\agonyStart.sqf
Author:
	Ben
Description:
	run on all context,
*/

#include "..\_debug.hpp"

#ifdef DEBUG
debug(LL_DEBUG, "others agonyStart");
#endif

params ["_wounded"];

if ( isNull _wounded ) exitWith {
    #ifdef DEBUG
	debug(LL_DEBUG, "others agonyStart: wounded is null, abording");
	#endif

};

if ( _wounded isEqualTo player ) exitWith {
    #ifdef DEBUG
	debug(LL_DEBUG, "others agonyStart: wounded is player, abording");
	#endif
};

if !( alive _wounded ) exitWith {
    #ifdef DEBUG
	debug(LL_DEBUG, "others agonyStart: wounded is dead, abording");
	#endif
};

if !( _wounded getVariable['agony', false] ) exitWith {
    #ifdef DEBUG
	debug(LL_DEBUG, "others agonyStart: wounded is not in agony anymore, abording");
	#endif
};

{
    _wounded setHitIndex [_forEachIndex, 0];
} forEach ((getAllHitPointsDamage _wounded) select 2);
_wounded setDamage 0;
_wounded setCaptive true;

if ( !CTXT_PLAYER )  exitWith {
    #ifdef DEBUG
	debug(LL_DEBUG, "others agonyStart: not a player, skipping actions add");
	#endif
};

#ifdef DEBUG
private _debug = format["others agonyStart: adding actions for role '%1' to %2", (player getVariable ["role", ""]), _wounded];
debug(LL_DEBUG, _debug);
#endif

private ["_cond", "_action"];

//needed for JIP, playerInit has not been ran yet.
if ( isNil "reviveOnlyMedic" ) then {
    reviveOnlyMedic = (["reviveOnlyMedic"] call core_fnc_getParam isEqualTo 1);
};

if ( ((player getVariable "role") isEqualTo "medic") || !reviveOnlyMedic ) then {
    #ifdef DEBUG
	debug(LL_DEBUG, "others agonyStart: adding heal action");
	#endif
	_cond = "['heal', _target] call revive_fnc_actionCondition";
	_action = ["revive", "actions", "heal"] call core_fnc_getSetting;
	_action = _wounded addAction [format[_action, (name _wounded)], {reviveActionThread = _this spawn revive_fnc_actionHeal}, "", 100, false, true, "", _cond];
    _wounded setVariable["actionHeal", _action];
};

_cond = "['vehHeal', _target] call revive_fnc_actionCondition";
_action = ["revive", "actions", "vehHeal"] call core_fnc_getSetting;
_action = _wounded addAction [format[_action, (name _wounded)], {reviveActionThread = _this spawn revive_fnc_actionVehHeal}, "", 99, false, true, "", _cond];
_wounded setVariable["actionVehHeal", _action];

_cond = "['drag', _target] call revive_fnc_actionCondition";
_action = ["revive", "actions", "drag"] call core_fnc_getSetting;
_action = _wounded addAction [format[_action, (name _wounded)], {reviveActionThread = _this spawn revive_fnc_actionDrag}, "", 99, false, true, "", _cond];
_wounded setVariable["actionDrag", _action];

_cond = "['carry', _target] call revive_fnc_actionCondition";
_action = ["revive", "actions", "carry"] call core_fnc_getSetting;
_action = _wounded addAction [format[_action, (name _wounded)], {reviveActionThread = _this spawn revive_fnc_actionCarry}, "", 98, false, true, "", _cond];
_wounded setVariable["actionCarry", _action];

_cond = "['help', _target] call revive_fnc_actionCondition";
_action = ["revive", "actions", "help"] call core_fnc_getSetting;
_action = _wounded addAction [format[_action, (name _wounded)], {reviveActionThread = _this spawn revive_fnc_actionHelp}, "", 97, false, true, "", _cond];
_wounded setVariable["actionHelp", _action];

_cond = "['load', _target] call revive_fnc_actionCondition";
_action = ["revive", "actions", "load"] call core_fnc_getSetting;
_action = _wounded addAction [format[_action, (name _wounded)], { _this call revive_fnc_actionLoad}, "", 96, false, true, "", _cond];
_wounded setVariable["actionLoad", _action];