/*
@filename: feats\revive\others\inVeh.sqf
Author:
	Ben
Description:
	run on all context,
*/

#include "..\_debug.hpp"

#ifdef DEBUG
debug(LL_DEBUG, "others inVeh");
#endif

params ["_wounded", "_veh"];

if ( isNull _wounded ) exitWith {
    #ifdef DEBUG
	debug(LL_DEBUG, "others inVeh : wounded is null, abording");
	#endif
};

if ( !CTXT_PLAYER ) exitWith {
    #ifdef DEBUG
	debug(LL_DEBUG, "others inVeh : not a player, skipping actions add");
	#endif
};

if ( _wounded isEqualTo player ) exitWith {
    #ifdef DEBUG
	debug(LL_DEBUG, "others inVeh : wounded is player, abording");
	#endif
};

if !( (vehicle _wounded) isEqualTo _veh ) exitWith {
    #ifdef DEBUG
	debug(LL_DEBUG, "others inVeh : wounded not in that vehicle anymore, abording");
	#endif
};

if !( _wounded getVariable["agony", false] ) exitWith {
    #ifdef DEBUG
	debug(LL_DEBUG, "others inVeh : wounded is not in agony anymore, abording");
	#endif
};

if ( _veh isEqualTo _wounded ) then {
    #ifdef DEBUG
	debug(LL_DEBUG, "others inVeh : wounded == vehicle ????");
	#endif
    sleep 2;
    _veh = (vehicle _wounded);
};

if ( _veh isEqualTo _wounded ) exitWith {
    #ifdef DEBUG
	debug(LL_DEBUG, "others inVeh: wounded ==  vehicle, tryed to fix it, but nop, nothing i can do");
	#endif
};

#ifdef DEBUG
private _debug = format["others inVeh: adding unload action to %1 for %2", _veh, _wounded];
debug(LL_DEBUG, _debug);
#endif

private _actions = [];

private _uid = (getPlayerUID _wounded);    
            
private _cond = format["['unload', _target, '%1'] call revive_fnc_actionCondition", _uid];
private _action = ["revive", "actions", "unload"] call core_fnc_getSetting;
_actions pushback (_veh addAction [format[_action, (name _wounded)], {_this call revive_fnc_actionUnload}, _wounded, 99, false, true, "", _cond, 5]);

private _inMedEvac = false;
if !( isNil "reviveMedEvacs" ) then {
	_inMedEvac = ( (getNumber(configFile >> "cfgVehicles" >> (typeof _veh) >> "attendant") > 0) || ((typeOf _veh) in reviveMedEvacs) );
};

if ( isNil "reviveOnlyMedic" ) then { 
	reviveOnlyMedic = (["reviveOnlyMedic"] call core_fnc_getParam isEqualTo 1); 
};

if ( ((player getVariable "role") isEqualTo "medic") || !reviveOnlyMedic || _inMedEvac ) then {
	_cond = format["['inVehHeal', _target, '%1'] call revive_fnc_actionCondition", _uid];
	if ( _inMedEvac ) then {
		_action = ["revive", "actions", "vehHeal"] call core_fnc_getSetting;
	} else {
		_action = ["revive", "actions", "heal"] call core_fnc_getSetting;
	};
	//private _exec = format["reviveActionThread = [_this select 0, '%1'] spawn revive_fnc_actionInVehHeal", _uid];
    //_actions pushback (_veh addAction [format[_action, (name _wounded)], _exec, _wounded, 99, false, true, "", _cond]);
	_actions pushback (_veh addAction [format[_action, (name _wounded)], {reviveActionThread = _this spawn revive_fnc_actionInVehHeal;}, _wounded, 99, false, true, "", _cond]);
};
    
//is moveToCargo avail for that vehicle ?
if ( _wounded isEqualTo (driver _veh) ) then {
    if ( isNil "reviveMoveable" ) then {
        reviveMoveable = (["revive", "moveable"] call core_fnc_getSetting);
    };
    private _do = false;
    {
        if ( _veh isKindOf _x ) exitWith { _do = true; };
    } forEach reviveMoveable;
    if ( _do ) then {
    	_cond = format["['moveToCargo', _target, '%1'] call revive_fnc_actionCondition", _uid];
    	_action = ["revive", "actions", "moveToCargo"] call core_fnc_getSetting;
    	_actions pushback (_veh addAction [format[_action, (name _wounded)], {_this call revive_fnc_actionMoveToCargo}, _wounded, 98, false, true, "", _cond, 5]);
	};
};

if ( isNil "reviveVehActionStack" ) then { reviveVehActionStack = []; };

reviveVehActionStack pushback [_uid, _veh, _actions];

_wounded playActionNow "die";