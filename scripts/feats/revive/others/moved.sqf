/*
@filename: feats\revive\others\moved.sqf
Author:
	Ben
Description:
	run on player,
*/

#include "..\_debug.hpp"

params ["_wounded"];

if ( isNull _wounded ) exitWith {
    #ifdef DEBUG
	debug(LL_DEBUG, "others inVeh : wounded is null, abording");
	#endif
};

if ( !CTXT_PLAYER ) exitWith {
    #ifdef DEBUG
	debug(LL_DEBUG, "others inVeh : not a player, skipping actions add");
	#endif
};

if ( _wounded isEqualTo player ) exitWith {
    #ifdef DEBUG
	debug(LL_DEBUG, "others inVeh : wounded is player, abording");
	#endif
};

#ifdef DEBUG
private _debug = format["others moved : %1", _wounded];
debug(LL_DEBUG, _debug);
#endif

_wounded playActionNow "die";