/*
@filename: feats\revive\others\agonyStop.sqf
Author:
	Ben
Description:
	run on all context,
*/

#include "..\_debug.hpp"

#ifdef DEBUG
debug(LL_DEBUG, "others agonyStop");
#endif

params ["_wounded", ["_healer", objNull]];

if ( _wounded isEqualTo player ) exitWith {
    #ifdef DEBUG
	debug(LL_DEBUG, "others agonyStop: wounded is player, abording");
	#endif
};

_wounded setDamage 0;
_wounded setCaptive false;

if ( !CTXT_PLAYER )  exitWith {
    #ifdef DEBUG
	debug(LL_DEBUG, "others agonyStop: not a player, skipping actions remove");
	#endif
};

if ( _healer isEqualTo player ) then {
    #ifdef DEBUG
	debug(LL_DEBUG, "others agonyStop: player was healer");
	#endif 
	detach player; 
    detach _wounded;
};

{
	_wounded removeAction (_wounded getVariable[_x, -1]);
    _wounded setVariable[_x, -1];
} forEach ["actionHeal", "actionHelp", "actionVehHeal", "actionDrag", "actionCarry", "actionLoad"];

[_wounded] call revive_fnc_removeDropAction;
/*
(player getVariable ["action", ["", objNull]]) params ["_action", "_handled"];
if ( _handled isEqualTo _wounded ) then {
	if ( reviveDropAction >= 0 ) then {
		player removeAction reviveDropAction;
		reviveDropAction = -1;
	};
	if ( reviveCDLAction >= 0 ) then {
		player removeAction reviveCDLAction;
		reviveCDLAction = -1;
	};
};
*/
[(getPlayerUID _wounded)] call revive_fnc_removeVehActions;