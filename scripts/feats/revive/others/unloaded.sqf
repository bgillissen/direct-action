/*
@filename: feats\revive\others\unloaded.sqf
Author:
	Ben
Description:
	run on all context,
*/

#include "..\_debug.hpp"

params ["_wounded", ["_veh", objNull], ["_unloader", objNull]];

#ifdef DEBUG
private _debug = format["revive: others unloaded: wounded: %1, veh: %2, unloader: %3",_wounded, _veh, _unloader];
debug(LL_DEBUG, _debug, 0);
#endif

if ( _wounded isEqualTo player ) exitWith {
    #ifdef DEBUG
	debug(LL_DEBUG, "revive: others unloaded: wounded is player, abording", 0);
	#endif
};

if ( !CTXT_PLAYER ) exitWith {
    #ifdef DEBUG
	debug(LL_DEBUG, "revive: others unloaded: not a player, skipping action remove", 0);
	#endif
};

[(getPlayerUID _wounded), _veh] call revive_fnc_removeVehActions;