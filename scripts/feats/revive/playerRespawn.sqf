/*
@filename: feats\revive\playerRespawn.sqf
Credit:
	psycho 
Author:
	Ben
Description:
	run on player,
	define some global variables,
	start the revive main FSM
*/

#include "_debug.hpp"

if ( MOD_ace ) exitWith {
    #ifdef DEBUG
    debug(LL_INFO, "disabled by the presence of ACE");
    #endif
    nil
};

//removing JIP call from queue, just in case
reviveDelJIP = [];
if !( reviveJIPAgonyStart isEqualTo "" ) then { reviveDelJIP pushback reviveJIPAgonyStart; }; 
if !( reviveJIPHasMoved isEqualTo "" ) then { reviveDelJIP pushback reviveJIPHasMoved; };
if !( reviveJIPHasShot isEqualTo "" ) then { reviveDelJIP pushback reviveJIPHasShot; };
if !( reviveJIPInVeh isEqualTo "" ) then { reviveDelJIP pushback reviveJIPInVeh; };
if ( (count reviveDelJIP) > 0 ) then { publicVariableServer "reviveDelJIP"; };

player setCaptive false;

reviveAgonyHitPoints = [0,0,0,0,0,0,0,0,0,0,0];
player setVariable ["hitPoints", reviveAgonyHitPoints, true];

reviveNoMove = false;
reviveKeyEH = -1;
reviveShotEH = -1;
revivePreAnim = "";
reviveLockTime = 0;
reviveActionThread = scriptNull;
reviveAnimThread = scriptNull;
reviveAnimEH = -1;
reviveMedEvac = objNull;
reviveProgressBar = false;
reviveProgressBarHide = false;

private _startFSM = false;

if ( isNil "reviveFSM" ) then { _startFSM = true; };

if ( !_startFSM ) then { _startFSM = completedFSM reviveFSM; };

if ( _startFSM ) then { reviveFSM = execFSM ("feats\revive\fsm\main.fsm"); };

nil