/*
@filename: feats\revive\playerLeave.sqf
Credit:
	psycho 
Author:
	Ben
Description:
	run on player,
	used to remove unload action of wounded players when inside a vehicle and they leave
*/

#include "_debug.hpp"

if ( MOD_ace ) exitWith {
    #ifdef DEBUG
    debug(LL_INFO, "disabled by the presence of ACE");
    #endif
    nil
};

params["_unit", "_id", "_uid", "_name"];

[_uid] call revive_fnc_removeVehActions;

{
    _x params ["_bloodUnit", "_bloodUid", "_bodyPart"];
	if ( _uid isEqualTo _bloodUid ) then {
        #ifdef DEBUG
		private _debug = format["bloodParticle : stopping --- uid: %1 --- part: %2", _uid, _bodyPart];
        debug(LL_DEBUG, _debug);
        #endif
        _x set [5, true];
		reviveBleedStack set [_forEachIndex, _x];
	};
} forEach reviveBleedStack;

nil