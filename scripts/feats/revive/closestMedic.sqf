

private _woundedGotMedikit = false;
if ( !reviveRequireMedikit || ([player, reviveMedikit] call revive_fnc_gotItem) ) then {
    _woundedGotMedikit = true;
};

private _woundedGotFaks = false;
if ( !reviveConsumeFaks || ([player, reviveFaks] call revive_fnc_gotItem) ) then {
    _woundedGotFaks = true;
};

private _closest = objNull;

{  
	if !(_x getVariable["agony", false]) then {
		if ( (player distance _x) < (player distance _closest) ) then {
            private _canHeal = false;
            if ( !reviveOnlyMedic || (_x getVariable "role" isEqualTo "medic") ) then {
                _canHeal = true;
            };
	        if !( _canHeal ) then {
                private _veh = vehicle _x;
                if !(_veh isEqualTo _x) then {
                    if ( driver _veh isEqualTo _x ) then {
                		_canHeal = ( getNumber(configFile >> "cfgVehicles" >> (typeOf _veh) >> "attendant") > 0 );        
                    };
                };
            } else {
            	_canHeal = _woundedGotMedikit;
            	if !( _canHeal ) then {
                	_canHeal = ( !reviveRequireMedikit || ([_x, reviveMedikit] call revive_fnc_gotItem) );
				};
            	if ( _canHeal ) then { _canHeal = _woundedGotFaks; };
            	if !( _canHeal ) then {
					_canHeal = ( !reviveConsumeFaks || ([_x, reviveFaks] call revive_fnc_gotItem) );
				};
			};
            if ( _canHeal ) then { _closest = _x; };
	    };
	};
} forEach (allPlayers - (entities "HeadlessClient_F") - [player]);

_closest