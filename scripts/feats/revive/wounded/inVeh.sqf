/*
@filename: feats\revive\wounded\inVeh.sqf
Author:
	Ben
Description:
	run on player,
*/

#include "..\_debug.hpp"

#ifdef DEBUG
debug(LL_DEBUG, "wounded inVeh");
#endif

params ["_veh"];

player setVariable["inVeh", _veh, true];

reviveVehPreAnim = animationState player;

[player, "die", 1] call common_fnc_doGesture;

false call revive_fnc_woundedMute;