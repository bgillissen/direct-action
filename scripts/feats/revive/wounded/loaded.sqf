/*
@filename: feats\revive\wounded\loaded.sqf
Author:
	Ben
Description:
	run on player,
*/

#include "..\_debug.hpp"

params ["_veh", "_freeSeats", "_loader"];

#ifdef DEBUG
private _debug = format["wounded loaded : %1 is loading player into %2 @ %3", _loader, _veh, _freeSeats];
debug(LL_DEBUG, _debug);
#endif

detach player;
player setVariable ["dragger", objNull, true];
player setVariable ["carrier", objNull, true];
    
private _finish = false;

while { !_finish } do {
	private _seat = (selectRandom _freeSeats);
    _freeSeats = _freeSeats - [_seat];
	player moveInCargo [_veh, _seat];
	private _limit = time + 2;
	waitUntil {
    	sleep 0.1;
		( ((vehicle player) isEqualTo _veh) || (time > _limit) )
	};
    _finish = ( ((vehicle player) isEqualTo _veh) || ((count _freeSeats) isEqualTo 0) );
};

if ( (vehicle player) isEqualTo _veh ) then {
	private _vehName = getText(configFile >> "cfgVehicles" >> (typeOf _veh) >> "displayName");
	private _msg = (["revive", "load", "loaded"] call core_fnc_getSetting); 
	format[_msg, _vehName, (name _loader)] call revive_fnc_dynamicText;
	player setVariable ["inVeh", _veh];
} else {
    #ifdef DEBUG
	debug(LL_WARN, "wounded loaded : load failed, not in vehicle!");
	#endif
    player setVariable ["loadInVeh", [], true];
};