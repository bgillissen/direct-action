/*
@filename: feats\revive\wounded\agonyStart.sqf
Author:
	Ben
Description:
	run on player,
*/

#include "..\_debug.hpp"

#ifdef DEBUG
debug(LL_DEBUG, "wounded agonyStart");
#endif

private _hitPoints = ((getAllHitPointsDamage player) select 2);
{ player setHitIndex [_forEachIndex, 0]; } forEach _hitPoints;

player setDamage 0;

player setObjectMaterialGlobal [0, reviveBloodyMat];

if (visibleMap) then {openMap false};
while { dialog } do { closeDialog 0; };

player setCaptive true;

player switchCamera "INTERNAL";

showHud false;

[] spawn {
	sleep 1;
	reviveKeyEH = (findDisplay 46) displayAddEventHandler ["KeyDown", {_this call revive_fnc_woundedKeyUnbind}];
	reviveShotEH = player addEventHandler ["FiredMan", {player setVariable ["hasShot", true, true]}];
};

true call revive_fnc_woundedNoRadio;

//(player getVariable ["action", []]) params [["_action", ""], ["_wounded", objNull]];

[objNull] call revive_fnc_removeDropAction;

if ( reviveNoMoveEH >= 0 ) then {
	(findDisplay 46) displayRemoveEventHandler ["keyDown", reviveNoMoveEH];
    reviveNoMoveEH = -1;  
};


execFSM ("feats\revive\fsm\bleedOut.fsm");

private _unitMode = ["revive", "agony", "unitMode"] call core_fnc_getSetting;
private _group = group player;
if ( _unitMode > 0 ) then {
	private _msg = ["revive", "agony", "unitMsg"] call core_fnc_getSetting;
    private _targets = [];
	if ( _unitMode isEqualTo 1 ) then { _targets = allPlayers; };
	if ( _unitMode isEqualTo 2 ) then { _targets = (units _group); };
    [_targets, format[_msg, (player getVariable["MD_name", (name player)]), (mapGridPosition player)]] call common_fnc_systemChat;
};

private _squadMode = ["revive", "agony", "squadMode"] call core_fnc_getSetting;
if ( _squadMode > 0 ) then {
	if ( { !( _x getVariable ["agony", false] ) } count (units _group) == 0 ) then {
		private _msg = ["revive", "agony", "squadMsg"] call core_fnc_getSetting;
		[allPlayers, format[_msg, (groupId _group)]] call common_fnc_systemChat;
	};
};

[1] call commandChain_fnc_swapLeader;