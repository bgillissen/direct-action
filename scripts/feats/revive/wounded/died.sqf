/*
@filename: feats\revive\wounded\died.sqf
Author:
	Ben
Description:
	run on player,
*/

#include "..\_debug.hpp"

#ifdef DEBUG
debug(LL_DEBUG, "wounded died");
#endif

[player] call revive_fnc_stopBleeding;

player setCaptive false;

showHud true;

player setVariable ["healer", objNull, true];
player setVariable ["helper", objNull, true];
player setVariable ["dragger", objNull, true];
player setVariable ["carrier", objNull, true];
player setVariable ["droper", objNull, true];
player setVariable ["medEvac", objNull, true];
player setVariable ["inVeh", objNull, true];
player setVariable ["agony", false, true];
player setVariable ["hasMoved", false, true];
player setVariable ["hasShot", false, true];

player setObjectMaterialGlobal [0, reviveCleanMat];

[objNull] call revive_fnc_removeDropAction;

/*
(player getVariable ["action", []]) params [["_action", ""], ["_wounded", objNull]];
if !( _action isEqualTo "" ) then {
	player setVariable["action", [], true];
    player removeAction reviveDropAction;
	reviveDropAction = -1;
};
*/
reviveNoMove = false;
(findDisplay 46) displayRemoveEventHandler ["keyDown", reviveKeyEH];
player removeEventHandler ["FiredMan", reviveShotEH];
reviveKeyEH = -1;
reviveShotEH = -1;

false call revive_fnc_woundedNoRadio;
false call revive_fnc_woundedMute;

//removing JIP call from queue
reviveDelJIP = [reviveJIPAgonyStart];
reviveJIPAgonyStart = "";
if !( reviveJIPHasMoved isEqualTo "") then { reviveDelJIP pushback reviveJIPHasMoved; };
if !( reviveJIPHasShot isEqualTo "") then {  reviveDelJIP pushback reviveJIPHasShot; };
if !( reviveJIPInVeh isEqualTo "") then {   reviveDelJIP pushback reviveJIPInVeh; };
publicVariableServer "reviveDelJIP";