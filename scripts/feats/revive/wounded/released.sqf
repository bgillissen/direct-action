/*
@filename: feats\revive\wounded\released.sqf
Author:
	Ben
Description:
	run on player,
*/

#include "..\_debug.hpp"

params["_isDrag", "_isCarry"];

#ifdef DEBUG
private _debug = format["wounded released, isDrag: %1, isCarry: %2", _isDrag, _isCarry];
debug(LL_DEBUG, _debug);
#endif

player setVariable ["droper", objNull, true];
player setVariable ["carrier", objNull, true];
player setVariable ["dragger", objNull, true];

terminate reviveAnimThread;
reviveNoMove = false;

if ( _isDrag ) exitWith {
    #ifdef DEBUG
    debug(LL_DEBUG, "wounded released: isDrag");
    #endif
	if ( player getVariable["hasMoved", false] ) then {
		[player, "AinjPpneMrunSnonWnonDb_release", 1] call common_fnc_doAnim;
	} else {
		[player, "die", 1] call common_fnc_doGesture;
	};
    detach player;
};
if ( _isCarry ) exitWith {
    #ifdef DEBUG
    debug(LL_DEBUG, "wounded released: isCarry");
    #endif
    detach player;
	[player, "AinjPfalMstpSnonWnonDnon_carried_Down", 1] call common_fnc_doAnim;
	//[player, "AinjPfalMstpSnonWrflDnon_AmovPpneMstpSrasWrflDnon_injured", 1] call common_fnc_doAnim;
	[] spawn {
		sleep 5;
        if !( player getVariable["hasMoved", false] ) then {
			[player, "die", 1] call common_fnc_doGesture;
        };				
	};
};