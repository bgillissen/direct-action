/*
@filename: feats\revive\wounded\onLadder.sqf
Author:
	Ben
Description:
	run on player,
*/

#include "..\_debug.hpp"

#ifdef DEBUG
debug(LL_DEBUG, "wounded onLadder");
#endif

player action ["ladderOff", (nearestBuilding player)];