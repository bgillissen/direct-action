/*
@filename: feats\revive\wounded\dragged.sqf
Author:
	Ben
Description:
	run on player,
*/

#include "..\_debug.hpp"

#ifdef DEBUG
debug(LL_DEBUG, "wounded dragged");
#endif

params ["_dragger"];

_dragger setVariable["action", ["drag", player], true];

reviveNoMove = true;

[player, "AinjPpneMrunSnonWnonDb_grab", 2] call common_fnc_doAnim; //1s

player attachTo [_dragger,  [0, 1.1, 0.092]];
[player, 180] call common_fnc_setDir;

reviveAnimThread = _dragger spawn revive_fnc_dragAnim;
