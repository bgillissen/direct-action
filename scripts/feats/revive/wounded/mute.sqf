/*
@filename: feats\revive\wounded\mute.sqf
Author:
	Ben
Description:
	run on player,
*/

if !( MOD_tfar ) exitwith {};

if ( (["reviveMute"] call core_fnc_getParam) isEqualTo 0 ) exitWith {};

if ( _this ) then {
    #ifdef DEBUG
	debug(LL_DEBUG, "wounded mute: muting player");
	#endif
	player setVariable ["tf_voiceVolume", 0, true];
	player setVariable ["tf_globalVolume", 0.2];
} else {
    #ifdef DEBUG
	debug(LL_DEBUG, "wounded mute: unmuting player");
	#endif
	player setVariable ["tf_voiceVolume", 1, true];
	player setVariable ["tf_globalVolume", 1];    
};