/*
@filename: feats\revive\wounded\noRadio.sqf
Author:
	Ben
Description:
	run on player,
*/

if !( MOD_tfar ) exitwith {};

if ( (["reviveNoRadio"] call core_fnc_getParam) isEqualTo 0 ) exitWith {};

#ifdef DEBUG
if ( _this ) then {
	debug(LL_DEBUG, "wounded noRadio: disabling radio");
} else {
    debug(LL_DEBUG, "wounded noRadio: enabling radio");
};
#endif

player setVariable ["tf_unable_to_use_radio", _this];