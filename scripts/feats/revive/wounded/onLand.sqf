/*
@filename: feats\revive\wounded\onLand.sqf
Author:
	Ben
Description:
	run on player,
*/

#include "..\_debug.hpp"

#ifdef DEBUG
debug(LL_DEBUG, "wounded onLand");
#endif

player setVariable ["inWater", ( ((getPosASL player) select 2) < 0 )];

player setUnitPos "DOWN";

reviveLockTime = 0.2;
[player, "die", 1] call common_fnc_doGesture;

!( player getVariable['hasMoved', false] ) call revive_fnc_woundedMute;