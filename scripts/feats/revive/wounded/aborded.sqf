
params ["_type"]; 

player setVariable ["helper", objNull, true];
player setVariable ["healer", objNull, true];
player setVariable ["medEvac", objNull, true];

reviveNoMove = false;

if !( isNull (player getVariable["inVeh", objNull]) ) exitWith {};

detach player;

if ( player getVariable ["hasMoved", false] ) then {
    [player, "AinjPpneMstpSnonWrflDnon_rolltofront", 1] call common_fnc_doAnim;
} else {
    [player, "die", 1] call common_fnc_doGesture;
};