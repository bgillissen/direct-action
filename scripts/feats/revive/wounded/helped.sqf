/*
@filename: feats\revive\wounded\helped.sqf
Author:
	Ben
Description:
	run on player,
*/

#include "..\_debug.hpp"

#ifdef DEBUG
debug(LL_DEBUG, "wounded helped");
#endif

params ["_helper"];

_helper setVariable["action", ["help", player], true];

reviveNoMove = true;

[player, "AinjPpneMstpSnonWrflDnon_rolltoback"] call common_fnc_doAnim;