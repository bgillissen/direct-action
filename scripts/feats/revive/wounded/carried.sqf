/*
@filename: feats\revive\wounded\carried.sqf
Author:
	Ben
Description:
	run on player,
*/

#include "..\_debug.hpp"

#ifdef DEBUG
debug(LL_DEBUG, "wounded carried");
#endif

params ["_carrier"];

_carrier setVariable["action", ["carry", player], true];

reviveNoMove = true;

#ifdef DEBUG
[_carrier, format["%1 - wounded: set position", serverTime]] call common_fnc_systemChat;
systemChat format["%1 - wounded: set position", serverTime];
#endif
player setDir (getDir _carrier);
player setPosASL (AGLtoASL (_carrier modelToWorld [0.05, 0.8, 0]));

[player, "AinjPfalMstpSnonWnonDnon_carried_Up", 1] call common_fnc_doAnim;

private _anim = toLower "AinjPfalMstpSnonWnonDnon_carried_Up";
waitUntil {
	( ((animationState player) isEqualTo _anim)
      || !(alive player) 
      || !(alive _carrier) 
      || (_carrier getVariable ["agony", false]) )    
};

if ( !(alive player) || !(alive _carrier) || (_carrier getVariable ["agony", false]) ) exitWith {};

#ifdef DEBUG
[_carrier, format["%1 - wounded: carried_Up animation detected, inverting direction", serverTime]] call common_fnc_systemChat;
systemChat format["%1 - wounded: carried_Up animation detected, inverting direction", serverTime];
#endif
player setDir (getDir player + 180);

private _anim = toLower "AcinPercMstpSnonWnonDnon";
waitUntil { 
	sleep 0.05;
	( ((animationState _carrier) isEqualTo _anim)
      || !(alive player) 
      || !(alive _carrier) 
      || (_carrier getVariable ["agony", false]) )
};

if ( !(alive player) || !(alive _carrier) || (_carrier getVariable ["agony", false]) ) exitWith {};

sleep 0.2;

player attachTo [_carrier, [0.4, -0.1, -1.25], "leftShoulder"];
player setDir 195;
[player, "AinjPfalMstpSnonWnonDf_carried_still", 1] call common_fnc_doAnim;		
#ifdef DEBUG
[_carrier, format["%1 - wounded : attached to carrier shoulder", serverTime]] call common_fnc_systemChat;
systemChat format["%1 - wounded : attached to carrier shoulder", serverTime];
#endif

reviveAnimThread = _carrier spawn revive_fnc_carryAnim;