/*
@filename: feats\revive\wounded\healed.sqf
Author:
	Ben
Description:
	run on player,
*/

#include "..\_debug.hpp"

#ifdef DEBUG
debug(LL_DEBUG, "revive: wounded healed");
#endif

params ["_healer"];

_healer setVariable["action", ["heal", player], true];

if ( isNull (player getVariable["inVeh", objNull]) ) then {
	reviveNoMove = true;
	[player, "AinjPpneMstpSnonWrflDnon_rolltoback", 2] call common_fnc_doAnim;
};