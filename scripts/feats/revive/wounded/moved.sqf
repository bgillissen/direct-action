/*
@filename: feats\revive\wounded\moved.sqf
Author:
	Ben
Description:
	run on player,
*/

#include "..\_debug.hpp"

params ["_veh", "_seat", "_loader"];

_seat params ["_unit", "_role", "_idx", "_turret", "_pTurret"];

#ifdef DEBUG
private _debug = format["wounded moveToCargo : %1 is moving player to seat %3 (%4 - %5 - %6) in %2", _loader, _veh, _idx, _role, _turret, _pTurret];
debug(LL_DEBUG, _debug);
#endif

moveOut player;
if ( _role in ["turret", "Turret"] ) then {
	player moveInTurret [_veh, _turret];    
} else {
	player moveInCargo [_veh, _idx];
};

private _vehName = getText(configFile >> "cfgVehicles" >> (typeOf _veh) >> "displayName");
private _msg = (["revive", "moveToCargo", "moved"] call core_fnc_getSetting); 
format[_msg, (name _loader)] call revive_fnc_dynamicText;

[player, "die", 1] call common_fnc_doGesture;

[player] remoteExec ["revive_fnc_otherMoved", 0, false];