/*
@filename: feats\revive\wounded\dragFall.sqf
Author:
	Ben
Description:
	run on all context,
*/

#include "..\_debug.hpp"

#ifdef DEBUG
debug(LL_DEBUG, "wounded drag fall");
#endif

player setVariable ["dragger", ObjNull, true];

terminate reviveAnimThread;
reviveNoMove = false;
detach player;
[player, "AinjPpneMrunSnonWnonDb_release", 1] call common_fnc_doAnim; //1s

reviveLockTime = 1.1;
if ( player getVariable["hasMoved", false] ) then {
	reviveLockTime = 1.6;
	[] spawn {
		sleep 1.1;
		[player, "AinjPpneMstpSnonWnonDnon_rolltofront", 1] call common_fnc_doAnim; //0.5s
	};
};