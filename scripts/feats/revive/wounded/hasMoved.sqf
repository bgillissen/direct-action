/*
@filename: feats\revive\wounded\hasMoved.sqf
Author:
	Ben
Description:
	run on player,
*/

#include "..\_debug.hpp"

#ifdef DEBUG
debug(LL_DEBUG, "wounded hasMoved");
#endif

[player, "agonyStop", 2] call common_fnc_doGesture;

false call revive_fnc_woundedMute;