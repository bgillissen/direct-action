/*
@filename: feats\revive\wounded\agonyStop.sqf
Author:
	Ben
Description:
	run on player,
*/

#include "..\_debug.hpp"

#ifdef DEBUG
debug(LL_DEBUG, "wounded agonyStop");
#endif

[player] call revive_fnc_stopBleeding;

player setCaptive false;

showHud true;

player setVariable ["healer", objNull, true];
player setVariable ["helper", objNull, true];
player setVariable ["dragger", objNull, true];
player setVariable ["carrier", objNull, true];
player setVariable ["droper", objNull, true];
player setVariable ["medEvac", objNull, true];
player setVariable ["inVeh", objNull, true];
player setVariable ["agony", false, true];
player setVariable ["hasMoved", false, true];
player setVariable ["hasShot", false, true];

player setObjectMaterialGlobal [0, reviveCleanMat];

reviveNoMove = false;
(findDisplay 46) displayRemoveEventHandler ["keyDown", reviveKeyEH];
player removeEventHandler ["FiredMan", reviveShotEH];
reviveKeyEH = -1;
reviveShotEH = -1;

false call revive_fnc_woundedNoRadio;
false call revive_fnc_woundedMute;

detach player;

private _veh = (vehicle player);
if ( _veh isEqualTo player ) then {
    if !( player getVariable ["inWater", false] ) then {
		[player, "agonyStop", 1] call common_fnc_doGesture;
    };
} else {
    if !( isNil "reviveVehPreAnim" ) then {
		#ifdef DEBUG
		debug(LL_DEBUG, "wounded agonyStop: inVeh with preAnim");
		#endif
    	[player, reviveVehPreAnim, 2] call common_fnc_doAnim;
        reviveVehPreAnim = nil;
	} else {
    	private _seat = [];
		{	
    		if ((_x select 0) isEqualTo player) exitWith { _seat = _x; };
		} forEach (fullCrew _veh);
    	#ifdef DEBUG
    	private _debug = format["wounded agonyStop: inVeh --- %1", _seat];
		debug(LL_DEBUG, _debug);
		#endif
    	player setPosASL ((getPosASL player) vectorAdd [0,0,1000]);
    	_seat params ["_unit", "_position", "_index", "_turret"];
		if ( _position isEqualTo "driver" ) then {
			player moveInDriver _veh;
        	#ifdef DEBUG
			debug(LL_DEBUG, "wounded agonyStop: isDriver");
			#endif
		} else {
			if ( _position isEqualTo "cargo" ) then {
				player moveInCargo [_veh, _index];
            	#ifdef DEBUG
				debug(LL_DEBUG, "wounded agonyStop: inCargo");
				#endif
			} else {
				player moveInTurret [_veh, _turret];
            	#ifdef DEBUG
				debug(LL_DEBUG, "wounded agonyStop: inTurret");
				#endif
			};
		};
	};        
};

if ( (primaryWeapon player) isEqualTo "FakeWeapon" ) then {
	player removeWeapon "FakeWeapon";
};

//removing JIP call from queue
reviveDelJIP = [reviveJIPAgonyStart];
reviveJIPAgonyStart = "";
if !( reviveJIPHasMoved isEqualTo "") then { reviveDelJIP pushback reviveJIPHasMoved; };
if !( reviveJIPHasShot isEqualTo "") then {  reviveDelJIP pushback reviveJIPHasShot; };
if !( reviveJIPInVeh isEqualTo "") then {   reviveDelJIP pushback reviveJIPInVeh; };
publicVariableServer "reviveDelJIP";

[-1] call commandChain_fnc_swapLeader;