/*
@filename: feats\revive\wounded\unloaded.sqf
Author:
	Ben
Description:
	run on player,
*/

#include "..\_debug.hpp"

params ["_veh", "_unloader"];

#ifdef DEBUG
private _debug = format["wounded unloaded : veh: %1, unloader: %2", _veh, _unloader];
debug(LL_DEBUG, _debug);
#endif

private _unloaded = false; 

if ( (vehicle player) isEqualTo _veh ) then {
    _unloaded = true; 
	moveOut player; 
	waitUntil {
    	sleep 0.1;
    	( !((vehicle player) isEqualTo _veh) || !(alive player) )
	};
};

#ifdef DEBUG
debug(LL_DEBUG, "wounded unloaded : is out of the vehicle");
#endif

if ( alive player ) then {
	player setVariable ["unloader", objNull, true];
	player setVariable ["inVeh", objNull, true];
    reviveNoMove = false;
	if ( !isNull _unloader && _unloaded ) then {
		private _vehName = getText(configFile >> "cfgVehicles" >> (typeOf _veh) >> "displayName");
		private _msg = (["revive", "load", "unloaded"] call core_fnc_getSetting); 
		format[_msg, _vehName, (name _unloader)] call revive_fnc_dynamicText;
	};
};

//removing JIP call from queue
reviveDelJIP = [reviveJIPInVeh];
publicVariableServer "reviveDelJIP";
reviveJIPInVeh = "";