/*
@filename: feats\revive\wounded\inWater.sqf
Author:
	Ben
Description:
	run on player,
*/

#include "..\_debug.hpp"

#ifdef DEBUG
debug(LL_DEBUG, "wounded inWater");
#endif

player setVariable ["inWater", true];

false call revive_fnc_woundedMute;

reviveLockTime = 0;

sleep 2;

if (((getPosASL player) select 2) >= -2.2 ) exitWith {
	[player, "asswpercmstpsnonwnondnon", 1] call common_fnc_doAnim;    
};

while { (((eyePos player) select 2) < 0) && (alive player) && (player getVariable ["agony", false]) } do {
    if !( (animationState player) isEqualTo "AsswPercMstpSnonWnonDnon_goup" )  then {
		[player, "AsswPercMstpSnonWnonDnon_goup", 1] call common_fnc_doAnim;        
    };
    (getPosASL player) params ["_x", "_y", "_z"];
    player setPosASL [_x, _y, (_z + 0.1)];
    sleep 0.05;
};

#ifdef DEBUG
systemChat "surface reached, switching to idle swim anim");
#endif

[player, "asswpercmstpsnonwnondnon", 1] call common_fnc_doAnim;