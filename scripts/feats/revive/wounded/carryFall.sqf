/*
@filename: feats\revive\wounded\carryFall.sqf
Author:
	Ben
Description:
	run on all context,
*/

#include "..\_debug.hpp"

#ifdef DEBUG
debug(LL_DEBUG, "wounded carry fall");
#endif


player setVariable ["carrier", ObjNull, true];
terminate reviveAnimThread;
detach player;
reviveNoMove = false;
reviveLockTime = 5;

[player, "AinjPfalMstpSnonWnonDnon_carried_still", 1] call common_fnc_doAnim;

if ( player getVariable["hasMoved", false] ) then {
	[player, "AinjPfalMstpSnonWrflDnon_AmovPpneMstpSrasWrflDnon_injured", 1] call common_fnc_doAnim;
} else {
	//[player, "agonyStart", 1] call common_fnc_doGesture;
	[player, "AinjPfalMstpSnonWrflDnon_carried_Down", 1] call common_fnc_doAnim;
};
