
params ["_wounded", "_veh"];

if !( isNull _veh ) then {
	if ( [_veh, reviveFaks] call revive_fnc_gotItem ) exitWith {
        private _items = getItemCargo _veh;
        private _idx = (_items select 0) find reviveFaks;
        private _amount = (_items select 1) select _idx;
        (_items select 1) set [_idx, (_amount - 1)];
        clearItemCargoGlobal _veh;
        {
            _veh addItemCargoGlobal [_x, ((_items select 1) select _forEachIndex)];
        } forEach (_items select 0);
    };
};

if ( [_wounded, reviveFaks] call revive_fnc_gotItem ) exitWith {
	_wounded removeItem reviveFaks;
};

player removeItem reviveFaks;