// by EightSix

#include "_debug.hpp"

params[["_partName", ""], ["_prct", 1], ["_left", 0]];

#define MPSF_PROGBAR_BG (uiNamespace getVariable "reviveProgressBar_gui_bg")
#define MPSF_PROGBAR_FG (uiNamespace getVariable "reviveProgressBar_gui_fg")
#define MPSF_PROGBAR_TXT (uiNamespace getVariable "reviveProgressBar_gui_txt")

disableSerialization;

private _display = uinamespace getvariable ["reviveProgressBar_gui", objNull];
if( isNull _display ) then {
	if ( isnil "reviveHudtext_n" ) then { reviveHudtext_n = 2733; };
	reviveHudtext_n cutRsc ["reviveProgressBar","plain"];
	reviveHudtext_n = reviveHudtext_n + 1;
	_display = uinamespace getvariable ["reviveProgressBar_gui", objNull];
    reviveProgressBar = true;
};

private _max = ctrlPosition MPSF_PROGBAR_BG select 2;
private _pos = ctrlPosition MPSF_PROGBAR_FG;

_pos set [2, ( _max / 100 * _prct )];

MPSF_PROGBAR_BG ctrlSetFade 0.3;
MPSF_PROGBAR_BG ctrlCommit 0;
MPSF_PROGBAR_FG ctrlSetPosition _pos;
MPSF_PROGBAR_FG ctrlSetFade 0.3;
MPSF_PROGBAR_FG ctrlCommit 0.05;
MPSF_PROGBAR_TXT ctrlSetFade 0.0;
MPSF_PROGBAR_TXT ctrlCommit 0;

private _sitrep = format[(["revive", "heal", "progressBar"] call core_fnc_getSetting), _partName, ((str round(_prct)) + "%"), (_left max 0)];
MPSF_PROGBAR_TXT ctrlsetstructuredtext parsetext _sitrep;

if ( reviveProgressBarHide ) then {	
	reviveProgressBarHide = false;
	[] spawn {
        #ifdef DEBUG
		debug(LL_DEBUG, "player healing : pgbar hide thread is running");
		#endif
        
        private _pos = ctrlPosition MPSF_PROGBAR_FG;
		_pos set [2,0.02];

		MPSF_PROGBAR_BG ctrlSetFade 1;
		MPSF_PROGBAR_BG ctrlCommit 1;
		MPSF_PROGBAR_FG  ctrlSetFade 1;
		MPSF_PROGBAR_FG  ctrlSetPosition _pos;
		MPSF_PROGBAR_FG  ctrlCommit 1;
		MPSF_PROGBAR_TXT ctrlSetFade 1;
		MPSF_PROGBAR_TXT ctrlCommit 1;
		
        sleep 1;
        
        reviveProgressBar = false;
	};
};