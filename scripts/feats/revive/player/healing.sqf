/*
@filename: feats\revive\player\healing.sqf
Author:
	Ben
Description:
	run on player,
*/

#include "..\_debug.hpp"

#ifdef DEBUG
debug(LL_DEBUG, "player healing");
#endif

params ["_wounded", ["_veh", objNull]];

private ["_lifeTime", "_defi", "_animEH"];

if ( isNull _veh ) then {
	call revive_fnc_weaponCheck;
	revivePreAnim = animationState player;
	[player, "medicStart"] call common_fnc_doGesture;
	reviveAnimDelay = time + 2;
	_animEH = player addEventhandler ["AnimChanged", {_this call revive_fnc_healAnimEH} ];
	
    private _offset = [0.2, 0.7, 0]; 
	private _dir = (getDir player) + 180;// + 270;
	private _relpos = player worldToModel (getPos _wounded);
	if( (_relpos select 0) < 0 ) then { _offset = [-0.2, 0.7, 0]; };

	_wounded setDir _dir;
	_wounded attachTo [player, _offset];

	private _pos = player modelToWorld [-0.5,0.2,0];
	_defi = "Land_Defibrillator_F" createVehicle _pos;
	_defi setPos _pos;
	_defi setDir (getDir player - 180);

	_lifeTime = ["revive", "heal", "garbageLifeTime"] call core_fnc_getSetting;
	reviveHealEquipment = [];
};

private _hitParts = (getAllHitPointsDamage _wounded) select 1;
private _dftHitPoints = [];
{ _dftHitPoints pushback 0; } forEach _hitParts;
private _hitPoints = _wounded getVariable ["hitPoints", _dftHitPoints];

private _skillFactor = (["revive", "heal", "baseFactor"] call core_fnc_getSetting);
private _randomFactor = (["revive", "heal", "randomFactor"] call core_fnc_getSetting);
_skillFactor = _skillFactor - (random _randomFactor);
if ( (player getVariable "role") isEqualto "medic" ) then {
    private _medicFactor = (["revive", "heal", "medicFactor"] call core_fnc_getSetting); 
	_skillFactor = _skillFactor - _medicFactor;
};
if !( isNull (_wounded getVariable ["medEvac", objNull]) ) then {
    private _medEvacFactor = (["revive", "heal", "medEvacFactor"] call core_fnc_getSetting);
	_skillFactor = _skillFactor - _medEvacFactor;
};
if ( _skillFactor <= 0 ) then { _skillFactor = 0.1; };

private _timeFactor = (["revive", "heal", "timeFactor"] call core_fnc_getSetting);
if ( _timeFactor <= 0 ) then { _timeFactor = 1; };

private _startTime = time;
private _finishAt = _startTime;

private _partsFinish = [];
{ 
    _finishAt = _finishAt + (_x * _timeFactor * _skillFactor);
    _partsFinish set [_forEachIndex, _finishAt]; 
} forEach _hitPoints;

//to prevent too short / long revive
private _timeNeeded = _finishAt - _startTime;
private _overwrite = 0;
private _maxTime = (["revive", "heal", "maxTime"] call core_fnc_getSetting);
private _minTime = (["revive", "heal", "minTime"] call core_fnc_getSetting);
if ( _timeNeeded > _maxTime ) then {
	_overwrite = _maxTime;    
} else {
    if ( _timeNeeded < _minTime ) then { _overwrite = _minTime; };
};

if ( _overwrite > 0 ) then {
    private _totDmg = 0;
    { _totDmg = _totDmg + _x; } forEach _hitPoints;
    _finishAt = _startTime;
    {
        if ( _totDmg > 0 ) then { _finishAt = _finishAt + (_overwrite * (_x / _totDmg) ); };
		_partsFinish set [_forEachIndex, _finishAt];        
	} forEach _hitPoints;
};

private _stopped = false;
private _partName = "";
private _prct = 1;
private _bbCount = 0;
private _bandageCount = 0;
private _medEvac = (_wounded getVariable["medEvac", objNull]);

private "_loopCond";

if ( isNull _veh ) then {
    _loopCond = {
        		params ["_partFinishAt", "_wounded"];
        		(time <= _partFinishAt)
				&& (alive player)
				&& (alive _wounded)
				&& !(player getVariable ["agony", false])
				&& !(player getVariable ["stopped", false])   
    };
} else {
    _loopCond = {
        		params ["_partFinishAt", "_wounded", "_veh"];
        		(time <= _partFinishAt)
				&& (alive player)
				&& (alive _wounded)
                && (alive _veh)
                && ((vehicle player) isEqualTo _veh)
                && ((vehicle _wounded) isEqualTo _veh)
				&& !(player getVariable ["agony", false])
				&& !(player getVariable ["stopped", false])
    };
};
{
	scopeName "healLoop";
	if ( _stopped ) then { breakOut "healLoop"; };
	_partName = _forEachIndex call {
        if ( _this isEqualTo 0) exitWith { "Face" };
        if ( _this isEqualTo 1) exitWith { "Neck" };
        if ( _this isEqualTo 2) exitWith { "Head" };
        if ( _this isEqualTo 3) exitWith { "Pelvis" };
        if ( _this isEqualTo 4) exitWith { "Spine" };
        if ( _this isEqualTo 5) exitWith { "Torso" };
        if ( _this isEqualTo 6) exitWith { "Back" };
        if ( _this isEqualTo 7) exitWith { "Body" };
        if ( _this isEqualTo 8) exitWith { "Arms" };
        if ( _this isEqualTo 9) exitWith { "Hands" };
        if ( _this isEqualTo 10) exitWith { "Legs" };
        "Scratches"
    };
    if ( isNull _veh ) then {
		if ( (_x >= 0.8) && (_bbCount <= 2) ) then {
			private _pos = player modelToWorld [0.4, (0.2 - (random 0.5)), 0];
			private _bb = "Land_BloodBag_F" createVehicle _pos;
			_bb setPos _pos;
			_bb setDir (random 359);
			reviveHealEquipment pushback [_bb, time + _lifeTime];
        	_bbCount = _bbCount + 1;
		};
		if ( (_x >= 0.5) && (_bandageCount <= 4) ) then {
			private _pos = player modelToWorld [(random 1.3), (0.8 + (random 0.6)), 0];
			private _band = "Land_Bandage_F" createVehicle _pos;
			_band setPos  _pos;
			_band setDir (random 359);
			reviveHealEquipment pushback [_band, time + _lifeTime];
        	_bandageCount = _bandageCount + 1;
		};
	};
    private _partPrct = 0;
    private _partStart = time;
    private _partFinish = (_partsFinish select _forEachIndex);
	while { [_partFinish, _wounded, _veh] call _loopCond } do {
		sleep 0.05;
		private _dur = (_finishAt - _startTime);
        if ( _dur <= 0 ) then { _dur = 1; };
        private _partDur = (_partFinish - _partStart);
		if ( _partDur <= 0 ) then { 
        	_partPrct = 1; 
        } else {
			_partPrct = (time - _partStart) / (_partFinish - _partStart);
		}; 
		_prct = (time - _startTime) / _dur * 100;
		[_partName, _prct, (round (_finishAT - time))] call revive_fnc_progressbar;
	};

    private _ok = ( !(player getVariable ["stopped", false]) && !(player getVariable ["agony", false]) && (alive player) && (alive _wounded) );
	if ( !(isNull _veh) && _ok ) then { _ok = ( (alive _veh) && ((vehicle player) isEqualTo _veh) && ((vehicle _wounded) isEqualTo _veh)); };
    private "_dmg";
	if ( _ok ) then {
        _dmg = 0;
    } else {
        _dmg = ( _x - (_x * _partPrct) );
    };
    _hitPoints set [_forEachIndex, _dmg];
	if ( (_forEachIndex < count _hitParts) && (_dmg <= reviveBloodThreshold) ) then {
		[_wounded, (_hitParts select _forEachIndex)] call revive_fnc_stopBleeding;
	};
	_stopped = !_ok;
	if ( !isNull(_medEvac) && !_stopped ) then {
		if ( (player distance _medEvac) > 10 ) then { _stopped = true; };
	};
} forEach _hitPoints;

player setVariable ["action", [], true];

if ( isNull _veh ) then { player removeEventHandler["Animchanged", _animEH]; };

reviveProgressBarHide = true;
[_partName, _prct] call revive_fnc_progressbar;

_wounded setVariable ["hitPoints", _hitPoints, true];

if ( isNull _veh ) then {
	if ( (alive player) && !(player getVariable["agony", false]) ) then {
		[player, "medicStop"] call common_fnc_doGesture;
	};
	detach player;
	detach _wounded;
};

waitUntil {
	sleep 0.2;
	!reviveProgressBar
};
        
if ( !(alive _wounded) || (isNull _wounded) ) then {
    #ifdef DEBUG
	debug(LL_DEBUG, "healing aborded, wounded died");
    #endif
	private _text = ["revive", "ripMessage"] call core_fnc_getSetting;
	format[_text, (name _wounded)] call revive_fnc_dynamicText;
} else {
	if ( _stopped ) then {
        #ifdef DEBUG
		debug(LL_DEBUG, "healing interrupted");
    	#endif
        player setVariable ["stopped", true, true];
        (["revive", "heal", "interrupted"] call core_fnc_getSetting) call revive_fnc_dynamictext;
	} else {
        #ifdef DEBUG
        debug(LL_DEBUG, "healing finished");
        #endif
        player setVariable ["finished", true, true];
    	if ( isNull (_wounded getVariable ["medEvac", objNull]) ) then {
            if ( reviveConsumeFaks ) then { [_wounded, _veh] call revive_fnc_removeFaks; }; 
		};
		reviveScore = [player, 1];
		publicVariableServer "reviveScore";
	};    
};

if ( isNull _veh ) then {
	deleteVehicle _defi;
	publicVariableServer "reviveHealEquipment";
};