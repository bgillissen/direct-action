/*
@filename: feats\revive\player\carrying.sqf
Author:
	Ben
Description:
	run on player,
*/

#include "..\_debug.hpp"

#ifdef DEBUG
debug(LL_DEBUG, "player carrying");
#endif

params ["_wounded"];

private _name = (name _wounded);

call revive_fnc_weaponCheck;

revivePreAnim = animationState player;

private _action = ["revive", "actions", "drop"] call core_fnc_getSetting;

[objNull] call revive_fnc_removeDropAction;
/*
if ( reviveDropAction >= 0 ) then { 
	player removeAction reviveDropAction;
	reviveDropAction = -1;
};
if ( reviveCDLAction >= 0 ) then { 
	player removeAction reviveCDLAction;
	reviveCDLAction = -1; 
};
reviveDropAction = player addAction [format[_action, _name], {_this call revive_fnc_actionDrop}, _wounded, 100, false, true, "", "true"];

if ( reviveCDLAction >= 0 ) then { player removeAction reviveCDLAction; };
private _cond = "['CDLoad', _target] call revive_fnc_actionCondition";
private _call = { [((player getVariable ["action", ["", objNull]]) select 1)] call revive_fnc_actionLoad };
private _action = ["revive", "actions", "load"] call core_fnc_getSetting;
reviveCDLAction = player addAction [format[_action, _name], _call, "", 99, false, true, "", _cond];
*/

reviveNoMoveEH = (findDisplay 46) displayAddEventHandler ["KeyDown", {_this call revive_fnc_playerKeyUnbind}];

private _anim = toLower "AinjPfalMstpSnonWnonDnon_carried_Up";
waitUntil {
	sleep 0.05;
	( ((animationState _wounded) isEqualTo  _anim)
      || !(alive player) 
      || !(alive _wounded) 
      || (player getVariable ["agony", false]) )
};

if ( !(alive player) || !(alive _wounded) || (player getVariable ["agony", false]) ) exitWith {};

#ifdef DEBUG
[_wounded, format["%1 - carrier: carried_Up animation detected on wounded", serverTime]] call common_fnc_systemChat;
systemChat format["%1 - carrier: carried_Up animation detected on wounded", serverTime];
#endif

[player, "AcinPknlMstpSnonWnonDnon_AcinPercMrunSnonWnonDnon", 2] call common_fnc_doAnim;
			
private _limit = time + 15;
waitUntil { 
	sleep 0.05;
	( !(alive _wounded) || !(alive player) || (player getVariable ["agony", false]) || (time > _limit) )
};

if ( !(alive _wounded) || !(alive player) || (player getVariable ["agony", false]) ) exitWith {};

#ifdef DEBUG
[_wounded, format["%1 - carrier: carrier pickup anim end", serverTime]] call common_fnc_systemChat;
systemChat format["%1 - carrier: carrier pickup anim end", serverTime];
#endif
(findDisplay 46) displayRemoveEventHandler ["keyDown", reviveNoMoveEH];
revivePlayerNoMoveEH = -1;
[player, "AcinPercMstpSnonWnonDnon", 1] call common_fnc_doAnim;

//drop Action
reviveDropAction set [0, _wounded];
reviveDropAction set [1, (player addAction [format[_action, _name], {_this call revive_fnc_actionDrop}, _wounded, 100, false, true, "", "true"])];
//load Action
private _cond = "['CDLoad', _target] call revive_fnc_actionCondition";
private _call = { [((player getVariable ["action", ["", objNull]]) select 1)] call revive_fnc_actionLoad };
private _action = ["revive", "actions", "load"] call core_fnc_getSetting;
reviveDropAction set [2, (player addAction [format[_action, _name], _call, "", 99, false, true, "", _cond])];

waitUntil {
    sleep 1;
    ( !(alive _wounded) 
      || !(_wounded getVariable["agony", false])
      || !(alive player) 
      || (player getVariable["agony", false]) 
      || !((vehicle player) isEqualTo player)
      || (((getPosASL player) select 2) < -1.45) )
};

reviveActionThread = scriptNull;

if !( _wounded getVariable["agony", false] ) then {
	#ifdef DEBUG
	debug(LL_DEBUG, "carrying : wounded has been healed, droping");
	#endif	
	[_name, _wounded, true] call revive_fnc_playerCarryCanceled;
} else {
	if (  !(alive _wounded) || (isNull _wounded) ) then {
		#ifdef DEBUG
		debug(LL_DEBUG, "carrying : wounded is gone, droping");
		#endif
		[_name, _wounded] call revive_fnc_playerCarryCanceled;
	} else {
	    if !( (vehicle player) isEqualTo player ) then {
	    	#ifdef DEBUG
			debug(LL_DEBUG, "carrying : player entered a vehicle");
			#endif    
	        [_wounded] call revive_fnc_actionLoad;
	    } else {
			if ( ((getPosASL player) select 2) < -1.45 ) then {
	        	#ifdef DEBUG
				debug(LL_DEBUG, "carrying : forced drop, water is too deep");
				#endif
	        	"Water is too deep" call revive_fnc_dynamicText;
	    		[player, player, reviveDropAction, _wounded] call revive_fnc_actionDrop;
			};
		};
	};
};

#ifdef DEBUG
if ( !(alive player) || (player getVariable["agony", false]) ) then {
	debug(LL_DEBUG, "carrying : player is dead or in agony");
};
#endif