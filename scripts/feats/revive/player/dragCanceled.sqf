/*
@filename: feats\revive\player\dragCanceled.sqf
Author:
	Ben
Description:
	run on all context,
*/

#include "..\_debug.hpp"

#ifdef DEBUG
debug(LL_DEBUG, "player drag canceled");
#endif

params ["_name", "_wounded", "_alive"];

player setVariable ["action", [], true];

private "_text";
if ( _alive ) then {
	_text = ["revive", "okMessage"] call core_fnc_getSetting;
} else {
	_text = ["revive", "ripMessage"] call core_fnc_getSetting;
};
format[_text, _name] call revive_fnc_dynamicText;

[player, revivePreAnim] call common_fnc_doAnim;

detach player;

reviveActionThread = scriptNull;

[_wounded] call revive_fnc_removeDropAction;
/*
player removeAction reviveDropAction;
player removeAction reviveCDLAction;
reviveDropAction = -1;
reviveCDLAction = -1;
*/