#include "..\..\..\common\_dik.hpp"

params ["_control", "_key", "_shift", "_ctrl", "_alt"];		

if ( _key isEqualTo DIK_ESCAPE ) exitWith { false };

if ( _ctrl ) exitWith { true };//to prevent stance change

private _handled = false;
private _list = ['MoveForward', 
				 'MoveBack', 
                 'TurnLeft', 
                 'TurnRight', 
                 'MoveFastForward', 
                 'MoveSlowForward',
                 'turbo',
				 'TurboToggle',
				 'MoveLeft',
				 'MoveRight',
                 'AdjustUp',
				 'AdjustDown',
                 'AdjustLeft',
                 'AdjustRight',
                 'Stand',
                 'Crouch',
                 'Prone',
                 'MoveUp',
                 'MoveDown',
                 'SwimUp',
                 'SwimDown', 
                 'EvasiveLeft', 
                 'EvasiveRight', 
                 'LeanLeft', 
                 'LeanLeftToggle', 
                 'LeanRight',
                 'LeanRightToggle',
                 'GetOver', 
                 'Salute', 
                 'SitDown'];
{
	if ( _key in (actionKeys _x) ) exitwith { _handled = true; };
} forEach _list;

_handled