/*
@filename: feats\revive\player\dragging.sqf
Author:
	Ben
Description:
	run on all context,
*/

#include "..\_debug.hpp"

#ifdef DEBUG
debug(LL_DEBUG, "player dragging");
#endif

params ["_wounded"];

private _name = (name _wounded);

call revive_fnc_weaponCheck;

revivePreAnim = animationState player;

[player, "grabDrag"] call common_fnc_doGesture;

_wounded setDir (getDir player + 180);

private _action = ["revive", "actions", "drop"] call core_fnc_getSetting;

[objNull] call revive_fnc_removeDropAction;

//drop action
reviveDropAction set [0, _wounded]; 
reviveDropAction set [1, (player addAction [format[_action, _name], {_this call revive_fnc_actionDrop}, _wounded, 100, false, true, "", "true"])];
//load action
private _cond = "['CDLoad', _target] call revive_fnc_actionCondition";
private _call = { [((player getVariable ["action", ["", objNull]]) select 1)] call revive_fnc_actionLoad };
private _action = ["revive", "actions", "load"] call core_fnc_getSetting;
reviveDropAction set [2, (player addAction [format[_action, _name], _call, "", 99, false, true, "", _cond])];

waitUntil {
    sleep 1;
    ( !(alive _wounded) 
      || !(_wounded getVariable["agony", false])
      || !(alive player) 
      || (player getVariable["agony", false])
      || !((vehicle player) isEqualTo player) 
      || (((getPosASL player) select 2) < -0.8) )
};

reviveActionThread = scriptNull;

if !( _wounded getVariable["agony", false] ) then {
	#ifdef DEBUG
	debug(LL_DEBUG, "dragging : wounded has been healed, droping");
	#endif	
	[_name, _wounded, true] call revive_fnc_playerDragCanceled;
} else {
	if (  !(alive _wounded) || (isNull _wounded) ) then {
		#ifdef DEBUG
		debug(LL_DEBUG, "dragging : wounded is gone, droping");
		#endif	
		[_name, _wounded, false] call revive_fnc_playerDragCanceled;
	} else {
	    if !( (vehicle player) isEqualTo player ) then {
	    	#ifdef DEBUG
			debug(LL_DEBUG, "dragging : player entered a vehicle");
			#endif    
	        [_wounded] call revive_fnc_actionLoad;
	    } else {
			if ( ((getPosASL player) select 2) < -0.8 ) then {
		        #ifdef DEBUG
				debug(LL_DEBUG, "dragging : forced drop, water is too deep");
				#endif
		        "Water is too deep" call revive_fnc_dynamicText;
		    	[player, player, reviveDropAction, _wounded] call revive_fnc_actionDrop;
			};
		};
	};
};
#ifdef DEBUG
if ( !(alive player) || (player getVariable["agony", false]) ) then {
	debug(LL_DEBUG, "dragging : player is dead or in agony");
};
#endif