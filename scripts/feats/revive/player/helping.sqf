/*
@filename: feats\revive\player\helping.sqf
Author:
	Ben
Description:
	run on player,
*/

#include "..\_debug.hpp"

#ifdef DEBUG
debug(LL_DEBUG, "player helping");
#endif

params ["_wounded"];

call revive_fnc_weaponCheck;
revivePreAnim = animationState player;


[player, "AinvPknlMstpSlayWrflDnon_1"] call common_fnc_doAnim;
reviveAnimDelay = time + 2;
private _animEH = player addEventhandler ["AnimChanged", {_this call revive_fnc_helpAnimEH} ];

private _offset = [0.2, 0.7, 0];
private _dir = (getDir player) + 180;// + 270;
private _relpos = player worldToModel (position _wounded);
if ( (_relpos select 0) < 0) then { _offset=[-0.2, 0.7, 0]; };

_wounded setDir _dir;
_wounded attachTo [player, _offset];

while {
	( (alive player)
	&& (alive _wounded)
	&& ((player distance _wounded) < 2)
	&& !(player getVariable ["agony", false])
	&& !(player getVariable ["stopped", false]) )
} do {
	sleep 0.2;
};

player setVariable ["action", [], true];
player removeEventHandler["Animchanged", _animEH];

[player, revivePreAnim, 2] call common_fnc_doAnim;        	

detach player;
detach _wounded;