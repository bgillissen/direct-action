/*
@filename: feats\revive\player\release.sqf
Author:
	Ben
Description:
	run on player,
*/

#include "..\_debug.hpp"

#ifdef DEBUG
debug(LL_DEBUG, "player release");
#endif

params["_isDrag", "_isCarry", "_wounded"];


terminate reviveActionThread;
reviveActionThread = scriptNull;

if ( reviveNoMoveEH >= 0 ) then {
	(findDisplay 46) displayRemoveEventHandler ["keyDown", reviveNoMoveEH];
    reviveNoMoveEH = -1;  
};

player setVariable ["action", [], true];

[_wounded] call revive_fnc_removeDropAction;
/*
player removeAction reviveDropAction;
player removeAction reviveCDLAction;
reviveDropAction = -1;
reviveCDLAction = -1;
*/
if !( (vehicle player) isEqualto player ) exitWith {
	#ifdef DEBUG
    debug(LL_DEBUG, "player release: player is in a vehicle");
    #endif
};

if ( _isDrag ) then {
    #ifdef DEBUG
    debug(LL_DEBUG, "player release: isDrag");
    #endif
	[player, revivePreAnim, 2] call common_fnc_doAnim;
	detach player;
};

if ( _isCarry ) then {
    #ifdef DEBUG
    debug(LL_DEBUG, "player release: isCarry");
    #endif
	[player, "AcinPercMrunSnonWnonDf_AmovPercMstpSnonWnonDnon", 2] call common_fnc_doAnim;	
	revivePreAnim spawn {
		sleep 5;
		[player, _this] call common_fnc_doAnim; 
		detach player;
	};
};

if ( (primaryWeapon player) isEqualTo "FakeWeapon" ) then {
	player removeWeapon "FakeWeapon";
};