
private _dragger = _this;
private _still = true;
private _nextMove = time;
while { !isNull(_dragger) } do {
	if ( (speed _dragger) isEqualTo  0 ) then {
		if !( _still ) then {
			[player, "AinjPpneMrunSnonWnonDb_still", 1] call common_fnc_doAnim;
			_still = true;
		};
	} else {
		if ( _still || (time > _nextMove) )then {
			[player, "AinjPpneMrunSnonWnonDb", 1] call common_fnc_doAnim; //1s
			_nextMove = time + 1; 
			_still = false;
		};
	};
	sleep 0.1;
	_dragger = player getVariable ["dragger", objNull];
};