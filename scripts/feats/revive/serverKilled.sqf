/*
@filename: feats\revive\serverKilled.sqf
Author:
	Ben
Description:
	run on server,
	remove queued JIP call for that player
*/

#include "_debug.hpp"

if ( MOD_ace ) exitWith {
    #ifdef DEBUG
    debug(LL_INFO, "disabled by the presence of ACE");
    #endif
    nil
};

params ["_eventArgs", "_uid"];

{
	_x params ["_quid", "_jip"];
	if ( _quid isEqualTo _uid ) then {
   		#ifdef DEBUG
		private _debug = format["del JIP (killed) : %1", _jip];
   		debug(LL_DEBUG, _debug);
   		#endif  
		remoteExec["", _jip];
		reviveJIPServer deleteAt _forEachIndex;
   	};
} forEach reviveJIPServer;

nil