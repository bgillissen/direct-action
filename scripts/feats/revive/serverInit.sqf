/*
@filename: feats\revive\serverInit.sqf
Credit:
	psycho 
Author:
	Ben
Description:
	run on player,
	define reviveBleedStack public variables,
	clean up the spawned heal equipment 
*/

#include "_debug.hpp"

if ( MOD_ace ) exitWith {
    #ifdef DEBUG
    debug(LL_INFO, "disabled by the presence of ACE");
    #endif
    nil
};

reviveBleedStackServer = [];
publicVariable "reviveBleedStackServer";

if ( isNil "reviveHealEquipmentPVEH" ) then {
    reviveHealEquipmentServer = [];
	reviveHealEquipmentPVEH = "reviveHealEquipment" addPublicVariableEventHandler {
		reviveHealEquipmentServer append (_this select 1);
	};
};

if ( (["revive", "bloodParticle"] call core_fnc_getSetting) == 1 ) then {
    if ( isNil "reviveBloodServerPVEH" ) then {
        reviveBloodServerPVEH = true;
		"reviveAddBlood" addPublicVariableEventHandler {
            (_this select 1) params ["_nunit", "_nuid", "_nbodyPart"];
			private _add = true;
			{
				_x params ["_unit", "_uid", "_bodyPart"];
				if ( _unit isEqualTo _nunit ) then {
					if ( _bodyPart isEqualTo _nbodyPart ) then { _add = false; };
				};
			} forEach reviveBleedStackServer;
			if ( _add ) then { reviveBleedStackServer pushback (_this select 1); };
            publicVariable "reviveBleedStackServer";   
		};
		"reviveDelBlood" addPublicVariableEventHandler {
           	(_this select 1) params ["_wounded", ["_part", ""]];
           	{
				_x params ["_unit", "_uid", "_bodyPart"];
				if ( _unit isEqualTo _wounded ) then {
					if ( (_part isEqualTo _bodyPart) || (_part isEqualTo "") ) then {
						reviveBleedStackServer deleteAt _forEachIndex;
					};
				};
			} forEach reviveBleedStackServer;  
            publicVariable "reviveBleedStackServer";
		};
	}; 
};

if ( isNil "reviveJIPServer" ) then { reviveJIPServer = []; };

if ( isNil "reviveJIP_PVEH" ) then {
    reviveJIP_PVEH = true;   
	"reviveAddJIP" addPublicVariableEventHandler {
        #ifdef DEBUG
        private _debug = format["add JIP : %1", (_this select 1)];
        debug(LL_DEBUG, _debug);
        #endif
		reviveJIPServer pushback (_this select 1);
	};
	"reviveDelJIP" addPublicVariableEventHandler {
        private _toDel = (_this select 1);
		{
            _x params ["_uid", "_jip"];
            if ( _jip in _toDel ) then {
                #ifdef DEBUG
        		private _debug = format["del JIP (request) : %1", _jip];
        		debug(LL_DEBUG, _debug);
        		#endif 
            	nil remoteExec["", _jip];
                reviveJIPServer deleteAt _forEachIndex; 
			};
        } forEach reviveJIPServer;
	};
};

if ( isNil "reviveScore_PVEH" ) then {   
	reviveScore_PVEH = "reviveScore" addPublicVariableEventHandler {
		(_this select 1) params ["_player", "_score"];
        _player addScore _score; 
	};
};

private _delay = (["cleanup", "loopDelay"] call core_fnc_getSetting);

#ifdef DEBUG
private _debug = format["cleanup thread is running with %1s delay", _delay];
debug(LL_DEBUG, _debug);
#endif
    
while { true } do { 
	sleep _delay;
	call revive_fnc_removeHealEquipment;
    {
        _x params ["_unit"];
        if ( isNull _unit ) then { reviveBleedStackServer deleteAt _forEachIndex; };
    } forEach reviveBleedStackServer;
    publicVariable "reviveBleedStackServer";
};

nil