class revive : feat_base  {
	class player : featPlayer_base {
		class destroy : featEvent_enable {};
		class init : featEvent_enable {};
		class killed : featEvent_enable { thread = 1; };
		class leave : featEvent_enable {};
		class respawn : featEvent_enable {};
	};
	class server : featServer_base {
		class destroy : featEvent_enable {};
		class init : featEvent_enable { thread = 1; };
		class killed : featEvent_enable {};
		class leave : featEvent_enable {};
	};
};
