class revive {
	resistance = 1;			//damage resistance factor, 1 for default, higher to increase, lower to decrease
	bloodParticle = 0;		//generate extra blood particles
	bloodThreshold = 0.5;	//damage at which a hit part start bleeding
	impact = 1;				//visible impact effect (white flash)
	showTimer = 1;			//show bleedout countdown
	deadCam = 1;			//camera span + quotes when player die
	mediKitClass = "Medikit";
	faksClass = "FirstAidKit";
	loadable[] = {"Air", "LandVehicle", "Ship"};
	moveable[] = {"Ship", "Car", "Truck"};
	ladderAnims[] = {"laddercivildownloop", "laddercivildownoff", "laddercivil_onbottom", "laddercivil_ontop", "laddercivilstatic", "ladderciviltopoff", "ladderciviluploop", "ladderrifledownloop", "ladderrifledownoff", "ladderrifleon", "ladderriflestatic", "ladderrifletopoff", "ladderrifleuploop"};
	loadDistance = 13;
	class agony {
		//display a message when a unit enter agony mode (0 disabled, 1 side, 2 group)
		unitMode = 2;
		//message to display when a unit enter agony mode (%1 unit name, %2 grid position)
		unitMsg = "%1 is down, he needs help at %2";
		//display a message when all the units of a squad are in agony (0 disabled, 1 side, 2 group)
		squadMode = 1;
		//message to display when a all the units of a squad are in agony (%1 squad name)
		squadMsg = "%1 has been wiped!";
		//change the leader of a squad when his leader go down (restore it when he is back up)
		changeLeader = 1;
		//display a message when teamleader goes down, disabled if changeLeader is disabled, otherwise (0 disabled, 1 side, 2 group)
		leadMode = 2;
		//message to display when teamleader goes down (%1 new TL)
		leadMsg = "%1 is taking command";
		noChat = "Chat is disabled while unconcious";
		help = "Help me! Please!";
		helpTooSoon = "Quit screaming - you already have called out for help!";
		helpInCharge = "Someone is already taking care of your ass, stop screaming.";
		deadlyPartThreshold = 1.2;
		partThreshold = 1.7;
	};
	class actions {
		drag = "<t color='#FC9512'>Drag %1</t>";
		carry = "<t color='#FC9512'>Carry %1</t>";
		drop = "<t color='#FC9512'>Drop %1</t>";
		help = "<t color='#FC9512'>Stop %1's bleeding temporarily</t>";
		heal = "<t color='#FF0000'>Revive %1</t>";
		load = "<t color='#FC9512'>Load %1</t>";
		unload = "<t color='#FC9512'>Unload %1</t>";
		vehHeal = "<t color='#FF0000'>Revive %1 (medEvac)</t>";
		moveToCargo= "<t color='#FC9512'>Move %1 to cargo</t>";
	};
	class carry {
		withLauncher = 0;
		launcherMsg = "You are not able to carry anyone else while carrying a launcher on your back.";
	};
	class load {
		noneNearby = "No vehicle nearby.";
		noneStopped = "No stopped vehicle nearby, try again.";
		noCargoSeat = "No free cargo seat left in %1";
		loading = "Loading %1 into %2.";
		loaded = "You have been loaded into %1 by %2";
		unloaded = "You have been unloaded from %1 by %2";
	};
	class moveToCargo {
		noCargoSeat = "No free cargo seat left in %1";
		moved = "You have been moved to a cargo seat by %1";
	};
	class medEvac {
		entities[] = {"Air", "LandVehicle", "Ship", "ReammoBox_F"};
		noneNearby = "No medEvac vehicle nearby.";
		noneStopped = "No stopped medEvac vehicle nearby, try again.";
	};
	class heal {
		timeFactor = 10;//was 15
		baseFactor = 1;
		randomFactor = 0.1;
		medicFactor = 0.2;
		medEvacFactor = 0.15;
		maxTime = 120;
		minTime = 5;
		progressBar = "<t align='left' shadow='1'>Applying First Aid : %1, %3s left ( %2 )</t>";
		interrupted = "Healing process has been interrupted";
		garbageLifeTime = 30;
	};
	ripMessage = "We lost %1, R.I.P";
	okMessage = "%1 is up !";
	dynamicText = "<t color='#fDCDBF6' font='PuristaMedium'>%1</t>";
	quote = "<t color='#ffffff'>%1</t><br/><br/><br/><t color='#ffffff'><t font='PuristaMedium'><t align='right'>%2</t></t></t>";
	class quotes {
		class entry0 {
			quote = "It's not waterboarding, it's baptizing them with freedom";
			author = "Neuel - 2016";
		};
		class entry1 {
			quote = "All you Americans had to do was sign the paper";
			author = "Jeffs - 2016";
		};
		class entry2 {
			quote = "So ugh... we give them a warning shot to the head is that it?";
			author = "GenericNord - 2016";
		};
		class entry3 {
			quote = "Let's go spread some freedom";
			author = "Neuel - 2016";
		};
		class entry4 {
			quote = "Because you are The God of Thunder roaaaaaar!!!!!";
			author = "Big - 2016";
		};
		class entry5 {
			quote = "You can forget about hearts and minds";
			author = "GenericNord - 2016";
		};
		class entry6 {
			quote = "That's exactly how I like my enemies, incompetent.";
			author = "GenericNord - 2016";
		};
		class entry7 {
			quote = "Frag... instead of smoke";
			author = "GenericNord - 2015";
		};
		class entry8 {
			quote = "JEFFS WAVE OFF!!!";
			author = "Neuel - 2016";
		};
		class entry9 {
			quote = "Sounds more like you're setting it up for a... a really nasty gangbang.";
			author = "GenericNord - 2016";
		};
		class entry10 {
			quote = "Don't drive off the bridge don't drive off the bridge! --- SHUT THE FAAAAAAAAAKK UUUUUUUUUUP!!!";
			author = "Neuel --- GenericNord - 2016";
		};
		class entry11 {
			quote = "And red team was as useful as a soggy tampon";
			author = "Michael - 2016";
		};
		class entry12 {
			quote = "...Speaking of bitches, where's Haggis?";
			author = "Rainman - 2017";
		};
		class entry13 {
			quote = "Could you uh...check that range properly.... and I'll see if it's in range for my uh, Blaster here?";
			author = "Rainman - 2017";
		};
		class entry14 {
			quote = "I don't speak london";
			author = "Tankac - 2016";
		};
		class entry15 {
			quote = "I think I can clear that up with a rifle butt to the head";
			author = "GenericNord - 2017";
		};
		class entry16 {
			quote = "Neuel.. I am about to clock you like an orphan";
			author = "GenericNord - 2017";
		};
		class entry17 {
			quote = "I want a purple heart! (Operation Goat Hide)";
			author = "Nancy - 2017";
		};
		class entry18 {
			quote = "Neuel .556'd the fuck out of him with like 100 rounds, he even stopped to reload";
			author = "Big - 2017";
		};
		class entry19 {
			quote = "Don't shoot the bushes!";
			author = "Tinman - 2017";
		};
		class entry20 {
			quote = "No, I won't pee in the Humvee...";
			author = "Haggis - 2017";
		};
		class entry21 {
			quote = "Don't bathe in the blood of your enemies";
			author = "GenericNord - 2017";
		};
		class entry22 {
			quote = "Did you need a gun for that scope Nancy?";
			author = "Pistol Pete - 2017";
		};
		class entry23 {
			quote = "If you got weapons on your lasers, put them on";
			author = "Neuel - 2017";
		};
		class entry24 {
			quote = "Holy shit Nancy, got enough stuff on that gun yet? Just missing an Ipod dock!";
			author = "Neuel - 2017";
		};
		class entry25 {
			quote = "What's up with the butterflies? --- Nothing's wrong with the butterflies --- Fucking disney princesses";
			author = "GenericNord --- Big - 2017";
		};
		class entry26 {
			quote = "Can I shamelessly promote myself ?";
			author = "Serza - 2017";
		};
		class entry27 {
			quote = "Dorida, is this where they make doritos ?";
			author = "Neuel - 2017";
		};
		class entry28 {
			quote = "I am literally shoveling his brains back in his head as fast as I can";
			author = "Eldridge - 2017";
		};
		class entry29 {
			quote = "They can't predict what we do, if we can't do it either.";
			author = "Generic - 2017";
		};
		class entry30 {
			quote = "I am going to cap this guy.. if it's the last thing I do";
			author = "BobTheWarrior - 2017";
		};
		class entry31 {
			quote = "Don't move! Don't move Robert! -- Who's Robert?!";
			author = "Neuel -- BobTheWarrior - 2017";
		};
		class entry32 {
			quote = "Venancio if you see Jar Jar Binks kill that motherfucker";
			author = "Fran - 2017";
		};
		class entry33 {
			quote = "You Goddamn Muppets!!";
			author = "Nancy - 2017";
		};
		class entry34 {
			quote = "Your mod is retarded";
			author = "Haggis - 2017";
		};
		class entry35 {
			quote = "Neuel the one person that I thought would back me up on the colonial brouge and diversity of the English language and serendipity of the local coloquilism would of been you my fine comrade but no like Julius Caesar ' et tu Brutus'... !";
			author = "Haggis - 2017";
		};
		class entry36 {
			quote = "Adam's hard to keep up with. He's slippery like an uncooked wiener";
			author = "Neuel - 2017";
		};
		class entry37 {
			quote = "How the fuck did you not see that tree, it was standing there for the last 75 years.";
			author = "Haggis - 2017";
		};
		class entry38 {
			quote = "Where's Tinman? Oh, in the open of course!";
			author = "Ben - 2017";
		};
		class entry39 {
			quote = "If I had my UAV, I could see how many Russians are around us so we can cry -- Sometimes its best not to know";
			author = "Matthew -- Rainman - 2017";
		};
		class entry40 {
			quote = "Ahh TFU, where shooting someone in the face is a medical procedure";
			author = "GenericNord - 2017";
		};
		class entry41 {
			quote = "Unicorns do exist in real life, they're just fat, grey and are called rhinos";
			author = "Generic - 2018";
		};
		class entry42 {
			quote = "Overkill is the best Kill";
			author = "Generic - 2018";
		};
		class entry43 {
			quote = "He's like a succubus but with dingle dangle";
			author = "Haggis - 2017";
		};
		class entry44 {
			quote = "Did you see the way I caught that Howitzer with my face?";
			author = "Body Bag Blu - 2017";
		}
	};
};
