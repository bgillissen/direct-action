/*
@filename: feats\revive\playerDestroy.sqf
Author:
	Ben
Description:
	run on player,
	revive player, stop all bleeding, stop deadcam
*/

if ( count(_this) == 0 ) exitWith {
    player setVariable ["agony", false, true];
    
	{
		_x set [5, true]; 
		reviveBleedStack set [_forEachIndex, _x];			
	} forEach reviveBleedStack;
	
	if !( isNil "reviveIconEH" ) then {
		removeMissionEventHandler["Draw3D", reviveIconEH];
	    reviveIconEH = nil;
	};    
};

params ["_when", "_thread"];

if ( scriptDone _thread ) exitWith { nil };

terminate _thread;

waitUntil{ scriptDone _thread };

nil