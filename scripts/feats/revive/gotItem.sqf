/*
@filename: feats\revive\gotItem.sqf
Author:
	Ben
Description:
	run on player,
*/

#include "_debug.hpp"

params ["_unit", "_item"];

if ( _unit isKindOf "CAManBase" ) exitWith {
	private _items = ((getItemCargo uniformContainer _unit) select 0);
	if ( _item in _items ) exitWith { true };

	_items = ((getItemCargo vestContainer _unit) select 0);
	if ( _item in _items ) exitWith { true };

	_items = ((getItemCargo backpackContainer _unit) select 0);
	if ( _item in _items ) exitWith { true };

	false
};

private _items = getItemCargo _unit;

(_item in (_items select 0) );