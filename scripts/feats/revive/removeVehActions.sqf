/*
@filename: feats\revive\removeVehActions.sqf
Author:
	Ben
Description:
	run on player,
	remove unload action for given player's uid from given vehicle (optional)
*/

#include "_debug.hpp"

params ["_uid", ["_veh", objNull]];

If !( isNil "reviveVehActionStack" ) then {
	{
	    _x params ["_wUid", "_wVeh", "_actions"];
	    private _do = false;
	    if ( _wUid isEqualTo _uid ) then {
	        _do = true;
	      	if !( isNull _veh ) then { _do = (_wVeh isEqualTo _veh); };
	    };
	    if ( _do ) then {
	        { _wVeh removeAction _x; } forEach _actions;
			reviveVehActionStack deleteAt _forEachIndex;
	        #ifdef DEBUG
			private _debug = format["removeVehActions : action removed from %1 for %2", _wVeh, _wUid];
			debug(LL_DEBUG, _debug);
			#endif
    	};
	} forEach reviveVehActionStack;
};