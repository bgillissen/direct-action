/*
@filename: feats\nightVision\playerDestroy.sqf
Credits: 
	Quiksilver
Author:
	Ben
Description:
	run on client
	kill the waiting threads and remove the registred eventHandlers
*/

if ( (["nightVision"] call core_fnc_getParam) == 0 ) exitWith { nil };

params ["_when", "_thread"];

if ( scriptDone _thread ) exitWith { nil };

terminate _thread;

waitUntil{ scriptDone _thread };

nil