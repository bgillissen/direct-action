class nightVision {
	tag = "nightVision";
	class functions {
		class playerPostInit { file="feats\nightVision\playerPostInit.sqf"; };
		class playerDestroy { file="feats\nightVision\playerDestroy.sqf"; };
	};
};
