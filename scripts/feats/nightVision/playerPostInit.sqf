/*
@filename: feats\nightVision\playerPostInit.sqf
Credits: 
	Killzone Kid
Author:
	Ben
Description:
	run on client
	add some custom ppEffects to night / thermal vision
*/

#include "_debug.hpp"

if ( (["nightVision"] call core_fnc_getParam) == 0 ) exitWith {
    #ifdef DEBUG
    debug(LL_INFO, "disabled by mission parameter"); 
    #endif
    nil
};

NV_air = ( ['nightVision_airOptic'] call core_fnc_getParam == 1 );
NV_blurBase = ['nightVision', 'blurBase'] call core_fnc_getSetting;
NV_blurFactor = ['nightVision', 'blurFactor'] call core_fnc_getSetting;
NV_FilmBase = ['nightVision', 'filmBase'] call core_fnc_getSetting;
NV_FilmFactor = ['nightVision', 'filmFactor'] call core_fnc_getSetting;
NV_color = ['nightVision', 'color'] call core_fnc_getSetting;

private _getZoomFnc = {
    ( [0.5,0.5] distance worldToScreen positionCameraToWorld [0,1.05,1]) * (getResolution select 5 )
};

private _nvEffectFnc = {
	private _veh = (vehicle player);
    if ( _veh isEqualTo player ) exitWith { true };
    if !( NV_air ) exitWith {
    	(assignedVehicleRole player) params [['_role', '']];
		if ( ((_veh isKindOf "Plane") || (_veh isKindOf "Helicopter")) && ((_role isEqualTo "driver") || (_role isEqualTo "gunner")) ) exitWith {
			!(cameraView isEqualTo "GUNNER") 
		};
		true 
	};
    true
};

while { true } do {
    
    waitUntil {
        sleep 0.1;
        ( ((currentVisionMode player) == 1) && (isNull curatorCamera) )
    };
    // Color and Contrast
    private _ppColor = ppEffectCreate ["ColorCorrections", 1500];
    _ppColor ppEffectEnable true;
    _ppColor ppEffectAdjust [0.6, 2.5, -0.15, NV_color, [1,1,1,1], [0,0,0,0]]; 
    _ppColor ppEffectCommit 0;
    _ppColor ppEffectForceInNVG true;
    // Edge Blur
	private _ppRim = ppEffectCreate ["RadialBlur",100]; 
    _ppRim ppEffectEnable true;   
    _ppRim ppEffectAdjust [0.015, 0.015, 0.22, 0.3];
    _ppRim ppEffectCommit 0;  
    100 ppEffectForceInNVG true;
    
	private ["_ppBlur", "_ppFilm"];
	private _ppEffects = false;
	
    waitUntil {
        sleep 0.1;
        if ( call _nvEffectFnc ) then {
	        if !( _ppEffects ) then {
	        	_ppEffects = true;
	           	// Dynamic Blur
	        	_ppBlur = ppEffectCreate ["dynamicBlur", 250]; 
	        	_ppBlur ppEffectEnable true;    
    			250 ppEffectForceInNVG true;
    			// Film Grain
				_ppFilm = ppEffectCreate ["FilmGrain", 2000];
				_ppFilm ppEffectEnable true;   
	    		2000 ppEffectForceInNVG true;
	        };
	        private _zoom = (call _getZoomFnc * 7) / 30;
	    	_ppBlur ppEffectAdjust [NV_blurBase + (_zoom * NV_blurFactor)]; 
	    	_ppBlur ppEffectCommit 0; 	    
	        _ppFilm ppEffectAdjust [0.35, 1, (NV_filmBase + (_zoom * NV_filmFactor)), 0.4, 0.2, false];
	    	_ppFilm ppEffectCommit 0;  
        } else {
        	if ( _ppEffects ) then {
        		_ppEffects = false;       
        		ppEffectDestroy _ppBlur;
				ppEffectDestroy _ppFilm;
        	};
        };
        ( !((currentVisionMode player) == 1) || !( isNull curatorCamera ) )
    };
    
	ppEffectDestroy _ppColor;
	ppEffectDestroy _ppRim;
	if ( _ppEffects ) then {      
		ppEffectDestroy _ppBlur;
		ppEffectDestroy _ppFilm;
	};

};

nil