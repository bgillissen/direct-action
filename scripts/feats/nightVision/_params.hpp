class nightVision {
	title = "Use custom Night vision post-processing effects";
	values[] = {0, 1};
	texts[] = {"No", "Yes"};
	default = 0;
};

class nightVision_airOptic {
	title = "Use custom Nigh vision post-processing effects in air vehicle optic";
	values[] = {0, 1};
	texts[] = {"No", "Yes"};
	default = 0;
};
