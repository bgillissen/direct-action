/*
@filename: feats\checkTS\playerPostInit.sqf
Author:
	Ben
Description:
	run on player
 	check that player is connected to the defined TS server and in right channel
	his TFAR plugin got to properly enabled in TS for hom to pass through this
*/

#include "_debug.hpp"

if !( isMultiplayer ) exitWith {
    #ifdef DEBUG
    debug(LL_INFO, "not multiplayer, abording");
    #endif
    nil
};

if !( MOD_tfar ) exitWith {
    #ifdef DEBUG
    debug(LL_INFO, "TFAR is not loaded, abording");
    #endif
    nil
};

if ( (["checkTS"] call core_fnc_getParam) == 0 ) exitWith {
    #ifdef DEBUG
    debug(LL_INFO, "disabled by mission parameter");
    #endif
    nil
};

waitUntil { !PLAYER_INIT };

private _server = (["tfar", "srvName"] call core_fnc_getSetting);
private _channels = [];
if ( (["tfar", "seriousMode"] call core_fnc_getSetting) == 0 ) then {
    {
        _channels pushback getText(_x >> "name");
    } forEach ( "true" configClasses(missionConfigFile >> "settings" >> "tfar" >> "channels") );
} else {
	_channels pushback tf_radio_channel_name;
};

private _delay = (["checkTS", "delay"] call core_fnc_getSetting);
private _limit = serverTime + _delay;

#ifdef DEBUG
private _debug = format["delay: %1, server: %2, channels: %3", _delay, _server, _channels];
debug(LL_DEBUG, _debug);
#endif

private _expired = false;
private _ok = false;
private _first = true;
private _TSPlugin = false;
private _isOnServer = false;
private _inChannel = false;
    
while { !_expired && !_ok } do {
	private _playerServer = call TFAR_fnc_getTeamSpeakServerName;
	private _playerChannel = call TFAR_fnc_getTeamSpeakChannelName;
    private _now = serverTime;
	_TSPlugin = call TFAR_fnc_isTeamSpeakPluginEnabled;;
	_isOnServer = (_playerServer isEqualTo _server);
	_inChannel = (_playerChannel in _channels);
	_ok = ( _isOnServer && _TSPlugin && _inChannel );
	_expired = ( _now >= _limit );
	
	if ( _ok && _first ) exitWith {};
	DOLOCK = true;
	private _msg = "";
	if ( _ok && !_first ) then { 
		_msg = "Thanks."; 
	} else {
        if !( _TSPlugin ) then {
            _msg = ["checkTS", "messages", "notOnServer"] call core_fnc_getSetting;
			//_msg = ["checkTS", "messages", "noPlugin"] call core_fnc_getSetting;
		} else {
			if !( _isOnServer ) then { 
				_msg = ["checkTS", "messages", "notOnServer"] call core_fnc_getSetting;
			} else {
				_msg = ["checkTS", "messages", "wrongChannel"] call core_fnc_getSetting;
				_msg = format[_msg, (_channels select 0)];
			};
		};
		_msg = format["%1 ( %2s )", _msg, round (_limit - _now)];
	};
	"noTS" cutText [_msg, "BLACK", 0.1, true];
	_first = false;
	sleep 1;
};

if !( _ok ) then {
	"noTS" cutFadeOut 1;
    //if !( _TSPlugin ) exitWith { endMission "NOTSPLUGIN"; };
    if !( _TSPlugin ) exitWith { endMission "NOTONTS"; };
    if !( _isOnServer ) exitWith { endMission "NOTONTS"; };
    if !( _inChannel ) exitWith { endMission "WRONGCHANNEL"; };
}  else {
	if !( _first ) then {
		"noTS" cutFadeOut 1;
		DOLOCK = false;
	};
};

nil