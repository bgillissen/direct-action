class checkTS {
	delay = 60;
	class messages {
		notOnServer = "You must be on our TeamSpeak server (ts.taskforceunicorn.com) and have the TFAR plugin properly installed and activated to play here. Please double check this and try connecting again.";
		noPlugin = "TFAR radio plugin is not installed / activated on your TeamSpeak client, or it is not running and/or not connected to a server, make sure it is and try again.";
		wrongChannel = "You are in the wrong TeamSpeak channel, please join %1";
	};
};
