class NOTSPLUGIN {
	title = "Communication issue.";
	description = "Please rejoin once you have enabled the TFAR plugin on your teamSpeak client.";
	picture = "";
};
class NOTONTS {
	title = "Communication issue.";
	description = "Please rejoin once you are connected to our teamSpeak server (ts.taskforceunicorn.com) with the TFAR plugin enabled.";
	picture = "";
};
class WRONGCHANNEL {
	title = "Communication issue.";
	description = "Please rejoin once you joined the 'TFU Invade and Annex Server' channel on our teamSpeak server.";
	picture = "";
};
