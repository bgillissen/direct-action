/*
@filename: feats\hallOfShame\serverPostInit.sqf
Author:
	Ben
Description:
	run on server,
	apply texture to the hall of shame billboards and hidde the extra one
*/

#include "_debug.hpp"

private _i = 1;
#ifdef DEBUG
private _m = 0;
#endif
{
	private _entries = getArray(_x >> "hallOfShame" >> "entries");
	private _path = getText(_x >> "hallOfShame" >> "path");
	{
		private _name = format["BA_hos_%1", _i];
		private _obj = missionNamespace getVariable _name;
		if !( isNil "_obj" ) then {
			[_obj, 0, format[_path, _x]] call global_fnc_setTexture;
			_i = _i + 1;
        #ifdef DEBUG
        } else {
            _m = _m + 1;
        #endif
		};
	} forEach _entries;
} forEach ("true" configClasses (configFile >> "cfgDirectAction"));

#ifdef DEBUG
if ( _m > 0 ) then {
	private _debug = format["not enough bilboard placed, got: %1, need: %2", _i, (_m + _i)];
    debug(LL_WARN, _debug);
};
#endif

for "_x" from _i to 99 do {
	private _name = format["BA_hos_%1", _x];
	private _obj = missionNamespace getVariable _name;
	if !( isNil "_obj" ) then { hideObjectGlobal _obj; };
};

nil