/*
@filename: init.sqf
Author:
	Ben
Description:
	run on all context by game engine on mission start
	spawn init functions in single player
*/

if ( isMultiplayer ) exitWith {};

[] spawn core_fnc_initServer;
[] spawn core_fnc_initPlayer;

nil