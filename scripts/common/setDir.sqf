
params ["_unit", "_dir"];

if !( local _unit ) exitWith {
	_this remoteExec ["common_fnc_setDir", _unit, false];
};

_unit setDir _dir;