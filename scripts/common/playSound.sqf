/*
@filename: common\playSound.sqf
Author:
	Ben
Description:
	create a soundSource on the base things with a soundSource action
*/

#include "_debug.hpp"

if !( isServer ) exitWith {
	_this remoteExec["common_fnc_playSound", 2, false];    
};

params["_sfx", ["_time", 0]];

if !( isClass(configFile >> "CfgVehicles" >> _sfx) ) exitWith {
    #ifdef DEBUG
    private _debug = format["playSound: play failed, class (configFile >> CfgVehicles >> %1) was not found", _sfx];
    debug(LL_ERR, _debug);
    #endif
};

private _speakers = [];
{
	if !( (configName _x) isEqualTo "zones" ) then {
		private _actions = ("true" configClasses (_x >> "actions") apply { configName _x });
		if ( "soundSource" in _actions ) then {
			{
                private _name = (configName _x);
                private _type =  getText(_x >> "type");
                private _ATL = getNumber(_x >> "atl");
                private _pos = [0,0,0];
                if ( _type isEqualTo "marker" ) then {
                	_pos = getMarkerPos _name;    
                } else {
					private _thing = missionNamespace getVariable [_name, objNull];
                    _pos = getPos _thing;
				};
                if !(_pos isEqualTo [0,0,0] ) then {
                	if ( _ATL > 0 ) then { _pos set [2, _ATL]; };
					_speakers pushback _pos;
                #ifdef DEBUG
                	private _debug = format["playSound: adding source '%1'", _name];
    				debug(LL_DEBUG, _debug);
				} else {
                    private _debug = format["playSound: position not found for base thing '%1'", _name];
    				debug(LL_WARN, _debug);
                #endif    
                };
			} forEach (configProperties [(_x >> "things"), "isClass _x", true]);
		};
	};
} forEach (configProperties [(missionConfigFile >> "settings" >> "baseAtmosphere" >> toUpper(worldName) >> BASE_NAME), "isClass _x", true]);

if ( (count _speakers) isEqualTo 0 ) exitWith {
    #ifdef DEBUG
    debug(LL_ERR, "playSound: play failed, no source found");
    #endif
};

if !(isNil "soundObjects" ) then {
   { deleteVehicle _x; } forEach soundObjects; 
}; 

soundObjects = [];

if !( isClass(configFile >> "CfgVehicles" >> _sfx) ) exitWith {
	#ifdef DEBUG
	debug(LL_ERR, "playSound: play failed, class not found");
	#endif  
};


{ soundObjects pushback (createSoundSource [_sfx, _x, [], 0]); } forEach _speakers;

publicVariable "soundObjects";

private _length = getNumber(configFile >> "CfgVehicles" >> _sfx >> "length");

lastSoundClass = _sfx;
lastSoundEndTime = _time;

if ( _length > 0 ) then {
    if ( _time isEqualTo 0 ) then { _time = serverTime; };
    lastSoundEndTime = _time + _length;
	_length spawn {
        #ifdef DEBUG
		debug(LL_DEBUG, "playSound: cleanup thread has spawn");
		#endif
		sleep _this;
		call common_fnc_stopSound;
	};    
};

publicVariable "lastSoundClass";
publicVariable "lastSoundEndTime";