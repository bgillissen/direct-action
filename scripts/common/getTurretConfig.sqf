
#include "_debug.hpp"

params ["_thing", "_turretPath"];

private "_vType";
if ( _thing isEqualType objNull ) then {
	_vType = typeOf (vehicle _thing);
};
if ( _thing isEqualType "" ) then {
	_vType = _thing; 
};
if ( isNil '_vType' ) exitWith {
	#ifdef DEBUG
	debug(LL_ERR, 'getTurretConfig: first param must be a vehicle object or a vehicle class');
	#endif
	configNull
};

if ( (count _turretPath) isEqualTo 0 ) exitWith {
	#ifdef DEBUG
	debug(LL_DEBUG, 'getTurretConfig: turret is an empty array, abording');
	#endif
	configNull 
};

private _findRecurse = {
	params ['_cfg', '_turretPath'];
	if ( isNil "_cfg" ) exitWith { configNull };
	if ( _cfg isEqualType "") then { _cfg = (configFile >> 'cfgVehicles' >> _cfg); };
	
    private _find = (_turretPath select 0);
    _turretPath deleteAt 0;
    if ( (count(_cfg >> 'Turrets') - 1) < _find ) exitWith {
		#ifdef DEBUG
		private _debug = format["getTurretConfig: turret '%1' was not found in %2", _find, (_cfg >> 'Turrets')];
		debug(LL_ERR, _debug);
		#endif    	 
    	configNull 
	};
   	_cfg = (_cfg >> 'Turrets') select _find; 
   	if ( (count _turretPath) isEqualTo 0 ) exitWith { _cfg };
   	([_cfg, _turretPath] call _findRecurse)
};

([_vType, (_turretPath + [])] call _findRecurse)