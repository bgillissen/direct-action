
if !( isServer ) exitWith {
	remoteExec["common_fnc_stopSound", 2, false];    
};

{ deleteVehicle _x; } forEach soundObjects;
soundObjects = [];
publicVariable "soundObjects";