
params ["_slope", "_angle", "_bearing"];

_slope params ["_slopeX", "_slopeY"];

private _vUp = vectorNormalized [rad(_slopeX), rad(_slopeY), 1-(rad(_slopeX)+rad(_slopeY))];
private _v = [sin(_bearing), cos(_bearing), 1*sin(_angle)/sin(90-_angle)];
private _angleRel = 90 - acos(_vUp vectorCos _v);
private _z = (_v select 2) / (_vUp select 2);
private _x = (_v select 0) - (_z * (_vUp select 0));
private _y = (_v select 1) - (_z * (_vUp select 1));
private _bearingRel = acos([0,1,0] vectorCos [_x, _y, 0]);
if ( _x < 0 ) then { _bearingRel = 360 - _bearingRel; };

[_angleRel, _bearingRel]