//get max / min tube world elevation (relative to vehicle pitch / bank)

params ["_veh", "_turretPath", ["_slope", [0,0]], ["_bearing", 0]];

private _turretCfg = [_veh, _turretPath] call common_fnc_getTurretConfig;
private _maxElev = getNumber(_turretCfg >> 'maxElev');
private _minElev = getNumber(_turretCfg >> 'minElev');

if ( _veh isKindOf "StaticMortar" ) then {
	private _offset = 0;
	{
		if ( _veh isKindOf (configName _x) ) exitWith { _offset = getNumber _x; };
	} forEach (configProperties [missionconfigFile >> "settings" >> "artiComputer" >> "elevOffsets"]);
	_minElev = _minElev + _offset;
	_maxElev = _maxElev + _offset; 
};

_minElev = ([_slope, _minElev, _bearing] call arti_fnc_getRelativeData) select 0;
_maxElev = ([_slope, _maxElev, _bearing] call arti_fnc_getRelativeData) select 0;

[_minElev, _maxElev]