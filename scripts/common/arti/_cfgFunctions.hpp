class arti {
	tag = "arti";
	class functions {
		class findSolution { file="common\arti\findSolution.sqf"; };
		class getBearing { file="common\arti\getBearing.sqf"; };
		class getDistance { file="common\arti\getDistance.sqf"; };
		class getFireSolution { file="common\arti\getFireSolution.sqf"; };
		class getMags { file="common\arti\getMags.sqf"; };
		class getModes { file="common\arti\getModes.sqf"; };
		class getMuzzles { file="common\arti\getMuzzles.sqf"; };
		class getRelativeData { file="common\arti\getRelativeData.sqf"; };
		class getTubePosition { file="common\arti\getTubePosition.sqf"; };
		class getTurretLimits { file="common\arti\getTurretLimits.sqf"; };
		class getWeapon { file="common\arti\getWeapon.sqf"; };
		class gridToCoord { file="common\arti\gridToCoord.sqf"; };
		class isArtillery { file="common\arti\isArtillery.sqf"; };
		class simulateShot { file="common\arti\simulateShot.sqf"; };
	};
};
