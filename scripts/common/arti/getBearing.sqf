
params ["_tube", "_trgt"];

_tube params ["_tubeX", "_tubeY", "_tubeZ"];
_trgt params ["_trgtX", "_trgtY", "_trgtZ"];

private _dir = acos((_trgtY - _tubeY) / sqrt((_trgtX - _tubeX)^2 + (_trgtY - _tubeY)^2));
if ( _tubeX >= _trgtX ) then { _dir = 360 - _dir; };

_dir