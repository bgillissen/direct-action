#include "..\..\core\debugLevels.hpp"

//comment the following line to disable LARs debug
#define DEBUG()

//debug level for LARs
#define DEBUG_LVL LL_INFO
//debug context
#define DEBUG_CTXT "arti"

#include "..\__debug.hpp"
