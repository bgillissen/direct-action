
#include "_debug.hpp"

params ["_veh", "_turretPath", ["_mag", ""], ["_distance", -1], ["_altDiff", 0], ["_highAngle", true]];

([_veh, _turretPath] call arti_fnc_getTurretLimits) params ["_minElev", "_maxElev"];

if ( _highAngle ) then {
	if ( _minElev < 45 ) then { _minElev = 45; };
} else {
	if ( _maxElev > 45 ) then { _maxElev = 45; };
};

private _weapons = (_veh weaponsTurret _turretPath);
private "_weapon"; 
{
    if ( getText(configFile >> "cfgWeapons" >> _x >> "cursor") in ["mortar", "EmptyCursor"] ) exitWith {
    	_weapon = _x; 
	};
} forEach _weapons;

if ( isNil '_weapon' ) exitWith { [] };

if ( _mag isEqualTo "" ) then {
	private _mags = getArray(configFile >> "cfgWeapons" >> _weapon >> "magazines");
	if ( count _mags > 0 ) then { _mag = _mags select 0; }; 
};

if ( _mag isEqualTo "" ) exitWith { [] };

#define G 9.81

private _initSpeed = getNumber(configfile >> "CfgMagazines" >> _mag >> "initSpeed");

private _rangeFnc = {
	params ["_elev", "_v", "_alt"];
	((_v * (cos _elev)) / G) * ((_v * (sin _elev)) + sqrt ((_v * (sin _elev))^2 + (2 * G * _alt)));
};

private _modes = getArray(configFile >> "cfgWeapons" >> _weapon >> "modes");
private _baseCfg = (configFile >> "cfgWeapons" >> _weapon);

private _results = [];
{
	private _mode = _x;
	private _modeCfg = (_baseCfg >> _mode);
    if ( (getNumber(_modeCfg >> "burst") isEqualTo 1) && (getNumber(_modeCfg >> "showToPlayer") isEqualTo 1) ) then {
        private _charge = getNumber(_modeCfg >> 'artilleryCharge');
        private ["_maxR", "_maxRE", "_minR", "_minRE"];
        private _r1 = [_minElev, (_initSpeed * _charge), _altDiff] call _rangeFnc;
        if !( _r1 isEqualType 0 ) then { _r1 = 0; };
        if ( _r1 < 0 ) then { _r1 = 0; };
        private _r2 = [_maxElev, (_initSpeed * _charge), _altDiff] call _rangeFnc;
        if !( _r2 isEqualType 0 ) then { _r2 = 0; };
        if ( _r2 < 0 ) then { _r2 = 0; };
        if ( _r1 < _r2 ) then {
        	_maxR = _r2;
        	_maxRE = _maxElev;
        	_minR = _r1;
        	_minRE = _minElev;
        } else {
        	_maxR = _r1;
        	_maxRE = _minElev;
        	_minR = _r2;
        	_minRE = _maxElev;
        };
        if !( _maxR isEqualTo 0 ) then {
			private _inRange = true;
			if ( _distance >= 0 ) then {
				_inRange = ( (_maxR >= _distance) && (_minR <= _distance) );
			};
			#ifdef DEBUG
    		private _debug = format ['Mode %1, minRange: %2 (%3�), maxRange: %4 (%5�), charge: %6, inRange: %7', _mode, _minR, _minRE, _maxR, _maxRE, _charge, _inRange];
    		debug(LL_DEBUG, _debug);
    		#endif 		 	
			_results pushback [_mode, _charge, _minR, _maxR, _inRange];
		};
    };
} forEach _modes;

([_results, [], {_x select 1}, "ASCEND"] call BIS_fnc_sortBy)