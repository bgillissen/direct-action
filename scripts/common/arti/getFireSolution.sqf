
#include "_debug.hpp"

params ["_veh", "_turretPath", "_mag", "_charge", "_gridX", "_gridY", "_gridZ", "_corX", "_corY", ["_highAngle", true]];

private _trgt = ([_gridX, _gridY, _corX, _corY] call arti_fnc_gridToCoord);
_trgt params ["_trgtX", "_trgtY"];
_trgt set [2, _gridZ];

if !( isNil "artiComputer_trgtArrow" ) then { artiComputer_trgtArrow setPosASL _trgt; };

private _tube = (_veh call arti_fnc_getTubePosition);
_tube params ["_tubeX", "_tubeY", "_tubeZ", "_slope"];

([_tube, _trgt] call arti_fnc_getDistance) params ["_distance", "_trgtHeight", "_altDiff"];

private _bearing = ([_tube, _trgt] call arti_fnc_getBearing);

private _muzzleV = getNumber(configfile >> "CfgMagazines" >> _mag >> "initSpeed") * _charge;

#define G 9.81

if ( (_muzzleV^4 - G * (G * _distance^2 + 2 * _trgtHeight * _muzzleV^2)) < 0 ) exitWith {
    #ifdef DEBUG
    private _debug = format["target is not in range (charge:%3, dst:%1m, alt:%2m)", _distance, _trgtHeight, _charge];
    debug(LL_WARN, _debug);
    #endif
    []
};

private _angle = -1;
if ( _highAngle ) then {    
	_angle = atan((_muzzleV^2 + sqrt(_muzzleV^4 - G * (G * _distance^2 + 2 * _trgtHeight * _muzzleV^2))) / (G * _distance));
} else {
    _angle = atan((_muzzleV^2 - sqrt(_muzzleV^4 - G * (G * _distance^2 + 2 * _trgtHeight * _muzzleV^2))) / (G * _distance));
};
private _eta = _distance / (_muzzleV * cos(_angle));

([_slope, _angle, _bearing] call arti_fnc_getRelativeData) params ["_angleRel", "_bearingRel"];

_bearing = ([_bearing, 2] call common_fnc_roundTo);
_bearingRel = ([_bearingRel, 2] call common_fnc_roundTo);
_angle = ([_angle, 2] call common_fnc_roundTo);
_angleRel = ([_angleRel, 2] call common_fnc_roundTo);
_distance = ([_distance, 2] call common_fnc_roundTo);
_trgtHeight = ([_trgtHeight, 2] call common_fnc_roundTo);
_eta = ([_eta, 1] call common_fnc_roundTo);

#ifdef DEBUG
private _debug = format["fire solution: bearings [%1,%2], angles [%3,%4], charge: %5, mag:%6, eta: %7", _bearing, _bearingRel, _angle, _angleRel, _charge, _mag, _eta];
debug(LL_DEBUG, _debug);
#endif

[[_bearing, _bearingRel], [_angle, _angleRel], _charge, _mag, _distance, _trgtHeight, _eta]