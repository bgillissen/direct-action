
params [["_veh", objNull]];

if ( isNull _veh ) exitWith { [] };

if ( _veh isKindOf "Man" ) then { _veh = vehicle _veh; };

(getArtilleryAmmo [_veh])