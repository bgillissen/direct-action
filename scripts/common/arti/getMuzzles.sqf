
private _weapon = (_this call arti_fnc_getWeapon);

if ( _weapon isEqualTo "" ) exitWith { [] };

private _muzzles = getArray(configFile >> "cfgWeapons" >> _weapon >> "muzzles");

if ( "this" in _muzzles ) then {
	_muzzles = _muzzles - ["this"];
	_muzzles pushback _weapon;
};

if ( count _muzzles > 0 ) exitWith { _muzzles };

[_weapon]
