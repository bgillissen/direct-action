
params ["_tube", "_trgt"];

_tube params ["_tubeX", "_tubeY", "_tubeZ"];
_trgt params ["_trgtX", "_trgtY", "_trgtZ"];  

private _distance = sqrt((_tubeX - _trgtX)^2 + (_tubeY - _trgtY)^2);

private _trgtHeight = (_trgtZ - _tubeZ);
private _altDiff = (_tubeZ - _trgtZ);

[_distance, _trgtHeight, _altDiff]