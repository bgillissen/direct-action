/*
 * Credits  PabstMirror, from ACE3 mortar table
 */
#define MAX_ATTEMPTS 30
#define MAX_ERROR 0.6
#define ERROR 0.3

params ["_distance", "_altDiff", "_muzzleV", "_airFriction", "_minElev", "_maxElev", "_highAngle"];

if ( _highAngle ) then { 
	_minElev = _minElev + ((_maxElev - _minElev) / 2); 
} else {
    _maxElev = _maxElev - ((_maxElev - _minElev) / 2);
};

//systemChat format["highAngle: %1 min: %2, max: %3", _highAngle, _minElev, _maxElev];

private _elev = -1;

private _error = 10000;
private _result = [];
private _closestError = 10000;
private _closestResult = [];
private _c = 0;

while { (_c < MAX_ATTEMPTS) && (_error > ERROR) } do {    
    _c = _c + 1;
    _elev = (_maxElev + _minElev) / 2;
    _result = [_elev, _muzzleV, _airFriction, _altDiff] call arti_fnc_simulateShot;
    _error = _distance - (_result select 0);
    if ( _error > 0 ) then {
        //systemChat format["%5 - %1 SHORTBY %2 meters (%3 -- %4)", _elev, abs _error, _minElev, _maxElev, _c];
        if ( _highAngle ) then {
            _maxElev = _elev;
		} else {
            _minElev = _elev;
        };
    } else {
        //systemChat format["%5 - %1 LONGBY %2 meters (%3 -- %4)", _elev, abs _error, _minElev, _maxElev, _c];
        if ( _highAngle ) then {
			_minElev = _elev;        	
		} else {
            _maxElev = _elev;
        };
        
    };
    _error = abs _error;
    if ( _error < _closestError ) then {
        _closestError = _error;
        _closestResult = [_elev, _result select 1];
    };
	if ( _maxElev isEqualTo _minElev ) then { _c = MAX_ATTEMPTS; };  
};

if ( _closestError > MAX_ERROR ) exitWith { [] };

//systemChat format["FOUND: %1deg @ %2m", _elev, _closestError];

_closestResult