
(getPosASL _this) params ["_tubeX", "_tubeY", "_tubeZ"];
(vectorUp _this) params ["_vuX", "_vuY"];
private _slopeX = deg(_vuX);
private _slopeY = deg(_vuY);

if ( arti_xInverted ) then {
	_tubeX = getNumber(configfile >> "CfgWorlds" >> worldName >> "mapSize") - _tubeX;
};

if ( arti_yInverted ) then {
	_tubeY = getNumber(configfile >> "CfgWorlds" >> worldName >> "mapSize") - _tubeY;
};

[_tubeX + arti_xOffset, _tubeY + arti_yOffset, _tubeZ, [_slopeX, _slopeY]]