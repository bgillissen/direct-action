
params ["_gridX", "_gridY", ["_corX", 0], ["_corY", 0]];

if ( isNil "arti_mapData" ) then {
	private _gridOrigin = mapGridPosition [0,0];
	private _xOrigin = [_gridOrigin, 0, 2] call BIS_fnc_trimString;
	private _yOrigin = [_gridOrigin, 3, 5] call BIS_fnc_trimString;
	//is Y inverted ?
	private _gridStepY = mapGridPosition [0,100];
	private _yStep = [_gridStepY, 3, 5] call BIS_fnc_trimString;
	private _yInverted = ( (parseNumber _yOrigin) > (parseNumber _yStep) );
    //is X inverted ?
	private _gridStepX = mapGridPosition [100,0];
	private _xStep = [_gridStepX, 0, 2] call BIS_fnc_trimString;
	private _xInverted = ( (parseNumber _xOrigin) > (parseNumber _xStep) );
	//find map X origin
    private _j = 1;
	while {mapGridPosition [-_j,0] == _gridOrigin} do { _j = _j + 1; };
    if ( _xInverted ) then { _j = 0 - _j; };
	private _xOffset = (parseNumber _xOrigin) * 100 + _j;
    //find map Y origin
	private _i = 1; 
	while {mapGridPosition [0,-_i] == _gridOrigin} do { _i = _i + 1; };
	if ( _yInverted ) then { _i = 0 - _i; };
	private _yOffset = (parseNumber _yOrigin) * 100 + _i;

	arti_xOffset = _xOffset;
    arti_yOffset = _yOffset;
	arti_yInverted =  _yInverted;
	arti_XInverted = _xInverted;
    arti_mapData = true;    
};
/*
systemChat "---------------------------------";
systemChat format["Inverted: [%1, %2]", arti_xInverted, arti_yInverted];
systemChat format["Offsets: [%1, %2]", arti_xOffset, arti_yOffset];
systemChat format["Pre: [%1, %2]", _gridX, _gridY];
*/
_gridX = _gridX * 100;
_gridY = _gridY * 100;
//systemChat format["Post: [%1, %2]", _gridX, _gridY];

if ( arti_xInverted ) then {
	_gridX = (getNumber(configfile >> "CfgWorlds" >> worldName >> "mapSize") / 10) - _gridX;
};
if ( arti_yInverted ) then {
	_gridY = (getNumber(configfile >> "CfgWorlds" >> worldName >> "mapSize") / 10) - _gridY;
};

_gridX = _gridX + arti_xOffset + _corX;
_gridY = _gridY + arti_yOffset + _corY;

//systemChat format["Final: [%1, %2]", _gridX, _gridY];

[_gridX, _gridY]