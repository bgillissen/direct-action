
params ["_veh", "_turretPath", "_mag", "_charge", "_gridX", "_gridY", "_alt", "_corX", "_corY", ["_highAngle", true], ["_fuse", 0]];

private _trgt = ([_gridX, _gridY, _corX, _corY] call arti_fnc_gridToCoord);
_trgt params ["_trgtX", "_trgtY"];
_trgt set [2, _alt];

if !( isNil "artiComputer_trgtArrow" ) then { artiComputer_trgtArrow setPosASL _trgt; };

private _tube = (_veh call arti_fnc_getTubePosition);
_tube params ["_tubeX", "_tubeY", "_tubeZ", "_slope"];

([_tube, _trgt] call arti_fnc_getDistance) params ["_distance", "_trgtHeight"];

//systemChat format["Distance: %1, trgtHeight: %2", _distance, _trgtHeight];

private _bearing = ([_tube, _trgt] call arti_fnc_getBearing);

//systemChat format["Bearing: %1", _bearing];

([_veh, _turretPath, _slope, _bearing] call arti_fnc_getTurretLimits) params ["_minElev", "_maxElev"];

//systemChat format["MinElev: %1, MaxElev: %2", _minElev, _maxElev];

private _muzzleV = getNumber(configfile >> "CfgMagazines" >> _mag >> "initSpeed") * _charge;
private _ammo = getText(configfile >> "CfgMagazines" >> _mag >> "ammo");
private _airFriction = getNumber(configfile >> "CfgAmmo" >> _ammo >> "airFriction"); 
/*
systemChat format["Charge: %1", _charge];
systemChat format["Mag: %1", _mag];
systemChat format["Fuse altitude: %1", _fuse];
systemChat format["MuzzleV: %1", _muzzleV];
systemChat format["airFriction: %1", _airFriction];
*/
([_distance, _trgtHeight, _muzzleV, _airFriction, _minElev, _maxElev, _highAngle] call arti_fnc_findSolution) params ["_angle", "_eta"];

if ( isNil "_angle" ) exitWith { [] };

([_slope, _angle, _bearing] call arti_fnc_getRelativeData) params ["_angleRel", "_bearingRel"];

_bearing = ([_bearing, 2] call common_fnc_roundTo);
_bearingRel = ([_bearingRel, 2] call common_fnc_roundTo);
_angle = ([_angle, 2] call common_fnc_roundTo);
_angleRel = ([_angleRel, 2] call common_fnc_roundTo);
_distance = ([_distance, 2] call common_fnc_roundTo);
_trgtHeight = ([_trgtHeight, 2] call common_fnc_roundTo);
_eta = ([_eta, 1] call common_fnc_roundTo);

[[_bearing, _bearingRel], [_angle, _angleRel], _charge, _mag, _distance, _trgtHeight, _eta]