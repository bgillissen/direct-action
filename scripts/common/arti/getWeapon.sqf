params [["_unit", player]];

(assignedVehicleRole _unit) params[["_role", ""], ["_turretPath", []]];

if ( (_turretPath isEqualTo []) || (_role isEqualTo "") ) exitwith { "" };
  
private _weapon = "";
private _weapons = ((vehicle _unit) weaponsTurret _turretPath);
 
{
    if ( getText(configFile >> "cfgWeapons" >> _x >> "cursor") in ["mortar", "EmptyCursor"] ) exitWith {
    	_weapon = _x; 
	};
} forEach _weapons;

_weapon