
if !( isServer ) exitWith { remoteExec ["common_fnc_getTimestamp", 2]; };

private _stamp = ("real_date" callExtension "0");

if !( isNil "_stamp" ) then { serverStamp = call compile _stamp; };

if ( isNil "serverStamp" ) then { serverStamp = 0; };

publicVariable "serverStamp";

serverStamp