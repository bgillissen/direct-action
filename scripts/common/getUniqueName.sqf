
params ["_baseName", "_array", ["_code", {_this}], ["_idx", -1]];

private _c = 0;
private _curName = _baseName;

while { true } do {
    private _inUse = false;
    if ( _c > 0 ) then { _curName = format["%1 - %2", _baseName, _c]; };
	{
        private _name = (_x call _code);
        if ( _name isEqualTo _curName ) exitWith { 
        	_inUse = ( (_idx < 0) || !(_idx isEqualTo _forEachIndex) ); 
		}; 
    } forEach _array;
    if !( _inUse ) exitWith {};
    _c = _c + 1;    
};

_curName