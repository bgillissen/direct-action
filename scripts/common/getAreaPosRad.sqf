
params ["_area"];

private _pos = [0,0,0];
private _sizes = 0;
switch(typeName _area) do {
	case 'STRING' : { _pos = getMarkerPos _area;
    				  _sizes = markerSize _area; 
    				};
	case 'ARRAY' : { _pos = _area select 0; 
    				 _sizes = [_area select 1, _area select 2];
				   };
};

private _radius = 0;
if ( (_sizes select 0) > (_sizes select 1) ) then {
    _radius = _sizes select 0;
} else {
    _radius = _sizes select 1;    
};

[_pos, _radius]