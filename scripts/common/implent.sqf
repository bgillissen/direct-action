/*
@filename: common\implent.sqf
Author:
	Ben
Description:
	run on all context,
	it is used to append the things provided by a map/mod to the corresponding global var
*/

#include "_debug.hpp"

params [["_list", []], "_target", "_filter", "_fKey", "_sub"];

if ( typeName _list != "ARRAY" ) exitWith {
#ifdef DEBUG_COMMON
	private _debug = format["implent: target=%1, filter=%2, given list is not an array!", _target, _filter];
	debug(LL_ERR, _debug);
#endif	
};

if ( (count _list) == 0 ) exitWith {};

#ifdef DEBUG_COMMON
private "_debug";
if ( isNil "_sub" ) then {
	_debug = format["implent: target=%1, filter=%2, #items=%3, list : %4", _target, _filter, (count _list)];
} else {
	_debug = format["implent: target=%1 (%2), filter=%3, #items=%4", _target, _sub, _filter, (count _list)];
};
debug(LL_DEBUG, _debug);
#endif



{
	private "_toFilter";
	if ( isNil "_fKey" ) then {
		_toFilter = _x;
	} else {
		_toFilter = _x select _fKey;
	};
	if ( !([_toFilter, _filter] call common_fnc_isBlacklisted) ) then {
		private "_stack";
		if ( isNil "_sub" ) then {
			_stack = (missionNamespace getVariable _target);
		} else {
			_stack = ((missionNamespace getVariable _target) select _sub);
		};
		if !( isNil "_fKey" ) then {
			private _qty =  (_x select (_fKey + 1));
			if ( _qty isEqualType 0 ) then {
				private _idx = -1;
				{
					if ( (_x select _fKey) isEqualTo _toFilter ) exitWith { _idx = _forEachIndex; };
				} forEach _stack;
				if ( _idx >= 0 ) then {
					private "_newQty";
        			if ( _qty isEqualTo 0 ) then {
        				_newQty = 0;
					} else {
            			_newQty = (((_stack select _idx) select (_fKey + 1)) + _qty);
					};
            		if ( _newQty <= 0 ) then {
                		_stack deleteAt _idx;
            		} else {
            			(_stack select _idx) set [(_fKey +1), _newQty];
					};	
				} else {
					if ( _qty > 0 ) then { _stack pushback _x; };
				};
			} else {
				_stack pushback _x;
			};
		} else {
			_stack pushback _x;
		};
	};
	true
} count _list;

true