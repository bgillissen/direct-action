
params ["_value", ["_digit", 0]];

if ( _digit <= 0 ) exitWith { round _value };

( round(_value * 10^_digit) / 10^_digit )