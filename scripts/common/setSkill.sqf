
params ["_units", "_skill"];


private _path = ["ia", "skills", format["rank%1", _skill]];

if !( (typeName _units) isEqualTo "ARRAY" ) then { _units = [_units]; };

{
    private _cat = _x;
	private _base = ((_path + [_cat]) call core_fnc_getSetting);
	private _random = (["ia", "skills", "randomness", _cat] call core_fnc_getSetting);
    { _x setSkill [_cat, (_base + (random _random))]; } forEach _units;
} forEach ["aimingAccuracy",
           "aimingShake",
           "aimingSpeed",
           "spotDistance",
           "spotTime",
           "courage",
           "commanding",
           "general",
           "endurance",
           "reloadSpeed"];