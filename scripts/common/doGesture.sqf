
#include "_debug.hpp"

params ["_unit", "_anim", ["_priority", 0]];

if !( local _unit ) exitWith {
    #ifdef DEBUG
    debug(LL_DEBUG, "doGesture: unit is not local");
    #endif
    _this remoteExec ["common_fnc_doGesture", _unit, false];
};

if ( _priority > 0 ) exitWith {
    #ifdef DEBUG
	private _debug = format["doGesture : playActionNow %1", _anim];
	debug(LL_DEBUG, _debug);
	#endif
	_unit playActionNow _anim;
};

#ifdef DEBUG
private _debug = format["doGesture : playAction %1", _anim];
debug(LL_DEBUG, _debug);
#endif
_unit playAction _anim;

