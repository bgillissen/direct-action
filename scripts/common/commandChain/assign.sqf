/*
@filename: feats\commandChain\assign.sqf
Author:
	Ben
Description:
	run on player,
*/

#include "_debug.hpp"

params ["_source", "_target", "_slot", ["_swapLeader", false], ["_sendMsg", true]];

if !( isPlayer _target ) exitWith {};

private _uid = getPlayerUID _target;
private _grp = group _target;
private _chain = _grp getVariable ["commandChain", []];
private _toPushback = [];
{
	_x params ["_cuid", "_isLeading"];
    if ( _forEachIndex >= _slot ) then { 
    	if !( _cuid isEqualTo _uid ) then { _toPushback pushback _x; }; 
		_chain deleteAt _forEachIndex;
	} else {
        if ( _cuid isEqualTo _uid ) then { _slot = _slot - 1; };
    }
} forEach _chain;

_chain set [_slot, [_uid, false]];
if ( (count _toPushback) > 0 ) then { _chain append _toPushback; };

private _leader = (leader _grp);
private _leaderUid = (getPlayerUid _leader);
private _leaderIdx = -1;
{
	_x params ["_cuid", "_isLeading"];
    if (_cuid isEqualTo _leaderUid) then { 
		_x set[1, true];
    	_leaderIdx = _forEachIndex;
	} else {
		_x set[1, false];
	};
} forEach _chain;

_grp setVariable ["commandChain", _chain, true];

if ( _sendMsg ) then {
	if ( _slot > 0 ) then {
		[_grp, "assign", [_target, _slot, _source]] call commandChain_fnc_message;
	} else {
		[_grp, "actual", [_target]] call commandChain_fnc_message;
	};
};

if !( _swapLeader ) exitWith {};

if ( _leaderIdx < 0 ) then {
    _leaderUid = ((_chain select 0) select 0);
    _leader = (_leaderUid call common_fnc_playerByUid);
	[0, _leader] call commandChain_fnc_swapLeader;    
};