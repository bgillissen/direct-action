/*
@filename: feats\commandChain\swapLeader.sqf
Author:
	Ben
Description:
	run on player,
*/

#include "_debug.hpp"

params ["_way", ["_target", objNull]];

if ( isNull _target ) then { _target = player; };

private _grp = group _target;
private _targetUid = getPlayerUID _target;
private _leader = leader _grp;
private _leaderUid = getPlayerUID _leader;

[_grp, true] call commandChain_fnc_cleanup;

private _chain = _grp getVariable ["commandChain", []];
private _broadcast = false;

private _leaderIdx = -1;
private _targetIdx = -1;
{
    _x params ["_cuid", "_isLeading"];
    if ( _cuid isEqualTo _leaderUid ) then { _leaderIdx = _forEachIndex; };
    if ( _cuid isEqualTo _targetUid ) then { _targetIdx = _forEachIndex; };
} forEach _chain;

private _newUid = "";
private _newIdx = -1;
private _newLeader = objNull;
private _msg = "";

if ( _way > 0 ) then {
	if ( _target isEqualTo _leader ) then {
		{
	    	_x params ["_cuid", "_isLeading"];
            private _unit = _cuid call common_fnc_playerByUid;
	    	private _isUp = ( !(_unit getVariable ['agony', false]) && !(_unit getVariable ["ACE_isUnconscious", false]) ); 
	    	if ( (_forEachIndex > _leaderIdx) && _isUp ) exitWith {
                _newIdx = _forEachIndex; 
	        	_newUid = _cuid;
                _newLeader = _unit;
	            _msg = "give";
                _broadcast = true;  
	        };
		} forEach _chain;
	}; 
};

if ( _way < 0 ) then {
	if ( _targetIdx >= 0 ) then {
	   {
	    	_x params ["_cuid", "_isLeading"];
            private _unit = _cuid call common_fnc_playerByUid;
            private _isUp = ( !(_unit getVariable ['agony', false]) && !(_unit getVariable ["ACE_isUnconscious", false]) );
		    if ( (_forEachIndex < _leaderIdx) && _isUp ) exitWith {
                _newIdx = _forEachIndex; 
            	_newUid = _cuid;
                _newLeader = _unit;
                _msg = "give";
                _broadcast = true;  
            };
		} forEach _chain;
	};
};

if ( _way isEqualTo 0 ) then {
    if ( _targetIdx >= 0 ) then {
	    {
	    	_x params ["_cuid", "_isLeading"];
	        if ( _cuid isEqualTo _targetUid ) exitWith {
                _newIdx = _forEachIndex;
	        	_newUid = _playerUid;
                _newLeader = player;
	            _msg = "take";
                _broadcast = true;    
	        };    
	    } forEach _chain;
	};
};

if ( _newIdx >= 0 ) then {
    [_grp, _msg, [_newLeader, _newIdx]] call commandChain_fnc_message;
    [_grp, _newLeader] call groups_fnc_setLeader;
	{ _x set [1, (_forEachIndex isEqualTo _newIdx )]; } forEach _chain;
};

if ( _broadcast ) then { _grp setVariable ["commandChain", _chain, true]; };

nil