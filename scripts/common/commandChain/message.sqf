
params ["_grp", "_msg", "_msgArg"];

private _msg = getText(missionConfigFile >> "settings" >> "groups" >> "messages" >> _msg);

{
    switch ( _forEachIndex ) do {
        case 0;
        case 2 : { _msgArg set [_forEachIndex, (_x getVariable["MD_name", (name _x)])]; };
        case 1 : { _msgArg set [_forEachIndex, getText(missionConfigFile >> "settings" >> "groups" >> "slots" >> format['cc_%1', _x])]; };
    };
} forEach _msgArg;

private _formatArg = [_msg] + _msgArg;

_msg = format _formatArg; 

[(units _grp), _msg] call common_fnc_systemChat;

