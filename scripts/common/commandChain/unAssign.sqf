/*
@filename: feats\commandChain\unAssign.sqf
Author:
	Ben
Description:
	run on player,
*/

#include "_debug.hpp"

params ["_target", ["_grp", grpNull], ["_swapLeader", true], ["_sendMsg", true]];

private _uid = "";
if ( _target isEqualType objNull ) then {
    if ( isPlayer _target ) then { _uid = getPlayerUid _target; };
} else {
    if ( _target isEqualType "" ) then {
    	_uid = _target;
        {
        	if ( (getPlayerUid _x) isEqualTo _uid ) exitWith { _target = _x; };
    	} forEach allPlayers; 
	};
};

if ( _uid isEqualTo "" ) exitWith {
    #ifdef DEBUG
    debug(LL_WARN, "unAssign failed, uid is empty!");
    #endif
};

if ( isNull _grp ) then {
    {
        private _g = _x;
        {
            _x params ["_cuid", "_isLeading"];
   			if ( _cuid isEqualTo _uid ) exitWith { _grp = _g; };
        } forEach (_g getVariable ["commandChain", []]);
    } forEach SQUADS;
};

if ( isNull _grp ) exitWith {
	#ifdef DEBUG
    private _debug = format["unAssign failed, group for uid '%1' was not found, cleaning", _uid];
    debug(LL_WARN, _debug);
    #endif
	[grpNull, true] call commandChain_fnc_cleanup;
};

private _chain = _grp getVariable ["commandChain", []];

if ( ({( (_x select 0) isEqualTo _uid )} count _chain) isEqualTo 0 ) exitWith {
    #ifdef DEBUG
    private _debug = format["unAssign failed, uid '%1' not found in group '%2', cleaning", _uid, _grp];
    debug(LL_WARN, _debug);
    #endif
};

private _broadcast = false;
private _wasLeading = false;
private _wasFirst = false;
{
	_x params ["_cuid", "_isLeading"];
   	if ( _cuid isEqualTo _uid ) then {
        #ifdef DEBUG
    	private _debug = format['unAssign target found in group chain @ %1 = %2', _forEachIndex, _target];
        debug(LL_DEBUG, _debug);
    	#endif
        _broadcast = true; 
        _wasLeading = _isLeading;
        _wasFirst = (_forEachIndex isEqualTo 0);
        _chain deleteAt _forEachIndex;
	};
} forEach _chain;

if ( !_wasLeading && !_wasFirst ) exitWith {
    #ifdef DEBUG
    debug(LL_DEBUG, "unAssign, not leading, not first IC");
    #endif
	if ( _broadcast ) then { _grp setVariable ["commandChain", _chain, true]; };    
};

private _newLeader = objNull;
private _newActual = objNull;
private _newLeaderIdx = -1;
private _newActualIdx = -1;

{ //search new leader / actual in commandChain
	_x params ["_cuid", "_isLeading"];
	private _unit = _cuid call common_fnc_playerByUid;
	private _ok = !(isNull _unit);
	if ( _ok && (isNull _newActual) ) then {
		_newActual = _unit;
		_newActualIdx = _forEachIndex;
	};             
	if ( _ok ) then { _ok = (alive _unit); };
	if ( _ok ) then { _ok = ( !(_unit getVariable ['agony', false]) && !(_unit getVariable ["ACE_isUnconscious", false]) ); };
    if ( _ok && (isNull _newLeader) ) then {
    	_newLeader = _unit;
        _newLeaderIdx = _forEachIndex;  	        
	};
} forEach _chain;

if ( isNull _newLeader ) then {
    //search for the most ranked player in the group
    private _rank = -1;
    {
        if !( _x isEqualTo _target ) then {
	        private _ok = !(isNull _x);
	        if ( _ok && (isNull _newActual) ) then {
	            if ( (_x getVariable ['MD_rank', 0]) > _rank ) then { _newActual = _x; };
			};             
	    	if ( _ok ) then { _ok = (alive _x); };
			if ( _ok ) then { _ok = ( !(_x getVariable ['agony', false]) && !(_x getVariable ["ACE_isUnconscious", false]) ); };
	        if ( _ok ) then { _ok = ( (_x getVariable['MD_rank', 0]) > _rank ); };
	        if ( _ok ) then { _newLeader = _x; };
        };
    } forEach (units _grp);
};

if !( isNull _newActual ) then {
    #ifdef DEBUG
    private _debug = format['unAssign newActual found @ %1 = %2', _newActualIdx, _newActual];
    debug(LL_DEBUG, _debug);
    #endif
    if ( _newActualIdx >= 0 ) then {
        _broadcast = true;
        private _newActualUid = (getPlayerUid _newActual);
        private _pre = [[_newActualUid, (_newLeader isEqualTo _newActual)]];
    	_chain deleteAt _newActualIdx;
        _chain = _pre + _chain;
        if ( _sendMsg ) then { [_grp, 'actual', [_newActual]] call commandChain_fnc_message; };
	} else {
        _broadcast = false;
        [objNull, _newActual, 0, false, true] call commandChain_fnc_assign;    
	};    
};

if !( isNull _newLeader ) then {
    #ifdef DEBUG
    private _debug = format['unAssign newLeader found in chain @ %1 = %2', _newLeaderIdx, _newLeader];
    debug(LL_DEBUG, _debug);
    #endif
    if ( _newLeaderIdx >= 0 ) then {
        _broadcast = true;
        { _x set [1, (_forEachIndex isEqualTo _newLeaderIdx)]; } forEach _chain;
        if ( !(_newLeader isEqualTo _newActual) && _sendMsg ) then {
			[_grp, 'give', [_newLeader, _newLeaderIdx]] call commandChain_fnc_message;
        };
	};
	if ( !((leader _grp) isEqualTo _newLeader) && _swapLeader ) then { [_grp, _newLeader] call groups_fnc_setLeader; };
};

if ( _broadcast ) then { _grp setVariable ["commandChain", _chain, true]; };
  