class commandChain {
	tag = "commandChain";
	class functions {
		class assign { file="common\commandChain\assign.sqf"; };
		class cleanup { file="common\commandChain\cleanup.sqf"; };
		class message { file="common\commandChain\message.sqf"; };
		class swapLeader { file="common\commandChain\swapLeader.sqf"; };
		class unAssign { file="common\commandChain\unAssign.sqf"; };
	};
};
