
params [["_grp", grpNull], ["_broadcast", false]];

if ( isNull _grp ) exitWith {
	{
        if !( isNull _x ) then { [_x, _broadcast] call commandChain_fnc_cleanup; }; 
	} forEach SQUADS;       
};

private _chain = _grp getVariable ["commandChain", []];

if ( (count _chain) isEqualTo 0 ) exitWith {};

private _changed = false;
{
	_x params ["_cuid", "_isLeading"];
	private _unit = _cuid call common_fnc_playerByUid;
    if ( (isNull _unit) || !((group _unit) isEqualTo _grp) ) then {
        _changed = true; 
        _chain deleteAt _forEachIndex;
    };
} forEach _chain;

if ( _changed ) then {
	_grp setVariable ["commandChain", _chain, _broadcast];
};      