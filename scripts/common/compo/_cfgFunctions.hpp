class compo {
	tag = "compo";
	class functions {
		class create { file="common\compo\create.sqf"; };
		class spawn { file="common\compo\spawn.sqf"; };
		class remove { file="common\compo\remove.sqf"; };
	};
};
