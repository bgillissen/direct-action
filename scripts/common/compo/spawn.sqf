
#include "_debug.hpp"

params ["_compName"];

private _spawned = false;

if ( isNil "compoSpawned" ) then {
	compoSpawned = [];
} else {
	{
		_x params ["_ref", "_stack"];
    	if ( _ref isEqualTo _compName ) exitWith { _spawned = true; };
	} forEach compoSpawned;
};

if ( _spawned ) exitWith {
    #ifdef DEBUG
   	private _debug = format["Skipping composition '%1', already spawned", _compName];
    debug(LL_DEBUG, _debug);
    #endif 
	true 
};

if !( isClass (configFile >> "DAcompo" >> _compName) ) exitWith {
	#ifdef DEBUG
   	private _debug = format["Failed to spawn composition '%1', config not found", _compName];
    debug(LL_ERR, _debug);
    #endif 
	false
};

private _stack = (_this call compo_fnc_create);
{
	if ( isNil "_x" ) then { _stack set [_forEachIndex, objNull]; };
} forEach _stack;

_stack = _stack - [objNull];

#ifdef DEBUG
private _debug = format["Composition '%1' created %2 entities", _compName, count _stack];
debug(LL_INFO, _debug);
#endif 

compoSpawned pushback [_compName, _stack];

true