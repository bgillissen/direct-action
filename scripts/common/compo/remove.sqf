
params["_compName"];

if ( isNil "compoSpawned" ) exitWith { false };

private _index = -1;
{
	_x params ["_ref"];
   	if ( _ref isEqualTo _compName ) exitWith { _index = _forEachIndex; };
} forEach compoSpawned;

if ( _index < 0 ) exitWith {
    systemChat format["%1 is not spawned", _compName]; 
	false 
};

(compoSpawned select _index) params ["_ref", "_stack"];

private _deferedGroups = [];

{
	private _obj = _x;
	switch ( typeName _obj ) do {
		//OBJECT, TRIGGER, LOGIC
		case ( "OBJECT" ) : {
			private _group = group _obj;
			deleteVehicle _obj;
			if !( isNull _group ) then {
				if ( count units _group isEqualTo 0 ) then {
					deleteGroup _group;
				} else {
					_deferedGroups pushBack _group;
				};
			};
		};
		//GROUP
		case ( "GROUP" ) : {
			if ( count units _obj isEqualTo 0 ) then {
				deleteGroup _obj;
			}else{
				_deferedGroups pushBack _obj;
			};
		};
		//MARKER
		case ( "STRING" ) : {
			if !( getMarkerPos _obj isEqualTo [0,0,0] ) then {
				deleteMarker _obj;
			};
		};
	};
} forEach _stack;

{
	if !( isNull _x ) then {
		if ( (count (units _x)) isEqualTo 0 ) then { deleteGroup _x; };
	};
} forEach _deferedGroups;

compoSpawned deleteAt _index;

true