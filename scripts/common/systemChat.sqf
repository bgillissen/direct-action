
params ["_target", "_msg"];

if !( (typeName _target) isEqualTo "ARRAY" ) then { _target = [_target]; }; 

private _do = false;

{
	if !( local _x ) then { 
		[_x, _msg] remoteExec ["common_fnc_systemChat", _x, false];
	} else {
		_do = true;
	};
} forEach _target;

if ( _do ) then { systemChat _msg; };