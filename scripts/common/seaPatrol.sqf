/*
	File: taskPatrol.sqf
	Author: Joris-Jan van 't Land

	Description:
	Create a random patrol of several waypoints around a given position.

	Parameter(s):
	_this select 0: the group to which to assign the waypoints (Group)
	_this select 1: the position on which to base the patrol (Array)
	_this select 2: the maximum distance between waypoints (Number)
	_this select 3: (optional) blacklist of areas (Array)

	Returns:
	Boolean - success flag
*/

//Validate parameter count

params ["_grp", "_pos", "_maxDist", ["_blacklist", []]];

//Validate parameters
if !( _grp isEqualType grpNull ) exitWith {debugLog "Log: [taskPatrol] Group (0) must be a Group!"; false};
if !( _pos isEqualType [] ) exitWith {debugLog "Log: [taskPatrol] Position (1) must be an Array!"; false};
if !( _maxDist isEqualType 0 ) exitWith {debugLog "Log: [taskPatrol] Maximum distance (2) must be a Number!"; false};
if !( _blacklist isEqualType [] ) exitWith {debugLog "Log: [taskPatrol] Blacklist (3) must be an Array!"; false};

//Create a string of randomly placed waypoints.
private _prevPos = _pos;

for "_i" from 0 to (2 + (floor (random 3))) do
{
	private ["_wp", "_newPos"];
	_newPos = [_prevPos, 50, _maxDist, 1, 2, 60 * (pi / 180), 0, _blacklist] call BIS_fnc_findSafePos;
	_prevPos = _newPos;

	_wp = _grp addWaypoint [_newPos, 0];
	_wp setWaypointType "MOVE";
	_wp setWaypointCompletionRadius 20;
};

//Cycle back to the first position.
private _wp = _grp addWaypoint [_pos, 0];
_wp setWaypointType "CYCLE";
_wp setWaypointCompletionRadius 20;

true