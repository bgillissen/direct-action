/*
@filename: common\attachTo.sqf
Author:
	Ben
Description:
	run on player, server
	attach obj 1 to obj 2, but run where obj1 is local
*/

#include "_debug.hpp"

params ["_obj1", "_obj2", ["_offset", [0,0,0]], "_mempoint"];

if ( isNull _obj1 ) exitWith {};

if !( local _obj1 ) exitWith {
    _this remoteExec ["common_fnc_attachTo", (owner _obj1), false];
};

if ( isNil "_mempoint" ) then {
    _obj1 attachTo [_obj2, _offset];
} else {
    _obj1 attachTo [_obj2, _offset, _mempoint];
};