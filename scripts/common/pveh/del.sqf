
#include "_debug.hpp" 

params ["_var", ["_stackID", -1], ["_target", objNull]];

if ( isNil "PVEH" ) exitWith { false };

private _entry = -1;
private _real = -1;
private _stack = [];
{
    _x params ["_sTarget", "_sVar", "_sStack"];
	if ( (_sTarget isEqualTo _target) && (_sVar isEqualTo _var) ) exitWith { 
		_entry = _forEachIndex;
		_stack = _sStack; 
	};
} forEach PVEH;

if ( _entry < 0 ) exitWith { false };

if ( _stackID < 0 ) then {
    _stack = [];
    PVEH deleteAt _entry;
} else {
    if ( (_stackID + 1) isEqualTo (count _stack) ) then {
        _stack deleteAt _stackID;
    } else {
        _stack set [_stackID, {}];
	};    
};

if ( count _stack isEqualTo 0 ) then {
	PVEH deleteAt _entry;
} else {
   	PVEH set [_entry, [_target, _var, _stack]];
};
    
true