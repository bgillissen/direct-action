
#include "_debug.hpp"
 
params ["_var", "_code", ["_target", objNull]];

#ifdef DEBUG
private "_debug";
if ( isNull _target ) then {
	_debug = format["PVEH: adding entry to %1", _var, _code];
} else {
    _debug = format["PVEH: adding entry to %1 :: %2", _target, _var, _code];
};
debug(LL_DEBUG, _debug);
#endif

if ( isNil "PVEH" ) then { PVEH = []; };

private _entry = -1;
private _stack = [];

{
    _x params ["_sTarget", "_sVar", "_sStack"];
	if ( (_sTarget isEqualTo _target) && (_sVar isEqualTo _var) ) exitWith {     
		_entry = _forEachIndex;
        _stack = _sStack; 
	};
} forEach PVEH;

private "_stackID";

if ( _entry < 0 ) then {
   _stackID = 0;
    PVEH pushback [_target, _var, [_code]];
    if ( isNull _target ) then {
    	_var addPublicVariableEventHandler { _this call pveh_fnc_run; };
	} else {
        _var addPublicVariableEventHandler [_target, { _this call pveh_fnc_run; }];
	};
} else {
	_stackID = (_stack pushback _code);
	PVEH set [_entry, [_target, _var, _stack]];
};

_stackID