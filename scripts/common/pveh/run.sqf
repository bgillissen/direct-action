#include "_debug.hpp" 

params ["_var", "_val", ["_target", objNull]];

if ( (typeName _target) isEqualTo "NAMESPACE") then { _target = objNull; };

_var = (toLower _var);

#ifdef DEBUG
private "_debug";
if ( isNull _target ) then {
	_debug = format["running %1 = %2", _var, _val];
} else {
	_debug = format["running %1 :: %2 = %3", _target, _var, _val];
};
debug(LL_DEBUG, _debug);
#endif

private _stack = [];
private _found = false;
{
    _x params ["_sTarget", "_sVar", "_sStack"];
    if ( isNull _target ) then {
        _found =  ( (isNull _sTarget) && ((toLower _sVar) isEqualTo _var) );
	} else {
    	_found = ( (_sTarget isEqualTo _target) && ((toLower _sVar) isEqualTo _var) );
	};
    if ( _found ) exitWith { _stack = _sStack; };
} forEach PVEH;

if !( _found ) exitWith {};

{
    #ifdef DEBUG
    private "_debug";
    if ( isNull _target ) then {
		_debug = format["calling stackID %1 for %2", _forEachIndex, _var];
	} else{
		_debug = format["calling stackID %1 for %2 :: %3", _forEachIndex, _target, _var];            
	};
	debug(LL_DEBUG, _debug);
	#endif 
	[_var, _val, _target] call _x; 
} forEach _stack;

true