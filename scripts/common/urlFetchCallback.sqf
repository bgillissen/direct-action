/*
@filename: common\urlFetchCallback.sqf
Author:
	Ben
Description:
	run server side (no extension on players).
	fetch the given url, and call the given code on success or error 
*/

#include "_debug.hpp"

params ["_url", "_cbOK", ["_cbERROR", ""]];

private _limit = time + 0.5;
private _ok = false;
waitUntil {
	 sleep 0.01;
	 _ok = ( ("url_fetch" callExtension format["%1",_url]) isEqualTo "OK" ); 
	 ( _ok || (time > _limit) )
};

if !( _ok ) exitWith {
    #ifdef DEBUG
	debug(LL_WARN, "urlFetch: extension is not loaded");
    #endif
    ["Extension is not loaded", _url] call compile _cbERROR;
};

private _result = "";

waitUntil {
	sleep 0.01;
	_result = "url_fetch" callExtension "OK";
    (_result != "WAIT")
};

if (_result == "ERROR") exitWith {
    private _err = ("url_fetch" callExtension "ERROR");
	#ifdef DEBUG
    private _debug = format ["urlFetch: error fetching %1, message was: %2", _url, _err];
	debug(LL_ERR, _debug);
	#endif	
	[_err, _url] call compile _cbERROR;
};

[_result] call compile _cbOK;