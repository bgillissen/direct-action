/*
@filename: common\deleteObjects.sqf
Author:
	Ben
Description:
	it remove the objects given 
*/

params ["_things"];
 
if !( _things isEqualType [] ) then { _things = [_things]; };

{
	if ( _x isEqualType grpNull ) then {
		{
			if !( isNull (objectParent _x) ) then {
				deleteVehicle (vehicle _x);
			};
        	deleteVehicle _x;
		} forEach (units _x);
	} else {
		if !( isNull (objectParent _x) ) then {
			deleteVehicle (vehicle _x);
			if !( _x isKindOf "Man" ) then {
				{ deleteVehicle _x; } count (crew _x);
			};
		};
		deleteVehicle _x;
	};
} forEach _things;