
params ["_unit", "_anim", ["_priority", 0]];

if (_unit isEqualTo (vehicle _unit) ) then {
	if !( local _unit ) exitWith {
        _this remoteExec ["common_fnc_doAnim", (owner _unit), false];
	};
    _this call global_fnc_doAnimPlayer;
} else {
	_this call global_fnc_doAnim;
};