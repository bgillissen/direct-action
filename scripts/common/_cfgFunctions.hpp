class common {
	tag = "common";
	class functions {
		class addToBlacklists { file="common\addToBlacklists.sqf"; };
		class attachTo { file="common\attachTo.sqf"; };
		class deleteObjects { file="common\deleteObjects.sqf"; };
		class deleteRuins { file="common\deleteRuins.sqf"; };
		class doAnim { file="common\doAnim.sqf"; };
		class doGesture { file="common\doGesture.sqf"; };
		class getAreaPosRad { file="common\getAreaPosRad.sqf"; };
		class getDate { file="common\getDate.sqf"; };
		class getFriends { file="common\getFriends.sqf"; };
		class getMarkerColor { file="common\getMarkerColor.sqf"; };
		class getTimestamp { file="common\getTimestamp.sqf"; };
		class getTurretConfig { file="common\getTurretConfig.sqf"; };
		class getUniqueName { file="common\getUniqueName.sqf"; };
		class implent { file="common\implent.sqf"; };
		class isBlacklisted { file="common\isBlacklisted.sqf"; };
		class isCopilot { file="common\isCopilot.sqf"; };
		class numberToSide { file="common\numberToSide.sqf"; };
		class removeWP { file="common\removeWP.sqf"; };
		class setCargo { file="common\setCargo.sqf"; };
		class setDir { file="common\setDir.sqf"; };
		class setLoadout { file="common\setLoadout.sqf"; };
		class setSkill { file="common\setSkill.sqf"; };
		class smartSleep { file="common\smartSleep.sqf"; };
		class systemChat { file="common\systemChat.sqf"; };
		class urlFetchCallback { file="common\urlFetchCallback.sqf"; };
		class urlFetchReturn { file="common\urlFetchReturn.sqf"; };
		class playerByUid { file="common\playerByUid.sqf"; };
		class playSound { file="common\playSound.sqf"; };
		class airPatrol { file="common\airPatrol.sqf"; };
		class seaPatrol { file="common\seaPatrol.sqf"; };
		class stopSound { file="common\stopSound.sqf"; };
		class roundTo { file="common\roundTo.sqf"; };
	};
};

#include "arti\_cfgFunctions.hpp"
#include "commandChain\_cfgFunctions.hpp"
#include "global\_cfgFunctions.hpp"
#include "compo\_cfgFunctions.hpp"
#include "pveh\_cfgFunctions.hpp"
#include "keybind\_cfgFunctions.hpp"
