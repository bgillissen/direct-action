#include "..\core\debugLevels.hpp"

//comment the following line to disable the common functions debug
#define DEBUG_COMMON

//default common functions debug level
#ifndef DEBUG_LVL
#define DEBUG_LVL LL_INFO
#endif

#ifndef DEBUG_COMMON
#undef DEBUG
#endif

#include "..\core\__debug.hpp"
