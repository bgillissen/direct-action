
#include "_debug.hpp"

params ['_isDown', '_eventInfo'];

private _key = ([2, 1] select _isDown);
private _match = false;
{
    _x params ["_id", "_down", "_up", '_bind', "_repeat", "_delay"];
    private _var = format["KBState_%1", _id];
    private _state = missionNamespace getVariable [_var, [false, 0, 0]];
    _match = [_eventInfo, _bind] call keybind_fnc_check;
    if ( _match ) exitWith {
        private _ok = !( (_x select _key) isEqualTo {} );
        if ( _ok ) then {
            
	    	if ( _repeat ) then {
	        	if ( (_state select 0) isEqualTo _isDown ) then {
					_ok = ( ((_state select 0) isEqualTo _isDown)  && ((time - _delay) > (_state select _key)) );
				};
			} else {
				_ok = !( (_state select 0) isEqualTo _isDown );
	    	};
		    if ( _ok ) then {
                #ifdef DEBUG
				private _debug = format["keyEvent handled by %1", _id];
	    		debug(LL_DEBUG, _debug);
        		#endif	         
		        _state set [_key, time];
	    		_eventInfo call (_x select _key);
			};
		};
        _state set [0, _isDown];
    	missionNamespace setVariable [_var, _state];
	};
} forEach KB_stack;

_match