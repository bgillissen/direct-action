
params ['_id', '_dsp', ['_down', {}], ['_up', {}], ['_dft', []], ['_repeat', false], ['_delay', 0]];

if ( (typeName _down) isEqualTo "STRING" ) then { _down = compile format["_this call %1;", _down]; };
if ( (typeName _up) isEqualTo "STRING" ) then { _up = compile format["_this call %1;", _up]; }; 
 
if ( _dft isEqualTo [] ) then {
	_dft = [_id, "keycode"] call keybind_fnc_get;
} else {
    _dft = _dft call keybind_fnc_get;
};
if ( MOD_cba3 ) then {
	["Direct Action", _id, _dsp, _down, _up, _dft, _repeat, _delay] call CBA_fnc_addKeybind;
} else {
	KB_stack pushback [_id, _down, _up, _dft, _repeat, _delay];    
};