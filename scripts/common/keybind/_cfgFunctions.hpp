class keybind {
	tag = "keybind";
	class functions {
		class add { file="common\keybind\add.sqf"; };
		class check { file="common\keybind\check.sqf"; };
		class get { file="common\keybind\get.sqf"; };
		class event { file="common\keybind\event.sqf"; };
	};
};
