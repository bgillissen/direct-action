
params ['_eventInfo', '_bind'];

_eventInfo  params ["_control", "_keyCode", "_shift", "_ctrl", "_alt"];

private _ok = false;

if ( _shift isEqualTo (_bind select 1) ) then {
    if ( _ctrl isEqualTo (_bind select 2) ) then {
        if ( _alt isEqualTo (_bind select 3) ) then {
			if ( _keyCode isEqualTo (_bind select 0) ) then { _ok = true; };                    
        };
	};
};

_ok