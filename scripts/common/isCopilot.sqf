
params ["_veh", "_player"];

private _tp = [];
{
    _x params ["_unit", "_role", "_index", "_turretPath", "_personTurret"];
    if ( player isEqualTo _unit ) exitWith { _tp = _turretPath; };
} forEach (fullCrew _veh);

private _cfg = ([(typeOf _veh), _tp] call common_fnc_getTurretConfig);

if ( isNull _cfg ) exitWith { false };

if ( getNumber(_cfg >> "iscopilot") isEqualTo 1 ) exitWith { true };

false