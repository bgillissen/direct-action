
if !( CTXT_SERVER ) exitWith {
	_this remoteExec ["global_fnc_hideObject", 2, false];    
};

params ["_obj", "_hidden"];

_obj hideObjectGlobal _hidden;