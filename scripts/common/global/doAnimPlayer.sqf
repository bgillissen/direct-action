
#include "_debug.hpp"

params ["_unit", "_anim", ["_priority", 0]];

#ifdef DEBUG
private _debug = format["doAnimPlayer : unit: %1 --- anim: %2 --- priority: %3", _unit, _anim, _priority];
debug(LL_DEBUG, _debug);
#endif

switch (_priority) do {
	case 0: { _unit playMove _anim; };
    case 1: { _unit playMoveNow _anim; };
    case 2: { _unit switchMove _anim; };
    default {};
};