#include "..\..\core\debugLevels.hpp"

//comment the following line to disable global functions debug

#define DEBUG()

//global functions debug level
#define DEBUG_LVL LL_INFO
//debug context
#define DEBUG_CTXT "global"

#include "..\__debug.hpp"
