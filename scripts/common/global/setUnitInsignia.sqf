
#include "_debug.hpp"

if !( CTXT_SERVER ) exitWith {
	_this remoteExec ["global_fnc_setUnitInsignia", 2, false];
};



params ["_unit", ["_insignia", ""]];

private _uniform = getText (configfile >> "CfgWeapons" >> uniform _unit >> "ItemInfo" >> "uniformClass");
private _index = -1;

{
	if ( _x isEqualTo "insignia" ) exitWith { _index = _foreachindex; };
} foreach getArray (configfile >> "CfgVehicles" >> _uniform >> "hiddenSelections");

if ( _index < 0 ) exitWith { 
	#ifdef DEBUG
	private _debug = format["setUnitInsignia: insignia index not found in unit uniform (%1)", _uniform];
    debug(LL_ERR, _debug);
    #endif
};

private _texture = "";
if !( _insignia isEqualTo "" ) then {
    private _cfg = (configfile >> "CfgUnitInsignia" >> _insignia);
    if !( isClass _cfg ) exitWith {
        #ifdef DEBUG
		private _debug = format["setUnitInsignia: insignia '%1' class was not found", _insignia];
    	debug(LL_ERR, _debug);
    	#endif
    };
	_texture = getText (_cfg >> 'texture');
};

_unit setObjectTextureGlobal [_index, _texture];

[_unit, _index, _texture] remoteExec ["global_fnc_setUnitInsigniaPlayer", 0, false];