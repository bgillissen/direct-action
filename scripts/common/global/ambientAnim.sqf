
#include "_debug.hpp"

if !( CTXT_SERVER ) exitWith {
    #ifdef DEBUG
	debug(LL_DEBUG, "ambientAnim : asking server to propagate");
	#endif
	_this remoteExec ["global_fnc_ambientAnim", 2, false];
};

if ( isNil "ambientAnimJIP" ) then { ambientAnimJIP = []; };

params ["_unit", "_anim"];

if ( _anim isEqualTo "" ) exitWith {
	{
        _x params ["_j", "_u", ["_uid", ""]];
        private ["_remove", "_exec"];
        if ( (typeName _unit) isEqualTo "STRING" ) then {
            _remove = (_unit isEqualTo _uid );
            _exec = false;
        } else {
            _exec = true; 
        	if ( isPlayer _unit ) then {
            	_remove = ( (getPlayerUID _unit) isEqualTo _uid );
        	} else {
                _remove = ( _unit isEqualTo _u );
            };
		};
        if ( _remove ) then {
            remoteExec [_j, ""];
    		ambientAnimJIP deleteAt _forEachIndex;
            if ( _exec ) then { _this remoteExec ["global_fnc_ambientAnimPlayer", 0, false]; };
        };
    } forEach ambientAnimJIP;	    
};

debug(LL_DEBUG, "ambientAnim : propagating to everyone");

private _jip = _this remoteExec ["global_fnc_ambientAnimPlayer", 0, true];

{
	_x params ["_j", "_u", ["_uid", ""]];
    if ( _unit isEqualTo _u ) exitWith {
        remoteExec [_j, ""];
    	ambientAnimJIP deleteAt _forEachIndex;    
    };
} forEach ambientAnimJIP;

private _new = [_jip, _unit];
if ( isPlayer _unit ) then { _new pushback (getPlayerUID _unit); };
ambientAnimJIP pushback _new;