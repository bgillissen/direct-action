class global {
	tag = "global";
	class functions {
		class ambientAnim { file="common\global\ambientAnim.sqf"; };
		class ambientAnimPlayer { file="common\global\ambientAnimPlayer.sqf"; };
		class chat { file="common\global\chat.sqf"; };
		class chatPlayer { file="common\global\chatPlayer.sqf"; };
		class doAnim { file="common\global\doAnim.sqf"; };
		class doAnimPlayer { file="common\global\doAnimPlayer.sqf"; };
		class hideObject { file="common\global\hideObject.sqf"; };
		class hint { file="common\global\hint.sqf"; };
		class hintPlayer { file="common\global\hintPlayer.sqf"; };
		class notification { file="common\global\notification.sqf"; };
		class notificationPlayer { file="common\global\notificationPlayer.sqf"; };
		class setFlagTexture { file="common\global\setFlagTexture.sqf"; };
		class setFlagTexturePlayer { file="common\global\setFlagTexturePlayer.sqf"; };
		class setTexture { file="common\global\setTexture.sqf"; };
		class setTexturePlayer { file="common\global\setTexturePlayer.sqf"; };
		class setUnitInsignia { file="common\global\setUnitInsignia.sqf"; };
		class setUnitInsigniaPlayer { file="common\global\setUnitInsigniaPlayer.sqf"; };
	};
};
