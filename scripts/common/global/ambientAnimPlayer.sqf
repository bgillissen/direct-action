
#include "_debug.hpp"

if !( CTXT_PLAYER ) exitWith { };

params ["_unit", "_anim", ["_lvl", "ASIS"], ["_snap", objNull], ["_inter", true]];

if ( _anim isEqualTo "" ) then {
  _unit call BIS_fnc_ambientAnim__terminate;  
} else {
   [_unit, _anim, _lvl, _snap, _inter] call BIS_fnc_ambientAnim; 
};