
#include "_debug.hpp"

if !( isServer ) exitWith {
    #ifdef DEBUG
	debug(LL_DEBUG, "doAnim : asking server to propagate");
	#endif
	_this remoteExec ["global_fnc_doAnim", 2, false];
};

debug(LL_DEBUG, "doAnim : propagating to everyone");
_this remoteExec ["global_fnc_doAnimPlayer", 0, false];