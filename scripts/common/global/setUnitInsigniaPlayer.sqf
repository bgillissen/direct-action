
if !( CTXT_PLAYER ) exitWith {};

params ["_unit", "_index", "_texture"];

_unit setObjectTexture [_index, _texture];