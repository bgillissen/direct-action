class global_fnc_ambientAnim {
	allowedTargets=2;
	jip=0;
};
class global_fnc_ambientAnimPlayer {
	allowedTargets=0;
	jip=1;
};
class global_fnc_doAnim {
	allowedTargets=2;
	jip=0;
};
class global_fnc_doAnimPlayer {
	allowedTargets=0;
	jip=0;
};
class global_fnc_hideObject {
	allowedTargets=2;
	jip=0;
};
class global_fnc_hintPlayer {
	allowedTargets=1;
	jip=0;
};
class global_fnc_notificationPlayer {
	allowedTargets=1;
	jip=0;
};
class global_fnc_setFlagTexturePlayer {
	allowedTargets=1;
	jip=1;
};
class global_fnc_setTexturePlayer {
	allowedTargets=1;
	jip=1;
};
class global_fnc_chat {
	allowedTargets=2;
	jip=0;
};
class global_fnc_chatPlayer {
	allowedTargets=1;
	jip=0;
};
class global_fnc_setUnitInsignia {
	allowedTargets=2;
	jip=0;
};
class global_fnc_setUnitInsigniaPlayer {
	allowedTargets=1;
	jip=1;
};
