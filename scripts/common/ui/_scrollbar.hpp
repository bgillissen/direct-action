
color[] = COL_SCROLL_COLOR;
colorActive[] = COL_SCROLL_ACTIVE;
colorDisabled[] = COL_SCROLL_DISABLED;
shadow = 0;
thumb = COL_SCROLL_THUMB;
arrowFull = COL_SCROLL_FULL;
arrowEmpty = COL_SCROLL_EMPTY;
border = COL_SCROLL_BORDER;
