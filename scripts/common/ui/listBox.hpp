
class daListBox {
	idc = -1;
	type = CT_LISTBOX;
	style = 16;
	access = 0;
	rowHeight = 0;
	color[] = COL_INPUT_MAIN;
	colorShadow[] = COL_INPUT_SHADOW;
	colorDisabled[] = COL_INPUT_DIS;
	colorText[] = COL_INPUT_TXT;
	colorScrollbar[] = COL_SCROLL_MAIN;
	colorSelect[] = COL_INPUT_SEL;
	colorSelect2[] = COL_INPUT_SEL;
	colorSelectBackground[] = COL_INPUT_BSEL;
	colorSelectBackground2[] = COL_INPUT_BSEL2;
	colorBackground[] = COL_INPUT_BCG;
	soundSelect[] = {"\A3\ui_f\data\sound\RscButtonMenu\soundClick", 0.09, 1};
	arrowEmpty = COL_SCROLL_EMPTY;
	arrowFull = COL_SCROLL_FULL;
	font = FONT;
	sizeEx = FONT_sizeEx;
	shadow = 0;
	period = 1.2;
	maxHistoryDelay = 1;
	autoScrollSpeed = -1;
	autoScrollDelay = 5;
	autoScrollRewind = 0;
	class ScrollBar {
		#include "_scrollbar.hpp"
	};
	x= 0;
	y = 0;
	w = 0.2;
	h = 0.2;
};
