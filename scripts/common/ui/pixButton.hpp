
class daPixButton {
	idc = -1;
	type = CT_SHORTCUTBUTTON;
	style = 0;
	access = 0;
	color[] = COL_TXT;
	color2[] = {0.95, 0.95, 0.95, 1};
	colorBackground[] = COL_USR_BCG;
	colorbackground2[] = COL_DISABLED_BCG;
	colorDisabled[] = COL_DISABLED_TXT;
	textureNoShortcut = "";
	animTextureNormal = "#(argb,8,8,3)color(1,1,1,1)";
	animTextureDisabled = "#(argb,8,8,3)color(0.3,0.3,0.3,1)";
	animTextureOver = "#(argb,8,8,3)color(0.8,0.3,0,1)";
	animTextureFocused = "#(argb,8,8,3)color(1,0.5,0,1)";
	animTexturePressed = "#(argb,8,8,3)color(1,0,0,1)";
	animTextureDefault = "#(argb,8,8,3)color(0,1,0,1)";
	period = 0.4;
	periodFocus = 1.2;
	periodOver = 0.8;
	font = FONT;
	size = FONT_SIZE;
	sizeEx = FONT_sizeEx;
	text = "";
	soundClick[] = {"\A3\ui_f\data\sound\rscbutton\soundclick", 0.07, 1};
	soundEnter[] = {"", 0.09, 1};
	soundPush[] = {"", 0.09, 1};
	soundEscape[] = {"", 0.09, 1};
	action = "";
	toolTip = "";
	class HitZone {
		left = 1;
		top = 1;
		right = 1;
		bottom = 1;
	};
	class ShortcutPos { //?????
		left = 0.0145;
		top = 0.026;
		w = 0.0392157;
		h = 0.0522876;
	};
	class TextPos { //????
		left = 0.05;
		top = 0.034;
		right = 0.005;
		bottom = 0.005;
	};
	class Attributes {
		font = FONT;
		color = COL_TEXT_HTML;
		align = "left";
		shadow = "true";
	};
	class AttributesImage {
		font = FONT;
		color = COL_TEXT_HTML;
		align = "left";
		shadow = "true";
	};
	x = 0;
	y = 0;
	w = 0.2;
	h = 0.2;
};
