
class daStructuredText {
	idc = -1;
	type = CT_STRUCTURED_TEXT;
	style = 0;
	access = 0;
	colorBackground[] = COL_NONE;
	colorText[] = COL_TXT;
	colorShadow[] = COL_SHADOW;
	shadow = 0;
	text = "daStructuredText";
	size = FONT_SIZE;
	class Attributes {
		font = FONT;
		color = COL_TXT_HTML;
		align = "left";
		shadow = 0;
	};
	x = 0;
	y = 0;
	h = 0.2;
	w = 0.2;
};
