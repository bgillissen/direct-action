
#define COL_USR_BCG {"(profilenamespace getvariable ['GUI_BCG_RGB_R',0.69])",\
					 "(profilenamespace getvariable ['GUI_BCG_RGB_G',0.75])",\
					 "(profilenamespace getvariable ['GUI_BCG_RGB_B',0.5])", 0.7}
#define COL_USR_TXT {1, 1, 1, 0.7}

#define COL_NONE {0, 0, 0, 0}
#define COL_MAIN {0, 0, 0, 0.7}
#define COL_BCG {0, 0, 0, 0.7}
#define COL_FOCUS_BORDER {"(profilenamespace getvariable ['GUI_BCG_RGB_R',0.69])",\
						  "(profilenamespace getvariable ['GUI_BCG_RGB_G',0.75])",\
					      "(profilenamespace getvariable ['GUI_BCG_RGB_B',0.5])", 1}
#define COL_TXT {1, 1, 1, 1}
#define COL_TXT_HTML "#ffffff"
#define COL_BORDER {0, 0, 0, 1}
#define COL_SHADOW {0, 0, 0, 0.5}

#define COL_ACTIVE_BCG {"(profilenamespace getvariable ['GUI_BCG_RGB_R',0.69])",\
						"(profilenamespace getvariable ['GUI_BCG_RGB_G',0.75])",\
						"(profilenamespace getvariable ['GUI_BCG_RGB_B',0.5])", 1}
#define COL_ACTIVE_TXT  {0.4, 0.4, 0.4, 1}

//#define COL_DISABLED_BCG {0.95, 0.95, 0.95, 1 }
//#define COL_DISABLED_TXT {0.4, 0.4, 0.4, 1}

#define COL_DISABLED_BCG {0.4, 0.4, 0.4, 0.6 }
#define COL_DISABLED_TXT {1, 1, 1, 0.4}


#define COL_INPUT_BCG {0, 0, 0, 0.7}
#define COL_INPUT_FOCUS {0, 0, 0, 1}
#define COL_INPUT_MAIN {1, 1, 1, 1}
#define COL_INPUT_SHADOW {0, 0, 0, 0.5}
#define COL_INPUT_TXT {0.95, 0.95, 0.95, 1}
#define COL_INPUT_DIS {0, 0, 0, 0.2}
#define COL_INPUT_SEL {"(profilenamespace getvariable ['GUI_BCG_RGB_R',0.69])",\
					 "(profilenamespace getvariable ['GUI_BCG_RGB_G',0.75])",\
					 "(profilenamespace getvariable ['GUI_BCG_RGB_B',0.5])", 1}
#define COL_INPUT_SEL2 {"(profilenamespace getvariable ['GUI_BCG_RGB_R',0.69])",\
					 "(profilenamespace getvariable ['GUI_BCG_RGB_G',0.75])",\
					 "(profilenamespace getvariable ['GUI_BCG_RGB_B',0.5])", 1}
#define COL_INPUT_BSEL {0.95, 0.95, 0.95, 1}
#define COL_INPUT_BSEL2 {1, 1, 1, 0.5}

#define COL_SCROLL_MAIN { 1, 1, 1, 1}
#define COL_SCROLL_COLOR { 1, 1, 1, 1}
#define COL_SCROLL_ACTIVE { 1, 1, 1, 1}
#define COL_SCROLL_DISABLED { 1, 1, 1, 0.3}
#define COL_SCROLL_THUMB "\A3\ui_f\data\gui\cfg\scrollbar\thumb_ca.paa"
#define COL_SCROLL_FULL "\A3\ui_f\data\gui\cfg\scrollbar\arrowFull_ca.paa"
#define COL_SCROLL_EMPTY "\A3\ui_f\data\gui\cfg\scrollbar\arrowEmpty_ca.paa"
#define COL_SCROLL_BORDER "\A3\ui_f\data\gui\cfg\scrollbar\border_ca.paa"

#define COL_FAILED {1, 0, 0, 0.7}
