
class daCombo {
	idc = -1;
	type = CT_COMBO;
	style = 16;
	access = 0;
	color[] = COL_INPUT_MAIN;
	colorShadow[] = COL_INPUT_SHADOW;
	colorDisabled[] = COL_INPUT_DIS;
	colorSelect[] = COL_INPUT_SEL;
	colorText[] = COL_INPUT_TXT;
	colorBackground[] = COL_INPUT_BCG;
	colorScrollbar[] = COL_SCROLL_MAIN;
	colorSelectBackground[] = COL_INPUT_BSEL;
	soundSelect[] = {"\A3\ui_f\data\sound\RscButtonMenu\soundClick", 0.09, 1};
	soundExpand[] = {"", 0.1, 1};
	soundCollapse[] = {"", 0.1, 1 };
	maxHistoryDelay = 1;
	shadow = 0;
	arrowEmpty = "\A3\ui_f\data\GUI\RscCommon\rsccombo\arrow_combo_ca.paa";
	arrowFull = "\A3\ui_f\data\GUI\RscCommon\rsccombo\arrow_combo_active_ca.paa";
	wholeHeight = 0.45;
	font = FONT;
	sizeEx = FONT_sizeEx;
	class comboScrollbar {
		#include "_scrollbar.hpp"
	};
	x = 0;
	y = 0;
	w = 0.2;
	h = 0.2;
};
