class daSlider {
	idc = -1;
	style = "0x400 + 0x10";
	type = CT_SLIDER;
	shadow = 0;
	color[] = {1,1,1,0.4};
	colorActive[] = {1,1,1,1};
	colorDisabled[] = {0.5,0.5,0.5,0.2};
	arrowEmpty = "\A3\ui_f\data\gui\cfg\slider\arrowEmpty_ca.paa";
	arrowFull = "\A3\ui_f\data\gui\cfg\slider\arrowFull_ca.paa";
	border = "\A3\ui_f\data\gui\cfg\slider\border_ca.paa";
	thumb = "\A3\ui_f\data\gui\cfg\slider\thumb_ca.paa";
	x = 0;
	y = 0;
	w = 0.2;
	h = 0.2;
};
