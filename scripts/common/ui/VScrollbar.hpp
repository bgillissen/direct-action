class daVScrollbar {
	width = SCROLLBAR;
	autoScrollSpeed = -1;
	autoScrollDelay = 5;
	autoScrollRewind = 0;
	autoScrollEnabled = 0;
	scrollSpeed = 0.06;
	#include "_scrollbar.hpp"
};
