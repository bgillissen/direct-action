/*
@filename: onPlayerRespawn.sqf
Author:
	ben
Description:
	run on player by game engine when player (re)spawn
	trigger the features linked to respawn event
*/

if !( CTXT_PLAYER ) exitWith {};

[] spawn {
	waitUntil {
		( (missionNamespace getVariable "SERVER_INIT") isEqualTo false )
	};
	waitUntil {
		( (missionNamespace getVariable "PLAYER_INIT") isEqualTo false )
	};
	["PLAYER", "respawn", [player]] call core_fnc_featEvent;
	["SERVER", "respawn", [player]] call core_fnc_featEvent;
};

nil